<?php
/**
 * Jet API integration 
 * Author : Nareshseeta
 * Api calling for products creation in Jet API
 * **/
  
  include('token.php');
    require_once '../app/Mage.php';
    umask(0);
    Mage::app('default');
	$collection = Mage::getResourceModel('catalog/product_collection');
	$collection->addFieldToFilter(array(array('attribute'=>'manufacturer','eq'=>'787'),));
	//print_r($collection->getData()); die;	
	$i = 0;
	$productArray = array();
	foreach($collection as $product){
	
		$product = Mage::getModel("catalog/product")->load($product->entityId);
			//print_r($product->getData());
			//echo '<br />'; die;
		$productArray['product_title'] = $product->name;
		$productArray['jet_browse_node_id'] = 13000065;
		$upc = $product->asin;//
		$productArray['standard_product_codes'][0]['standard_product_code'] = "$upc" ;
		$productArray['standard_product_codes'][0]['standard_product_code_type'] = "UPC";
		$productArray['category_path'] ="Flowers";
		$productArray['multipack_quantity'] = 1;
		$productArray['brand'] ="KaBloom";
		$productArray['manufacturer'] ="kabloom";
		$productArray['mfr_part_number'] = "$upc" ;
		$productArray['product_description'] = $product->description;
		$productArray['bullets'][0] = $product->description;
		//$productArray['bullets'][1] = $product->description;
		$productArray['number_units_for_price_per_unit'] = 1;
		$productArray['type_of_unit_for_price_per_unit'] = "each";
		$productArray['shipping_weight_pounds'] = 4;
		$productArray['package_length_inches'] = 25;
		$productArray['package_width_inches'] = 5;
		$productArray['package_height_inches'] = 5;
		$productArray['display_length_inches'] = 22;
		$productArray['display_width_inches'] = 4;
		$productArray['display_height_inches'] = 4;
		$productArray['prop_65'] = false;
		$productArray['legal_disclaimer_description'] = "null";
		$productArray['cpsia_cautionary_statements'][0] = "no warning applicable";
		$productArray['country_of_origin'] = "U.S.A.";
		$productArray['safety_warning'] = "";
	    $sellData =  date( 'Y-m-d\TH:i:s'. substr ( ( string ) microtime (), 1, 8 ) , strtotime($product->updated_at) );
		$productArray['start_selling_date'] = "$sellData";
		$productArray['fulfillment_time'] = 2;
		$productArray['msrp'] = round($product->price);
		$productArray['map_price'] = 0.01;
		$productArray['map_implementation'] = "103";
		$productArray['product_tax_code'] = "123";
		$productArray['no_return_fee_adjustment'] = round(0.00);		
		$productArray['exclude_from_fee_adjustments'] = true;
		$productArray['ships_alone'] = true;
		$productArray['attributes_node_specific'][0]['attribute_id']= 2;
		$productArray['attributes_node_specific'][0]['attribute_value']="yellow";
		$imageUrl  =  Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product/k/j/' . strtolower($product->sku).'.jpg';
		$productArray['main_image_url'] ="$imageUrl";
		$productArray['swatch_image_url'] = "";
		$productArray['alternate_images'][0]['image_slot_id'] = 1;
		$productArray['alternate_images'][0]['image_url'] = "$imageUrl";
		print_r($productArray);
		$end_point = "merchant-skus/".$product->sku;
		
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$writeConnection = $resource->getConnection('core_write');
		$warehouse_id = 3;
		$query_qty_select_warehouse = "select `quantity_in_stock` from `advancedinventory_stock` where product_id =" .$product->entityId . " and place_id =" . $warehouse_id;
		 $existing_qty_warehouse = $readConnection->fetchOne($query_qty_select_warehouse);
		$productQuantityArray = array();
		$productQuantityArray['fulfillment_nodes'][0]['fulfillment_node_id'] = "e36e9d240ffb409f870f8ecba1ae957e";
		$productQuantityArray['fulfillment_nodes'][0]['quantity'] = intval($existing_qty_warehouse);
		//echo "this quantity"; print_r($productQuantityArray);
		$quantityEnd_point = "merchant-skus/".$product->sku."/inventory";
		
		$productPriceArray = array();
		$productPriceArray['price'] = floatval($product->price);
		$productPriceArray['fulfillment_nodes'][0]['fulfillment_node_id'] = "e36e9d240ffb409f870f8ecba1ae957e";
		$productPriceArray['fulfillment_nodes'][0]['fulfillment_node_price'] = floatval($product->price);
		//echo "this price";print_r($productPriceArray);
		$priceEnd_point = "merchant-skus/".$product->sku."/price";
		
		$productImageArray = array();
		$productImageArray['main_image_url'] = "$imageUrl";
		$productImageArray['swatch_image_url'] = "";
		$productImageArray['alternate_images'][0]['image_slot_id'] = 1;
		$productImageArray['alternate_images'][0]['image_url'] = "$imageUrl";//echo "this price";print_r($productPriceArray);
		$imageEnd_point = "merchant-skus/".$product->sku."/image";
		//echo "this image";print_r($productImageArray);
		
		jetAPIPUT($end_point, $productArray);
		jetAPIPUT($quantityEnd_point, $productQuantityArray);
		jetAPIPUT($priceEnd_point, $productPriceArray);
		jetAPIPUT($imageEnd_point, $productImageArray);
		
		$data = jetAPIGET($end_point);
		print_r($data);
		echo "<br />" ;
		echo "this is product".$i++;
		//print_r($productPriceArray);
		
		 //die;
		
	}

		
	
	
	
	
	?>
