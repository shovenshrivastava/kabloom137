<?php
/**
 * Jet API integration 
 * Author : Nareshseeta
 * Api calling for Orders retrival form Jet API
 * **/

	include('createorder.php');
	include('token.php');
	include('returnorder.php');
	include('../app/Mage.php');
    umask(0);
    Mage::app('default');

		
     function processOrdersByStatus($status){		 
		$data = jetAPIGET("orders/$status");
				
		foreach($data->order_urls as $end_point){
			//echo $end_point;			
			processOrder($end_point);
			}	
		}
		
		
		
	 function processOrder($end_point){		 
		$data = jetAPIGET($end_point);
		//print_r($data);
		$ackJetOrder = createOrder($data);
		acknowledgeJetOrder($ackJetOrder);
			
		}
	
	
	
	function acknowledgeJetOrder($ackJetOrder) {
		echo "this is ack";

		
		$orderArray = array();	
		$order = Mage::getModel('sales/order')->loadByIncrementId($ackJetOrder['orderId']);
		$ordered_items = $order->getAllItems(); 			
		$orderArray['acknowledgement_status'] = $ackJetOrder['status'];
		$orderArray['alt_order_id'] = $ackJetOrder['orderId'];
		$i = 0;
		foreach($ordered_items as $item){ 
		
		$orderArray['order_items'][$i]['order_item_acknowledgement_status'] = "fulfillable" ;
		$orderArray['order_items'][$i]['order_item_id'] = $item->getJetOrderItemId(); 
		$orderArray['order_items'][$i]['alt_order_item_id'] = "";    
	  	$i++;
		   } 
		//print_r(json_encode($orderArray));
		$end_point = "orders/".$order->getPartnerOrderId()."/acknowledge";
		jetAPIPUT($end_point, $orderArray);
		 //die;
		
	}
	
	
	
	
	
	function jetInventoryUpdate(){
		
	$collection = Mage::getResourceModel('catalog/product_collection');
	$collection->addFieldToFilter(array(array('attribute'=>'manufacturer','eq'=>'787'),));
	//print_r($collection->getData()); die;	

	foreach($collection as $product){
			 
	 $_product = Mage::getSingleton('catalog/product')->load($product->entityId);
		$sku = $_product->getSku();//die;
	  	$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$writeConnection = $resource->getConnection('core_write');
		$warehouse_id = 3;
		$query_qty_select_warehouse = "select `quantity_in_stock` from `advancedinventory_stock` where product_id =" .$product->entityId . " and place_id =" . $warehouse_id;
		$existing_qty_warehouse = $readConnection->fetchOne($query_qty_select_warehouse);
		$productQuantityArray = array();
		$productQuantityArray['fulfillment_nodes'][0]['fulfillment_node_id'] = "e36e9d240ffb409f870f8ecba1ae957e";
		$productQuantityArray['fulfillment_nodes'][0]['quantity'] = intval($existing_qty_warehouse);
		$quantityEnd_point = "merchant-skus/".$sku."/inventory";
			//print_r($productQuantityArray); //die;	
		jetAPIPUT($quantityEnd_point, $productQuantityArray);
		}
	}		
	
     function processReturnOrders($status){		 
		$data = jetAPIGET("returns/$status");
				
		foreach($data->return_urls as $end_point){
		//echo $end_point;			
			returnOrder($end_point);
			}	
		}
	
			
	 function returnOrder($end_point){		 
		$data = jetAPIGET($end_point);
		//print_r($data); 
		$ackJetReturn = createReturn($data, $end_point);
		//print_r($ackJetReturn);
		acknowledgeReturnJetOrder($ackJetReturn);
			
		}
    
    function acknowledgeReturnJetOrder($ackJetReturn) {
		//echo "this is return ack";
		
		
		$order = Mage::getModel('sales/order')->load($ackJetReturn['orderId']);
		$orderReturnArray = array();	
		$orderReturnCompleteArray = array();
		
		$ordered_items = $order->getAllItems(); 			
		$orderReturnArray['return_status'] = $ackJetReturn['status'];
		$orderReturnArray['jet_pick_return_location'] = false;
		$orderReturnCompleteArray['merchant_order_id']= $order->getPartnerOrderId(); 
		$i = 0;
		foreach($ordered_items as $item){ 
		
		$orderReturnArray['return_location'][$i]['order_item_id'] = $item->getJetOrderItemId() ;
		$orderReturnArray['return_location'][$i]['location_name'] = "Kabloom"; 
		$orderReturnArray['return_location'][$i]['location_address_1'] = "2 Pin Oak Lane, Suite 100";  
		$orderReturnArray['return_location'][$i]['location_address_2'] = "" ;
		$orderReturnArray['return_location'][$i]['location_city'] = "CHERRY HILL"; 
		$orderReturnArray['return_location'][$i]['location_state'] = "NJ";  
		$orderReturnArray['return_location'][$i]['location_postal_code'] = "08003" ;
		
		$orderReturnCompleteArray['items'][$i]['order_item_id'] = $item->getJetOrderItemId() ;
		$orderReturnCompleteArray['items'][$i]['total_quantity_returned'] = round($item->getQtyOrdered()); 
		$orderReturnCompleteArray['items'][$i]['order_return_refund_qty'] = round($item->getQtyOrdered());  
		$orderReturnCompleteArray['items'][$i]['return_refund_feedback'] = "other" ;
		$orderReturnCompleteArray['items'][$i]['refund_amount']['principal'] = $ackJetReturn['principal']; 
		$orderReturnCompleteArray['items'][$i]['refund_amount']['tax'] = 0;  
		$orderReturnCompleteArray['items'][$i]['refund_amount']['shipping_cost']= $ackJetReturn['shipping_cost']; 
		$orderReturnCompleteArray['items'][$i]['refund_amount']['shipping_tax']=  0;

	  	$i++;
		   } 
		   
		$orderReturnCompleteArray['agree_to_return_charge']= false ;   
		//print_r(json_encode($orderReturnArray));
		echo $end_point = "returns/".$ackJetReturn['id']."/complete";
		jetAPIPUT($end_point, $orderReturnCompleteArray);
		 //die;
		
	}
	
	
	
	
	

	
		jetInventoryUpdate();	
		processOrdersByStatus('ready');
		processReturnOrders('created');
	    echo "this is order end";
	
?>

