<?php 
/* Author : Nareshseeta
 * Auto Order Status Update running for every 1hr mon-thu
 * updating status for orders which have status invoice_sent will be Done  
 * */
    ini_set('display_errors', 1);
    require_once ('app/Mage.php');
    umask(0);
    Mage::app('default');
    echo "Order Status Update Begins"; echo "<br />";
    $time = time();

$tolastTime = $time - 86400; // 60*60*24*1
$lastTime = $time - 172800; // 60*60*24*2
$from = date('Y-m-d H:i:s', $lastTime);
$to = date('Y-m-d H:i:s', $tolastTime);
$orders = Mage::getModel('sales/order')->getCollection()
    ->addAttributeToFilter('created_at', array('from'=>$from, 'to'=>$to))
    ->addAttributeToFilter('status', array('eq' =>'invoice_sent'));
		
	foreach ($orders as $order) {
		
		echo "Status done for Order No #".$order->getIncrementId(); 
		
		echo "<br />";
		 $order->setStatus('done')->save();
	
		//die;
	}
