#!/usr/bin/env bash

#Date : 28 April 2015
#Author : Kaushal Mewar
#Description : The script is used for performing database and document root backup and place it inside a pre-defined directory defined in one #of the variables.

#start

function main() {
	init 
	chkBackupDir
}

function init() {
	BACKUP_DIR=/home/kabloom/backup
	DB_USER=kabloom_us
	DB_PASSWORD=Abcd123#
	DB_NAME=kabloom_kabloom
	DOC_ROOT=/home/kabloom/public_html	#path to document root
	DAYS_TO_KEEP_BACKUP=5
}

function chkBackupDir() {
	if [[ ! -d "$BACKUP_DIR" ]]; then
	mkdir "$BACKUP_DIR"

	fi
	backupDocRoot
}

function backupDocRoot() {
	`which tar` -cjf "$BACKUP_DIR"/doc_root_backup-$(date +%d-%m-%y).tar.bz2 --directory="$DOC_ROOT" .

	if [[ -e "$BACKUP_DIR"/doc_root_backup-$(date +%d-%m-%y).tar.bz2 ]]; then
	backupDatabase
	else
	echo "Backup failed for document root on your server.Please check manually" | mail -s "Backup Failed - Document Root" kaushal@cdotsys.in
	fi
}

function backupDatabase()
{
	`which mysqldump` -u "$DB_USER" -p"$DB_PASSWORD" "$DB_NAME" > "$BACKUP_DIR"/"$DB_NAME"-$(date +%d-%m-%y).sql | tee "$BACKUP_DIR"/backup.log

	tar -cjf "$BACKUP_DIR"/"$DB_NAME"-$(date +%d-%m-%y).tar.bz2 "$BACKUP_DIR"/"$DB_NAME"-$(date +%d-%m-%y).sql --remove-file 2> $BACKUP_DIR/tar.log

	if [[ -e "$BACKUP_DIR"/"$DB_NAME"-$(date +%d-%m-%y).tar.bz2 ]]; then

	createTar 
	else
	echo "Backup failed for database on your server.Please check manually" | mail -s "Backup Failed - Database" kaushal@cdotsys.in
	fi
}

function createTar()
{
	cd "$BACKUP_DIR"
	`which tar` -cf backup-$(date +%d-%m-%y).tar doc_root_backup-$(date +%d-%m-%y).tar.bz2 "$DB_NAME"-$(date +%d-%m-%y).tar.bz2 --remove-files 2> final.log
 deleteOldBackup
}

function deleteOldBackup()
{
	`which find` "$BACKUP_DIR"/"$DB_NAME"* -mtime +"$DAYS_TO_KEEP_BACKUP" -exec rm {} \; >> "$BACKUP_DIR"/delete.log
}

main 

#End
