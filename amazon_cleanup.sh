#!/bin/sh
# How many days retention do we want ?
DAYS=0

# geting present day 
now=$(date +"%m_%d_%Y")

# Where is the base directory
BASEDIR=/var/www/html/mendelson1/messages/Amazon/sent/kabloom


#where is the backup directory
BKPDIR=/var/www/backup

# Where is the log file
LOGFILE=$BKPDIR/log/amazon.log

# add to tar 
tar -cvzf $now.tar.gz $BASEDIR

mv $now.tar.gz $BKPDIR
 
# REMOVE OLD FILES
echo `date` Purge Started >> $LOGFILE
find $BASEDIR -mtime +$DAYS | xargs rm
echo `date` Purge Completed >> $LOGFILE
