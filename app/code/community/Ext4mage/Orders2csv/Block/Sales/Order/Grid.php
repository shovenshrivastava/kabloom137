<?php
/**
 * Ext4mage Orders2csv Module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to Henrik Kier <info@ext4mage.com> so we can send you a copy immediately.
 *
 * @category   Ext4mage
 * @package    Ext4mage_Orders2csv
 * @copyright  Copyright (c) 2012 Ext4mage (http://ext4mage.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Henrik Kier <info@ext4mage.com>
 * */
class Ext4mage_Orders2csv_Block_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
	const XPATH_CONFIG_SETTINGS_IS_ACTIVE		= 'orders2csv/settings/is_active';
	
	protected function _prepareMassaction()
    {
        parent::_prepareMassaction();
    	if (Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_IS_ACTIVE)) {
    		
	        $this->getMassactionBlock()->addItem('orders2csv', array(
	             'label'=> Mage::helper('sales')->__('Orders2CSV'),
	             'url'  => $this->getUrl('*/sales_order_orders2csv/makecsv'),
	        ));
    	}
		$this->getMassactionBlock()->setUseSelectAll(true);
    }



    
     protected function _prepareColumns()
    {
 
        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order#'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
		'filter_index'=>'main_table.increment_id',
	
        ));
		
		 /*$this->addColumnAfter('Fedex Label Status', array(
                'header' => Mage::helper('sales')->__('Fedex Label Status'),
                'index' => 'fedex_label_status',
             
            ),'real_order_id');*/

	  $this->addColumnAfter('Pickup date', array(
                'header' => Mage::helper('sales')->__('Pickup date'),
                'index' => 'pickup_date',
                'type' => 'datetime',                
                'column_css_class' => 'pickup_date'
            ),'real_order_id');

            $this->addColumnAfter('Pickup Method', array(
                'header' => Mage::helper('sales')->__('Pickup Method'),
                'index' => 'pickup_method',
				'column_css_class' => 'pickup_method'
            ),'real_order_id');
            
             $this->addColumnAfter('Trading Partner', array(
                'header' => Mage::helper('sales')->__('Trading Partner'),
                'index' => 'trading_partner',
              //    'filter'    => false,
	'filter_condition_callback' => array($this, '_partnerFilter'),
         //  'sortable'  => false
             
            ),'real_order_id');
            
            $this->addColumnAfter('Partner Order ID ', array(
                'header' => Mage::helper('sales')->__('Partner Order ID'),
                'index' => 'partner_order_id',
                'type'  => 'text',
				'column_css_class' => 'pickup_method'
            ),'real_order_id');
             
			 $this->addColumnAfter('Delivery date', array(
                'header' => Mage::helper('sales')->__('Delivery date'),
                'index' => 'delivering_date',
                
                
            ),'real_order_id');
            
           /*$this->addColumnAfter('Delivery Date', array(
                'header' => Mage::helper('operations')->__('Delivery Date'),
                'index' => 'entity_id',
                'sortable'  => true,
                'filter'    => false,
				'type' => 'datetime',
                'renderer'  => 'operations_adminhtml/operations_order_renderer_delivery',
        ),'real_order_id');*/
           

                if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('sales')->__('Purchased from (store)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
                'filter_index' => 'main_table.store_id'
            ));
        }
 
        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
            'filter_index' => 'main_table.created_at'
        ));
 
        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));
 
      /*  $this->addColumn('qty_ordered', array(
            'header'    => Mage::helper('sales')->__('Items Ordered'),
            'index'     => 'qty_ordered',
            'type'      => 'number',
            'total'     => 'sum'
        ));
 
        $this->addColumn('sku', array(
            'header'    => Mage::helper('catalog')->__('SKU'),
            'index'     => 'sku',
			'type' => 'text'
        ));

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Base)'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ));
 
        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ));
  */
        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
            'filter_index'=>'main_table.status',
        ));
 
 
        return $this;
    }
    
}
?>
