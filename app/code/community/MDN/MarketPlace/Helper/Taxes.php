<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_MarketPlace
 * @version 2.1
 */
class MDN_MarketPlace_Helper_Taxes extends Mage_Core_Helper_Abstract
{

    /**
     * Get Tax Rate
     *
     * @param $product
     * @return float
     */
    public function getTaxRate($product)
    {
        $config = Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject();

        $storeId = Mage::registry('mp_country')->getParam('store_id');
        $customerTaxClassId = $config->getmp_customer_tax_class_id();
        $countryCode = Mage::registry('mp_country')->getParam('country_code');
        $regionId = Mage::registry('mp_country')->getParam('region');
        $zipCode = Mage::registry('mp_country')->getParam('zip_code');

        return $this->calculateProductTaxRate($product, $storeId, $countryCode, $regionId, $customerTaxClassId, $zipCode);
    }

    /**
     * Calculate Product Tax Rate
     *
     * @param Mage_Catalog_Model_Product $product
     * @param int $storeId
     * @param string $countryCode
     * @param int $region
     * @param int $customerTaxClassId
     * @param int|string $postCode
     * @return float $rate
     */
    public function calculateProductTaxRate($product, $storeId, $countryCode, $region, $customerTaxClassId, $postCode)
    {
        $store = Mage::app()->getStore($storeId);
        $productTaxClassId = $product->gettax_class_id();

        $taxRequest = new Varien_Object();
        $taxRequest->setCountryId($countryCode);
        $taxRequest->setRegionId($region);
        $taxRequest->setPostcode($postCode);
        $taxRequest->setStore($store);
        $taxRequest->setCustomerClassId($customerTaxClassId);
        $taxRequest->setProductClassId($productTaxClassId);

        $taxCalculationModel = Mage::getSingleton('tax/calculation');
        $rate = $taxCalculationModel->getRate($taxRequest);

        return $rate;
    }

    /**
     * Get Shipping Tax Rate
     *
     * @return float
     */
    public function getShippingTaxRate()
    {
        $config = Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject();

        $storeId = Mage::registry('mp_country')->getParam('store_id');
        $customerTaxClassId = $config->getmp_customer_tax_class_id();
        $countryCode = Mage::registry('mp_country')->getParam('country_code');
        $regionId = Mage::registry('mp_country')->getParam('region');
        $zipCode = Mage::registry('mp_country')->getParam('zip_code');

        return $this->calculateShippingTaxRate($storeId, $countryCode, $regionId, $customerTaxClassId, $zipCode);
    }

    /**
     * Calculate Shipping Tax Rate
     *
     * @param int $storeId
     * @param string $countryCode
     * @param int $region
     * @param int $customerTaxClassId
     * @param int|string $postCode
     * @return float $rate
     */
    public function calculateShippingTaxRate($storeId, $countryCode, $region, $customerTaxClassId, $postCode)
    {
        $store = Mage::app()->getStore($storeId);
        $shippingTaxClassId = Mage::getStoreConfig('tax/classes/shipping_tax_class', $storeId);

        $taxRequest = new Varien_Object();
        $taxRequest->setCountryId($countryCode);
        $taxRequest->setRegionId($region);
        $taxRequest->setPostcode($postCode);
        $taxRequest->setStore($store);
        $taxRequest->setCustomerClassId($customerTaxClassId);
        $taxRequest->setProductClassId($shippingTaxClassId);

        $taxCalculationModel = Mage::getSingleton('tax/calculation');
        $rate = $taxCalculationModel->getRate($taxRequest);

        return $rate;
    }
}
