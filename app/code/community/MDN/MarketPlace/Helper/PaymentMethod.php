<?php

/**
 * Class MDN_MarketPlace_Helper_PaymentMethod
 *
 * @category  {category}
 * @package   MDN_MarketPlace
 * @author    Nicolas Mugnier <nicolas@boostmyshop.com>
 * @copyright 2012-2015 Copyright (c) Boost My Shop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      {link}
 */
class MDN_MarketPlace_Helper_PaymentMethod extends Mage_Core_Helper_Abstract
{

    /**
     * Get Name
     *
     * @return string
     * @throws Exception
     */
    public function getName()
    {
        $payment_method = "purchaseorder";

        if (empty($payment_method)) {
            throw new Exception('Default payment method not set');
        }
        
        return $payment_method;
    }

    /**
     * Get Default Method
     *
     * @return string
     */
    private function getDefaultMethod()
    {
        return Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_default_payment_method();
    }
}
