<?php

/*
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_MarketPlace
 * @version 2.1
 */
class MDN_MarketPlace_Helper_Product extends Mage_Core_Helper_Abstract
{

    /**
     * Get product stock availibility
     *
     * @param product $product
     * @return integer
     */
    public function getStockToExport($product, $mp = null)
    {
        return Mage::Helper('MarketPlace/Stock')->getValueToExportForProduct($product, Mage::registry('mp_country'));
    }

    /**
     * get delay to export
     *
     * @param product $product
     * @return int
     */
    public function getDelayToExport($product)
    {
        return Mage::Helper('MarketPlace/Delay')->getValueToExportForProduct($product);
    }

    /**
     * Get product price (including tax)
     *
     * @param product $product
     * @return float
     *
     * @see getPrice()
     */
    public function getPriceToExport($product)
    {
        return Mage::Helper('MarketPlace/Price')->getPriceToExport($product);
    }
    
    /**
     * Get price before event
     *
     * @param Varien_Object $product
     * @return float $price
     */
    public function getPriceBeforeEvent($product)
    {
        return Mage::Helper('MarketPlace/Price')->getPriceBeforeEvent($product);
    }

    /**
     * Get price
     *
     * @param product $product
     * @return float
     */
    public function getPrice($product)
    {
        return Mage::Helper('MarketPlace/Price')->getPrice($product);
    }
    
    /**
     * get products from request
     *
     * @param string $marketplace
     * @param mixed $request
     * @return Varien_Data_Collection
     */
    public function getProductsFromRequest($marketplace, $request)
    {
        return Mage::Helper('MarketPlace/ProductCollection')->getProductsFromRequest($marketplace, $request);
    }
    
    /**
     * Get product collection to export
     *
     * @param int $marketplaceId
     * @return Varien_Data_Collection
     */
    public function getProductsToExport($marketplaceId)
    {
        return Mage::Helper('MarketPlace/ProductCollection')->getProductsToExport($marketplaceId);
    }

    /**
     * Is product has special price
     *
     * @param object $product
     * @return boolean
     */
    public function hasSpecialPrice($product)
    {
        return Mage::Helper('MarketPlace/Price')->hasSpecialPrice($product);
    }

    /**
     * Format string
     *
     * @param string $txt
     * @return string
     */
    public function formatExportedTxt($txt)
    {
        $txt = strip_tags($txt);
        $txt = preg_replace('/"/', "'", $txt);
        return $txt;
    }

    /**
     * Get product from barcode
     *
     * @param int $ean
     * @return object
     */
    public function getProductFromBarcode($ean)
    {
        $p = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToSelect(Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_barcode_attribute())
                    ->addFieldToFilter(Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_barcode_attribute(), $ean);
                    
        return ($p->count() > 0) ? $p->getFirstItem() : null;
    }
    
    /**
     * Get product description
     *
     * @param object $product
     * @return string $retour
     */
    public function getDescription($product)
    {
        return Mage::Helper('MarketPlace/Description')->getValueForProduct($product);
    }

    /**
     * Exclude products from request
     *
     * @param $request
     */
    public function exclude($request)
    {
        $ids = $request->getPost('product_ids');
        $countryId = $request->getParam('country_id');
        Mage::getModel('MarketPlace/Data')->exclude($ids, $countryId);
    }

    /**
     * Exclude products from request
     *
     * @param $request
     */
    public function activate($request)
    {
        $ids = $request->getPost('product_ids');
        $countryId = $request->getParam('country_id');
        Mage::getModel('MarketPlace/Data')->activate($ids, $countryId);
    }
}
