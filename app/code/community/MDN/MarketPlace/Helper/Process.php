<?php

/**
 * Class MDN_MarketPlace_Helper_Process
 *
 * @category  {category}
 * @package   MDN_MarketPlace
 * @author    Nicolas Mugnier <nicolas@boostmyshop.com>
 * @copyright 2012-2015 Copyright (c) Boost My Shop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      {link}
 */
class MDN_MarketPlace_Helper_Process extends Mage_Core_Helper_Abstract {

    /**
     * Process Match
     *
     * @param string $mp
     * @param Varien_Object $country
     * @param int $productId
     * @param int $countryId
     * @return mixed
     * @throws Exception
     */
    public function processMatch($mp, $country, $productId, $countryId){

        if (Mage::Helper(ucfirst($mp) . '/ProductCreation')->allowMatchingEan()) {

            $product = Mage::getModel('Catalog/Product')->setStoreId($country->getParam('store_id'))->load($productId);
            $barcode = Mage::Helper('MarketPlace/Barcode')->getBarcodeForProduct($product);

            if (Mage::Helper('MarketPlace/Checkbarcode')->checkCode($barcode) === true) {

                $helper = Mage::Helper(ucfirst($mp) . '/Matching');
                $mathingArray = $helper->buildMatchingArray(array($product));
                $helper->Match($mathingArray);

                $message = Mage::Helper('MarketPlace')->__('Matching submitted');
            } else {
                Mage::getModel('MarketPlace/Data')->updateStatus(array($productId), $countryId, MDN_MarketPlace_Helper_ProductCreation::kStatusInError);
                Mage::getModel('MarketPlace/Data')->addMessage($productId, Mage::Helper('MarketPlace')->__('Invalid EAN code'), $countryId);
                throw new Exception(Mage::Helper('MarketPlace')->__('Invalid EAN code for product ID : %s', $productId));
            }
        } else {
            $message = Mage::Helper('MarketPlace')->__(ucfirst($mp) . ' is not allowed to process matching EAN');
            throw new Exception($message);
        }

        return $message;

    }

    /**
     * Process Add
     *
     * @param string $mp
     * @param int $productId
     * @return string
     */
    public function processAdd($mp, $productId){

        $request = new Zend_Controller_Request_Http();
        $request->setParam('product_ids', array($productId));
        Mage::Helper(ucfirst($mp) . '/ProductCreation')->massProductCreation($request);
        return Mage::Helper('MarketPlace')->__('Product creation submitted');

    }

    /**
     * Process Revise
     *
     * @param string $mp
     * @param int $productId
     * @return string
     */
    public function processRevise($mp, $productId){

        $request = new Zend_Controller_Request_Http();
        $request->setParam('product_ids', array($productId));
        Mage::Helper(ucfirst($mp) . '/ProductCreation')->massReviseProducts($request);
        return Mage::Helper('MarketPlace')->__('Product data submitted');

    }

    /**
     * Process Update
     *
     * @param string $mp
     * @param int $productId
     * @return string
     */
    public function processUpdate($mp, $productId){

        Mage::Helper(ucfirst($mp) . '/ProductUpdate')->update(array($productId));
        return Mage::Helper('MarketPlace')->__('Stock and price submitted');

    }

    /**
     * Process Update Image
     *
     * @param string $mp
     * @param int $productId
     * @return string
     */
    public function processUpdateimage($mp, $productId){

        Mage::Helper(ucfirst($mp) . '/ProductUpdate')->updateImageFromGrid(array($productId));
        return Mage::Helper('MarketPlace')->__('Picture submitted');

    }

    /**
     * Process Delete
     *
     * @param string $mp
     * @param int $productId
     * @return string
     */
    public function processDelete($mp, $productId){

        $helper = Mage::Helper(ucfirst($mp) . '/Delete');
        $helper->process(array($productId));
        return Mage::Helper('MarketPlace')->__('Product deleted');

    }

    /**
     * Process Set As Created
     *
     * @param int $productId
     * @param int $countryId
     * @return string
     */
    public function processSetascreated($productId, $countryId){

        $ids = array($productId);
        $status = 'created';
        Mage::getModel('MarketPlace/Data')->updateStatus($ids, $countryId, $status);
        return Mage::Helper('MarketPlace')->__('Product status updated as created');

    }

    /**
     * Process Set As Not Created
     *
     * @param int $productId
     * @param int $countryId
     * @return string
     */
    public function processSetasnotcreated($productId, $countryId){

        $ids = array($productId);
        $status = 'notCreated';
        Mage::getModel('MarketPlace/Data')->updateStatus($ids, $countryId, $status);
        return Mage::Helper('MarketPlace')->__('Product status updated as not created');

    }

    /**
     * Process Set As Pending
     *
     * @param int $productId
     * @param int $countryId
     * @return string
     */
    public function processSetaspending($productId, $countryId){

        $ids = array($productId);
        $status = 'pending';
        Mage::getModel('MarketPlace/Data')->updateStatus($ids, $countryId, $status);
        return Mage::Helper('MarketPlace')->__('Product status updated as pending');

    }

}