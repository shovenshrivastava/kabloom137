<?php

/**
 * Class MDN_MarketPlace_Helper_Log
 *
 * @category  {category}
 * @package   MDN_MarketPlace
 * @author    Nicolas Mugnier <nicolas@boostmyshop.com>
 * @copyright 2012-2015 Copyright (c) Boost My Shop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      {link}
 */
class MDN_MarketPlace_Helper_Log extends Mage_Core_Helper_Abstract {

    /**
     * Set log for marketplace imporation orders
     *
     * @param array $successOrders
     * @param array $skippedOrders
     * @param array $errorOrders
     * @param array $errorMessages
     * @throws Exception
     * @return string
     */
    public function setOrderImportationLog($successOrders, $skippedOrders, $errorOrders, $errorMessages = array())
    {

        // Some error occured during order importation (invalid product)
        if (count($errorOrders) != 0) {

            // build error message
            $errorMsg = "An error occured during orders importation :";
            foreach ($errorOrders as $order) {
                $errorMsg .= ' ' . $order.(isset($errorMessages[$order]) ? ' : '.$errorMessages[$order] : '');
            }

            throw new Exception($errorMsg);
        }

        $successMsg = "Imported orders :";
        // add information log
        if (count($successOrders) != 0) {
            // build success message
            foreach ($successOrders as $order) {
                $successMsg .= ' ' . $order;
            }
        } else {
            $successMsg .= ' none';
        }

        $successMsg .= ' Skipped orders (already exist) :';
        if (count($skippedOrders) != 0) {
            foreach ($skippedOrders as $order) {
                $successMsg .= " " . $order;
            }
        } else {
            $successMsg .= ' none.';
        }

        return $successMsg;
    }

}