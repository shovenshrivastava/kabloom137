<?php

/**
 * Class MDN_MarketPlace_Helper_Block_Abstract
 *
 * @category  {category}
 * @package   MDN_MarketPlace
 * @author    Nicolas Mugnier <nicolas@boostmyshop.com>
 * @copyright 2012-2015 Copyright (c) Boost My Shop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      {link}
 */
abstract class MDN_MarketPlace_Helper_Block_Abstract extends Mage_Core_Helper_Abstract {

    const XML_PATH_PAYMENT_METHODS = 'payment';

    /**
     * Get attributes
     *
     * @return array $retour
     */
    public function getAttributeOptions()
    {
        $retour = array('' => '');

        $entityTypeId = Mage::getModel('eav/entity_type')->loadByCode('catalog_product')->getId();
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setEntityTypeFilter($entityTypeId);

        foreach ($attributes as $attribute) {
            $retour[$attribute->getAttributeCode()] = $attribute->getName();
        }

        return $retour;
    }

    /**
     * Get yes/no
     *
     * @return array
     */
    public function getYesNoOptions()
    {
        return array(
            '0' => $this->__('No'),
            '1' => $this->__('yes')
        );
    }

    /**
     * Get payments
     *
     * @return array $retour
     */
    public function getPaymentOptions()
    {
        $retour = array();
        $methods = Mage::getStoreConfig(self::XML_PATH_PAYMENT_METHODS, null);

        foreach ($methods as $code => $methodConfig) {
            $prefix = self::XML_PATH_PAYMENT_METHODS.'/'.$code.'/';

            if (!$model = Mage::getStoreConfig($prefix.'model', null)) {
                continue;
            }

            $retour[Mage::getModel($model)->getcode()] = Mage::getModel($model)->getcode();
        }

        return $retour;
    }

    /**
     * Get order statuses
     *
     * @return array $retour
     */
    public function getOrderStateOptions()
    {
        $retour = array();

        $statuses = Mage::getSingleton('sales/order_config')->getStatuses();

        foreach ($statuses as $code => $label) {
            $retour[$code] = $label;
        }

        return $retour;
    }

    /**
     * Get categories
     *
     * @return array $retour
     */
    public function getCategoryOptions()
    {
        $retour = array();

        $collection = Mage::getResourceModel('catalog/category_collection');

        $collection->addAttributeToSelect('name')
            ->addPathFilter('^1/[0-9]+$')
            ->load();

        foreach ($collection as $category) {
            $retour[] = array(
                'value' => $category->getId(),
                'label' => $category->getName()
            );
        }

        return $retour;
    }

    /**
     * Get customer group options
     *
     * @return array $retour
     */
    public function getCustomerGroupOptions()
    {
        $retour = array();

        $groups = mage::getModel('Customer/group')
            ->getCollection();

        foreach ($groups as $group) {
            $retour[$group->getcustomer_group_id()] = $group->getcustomer_group_code();
        }

        return $retour;
    }

}