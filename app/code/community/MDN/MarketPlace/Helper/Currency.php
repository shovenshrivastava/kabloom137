<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_MarketPlace
 * @version 2.1
 */
class MDN_MarketPlace_Helper_Currency extends Mage_Core_Helper_Abstract
{
    
    /**
     * Get currency for current country
     *
     * @return string $currency
     * @throws Exception
     */
    public function getCurrencyForCurrentCountry()
    {

        $country = Mage::registry('mp_country');
        
        if ($country instanceof MDN_MarketPlace_Model_Countries) {

            $currencies = array(
                'JP' => 'JPY',
                'US' => 'USD',
                'CI' => 'CNY',
                'CA' => 'CAD',
                'GB' => 'GBP',
            );

            $currency = (isset($currencies[$country->getmpac_country_code()])) ? $currencies[$country->getmpac_country_code()] : 'EUR';

        } else {
            throw new Exception('Current country is not defined in '.__METHOD__);
        }
        
        return $currency;
    }

    /**
     * Get Configured Currency For Current Country
     *
     * @return mixed
     * @throws Exception
     */
    public function getConfiguredCurrencyForCurrentCountry(){

        $currency = Mage::registry('mp_country')->getParam('currency');
        if ($currency == "") {
            throw new Exception($this->__('Currency attribute not set in Sales > MarketPlace > Configuration > <a href="'.Mage::Helper('adminhtml')->getUrl('adminhtml/Marketplace_Configuration', array()).'">Accounts</a>'), 15);
        }

        return $currency;

    }

}
