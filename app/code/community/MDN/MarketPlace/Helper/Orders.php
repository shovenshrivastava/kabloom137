<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_MarketPlace
 * @version 2.1
 * @todo : check that orders are ok (check magento reports)
 */
abstract class MDN_MarketPlace_Helper_Orders extends Mage_Core_Helper_Abstract
{

    const kOrderAccepted = "Accepted";

    /**
     * Import orders manually from exported file
     *
     * @param string $path
     * @param string $file
     * @return string $debug
     *
     */
    public function importOrdersFromUploadedFile($path, $file)
    {
        $filePath = $path . $file;
        $lines = file($filePath);
        

        $ordersTab = $this->buildOrdersTab($lines);
       // print_r($ordersTab); die;
        return $this->importMarketPlaceOrders($ordersTab);
    }

    /**
     * Import orders from marketplace
     *
     * @param array $ordersTab
     * @return string
     */
    public function importMarketPlaceOrders($ordersTab)
    {
        $debug = "";
        $nbImported = 0;
        $nbSkipped = 0;

        $successOrders = array();
        $errorOrders = array();
        $skippedOrders = array();
        $errorMessages = array();
        
        foreach ($ordersTab as $order) {

            try {

                if ($this->orderAlreadyImported($order['mpOrderId'])) {
                    $nbSkipped += 1;
                    $skippedOrders[] = $order['mpOrderId'];
                }else{

                    $new_order = $this->importOrder($order);

                    if ($new_order != null) {
                        $nbImported += 1;
                        $successOrders[] = $order['mpOrderId'];
                    } else {
                        $nbSkipped += 1;
                        $errorOrders[] = $order['mpOrderId'];
                    }

                }

            } catch (Exception $ex) {
                $nbSkipped += 1;
                $errorOrders[] = $order['mpOrderId'];
                $errorMessages[$order['mpOrderId']] = $ex->getMessage();
            }
        }

        $this->updateOrders($successOrders, $this->getOrderStatus(self::kOrderAccepted));
        Mage::Helper('MarketPlace/Log')->setOrderImportationLog($successOrders, $skippedOrders, $errorOrders, $errorMessages);

        $debug .= Mage::Helper('MarketPlace')->__('%s lines to process', count($ordersTab));
        $debug .= '<br/>';
        $debug .= Mage::Helper('MarketPlace')->__('%s order(s) imported', $nbImported);
        $debug .= '<br/>';
        $debug .= Mage::Helper('MarketPlace')->__('%s order(s) skipped (already exists)', $nbSkipped);

        return $debug;
    }

    /**
     * Update orders
     *
     * @param array $orders
     * @param string $status
     * @return int 0
     */
    protected function updateOrders($orders, $status)
    {
        // to implement in subclass
        return 0;
    }

    /**
     * Get order status
     *
     * @param string $status
     * @return string
     */
    protected function getOrderStatus($status)
    {
        return '';
    }

    /**
     * Order importation
     *
     * @param array $order
     * @throws Exception
     * @return order
     */
     
      /*
     * 
     * lets get some */

protected function tempvalue($postCode_from,$postCode_to,$NDate,$addressCity_to) {
	echo $tmp_pickup_date = str_replace("-", "", $NDate);
     //echo $postCode_from."g".$postCode_to."s".$delivery_date; //die;
	
    //$tmp_array = explode("-", $delivery_date);
    //$tmp_delivery_date = array();
    //$tmp_delivery_date['year'] = $tmp_array[0];
    //$tmp_delivery_date['month'] = $tmp_array[1];
    //$tmp_delivery_date['day'] = $tmp_array[2];

    //// predict the tmp_pickup_date, if the delivery date is away from 2 days - 
    ////u should subtract 2 days, if the delivery date is away from 1 day - u should subtract 1 day,
    ////if the delivery date is today  then you can make delivery date itself as tmp_pickup_date bcoz in 
    ////bcoz the timeintransit api will consider pickup date as today itself automatically
    //$currentdate = strtotime(date("Y-m-d"));
    //$date = strtotime($delivery_date);
    //$datediff = $date - $currentdate;
    //$differance = floor($datediff / (60 * 60 * 24));
    //if ($differance == 0) {
        //$tmp_pickup_date = str_replace("-", "", $delivery_date);
    //} elseif ($differance == 1) {
        //$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
    //} elseif ($differance > 1) {
        //$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 2 days')));
    //} else {
        //$tmp_pickup_date = str_replace("-", "", $delivery_date);
    //}

    $tmp_pickup_datetime['year'] = substr($tmp_pickup_date, 0, 4);
    $tmp_pickup_datetime['month'] = substr($tmp_pickup_date, 4, 2);
    $tmp_pickup_datetime['day'] = substr($tmp_pickup_date, 6, 2);
    $tmp_pickup_datetime['hrs'] = '17'; // assumption as customer is not entering delivery time details
    $tmp_pickup_datetime['mins'] = '59'; // assumption as customer is not entering delivery time details
    $tmp_pickup_datetime['secs'] = '59'; // assumption as customer is not entering delivery time details
    //$address = $order->getShippingAddress();
     $pudate = strtotime($NDate);
	 //echo $orderDeliveryDay = date('l', $pudate); //die;
		$thursdel = date('l', $NDate);
		if($thursdel=="Thursday"){
        echo $pudate = strtotime("+1 day", $pudate);
	}else{
		 echo $pudate = strtotime("+2 day", $pudate);
		
		}
         //$NDate = date('Y-m-d', $pudate);
	
   // $DelDate = strtotime($pudate);
	 echo $orderDeliveryDay = date('l', $pudate); 
	 
   echo $delivery_date_day = $orderDeliveryDay;//$tmp_delivery_date['day'];
//die; 
      //  $postCode_from = '08003'; 
	
    $country_from = 'US'; 
  //  $postCode_to = '55447'; 
	
	 $country_to ='US'; 
	 $addressCity_to = $addressCity_to; 

    // 2 days = 48 hours = 172800 seconds
    $timeTransit_response = $this->timeInTransit($tmp_pickup_date, $postCode_to, $country_to, $postCode_from, $country_from, $delivery_date_day, $shipping_amount,$addressCity_to); //die;
	//echo "tt***";
    //print_r($timeTransit_response);die;
   $delDay = substr($delivery_date_day,0,3);
  echo  $delDay = strtoupper($delDay);
    $is_shortlisted_service_method = false;
    //  if (isset($shipping_amount)) { // if condition for free shipping
   echo $ups_end_method = end($timeTransit_response); print_r($ups_end_method);//die;

    if ($ups_end_method->Service->Code == "GND") {// first check with ground method with pickup date,delvery time diff as <=48 hrs
        if ($ups_end_method->EstimatedArrival->BusinessDaysInTransit <= 2) {
            //check if transit days is <= 48 hrs
            $gnd_arrival_datetime = array();
            $gnd_pickup_datetime_scheduled = array();
           // echo "this is ground"; die;

            $gnd_pickup_date = $ups_end_method->EstimatedArrival->Pickup->Date;
            $gnd_pickup_time = $ups_end_method->EstimatedArrival->Pickup->Time;
            $gnd_pickup_datetime_scheduled['year'] = substr($gnd_pickup_date, 0, 4);
            $gnd_pickup_datetime_scheduled['month'] = substr($gnd_pickup_date, 4, 2);
            $gnd_pickup_datetime_scheduled['day'] = substr($gnd_pickup_date, 6, 2);
            $gnd_pickup_datetime_scheduled['hrs'] = substr($gnd_pickup_time, 0, 2);
            $gnd_pickup_datetime_scheduled['mins'] = substr($gnd_pickup_time, 2, 2);
            $gnd_pickup_datetime_scheduled['secs'] = substr($gnd_pickup_time, 4, 2);

            $gnd_arrival_date = $ups_end_method->EstimatedArrival->Arrival->Date;
            $gnd_arrival_time = $ups_end_method->EstimatedArrival->Arrival->Time;
            $gnd_arrival_datetime['year'] = substr($gnd_arrival_date, 0, 4);
            $gnd_arrival_datetime['month'] = substr($gnd_arrival_date, 4, 2);
            $gnd_arrival_datetime['day'] = substr($gnd_arrival_date, 6, 2);
            $gnd_arrival_datetime['hrs'] = substr($gnd_arrival_time, 0, 2);
            $gnd_arrival_datetime['mins'] = substr($gnd_arrival_time, 2, 2);
            $gnd_arrival_datetime['secs'] = substr($gnd_arrival_time, 4, 2);
            $gnd_time_diff = strtotime($gnd_arrival_datetime['year'] . '-' . $gnd_arrival_datetime['month'] . '-' . $gnd_arrival_datetime['day'] . ' ' . $gnd_arrival_datetime['hrs'] . ':' . $gnd_arrival_datetime['mins'] . ':' . $gnd_arrival_datetime['secs']) - strtotime($tmp_pickup_datetime['year'] . '-' . $tmp_pickup_datetime['month'] . '-' . $tmp_pickup_datetime['day'] . ' ' . $tmp_pickup_datetime['hrs'] . ':' . $tmp_pickup_datetime['mins'] . ':' . $tmp_pickup_datetime['secs']); // time diff bw tmp pickup and ups grnd delivery
           // print_r($gnd_arrival_datetime);die;

            if ($gnd_time_diff < 221000) {
		
               // if ($delDay == $ups_end_method->DayOfWeek) { // check whether ups delivery date matches w/ customer delivery date
				
                    $shortlisted_ups_service_code = $ups_end_method->Service->Code;
                    $shortlisted_ups_service_desc = $ups_end_method->Service->Description;
                    $shortlisted_ups_service_pickup_datetime = $gnd_pickup_datetime_scheduled;
                    $shortlisted_ups_service_delivery_date = $gnd_arrival_datetime['year'] . '-' . $gnd_arrival_datetime['month'] . '-' . $gnd_arrival_datetime['day'];
                    

                    array_pop($timeTransit_response); 
                 
                    $is_shortlisted_service_method = true;
               //} 					
            } 
        } 
    } 

    if (!$is_shortlisted_service_method) { // execute foreach if service is not shortlisted
        $tmp_time_diff = 0;
        //echo "this is not ground"; die;
        foreach ($timeTransit_response as $each_timeTransit_response) {
            if ($each_timeTransit_response->EstimatedArrival->BusinessDaysInTransit <= 2) {
                //check if transit days is <= 48 hrs
                $air_arrival_datetime = array();
                $air_pickup_datetime_scheduled = array();

                $air_pickup_date = $each_timeTransit_response->EstimatedArrival->Pickup->Date;
                $air_pickup_time = $each_timeTransit_response->EstimatedArrival->Pickup->Time;
                $air_pickup_datetime_scheduled['year'] = substr($air_pickup_date, 0, 4);
                $air_pickup_datetime_scheduled['month'] = substr($air_pickup_date, 4, 2);
                $air_pickup_datetime_scheduled['day'] = substr($air_pickup_date, 6, 2);
                $air_pickup_datetime_scheduled['hrs'] = substr($air_pickup_time, 0, 2);
                $air_pickup_datetime_scheduled['mins'] = substr($air_pickup_time, 2, 2);
                $air_pickup_datetime_scheduled['secs'] = substr($air_pickup_time, 4, 2);

                $air_arrival_date = $each_timeTransit_response->EstimatedArrival->Arrival->Date;
                $air_arrival_time = $each_timeTransit_response->EstimatedArrival->Arrival->Time;
                $air_arrival_datetime['year'] = substr($air_arrival_date, 0, 4);
                $air_arrival_datetime['month'] = substr($air_arrival_date, 4, 2);
                $air_arrival_datetime['day'] = substr($air_arrival_date, 6, 2);
                $air_arrival_datetime['hrs'] = substr($air_arrival_time, 0, 2);
                $air_arrival_datetime['mins'] = substr($air_arrival_time, 2, 2);
                $air_arrival_datetime['secs'] = substr($air_arrival_time, 4, 2);
					
                $air_time_diff = strtotime($air_arrival_datetime['year'] . '-' . $air_arrival_datetime['month'] . '-' . $air_arrival_datetime['day'] . ' ' . $air_arrival_datetime['hrs'] . ':' . $air_arrival_datetime['mins'] . ':' . $air_arrival_datetime['secs']) - strtotime($tmp_pickup_datetime['year'] . '-' . $tmp_pickup_datetime['month'] . '-' . $tmp_pickup_datetime['day'] . ' ' . $tmp_pickup_datetime['hrs'] . ':' . $tmp_pickup_datetime['mins'] . ':' . $tmp_pickup_datetime['secs']); // time diff bw tmp pickup and ups air delivery

                if ($air_time_diff < 221000) {
                   //if ($delDay == $ups_end_method->DayOfWeek) { // check whether ups delivery date matches w/ customer delivery date
                       
                        if ($air_time_diff > $tmp_time_diff) {
                            $tmp_time_diff = $air_time_diff;

                           echo  $shortlisted_ups_service_code = $each_timeTransit_response->Service->Code;
                            $shortlisted_ups_service_desc = $each_timeTransit_response->Service->Description;
                            $shortlisted_ups_service_pickup_datetime = $air_pickup_datetime_scheduled;
                            $shortlisted_ups_service_delivery_date = $air_arrival_datetime['year'] . '-' . $air_arrival_datetime['month'] . '-' . $air_arrival_datetime['day'];

                            $is_shortlisted_service_method = true;
                        } 
                    //} 
                } 
            } 
        } 
    } 
echo $shortlisted_ups_service_desc;

   echo $pickupDate = $shortlisted_ups_service_pickup_datetime['year'] . '-' . $shortlisted_ups_service_pickup_datetime['month'] . '-' . $shortlisted_ups_service_pickup_datetime['day']; // '2015-02-23 11:11:11' format
    return array('pickup_date'=>$pickupDate,'pickup_method'=>$shortlisted_ups_service_desc, 'delivery_date'=>$shortlisted_ups_service_delivery_date);

}
   
protected function timeInTransit($tmp_pickup_date, $postCode_to, $country_to, $postCode_from, $country_from, $delivery_date_day, $shipping_amount,$addressCity_to) {
	
	//echo $tmp_pickup_date; echo $postCode_to; echo $country_to;  echo $postCode_from;
    //Configuration
    $access = Mage::getStoreConfig('upslabel/credentials/accesslicensenumber'); //"8CCFD3D445A6F805";
    $userid = Mage::getStoreConfig('upslabel/credentials/userid'); //"newKaBloom";
    $passwd = Mage::getStoreConfig('upslabel/credentials/password'); //"Rosegarden123";
 $wsdl = Mage::getModuleDir('etc', 'Mage_Usa') . DS . 'wsdl' . DS . 'Ups' . DS . 'TNTWS.wsdl';
    $operation = "ProcessTimeInTransit";
    $endpointurl = 'https://wwwcie.ups.com/webservices/TimeInTransit';
    $outputFileName = "XOLTResult.xml";
    try {

        $mode = array
            (
            'soap_version' => 'SOAP_1_1', // use soap 1.1 client
            'trace' => 1
        );

        // initialize soap client
        $client = new SoapClient($wsdl, $mode);

        //set endpoint url
        $client->__setLocation($endpointurl);
        //create soap header
        $usernameToken['Username'] = $userid;
        $usernameToken['Password'] = $passwd;
        $serviceAccessLicense['AccessLicenseNumber'] = $access;
        $upss['UsernameToken'] = $usernameToken;
        $upss['ServiceAccessToken'] = $serviceAccessLicense;

        $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', $upss);
        $client->__setSoapHeaders($header);

        //create soap request
        $requestoption['RequestOption'] = 'TNT';
        $request['Request'] = $requestoption;

        $addressFrom['City'] = '';
        $addressFrom['CountryCode'] = $country_from; //'US';
        $addressFrom['PostalCode'] = $postCode_from; //'02446';
        $addressFrom['StateProvinceCode'] = '';
        $shipFrom['Address'] = $addressFrom;
        $request['ShipFrom'] = $shipFrom;
        $addressTo['City'] = $addressCity_to;
        $addressTo['CountryCode'] = $country_to; //'US';
        $addressTo['PostalCode'] = $postCode_to; //'55447';
        $addressTo['StateProvinceCode'] = '';
        $shipTo['Address'] = $addressTo;
        $request['ShipTo'] = $shipTo;

        $pickup['Date'] = $tmp_pickup_date; //'20150205'; //Ymd format
        $request['Pickup'] = $pickup;
        $unitOfMeasurement['Code'] = 'KGS';
        $unitOfMeasurement['Description'] = 'Kilograms';
        $shipmentWeight['UnitOfMeasurement'] = $unitOfMeasurement;
        $shipmentWeight['Weight'] = '2';
        $request['ShipmentWeight'] = $shipmentWeight;

        $request['TotalPackagesInShipment'] = '1';
        $invoiceLineTotal['CurrencyCode'] = 'CAD';
        $invoiceLineTotal['MonetaryValue'] = '10';
        $request['InvoiceLineTotal'] = $invoiceLineTotal;
        $request['MaximumListSize'] = '1';
        print_r($request); 

        $resp = $client->__soapCall($operation, array($request));

        $fw = fopen($outputFileName, 'w');
       //var_dump(get_object_vars($resp));
        $tmp_array = $resp->TransitResponse->ServiceSummary; //die;
		print_r($resp);	
        if (isset($shipping_amount)) {

            $tocheck_shortest_routes = $tmp_array;
            // can't explain, test b/w 02446 - 02482 u will understand to foreach loop
            $not_found = false;
            $not_found_count = 0;
            foreach ($tocheck_shortest_routes as $each_timeTransit_response) {
                $service_arrival_date = $each_timeTransit_response->EstimatedArrival->Arrival->Date;
                $air_arrival_datetime['day'] = substr($service_arrival_date, 6, 2);
                if ($delivery_date_day == $air_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer 		delivery date
                    $not_found = false;
                } else {
                    $not_found = true;
                    $not_found_count += 1;
                }
            }
            if ($not_found && (count($tocheck_shortest_routes) == $not_found_count)) {

                $tmp_pickup_datetime_year = substr($tmp_pickup_date, 0, 4);
                $tmp_pickup_datetime_month = substr($tmp_pickup_date, 4, 2);
                $tmp_pickup_datetime_day = substr($tmp_pickup_date, 6, 2);
                $tmp_pickup_datetime = $tmp_pickup_datetime_year . '-' . $tmp_pickup_datetime_month . '-' . $tmp_pickup_datetime_day;
                $tmp_pickup_date_new = str_replace("-", "", date('Y-m-d', strtotime($tmp_pickup_datetime . ' + 1 days')));

                $pickup['Date'] = $tmp_pickup_date_new; //'20150205'; //Ymd format
                $request['Pickup'] = $pickup;

                $resp = $client->__soapCall($operation, array($request));
                $tmp_array = $resp->TransitResponse->ServiceSummary; //die;
            }
        }
        //print_r($tmp_array); 
        return $tmp_array;
    } catch (Exception $e) {
        //print_r($e);
    }
} 
   // getting product sku based on product id
 protected function getSKU($prod_id){
	 
	 $_product = Mage::getSingleton('catalog/product')->load($prod_id);
	 return $_product->getSku();
	 
	 }
 protected  function syncSimpleInventory($product_id,$qty_ordered,$warehouse_id)
{

    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');
    $writeConnection = $resource->getConnection('core_write');

    echo $query_qty_select_warehouse = "select `quantity_in_stock` from `advancedinventory_stock` where product_id =" . $product_id . " and place_id =" . $warehouse_id;
    echo $query_qty_select_catalog_inventory = "select `qty` from `cataloginventory_stock_item` where product_id =" . $product_id;

    echo  $existing_qty_warehouse = $readConnection->fetchOne($query_qty_select_warehouse);
    echo $existing_qty_catalog = $readConnection->fetchOne($query_qty_select_catalog_inventory);
	
    $updated_qty_warehouse = $existing_qty_warehouse-$qty_ordered;
    $updated_qty_catalog = $existing_qty_catalog-$qty_ordered;
    
    echo $query_qty_update = "UPDATE `advancedinventory_stock` SET `quantity_in_stock` = '" . $updated_qty_warehouse . "' WHERE  place_id = " . $warehouse_id . ' and ' . '`product_id` =' . $product_id;
   
    
    echo $query_catalog_qty_update = "UPDATE `cataloginventory_stock_item` SET `qty` = '" . $updated_qty_catalog . "' WHERE `product_id` =" . $product_id;
     
    echo $query_targetqty_update = "UPDATE `advancedinventory_stock` SET `quantity_in_stock` = '" . $updated_qty_warehouse . "' WHERE  `place_id` = 3  and `product_id`= " .$product_id; 
     $writeConnection->query($query_qty_update); 
     $writeConnection->query($query_targetqty_update); 
    $writeConnection->query($query_catalog_qty_update); 
   // echo "all updated success";die;
}
     
     
    protected function importOrder($order)
    {
		/* Format which we recieve from marketplace 
		 * 
		 * Array
(
    [mpOrderId] => 113-9105892-5691451
    [email] => j0nvkr0p60p06g2@marketplace.amazon.com
    [marketplace] => amazon
    [phone] => 847-584-2225
    [firstname] => John Nordstrom
    [lastname] => 
    [date] => 2015-12-03T09:39:38-08:00
    [currency] => USD
    [shipping_excl_tax] => 9.99
    [shipping_tax] => 0
    [shipping_incl_tax] => 9.99
    [shipping_adress] => Array
        (
            [firstname] => Central Dupage Hospital
            [lastname] => 
            [zipCode] => 60190-1295
            [country] => US
            [state] => IL
            [city] => WINFIELD
            [comments] => 
            [company] => 
            [email] => j0nvkr0p60p06g2@marketplace.amazon.com
            [phone] => (630) 933-4234
            [street] => Array
                (
                    [adress1] => JUDITH MORRISON -=-Room 310
                    [adress2] => 25 North Winfield Road
                    [adress3] => 
                )

        )

    [billing_adress] => Array
        (
            [firstname] => John Nordstrom
            [lastname] => 
            [zipCode] => 60190-1295
            [country] => US
            [state] => IL
            [city] => WINFIELD
            [comments] => 
            [company] => 
            [email] => j0nvkr0p60p06g2@marketplace.amazon.com
            [phone] => 847-584-2225
            [street] => Array
                (
                    [adress1] => JUDITH MORRISON -=-Room 310
                    [adress2] => 25 North Winfield Road
                    [adress3] => 
                )

        )

    [products] => Array
        (
            [0] => Array
                (
                    [id] => 3973
                    [mp_item_id] => 64232150475826
                    [price_excl_tax] => 19.99
                    [price_tax] => 0
                    [price_incl_tax] => 19.99
                    [quantity] => 1
                )

        )

)
*/
//print_r($order); die;
        $billingState = Mage::Helper('MarketPlace/Order_Address')->getStateCode($order['billing_adress']['state']);
        $regionModel = Mage::getModel('directory/region')->loadByCode($billingState, $order['billing_adress']['country']);
        $billingRegionId = $regionModel->getId();

        $shippingState = Mage::Helper('MarketPlace/Order_Address')->getStateCode($order['shipping_adress']['state']);
        $regionModel = Mage::getModel('directory/region')->loadByCode($shippingState, $order['shipping_adress']['country']);
        $shippingRegionId = $regionModel->getId();

        $currency = Mage::Helper('MarketPlace/Currency')->getConfiguredCurrencyForCurrentCountry();
        $payment_method = Mage::Helper('MarketPlace/PaymentMethod')->getName();
        $customer = Mage::Helper('MarketPlace/Customer')->createOrReturnFromOrder($order);

        $new_order = Mage::getModel('sales/order');
        $new_order->reset();
        $new_order->setcustomer_id($customer->getId());
        $new_order->setCustomerGroupId($customer->getGroupId());
        $new_order->setCustomerFirstname($customer->getFirstname());
        $new_order->setCustomerLastname($customer->getLastname());
        $new_order->setCustomerIsGuest(0);
        $new_order->setCustomerEmail($customer->getemail());
        $new_order->setcreated_at(now());

        $new_order->setStore_id(Mage::registry('mp_country')->getParam('store_id'));
        $new_order->setorder_currency_code($currency);
        $new_order->setbase_currency_code($currency);
        $new_order->setstore_currency_code($currency);
        $new_order->setglobal_currency_code($currency);
        $new_order->setstore_to_base_rate(1);
        $new_order->setShippingAddress(Mage::Helper('MarketPlace/Order_Address')->getShippingAddress($new_order, $order, $shippingRegionId, $customer));
        $new_order->setBillingAddress(Mage::Helper('MarketPlace/Order_Address')->getBillingAddress($new_order, $order, $billingRegionId, $customer));

        $payment = Mage::getModel('sales/order_payment');
        $payment->setMethod($payment_method);
        $new_order->setPayment($payment);

        $shippingTaxAmount = $order['shipping_tax'];
        $shippingAmount = $order['shipping_excl_tax'];
        $shipping_method = Mage::registry('mp_country')->getParam('default_shipment_method');
        $shipping_method_title = $this->getShippingMethodTitle($shipping_method);

        $new_order->setshipping_method($shipping_method);
        $new_order->setshipping_description($shipping_method_title);
        $new_order->setshipping_amount((double) $shippingAmount);
        $new_order->setbase_shipping_amount((double) $shippingAmount);
        $new_order->setshipping_tax_amount((double) $shippingTaxAmount);
        $new_order->setbase_shipping_tax_amount((double) $shippingTaxAmount);
        $shippingInclTax = $shippingAmount + $shippingTaxAmount;
        $new_order->setshipping_incl_tax((double) $shippingInclTax);
        $new_order->setbase_shipping_incl_tax((double) $shippingInclTax);
        
        $new_order->setbase_to_global_rate(1);
        $new_order->setbase_to_order_rate(1);
        $new_order->setstore_to_order_rate(1);
        $new_order->setis_virtual(0);
        $new_order->setbase_discount_amount(0);
        $new_order->setdiscount_amount(0);
        $new_order->setbase_shipping_discount_amount(0);
        $new_order->setshipping_discount_amount(0);
        $new_order->sethidden_tax_amount(0);
        $new_order->setbase_hidden_tax_amount(0);
        $new_order->setshipping_hidden_tax_amount(0);
        $new_order->setbase_shipping_hidden_tax_amount(0);

        $new_order
                ->setGrandTotal($shippingAmount + $shippingTaxAmount)
                ->setBaseGrandTotal($shippingAmount + $shippingTaxAmount)
                ->setTaxAmount($shippingTaxAmount)
                ->setBaseTaxAmount($shippingTaxAmount);
                

        $this->addItems($new_order, $order);
        
        //save order
        $new_order->setstate('new');
        $partnerProducts=$order['products'];
        $wareHouseId = 3;
        $wareHouse = "AFRH";
        $businessType = "Marketplace";
      
       
    $productId = $order['products'][0]['id'];//Mage::getModel("catalog/product")->getIdBySku($productSKU);
    $qty = $order['products'][0]['quantity'];;//$partnerProducts[0]->quantity;
     $productSKU = $this->getSKU($productId);
    // print_r($productSKU); die;
    $json =  '{' . '"' . $productId . '":' . '{';
    for($i = 1; $i < 12; $i++) {
    $wareHouseQty = ($wareHouseId==$i)?$qty:0;
    $json = $json . '"'. $i . '":' . $wareHouseQty . ",";
    }
    $wareHouseQty = ($wareHouseId==$i)?$qty:0;
    $json = $json . '"'. $i . '":' . $wareHouseQty . '}}';
	$new_order->setProdSku($productSKU);
	
    $new_order->setAssignationStock($json);
    $new_order->setAssignationWarehouse($wareHouseId);
    $collection = Mage::getModel('pointofsale/pointofsale')->load($wareHouseId);
	echo $postCode_from = $collection->getPostalCode();
	echo $postCode_to = Mage::Helper('MarketPlace/Order_Address')->getShippingAddress($new_order, $order, $shippingRegionId, $customer)->getPostcode();
	echo $addressCity_to = Mage::Helper('MarketPlace/Order_Address')->getShippingAddress($new_order, $order, $shippingRegionId, $customer)->getCity();
	$gift_recipient = Mage::Helper('MarketPlace/Order_Address')->getShippingAddress($new_order, $order, $shippingRegionId, $customer)->getFirstname().' '.Mage::Helper('MarketPlace/Order_Address')->getShippingAddress($new_order, $order, $shippingRegionId, $customer)->getLastname();
	$giftText = $order['gift-message-text'];
	$gift_sender = $customer->getFirstname().' '.$customer->getLastname();
	
	/* delivery date logic 
	 * mon - thu day 2 days retention
	 * thu - friday delivery
	 * fri- sun - tuesday delivery 	 
	 * *
	 
	  $deliverydate = date('Y-m-d');
	 $deliverydate = strtotime($deliverydate);
	 $orderDeliveryDay = date('l', $deliverydate); //die;
	if($orderDeliveryDay =="Friday"){		
        $deliverydate = strtotime("+3 day", $deliverydate);
         $NewDate = date('Y-m-d', $deliverydate);
		}
	if($orderDeliveryDay =="Saturday"){		
      $deliverydate = strtotime("+2 day", $deliverydate);
       $NewDate = date('Y-m-d', $deliverydate);
		}
	if($orderDeliveryDay =="Sunday"){
		 $deliverydate = strtotime("+2 day", $deliverydate);
          $NewDate = date('Y-m-d', $deliverydate);
		}if($orderDeliveryDay =="Monday" || $orderDeliveryDay =="Tuesday" || $orderDeliveryDay =="Wednesday" || $orderDeliveryDay =="Thursday" ){
			$deliverydate = strtotime("+2 day", $deliverydate);
          $NewDate = date('Y-m-d', $deliverydate);			
			}
		*/	
			
//date_default_timezone_set('America/New_York');
		 
	  $pudate = date('Y-m-d');//"2016-02-21";
	 $pudate = strtotime($pudate);
	 
	 echo $orderDeliveryDay = date('l', $pudate); //die;
	 //if($orderDeliveryDay =="Thursday"){		
        //echo $pudate = strtotime("+4 day", $pudate);
         //$NDate = date('Y-m-d', $pudate);
		//}
	if($orderDeliveryDay =="Friday"){		
        echo $pudate = strtotime("+3 day", $pudate);
         $NDate = date('Y-m-d', $pudate);
		}
	if($orderDeliveryDay =="Saturday"){		
      echo $pudate = strtotime("+2 day", $pudate);
       $NDate = date('Y-m-d', $pudate);
		}
	if($orderDeliveryDay =="Sunday"){
		 echo $pudate = strtotime("+1 day", $pudate);
          $NDate = date('Y-m-d', $pudate);
	  }
	  // 
		if($orderDeliveryDay =="Monday" || $orderDeliveryDay =="Tuesday" || $orderDeliveryDay =="Wednesday" || $orderDeliveryDay =="Thursday"){
			echo $orderDeliveryDay;
			$pudate;// = strtotime($pudate);
            $NDate = date('Y-m-d', $pudate);			
			}
			echo "this is pickup";
			echo $NDate; //die;

			 
		
	//echo $delivery_date = $NewDate; //"2016-02-13";//die;
    $pick_info = $this->tempvalue($postCode_from,$postCode_to,$NDate,$addressCity_to);
   
   if($pick_info['pickup_date']=="--"){
	   
	   $pick_info['pickup_date'] =  date('Y-m-d');
	   
	   }else{
		   $pick_info['pickup_date'] = $pick_info['pickup_date'];
		   
		   }
    $new_order->setPickupDate($pick_info['pickup_date']);
    $new_order->setPickupMethod($pick_info['pickup_method']);

    //print_r($pick_info); die;
    $new_order->setDeliveringDate($pick_info['delivery_date']);

	$new_order->setPartnerWarehouse($wareHouse);
    $new_order->setTradingPartner($businessType);
    $new_order->setPartnerOrderId($order['mpOrderId']);
	$new_order->setWarehouse($wareHouse);
	
	// gift message 
	
    $giftMessage = Mage::getModel('giftmessage/message');
    $giftMessage->setCustomerId($customer->getId());
    $giftMessage->setSender($gift_sender);
    $giftMessage->setRecipient($gift_recipient);
    $giftMessage->setMessage($giftText);
    $giftObj = $giftMessage->save();
    $new_order->setGiftMessageId($giftObj->getId());
	
//print_r($new_order->getData()); die;
        $new_order->addStatusToHistory(
                                    'processing',
                                    'Commande ' . $order['marketplace'] . ' #' . $order['mpOrderId']
                                );
        $new_order->setcreated_at(date("Y-m-d H:i:s"), Mage::getModel('Core/Date')->timestamp());
        $new_order->setupdated_at(date("Y-m-d H:i:s"), Mage::getModel('Core/Date')->timestamp());
        $new_order->setfrom_site($order['marketplace']);
        $new_order->setmarketplace_order_id($order['mpOrderId']);
        $new_order->setinvoice_comments('Commande ' . $order['marketplace'] . ' #' . $order['mpOrderId']);
        $new_order->save();

        //change status
        $new_order->setState(Mage_Sales_Model_Order::STATE_NEW, Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_order_status())->save();

        Mage::helper('MarketPlace/Invoice')->createInvoice($new_order);
        
        $new_order->save();

        // dispatch event
        Mage::dispatchEvent('sales_marketplace_order_after_import', array('order' => $new_order));
        // print_r($new_order); die;
        return $new_order;
        
    }

    /**
     * Add Items
     *
     * @param Mage_Sales_Model_Order $new_order
     * @param array $order
     * @throws Exception
     */
    protected function addItems($new_order, $order){

        $_orderWeight = 0;
        $_orderQtyOrdered = 0;

        foreach ($order['products'] as $item) {

            //add product
            $product = Mage::getModel('catalog/product')->load($item['id']);

            // check if product exists
            if (!$product->getId()) {
                throw new Exception('Sku '.$item['id'].' does not exist');
            }

            //set price and tax
            $price_excl_tax = $item['price_excl_tax'];
            $price_incl_tax = $item['price_incl_tax'];
            $tax = $item['price_tax'];
            $qty = (int)$item['quantity'];

            $_orderWeight += $product->getweight() * $qty;
            $_orderQtyOrdered += $qty;
            $wareHouseId = 3; // this is hardcoded for newjersey
 
            $NewOrderItem = $this->getNewOrderItem($product, $qty, $price_excl_tax, $price_incl_tax, $tax, $item);
            //$this->updateProductStock($item['id'], $item['quantity'],$wareHouseId);
            $new_order->addItem($NewOrderItem);
            $new_order
                ->setSubtotal($new_order->getSubtotal() + $price_excl_tax * $qty)
                ->setBaseSubtotal($new_order->getBaseSubtotal() + $price_excl_tax * $qty)
                ->setGrandTotal($new_order->getGrandTotal() + (($tax + $price_excl_tax) * $qty))
                ->setBaseGrandTotal($new_order->getBaseGrandTotal() + (($tax + $price_excl_tax) * $qty))
                ->setTaxAmount($new_order->getTaxAmount() + $tax * $qty)
                ->setBaseTaxAmount($new_order->getBaseTaxAmount() + $tax * $qty)
                ->setbase_subtotal_incl_tax($new_order->getbase_subtotal_incl_tax() + $price_incl_tax * $qty)
                ->setsubtotal_incl_tax($new_order->getsubtotal_incl_tax() + $price_incl_tax * $qty);
        }

        $new_order->setweight($_orderWeight);
        $new_order->settotal_qty_ordered($_orderQtyOrdered);

    }

    /**
     * Update product stock
     *
     * @param Mage_Catalog_Model_Product $product
     * @param int $qtyToSubstract
     */
    protected function updateProductStock($product_id, $qty_ordered,$warehouse_id){

    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');
    $writeConnection = $resource->getConnection('core_write');

    echo $query_qty_select_warehouse = "select `quantity_in_stock` from `advancedinventory_stock` where product_id =" . $product_id . " and place_id =" . $warehouse_id;
    echo $query_qty_select_catalog_inventory = "select `qty` from `cataloginventory_stock_item` where product_id =" . $product_id;

    echo  $existing_qty_warehouse = $readConnection->fetchOne($query_qty_select_warehouse);
    echo $existing_qty_catalog = $readConnection->fetchOne($query_qty_select_catalog_inventory);
	
    $updated_qty_warehouse = $existing_qty_warehouse-$qty_ordered;
    $updated_qty_catalog = $existing_qty_catalog-$qty_ordered;
    
    echo $query_qty_update = "UPDATE `advancedinventory_stock` SET `quantity_in_stock` = '" . $updated_qty_warehouse . "' WHERE  place_id = " . $warehouse_id . ' and ' . '`product_id` =' . $product_id;
   
    
    echo $query_catalog_qty_update = "UPDATE `cataloginventory_stock_item` SET `qty` = '" . $updated_qty_catalog . "' WHERE `product_id` =" . $product_id;
     
    echo $query_targetqty_update = "UPDATE `advancedinventory_stock` SET `quantity_in_stock` = '" . $updated_qty_warehouse . "' WHERE  `place_id` = 3  and `product_id`= " .$product_id; 
     $writeConnection->query($query_qty_update); 
     $writeConnection->query($query_targetqty_update); 
    $writeConnection->query($query_catalog_qty_update); 
   // echo "all updated success";die;
    /*if (Mage::getStoreConfig('cataloginventory/options/can_subtract') == 1) {

            $stock_item = Mage::getModel('cataloginventory/stock_item');
            $current_qty = $stock_item->loadByProduct($product)->getQty();
            $qty = $current_qty - $qtyToSubstract;

            $stock_item->setQty($qty)
                ->save();
        }*/

    }

    /**
     * Get New Order Item
     *
     * @param Mage_Catalog_Model_Product $product
     * @param int $qty
     * @param float $price_excl_tax
     * @param float $price_incl_tax
     * @param float $tax
     * @param array $item
     * @return Mage_Sales_Model_Order_Item mixed
     */
    protected function getNewOrderItem($product, $qty, $price_excl_tax, $price_incl_tax, $tax, $item){

        $taxTotal = $tax * $qty;
        $htTotal = $price_excl_tax * $qty;

        $taxRate = Mage::Helper('MarketPlace/Taxes')->getTaxRate($product);
        $NewOrderItem = Mage::getModel('sales/order_item')
            ->setProductId($product->getId())
            ->setSku($product->getSku())
            ->setName($product->getName())
            ->setWeight($product->getWeight())
            ->setTaxClassId($product->getTaxClassId())
            ->setBaseCost($product->getCost())
            ->setOriginalPrice($price_excl_tax)
            ->setbase_original_price($price_excl_tax)
            ->setIsQtyDecimal(0)
            ->setProduct($product)
            ->setPrice((double) $price_excl_tax)
            ->setBasePrice((double) $price_excl_tax)
            ->setprice_incl_tax((double) $price_incl_tax)
            ->setbase_price_incl_tax((double) $price_incl_tax)
            ->setbase_row_total_incl_tax((double) $price_incl_tax * $qty)
            ->setrow_total_incl_tax((double) $price_incl_tax * $qty)
            ->setQtyOrdered($item['quantity'])
            ->setmarketplace_item_id($item['mp_item_id'])
            ->setTaxAmount($taxTotal)
            ->setBaseTaxAmount($taxTotal)
            ->setTaxPercent($taxRate)
            ->setRowTotal($htTotal)
            ->setBaseRowTotal($htTotal)
            ->setRowWeight($product->getWeight() * $qty)
            ->setbase_tax_before_discount($taxTotal)
            ->settax_before_discount($taxTotal)
            ->setstore_id($this->getCurrentStoreID())
            ->setproduct_type($product->getTypeId());

        $NewOrderItem->setbase_weee_tax_applied_amount(0);
        $NewOrderItem->setbase_weee_tax_disposition(0);
        $NewOrderItem->setweee_tax_applied_amount(0);
        $NewOrderItem->setweee_tax_disposition(0);

        return $NewOrderItem;

    }
   

    /**
     * Return shipment method title
     *
     * @param string $shipping_method
     * @return string
     */
    public function getShippingMethodTitle($shipping_method)
    {
        return mage::helper('MarketPlace')->getShippingMethodTitle($shipping_method);
    }

    /**
     * Get current store id
     *
     * @return int
     */
    public function getCurrentStoreID()
    {
        return Mage::registry('mp_country')->getParam('store_id');
    }

    /**
     * Check if current order has not be imported yet
     *
     * @param string $marketplaceOrderId
     * @return boolean
     */
    protected function orderAlreadyImported($marketplaceOrderId)
    {
        return mage::helper('MarketPlace')->orderAlreadyImported($marketplaceOrderId);
    }

    /**
     * Get marketplace orders
     * (must be implemented in subclass )
     */
    abstract public function getMarketPlaceOrders();

    /**
     * Check file version
     * (must be implemented in subclass)
     *
     * @param array $lines
     */
    abstract public function checkFileVersion($lines);

    /**
     * Build order tab
     * (must be implemented in subclass )
     *
     * @param string $str
     */
    abstract public function buildOrdersTab($str);

    /**
     * Check that file is ok
     * (ust be implemented in subclass)
     *
     * @param array $lines
     */
    abstract public function isFileOk($lines);

    /**
     * Get marketPlace name
     * (must be implemented in subclass)
     */
    abstract public function getMarketPlaceName();
}
