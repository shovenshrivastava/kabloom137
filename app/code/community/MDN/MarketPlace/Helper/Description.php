<?php

/**
 * Class MDN_MarketPlace_Helper_Description
 *
 * @category  {category}
 * @package   MDN_MarketPlace
 * @author    Nicolas Mugnier <nicolas@boostmyshop.com>
 * @copyright 2012-2015 Copyright (c) Boost My Shop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      {link}
 */
class MDN_MarketPlace_Helper_Description extends Mage_Core_Helper_Abstract {

    /**
     * Get Value For Product
     *
     * @param Varien_Object $product
     * @return string $retour
     */
    public function getValueForProduct($product){

        $retour = '';

        $data = array(
            'text_description' => array(
                'heading' => 'additional_textheading',
                'content' => 'text_description'
            ),
            'product_advantages' => array(
                'heading' => 'advantages_heading',
                'content' => 'product_advantages'
            ),
            'product_accessories' => array(
                'heading' => 'accessories_heading',
                'content' => 'product_accessories'
            ),
            'product_characteristics' => array(
                'heading' => 'characteristics_heading',
                'content' => 'product_characteristics'
            ),
            'product_standards' => array(
                'heading' => 'productstandards_heading',
                'content' => 'product_standards'
            ),
            'adjustleft_text' => array(
                'heading' => 'adjustleft_heading',
                'content' => 'adjustleft_text'
            ),
            'adjustleft_text2' => array(
                'heading' => 'adjustleft_heading2',
                'content' => 'adjustleft_text2'
            ),
            'adjustright_text' => array(
                'heading' => 'adjustright_heading',
                'content' => 'adjustright_text'
            )
        );

        if ($product->getdescription() != '.') {
            $retour .= ($product->getdescription_heading() != "") ? '<h2>'.$product->getdescription_heading().'</h2>' : "";
            $retour .= $product->getdescription();
        } else {
            $retour .= ($product->getshortdescription_heading() != '') ? '<h2>'.$product->getshortdescription_heading().'</h2>' : "";
            $retour .= $product->getshort_description();
        }

        foreach ($data as $array) {
            $getHeading = 'get'.$array['heading'];
            $getContent = 'get'.$array['content'];
            $retour .= ($product->$getHeading() != '') ? '<h2>'.$product->$getHeading().'</h2>' : "";
            $retour .= ($product->$getContent() != '') ? $product->$getContent() : "";
        }

        return $retour;

    }

}