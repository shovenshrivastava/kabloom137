<?php

/**
 * Class MDN_MarketPlace_Helper_Order_Address
 *
 * @category  {category}
 * @package   MDN_MarketPlace
 * @author    Nicolas Mugnier <nicolas@boostmyshop.com>
 * @copyright 2012-2015 Copyright (c) Boost My Shop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      {link}
 */
class MDN_MarketPlace_Helper_Order_Address extends Mage_Core_Helper_Abstract {

    /**
     * Get Shipping Address
     *
     * @param Mage_Sales_Model_Order $newOrder
     * @param array $orderTab
     * @param int $shippingRegionId
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Sales_Model_Order_Address $shipping_address
     */
    public function getShippingAddress($newOrder, $orderTab, $shippingRegionId, $customer){

        $shipping_address = Mage::getModel('sales/order_address');
        $shipping_address->setOrder($newOrder);
        $shipping_address->setId(null);
        $shipping_address->setentity_type_id(12);

        $tShippingName = explode(' ', $orderTab['shipping_adress']['firstname']);
        $shippingFirstname = $tShippingName[0];
        $shippingLastname = '';
        for ($i = 1; $i < count($tShippingName); $i++) {
            $shippingLastname .= $tShippingName[$i] . ' ';
        }

        $shipping_address->setfirstname($shippingFirstname);
        $shipping_address->setlastname($shippingLastname);
        $shipping_address->setStreet($orderTab['shipping_adress']['street']);
        $shipping_address->setCity($orderTab['shipping_adress']['city']);
        $shipping_address->setPostcode($orderTab['shipping_adress']['zipCode']);
        $shipping_address->setcountry_id($orderTab['shipping_adress']['country']);
        $shipping_address->setregion_id($shippingRegionId);
        $shipping_address->setEmail($customer->getEmail());
        $shipping_address->setTelephone($orderTab['shipping_adress']['phone']);
        $shipping_address->setcomments($orderTab['shipping_adress']['comments']);
        $shipping_address->setcompany($this->getFieldAsEmptyIsMissing('company', $orderTab['shipping_adress']));
        $shipping_address->setbuilding($this->getFieldAsEmptyIsMissing('building', $orderTab['shipping_adress']));
        $shipping_address->setappartment($this->getFieldAsEmptyIsMissing('appartment', $orderTab['shipping_adress']));

        return $shipping_address;

    }

    /**
     * Get Billing Address
     *
     * @param Mage_Sales_Model_Order $newOrder
     * @param array $orderTab
     * @param int $billingRegionId
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Sales_Model_Order_Address $billing_address
     */
    public function getBillingAddress($newOrder, $orderTab, $billingRegionId, $customer){

        $billing_address = Mage::getModel('sales/order_address');
        $billing_address->setOrder($newOrder);
        $billing_address->setId(null);
        $billing_address->setentity_type_id(Mage::Helper('MarketPlace/FlatOrder')->getEntityTypeIdAddress());

        $tBillingName = explode(' ', $orderTab['billing_adress']['firstname']);
        $billingFirstname = $tBillingName[0];
        $billingLastname = '';
        for ($i = 1; $i < count($tBillingName); $i++) {
            $billingLastname .= $tBillingName[$i] . ' ';
        }

        $billing_address->setfirstname($billingFirstname);
        $billing_address->setlastname($billingLastname);
        $billing_address->setStreet($orderTab['billing_adress']['street']);
        $billing_address->setCity($orderTab['billing_adress']['city']);
        $billing_address->setPostcode($orderTab['billing_adress']['zipCode']);
        $billing_address->setcountry_id($orderTab['billing_adress']['country']);
        $billing_address->setregion_id($billingRegionId);
        $billing_address->setEmail($customer->getEmail());
        $billing_address->setTelephone($orderTab['billing_adress']['phone']);
        $billing_address->setcomments($orderTab['billing_adress']['comments']);
        $billing_address->setcompany($this->getFieldAsEmptyIsMissing('company', $orderTab['billing_adress']));
        $billing_address->setbuilding($this->getFieldAsEmptyIsMissing('building', $orderTab['billing_adress']));
        $billing_address->setappartment($this->getFieldAsEmptyIsMissing('appartment', $orderTab['billing_adress']));

        return $billing_address;

    }

    /**
     * Get Field As Empty Is Missing
     *
     * @param string $field
     * @param array $tab
     * @return string
     */
    private function getFieldAsEmptyIsMissing($field, $tab){

        return (array_key_exists($field, $tab)) ? $tab[$field] : '';

    }

    /**
     * Retrieve country code
     *
     * @param $text
     * @return string $stateCode
     */
    public function getStateCode($text)
    {
        $stateString = trim(strtolower($text));
        $tabStateCode = array('AL','AK','AS','AZ','AR','AF','AA','AC','AE','AM','AP','CA','CO','CT','DE','DC','FM','FL','GA','GU','HI','ID','IL','IN','IA','KS','KY','LA','ME','MH','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','MP','OH','OK','OR','PW','PA','PR','RI','SC','SD','TN','TX','UT','VT','VI','VA','WA','WV','WI','WY');
        $tabStateString = array('alabama','alaska','american samoa','arizona','arkansas','armed forces africa','armed forces americas','armed forces canada','armed forces europe','armed forces middle east','armed forces pacific','california','colorado','connecticut','delaware','district of columbia','federated states of micronesia','florida','georgia','guam','hawaii','idaho','illinois','indiana','iowa','kansas','kentucky','louisiana','maine','marshall islands','maryland','massachusetts','michigan','minnesota','mississippi','missouri','montana','nebraska','nevada','new hampshire','new jersey','new mexico','new york','north carolina','north dakota','northern mariana islands','ohio','oklahoma','oregon','palau','pennsylvania','puerto rico','rhode island','south carolina','south dakota','tennessee','texas','utah','vermont','virgin islands','virginia','washington','west virginia','wisconsin','wyoming');

        $stateCode = str_replace($tabStateString, $tabStateCode, $stateString);

        return $stateCode;
    }

}