<?php

/**
 * Class MDN_MarketPlace_Helper_Customer
 *
 * @category  {category}
 * @package   MDN_MarketPlace
 * @author    Nicolas Mugnier <nicolas@boostmyshop.com>
 * @copyright 2012-2015 Copyright (c) Boost My Shop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      {link}
 */
class MDN_MarketPlace_Helper_Customer extends Mage_Core_Helper_Abstract {

    /**
     * Create Or Return From Order
     *
     * @param Varien_Object $order
     * @return mixed
     */
    public function createOrReturnFromOrder($order){

        $storeId = Mage::registry('mp_country')->getParam('store_id');
        $webSiteId = Mage::getModel('core/store')->load($storeId)->getWebsiteId();

        $email = $order['email'];
        $fakeCustomer = Mage::getModel('customer/customer');
        $fakeCustomer->setWebsiteId($webSiteId);
        $tName = explode(' ', $order['firstname']);
        $firstname = $tName[0];
        $lastname = '';
        for ($i = 1; $i < count($tName); $i++) {
            $lastname .= $tName[$i] . ' ';
        }

        $customer = Mage::getModel('customer/customer')
            ->setWebsiteId($webSiteId)
            ->loadByEmail($email);
        if ($customer->getId()) {
            return $customer;
        }

        $customer = Mage::getModel('customer/customer');
        $customer->setWebsiteId($webSiteId);
        $customer->setstore_id($storeId);
        $customer->setFirstname($firstname);
        $customer->setLastname($lastname);
        $customer->setEmail($email);
        $customer_group_id = Mage::Helper('MarketPlace')->getCustomerGroupId(strtolower($order['marketplace']));
        $customer->setgroup_id($customer_group_id);
        $customer->save();

        return $customer;

    }

}