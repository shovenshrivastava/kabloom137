<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_MarketPlace
 * @version 2.1
 */
class MDN_MarketPlace_Helper_Shippingcost extends Mage_Core_Helper_Abstract
{

    const kDelayTnt = '1';
    const kDelayColissimo = '2';

    /**
     * Calculate shipping cost
     *
     * @param Varien_Object $product
     * @param boolean $addTax
     * @return float
     */
    public function calculateShippingCost($_product, $addTax, $carrierCode = null)
    {
        // must load product, if not, error when trying to setStockItem...
        $product = Mage::getModel('catalog/product')->load($_product->getentity_id());
        $country = Mage::registry('mp_country')->getmpac_country_code();

        $storeId = Mage::registry('mp_country')->getParam('store_id');
        $store = Mage::getModel('core/store')->load($storeId);
        $websiteId = $store->getwebsite_id();

        $carrierObject = $this->retrieveCarrierObjectFromCarrierCode($carrierCode);

        try {
            $request = Mage::getModel('shipping/rate_request');

            //force stock item load (ressource consumer but no choice regarding the way magento implements cost calculation)
            $product->setStockItem(mage::getModel('cataloginventory/stock_item')->loadByProduct($product));

            //set request item (build a quote)
            $quote = mage::getModel('sales/quote');
            $quoteItem = mage::getModel('sales/quote_item');
            $quoteItem->setQuote($quote);
            $quoteItem->setProduct($product);
            $quoteItem->setWeight($product->getweight());
            $quoteItem->setQty(1);
            $quote->addItem($quoteItem);

            //set request
            $request->setAllItems($quote->getAllItems());
            $request->setPackageValue($product->getFinalPrice());
            $request->setPackageValueWithDiscount($product->getFinalPrice());
            $request->setPackagePhysicalValue($product->getFinalPrice());
            $request->setDestCountryId($country);
            $request->setPackageWeight($product->getweight());
            $request->setPackageQty(1);
            $request->setFreeMethodWeight(0);
            $request->setFreeShipping(0);
            $request->setStoreId($storeId);
            $request->setWebsiteId($websiteId);

            $retour = $this->collectRates($carrierObject, $carrierCode, $request, $addTax);
        } catch (Exception $ex) {
            throw new Exception('Unable to collect shipping cost for product ' . $product->getName() . ':' . $ex->getMessage());
        }

        // dispatch event for updating shipping price
        $_product->setData('shipping_to_export', $retour);
        Mage::dispatchEvent('marketplace_before_export_shipping', array('product'=>$_product));
        $retour = $_product->getData('shipping_to_export');

        return $retour;
    }

    /**
     * Collect Rates
     *
     * @param Varien_Object $carrierObject
     * @param string $carrierCode
     * @param Varien_Object $request
     * @param boolean $addTax
     * @return null
     */
    private function collectRates($carrierObject, $carrierCode, $request, $addTax)
    {
        $rate = null;
        $rates = $carrierObject->collectRates($request);
        $cheaperRate = null;

        foreach ($rates->getAllRates() as $rate) {
            if ($rate->getcarrier().'_'.$rate->getmethod() == $carrierCode) {
                $cheaperRate = $rate;
            }
        }

        if ($cheaperRate === null && $rates) {
            $cheaperRate = $rates->getCheapestRate();
        }

        if ($cheaperRate !== null && !$cheaperRate->getError()) {
            $rate = $cheaperRate->getPrice();
            if ($addTax === true) {
                $tax = Mage::Helper('MarketPlace/Taxes')->getShippingTaxRate();
                $rate = $rate * (1 + $tax / 100);
            }
        }

        return $rate;
    }

    /**
     * Retrieve Carrier Object From Carrier Code
     *
     * @param string $carrierCode
     * @return Model
     */
    private function retrieveCarrierObjectFromCarrierCode($carrierCode)
    {
        if ($carrierCode === null) {
            $carrierCode = Mage::registry('mp_country')->getParam('default_shipment_method');
        }

        $code = explode("_", $carrierCode);

        return $this->getCarrierFromCode($code[0]);
    }

    /**
     * Return carrier model from carrier code
     *
     * @param $CarrierCode
     * @throws Exception
     * @internal param $string
     * @return Model
     */
    public function getCarrierFromCode($CarrierCode)
    {
        $config = Mage::getStoreConfig('carriers');
        foreach ($config as $code => $methodConfig) {
            if (Mage::getStoreConfigFlag('carriers/' . $code . '/active')) {
                if ($code == $CarrierCode) {
                    if (isset($methodConfig['model'])) {
                        $modelName = $methodConfig['model'];
                        $Model = Mage::getModel($modelName);
                        return $Model;
                    }
                }
            }
        }

        throw new Exception('Unable to find carrier with code = '.$CarrierCode, 17);
    }
}
