<?php

/**
 * Class MDN_MarketPlace_Helper_Stock
 *
 * @author Nicolas Mugnier <nicolas@boostmyshop.com>
 */
class MDN_MarketPlace_Helper_Stock extends Mage_Core_Helper_Abstract
{

    /**
     * @var stockZeroReason null
     */
    protected $stockZeroReason = null;

    /**
     * @var stock int
     */
    protected $stock = 0;

    /**
     * @var country null
     */
    protected $country = null;

    /**
     * Get Stock Zero Reason
     *
     * @return stockZeroReason
     */
    public function getStockZeroReason()
    {
        return $this->stockZeroReason;
    }

    /**
     * Get Value To Export For Product
     *
     * @param $product
     * @param object $country
     *
     * @return int
     */
    public function getValueToExportForProduct($product, $country)
    {
        $this->stock = 0;
        $this->stockZeroReason = null;
        $this->country = $country;
        if (!$this->getStockFromCustomAttribute($product)) {
            if ($this->checkProductStatus($product)) {
                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getentity_id());

                if (Mage::Helper('MarketPlace')->isErpInstalled()) {
                    $this->tryToGetStockFromErp($product, $stockItem);
                } else {
                    $this->stock = $stockItem->getQty();
                }

                $this->checkForceQuantity($product);
                $this->checkIsExcluded($product);
                $this->checkIsInStock($stockItem);
            }
        }

        $this->applyMarginFilter($product);
        $product->setData('stock_to_export', $this->stock);
        Mage::dispatchEvent('marketplace_before_export_stock', array('product' => $product));
        $this->processAfterDispatchEvent($product);
        $this->processLogStockZeroReason();

        return $this->stock;
    }

    /**
     * Get Stock From Custom Attribute
     *
     * @param $product
     *
     * @return bool
     */
    protected function getStockFromCustomAttribute($product)
    {
        $hasCustomPrice = false;
        if (is_integer($product->getmp_custom_stock()) || ($product->getmp_custom_stock() != '' && $product->getmp_custom_stock() !== null)) {
            $this->stock = $product->getmp_custom_stock();
            $hasCustomPrice = true;
        }

        return $hasCustomPrice;
    }

    /**
     * Check Product Status
     *
     * @param $product
     *
     * @return bool
     */
    protected function checkProductStatus($product)
    {
        if ($product->getstatus() == 2) {
            $this->stockZeroReason = 'pa_status';
        }

        return ($this->stockZeroReason === null) ? true : false;
    }

    /**
     * Try To Get Stock From Erp
     *
     * @param $product
     * @param $stockItem
     */
    protected function tryToGetStockFromErp($product, $stockItem)
    {
        $pas = Mage::getModel('SalesOrderPlanning/ProductAvailabilityStatus')->load($product->getentity_id(), 'pa_product_id');

        if ($pas->getId()) {
            $this->stock = $pas->getpa_available_qty();
        } else {
            $this->stock = $stockItem->getQty();
        }
    }

    /**
     * Check Force Quantity
     *
     * @param $product
     */
    protected function checkForceQuantity($product)
    {
        if ($product->getmp_force_qty() != '') {
            $this->stock = $product->getmp_force_qty();
        }
    }

    /**
     * Check Is Excluded
     *
     * @param $product
     */
    protected function checkIsExcluded($product)
    {
        if ($product->getmp_exclude() == 1) {
            $this->stock = 0;
            $this->stockZeroReason = 'mp_exclude';
        }
    }

    /**
     * Check Is In Stock
     *
     * @param $stockItem
     */
    protected function checkIsInStock($stockItem)
    {
        if ($stockItem->getis_in_stock() == 0) {
            $this->stock = 0;
            $this->stockZeroReason = 'is_in_stock';
        }
    }

    /**
     * Apply Margin Filter
     *
     * @param $product
     */
    protected function applyMarginFilter($product)
    {
        $margin = $this->country->getParam('margin_min');
        if ($margin != '') {
            $cost = $product->getcost();
            $price = str_replace(",", ".", Mage::Helper('MarketPlace/Product')->getPriceToExport($product));
            $cost = str_replace(",", ".", $cost);

            $product_margin = round(($price - $cost) / $price * 100, 2);

            if ($product_margin < $margin && $product->getmp_force_export() == 0) {
                $this->stock = 0;
                $this->stockZeroReason = 'margin';
            }
        }
    }

    /**
     * Process After Dispatch Event
     *
     * @param $product
     */
    protected function processAfterDispatchEvent($product)
    {
        if ($product->getData('stock_to_export') >= 0) {
            $this->stock = (int)$product->getData('stock_to_export');
        } else {
            $this->stock = 0;
            $this->stockZeroReason = 'missing product data';
        }
    }

    /**
     * Process Log Stock Zero Reason
     *
     */
    protected function processLogStockZeroReason()
    {
        if ($this->stockZeroReason !== null) {
            Mage::log($this->stockZeroReason, null, 'marketplace_zero_stock_reason.log');
        }
    }

}
