<?php

/**
 * Class MDN_MarketPlace_Helper_Delay
 *
 * @category  {category}
 * @package   MDN_MarketPlace
 * @author    Nicolas Mugnier <nicolas@boostmyshop.com>
 * @copyright 2012-2015 Copyright (c) Boost My Shop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      {link}
 */
class MDN_MarketPlace_Helper_Delay extends Mage_Core_Helper_Abstract {

    /**
     * Get Value To Export For Product
     *
     * @param Varien_Object $product
     * @return mixed
     */
    public function getValueToExportForProduct($product){

        $mp_delay = $product->getmp_delay();
        return ($mp_delay !== null && $mp_delay >= 0) ? $mp_delay : Mage::registry('mp_country')->getParam('default_shipment_delay');

    }

}