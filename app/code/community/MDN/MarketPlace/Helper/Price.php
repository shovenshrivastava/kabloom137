<?php

/**
 * Class MDN_MarketPlace_Helper_Price
 *
 * @category  {category}
 * @package   MDn_MarketPlace
 * @author    Nicolas Mugnier <nicolas@boostmyshop.com>
 * @copyright 2012-2015 Copyright (c) Boost My Shop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      {link}
 */
class MDN_MarketPlace_Helper_Price extends Mage_Core_Helper_Abstract {

    /**
     * Get product price (including tax)
     *
     * @param product $product
     * @return float
     *
     * @see getPrice()
     */
    public function getPriceToExport($product)
    {
        $price = $this->getPriceBeforeEvent($product);

        $product->setData('price_to_export', $price);
        Mage::dispatchEvent('marketplace_before_export_price', array('product'=>$product));

        $price = $product->getData('price_to_export');
        $price = round($price, 2);
        $price = str_replace(".", ",", $price);

        return $price;
    }

    /**
     * Get price
     *
     * @param product $product
     * @return float
     */
    public function getPrice($product)
    {
        $price = $product->getprice();

        if ($product->getspecial_price()) {
            $price = ($this->hasSpecialPrice($product)) ? $product->getspecial_price() : $price;
        }

        return $price;
    }

    /**
     * Get price before event
     *
     * @param Varien_Object $product
     * @return float $price
     */
    public function getPriceBeforeEvent($product)
    {

        if ($product->getmp_custom_price()) {

            $price = $this->getPriceFromCustomAttribute($product);

        } else {

            $price = $this->getPriceFromMagentoAttribute($product);

        }

        $price = str_replace(",", ".", $price);

        return $price;
    }

    /**
     * Get Price From Custom Attribute
     *
     * @param $product
     * @return float|mixed
     */
    protected function getPriceFromCustomAttribute($product){

        $customPrice = $product->getmp_custom_price();
        $customPrice = $this->applyCoef($customPrice);

        $isCustomPriceAttributeInclTax = (Mage::registry('mp_country')->getParam('is_price_attribute_including_tax') == 0) ? false : true;

        if (!$isCustomPriceAttributeInclTax) {
            $customPrice = $this->addTaxes($product, $customPrice);
        }

        return $customPrice;
    }

    /**
     * Get Price From Magento Attribute
     *
     * @param $product
     * @return float|mixed
     */
    protected function getPriceFromMagentoAttribute($product){

        $price = $this->getPrice($product);
        $price = $this->applyCoef($price);

        $isMagentoAttributeInclTax = (!Mage::getStoreConfig('tax/calculation/price_includes_tax')) ? false : true;

        if (!$isMagentoAttributeInclTax) {
            $price = $this->addTaxes($product, $price);
        }

        return $price;

    }

    /**
     * Apply Coef
     *
     * @param $price
     * @return mixed
     */
    protected function applyCoef($price){

        $coef = str_replace(',', '.', Mage::registry('mp_country')->getParam('price_coef'));

        if($coef){
            $price *= $coef;
        }

        return $price;

    }

    /**
     * Add Taxes
     *
     * @param $product
     * @param $price
     * @return float|mixed
     */
    protected function addTaxes($product, $price){

        $taxRate = Mage::Helper('MarketPlace/Taxes')->getTaxRate($product);

        $price = str_replace(",", ".", $price);
        $price = round($price, 2);
        $taxCoef = 1 + $taxRate / 100;
        $price = $price * $taxCoef;

        return $price;

    }

    /**
     * Is product has special price
     *
     * @param object $product
     * @return boolean
     */
    public function hasSpecialPrice($product)
    {
        $hasSpecialPrice = false;

        if ($product->getspecial_price() != '') {
            $hasSpecialPrice = true;

            $fromdate = $product->getspecial_from_date();

            if ($fromdate != '') {
                if (strtotime($fromdate) > time()) {
                    $hasSpecialPrice = false;
                }
            }

            $todate = $product->getspecial_to_date();

            if ($todate != '') {
                if (strtotime($todate) < time()) {
                    $hasSpecialPrice = false;
                }
            }
        }

        return $hasSpecialPrice;
    }

}