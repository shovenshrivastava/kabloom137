<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_MarketPlace
 * @version 2.1
 */
class MDN_MarketPlace_Helper_Checkbarcode extends Mage_Core_Helper_Abstract
{

    const kUPC = "UPC";
    const kEAN = "EAN";
    const kISBN = "ISBN";

    const kPondMax = 3;
    const kPondMin = 1;

    private $errorMessage = false;

    /**
     * Retrieve barcode type
     *
     * @param int $code
     * @return string
     */
    public function getType($code)
    {
        $length = strlen($code);
        $type = null;

        switch ($length) {

            case '12':
                $type = self::kUPC;
                break;
            case '13':
            case '14':
            case '8':
                $type = self::kEAN;
                break;
            default:
                $type = null;
                $this->errorMessage = 'Incorrect code length';
                break;

        }

        return $type;
    }

    /**
     * Check barcode
     *
     * @param int $code
     *
     * @return mixed
     */
    public function checkCode($code)
    {
        if (!$this->isAsin($code)) {
        
            $type = $this->getType($code);

            $this->checkIsInteger($code);

            if ($this->errorMessage === false) {
                $values = str_split($code);
                $validationCode = $values[count($values) - 1];

                if ($this->calculValidationCode($values, $type) != $validationCode) {
                    $this->errorMessage = 'Incorrect validation key.';
                }
            }
        }

        return ($this->errorMessage === false) ? true : $this->errorMessage;
    }

    /**
     * Is Asin
     *
     * @param string $code
     * @return bool
     */
    private function isAsin($code)
    {
        return (preg_match('#^B[0-9]{2}#', $code)) ? true : false;
    }

    /**
     * Calcul Validation Code
     *
     * @param array $values
     * @param string $type
     * @return int
     */
    private function calculValidationCode($values, $type)
    {
        $coef = $this->getCoefficientsByType($type);
        $somme = 0;

        for ($i = 0; $i < count($values) - 1; $i++) {
            if ($i % 2) {
                $somme += $values[$i] * $coef['x'];
            } else {
                $somme += $values[$i] * $coef['y'];
            }
        }

        // divide / 10
        $tmp = $somme / 10;

        $tmp = explode('.', $tmp);
        if (count($tmp) == 1) {
            $rest = 0;
        } else {
            $rest = $tmp[1];
        }

        $key = 10 - $rest;
        return ($key == 10) ? 0 : $key;
    }

    /**
     * Get Coefficients By Type
     *
     * @param string $type
     * @return array $coef
     */
    private function getCoefficientsByType($type)
    {
        $coef = array('x' => null, 'y' => null);
        if ($type == 'EAN') {
            $coef = array('x' => 3, 'y' => 1);
        }

        if ($type == 'UPC') {
            $coef = array('x' => 1, 'y' => 3);
        }

        return $coef;
    }

    /**
     * Check code length in fonction af code type
     *
     * @param int $code
     * @param string $typeCode
     * @return boolean
     */
    public function checkCodeLength($code, $typeCode)
    {
        $type = $this->getType($code);

        $type = strtolower($type);
        $typeCode = strtolower($typeCode);

        return $type == $typeCode;
    }

    /**
     * Check Is Integer
     *
     * @param int $code
     */
    private function checkIsInteger($code)
    {
        if (!preg_match('/^[0-9]*$/', $code)) {
            $this->errorMessage = 'Invalid code';
        }
    }
}
