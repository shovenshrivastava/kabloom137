<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml sales orders grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class MDN_MarketPlace_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('sales_order_grid');
        $this->setUseAjax(true);
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareColumns() {
		
		 //$this->addColumnAfter('Sku', array(
                //'header' => Mage::helper('sales')->__('SKUs'),
                //'index' => 'prod_sku',
                //'type' => 'text',                
                
            //));


     $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order#'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
		'filter_index'=>'main_table.increment_id',
	
        ));
		
		 /*$this->addColumnAfter('Fedex Label Status', array(
                'header' => Mage::helper('sales')->__('Fedex Label Status'),
                'index' => 'fedex_label_status',
             
            ),'real_order_id');*/

	  $this->addColumnAfter('Pickup date', array(
                'header' => Mage::helper('sales')->__('Pickup date'),
                'index' => 'pickup_date',
                'type' => 'datetime',                
                'column_css_class' => 'pickup_date'
            ),'real_order_id');

            $this->addColumnAfter('Pickup Method', array(
                'header' => Mage::helper('sales')->__('Pickup Method'),
                'index' => 'pickup_method',
				'column_css_class' => 'pickup_method'
            ),'real_order_id');
            
             $this->addColumnAfter('Trading Partner', array(
                'header' => Mage::helper('sales')->__('Trading Partner'),
                'index' => 'trading_partner',
              //    'filter'    => false,
	'filter_condition_callback' => array($this, '_partnerFilter'),
         //  'sortable'  => false
             
            ),'real_order_id');
            
            $this->addColumnAfter('Partner Order ID ', array(
                'header' => Mage::helper('sales')->__('Partner Order ID'),
                'index' => 'partner_order_id',
                'type'  => 'text',
				'column_css_class' => 'pickup_method'
            ),'real_order_id');
             
			 $this->addColumnAfter('Delivery date', array(
                'header' => Mage::helper('sales')->__('Delivery date'),
                'index' => 'delivering_date',
                
                
            ),'real_order_id');
            
           /*$this->addColumnAfter('Delivery Date', array(
                'header' => Mage::helper('operations')->__('Delivery Date'),
                'index' => 'entity_id',
                'sortable'  => true,
                'filter'    => false,
				'type' => 'datetime',
                'renderer'  => 'operations_adminhtml/operations_order_renderer_delivery',
        ),'real_order_id');*/
           

                if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('sales')->__('Purchased from (store)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
                'filter_index' => 'main_table.store_id'
            ));
        }
 
        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
            'filter_index' => 'main_table.created_at'
        ));
 
        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));
        
        
 
      /*  $this->addColumn('qty_ordered', array(
            'header'    => Mage::helper('sales')->__('Items Ordered'),
            'index'     => 'qty_ordered',
            'type'      => 'number',
            'total'     => 'sum'
        ));
 
       

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Base)'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ));
 
        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ));
  */
        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
            'filter_index'=>'main_table.status',
        ));
 
  $this->addColumn('tracking_number', array(
            'header' => Mage::helper('sales')->__('Tracking Number'),
            'index' => 'tracking_number',
        ));
 
        return $this;
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/cancel')) {
            $this->getMassactionBlock()->addItem('cancel_order', array(
                'label' => Mage::helper('sales')->__('Cancel'),
                'url' => $this->getUrl('*/sales_order/massCancel'),
            ));
        }
        //if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/done')) {
            //$this->getMassactionBlock()->addItem('done', array(
                //'label' => Mage::helper('sales')->__('Done'),
                ////'url' => $this->getUrl('*/sales_order/massCancel'),
            //));
        //}
        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/hold')) {
            $this->getMassactionBlock()->addItem('hold_order', array(
                'label' => Mage::helper('sales')->__('Hold'),
                'url' => $this->getUrl('*/sales_order/massHold'),
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/unhold')) {
            $this->getMassactionBlock()->addItem('unhold_order', array(
                'label' => Mage::helper('sales')->__('Unhold'),
                'url' => $this->getUrl('*/sales_order/massUnhold'),
            ));
        }

        $this->getMassactionBlock()->addItem('pdfinvoices_order', array(
            'label' => Mage::helper('sales')->__('Print Invoices'),
            'url' => $this->getUrl('*/sales_order/pdfinvoices'),
        ));

        $this->getMassactionBlock()->addItem('pdfshipments_order', array(
            'label' => Mage::helper('sales')->__('Print Packingslips'),
            'url' => $this->getUrl('*/sales_order/pdfshipments'),
        ));

        $this->getMassactionBlock()->addItem('pdfcreditmemos_order', array(
            'label' => Mage::helper('sales')->__('Print Credit Memos'),
            'url' => $this->getUrl('*/sales_order/pdfcreditmemos'),
        ));

        $this->getMassactionBlock()->addItem('pdfdocs_order', array(
            'label' => Mage::helper('sales')->__('Print All'),
            'url' => $this->getUrl('*/sales_order/pdfdocs'),
        ));

        $this->getMassactionBlock()->addItem('print_shipping_label', array(
            'label' => Mage::helper('sales')->__('Print Shipping Labels'),
            'url' => $this->getUrl('*/sales_order_shipment/massPrintShippingLabel'),
        ));

        return $this;
    }

    public function getRowUrl($row) {
        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            return $this->getUrl('*/sales_order/view', array('order_id' => $row->getId()));
        }
        return false;
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

}
