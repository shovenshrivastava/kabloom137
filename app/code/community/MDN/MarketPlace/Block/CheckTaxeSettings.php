<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_MarketPlace
 * @version 2.1
 */
class MDN_MarketPlace_Block_CheckTaxeSettings extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * To HTML
     *
     * @return string
     */
    protected function _toHtml() {

        $html = '<div class="notification-global"> ';
        $error = false;

        foreach(Mage::Helper('MarketPlace')->getHelpers() as $k => $v){

            $helper = Mage::Helper($v);
            $name = $helper->getMarketPlaceName();

            $accountCollection = Mage::getModel('MarketPlace/Accounts')->getCollection()
                ->addFieldToFilter('mpa_mp', $name);

            foreach($accountCollection as $item){

                $countryCollection = Mage::getModel('MarketPlace/Countries')->getCollection()->addFieldToFilter('mpac_account_id', $item->getmpa_id());

                foreach($countryCollection as $countryObj){

                    $mpac_params = unserialize($countryObj->getmpac_params());

                    if(array_key_exists('active', $mpac_params) && $mpac_params['active'] == 1){

                        if(!$countryObj->getParam('country_code')){

                            $error = true;
                            $html .= '<font color=red><b>Caution !! It seems that taxes settings are missing for '.ucfirst($name).' - '.$item->getmpa_name().' - '.$mpac_params['countryCode'].'</b></font><br/>';

                        }

                    }
                }

            }

        }

        $html .= '</div>';

        return ($error === true) ? $html : '';
    }

}
