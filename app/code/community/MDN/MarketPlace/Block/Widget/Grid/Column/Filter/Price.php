<?php

class MDN_MarketPlace_Block_Widget_Grid_Column_Filter_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Range {

    public function getCondition(){

        $array = array();

        $value = $this->getValue();

        if (!array_key_exists('from', $value) || $value['from'] == '')
            $value['from'] = 0;

        if (!array_key_exists('to', $value) || $value['to'] == '')
            $value['to'] = '999999999';

        //load collection
        $collection = mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('status')
            ->addAttributeToSelect('visibility')
            ->addAttributeToSelect('cost')
            ->addAttributeToSelect('special_price')
            ->addAttributeToSelect('special_from_date')
            ->addAttributeToSelect('special_to_date');

        foreach ($collection as $item) {

            $price = round(str_replace(',','.',Mage::helper('MarketPlace/Product')->getPriceToExport($item)), 2);

            if ($price >= $value['from'] && $price <= $value['to'])
                $array[] = $item->getentity_id();
        }

        return (count($array) > 0) ? array('in', $array) : array('null');

    }

}