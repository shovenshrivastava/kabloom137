<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2.0.1
 */
class MDN_Amazon_Model_VariationTypes extends Mage_Core_Model_Abstract {
    
    /**
     * Construct 
     */
    public function __construct(){
        parent::_construct();
        $this->_init('Amazon/VariationTypes');
    }
    
    /**    
     *  Get saved value
     * 
     * @param string $attrCode
     * @return string $retour 
     */
    public function getSavedValue($attrCode){
        
        $retour = 'none';
        
        $item = $this->load($attrCode, 'avt_attribute_code');
        
        if($item->getId()){
            
            $retour = $item->getavt_variation_type();
            
        }
        
        return $retour;
        
    }
    
    /**
     * Retrieve associated values
     * 
     * @param array $attributeCodes
     * @return array $retour 
     */
    public function retrieve($attributeCodes){
        
        $retour = array();
        
        foreach($attributeCodes as $code){
            
            $value = $this->getSavedValue($code);
            
            if($value)
                $retour[] = $value;
            
        }
        
        return $retour;
        
    }
    
}
