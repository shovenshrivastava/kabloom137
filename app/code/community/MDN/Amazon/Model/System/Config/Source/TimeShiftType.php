<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 * @deprecated
 */
class MDN_Amazon_Model_System_Config_Source_TimeShiftType extends Mage_Core_Model_Abstract {
    
    /**
     * To option array
     * 
     * @deprecated
     * @return array 
     */
    public function toOptionArray(){
        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        //return $this->getAllOptions();
    }
    
    /**
     * Get all options
     * 
     * @return array 
     * @deprecated
     */
    public function getAllOptions(){
        
        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*if(!$this->_options){
            
            $this->_options = array(
                array(
                    'value' => '',
                    'label' => ''
                ),
                array(
                    'value' => '+',
                    'label' => '+'
                ),
                array(
                    'value' => '-',
                    'label' => '-'
                )
            );
            
        }
        
        return $this->_options;*/
        
    }
    
}
