<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 * @deprecated since version 2
 */

class MDN_Amazon_Model_System_Config_Source_WebServiceUrls extends Mage_Core_Model_Abstract {

    /* @var array */
    protected $countryCode = array(
        'CA' => 'CA',
        'CN' => 'CN',
        'DE' => 'DE',
        'ES' => 'ES',
        'FR' => 'FR',
        'IT' => 'IT',
        'JP' => 'JP',
        'UK' => 'UK',
        'US' => 'US'
    );

    /**
     * ge tall options
     * 
     * @deprecated
     * @return array 
     */
    public function getAllOptions() {

        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*if (!$this->_options) {

            $options = array();

            foreach ($this->countryCode as $value => $label) {

                $options[] = array(
                    'value' => $value,
                    'label' => $label
                );

                $this->_options = $options;
            }
        }

        return $this->_options;*/
    }

    /**
     * To option array
     * 
     * @deprecated
     * @return array
     * @see getAllOptions 
     */
    public function toOptionArray() {
        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        //return $this->getAllOptions();
    }

}
