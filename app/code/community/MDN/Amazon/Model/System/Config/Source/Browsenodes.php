<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 * @deprecated since version 2
 */
class MDN_Amazon_Model_System_Config_Source_Browsenodes extends Mage_Core_Model_Abstract {
    
    /**
     * Get all options
     * 
     * @return array 
     * @deprecated
     */
    public function getAllOptions(){
        
        throw new Exception('Method deprecated in '.__METHOD__);
        
        /*if(!$this->_options){
            
            $options = array();
            $browseNodes = Mage::Helper('Amazon/GUA_Main')->getTrees('all');           
            
            foreach($browseNodes as $k => $v){
                
                if($k == "")
                    continue;
                
                $options[] = array(
                    'value' => strtolower($v),
                    'label' => $v
                );
                
            }        
            
            $this->_options = $options;
            
        }
        
        return $this->_options;*/
        
    }
    
    /**
     * To option array
     * 
     * @return array 
     * @deprecated
     */
    public function toOptionArray(){
        
        throw new Exception('Method deprecated in '.__METHOD__);
        
        //return $this->getAllOptions();
    }
    
}
