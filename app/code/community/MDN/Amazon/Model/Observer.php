<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Model_Observer {

    /**
     * Add amazon link in product grid
     * 
     * @param Varien_Event_Observer $observer
     */
    public function addColumn(Varien_Event_Observer $observer) {

        $grid = $observer->getgrid();

        if ($grid instanceof MDN_Amazon_Block_Grids_ProductsCreated) {

            $grid->addColumn('link', array(
                'header' => Mage::helper('MarketPlace')->__('Link'),
                'index' => 'amazon_link',
                'renderer' => 'MDN_Amazon_Block_Widget_Grid_Column_Renderer_Link',
                'filter' => false,
                'sortable' => false,
                'width' => '150px'
            ));
        }
    }

    /**
     * Test connection and set availables countries
     *
     * @param Varien_Event_Observer $observer 
     */
    public function testConnection(Varien_Event_Observer $observer) {

        $account = $observer->getaccount();
        $exception = '';
        $response = null;
        $_country = null;

        if ($account instanceof MDN_MarketPlace_Model_Accounts && $account->getmpa_mp() == 'amazon') {

            //update time shift
            Mage::helper('Amazon/MWS_TimeShift')->update();
                      
            // init xml
            $xml = new DomDocument();
            // on ne connait pas le type de compte (europeen ,amerique , asie) utilisé au départ donc il faut tester 3 urls max (com, fr, jp)
            $codes = array('US', 'FR', 'JP', 'IN');
            foreach ($codes as $code) {

                try {
                    $_country = Mage::getModel('MarketPlace/Countries')
                            ->setmpac_account_id($account->getId())
                            ->setmpac_country_code($code);

                    Mage::register('mp_country', $_country);

                    $response = Mage::helper('Amazon/MWS_Sellers')->listMarketplaceParticipations();

                    if ($response->getStatus() == 200) {
                        break;
                    }
                    
                } catch (Exception $e) {                    
                    $_country = null;
                    Mage::unregister('mp_country');
                    $exception .= $e->getMessage() . "<br/>";
                }
            }

            if ($response instanceof Zend_Http_Response && $response->getStatus() == 200) {

                $xml->loadXML($response->getBody());
                if ($xml->getElementsByTagName('ListMarketplaces')->item(0)) {

                    foreach ($xml->getElementsByTagName('ListMarketplaces')->item(0)->getElementsByTagName('Marketplace') as $marketplace) {

                        $mpac_params = array(
                            'marketplaceId' => $marketplace->getElementsByTagName('MarketplaceId')->item(0)->nodeValue,
                            'name' => $marketplace->getElementsByTagName('Name')->item(0)->nodeValue,
                            'countryCode' => $marketplace->getElementsByTagName('DefaultCountryCode')->item(0)->nodeValue,
                            'currencyCode' => $marketplace->getElementsByTagName('DefaultCurrencyCode')->item(0)->nodeValue,
                            'languageCode' => $marketplace->getElementsByTagName('DefaultLanguageCode')->item(0)->nodeValue,
                            'domain' => $marketplace->getElementsByTagName('DomainName')->item(0)->nodeValue
                        );

                        $country = Mage::getModel('MarketPlace/Countries')->getCollection()
                                ->addFieldToFilter('mpac_account_id', $account->getId())
                                ->addFieldToFilter('mpac_country_code', $marketplace->getElementsByTagName('DefaultCountryCode')->item(0)->nodeValue)
                                ->getFirstItem();

                        if (!$country->getmpac_id()) {

                            $country = Mage::getModel('MarketPlace/Countries')
                                    ->setmpac_country_code($mpac_params['countryCode'])
                                    ->setmpac_params($mpac_params)
                                    ->setmpac_account_id($account->getId())
                                    ->save();
                        }
                    }
                }
            } else {
                // access denied for all
                throw new Exception($exception);
            }
        }
    }

}

