<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Block_Configuration_Main extends Mage_Adminhtml_Block_Widget_Form {
    
    /* @vars $_mp */
    protected $_mp = '';
    
    /**
     * Construct 
     */
    public function __construct(){

        parent::_construct();
        
        $this->setTemplate('Amazon/Configuration/Main.phtml');
        
    }
    
    /**
     * Setter mp
     * 
     * @param string $value 
     */
    public function setMp($value){
        $this->_mp = $value;
    }
    
    /**
     * Getter mp
     * 
     * @return string 
     */
    public function getMp(){
        return $this->_mp;
    }
    
    /**
     * Prepare layout
     * 
     * @return type 
     */
    protected function _prepareLayout(){
        $this->setChild('save_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('Save'),
                    'onclick'   => 'mainForm.submit()',
                    'class' => 'save'
                ))
        );                
        
        return parent::_prepareLayout();
    }
    
    /**
     * Prepare form
     * 
     * @return type 
     */
    protected function _prepareForm(){
        
        $config = Mage::getModel('MarketPlace/Configuration')->getConfiguration($this->_mp);                

        $form = new Varien_Data_Form(array(
            'id' => 'mainForm',
            'name' => 'mainForm',
            'action' => $this->getUrl('MarketPlace/Configuration/saveMain'),
            'method' => 'post',
            'onsubmit' => ''
        ));
        
        $form->addField(
                'mpc_id',
                'hidden',
                array(
                    'name' => 'data[mpc_id]',
                    'value' => $config->getmpc_id(),
                )
        );
        
        $form->addField(
                'mpc_marketplace_id',
                'hidden',
                array(
                    'name' => 'data[column][mpc_marketplace_id]',
                    'value' => $config->getmpc_marketplace_id()
                )
        );
        
        $fieldset = $form->addFieldSet(
                'main_fieldset',
                array(
                    'legend' => $this->__('Configuration'),
                )
        );

        $fieldset->addField(
            'time_shift',
            'hidden',
            array(
                'name' => 'data[params][timeShift]',
                'value' => $config->gettimeShift()
            )
        );
        
        $fieldset->addField(
           'order_range',
            'select',
            array(
                'name' => 'data[params][orderRange]',
                'value' => $config->getorderRange(),
                'options' => $this->_getOrderRangeOptions(),
                'label' => $this->__('Order Range'),
                'note' => $this->__('Allow to configure Amazon orders import range (in days)'),
                'required' => true
            )
        );
        
        $fieldset->addField(
           'stock_attribute',
            'select',
            array(
                'name' => 'data[params][stockAttribute]',
                'value' => $config->getstockAttribute(),
                'options' => $this->_getAttributesAsOptions('varchar'),
                'label' => $this->__('Stock attribute'),
                'note' => $this->__('Must be a catalog_product_entity_varchar attribute.<br/>Note : leave empty in order to disable this feature.'),
                'required' => true
            )
        );
        
        // decription attribute
        $fieldset->addField(
           'description_attribute',
            'select',
            array(
                'name' => 'data[params][descriptionAttribute]',
                'value' => $config->getdescriptionAttribute(),
                'options' => $this->_getAttributesAsOptions(),
                'label' => $this->__('Description attribute'),
                'note' => $this->__('Note : leave empty in order to disable this feature. Product description field will be used by default')
            )
        );

        // image attribute
        $fieldset->addField(
            'image_source',
            'select',
            array(
                'name' => 'data[params][imageSource]',
                'value' => $config->getimageSource(),
                'options' => array('' => '', 'parent' => 'Configurable product', 'simple' => 'Simple product'),
                'label' => $this->__('Image source'),
                'note' => $this->__('Note : this parameter allow to configure using parent or child product images during configurable product creation')
            )
        );

        // bullet points textarea attribute
        $fieldset->addField(
            'bullet_points_attribute',
            'select',
            array(
                'name' => 'data[params][bulletpointsAttribute]',
                'value' => $config->getbulletpointsAttribute(),
                'options' => $this->_getAttributesAsOptions('text'),
                'label' => $this->__('Bullet points attribute'),
                'note' => $this->__('Note : it will use each line of a textarea attribute as a bullet point. Leave empty in order to disable this feature.')
            )
        );
        
        $fieldset->addField(
           'categories',
            'multiselect',
            array(
                'name' => 'data[params][categories]',
                'value' => $config->getcategories(),
                'values' => $this->_getCategoriesOptions(),
                'label' => $this->__('Categories'),
                'note' => $this->__('Select used categories')
            )
        );
        
        // browsenodes attribute
        $fieldset->addField(
           'browsenodes_attributes',
            'select',
            array(
                'name' => 'data[params][browsenodesAttribute]',
                'value' => $config->getbrowsenodesAttribute(),
                'options' => $this->_getAttributesAsOptions(),
                'label' => $this->__('Browse node attribute'),
                'note' => $this->__('Note : leave empty in order to disable this feature.')
            )
        );
        
        $fieldset->addField(
           'browse_nodes',
            'multiselect',
            array(
                'name' => 'data[params][browseNodes]',
                'value' => $config->getbrowseNodes(),
                'values' => $this->_getBrowseNodesOptions(),
                'label' => $this->__('Browse nodes'),
                'note' => $this->__('Select used browse nodes')
            )
        );
        
        // condition attribute
        $fieldset->addField(
           'condition_attributes',
            'select',
            array(
                'name' => 'data[params][conditionAttribute]',
                'value' => $config->getconditionAttribute(),
                'options' => $this->_getAttributesAsOptions(),
                'label' => $this->__('Product condition attribute'),
                'note' => $this->__('Note : leave empty in order to disable this feature.')
            )
        );

        // condition default value
        $fieldset->addField(
            'condition_default_value',
            'select',
            array(
                'name' => 'data[params][conditionDefaultValue]',
                'value' => $config->getconditionDefaultValue(),
                'options' => $this->_getConditionsAsOptions(),
                'label' => $this->__('Product condition default value'),
                'note' => $this->__('Note : leave empty to disable this feature.')
            )
        );

        $fieldset->addField(
            'max_to_export',
            'text',
            array(
                'name' => 'data[params][max_to_export]',
                'value' => ($config->getmax_to_export()) ? $config->getmax_to_export() : 500,
                'label' => $this->__('Max to export'),
                'note' => $this->__('Set exported products limit')
            )
        );
        
        $fieldset->addField(
            'log_last_request',
            'select',
            array(
                'name' => 'data[params][logLastRequest]',
                'value' => $config->getlogLastRequest(),
                'options' => array('0' => $this->__('No'), '1' => $this->__('Yes')),
                'label' => $this->__('Log last request ?'),
                'note' => $this->__('Enable this option will log all Amazon queries'),
                'required' => false
            )
                
        );


        $fieldset->addField(
            'feed_sleep',
            'text',
            array(
                'name' => 'data[params][feed_sleep]',
                'value' => ($config->getfeed_sleep()) ? $config->getfeed_sleep() : 0,
                'label' => $this->__('Sleep time after feeds'),
                'note' => $this->__('seconds, to use only if you experience throttled requests')
            )
        );
        
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
        
    } 
    
    /**
     * Orders import range
     * 
     * @return array $retour
     */
    protected function _getOrderRangeOptions(){
        
        $retour = array();
        
        for($i = 1; $i<15; $i++){
            
            $retour[$i] = $i;
            
        }
        
        return $retour;
        
    }
    
    /**
     * Get browse nodes
     * 
     * @return array $retour 
     */
    protected function _getBrowseNodesOptions(){
        
        $retour = array();
        $browseNodes = Mage::Helper('Amazon/GUA_Main')->getAllBrowseNodes();

        asort($browseNodes);
        
        foreach($browseNodes as $k => $v){

            $retour[] = array(
                'value' => strtolower($v),
                'label' => $v
            );

        }        

        return $retour;
    }
    
    /**
     * Get categories
     * 
     * @return array $retour 
     */
    protected function _getCategoriesOptions(){
        
        $retour = array();
        
        $categories = Mage::Helper('Amazon/GUA_Main')->getCategories();    
        
        asort($categories);
        
        foreach($categories as $k => $v){                                

            if($k == "")
                continue;

            $retour[] = array(
                'value' => $k,
                'label' => $v
            );

        }
        
        return $retour;
        
    }
    
    /**
     * Get attributes
     * 
     * @return array $retour 
     */
    protected function _getAttributesAsOptions($type = null){
        
        $retour = array();
        
        $entityTypeId = Mage::getModel('eav/entity_type')->loadByCode('catalog_product')->getId();
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setEntityTypeFilter($entityTypeId);

        if($type !== null)
            $attributes->addFieldToFilter('backend_type', $type);

        //add empty
        $retour[] = '';           

        foreach ($attributes as $attribute) {
            $retour[$attribute->getAttributeCode()] = $attribute->getName();            
        }

        return $retour;
    }

    protected function _getConditionsAsOptions()
    {
        $retour = array();
        $retour[] = '';
        $values = explode(',', 'New,UsedLikeNew,UsedVeryGood,UsedGood,UsedAcceptable,CollectibleLikeNew,CollectibleVeryGood,CollectibleGood,CollectibleAcceptable,Refurbished');
        foreach($values as $value)
            $retour[$value] = $value;
        return $retour;
    }
    
    /**
     * Get header
     * 
     * @return string 
     */
    protected function _getHeader(){
        return $this->__('Main');
    }
    
}
