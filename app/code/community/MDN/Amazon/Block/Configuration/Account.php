<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Block_Configuration_Account extends Mage_Adminhtml_Block_Widget_Form {
    
    /* @vars $_account */
    protected $_account = null;
    /* @vars $_params */
    protected $_params = null;   
    /* @vars $_accountId */
    protected $_accountId = null;
    
    /**
     * Construct 
     */
    public function __construct(){

        parent::_construct();
        
        $this->setTemplate('Amazon/Configuration/Account.phtml');
        
    }
    
    /**
     * Setter account id
     * 
     * @param int $value 
     */
    public function setAccountId($value){
        $this->_accountId = $value;
    }
    
    /**
     * Getter account id
     * 
     * @return int 
     */
    public function getAccountId(){
        return $this->_accountId;
    }
    
    /**
     * Prepare layout
     * 
     * @return type 
     */
    protected function _prepareLayout(){
        
        $this->setChild('save_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('Save'),
                    'onclick'   => 'accountForm.submit()',
                    'class' => 'save'
                ))
        );            
        
        return parent::_prepareLayout();
        
    }
    
    /**
     * Before html
     * 
     * @return type 
     */
    protected function _beforeToHtml(){
        
        
        $this->setChild('delete_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('Delete'),
                    'onclick'   => 'setLocation(\''.$this->getUrl('MarketPlace/Configuration/deleteAccount', array('account' => $this->_accountId)).'\')',
                    'class' => 'delete'
                ))
        );
        
        return parent::_beforeToHtml();
    }
    
    /**
     * Prepare form
     * 
     * @return type 
     */
    protected function _prepareForm(){
        
        $form = new Varien_Data_Form(array(
                'id' => 'accountForm',
                'name' => 'accountForm',
                'action' => $this->getUrl('MarketPlace/Configuration/saveAccount'),
                'method' => 'post',
                'onsubmit' => ''
        ));
        
        /*$servicesFieldset = $form->addFieldset(
                'services_fieldset',
                array(
                    'legend' => $this->__('Services status')
                )
        );
        
        $servicesFieldset->addField(
                'service_status_button',
                'button',
                array(
                    'name' => 'services_status',
                    'value' => $this->__('Process'),
                    'label' => $this->__('Services status'),
                    'onclick' => 'account.checkServicesStatus('.$this->_accountId.')'
                )
        );*/
        
        $mainFieldset = $form->addFieldset(
                'main_fieldset',
                array(
                    'legend' => $this->__('Main')
                )
        );
        
        $mwsFieldset = $form->addFieldset(
                'mws_fieldset',
                array(
                    'legend' => $this->__('MWS'),                   
                )
        );                
        
        // account name
        $mainFieldset->addField(
                'mpa_name',
                'text',
                array(
                    'name' => 'data[column][mpa_name]',
                    'value' => $this->getAccount()->getmpa_name(),
                    'label' => $this->__('Account name'),
                    'required' => true,
                    'class' => 'validate-email'
                )
        );
        
        // marketplace id
        $form->addField(
                'mpa_mp',
                'hidden',
                array(
                    'name' => 'data[column][mpa_mp]',
                    'value' => Mage::Helper('Amazon')->getMarketPlaceName()
                )
        );
        
        $form->addField(
                'mpa_id',
                'hidden',
                array(
                    'name' => 'data[column][mpa_id]',
                    'value' => $this->getAccount()->getmpa_id()
                )
        );
        
        // merchant id
        $mwsFieldset->addField(
                MDN_MarketPlace_Model_Accounts::kParamMWSMerchantID,
                'text',
                array(
                    'name' => 'data[params]['.MDN_MarketPlace_Model_Accounts::kParamMWSMerchantID.']',
                    'value' => $this->getDataParam(MDN_MarketPlace_Model_Accounts::kParamMWSMerchantID),
                    'label' => $this->__('Merchant ID'),
                    'required' => true
                )
        );
        
        // access key
        $mwsFieldset->addField(
                MDN_MarketPlace_Model_Accounts::kParamMWSAccessKeyID,
                'text',
                array(
                    'name' => 'data[params]['.MDN_MarketPlace_Model_Accounts::kParamMWSAccessKeyID.']',
                    'value' => $this->getDataParam(MDN_MarketPlace_Model_Accounts::kParamMWSAccessKeyID),
                    'label' => $this->__('Access Key ID'),
                    'required' => true
                )
        );
        
        // secret key
        $mwsFieldset->addField(
                MDN_MarketPlace_Model_Accounts::kParamMWSSecretKey,
                'text',
                array(
                    'name' => 'data[params]['.MDN_MarketPlace_Model_Accounts::kParamMWSSecretKey.']',
                    'value' => $this->getDataParam(MDN_MarketPlace_Model_Accounts::kParamMWSSecretKey),
                    'label' => $this->__('Secret Key'),
                    'required' => true,
                    'note' => $this->__('Visit <a href="https://developer.amazonservices.com/" target="_blank">https://developer.amazonservices.com/</a> in order to get your credentials')                
                )
        );
        
        // is active
        $mainFieldset->addField(
                'is_active',
                'select',
                array(
                    'name' => 'data[params][is_active]',
                    'value' => $this->getDataParam('is_active'),
                    'options' => $this->_getYesNoOptions(),
                    'required' => true,
                    'label' => $this->__('Active'),
                    'note' => $this->__('Note : is current account active ?')
                )
        );
        
        // is adult products
        $mainFieldset->addField(
               MDN_MarketPlace_Model_Accounts::kParamIsAdultProduct,
                'select',
                array(
                    'name' => 'data[params]['.MDN_MarketPlace_Model_Accounts::kParamIsAdultProduct.']',
                    'value' => $this->getDataParam(MDN_MarketPlace_Model_Accounts::kParamIsAdultProduct),
                    'options' => $this->_getYesNoOptions(),
                    'label' => $this->__('Is adult product'),
                    'note' => $this->__('Are you selling adult products ?')
                )
        );                
        
        // barcode attribute
        /*$mainFieldset->addField(
                MDN_MarketPlace_Model_Accounts::kParamBarcodeAttr,
                'select',
                array(
                    'name' => 'data[params]['.MDN_MarketPlace_Model_Accounts::kParamBarcodeAttr.']',
                    'value' => $this->getDataParam(MDN_MarketPlace_Model_Accounts::kParamBarcodeAttr),
                    'options' => $this->_getAttributesValues(),
                    'label' => $this->__('Barcode attribute'),
                    'note' => $this->__('note : select barcode attribute')
                )
        );*/
        
        // barcode type
        $mainFieldset->addField(
                MDN_MarketPlace_Model_Accounts::kParamBarcodeType,
                'select',
                array(
                    'name' => 'data[params]['.MDN_MarketPlace_Model_Accounts::kParamBarcodeType.']',
                    'value' => $this->getDataParam(MDN_MarketPlace_Model_Accounts::kParamBarcodeType),
                    'options' => $this->_getBarcodeTypesValues(),
                    'label' => $this->__('Barcode type'),
                    'note' => $this->__('note : select barcode type')
                )
        );
        
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
        
    }
    
    /**
     * Ge tyes / no options
     * 
     * @return array 
     */
    protected function _getYesNoOptions(){
        
        return array(
            '' => '',
            '0' => $this->__('No'),
            '1' => $this->__('Yes')
         );
        
    }
    

    /**
     * Get params
     * 
     * @return array  
     */
    public function getParams() {

        if ($this->_params === null) {
            $this->_params = unserialize($this->getAccount()->getmpa_params());
        }

        return $this->_params;
    }

    /**
     * Get account
     * 
     * @return MDN_MarketPlace_Model_Account 
     */
    public function getAccount() {

        if ($this->_account === null) {    
            
            $this->_account = Mage::getModel('MarketPlace/Accounts')->load($this->_accountId);
        }

        return $this->_account;
    }

    /**
     * Get data params
     * 
     * @param string $label
     * @return string $retour 
     */
    public function getDataParam($label) {

        $retour = '';
        $data = $this->getParams();

        if (is_array($data) && array_key_exists($label, $data))
            $retour = $data[$label];

        return $retour;
    }

    /**
     * get attributes values
     * 
     * @return array $options 
     */
    protected function _getAttributesValues() {

        $options = array();
        $entityTypeId = Mage::getModel('eav/entity_type')->loadByCode('catalog_product')->getId();
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($entityTypeId);

        //add empty
        $options[] = '';

        foreach ($attributes as $attribute) {
            $options[$attribute->getAttributeCode()] = $attribute->getName();
        }
        
        return $options;
    }

    /**
     * Get barcodes types values
     * 
     * @return array 
     */
    protected function _getBarcodeTypesValues(){
        
        return Mage::Helper('Amazon/ECS_Main')->getTypes();        
        
    }
    
    /**
     * get header
     * 
     * @return string 
     */
    protected function _getHeader(){
        
        if($this->_accountId)
            $header = $this->__('%s',$this->getAccount()->getmpa_name());
        else
            $header = $this->__('Create a new account');
        
        return $header;
    }
    
}
