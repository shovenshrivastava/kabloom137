<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Block_Requiredfields_Edit extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * Construct 
     */
    public function __construct(){

        $this->category = $this->getRequest()->getParam('category');
        $this->marketplace = Mage::helper('Amazon')->getMarketPlaceName();
        $this->fields = $this->getFields($this->category);
        $this->attributeTab = Mage::getModel('MarketPlace/System_Config_ProductAttribute')->getAllOptions();

    }

    /**
     * get required fields for one category
     * 
     * @param string $category
     * @return array $retour 
     */
    public function getFields($category){

        $retour = array();
       
        $filename = Mage::helper('Amazon/XSD_Main')->retrieveFilename($category);
        $retour = Mage::helper('Amazon/ProductCreation')->getRequiredFields($filename);        
                
        return $retour;

    }

    /**
     * Get back url
     * 
     * @return string 
     */
    public function GetBackUrl() {
        return $this->getUrl('Amazon/Main/index', array());
    }

    /**
     * Count main category fields
     * 
     * @param array $fields
     * @return int $i
     */
    public function countMainCategoryFields($fields){

        $i = 0;

        foreach($fields as $field){
            if(!preg_match('/producttype/i', $field['parentNode'])) $i++;
        }

        return $i;

    }

    /**
     * Get restrictions list
     * 
     * @param string $field
     * @return string $html 
     */
    public function getRestrictionList($field){

        $html = "<ul>";
        
        if(array_key_exists('restriction', $field)){
            
            $type = $field['restriction'];

            $type = preg_replace('/xsd:/','',$type);

            foreach($field[$type]['enumeration'] as $k => $v){

                $html .= '<li>'.$v.'</li>';

            }

        }

        if(array_key_exists('constraint', $field) && !is_string($field['constraint'])){

            $type = $field['constraint'][1]['restriction'];

            $type = preg_replace('/xsd:/','',$type);

            foreach($field['constraint'][1][$type]['enumeration'] as $k => $v){

                $html .= '<li>'.$v.'</li>';

            }

        }

        $html .= "</ul>";

        return $html;

    }

    /**
     * Get type
     * 
     * @param string $field
     * @return string $retour 
     */
    public function getType($field){

        $retour = "";

        if(array_key_exists('constraint', $field)){

            
            $retour =  (is_string($field['constraint'])) ? $field['constraint'] : $field['constraint'][1]['name'];


        }

        return $retour;

    }
    

}
