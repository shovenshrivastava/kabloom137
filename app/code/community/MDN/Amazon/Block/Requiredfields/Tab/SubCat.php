<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Block_Requiredfields_Tab_SubCat extends Mage_Adminhtml_Block_Widget{
    
    /**
     * Construct 
     */
    public function __construct(){

        parent::__construct();
        $this->setHtmlId('sub_cat');
        $this->setTemplate('Amazon/Requiredfields/Tab/SubCat.phtml');

    }
    
    /**
     * Get back url
     * 
     * @return string 
     * @todo : factorise avec MDN_Amazon_Block_Index_Tab_MainCat
     */
    public function GetBackUrl(){
        return $this->getUrl('MarketPlace/Attributes/Index', array('mp' => 'amazon', 'tab' => 'amazon_required'));
    }
    
    /**
     * Get restrictions list
     * 
     * @param string $field
     * @return string $html 
     * @todo : factorise avec MDN_Amazon_Block_Index_Tab_MainCat
     */
    public function getRestrictionList($field){

        $html = "<ul>";
        
        if(array_key_exists('restriction', $field)){
            
            $type = $field['restriction'];

            $type = preg_replace('/xsd:/','',$type);

            foreach($field[$type]['enumeration'] as $k => $v){

                $html .= '<li>'.$v.'</li>';

            }

        }

        if(array_key_exists('constraint', $field) 
                && is_array($field['constraint'])
                && array_key_exists(1, $field['constraint'])
                && array_key_exists('restriction', $field['constraint'][1])){

            $type = $field['constraint'][1]['restriction'];

            $type = preg_replace('/xsd:/','',$type);

            foreach($field['constraint'][1][$type]['enumeration'] as $k => $v){

                $html .= '<li>'.$v.'</li>';

            }

        }

        $html .= "</ul>";

        return $html;

    }

    /**
     * Get type
     * 
     * @param string $field
     * @return string $retour 
     * @todo : factorise avec MDN_Amazon_Block_Index_Tab_MainCat
     */
    public function getType($field){

        $retour = "";

        if(array_key_exists('constraint', $field)){

            
            $retour =  (is_string($field['constraint'])) ? $field['constraint'] : $field['constraint'][1]['name'];


        }

        return $retour;

    }
    
    /**
     * Get attributes set as combo
     * 
     * @return string $html
     * @todo : factorise avec MDN_Amazon_Block_Index_Tab_MainCat
     */
    public function getAttributesSetAsCombo(){
        
        $html = '';
        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
                ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
                ->load()
                ->toOptionHash();
        
        $html .= '<select id="attributesSetCombo'.$this->getKey().'" name="attributesSetCombo'.$this->getKey().'" style="display:none;" onchange="createAttributes(this.value, \''.$this->getCategory().'\', \''.$this->getKey().'\');">';
        $html .= '<option></option>';
        foreach($sets as $k => $value){            
            
            $html .= '<option value="'.$k.'">'.$value.'</option>';
        }
        $html .= '</select>';
        
        return $html;                
        
    }
    
}
