<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Block_Requiredfields_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {
    
    /**
     * Construct 
     */
    public function __construct() {

        parent::__construct();                
        
        $this->setId('amazon_requiredfields_tabs');
        $this->setDestElementId('amazon_requiredfields_tab_content');
        
        $this->category = $this->getRequest()->getParam('category');
        $this->fields = $this->getFields($this->category);
        $this->attributeTab = Mage::getModel('MarketPlace/System_Config_ProductAttribute')->getAllOptions();
        $this->setTitle(Mage::helper('MarketPlace')->__('Categories'));        

    }

    /**
     * Before HTML
     * 
     * @return type 
     */
    protected function _beforeToHtml(){

        if($this->countMainCategoryFields($this->fields['mainCat']) > 0){
        
            $this->addTab('main_category', array(
                'label' => Mage::helper('MarketPlace')->__('Main category'),
                'content' => $this->getLayout()->createBlock('Amazon/Requiredfields_Tab_MainCat')->setFields($this->fields['mainCat'])->setCategory($this->category)->setAttributeTab($this->attributeTab)->toHtml(),
            ));
            
        }
        
        $_subCategories = $this->fields['subCat'];
        foreach ($_subCategories as $k => $v){
            if(count($v) > 0){
                $this->addTab($k, array(
                'label' => Mage::Helper('MarketPlace')->__($k),
                    'content' => $this->getLayout()->createBlock('Amazon/Requiredfields_Tab_SubCat')->setKey($k)->setFields($v)->setCategory($this->category)->setAttributeTab($this->attributeTab)->toHtml()
                ));
            }
            
        }
        
        //set active tab
        $defaultTab = $this->getRequest()->getParam('tab');
        if ($defaultTab == null)
        	$defaultTab = 'main_category';
        $this->setActiveTab($defaultTab);

        return parent::_beforeToHtml();
    }
    
    /**
     * get required fields for one category
     * 
     * @param string $category
     * @return array $retour 
     */
    public function getFields($category){

        $retour = array();
       
        $filename = Mage::helper('Amazon/XSD_Main')->retrieveFilename($category);
        $retour = Mage::helper('Amazon/ProductCreation')->getRequiredFields($filename);
                
        return $retour;

    }

    /**
     * Get back url
     * 
     * @return string 
     */
    public function GetBackUrl() {
        return $this->getUrl('Amazon/Main/index', array());
    }

    /**
     * Count main category fields
     * 
     * @param array $fields
     * @return int $i
     */
    public function countMainCategoryFields($fields){

        $i = 0;

        foreach($fields as $field){
            if(!preg_match('/producttype/i', $field['parentNode'])) $i++;
        }

        return $i;

    }
    
}
