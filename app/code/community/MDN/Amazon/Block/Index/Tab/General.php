<?php
/* 
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Block_Index_Tab_General extends Mage_Adminhtml_Block_Widget {

    /**
     * Construct 
     */
    public function __construct(){

        parent::__construct();
        $this->setHtmlId('general');
        $this->setTemplate('Amazon/Index/Tab/General.phtml');

    }

    /**
     * Prepare Layout 
     */
    protected function _prepareLayout()
    {

        // product grid
        $block = $this->getLayout()->createBlock('MarketPlace/Products');
        $block->setTemplate('MarketPlace/Products.phtml');

        $this->setChild('marketplace_products',
                $block
        );

        // last logs table
        $lastErrorsBlock = $this->getLayout()->createBlock('MarketPlace/Logs_Errors');
        $lastErrorsBlock->setTemplate('MarketPlace/Logs/Errors.phtml');
        $this->setChild('marketplace_last_errors',$lastErrorsBlock);

    }

}
