<?php

/**
 * Class MDN_Amazon_Block_Index_Tab_MultipleAsins
 *
 * @author Nicolas Mugnier <nicolas@boostmyshop.com>
 */
class MDN_Amazon_Block_Index_Tab_MultipleAsins extends Mage_Adminhtml_Block_Widget {

    /**
     * Construct
     */
    public function __construct(){
        parent::__construct();
        $this->setHtmlId('multiple_asins');
        $this->setTemplate('Amazon/Index/Tab/MultipleAsins.phtml');
    }

    /**
     * Prepare Layout
     */
    protected function _prepareLayout()
    {

        // product grid
        $block = $this->getLayout()->createBlock('Amazon/Grids_MultipleAsins');
        $block->setTemplate('MarketPlace/Products.phtml');

        $this->setChild('multiple_asins_grid',$block);

    }

}