<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2.0
 */
class MDN_Amazon_Block_Index_Tab_variationTypes extends Mage_Adminhtml_Block_Widget_Form {
    
    /**
     * Construct 
     */
    public function __construct(){
        
        parent::__construct();
        $this->setTemplate('Amazon/Index/Tab/VariationTypes.phtml');
        
    }
    
    /**
     * Prepare layout
     * 
     * @return type 
     */
    protected function _prepareLayout(){
        
        $this->setChild('save_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('Save'),
                    'onclick'   => 'variationTypesForm.submit()',
                    'class' => 'save'
                ))
        );     
        
        return parent::_prepareLayout();
        
    }
    
    /**
     * Prepare form
     * 
     * @return type 
     */
    protected function _prepareForm(){
        
        $form = new Varien_Data_Form(array(
                'id' => 'variationTypesForm',
                'name' => 'variationTypesForm',
                'action' => $this->getUrl('Amazon/VariationTypes/save'),
                'method' => 'post',
                'onsubmit' => ''
        ));
        
        $mainFieldset = $form->addFieldset(
                'main_fieldset',
                array(
                    'legend' => $this->__('Main'),
                )
        );
        
        $entityTypeId = Mage::getModel('eav/entity_type')->loadByCode('catalog_product')->getId();
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($entityTypeId);

        foreach ($attributes as $attribute) {
            //echo $attribute->getAttributeCode().' : '.$attribute->getfrontend_input().'<br/>';
            if(is_object($attribute) && $attribute->getAttributeCode() != 'seo_category' && $attribute->getfrontend_input() == 'select'){
            
                $mainFieldset->addField(
                    'variation_'.$attribute->getAttributeCode(),
                        'select',
                        array(
                            'name' => 'data['.$attribute->getAttributeCode().']',
                            'value' => $this->getSavedValue($attribute->getAttributeCode()),
                            'options' => $this->_getVariationTypes(),
                            'label' => $attribute->getFrontend()->getLabel()
                        )
                );
                
            }
            
        }        

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
        
    }
    
    /**
     * get variation types
     * 
     * @return array $retour 
     */
    protected function _getVariationTypes(){
        
        $retour = array();
        
        $retour[] = '';
        
        foreach(Mage::helper('Amazon/Configurables_Configurables')->getVariationTypesAsArray() as $k => $v)
            $retour[$k] = $v;
        
        
        return $retour;
    }
    
    /**
     * Get saved value
     * 
     * @param string $attrCode
     * @return string 
     */
    public function getSavedValue($attrCode){
        return Mage::getModel('Amazon/VariationTypes')->getSavedValue($attrCode);
    }
    
}
