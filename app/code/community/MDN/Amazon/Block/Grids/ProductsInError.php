<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Block_Grids_ProductsInError extends MDN_MarketPlace_Block_Products {

    /**
     * Construct 
     */
    public function __construct() {

        parent::__construct();
        $this->setId('error_products_grid');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText('Aucun elt');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * Get condition
     * 
     * @return string 
     */
    protected function _getCondition() {
        return ($this->getCountry()->getId()) ? 'mp_marketplace_id="' . $this->getCountry()->getId() . '" AND mp_marketplace_status IN ("error", "action_required")' : '1';
    }

    /**
     * get grid url (ajax use) 
     */
    public function getGridUrl() {
        return $this->getUrl('Amazon/Main/productsInErrorGridAjax', array('_current' => true, 'country_id' => $this->getCountry()->getId()));
    }

    /**
     * Prepare columns
     * 
     * @return type 
     */
    protected function _prepareColumns() {

        // DISPLAY MANUFACTURER
        $this->addColumn('manufacturer', array(
            'header' => Mage::helper('MarketPlace')->__('Manufacturer'),
            'index' => Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_manufacturer_attribute(),
            'type' => 'options',
            'options' => $this->getManufacturerOptions(),
            'width' => '100px'
        ));

        // DISPLAY SKU
        $this->addColumn('sku', array(
            'header' => Mage::helper('MarketPlace')->__('Sku'),
            'index' => 'sku',
            'width' => '120px'
        ));

        // DISPLAY EAN
        $this->addColumn('ean', array(
            'header' => Mage::helper('MarketPlace')->__('Ean'),
            'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Barcode',
            'filter' => 'MarketPlace/Widget_Grid_Column_Filter_Barcode',
            'index' => Mage::helper('MarketPlace/Barcode')->getCollectionBarcodeIndex(),
            'type' => 'number',
            'align' => 'center',
            'sort' => false,
            'width' => '110px'
        ));

        // DISPLAY NAME
        $this->addColumn('name', array(
            'header' => Mage::helper('MarketPlace')->__('Product'),
            'index' => 'name'
        ));

        if($this->getCountry()->getParam('price_attribute')){

            // DISPLAY PRICE
            $this->addColumn('price', array(
                'header' => Mage::helper('MarketPlace')->__('Price excl tax'),
                'index' => $this->getCountry()->getParam('price_attribute'),
                'type' => 'price',
                'align' => 'right',
                'currency_code' => Mage::getStoreConfig('currency/options/base')
            ));

        }else{

            // DISPLAY PRICE
            $this->addColumn('price', array(
                'header' => Mage::helper('MarketPlace')->__('Price excl tax'),
                'index' => 'entity_id',
                'type' => 'price',
                'align' => 'right',
                'currency_code' => Mage::getStoreConfig('currency/options/base'),
                'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Price',
                'filter' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Filter_Price'
            ));

        }

        // DISPLAY VISIBILITY
        $this->addColumn('visibility', array(
            'header' => Mage::helper('MarketPlace')->__('Visibility'),
            'width' => '150px',
            'index' => 'visibility',
            'type' => 'options',
            'align' => 'center',
            'options' => Mage::getModel('catalog/product_visibility')->getOptionArray(),
        ));

        // DISPLAY STATUS
        $this->addColumn('status', array(
            'header' => Mage::helper('MarketPlace')->__('Enabled'),
            'width' => '70px',
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));

        // DISPLAY STOCKS
        $this->addColumn('Stocks', array(
            'header' => Mage::helper('MarketPlace')->__('Stocks'),
            'sortable' => false,
            'filter' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Filter_Stocks',
            'width' => '10px',
            'renderer' => 'MDN_MarketPlace_Block_Widget_Grid_Column_Renderer_Stocks',
            'index' => 'entity_id'
        ));

        // allow other modules to add some columns
        Mage::dispatchEvent('marketplace_products_grid_addcolumns', array('grid' => $this));


        $this->addColumn('add', array(
            'header' => Mage::Helper('MarketPlace')->__('Status'),
            'index' => 'mp_message',
            'frame_callback' => array($this, 'decorateStatus')
        ));

        $this->addColumn('action',
            array(
                'header'    => Mage::helper('catalog')->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'     => 'getmp_product_id',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('catalog')->__('Edit'),
                        'url'     => array(
                            'base'=>'adminhtml/catalog_product/edit'
                        ),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
            ));

        // EXPORT
        $this->addExportType('Amazon/Grid/exportCsv/country_id/' . Mage::registry('mp_country')->getId().'/grid/ProductsInError', Mage::helper('customer')->__('CSV'));

        return parent::_prepareColumns();
    }

}
