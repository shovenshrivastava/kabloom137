<?php

/**
 * Class MDN_Amazon_Block_Grids_MultipleAsins
 *
 * @author Nicolas Mugnier <nicolas@boostmyshop.com>
 */
class MDN_Amazon_Block_Grids_MultipleAsins extends MDN_MarketPlace_Block_Products {

    /**
     * Construct
     */
    public function __construct() {

        parent::__construct();
        $this->setId('multiple_asins_grid');
        $this->_parentTemplate = $this->getTemplate();
        $this->setEmptyText('Aucun elt');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * get condition
     *
     * @return string
     */
    protected function _getCondition() {
        return ($this->getCountry()->getId()) ? 'mp_marketplace_id="' . $this->getCountry()->getId() . '" AND mp_marketplace_status="'.MDN_MarketPlace_Helper_ProductCreation::kStatusActionRequired.'" AND mp_message like "%multiple ASINs%"' : '1';
    }

    /**
     * get grid url (ajax use)
     */
    public function getGridUrl() {
        return $this->getUrl('Amazon/Main/multipleAsinsGridAjax', array('_current' => true, 'country_id' => $this->getCountry()->getId()));
    }

    /**
     * _prepareColumns
     *
     */
    protected function _prepareColumns(){

        // SKU
        $this->addColumn('sku', array(
            'header' => Mage::Helper('MarketPlace')->__('SKU'),
            'index' => 'sku',
            'type' => 'string'
        ));

        // NAME
        $this->addColumn('name', array(
            'header' => Mage::Helper('MarketPlace')->__('Name'),
            'index' => 'name',
            'type' => 'string'
        ));

        // LAST UPDATED DATE
        $this->addColumn('suggested_asins', array(
            'header' => Mage::Helper('MarketPlace')->__('Suggested Asins'),
            'index' => 'mp_product_id',
            'renderer' => 'MDN_Amazon_Block_Widget_Grid_Column_Renderer_MultipleAsins',
            'type' => 'string',
            'filter' => false,
            'sortable' => false
        ));

    }

    /**
     * Prepare mass actions
     *
     * @return MDN_MarketPlace_Block_Products
     */
    protected function _prepareMassaction() {

    }

}