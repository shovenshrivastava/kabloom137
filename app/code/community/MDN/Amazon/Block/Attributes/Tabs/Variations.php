<?php

class MDN_Amazon_Block_Attributes_Tabs_Variations extends Mage_Adminhtml_Block_Widget_Form {
    
    /**
     * Construct 
     */
    public function __construct(){

        parent::__construct();
        $this->setHtmlId('amazon_attributes_tabs_variations');
        $this->setTemplate('Amazon/Attributes/Tabs/Variations.phtml');

    }
 
    /**
     * Prepare layout
     * 
     * @return type 
     */
    protected function _prepareLayout(){
        
        $this->setChild('save_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('catalog')->__('Save'),
                    'onclick'   => 'variationTypesForm.submit()',
                    'class' => 'save'
                ))
        );     
        
        return parent::_prepareLayout();
        
    }
    
    /**
     * Prepare form
     * 
     * @return type 
     */
    protected function _prepareForm(){
        
        $form = new Varien_Data_Form(array(
                'id' => 'variationTypesForm',
                'name' => 'variationTypesForm',
                'action' => $this->getUrl('Amazon/VariationTypes/save', array('tab' => 'variation_types')),
                'method' => 'post',
                'onsubmit' => ''
        ));
        
        $mainFieldset = $form->addFieldset(
                'main_fieldset',
                array(
                    'legend' => $this->__('Main'),
                )
        );
        
        $entityTypeId = Mage::getModel('eav/entity_type')->loadByCode('catalog_product')->getId();
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($entityTypeId);

        foreach ($attributes as $attribute) {
            
            if($attribute->getfrontend_input() == 'select'){
            
                $mainFieldset->addField(
                    'variation_'.$attribute->getAttributeCode(),
                        'select',
                        array(
                            'name' => 'data['.$attribute->getAttributeCode().']',
                            'value' => $this->getSavedValue($attribute->getAttributeCode()),
                            'options' => $this->_getVariationTypes(),
                            'label' => $attribute->getFrontend()->getLabel()
                        )
                );      
                
            }
            
        }        
        
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
        
    }
    
    /**
     * get variation types
     * 
     * @return array $retour 
     */
    protected function _getVariationTypes(){
        
        $retour = array();
        
        $retour['none'] = '';
        
        foreach(Mage::helper('Amazon/Configurables_Configurables')->getVariationTypesAsArray() as $k => $v){
            $retour[$k] = $v;
        }        
        
        return $retour;
    }
    
    /**
     * Get saved value
     * 
     * @param string $attrCode
     * @return string 
     */
    public function getSavedValue($attrCode){
        return Mage::getModel('Amazon/VariationTypes')->getSavedValue($attrCode);
    }
    
}
