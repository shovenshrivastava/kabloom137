<?php

class MDN_Amazon_Block_Attributes_Tabs_Required extends Mage_Adminhtml_Block_Widget {
    
    /**
     * Construct 
     */
    public function __construct(){

        parent::__construct();
        $this->setHtmlId('amazon_attributes_tabs_required');
        $this->setTemplate('Amazon/Attributes/Tabs/Required.phtml');
        $this->categories = Mage::helper('Amazon/XSD_Main')->getAllCategories(true);

    }
    
}
