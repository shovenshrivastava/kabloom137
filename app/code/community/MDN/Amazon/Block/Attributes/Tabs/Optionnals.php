<?php

class MDN_Amazon_Block_Attributes_Tabs_Optionnals extends Mage_Adminhtml_Block_Widget {
    
    /**
     * Construct 
     */
    public function __construct(){

        parent::__construct();
        $this->setHtmlId('amazon_attributes_tabs_optionnals');
        $this->setTemplate('Amazon/Attributes/Tabs/Optionnals.phtml');

    }
    
    public function getOptionnalsFields(){
        $retour = array();
        
        foreach(Mage::getModel('Amazon/OptionnalsFields')->getCollection() as $item){
            
            $retour[] = $item;
            
        }
        
        return $retour;
    }
    
    public function getAttributesAsCombo($item){
        $html = '';
        $html .= '<select name="data['.$item->getid().'][magento_attribute]" id="">';
        $html .= '<option></option>';
        
        $entityTypeId = Mage::getModel('eav/entity_type')->loadByCode('catalog_product')->getId();
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setEntityTypeFilter($entityTypeId);          

        foreach ($attributes as $attribute) {
            $selected = ($attribute->getAttributeCode() == $item->getaof_attribute_code()) ? 'selected' : '';
            $html .= '<option '.$selected.' value="'.$attribute->getAttributeCode().'">'.$attribute->getName().'</option>';           
        }

        
        return $html;
    }

    public function getActionsAsCombo($item){
        
        $html = '';
        $html .= '<a href="'.$this->getUrl('Amazon/OptionnalsFields/Delete', array('id' => $item->getId())).'">'.$this->__('Delete').'</a>';     
        
        return $html;
        
    }
    
}
