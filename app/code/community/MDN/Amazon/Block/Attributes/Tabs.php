<?php

class MDN_Amazon_Block_Attributes_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {
    
    /**
     * Construct 
     */
    public function __construct() {

        parent::__construct();                
        
        $this->setId('amazon_attributes_tabs');
        $this->setDestElementId('amazon_attributes_content');

        $this->setTitle(Mage::helper('MarketPlace')->__('Attributes'));        

    }
    
    /**
     * Before HTML
     * 
     * @return type 
     */
    protected function _beforeToHtml(){
        
        $this->addTab('amazon_required', array(
            'label' => Mage::helper('MarketPlace')->__('Required Fields'),
            'content' => $this->getLayout()->createBlock('Amazon/Attributes_Tabs_Required')->toHtml(),
        ));
        
        $this->addTab('amazon_optionnals', array(
            'label' => Mage::helper('MarketPlace')->__('Optionnals Fields'),
            'content' => $this->getLayout()->createBlock('Amazon/Attributes_Tabs_Optionnals')->toHtml(),
        ));
        
        $this->addTab('amazon_variations', array(
            'label' => Mage::helper('MarketPlace')->__('Variations Fields'),
            'content' => $this->getLayout()->createBlock('Amazon/Attributes_Tabs_Variations')->toHtml(),
        ));

        //set active tab
        $defaultTab = $this->getRequest()->getParam('tab');
        if ($defaultTab == null)
        	$defaultTab = 'amazon_required';
        $this->setActiveTab($defaultTab);

        return parent::_beforeToHtml();
    }
    
}
