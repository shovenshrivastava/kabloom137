<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2015 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas Mugnier
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Block_Widget_Grid_Column_Renderer_MultipleAsins extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    /**
     * render
     *
     * @param Varien_Object $row
     * @return string $html
     */
    public function render(Varien_Object $row){

        $country = strtolower(Mage::registry('mp_country')->getmpac_country_code());

        preg_match_all('#(B00\w+)#', $row->getmp_message(), $asinTab);

        $html = '';
        $html .= '<table>';
        $html .= '<tr>';
        if(count($asinTab) > 0){
            foreach($asinTab[0] as $asin){

                $radioName = 'selected_asin['.$row->getmp_id().']';
                $url = Mage::Helper('Amazon/Url')->getAmazonProductUri($country, $asin);
                $html .= '<td><center>';
                $html .= '<a href="'.$url.'" target="_blanck">'.$asin.'</a></br>';
                $html .= '<input type="radio" name="'.$radioName.'" value="'.$asin.'" />';
                $html .= '</td></center>';

            }
        }else{
            $html .= '<p>No available ASIN</p>';
        }
        $html .= '</tr>';
        $html .= '</table>';

        return $html;
    }

}
