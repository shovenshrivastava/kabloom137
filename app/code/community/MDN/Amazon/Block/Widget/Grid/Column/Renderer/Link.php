<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas Mugnier
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Block_Widget_Grid_Column_Renderer_Link extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    
    /**
     * render
     * 
     * @param Varien_Object $row
     * @return string $html 
     */
    public function render(Varien_Object $row){
        
        $html = '-';
        
        if($row->getmp_marketplace_status() == MDN_MarketPlace_Helper_ProductCreation::kStatusActionRequired){
            
            if($row->getmp_message() != ''){
                
                $tmp = explode(':', $row->getmp_message());
                if(count($tmp) > 1){
                    $references = explode(',', $tmp[1]);
                 
                    $html = '<ul style="list-style-type:none;">';
                    foreach($references as $reference){
                        $reference = trim($reference);
                        $html .= '<li>';
                        $html .= '<a href="http://'.Mage::registry('mp_country')->getParam('domain').'/gp/product/'.$reference.'" target="_blank">'.$reference.'</a>';
                        $html .= ' (<a href="'.$this->getUrl('MarketPlace/Products/MatchByMpReference', array('countryId' => Mage::registry('mp_country')->getId(), 'productId' => $row->getmp_product_id(), 'reference' => $reference)).'">'.$this->__('Select').'</a>)';
                        $html .= '</li>';
                    }
                    $html .= '</ul>';
                }
                
            }
            
        }else{
            
            if($row->getmp_reference())
                $html = Mage::Helper('Amazon')->getProductUrl($row->getmp_reference(), Mage::registry('mp_country'));//'<a href="http://'.Mage::registry('mp_country')->getParam('domain').'/gp/product/'.$row->getmp_reference().'" target="_blank">'.$row->getmp_reference().'</a>';
            
        }                
        
        return $html;
        
    }
    
}
