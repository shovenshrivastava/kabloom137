<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */

class MDN_Amazon_Helper_Feed extends MDN_Amazon_Helper_MWS_Feeds {

    /**
     * Get feed submission ids name
     *
     * @return string
     * @deprecated since version 2
     */
    public function getFeedSubmissionIdsName() {

        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        //return Mage::getConfig()->getTempVarDir() . '/export/marketplace/tmp/amazonFeedSubmissionIds.txt';
    }
    
    /**
     * Get current feed submission ids
     *
     * @return array $ids
     * @deprecated since version 2
     */
    public function getFeedSubmissionIds() {

        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*$ids = array();

        $file = $this->getFeedSubmissionIdsName();

        if(file_exists($file)) {

            $ids = file($file);
        }

        return $ids;*/
    }

    /**
     * Set feed submission ids
     *
     * @param array $ids
     * @deprecated since version 2
     */
    public function setFeedSubmissionIds($ids) {

        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*$str = "";

        $file = $this->getFeedSubmissionIdsName();

        foreach ($ids as $id) {

            $str .= trim($id) . "\n";
        }

        file_put_contents($file, $str);*/
    }

}