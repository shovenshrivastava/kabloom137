<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2.0
 */
class MDN_Amazon_Helper_Configurables_Configurables extends Mage_Core_Helper_Abstract {

    /* @var DOMDocument */
    protected $_xml = null;

    /**
     * Getter
     *
     * @return DOMDocument
     */
    public function getXML(){
        if($this->_xml === null){
            $filename = Mage::getBaseDir('lib') . '/MDN/Marketplace/Amazon/configurables.xml';

            $this->_xml = new DOMDocument();
            $this->_xml->loadXML(file_get_contents($filename));
        }

        return $this->_xml;
    }

    /**
     * Get variation types as array
     *
     * @return array $retour
     */
    public function getVariationTypesAsArray(){

        $retour = array();

        $xml = $this->getXML();

        foreach($xml->getElementsByTagName('variationtheme') as $variationThemeNode){

            $condition = $variationThemeNode->getElementsByTagName('condition')->item(0)->nodeValue;

            if(!preg_match('#[,|]#', $condition) && !array_key_exists(strtolower($condition), $retour))
                $retour[strtolower($condition)] = $condition;

        }

        return $retour;

    }

    /**
     * Get allowed categories
     *
     * @return array
     */
    public function getAllowedCategories(){

        return array(
            'Beauty',
            'ClothingAccessories',
            'Health',
            'ProductClothing',
            'Shoes',
            'Sports',
            'Baby',
            'ToysBaby',
            'Home'
        );

    }

}
