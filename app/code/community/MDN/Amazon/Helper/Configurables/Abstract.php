<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
abstract class MDN_Amazon_Helper_Configurables_Abstract extends Mage_Core_Helper_Abstract {

    const kVariationThemeAttributeSuffix = 'avt';

    protected $_filename;
    protected $_xml = null;

    abstract function getRootNode();

    /**
     * Get xml
     * 
     * @return \DomDocument 
     */
    protected function _getXml() {

        if ($this->_xml === null) {
            $filename = dirname(__FILE__) . '/configurables.xml';

            $this->_xml = new DOMDocument();
            $this->_xml->loadXML(file_get_contents($filename));
        }

        return $this->_xml;
    }

    /**
     * get attributes 
     * 
     * @return array $retour
     */
    public function getAttributes() {

        $retour = array();

        $xml = $this->_getXml();

        if ($xml->getElementsByTagName($this->getRootNode())->item(0)) {

            $rootNode = $xml->getElementsByTagName($this->getRootNode())->item(0);

            $attributesNode = $rootNode->getElementsByTagName('attributes')->item(0);

            foreach ($attributesNode->getElementsByTagName('category') as $categoryNode) {

                if (!array_key_exists($categoryNode->getAttribute('name'), $retour))
                    $retour[$categoryNode->getAttribute('name')] = array();

                foreach ($categoryNode->getElementsByTagName('attribute') as $attributeNode) {

                    $retour[$categoryNode->getAttribute('name')][] = $attributeNode->nodeValue;
                }
            }
        }

        return $retour;
    }

    /**
     * get variation theme list
     * 
     * @return array $retour 
     */
    public function getVariationThemeList() {

        $retour = array();

        $xml = $this->_getXml();

        if ($xml->getElementsByTagName($this->getRootNode())->item(0)) {

            $rootNode = $xml->getElementsByTagName($this->getRootNode())->item(0);
            $variationThemesNode = $rootNode->getElementsByTagName('variationthemes')->item(0);

            foreach ($variationThemesNode as $variationThemeNode) {

                $retour[] = $variationThemeNode->getAttribute('name');
            }
        }

        return $retour;
    }

    /**
     * get variation theme 
     * 
     * @param array $attributes
     * @return string $retour 
     */
    public function getVariationTheme($attributes) {

        $retour = '';

        if(count($attributes) <= 0)
            return $retour;

        sort($attributes);
        $multiple = (count($attributes) > 1) ? true : false;

        $xml = $this->_getXml();

        if ($xml->getElementsByTagName($this->getRootNode())->item(0)) {

            $rootNode = $xml->getElementsByTagName($this->getRootNode())->item(0);
            $variationThemesNode = $rootNode->getElementsByTagName('variationthemes')->item(0);

            foreach ($variationThemesNode->getElementsByTagName('variationtheme') as $variationThemeNode) {

                $conditionNode = $variationThemeNode->getElementsByTagName('condition')->item(0);
                $condition = strtolower($conditionNode->nodeValue);

                if ($multiple === true) {

                    if ($conditionNode->hasAttribute('multiple')) {

                        $tmp = explode(',', $condition);
                        sort($tmp);

                        if (count($tmp) == count($attributes)) {
                            $diff = array_diff($tmp, $attributes);
                            if (count($diff) == 0) {

                                $retour = $variationThemeNode->getAttribute('name');
                                break;
                            }
                        }
                    } else {
                        continue;
                    }
                } else {

                    $tmp = explode('|', strtolower($condition));

                    if (in_array($attributes[0], $tmp)) {

                        $retour = $variationThemeNode->getAttribute('name');
                        break;
                    }
                }
            }
        }

        return $retour;
    }

    /**
     * get filename 
     * 
     * @return string
     */
    public function getFilename() {

        Mage::Helper('Amazon/XSD')->getFilename($this->_filename);
    }

    /**
     * get attribute suffix
     * 
     * @return string 
     */
    public function getAttributeSuffix() {
        return self::kVariationThemeAttributeSuffix;
    }

    /**
     * Get Super Attributes
     * 
     * @param Mage_Catalog_Product_Type_Configurable $product 
     * @return array $retour
     */
    public function getSuperAttributes($product) {

        $retour = array(
            'attributes' => array(),
            'associatedVariations' => array(),
            'variationTheme' => ''
        );

        $attributes = Mage::getModel('Catalog/Product_Type_Configurable')->getConfigurableAttributesAsArray($product);

        foreach($product->getData() as $k => $v)
        {
            $attributes[] = array('attribute_code' => $k);
        }

        foreach ($attributes as $attribute) {

            if (Mage::getModel('Amazon/VariationTypes')->getSavedValue($attribute['attribute_code'])){

                $associationResult = Mage::getModel('Amazon/VariationTypes')->getSavedValue($attribute['attribute_code']);
                if ($associationResult && ($associationResult != 'none') && (!in_array($associationResult, $retour['attributes'])))
                {
                    $retour['attributes'][] = $associationResult;
                    $retour['associatedVariations'][$associationResult] = $attribute['attribute_code'];
                }
            }
        }
        $retour['variationTheme'] = $this->getVariationTheme($retour['attributes']);

        return $retour;
    }

}
