<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_Configurables_ClothingAccessories extends MDN_Amazon_Helper_Configurables_Abstract {

    /* @var string */
    protected $_filename = 'ProductAccessories.xsd';
    
    /**
     * get root node
     * 
     * @return string 
     */
    public function getRootNode() {
        return 'clothingaccessories';
    }

}
