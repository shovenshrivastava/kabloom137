<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2009 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_Amazon_helper_CheckResponse extends MDN_MarketPlace_Helper_CheckResponse {

    const kDone = 'Complete';

    /* @var boolean */
    protected $_updateStatus = true;

    /**
     * Check stock / price export
     * 
     * @param type $feedId
     * @param type $submit
     * @return type 
     */
    public function checkExport($feedId = null, $submit = false) {

        $collection = $this->getCollection($feedId);
        $error = false;
        $messageInError = array();
        $deleteFlag = false;

        foreach ($collection as $feed) {

            $deleteFlag = ($feed->getmp_type() == MDN_MarketPlace_Helper_Feed::kFeedTypeDeleteProduct) ? true : false;
            $error = false;
            $description = '';
            $log = '';

            // get response
            // parse it
            try{
                $response = mage::helper('Amazon/Feed')->getFeedSubmissionResult($feed->getmp_feed_id());
            }catch(Exception $e){
                if(preg_match('#InputDataError#', $e->getMessage())){

                    $error = true;

                }else{
                    throw New Exception($e->getMessage());
                }

            }

            $xml = new DomDocument('1.0', 'utf-8');
            $xml->loadXML($response);

            if ($xml->getElementsByTagName('StatusCode')->item(0) && $xml->getElementsByTagName('StatusCode')->item(0)->nodeValue == self::kDone) {

                $summary = $xml->getElementsByTagName('ProcessingSummary')->item(0);
                $processed = $summary->getElementsByTagName('MessagesProcessed')->item(0)->nodeValue;
                $success = $summary->getElementsByTagName('MessagesSuccessful')->item(0)->nodeValue;
                $errors = $summary->getElementsByTagName('MessagesWithError')->item(0)->nodeValue;
                $warnings = $summary->getElementsByTagName('MessagesWithWarning')->item(0)->nodeValue;

                $this->_cpt = $processed;
                $this->_cptError = $errors;

                if ($errors > 0) {

                    $error = true;

                    foreach ($xml->getElementsByTagName('Result') as $resultNode) {

                        $description = $resultNode->getElementsByTagName('ResultDescription')->item(0)->nodeValue;

                        if (preg_match('#This SKU does not exist in the Amazon.com catalog#', $description))
                            $log = "This SKU does not exist in the Amazon.com catalog";
                        else
                            $log = $description;

                        if ($resultNode->getElementsByTagName('AdditionalInfo')->item(0)) {

                            $additionalInfo = $resultNode->getElementsByTagName('AdditionalInfo')->item(0);

                            $productId = Mage::getModel('catalog/product')->getIdBySku($additionalInfo->getElementsByTagName('SKU')->item(0)->nodeValue);
                            $this->_idsInError[] = $productId;

                            $this->_errors[$productId] = array(
                                'messageId' => $resultNode->getElementsByTagName('MessageID')->item(0)->nodeValue,
                                'code' => $resultNode->getElementsByTagName('ResultCode')->item(0)->nodeValue,
                                'messageCode' => $resultNode->getElementsByTagName('ResultMessageCode')->item(0)->nodeValue,
                                'description' => $description,
                                'sku' => $additionalInfo->getElementsByTagName('SKU')->item(0)->nodeValue,
                                'log' => $log
                            );
                            
                            $messageInError[] = $resultNode->getElementsByTagName('MessageID')->item(0)->nodeValue;
                            
                        } else {

                            $this->_errors[] = array(
                                'messageId' => $resultNode->getElementsByTagName('MessageID')->item(0)->nodeValue,
                                'code' => $resultNode->getElementsByTagName('ResultCode')->item(0)->nodeValue,
                                'messageCode' => $resultNode->getElementsByTagName('ResultMessageCode')->item(0)->nodeValue,
                                'description' => $description,
                                'log' => $log
                            );
                            
                            $messageInError[] = $resultNode->getElementsByTagName('MessageID')->item(0)->nodeValue;
                        }
                    }
                }

                if ($error === true) {                   
                    
                    // update feed status
                    $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                            ->save();
                } else {

                    $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                            ->save();
                }
            } else {

                if($error === false)
                    $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedInProgress)
                        ->save();
                else
                    $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                        ->save();
            }
            
            // retrieve successfully updated products
            $submittedFeed = $feed->getContent();
            if($submittedFeed != ""){

                $submittedXml = new DomDocument();
                $submittedXml->loadXml($submittedFeed);
                foreach($submittedXml->getElementsByTagName('Message') as $messageNode){

                    $messageIDNode = $messageNode->getElementsByTagName('MessageID')->item(0);
                    
                    if(!in_array($messageIDNode->nodeValue, $messageInError)){

                        $sku = $messageNode->getElementsByTagName('SKU')->item(0)->nodeValue;

                        $this->_idsOk[] = Mage::getModel('catalog/product')->getIdBySku($sku);

                        if($deleteFlag === true){
                            
                            Mage::getModel('MarketPlace/Data')->deleteProduct(Mage::getModel('catalog/product')->getIdBySku($sku));
                            
                        }
                        
                    }

                }

            }
            
        }

        return $this->notifyErrors();
    }

    /**
     * Getter current marketplace
     * 
     * @return type 
     */
    public function getMp() {
        return Mage::Helper('Amazon')->getMarketPlaceName();
    }

}
