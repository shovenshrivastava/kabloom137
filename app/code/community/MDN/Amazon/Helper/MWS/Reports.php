<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_MWS_Reports extends MDN_Amazon_Helper_MWS_Abstract {

    const kVersion = '2009-01-01';    
    
    const kMarketPlaceIdTypeId = 'id';
    const kMarketPlaceIdTypeList = 'list';
    
    /* @var string */
    protected $_marketPlaceIdType = '';

    /**
     * Get before query
     * 
     * @return string 
     */
    protected function _getBeforeQuery() {
        return '/';
    }

    /**
     * Get uri
     * 
     * @return string 
     */
    public function getUri() {
        return 'https://'.$this->_getBaseUrl().$this->_getBeforeQuery();
    }

    /**
     * Get version
     * 
     * @return string 
     */
    public function getVersion() {
        return self::kVersion;
    }
    
    /**
     * Add marketplace id data in request parameters 
     */
    protected function _addMarketPlaceId(){
        
        switch($this->_marketPlaceIdType){
            case self::kMarketPlaceIdTypeList:
                $k = 'MarketplaceIdList.Id.1';
                break;
            case self::kMarketPlaceIdTypeId:
            default:
                $k = 'MarketplaceId';
                break;
        }
        
        $this->_params[$k] = $this->_country->getParam('marketplaceId');
        
    }
    
    /**
     * Request a report
     *
     * @param string $reportType
     * @param date $startDate
     * @param date $endDate
     *
     * @return string
     *
     */
    public function requestReport($reportType, $startDate, $endDate) {

        $this->_marketPlaceIdType = self::kMarketPlaceIdTypeList;
        
        $params = array(
            'Action' => 'RequestReport',
            'ReportType' => $reportType
        );

        if ($startDate != null) {

            $params['StartDate'] = $startDate;
        }

        if ($endDate != null) {

            $params['EndDate'] = $endDate;
        }

        return $this->query($params, true)->getBody();
    }
    
    /**
     * Get report list
     *
     * @return string
     */
    public function getReportList() {

        $params = array(
            'Action' => 'GetReportList',
            'ReportTypeList.Type.1' => '_GET_ORDERS_DATA_',
            'ReportTypeList.Type.2' => '_GET_MERCHANT_LISTINGS_DATA_',
            'ReportTypeList.Type.3' => '_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_',
            'ReportTypeList.Type.4' => '_GET_FLAT_FILE_ORDERS_DATA_'
        );

        return $this->query($params, false)->getBody();
    }
    
    /**
     * Request report count
     *
     * @return string
     *
     */
    public function getReportCount() {

        $params = array(
            'Action' => 'GetReportCount'
        );

        return $this->query($params, false)->getBody();
    }

    /**
     * Get report by id
     *
     * @param int $reportId
     * @return string
     *
     */
    public function getReport($reportId) {

        $params = array(
            'Action' => 'GetReport',
            'ReportId' => $reportId
        );

        return $this->query($params, false)->getBody();
    }

    /**
     * Get report request list
     *
     * @param string $type
     * @return string
     */
    public function getReportRequestList($type, $requestId = null) {

        $params = array(
            'Action' => 'GetReportRequestList'
        );

        if($requestId !== null)
            $params['ReportRequestIdList.Id.1'] = $requestId;

        return $this->query($params, false)->getBody();

    }

    /**
     * Get report after retrive id in request list
     *
     * @param int $requestId
     * @param string $type
     * @return string
     * 
     */
    public function getReportById($requestId, $type) {

        $reportId = "";
        $status = "";
        $xml = new DomDocument();
        $i = 0;
        $max = 25;
        $token = null;
        $reportList = '';
        $debug = '';
        $find = false;
        
        // on boucle afin de retrouver les informations conrespondantes à la requete en parsant un certains nombre 
        // de pages.
        do{
        
            if(is_null($token))
                $reportList = $this->getReportRequestList($type, $requestId);
            else
                $reportList = $this->getReportListByNextToken ($token, $type);            
            
            $xml->loadXML($reportList);

            $reportRequestInfos = $xml->getElementsByTagName('ReportRequestInfo');

            foreach ($reportRequestInfos as $request) {
                if ($request->getElementsByTagName('ReportRequestId')->item(0)->nodeValue == $requestId) {

                    $status = $request->getElementsByTagName('ReportProcessingStatus')->item(0)->nodeValue;
                    $find = true;
                    break;
                }
            }

            $hasNext = $xml->getElementsByTagName('HasNext')->item(0)->nodeValue;
            if($hasNext == 'true')
                $token = $xml->getElementsByTagName('NextToken')->item(0)->nodeValue;

            $i++;
            
        }while($find == false && $hasNext == 'true' && $i < $max);
        
        if ($status == "") {
            // TODO : trouver un moyen de determiner le scope !
            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(), 
                    MDN_MarketPlace_Model_Logs::kIsError, 
                    Mage::Helper('MarketPlace')->__('Unable to find report that corresponding to request id : ' . $requestId),
                    MDN_MarketPlace_Model_Logs::kScopeCreation,
                    array('fileName' => NULL)
            );
        }       

        switch ($status) {

            case "_DONE_":
                $reportId = $request->getElementsByTagName('GeneratedReportId')->item(0)->nodeValue;
                break;
            case "_SUBMITTED_":
            case "_IN_PROGRESS_":
                $reportId = "unvailable";
                break;
            default:
                $reportId = "none";
                break;
        }

        return ($reportId != "none" && $reportId != "unvailable") ? $this->getReport($reportId) : $reportId;
    }

    /**
     * get report list by next token
     *
     * @param string $token
     * @param string $type
     * @param int $accountId
     * @return string
     */
    public function getReportListByNextToken($token, $type = '') {

        $params = array(
            'Action' => 'GetReportRequestListByNextToken',
            'NextToken' => $token
        );

        if ($type != '') {
            $params['ReportType'] = $type;
        }

        return $this->query($params, false)->getBody();
    }

}
