<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_MWS_Xml extends Mage_Core_Helper_Abstract {

    /**
     * Build delete product feed
     * 
     * @param string $merchantId
     * @param array $products
     * @return string 
     */
    public function buildDeleteProducts($merchantId, $products) {

        // add declaration
        $xml = new DOMDocument('1.0', 'utf-8');

        // create AmazonEnvelope
        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        // add attributes
        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        // add Header node
        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        // add document version to header node
        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        // add merchant identifier to header node
        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        // add Message type node to AmazonEnvelope
        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Product');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        // add purge and replace node to AmazonEnvelope
        $purgeAndReplace = $xml->createElement('PurgeAndReplace', '');
        $purgeAndReplace->appendChild($xml->createTextNode('false'));
        $amazonEnvelope->appendChild($purgeAndReplace);

        $i = 1;
        foreach ($products as $sku) {

            // add message node
            $message = $xml->createElement('Message', '');
            $messageId = $xml->createElement('MessageID', '');
            $messageId->appendChild($xml->createTextNode($i));
            $message->appendChild($messageId);

            // add operation type node
            $operationType = $xml->createElement('OperationType', '');
            $operationType->appendChild($xml->createTextNode('Delete'));
            $message->appendChild($operationType);

            $productNode = $xml->createElement('Product', '');
            $skuNode = $xml->createElement('SKU', $sku);
            $productNode->appendChild($skuNode);
            $message->appendChild($productNode);

            $amazonEnvelope->appendChild($message);

            $i++;
        }

        return $xml->saveXML();
    }

    /**
     * Build product product feed
     * The Product feed contains descriptive information about the products in your catalog. This information allows Amazon
     * to build a record and assign a unique identifier known as an ASIN (Amazon Standard Item Number) to each product.
     * This feed is always the first step in submitting products to Amazon because it establishes the mapping between the
     * seller's unique identifier (SKU) and Amazon's unique identifier (ASIN)
     *
     * @return string $xmlContent
     *
     */
    public function buildProductFeed($merchantId, $products) {

        // add declaration
        $xml = new DOMDocument('1.0', 'utf-8');

        // create AmazonEnvelope
        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        // add attributes
        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        // add Header node
        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        // add document version to header node
        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        // add merchant identifier to header node
        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        // add Message type node to AmazonEnvelope
        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Product');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        // add purge and replace node to AmazonEnvelope
        $purgeAndReplace = $xml->createElement('PurgeAndReplace', '');
        $purgeAndReplace->appendChild($xml->createTextNode('false'));
        $amazonEnvelope->appendChild($purgeAndReplace);

        $id = 0;
        // add new products to feed
        foreach ($products as $newProduct) {

            $id++;

            // add message node
            $message = $xml->createElement('Message', '');
            $messageId = $xml->createElement('MessageID', '');
            $messageId->appendChild($xml->createTextNode($id));
            $message->appendChild($messageId);

            // add operation type node
            $operationType = $xml->createElement('OperationType', '');
            $operationType->appendChild($xml->createTextNode('PartialUpdate'));
            $message->appendChild($operationType);

            // add product node
            $product = $xml->createElement('Product', '');

            // add general information to product node
            foreach ($newProduct['general'] as $k => $v) {

                if ($v != "") {

                    $elt = $xml->createElement($k, '');
                    $elt->appendChild($xml->createTextNode($v));
                    $product->appendChild($elt);
                }
            }

            // add standard product id to product node (EAN code)
            if (array_key_exists('StandardProductID', $newProduct) && $newProduct['StandardProductID']['Value'] != "") {

                $standardProductId = $xml->createElement('StandardProductID', '');

                foreach ($newProduct['StandardProductID'] as $k => $v) {
                    $elt = $xml->createElement($k, '');
                    $elt->appendChild($xml->createTextNode($v));
                    $standardProductId->appendChild($elt);
                }

                $product->appendChild($standardProductId);
            }

            // Condition
            if(array_key_exists('Condition', $newProduct)){
                $conditionNode = $xml->createElement('Condition', '');
                $product->appendChild($conditionNode);

                $elt = $xml->createElement('ConditionType', $newProduct['Condition']);
                $conditionNode->appendChild($elt);
            }

            // description data to product node
            $descriptionData = $xml->createElement('DescriptionData', '');
            $product->appendchild($descriptionData);

            // walk description data array
            foreach ($newProduct['descriptionData'] as $k => $v) {

                // if simple field
                if (!is_array($v)) {

                    if ($v != "") {

                        $unitOfMeasureTab = Mage::helper('Amazon/XSD_Main')->getUnitOfMeasureForRequiredFields();
                        if (array_key_exists($k, $unitOfMeasureTab)) {
                            $elt = $xml->createElement($k, '');
                            $elt->appendChild($xml->createTextNode($v));
                            $unitOfMeasure = $xml->createAttribute('unitOfMeasure');
                            $unitOfMeasure->appendChild($xml->createTextNode($unitOfMeasureTab[$k]));
                            $elt->appendChild($unitOfMeasure);
                            $descriptionData->appendChild($elt);
                        } else {
                            $elt = $xml->createElement($k, '');
                            if ($k == 'Description')
                                $elt->appendChild($xml->createCDATASection($v));
                            else
                                $elt->appendChild($xml->createTextNode($v));
                            $descriptionData->appendChild($elt);
                        }
                    }
                } else {
                    // multiple node (more than one occurence)
                    if (count($v) > 0) {
                        // walk occurences node
                        foreach ($v as $input) {

                            $elt = $xml->createElement($k, '');
                            $elt->appendChild($xml->createTextNode($input));
                            $descriptionData->appendChild($elt);
                        }
                    }
                }
            }

            // if there are some product data informations
            if (array_key_exists('ProductData', $newProduct)) {

                // create product data node
                $productData = $xml->createElement('ProductData', '');

                if ($newProduct['ProductData']['name']) {

                    // add main category node
                    $productDataType = $xml->createElement($newProduct['ProductData']['name'], '');
                    $productData->appendChild($productDataType);

                    // check details
                    if (count($newProduct['ProductData']['details']) > 0) {

                        foreach ($newProduct['ProductData']['details'] as $k => $v) {
                            $this->addProductDataDetail($xml, $productDataType, $v);
                        }
                    }
                }
                
                // add product data to product node
                $product->appendChild($productData);
            }

            // add product to message node
            $message->appendChild($product);

            // add messgae to AmazonEnvelope
            $amazonEnvelope->appendChild($message);
        }

        $xmlContent = $xml->saveXML();

        return $xmlContent;
    }

    /**
     * Add node to DomDocument by parsing array $var
     *
     * @param DomDocument $xml
     * @param DOMNode $node
     * @param array $var
     * @return 
     */
    private function addProductDataDetail($xml, $node, $var) {

        if (is_array($var)) {

            // get first key
            $keys = array_keys($var);

            foreach($keys as $key => $elt){ 

                if ($elt != "") {

                    // get next elt
                    $newVar = $var[$elt];

                    // check if $elt node exists
                    if (!$node->getElementsByTagName($elt)->item(0)) {
                        // if not create it
                        $newNode = $xml->createElement($elt, '');
                        // check unitOfMeasure
                        $unitOfMeasure = Mage::helper('Amazon/XSD_Main')->getUnitOfMeasureForRequiredFields();
                        if (array_key_exists($elt, $unitOfMeasure)) {
                            $attribute = $xml->createAttribute('unitOfMeasure');
                            $attribute->appendChild($xml->createTextNode($unitOfMeasure[$elt]));
                            $newNode->appendChild($attribute);
                        }
                        $node->appendChild($newNode);
                    } else {

                        // set it as newNode
                        $newNode = $node->getElementsByTagName($elt)->item(0);
                    }

                    // recursive call
                    $this->addProductDataDetail($xml, $newNode, $newVar);
                }
            
            }
            
        } else {
            // add text Node
            $node->appendChild($xml->createTextNode($var));
        }

        return;
    }

    /**
     * Build inventory feed
     * The Inventory feed allows you to update inventory quantities (stock levels) for your items
     *
     * @return string $xmlContent
     *
     */
    public function buildInventoryFeed($merchantId, $products) {

        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Inventory');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        $id = 0;
        foreach ($products as $product) {
            $id++;

            $message = $xml->createElement('Message', '');

            $messageId = $xml->createElement('MessageID');
            $txtMessageId = $xml->createTextNode($id);
            $messageId->appendChild($txtMessageId);
            $message->appendChild($messageId);

            $operationType = $xml->createElement('OperationType', '');
            $operationType->appendChild($xml->createTextNode('Update'));
            $message->appendchild($operationType);

            $inventory = $xml->createElement('Inventory', '');

            // add sku
            $sku = $xml->createElement('SKU');
            $skuValue = trim($product['sku']);
            $sku->appendChild($xml->createTextNode($skuValue));
            $inventory->appendChild($sku);

            // add stock
            $quantity = $xml->createElement('Quantity', '');
            $quantity->appendChild($xml->createTextNode((int) $product['stock']));
            $inventory->appendChild($quantity);

            // add delay
            $fulfillmentLatency = $xml->createElement('FulfillmentLatency', '');
            $fulfillmentLatency->appendChild($xml->createTextNode($product['delay']));
            $inventory->appendChild($fulfillmentLatency);

            $message->appendChild($inventory);
            $amazonEnvelope->appendChild($message);
        }

        $xmlContent = $xml->saveXML();

        return $xmlContent;
    }

    /**
     * Build price feed
     * The Price feed allows you to set the current price and sale price (when applicable) for an item. The sale price is
     * optional, but, if used, the start and end date must be provided also
     *
     * @return string $xmlContent
     *
     */
    public function buildPriceFeed($merchantId, $products) {


        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Price');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        $id = 0;
        $configCurrency = Mage::registry('mp_country')->getParam('currency');

        foreach ($products as $product) {
            $id++;

            $message = $xml->createElement('Message', '');

            $messageId = $xml->createElement('MessageID');
            $txtMessageId = $xml->createTextNode($id);
            $messageId->appendChild($txtMessageId);
            $message->appendChild($messageId);

            $price = $xml->createElement('Price', '');

            $sku = $xml->createElement('SKU');
            $skuValue = trim($product['sku']);
            $sku->appendChild($xml->createTextNode($skuValue));
            $price->appendChild($sku);

            $standardPrice = $xml->createElement('StandardPrice', '');
            $currency = $xml->createAttribute('currency');
            $currencyValue = $xml->createTextNode($configCurrency);
            $currency->appendChild($currencyValue);
            $standardPrice->appendChild($currency);

            $standardPrice->appendChild($xml->createTextNode($product['price']));
            $price->appendChild($standardPrice);

            $message->appendChild($price);
            $amazonEnvelope->appendChild($message);
        }

        $xmlContent = $xml->saveXML();

        return $xmlContent;
    }

    /**
     * Build image feed
     * The Image feed allows you to upload various images for a product. Amazon can display several images for each
     * product. It is in your best interest to provide several high-resolution images for each of your products so customers
     * can make informed buying decisions
     * 
     * @param string $merchantId
     * @param array $products
     * @return string $feed
     * 
     */
    public function buildImageFeed($merchantId, $products) {

        /**
         * Used to identify an individual product.
         * Each product must have a SKU, and each SKU must be unique
         */
        $sku;

        /**
         * The type of image (Main, Alternate, or Swatch)
         */
        $imageType;

        /**
         * The exact location of the image using a full URL
         */
        $imageLocation;

        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('ProductImage');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        $i = 0;
        foreach ($products as $product) {

            $images = $product->getMediaGalleryImages();

            if(count($images) == 0){
                $product = Mage::getModel('catalog/product')->load($product->getId());
                $images = $product->getMediaGalleryImages();
            }

            $config = Mage::getModel('MarketPlace/Configuration')->load('amazon', 'mpc_marketplace_id');
            if($config->getmpc_id()){
                $params = unserialize($config->getmpc_params());

                switch($params['imageSource']){

                    case 'parent':

                        $productParentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
                            ->getParentIdsByChild($product->getentity_id());

                        if (count($productParentIds) > 0) {
                            // on recupere les images du parent
                            $parent = Mage::getModel('catalog/product')->load($productParentIds[0]);
                            $images = $parent->getMediaGalleryImages();
                        }

                        break;
                    case 'simple':
                    default:
                        break;

                }
            }

            $j = 0;
            foreach ($images as $imageObj) {

                $j++;
                $i++;
                // pas plus de 9 images
                if ($j > 9)
                    continue;

                $message = $xml->createElement('Message', '');

                $messageId = $xml->createElement('MessageID', $i);
                $message->appendChild($messageId);

                $operationType = $xml->createElement('OperationType', 'Update');
                $message->appendChild($operationType);

                $productImage = $xml->createElement('ProductImage', '');
                $message->appendChild($productImage);

                $sku = $xml->createElement('SKU', $product->getsku());
                $productImage->appendChild($sku);

                if ($j == 1) {
                    $imageType = 'Main';
                } else {
                    $cpt = $j - 1;
                    $imageType = 'PT' . $cpt;
                }

                $imageType = $xml->createElement('ImageType', $imageType);
                $productImage->appendChild($imageType);

                $url = $imageObj->geturl();
                $url = str_replace('https', 'http', $url);
                $imageLocation = $xml->createElement('ImageLocation', $url);
                $productImage->appendChild($imageLocation);

                $amazonEnvelope->appendChild($message);
            }
        }

        $feed = $xml->saveXML();

        return $feed;
    }

    /**
     * Build relationship feed
     * 
     * @param string $merchantId
     * @param array $relationship
     * @return string $feed
     */
    public function buildRelationshipFeed($merchantId, $relationship) {

        /**
         * The master SKU for a product with variations
         */
        $parentSku;

        /**
         * Used to identify an individual product, one (child) variation of the parent SKU
         */
        $sku;

        /**
         * Type of relationship, variation or accessory
         */
        $type;

        $xml = new DomDocument('1.0', 'utf-8');
        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Relationship');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        $i = 1;
        foreach ($relationship as $parentId => $tab) {

            $messageNode = $xml->createElement('Message', '');
            $messageIdNode = $xml->createElement('MessageID', $i);
            $operationTypeNode = $xml->createElement('OperationType', 'Update');

            $messageNode->appendChild($messageIdNode);
            $messageNode->appendChild($operationTypeNode);

            $relationshipNode = $xml->createElement('Relationship');

            $parentSkuNode = $xml->createElement('ParentSKU', $tab['parentsku']);
            $relationshipNode->appendChild($parentSkuNode);

            foreach ($tab['childs'] as $childSku) {

                $relationNode = $xml->createElement('Relation', '');
                $skuNode = $xml->createElement('SKU', $childSku);
                $typeNode = $xml->createElement('Type', 'Variation');
                $relationNode->appendChild($skuNode);
                $relationNode->appendChild($typeNode);
                $relationshipNode->appendChild($relationNode);
            }

            $messageNode->appendChild($relationshipNode);
            $amazonEnvelope->appendChild($messageNode);
        }

        $feed = $xml->saveXML();

        return $feed;
    }

    /**
     * Build override feed
     * 
     * @todo : implement override feed
     */
    public function buildOverrideFeed() {

        /**
         * Used to identify an individual product.
         * Each product must have a SKU, and each SKU must be unique
         */
        $sku;

        /**
         * Locale and shipping service
         */
        $shipOption;

        /**
         * Indicates whether the SKU can or cannot be shipped to the
         * specified locale using the specified shipping service (ShipOption)
         */
        $isShippingRestricted;

        /**
         * The type of override shipping charge
         * (Additive or Exclusive) being applied to the SKU
         */
        $type;

        /**
         * The Additive or Exclusive shipping charge amount
         */
        $shipAmount;
    }

    /**
     * Build order adjustment feed
     * 
     * @todo : implement order adjustment feed
     */
    public function setOrderAdjustment() {
        
    }

    /**
     * Get settlement report
     * 
     * @todo : implement settlement report feed
     */
    public function getSettlementReport() {
        
    }

    /**
     * Build ordre aknowledgment
     * 
     * @todo : not use actually
     * @param array $orders
     * @param string $merchantId
     * @return string $feed 
     */
    public function buildOrderAcknowledgment($orders, $merchantId) {

        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');
        $xmlns = $xml->createAttribute('xmlns:xsi');
        $xmlns->appendChild($xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance'));

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlns);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $documentVersion = $xml->createElement('DocumentVersion', '1.01');
        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier', $merchantId);
        $header->appendChild($merchantIdentifier);

        $amazonEnvelope->appendChild($header);

        $messageType = $xml->createElement('MessageType', 'OrderAcknowledgment');
        $amazonEnvelope->appendChild($messageType);

        $i = 0;
        foreach ($orders as $order) {

            $i++;
            $message = $xml->createElement('Message', '');
            $messageId = $xml->createElement('MessageID', $i);
            $message->appendChild($messageId);

            $orderAcknowledgment = $xml->createElement('OrderAcknowledgment', '');

            $amazonOrderId = $xml->createElement('AmazonOrderID', $order['amazonOrderId']);
            $orderAcknowledgment->appendChild($amazonOrderId);

            if ($order['merchantOrderId'] != "") {
                $merchantOrderId = $xml->createElement('MerchantOrderId', $order['merchantOrderId']);
                $orderAcknowledgment->appendChild($merchantOrderId);
            }

            $statusCode = $xml->createElement('StatusCode', $order['statusCode']);
            $orderAcknowledgment->appendChild($statusCode);

            foreach ($order['items'] as $item) {

                $item = $xml->createElement('Item', '');

                $amazonOrderItemCode = $xml->createElement('AmazonOrderItemCode', $item['amazonOrderItemCode']);
                $item->appendChild($amazonOrderItemCode);

                if ($item['merchantOrderItemID'] != "") {
                    $merchantOrderItemID = $xml->createElement('MerchantOrderItemID', $item['merchantOrderItemID']);
                    $item->appendChild($merchantOrderItemCode);
                }

                $orderAcknowledgment->appendChild($item);
            }

            $message->appendChild($orderAcknowledgment);
            $amazonEnevelope->appendChild($message);
        }

        $xmlContent = $xml->saveXML();

        return $xmlContent;
    }

    /**
     * Build flat tracking file
     * 
     * @param array $orderIds
     * @return string $retour 
     * @todo : is it still used
     */
    public function buildFlatTrackingFile($orderIds) {

        $lineReturn = "\r\n";
        $retour = 'order-id	order-item-id	quantity	ship-date	carrier-code	carrier-name	tracking-number	ship-method' . $lineReturn;

        if (!$orderIds)
            return $retour;

        foreach ($orderIds as $orderId) {
            //init vars
            $lineModel = 'order_ref	order_item_id	qty	ship_date	carrier_code	carrier_name	tracking	ship_method';
            $order = mage::getModel('sales/order')->load($orderId);

            //retrieve shipment
            $shipment = null;
            foreach ($order->getShipmentsCollection() as $item) {
                $shipment = $item;
                break;
            }

            if ($shipment == null) {
                continue;
            }

            //add items
            foreach ($order->getAllItems() as $item) {
                $line = $lineModel;
                $line = str_replace('order_ref', $order->getmarketplace_order_id(), $line);
                $line = str_replace('order_item_id', $item->getmarketplace_item_id(), $line);
                $line = str_replace('qty', (int) $item->getqty_ordered(), $line);

                $shipDate = date('Y-m-d', strtotime($shipment->getcreated_at()));
                $line = str_replace('ship_date', $shipDate, $line);

                $line = str_replace('carrier_code', 'Other', $line);
                $line = str_replace('carrier_name', 'La Poste', $line);

                $tracking = '';
                foreach ($order->getTracksCollection() as $track) {
                    if (is_object($track->getNumberDetail()))
                        $tracking = $track->getNumberDetail()->gettracking();
                }

                $line = str_replace('tracking', $tracking, $line);
                $line = str_replace('ship_method', 'Colissimo', $line);

                $retour .= $line . $lineReturn;
            }
        }

        return $retour;
    }

    /**
     * Build XML tracking file
     * 
     * @param array $ordersIds
     * @param string $merchantId
     * @return string $xmlContent 
     */
    public function buildXMLTrackingFile($ordersIds, $merchantId) {

        $carrierCode = array();

        $content = file_get_contents(Mage::Helper('Amazon/XSD_Main')->getFilename('amzn-base.xsd'));
        $xmlBase = new DomDocument();
        $xmlBase->loadXML($content);

        $root = $xmlBase->documentElement;

        foreach ($root->getElementsByTagName('element') as $elt) {

            if ($elt->hasAttribute('name') && $elt->getAttribute('name') == 'CarrierCode') {

                $carrierNode = $elt;
                break;
            }
        }

        foreach ($carrierNode->getElementsByTagName('enumeration') as $enum) {

            $code = $enum->getAttribute('value');
            $carrierCode[$code] = strtolower($code);
        }

        $xml = new DomDocument();
        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');
        $xmlns = $xml->createAttribute('xmlns:xsi');
        $xmlns->appendChild($xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance'));

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlns);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $documentVersion = $xml->createElement('DocumentVersion', '1.01');
        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier', $merchantId);
        $header->appendChild($merchantIdentifier);

        $amazonEnvelope->appendChild($header);

        $messageType = $xml->createElement('MessageType', 'OrderFulfillment');
        $amazonEnvelope->appendChild($messageType);

        $i = 0;
        foreach ($ordersIds as $orderId) {

            // retrieve order
            $order = mage::getModel('sales/order')->load($orderId);

            //retrieve shipment
            $shipment = null;
            foreach ($order->getShipmentsCollection() as $item) {
                $shipment = $item;
                break;
            }

            if ($shipment == null) {
                continue;
            }

            $tracking = Mage::Helper('MarketPlace/Tracking')->getTrackingForOrder($order);

            if ($tracking == '')
                continue;

            $i++;
            $message = $xml->createElement('Message', '');

            $messageId = $xml->createElement('MessageID', $i);
            $message->appendChild($messageId);

            $orderFulfillment = $xml->createElement('OrderFulfillment', '');

            $orderRef = $order->getmarketplace_order_id();
            $amazonOrderId = $xml->createElement('AmazonOrderID', $orderRef);
            $orderFulfillment->appendChild($amazonOrderId);

            // (opt, depend of matching in orderAcknowledgment)
            //$merchantOrderId = $xml->createElement('MerchanrOrderID', $orderRef);
            //$orderFulfillment->appendChild($merchantOrderId);
            // merchant shipment id (not used by amazon)
            $fulfillmentId = $shipment->getentity_id();
            $merchantfulfillmentOrderId = $xml->createElement('MerchantFulfillmentID', $fulfillmentId);
            $orderFulfillment->appendChild($merchantfulfillmentOrderId);

            $shipDate = date("Y-m-d\TH:i:s", strtotime($shipment->getcreated_at()));
            $fulfillmentDate = $xml->createElement('FulfillmentDate', $shipDate);
            $orderFulfillment->appendChild($fulfillmentDate);

            $fulfillmentData = $xml->createElement('FulfillmentData', '');

            $shipping_method = Mage::registry('mp_country')->getParam('default_shipment_method');
            $shipping_method_title = mage::helper('MarketPlace')->getShippingMethodTitle($shipping_method);
            
            //$shipping_method_title = $order->getshipping_description();

            $merchantCarrierCode = strtolower($shipping_method_title);
            if (in_array($merchantCarrierCode, $carrierCode)) {
                $code = $carrierCode[$merchantCarrierCode];
                $carrierCode = $xml->createElement('CarrierCode', $code);
                $fulfillmentData->appendChild($carrierCode);
            }

            $carrierName = $xml->createElement('CarrierName', $shipping_method_title);
            $fulfillmentData->appendChild($carrierName);

            $shippingMethod = $xml->createElement('ShippingMethod', $shipping_method_title);
            $fulfillmentData->appendChild($shippingMethod);

            $shipperTrackingNumber = $xml->createElement('ShipperTrackingNumber', $tracking);
            $fulfillmentData->appendChild($shipperTrackingNumber);

            $orderFulfillment->appendChild($fulfillmentData);

            foreach ($order->getAllItems() as $item) {

                $itemNode = $xml->createElement('Item', '');

                $amazonOrderItemCode = $xml->createElement('AmazonOrderItemCode', $item->getmarketplace_item_id());
                $itemNode->appendChild($amazonOrderItemCode);

                //$merchantOrderItemId = $xml->createElement('MerchantOrderItemID', $item->getmarketplace_item_id());
                //$item->appendChild($merchantOrderItemId);

                /* $merchantFulfillmentItemId = $xml->createElement('MerchantFulfillmentItemID', '');
                  $itemNode->appendChild($merchantFulfillmentItemId); */

                $quantity = $xml->createElement('Quantity', (int) $item->getqty_ordered());
                $itemNode->appendChild($quantity);

                $orderFulfillment->appendChild($itemNode);
            }

            $message->appendChild($orderFulfillment);
            $amazonEnvelope->appendChild($message);
        }

        $xmlContent = $xml->saveXML();
        return $xmlContent;
    }

    /**
     * Accept / cancel orders
     * 
     * @param array $outOfStock
     * @param string $merchantId
     * @return string
     */
    public function buildXMLAcceptOrdersFile($outOfStock, $merchantId) {

        $xml = new DomDocument();

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');
        $xmlns = $xml->createAttribute('xmlns:xsi');
        $xmlns->appendChild($xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance'));

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlns);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $documentVersion = $xml->createElement('DocumentVersion', '1.01');
        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier', $merchantId);
        $header->appendChild($merchantIdentifier);

        $amazonEnvelope->appendChild($header);

        $messageType = $xml->createElement('MessageType', 'OrderAcknowledgement');
        $amazonEnvelope->appendChild($messageType);

        $effectiveDate = $xml->createElement('EffectiveDate', date('Y-m-d\TH:i:s'));
        $amazonEnvelope->appendChild($effectiveDate);

        $messageNode = $xml->createElement('Message', '');
        $i = 1;
        foreach ($outOfStock as $amazonOrderId => $data) {

            $messageIdNode = $xml->createElement('MessageID', $i);
            $messageNode->appendChild($messageIdNode);

            $operationTypeNode = $xml->createElement('OperationType', 'Update');
            $messageNode->appendChild($operationTypeNode);

            $orderAcknowledgementNode = $xml->createElement('OrderAcknowledgement', '');

            $amazonOrderIdNode = $xml->createElement('AmazonOrderID', $amazonOrderId);
            $orderAcknowledgementNode->appendChild($amazonOrderIdNode);

            if (count($data['products']) == 0) {

                // acceptation de toute la commande
                $statusCodeNode = $xml->createElement('StatusCode', 'Success');
                $orderAcknowledgementNode->appendChild($statusCodeNode);
            } else {

                // annulation de toute la commande
                if ($data['all'] == true) {
                    $statusCodeNode = $xml->createElement('StatusCode', 'Failure');
                    $orderAcknowledgementNode->appendChild($statusCodeNode);
                } else {

                    // annulation des produits out of stock
                    $statusCodeNode = $xml->createElement('StatusCode', 'Failure');
                    $orderAcknowledgementNode->appendChild($statusCodeNode);

                    foreach ($data['products'] as $amazonOrderItemId) {

                        $itemNode = $xml->createElement('Item', '');

                        $amazonOrderItemCodeNode = $xml->createElement('AmazonOrderItemCode', $amazonOrderItemId);
                        $itemNode->appendChild($amazonOrderItemCodeNode);

                        $cancelReasonNode = $xml->createElement('CancelReason', 'NoInventory');
                        $itemNode->appendChild($cancelReasonNode);

                        $orderAcknowledgementNode->appendChild($itemNode);
                    }
                }
            }

            $messageNode->appendChild($orderAcknowledgementNode);
            $amazonEnvelope->appendChild($messageNode);

            $i++;
        }

        $amazonEnvelope->appendChild($messageNode);

        return $xml->saveXML();
    }

}
