<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
abstract class MDN_Amazon_Helper_MWS_Abstract extends Mage_Core_Helper_Abstract {
    
    const kSignatureMethod = 'HmacSHA256';
    const kSignatureVersion = '2';
    
    /* @var array */
    protected $_params = array();
    /* @var MDN_MarketPlace_Model_Countries */
    protected $_country = null;
    /* @var MDN_MarketPLace_Model_Account */
    protected $_account = null;
    /* @var string */
    private $_stringToSign = '';
    
    /**
     * Add marketplace id : key depends on operation type... 
     */
    protected function _addMarketPlaceId(){
        
        $this->_params['MarketplaceId'] = $this->_country->getParam('marketplaceId');

    }
    
    /**
     * getVersion 
     */
    abstract public function getVersion(); 
    
    /**
     * getUri 
     */
    abstract public function getUri();
    
    /**
     * getBeforeQuery 
     */
    abstract protected function _getBeforeQuery();
    
    /**
     * Get account
     * 
     * @return MDN_MarketPlace_Model_Account 
     */
    public function getAccount(){
        
        if(!$this->_account){
            
            $this->_account = Mage::getModel('MarketPlace/Accounts')->load($this->_country->getmpac_account_id());
            
        }
        
        return $this->_account;
        
    }
    
    /**
     * Get base url
     * 
     * @return string $retour
     * @throws Exception 
     */
    protected function _getBaseUrl(){
        
        $retour = '';
                
        switch($this->_country->getmpac_country_code()){
            
            case 'CA':
                $retour = 'mws.amazonservices.ca';
                break;
            case 'CN':
                $retour = 'mws.amazonservices.com.cn';
                break;
            case 'DE':
                $retour = 'mws.amazonservices.de';
                break;
            case 'ES':
                $retour = 'mws.amazonservices.es';
                break;
            case 'FR':
                $retour = 'mws.amazonservices.fr';
                break;
            case 'IN':
                $retour = 'mws.amazonservices.in';
                break;
            case 'IT':
                $retour = 'mws.amazonservices.it';
                break;
            case 'JP':
                $retour = 'mws.amazonservices.jp';
                break;
            case 'UK':
            case 'GB':
                $retour = 'mws.amazonservices.co.uk';
                break;
            case 'US':
                $retour = 'mws.amazonservices.com';
                break;
            case 'MX':
                $retour = 'mws.amazonservices.com.mx';
                break;
        }
        
        if(!$retour)
            throw new Exception(Mage::Helper('Amazon')->__('Unknow country code : %s in %s', $this->_country->getmpac_country_code(), __METHOD__));
        
        return $retour;
    }        
    
    /**
     * Reset params     
     */
    protected function _reset(){
        $this->_country = null;
        $this->_account = null;
        $this->_params = array();
        $this->_stringToSign = '';
    }
    
    /**
     * Init query
     * 
     * @param array $params 
     */
    protected function _init($params, $flag = true){
        
        $this->_reset();       
        
        $this->_country = Mage::registry('mp_country');
        $this->_account = Mage::registry('mp_account');
        
        // certaines requetes ne nécessitent pas ce paramètre
        if($flag === true)
            $this->_addMarketPlaceId();    
            
        $this->_params['AWSAccessKeyId'] = $this->getAccount()->getParam('mws_accesskeyid');
        $this->_params['SellerId'] = $this->getAccount()->getParam('mws_merchantid');           
        $this->_params['version'] = $this->getVersion();        
        $this->_params['SignatureMethod'] = self::kSignatureMethod;
        $this->_params['SignatureVersion'] = self::kSignatureVersion;
        
        $timestamp = Mage::getModel('core/date')->timestamp();
        
        // check timeshift
        $config = Mage::getModel('MarketPlace/Configuration')->getConfiguration($this->_account->getmpa_mp());
        if($config->gettimeShift() != "")            
            $timestamp += ($config->gettimeShift());
                    
        $this->_params['Timestamp'] = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", $timestamp);
                
        // add parameters
        foreach($params as $k => $v){
                $this->_params[$k] = $v;
        }

        // sort array parameters
        uksort($this->_params, 'strcmp');

        // add signature
        $this->_params['Signature'] = $this->_calculSignature();
        
    }
    
    /**
     * Calcul signature
     * 
     * @return string $signature 
     */
    protected function _calculSignature(){        
        
        $signature = '';
        
        // set http method to use
        $method = 'POST';
        
        // url encode parameters names and values
        foreach($this->_params as $param => $value){

            $value = str_replace('%7E', '~', rawurlencode($value));            
            $queryParams[] = $param."=".$value;
            
        }

        // construct query string
        $query = implode('&', $queryParams);

        // construct signToString
        $this->_stringToSign = $method."\n";
        $this->_stringToSign .= $this->_getBaseUrl()."\n";                        
        $this->_stringToSign .= $this->_getBeforeQuery()."\n";                
        $this->_stringToSign .= $query;        
        
        // HMAC $signToString with $secretAccessKey and convert result to base64
        $signature = base64_encode(hash_hmac("sha256", $this->_stringToSign, $this->getAccount()->getParam('mws_secretkey'), true));

        // add signature
        return $signature;
        
    }
    
    /**
     * Send a MWS query (without feed)
     * 
     * @param array $params
     * @return Zend_Http_Response 
     */
    public function query($params, $flag = true){
        
        // init
        $this->_init($params, $flag);
        // request
        return $this->_sendRequest();
        
    }
    
    /**
     * Send feed
     * 
     * @param array $params
     * @param string $filename
     * @return Zend_Http_Response 
     */
    public function sendFeed($params, $filename){

        $this->_init($params);

        return $this->_sendRequest($filename);
    }
    
    /**
     * Send request
     * 
     * @param string $filename
     * @return Zend_Http_Response $response 
     */
    protected function _sendRequest($filename = ''){
        
        $response = '';
        $headers = array(
            'User-Agent' => 'MDN Tool/1.0 (Language=PHP/'.phpversion().'; Platform=Linux/Gentoo)'
        );
        
        $client = new Zend_Http_Client();
        
        if($filename != ''){
            // read uploaded file
            $handle = fopen($filename, 'r');
            $xml = stream_get_contents($handle);
            fclose($handle);

            // build Content-MD5 header
            $contentMD5 = base64_encode(md5_file($filename, true));
            $headers['Content-Type'] = 'text/xml; charset=iso-8859-1';
            $headers['Content-MD5'] = $contentMD5;
            $headers['Transfert-Encoding'] = 'chunked';            

            $client->setRawData($xml);
   
        }
        
        // ajout des headers
        $client->setHeaders($headers);
        $client->setUri($this->getUri());
        
        $client->setConfig(array(
                'maxredirects' => 0,
                'timeout'      => 30
            )
        );        
        
        // add parameters
        $client->setParameterGet($this->_params);
        
        // send request
        $response = $client->request('POST');     
        
        // log last request (for debug)
        if(Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getlogLastRequest() == 1){
            
            $lastRequest = $client->getLastRequest();
            $lastRequest = str_replace('&', "\n&", $lastRequest);
            
            mage::getModel('MarketPlace/Logs')->addLog(
                Mage::helper('Amazon')->getMarketPlaceName(), 
                MDN_MarketPlace_Model_Logs::kNoError, 
                htmlspecialchars($lastRequest), 
                MDN_MarketPlace_Model_Logs::kScopeMisc, 
                array('fileName' => NULL), 
                0
            );
            
        }
        
        // add log if an global error occures
        if($response->getStatus() != 200){
            
            $message = '';
            
            $xml = new DomDocument();
            $xml->loadXML($response->getBody());
            
            if($xml->getElementsByTagName('Error')->item(0)){
                    
                $errorNode = $xml->getElementsByTagName('Error')->item(0);

                // retrieve error type
                if($errorNode->getElementsByTagName('Type')->item(0)){
                    $message .= $errorNode->getElementsByTagName('Type')->item(0)->nodeValue.' : ';
                }

                // retrieve error code
                if($errorNode->getElementsByTagName('Code')->item(0)){                        
                    $message .= $errorNode->getElementsByTagName('Code')->item(0)->nodeValue.', ';                        
                }

                // retrieve error message
                if($errorNode->getElementsByTagName('Message')->item(0)){                        
                    $message .= $errorNode->getElementsByTagName('Message')->item(0)->nodeValue;                        
                }
            }           
            
            if(Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_stack_trace() == 1)
                $message .= "\n\n".str_replace(array("\n", '&'), array('<br/>', '<br/>&'),$this->_stringToSign);
            
            throw New Exception($message);         
            
        }

        //sleep if configured
        if ($sleep = Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getfeed_sleep())
        {
            $sleep = ($sleep > 10 ? 10 : $sleep);
            sleep($sleep);
        }

        return $response;
        
    }
    
    /**
     * Get scope : used in log messages
     * 
     * @return string $scope 
     */
    protected function _getScope(){
        
        $scope = '';
        
        switch($this->_params['Action']){
            
           case 'GetMatchingProductForId':
               $scope = MDN_MarketPlace_Model_Logs::kScopeCreation;
               break;
           
           case 'SubmitFeed':
               
               switch($this->_params['FeedType']){
                
                case '_POST_PRODUCT_DATA_':
                    $scope = MDN_MarketPlace_Model_Logs::kScopeCreation;
                    break;
                
                }
                break;
            case 'RequestReport':
                if(preg_match('#ORDER#', $this->_params['ReportType']))
                        $scope = MDN_MarketPlace_Model_Logs::kScopeOrders;
                break;
                              
        }                
        
        return $scope;
        
    }
    
} 
