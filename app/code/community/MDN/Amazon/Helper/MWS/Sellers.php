<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_MWS_Sellers extends MDN_Amazon_Helper_MWS_Abstract {

    const kVersion = '2011-07-01';
    
    /**
     * Get before query
     * 
     * @return string 
     */
    protected function _getBeforeQuery() {
        return '/Sellers/'.$this->getVersion();
    }

    /**
     * Get uri
     * 
     * @return string 
     */
    public function getUri() {
        return ('https://'.$this->_getBaseUrl().$this->_getBeforeQuery());
    }

    /**
     * Get version
     * 
     * @return string 
     */
    public function getVersion() {
        return self::kVersion;
    }
    
    /**
     * List marketplace participations
     * 
     * @return Zend_Http_Response 
     */
    public function listMarketplaceParticipations(){
        return $this->query(array('Action' => 'ListMarketplaceParticipations'), false);        
    }

}
