<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_MWS_Feeds extends MDN_Amazon_Helper_MWS_Abstract {

    const kVersion = '2009-01-01';
    
    /**
     * Get uri
     * 
     * @return string 
     */
    public function getUri(){
        return 'https://'.$this->_getBaseUrl().$this->_getBeforeQuery();
    }

    /**
     * Get before query
     * 
     * @return string 
     */
    protected function _getBeforeQuery() {
        return '/';
    }

    /**
     * Get version
     * 
     * @return string 
     */
    public function getVersion() {
        return self::kVersion;
    }
    
    /**
     * Add marketplace id 
     */
    protected function _addMarketPlaceId(){
        
        $this->_params['MarketplaceIdList.Id.1'] = $this->_country->getParam('marketplaceId');
        
    }
    
    /**
     * Submit feed
     * 
     * @param string $type
     * @param string $filename
     * @return Zend_Http_Response 
     */
    public function submitFeed($type, $filename){
        
        return $this->sendFeed(array('Action' => 'SubmitFeed', 'FeedType' => $type), $filename);
        
    }
    
    /**
     * Get feed submission result
     *
     * @param int $feedSubmissionId
     * @return string
     */
    public function getFeedSubmissionResult($feedSubmissionId) {

        $params = array(
            'Action' => 'GetFeedSubmissionResult',
            'FeedSubmissionId' => $feedSubmissionId
        );

        return $this->query($params)->getBody();
    }
    
    /**
     * Get feed submission list
     *
     * @return string
     */
    public function getFeedSubmissionList() {

        $params = array(
            'Action' => 'GetFeedSubmissionList',
            'FeedTypeList.Type.1' => '_POST_PRODUCT_DATA_',
            'FeedTypeList.Type.2' => '_POST_PRODUCT_PRICING_DATA_',
            'FeedTypeList.Type.3' => '_POST_INVENTORY_AVAILABILITY_DATA_'
        );

        return $this->query($params, false)->getBody();
    }    

    /**
     * Get feed submission count
     *
     * @return string
     */
    public function getFeedSubmissionCount() {

        $params = array(
            'Action' => 'GetFeedSubmissionCount',
            'FeedTypeList.Type.1' => '_POST_PRODUCT_DATA_',
            'FeedTypeList.Type.2' => '_POST_PRODUCT_PRICING_DATA_',
            'FeedTypeList.Type.3' => '_POST_INVENTORY_AVAILABILITY_DATA_',
            'FeedProcessingStatus.Status.1' => '_DONE_',
            'FeedProcessingStatus.Status.2' => '_SUBMITTED_'
        );

        return $this->query($params, false)->getBody();
    }

}
