<?php

class MDN_Amazon_Helper_MWS_TimeShift extends Mage_Core_Helper_Abstract {

    public function update()
    {
        //request
        $url = 'https://mws.amazonservices.com/';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($curl);
        if (!$content)
            $content = file_get_contents($url);

        //analyse xml content
        $xmlTimestamp = new DomDocument();
        $xmlTimestamp->loadXML($content);
        if($xmlTimestamp->getElementsByTagName('Timestamp')->item(0)){

            $amazonTimestampNode = $xmlTimestamp->getElementsByTagName('Timestamp')->item(0);
            $amazonTimestamp = $amazonTimestampNode->getAttribute('timestamp');
            $tmp = explode('T', $amazonTimestamp);
            $amazonTime = trim($tmp[1]);
            $tmp = explode(':', $amazonTime);

            $amazonHour = (int)trim($tmp[0]);
            $amazonMinutes = (int)trim($tmp[1]);

            $serverHour = date('H', Mage::getSingleton('core/date')->timestamp());
            $serverMinutes = date('i', Mage::getSingleton('core/date')->timestamp());

            $amazonSeconds = $amazonHour * 3600 + $amazonMinutes * 60;
            $serverSeconds = $serverHour * 3600 + $serverMinutes * 60;
            $deltaSeconds = $amazonSeconds - $serverSeconds;

            // save in config
            $config = Mage::getModel('MarketPlace/Configuration')->load('amazon', 'mpc_marketplace_id');
            if($config->getmpc_id()){
                $params = unserialize($config->getmpc_params());
                $params['timeShift'] = $deltaSeconds;
                $config->setmpc_params($params);
                $config->save();
            }else{
                // new install, add entry in database
                $config = Mage::getModel('MarketPlace/Configuration')
                    ->setmpc_marketplace_id('amazon')
                    ->setmpc_params(array('timeShift' => $deltaSeconds, 'max_to_export' => 500, 'logLastRequest' => 0, 'orderRange' => 7))
                    ->save();
            }

        }

        return $deltaSeconds;

    }

}