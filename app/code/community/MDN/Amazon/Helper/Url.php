<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 * @deprecated since version 2
 */

class MDN_Amazon_Helper_Url extends Mage_Core_Helper_Abstract {

    /**
     * get extension
     * 
     * @return string
     * @throws Exception 
     * @deprecated
     */
    public function getExt() {

        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*$value = Mage::getStoreConfig('amazon/general/web_service_url');
        
        if(!$value){
            throw new Exception($this->__('Check webservice url configuration in System > Configuration > Marketplace'));
        }

        $retour = '';

        $ext = array(
            'CA' => '.ca',
            'CN' => '.com.cn',
            'DE' => '.de',
            'ES' => '.es',
            'FR' => '.fr',
            'IT' => '.it',
            'JP' => '.jp',
            'UK' => '.co.uk',
            'US' => '.com'
        );

        $retour = array_key_exists($value, $ext) ? $ext[$value] : '.fr';

        return $retour;*/
    }

    /**
     * Get Amazon Product Uri
     *
     * @param string $country
     * @param string $asin
     * @return string
     */
    public function getAmazonProductUri($country, $asin){

        switch($country){
            case 'mx':
                $country = 'com.mx';
                break;
            case 'cn':
                $country = 'com.cn';
                break;
            case 'uk':
            case 'gb':
                $country = 'co.uk';
                break;
            case 'us':
                $country = 'com';
                break;
        }

        return 'http://www.amazon.'.$country.'/gp/product/'.$asin;

    }

}
