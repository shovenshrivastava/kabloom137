<?php

/**
 * Gestion des attributs facultatifs
 * 
// PAR CATEGORIE
- material : AutoAccessory, Home, PetSupplies, Sports
- exact_material
- style
- features
- Length width
- Attachment pattern
- sleeve_type
- sports
- multipack
- Design made
- neckline
- fastening
- Used
- rise
- sock_length
- long_johntype
- sleeve_length
- includes
- garment_care
- clothing_type
- sportswear_type
  
// GENERAL 
- bullet_point
- bullet_point1
- bullet_point2
- bullet_point3
- bullet_point4
- search_terms1
- search_terms2
- search_terms3
- search_terms4
- exact_colour
- size_map
  
// AJOUTER UN ATTRIBUT DANS LA CONFIG
- browesnodes
 */
abstract class MDN_Amazon_Helper_Fields_Optionnals_Abstract extends Mage_Core_Helper_Abstract {
    
    /**
     * Get field value
     * 
     * @param string $name
     * @param Mage_Catalog_Model_Product $product
     * @return string $retour
     */
    public function getValueForField($name, $product){
    
        $retour = '';
        
        $entry = Mage::getModel('Amazon/OptionnalsFields')->getCollection()
                ->addFieldToFilter('aof_name',$name)
                ->getFirstItem();

        if($entry->getId()){
            if($product->getAttributeText($entry->getaof_attribute_code())){
                $retour = $product->getResource()->getAttribute($entry->getaof_attribute_code())->setStoreId($product->getStoreId())->getFrontend()->getValue($product);
            }else{
                if($product->getData($entry->getaof_attribute_code())){
                    $retour = $product->getData($entry->getaof_attribute_code());
                }
            }
        }
        
        // SizeMap
        /*if($name == 'SizeMap' && $product->getData('size_map')){
            $retour = $product->getData('size_map');
        }
        
        // SleeveLength
        if($name == 'SleeveLength' && $product->getData('sleeve_length')){
            $retour = $product->getData('sleeve_length');
        }
        
        // SleeveType
        if($name == 'SleeveType' && $product->getData('sleeve_type')){
            $retour = $product->getData('sleeve_type');
        }
        
        // Style
        if($name == 'Style' && $product->getData('style')){
            $retour = $product->getData('style');
        }
        
        // Features
        if($name == 'Features' && $product->getData('features')){
            $retour = $product->getData('features');
        }
        
        // Pattern
        if($name == 'Pattern' && $product->getData('pattern')){
            $retour = $product->getData('pattern');
        }
        
        // Sport
        if($name == 'Sport' && $product->getData('sport')){
            $retour = $product->getData('sport');
        }
        
        // Design
        if($name == 'Design' && $product->getData('design')){
            $retour = $product->getData('design');
        }
        
        // OccasionAndLifestyle
        if($name == 'OccasionAndLifestyle' && $product->getData('occasion')){
            $retour = $product->getData('occasion');
        }
        
        // RiseStyle
        if($name == 'RiseStyle' && $product->getData('rise')){
            $retour = $product->getData('rise');
        }
        
        // FitType
        if($name == 'FitType' && $product->getData('fit')){
            $retour = $product->getData('fit');
        }
        
        // Material
        if($name == 'Material' && $product->getData('material')){
            $retour = $product->getData('material');
        }*/
        
        return $retour;
        
    }
    
    /**
     * Add optionnals fields
     * 
     * @param array $tab
     * @param Mage_Core_Catalog_Product $product
     */
    abstract function getOptionnalsFields($tab, $product);
    
}
