<?php

/**
 * Class MDN_Amazon_Helper_Fields_Optionnals_ProductClothing
 *
 * @author Nicolas Mugnier <nicolas@boostmyshop.com>
 */
class MDN_Amazon_Helper_Fields_Optionnals_ProductClothing extends MDN_Amazon_Helper_Fields_Optionnals_Abstract {
    
    /**
     * Add optionnals fields
     * 
     * @param array $tab
     * @param Mage_Core_Catalog_Product $product
     */
    public function getOptionnalsFields($tab, $product) {

        $mainOptionnalsTab = array();
        
        // chargement du XSD pour analyse
        $xsdHelper = Mage::Helper('Amazon/XSD_Main');
        $productXsd = $xsdHelper->getFilename('ProductClothing.xsd');
        $xml = new DomDocument();
        $xml->loadXML(file_get_contents($productXsd));
        
        // recuperation du noeud principal
        $classificationData = null;
        foreach($xml->getElementsByTagName('element') as $eltNode){
            
            if($eltNode->hasAttribute('name') && $eltNode->getAttribute('name') == 'ClassificationData'){                
                $classificationData = $eltNode;
                break;                
            }
            
        }
        
        // recuperation des attributs facultatifs globaux à cette catégorie
        if($classificationData !== null){
                        
            foreach($classificationData->getElementsByTagName('element') as $eltNode){
                
                if($eltNode->hasAttribute('name') && $eltNode->hasAttribute('minOccurs') && $eltNode->getAttribute('minOccurs') == 0){
                    
                    $name = $eltNode->getAttribute('name');
                    
                    $value = $this->getValueForField($name, $product);
                    if(!empty($value)){
                        if(is_array($value) && count($value) <= 0)
                            continue;
                     
                        $mainOptionnalsTab[$name] = $value;
                        
                    }
                    
                }
                
            }
            
        }
        
        if(count($mainOptionnalsTab) > 0){
            
            $index = null;
            // on retrouve la position de l'elt ClassificationData :
            foreach($tab['ProductData']['details'] as $k => $v){
                
                // pas de break, il faut mettre les champs optionnels à la suite des champs obligatoires !
                if(is_array($v) && array_key_exists('ClassificationData', $v)){
                    $index = $k;
                }
                
            }
            if($index !== null)
                foreach($mainOptionnalsTab as $name => $value)
                    $tab['ProductData']['details'][$index]['ClassificationData'][$name] = $value;
                
            
        }
        
        return $tab;
    }     
    
}
