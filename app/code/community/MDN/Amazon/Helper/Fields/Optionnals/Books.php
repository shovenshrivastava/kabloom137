<?php

/**
 * Class MDN_Amazon_Helper_Fields_Optionnals_Books
 *
 * @author Nicolas Mugnier <nicolas@boostmyshop.com>
 */
class MDN_Amazon_Helper_Fields_Optionnals_Books extends MDN_Amazon_Helper_Fields_Optionnals_Abstract
{

    /**
     * Add optionnals fields
     *
     * @param array $tab
     * @param Mage_Core_Catalog_Product $product
     */
    public function getOptionnalsFields($tab, $product)
    {

        $mainOptionnalsTab = array();

        // chargement du XSD pour analyse
        $xsdHelper = Mage::Helper('Amazon/XSD_Main');
        $productXsd = $xsdHelper->getFilename('Books.xsd');
        $xml = new DomDocument();
        $xml->loadXML(file_get_contents($productXsd));

        // recuperation du noeud principal
        $booksMisc = null;
        foreach ($xml->getElementsByTagName('element') as $eltNode) {

            if ($eltNode->hasAttribute('name') && $eltNode->getAttribute('name') == 'BooksMisc') {
                $booksMisc = $eltNode;
                break;
            }

        }

        // recuperation des attributs facultatifs globaux à cette catégorie
        if ($booksMisc !== null) {

            foreach ($booksMisc->getElementsByTagName('element') as $eltNode) {

                if ($eltNode->hasAttribute('name') && $eltNode->hasAttribute('minOccurs') && $eltNode->getAttribute('minOccurs') == 0) {

                    $name = $eltNode->getAttribute('name');

                    $value = $this->getValueForField($name, $product);
                    if (!empty($value)) {
                        if (is_array($value) && count($value) <= 0)
                            continue;

                        //$mainOptionnalsTab[$name] = $value;

                        $tab['ProductData']['details'][]['ProductType']['BooksMisc'][$name] = $value;

                    }

                }

            }

        }

        return $tab;
    }
}
