<?php

/**
 * Class MDN_Amazon_Helper_Fields_Optionnals_Sports
 *
 * @author Nicolas Mugnier <nicolas@boostmyshop.com>
 */
class MDN_Amazon_Helper_Fields_Optionnals_Sports extends MDN_Amazon_Helper_Fields_Optionnals_Abstract {    

    /**
     * Add optionnals fields
     * 
     * @param array $tab
     * @param Mage_Core_Catalog_Product $product
     */
    public function getOptionnalsFields($tab, $product) {

        $mainOptionnalsTab = array();
        
        // chargement du XSD pour analyse
        $xsdHelper = Mage::Helper('Amazon/XSD_Main');
        $productXsd = $xsdHelper->getFilename('Sports.xsd');
        $xml = new DomDocument();
        $xml->loadXML(file_get_contents($productXsd));
        
        // recuperation du noeud principal
        $sportNode = null;
        foreach($xml->getElementsByTagName('element') as $eltNode){
            
            if($eltNode->hasAttribute('name') && $eltNode->getAttribute('name') == 'Sports'){                
                $sportNode = $eltNode;
                break;                
            }
            
        }
        
        // recuperation des attributs optionnels des declinaisons
        $variationDataNode = null;
        foreach($xml->getElementsByTagName('element') as $eltNode){
            
            if($eltNode->hasAttribute('name') && $eltNode->getAttribute('name') == 'VariationData'){
                
                $variationDataNode = $eltNode;
                break;
                
            }
            
        }
        
        $variationOptionnalsFields = array();
        if($variationDataNode !== null){
            
            foreach($variationDataNode->getElementsByTagName('element') as $eltNode){
                
                if($eltNode->hasAttribute('name'))
                    $variationOptionnalsFields[] = $eltNode->getAttribute('name');
                
            }
            
        }
        
        // recuperation des attributs facultatifs globaux à cette catégorie
        if($sportNode !== null){
                        
            foreach($sportNode->getElementsByTagName('element') as $eltNode){
                
                if($eltNode->hasAttribute('name') && $eltNode->hasAttribute('minOccurs') && $eltNode->getAttribute('minOccurs') == 0){
                    
                    $name = $eltNode->getAttribute('name');
                    
                    if(!in_array($name, $variationOptionnalsFields)){
                        $value = $this->getValueForField($name, $product);
                        if(!empty($value)){
                            if(is_array($value) && count($value) <= 0)
                                continue;

                            $mainOptionnalsTab[$name] = $value;

                        }
                    }
                    
                }
                
            }
            
        }
        
        if(count($mainOptionnalsTab) > 0){
            
            foreach($mainOptionnalsTab as $name => $value)
                $tab['ProductData']['details'][] = array($name => $value);
            
        }
        
        return $tab;
    }     
}
