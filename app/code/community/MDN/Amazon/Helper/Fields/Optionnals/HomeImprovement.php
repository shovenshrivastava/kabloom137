<?php

/**
 * Class MDN_Amazon_Helper_Fields_Optionnals_HomeImprovement
 *
 * @author Nicolas Mugnier <nicolas@boostmyshop.com>
 */
class MDN_Amazon_Helper_Fields_Optionnals_HomeImprovement extends MDN_Amazon_Helper_Fields_Optionnals_Abstract {
    
    /**
     * Add optionnals fields
     * 
     * @param array $tab
     * @param Mage_Core_Catalog_Product $product
     */
    public function getOptionnalsFields($tab, $product) {

        $mainOptionnalsTab = array();
        $productTypeOptionnalsTab = array();
        
        // recuperation du product type
        $productType = Mage::helper('Amazon/CategoriesAssociation')->getDefaultProductType($product);
        
        // chargement du XSD pour analyse
        $xsdHelper = Mage::Helper('Amazon/XSD_Main');
        $productXsd = $xsdHelper->getFilename('HomeImprovement.xsd');
        $xml = new DomDocument();
        $xml->loadXML(file_get_contents($productXsd));
        
        // recuperation du noeud principal
        $autoAccessoryNode = null;
        foreach($xml->getElementsByTagName('element') as $eltNode){
            
            if($eltNode->hasAttribute('name') && $eltNode->getAttribute('name') == 'HomeImprovement'){                
                $autoAccessoryNode = $eltNode;
                break;                
            }
            
        }
        
        // recuperation des attributs facultatifs globaux à cette catégorie
        if($autoAccessoryNode !== null){
                        
            foreach($autoAccessoryNode->getElementsByTagName('element') as $eltNode){
                
                if($eltNode->hasAttribute('name') && $eltNode->hasAttribute('minOccurs') && $eltNode->getAttribute('minOccurs') == 0){
                    
                    $name = $eltNode->getAttribute('name');
                    
                    $value = $this->getValueForField($name, $product);
                    if(!empty($value)){
                        if(is_array($value) && count($value) <= 0)
                            continue;
                     
                        $mainOptionnalsTab[$name] = $value;
                        
                    }
                    
                }
                
            }
            
        }
        
        // recuperation des attributs facultatifs correspondants à ce product type
        if($productType){
                               
            $productTypeName = ($productType == 'Tools') ? 'HomeImprovementTools' : $productType;
            
            // recuperationdu noeud correspondant au product type
            $productTypeNode = null; 
            foreach($xml->getElementsByTagName('element') as $eltNode){
                
                if($eltNode->hasAttribute('name') && $eltNode->getAttribute('name') == $productTypeName){                    
                    $productTypeNode = $eltNode;
                    break;                    
                }
                
            }
            
            if($productTypeNode !== null){                                
                
                // recuperation des attributs facultatifs pour le product type
                foreach($productTypeNode->getElementsByTagName('element') as $eltNode){
                                        
                    if($eltNode->hasAttribute('name') && $eltNode->hasAttribute('minOccurs') && $eltNode->getAttribute('minOccurs') == 0){
                        
                        $name = $eltNode->getAttribute('name');
                        $value = $this->getValueForField($name, $product);
                        if(!empty($value)){
                            if(is_array($value) && count($value) <= 0)
                                continue;

                            $productTypeOptionnalsTab[$name] = $value;

                        }
                                                
                    }
                    
                }
                
            }
            
        }

        
        if(count($mainOptionnalsTab) > 0){
            
            foreach($mainOptionnalsTab as $name => $value)
                $tab['ProductData']['details'][] = array($name => $value);
            
        }
        
        if(count($productTypeOptionnalsTab) > 0){

            
            foreach($productTypeOptionnalsTab as $name => $value)
                $tab['ProductData']['details'][1]['ProductType'][$productType][$name] = $value;
            
        }
        
        return $tab;
    }        
}
