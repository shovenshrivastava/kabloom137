<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * Gestion des champs optionnels lors de la création de produits sur Amazon.
 * Catégories concernées :
 * <ul>
 * <li>Home</li>
 * <li>HomeImprovement</li>
 * <li>Sports</li>
 * <li>Office</li>
 * <li>AutoAccessory</li>
 * <li>PetSupplies</li>
 * <li>Shoes</li>
 * </ul>
 * 
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2.0.1
 */
class MDN_Amazon_Helper_Fields_Optionnals extends Mage_Core_Helper_Abstract {
    
    /**
     * Add optionnals fields
     * 
     * @param array $tab
     * @param Mage_Catalog_Model_Product $product
     * @return array $tab
     */
    public function getOptionnalsFields($tab, $product){
        
        $tmp = array();
        $descriptionDataNode = null;
        
        // recuperation de la liste des champs optionnels principaux
        $xsdHelper = Mage::Helper('Amazon/XSD_Main');
        $productXsd = $xsdHelper->getFilename('Product.xsd');
        $xml = new DomDocument();
        $xml->loadXML(file_get_contents($productXsd));
        
        // recherche de l'element contenant les champs facultatifs
        foreach($xml->getElementsByTagName('element') as $eltNode){

            if($eltNode->hasAttribute('name') && $eltNode->getAttribute('name') == 'DescriptionData'){
                $descriptionDataNode = $eltNode;
                break;
            }
            
        }
        
        if($descriptionDataNode !== null){
            foreach($descriptionDataNode->getElementsByTagName('element') as $eltNode){                    
                $name = $eltNode->getAttribute('name');                
                if(array_key_exists($name, $tab['descriptionData'])){
                    $tmp[$name] = $tab['descriptionData'][$name];
                }else{                    
                    $value = $this->getValueForField($name, $product);
                    if(!empty($value)){
                        if(is_array($value) && count($value) <= 0)
                            continue;
                        
                        $tmp[$name] = $value;
                    }
                }
            }
            $tab['descriptionData'] = $tmp;
        }
        // à ce stade le tableau tient comptes des champs optionnels communs à toutes les catégories
        // possédant une valueur dans Magento. Le tableau est trié
                
        // ajout des champs facultatifs par catégorie
        $category = $tab['ProductData']['name'];
        
        switch($category){
            
            case 'Clothing':
                $category = 'ProductClothing';
                break;
            
        }
        
        $allowedCategories = array(
            'shoes', // OK
            'sports', // OK
            'autoaccessory', // OK
            'productclothing', // OK
            'clothingaccessories', // OK
            'office', // OK
            'petsupplies', // OK
            'home', // OK
            'homeimprovement', // OK
            'books'
        );
        
        $name = $xsdHelper->retrieveFilename($category);
        if(in_array(strtolower($name), $allowedCategories)){

            $tab = Mage::Helper('Amazon/Fields_Optionnals_'.$name)->getOptionnalsFields($tab, $product);
            
        }                

        return $tab;
    }
    
    /**
     * Get field value
     * 
     * <ul>
     * <li>bullet_point (1-5)</li>
     * <li>search_terms (1-5)</li>
     * </ul>
     * 
     * @param string $name
     * @param Mage_Catalog_Model_Product $product
     * @return mixed array|string $retour
     */
    public function getValueForField($name, $product){

        $retour = '';
        
        //MfrPartNumber
        if(strtolower($name) == 'mfrpartnumber'){
            
            if($product->getData('mfr_part_number')){
                $retour = $product->getData('mfr_part_number');
            }
            
        }
        
        // bullet points
        if(strtolower($name) == 'bulletpoint'){

            $retour = array();
            $bulletPointsAttribute = Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getbulletpointsAttribute();

            if($bulletPointsAttribute){

                foreach(array_slice(explode("\n", strip_tags($product->getData($bulletPointsAttribute))), 0, 5) as $item)
                    if(rtrim($item) != '')
                        $retour[] = rtrim($item);

            }else{

                if($product->getData('bullet_point1'))
                    $retour[] = $product->getData('bullet_point1');

                if($product->getData('bullet_point2'))
                    $retour[] = $product->getData('bullet_point2');

                if($product->getData('bullet_point3'))
                    $retour[] = $product->getData('bullet_point3');

                if($product->getData('bullet_point4'))
                    $retour[] = $product->getData('bullet_point4');

                if($product->getData('bullet_point5'))
                    $retour[] = $product->getData('bullet_point5');

            }
            
        }
        
        // search terms
        if(strtolower($name) == 'searchterms'){
            
            $retour = array();
            
            if($product->getData('search_terms1'))
                $retour[] = $product->getData('search_terms1');
            
            if($product->getData('search_terms2'))
                $retour[] = $product->getData('search_terms2');
            
            if($product->getData('search_terms3'))
                $retour[] = $product->getData('search_terms3');
            
            if($product->getData('search_terms4'))
                $retour[] = $product->getData('search_terms4');
            
            if($product->getData('search_terms5'))
                $retour[] = $product->getData('search_terms5');
            
        }
        
        return $retour;
        
    }
    
}
