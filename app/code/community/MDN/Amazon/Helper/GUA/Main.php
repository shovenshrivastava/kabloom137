<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_GUA_Main extends Mage_Core_Helper_Abstract {

    private $_cache = array();

    //protected $_country = null;

    /**
     * Get configured country
     * 
     * @return string
     * @deprecated
     */
    /* public function getCountry(){

      if($this->_country === null){
      $this->_country = Mage::getStoreConfig('amazon/general/web_service_url');
      }

      return $this->_country;

      } */

    /**
     * Get all browsenodes list
     * 
     * @return array $retour
     */
    public function getAllBrowseNodes() {

        $retour = array();
        $filename = Mage::app()->getConfig()->getOptions()->getLibDir() . '/MDN/Marketplace/Amazon/GUA/ALL.csv';
        $lines = file($filename);

        foreach ($lines as $line) {

            $line = trim($line);

            $retour[] = $line;
        }

        return $retour;
    }

    /**
     * Get browse nodes for tree $tree
     *
     * @param string $country
     * @return array $retour
     */
    public function getBrowseNodes($country) {

        $retour = array();

        $config = Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon');

        $browseNodesUsed = $config->getbrowseNodes();

        //search in cache
        $cacheKey = $country;
        if (isset($this->_cache[$country])) {

            $retour = $this->_cache[$country];
        } else {

            $trees = $this->getTrees($country);

            // show selected browse node
            if (is_array($browseNodesUsed) && count($browseNodesUsed) > 0) {

                $retour[] = '';
                foreach ($trees as $tree) {

                    if (in_array(strtolower($tree['label']), $browseNodesUsed)) {

                        $options = $this->parseGUA($tree['label'], $country);

                        $retour = $retour + $options;
                    }
                }
            }

            //save in cache
            $this->_cache[$cacheKey] = $retour;
        }

        return $retour;
    }

    /**
     * Parse csv files
     *
     * @param string $country
     * @return array
     */
    public function parseGUA($tree, $country) {

        $country = ($country == 'GB') ? 'UK' : $country;
        $options = array();
        $filename = Mage::app()->getConfig()->getOptions()->getLibDir() . '/MDN/Marketplace/Amazon/GUA/' . $country . '/' . strtolower($tree) . '.csv';

        if (file_exists($filename)) {
            $tmp = file($filename);

            for ($i = 0; $i < count($tmp) - 1; $i++) {

                if ($i == 0)
                    continue;

                $option = explode(';', $tmp[$i]);
                $node = preg_replace('/"/', "", $option[1]);
                $node = trim($node);
                $code = trim($option[0]);
                $options[$code] = $node;
            }
        }

        return $options;
    }

    /**
     * Get trees avaliable for country for $country
     *
     * @param string $country
     * @return array
     */
    public function getTrees($country) {

        $retour = array();

        $countryCode = '';
        switch ($country) {
            case 'GB':
                $countryCode = 'UK';
                break;
            default:
                $countryCode = $country;
                break;
        }

        $filename = Mage::app()->getConfig()->getOptions()->getLibDir() . '/MDN/Marketplace/Amazon/GUA/' . $countryCode . '.csv';

        if(file_exists($filename)){

            $lines = file($filename);

            // be carefull, some browse nodes have same code for the same country...
            foreach ($lines as $line) {

                $line = trim($line);
                $tmp = explode(';', $line);
                if (count($tmp) == 2 && $tmp[1] != '')
                    $retour[] = array(
                        'label' => $tmp[0],
                        'code' => $tmp[1]
                    );
            }

        }

        return $retour;
    }

    /**
     * Get browse node for current country
     * 
     * @param int $usId
     * @return mixed $retour 
     */
    public function getBrowseNodeByCountry($usId) {

        return $usId;

        //$retour = '';
        /* $bn = $this->getTrees(Mage::registry('mp_country')->getmpac_country_code());
          if(count($bn)>0){
          $usNodes = $this->getTrees('US');
          if(array_key_exists($usId, $usNodes)){
          $usName = $usNodes[$usId];
          $bn = array_flip($bn);
          if(array_key_exists($usName, $bn)){
          $retour = $bn[$usName];
          }
          }
          } */

        /* $browseNode = $this->getBrowseNodes('all');
          if(array_key_exists($usId, $browseNode)){
          $retour = $browseNode[$usId];
          } */

        //return $retour;
    }

    /**
     * Get sub categories
     *
     * @return array $retour
     */
    public function getAllCategoriesSubCategories() {

        $retour = array();

        $config = Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon');

        $categoriesUsed = $config->getcategories();

        //check cache
        $cacheKey = 'all_category_and_subcategories';
        if (isset($this->_cache[$cacheKey])) {

            $retour = $this->_cache[$cacheKey];
        } else {

            $content = utf8_encode(file_get_contents(Mage::Helper('Amazon/XSD_Main')->getFilename('Product.xsd')));
            $xml = new DomDocument();
            $xml->loadXML($content);

            $schema = $xml->documentElement;

            if (is_array($categoriesUsed) && count($categoriesUsed) > 0) {
                foreach ($this->getCategories() as $category) {

                    if ($category == "" || !in_array($category, $categoriesUsed))
                        continue;

                    //add subcategory
                    $hasSubCategory = false;
                    foreach ($this->getSubCategory($category) as $subCategory) {
                        $key = $category . '-' . $subCategory;
                        $name = $category . ' / ' . $subCategory;
                        $retour[$key] = $name;
                        $hasSubCategory = true;
                    }
                    if (!$hasSubCategory) {
                        $key = $category . '-';
                        $name = $category;
                        $retour[$key] = $name;
                        $hasSubCategory = true;
                    }
                }
            }

            //save in cache
            $this->_cache[$cacheKey] = $retour;
        }

        return $retour;
    }

    /**
     * Return product categories
     * 
     * @return array $retour
     */
    public function getCategories() {

        $retour = array();

        //check cache
        $cacheKey = 'all_categories';
        if (isset($this->_cache[$cacheKey])) {

            $retour = $this->_cache[$cacheKey];
        } else {

            $content = utf8_encode(file_get_contents(Mage::Helper('Amazon/XSD_Main')->getFilename('Product.xsd')));
            $xml = new DomDocument();
            $xml->loadXML($content);

            $schema = $xml->documentElement;

            foreach ($schema->getElementsByTagName('include') as $include) {

                //parse category
                $schemaLocation = $include->getAttribute('schemaLocation');
                $tmp = explode(".", $schemaLocation);
                $categorie = $tmp[0];
                if ($categorie == "amzn-base")
                    $categorie = "";

                $retour[$categorie] = $categorie;
            }

            //save in cache
            $this->_cache[$cacheKey] = $retour;
        }

        return $retour;
    }

    /**
     * Return sub categories for one category
     * 
     * @param string $category
     * @return array $subcategories
     */
    public function getSubCategory($category) {

        $subcategories = array();

        $filename = Mage::Helper('Amazon/XSD_Main')->getFilename($category . '.xsd');
        $parseAmazon = new MDN_Amazon_Helper_XSD_Parse($filename);
        $subcategories = $parseAmazon->getSubcategories();

        return $subcategories;
    }

}
