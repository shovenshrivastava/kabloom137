<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */

class MDN_Amazon_Helper_Category extends Mage_Core_Helper_Abstract {
    
    /**
     * Is $actegory has isAdultAttrbute ?
     * 
     * @param string $category
     * @param string $productType
     * 
     * @return array $retour
     */
    public function hasAdultAttribute($category, $productType){
    
        $retour = array();
        $filename = $this->getXsdFilename($category);
        $parser = new MDN_Amazon_Helper_XSD_RequiredFields($filename);
        $tab = $parser->toArray($productType);
        
        // looking for adult attribute        
        $retour = $this->parseEntry($tab);
          
        return $retour;               
        
    }
    
    /**
     * Parse recursively
     * 
     * @param array $entry
     * @return array $retour 
     */
    public function parseEntry($entry){
        
        $retour = array();
        foreach($entry as $k => $v){

            if(is_array($v)){
            
                if(array_key_exists('name', $v) && $v['name'] == 'IsAdultProduct'){
                        $retour[$k] = 1;
                }else{
                    if(array_key_exists('type', $v) && $v['type'] == 'complex'){
                        $res = $this->parseEntry($v);
                        if(count($res) > 0)
                            $retour[$k] = $res;
                    }
                }   
            }
                    
        }
        return $retour;
        
    }
    
    /**
     * Get xsd filename
     * 
     * @param string $category
     * @return string $retour 
     */
    public function getXsdFilename($category){
        
        $retour = '';
        
        $retour .= Mage::Helper('Amazon/XSD_Main')->getFilename($category.'.xsd');
        
        return $retour;
        
    }
    
}
