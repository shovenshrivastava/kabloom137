<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */

class MDN_Amazon_Helper_Tracking extends Mage_Core_Helper_Abstract implements MDN_MarketPlace_Helper_Interface_Tracking {

    /**
     * Build and send send tracking file
     */
    public function sendTracking() {

        if ($this->buildTrackingFile()) {

            $this->send();

        }        

        $this->getOrdersToUpdate();
    }

    
    /**
     * Retrieve waiting shipment orders
     * 
     * @return boolean $retour
     */
    public function buildTrackingFile() {

        // init
        $reportRequestId = "";
        $retour = false;
        $unchippedOrders = array();
        $ordersIds = array();
        
        $feed = Mage::getModel('MarketPlace/Feed')->getCollection()
                    ->addFieldToFilter('mp_marketplace_id', Mage::Helper('Amazon')->getMarketPlaceName())
                    ->addFieldToFilter('mp_type', MDN_MarketPlace_Helper_Feed::kFeedTypeUnshippedOrders)
                    ->addFieldToFIlter('mp_status', MDN_MarketPlace_Helper_Feed::kFeedSubmitted)
                    ->addFieldToFilter('mp_country', Mage::registry('mp_country')->getId())
                    ->setOrder('mp_id', 'DESC')
                    ->getFirstItem();
        
        $reportRequestId = $feed->getmp_feed_id();
        
        if ($reportRequestId != "") {

            $report = mage::helper('Amazon/Report')->getReportById($reportRequestId, '_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_');

            if ($report != "none") {
                if ($report != "unvailable") {

                    // get orders ids
                    $lines = explode("\n", $report);

                    for ($i = 0; $i < count($lines) - 1; $i++) {

                        if ($i == 0)
                            continue;

                        if ($lines[$i] != "") {

                            $tmp = explode("\t", utf8_encode($lines[$i]));
                            $unchippedOrders[] = $tmp[0];
                        }
                    }

                    if (count($unchippedOrders) > 0) {

                        $orders = mage::getModel('sales/order')
                                        ->getCollection()
                                        ->addAttributeToSelect('*')
                                        ->addAttributeToFilter('from_site', 'amazon')
                                        ->addAttributeToFilter('marketplace_order_id', $unchippedOrders)
                                        ->addAttributeToFilter('status', 'complete');

                        foreach ($orders as $order) {

                            $ordersIds[] = $order->getentity_id();
                        }

                        $merchantId = Mage::registry('mp_country')->getAssociatedAccount()->getParam('mws_merchantid');                        

                        $xml = Mage::Helper('Amazon/MWS_Xml')->buildXMLTrackingFile($ordersIds, $merchantId);

                        // save file in order to be send
                        mage::getModel('MarketPlace/Logs')->saveFile(
                                array(
                                    'filenames' => array('exportTracking_' . date('Y-m-d_H:i:s') . '.xml', 'exportTracking.xml'),
                                    'fileContent' => $xml,
                                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                                    'type' => 'export'
                                )
                        );

                        $retour = true;
                    }
                }
            }

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                    ->save();            
        }

        return $retour;
    }

    /**
     * Send tracking file
     *
     * @return mixed boolean | string
     */
    public function send() {
        
        if(Mage::registry('mp_country')->getParam('enable_tracking_export') == 1){

        $filename = Mage::getConfig()->getTempVarDir() . '/export/marketplace/amazon/exportTracking.xml';       
        
        if(file_exists($filename)){

            $content = file_get_contents($filename);

            $params = array(
                'Action' => 'SubmitFeed',
                'FeedType' => '_POST_ORDER_FULFILLMENT_DATA_'
            );

            $response = Mage::Helper('Amazon/MWS_Feeds')->sendFeed($params, $filename);

            $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_content($content)
                    ->setmp_response($response->getBody())
                    ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                    ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeTracking)
                    ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName())
                    ->setmp_country(Mage::registry('mp_country')->getId());

            if ($response->getStatus() == 200) {

                $xml = new DomDocument();
                $report = $response->getBody();
                $xml->loadXML($report);
                $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

                $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                        ->setmp_feed_id($feedSubmissionId)
                        ->save();

                return $response;
            }
            else{

                $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                        ->setmp_feed_id(' - ')
                        ->save();

                throw new Exception($response->getMessage(), 16);
            }
            
        }
        else{
            return false;
        }
        }else
            throw new Exception ('API is disable in System > Configuration > MarketPlace', 16);
    }

    /**
     * Get orders to update (waiting for tracking)
     *
     * @return int $requestId
     */
    public function getOrdersToUpdate() {

        // request report
        $response = mage::helper('Amazon/Report')->requestReport('_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_', null, null);

        $xml = new DomDocument();
        $xml->loadXML($response);

        // check if some error occures
        if ($xml->getElementsByTagName('ErrorResponse')->item(0)) {

            $type = $xml->getElementsByTagName('Type')->item(0)->nodeValue;
            $code = $xml->getElementsByTagName('Code')->item(0)->nodeValue;
            $message = $xml->getElementsByTagName('Message')->item(0)->nodeValue;

            $errorMessage = " Some error occured while attempt to request order report : <ul><li>Type : $type</li><li>Code : $code</li><li>Message : $message</li></ul>";

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::Helper('Amazon')->getMarketPlaceName(),
                    MDN_MarketPlace_Model_Logs::kIsError,
                    Mage::Helper('MarketPlace')->__($errorMessage),
                    MDN_MarketPlace_Model_Logs::kScopeTracking,
                    array('fileName' => NULL)
            );

            $requestId = "";
        } else {
            // read request id
            $requestId = $xml->getElementsByTagName('ReportRequestInfo')->item(0)->getElementsByTagName('ReportRequestId')->item(0)->nodeValue;
        }

        $feed = Mage::getModel('MarketPlace/Feed')
                ->setmp_feed_id($requestId)
                ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName())
                ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeUnshippedOrders)
                ->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedSubmitted)
                ->setmp_content($response)
                ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                ->setmp_country(Mage::registry('mp_country')->getId())
                ->save();

        // debug mode
        return $requestId;

    }

}