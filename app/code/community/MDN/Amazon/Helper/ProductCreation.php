<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_ProductCreation extends MDN_MarketPlace_Helper_ProductCreation implements MDN_MarketPlace_Helper_Interface_ProductCreation {
    /* @var boolean */

    protected $_sendRelationship = false;

    /* @var boolean */
    protected $_reviseMode = false;

    /* @var array */
    protected $_addedProducts = array('ok' => 0, 'total' => 0);

    /**
     * Get product feed name
     *
     * @return string
     */
    public function getProductFeedName() {
        return 'productFeed.xml';
    }

    /**
     * Get file type
     *
     * @return string
     */
    public function getFileType() {
        return 'xml';
    }

    /**
     * Mass revise products
     * 
     * @param type $request
     * @return type 
     */
    public function massReviseProducts($request) {

        $this->_reviseMode = true;
        $retour = $this->buildMassProductFile($request);

        if ($retour == 0) {

            // envoie du flux de creation de produits
            $this->sendProductFile();

            // envoie du flux de relationship
            if ($this->_sendRelationship === true)
                $this->sendRelationshipFeed();
        }

        $this->_reviseMode = false;
        return $retour;
    }

    /**
     * Reset results
     */
    public function resetResults() {

        $this->_addedProducts = array(
            'ok' => 0,
            'total' => 0
        );
    }

    /**
     * Mass product creation
     *
     * @param request $request
     * @return type $retour
     */
    public function massProductCreation($request) {

        $this->resetResults();

        $retour = $this->buildMassProductFile($request);

        if ($retour == 0) {

            // envoie du flux de creation de produits
            $this->sendProductFile();

            // envoie du flux de relationship
            if ($this->_sendRelationship === true)
                $this->sendRelationshipFeed();
        }

        return $this->_addedProducts;
    }

    /**
     * Auto submit products (matching ean)
     */
    public function autoSubmit() {

        $ids = Mage::Helper('MarketPlace/AutoSubmit')->getProducts();

        $products = array();
        $helper = Mage::Helper('Amazon/Matching');
        $retour = 1;

        foreach ($ids as $id) {

            $product = Mage::getModel('Catalog/Product')->load($id);
            $barcode = Mage::Helper('MarketPlace/Barcode')->getBarcodeForProduct($product);

            if (Mage::Helper('MarketPlace/Checkbarcode')->checkCode($barcode) === true) {

                $products[$barcode] = $product;
            }
        }

        if (count($products) > 0) {
            $matchingArray = $helper->buildMatchingArray($products);
            $helper->match($matchingArray);
            $retour = 0;
        }

        return $retour;

        // creation auto : need to implement all relationship feeds... (CE etc, it's to much...)
        //$request = new Zend_Controller_Request_Http();
        //$request->setParam('product_ids', $ids);
        //return $this->massProductCreation($request);
    }

    /**
     *  Add item simple
     * 
     * @param Mage_Catalog_Model_Product $product
     * @param string $category
     * @param array $code
     * @param boolean $isAdult
     * @return array 
     */
    protected function _addItemSimple($product, $category, $code, $params) {

        return Mage::Helper('Amazon/Creation_AddItemSimple')->addItem($product, $category, $code, $params);
    }

    /**
     * Add item configurable
     * 
     * @param Mage_Catalog_Model_Product $product
     * @param string $category
     * @param array $code
     * @param boolean $isAdult
     * @return array 
     */
    protected function _addItemConfigurable($product, $category, $code, $params) {

        return Mage::Helper('Amazon/Creation_AddItemConfigurable')->addItem($product, $category, $code, $params);
    }

    /**
     * Build mass product file
     *
     * @param request $request
     * @return int
     */
    public function buildMassProductFile($request) {

        // get data
        $ids = $request->__get('product_ids');
        $asins = $request->__get('product_asins');
        $matchWithAsin = (count($asins) == count($ids)) ? true : false;

        // init
        $content = "";
        $products = array();
        $invalidCodes = array();
        $unassociatedCategories = array();
        $errorMessage = "";
        $alreadyCreated = array();
        $relationship = array();
        $code = array();

        // retrieve current account
        $account = Mage::registry('mp_country')->getAssociatedAccount();
        $isAdult = ($account->getParam('isadultproduct') == 1) ? true : false;

        // check manufacturer attribute
        $manufacturerAttribute = Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_manufacturer_attribute();

        if ($manufacturerAttribute == "") {

            // TODO : move log !!
            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(), MDN_MarketPlace_Model_Logs::kIsError, Mage::Helper('MarketPlace')->__('Manufacturer attribute haven\'t been setted in System > Configuration > MarketPlace'), MDN_MarketPlace_Model_Logs::kScopeCreation, array(
                'fileName' => NULL
                    )
            );
        } else {

            // set total
            $this->_addedProducts['total'] = count($ids);

            $i = 0;
            foreach ($ids as $id) {

                // retrieve product
                $product = Mage::getModel('catalog/product')->setStoreId(Mage::registry('mp_country')->getParam('store_id'))->load($id);

                Mage::getModel('MarketPlace/Data')->addMessage($product->getId(), '', Mage::registry('mp_country')->getId());

                // match with ASIN
                if ($matchWithAsin === true) {

                    $code = array(
                        'type' => 'ASIN',
                        'value' => $asins[$i]
                    );
                } else {
                    // match with barcode
                    // check barcode
                    if (Mage::registry('mp_country')->getParam('ean_required') == 'yes') {
                        $barcode = Mage::helper('MarketPlace/Barcode')->getBarcodeForProduct($product);

                        // si le produit est de type simple, alors on verifie le barcode
                        if (in_array($product->gettype_id(), array('simple', 'virtual'))) {

                            if (Mage::helper('MarketPlace/Checkbarcode')->checkCode($barcode) !== true) {

                                $invalidCodes[] = $product->getentity_id();
                                continue;
                            } else {

                                $barcodeType = Mage::helper('Amazon/Checkbarcode')->getType($barcode);

                                $code = array(
                                    'type' => $barcodeType,
                                    'value' => $barcode
                                );
                            }
                        }
                    } else {
                        $code = array();
                    }
                }

                // check if it is not created yet
                $is_created = Mage::getModel('MarketPlace/Data')->isCreated($product, Mage::helper('Amazon')->getMarketPlaceName());
                if ($is_created) {
                    $alreadyCreated[] = array('id' => $id);
                    continue;
                }

                // check category
                $category = Mage::helper('Amazon/CategoriesAssociation')->getDefaultCategory($product);
                if ($category == NULL || $category == "") {
                    $unassociatedCategories[] = $id;
                    continue;
                }

                if ($product->gettype_id() == 'configurable') {

                    // on verifie que les configurables sont gérés pour la catégorie concerné
                    if (!in_array(ucfirst($category), Mage::Helper('Amazon/Configurables_Configurables')->getAllowedCategories())) {

                        throw new Exception(Mage::Helper('Amazon')->__('Variations are not allowed yet for category %s', $category));
                    }

                    // ajout d'un produit de type configurable
                    $params = array(
                        'isAdult' => $isAdult,
                    );

                    $res = $this->_addItemConfigurable($product, $category, $code, $params);

                    $products[] = $res['parent'];
                    $childsSkus = array();
                    foreach ($res['childs'] as $childItem) {

                        $products[] = $childItem;
                        $childsSkus[] = $childItem['general']['SKU'];
                    }

                    $relationship[$id] = array(
                        'parentsku' => $product->getsku(),
                        'childs' => $childsSkus
                    );
                } else {

                    $params = array(
                        'isAdult' => $isAdult
                    );

                    // ajout d'un produit de type simple
                    $products[] = $this->_addItemSimple($product, $category, $code, $params);
                }

                $i++;
            }

            // add message for unassociated categories
            if (count($unassociatedCategories) > 0) {
                Mage::getModel('MarketPlace/Data')->updateStatus($unassociatedCategories, Mage::registry('mp_country')->getId(), self::kStatusInError);
                $errorMessage .= "Check categories association for products : ";
                foreach ($unassociatedCategories as $k => $v) {
                    $errorMessage .= $v . ' ';
                    Mage::getModel('MarketPlace/Data')->addMessage($v, 'Check categories association', Mage::registry('mp_country')->getId());
                }
                $errorMessage .= '<br/>';
            }

            // add message for bad barcodes
            if (count($invalidCodes) > 0) {
                Mage::getModel('MarketPlace/Data')->updateStatus($invalidCodes, Mage::registry('mp_country')->getId(), self::kStatusInError);
                $errorMessage .= 'Invalid EAN code for products : ';
                foreach ($invalidCodes as $k => $v) {
                    $errorMessage .= $v . ' ';
                    Mage::getModel('MarketPlace/Data')->addMessage($v, 'Invalid EAN code', Mage::registry('mp_country')->getId());
                }
                $errorMessage .= '<br/>';
            }

            // add message for already created products
            if (count($alreadyCreated) > 0) {
                $errorMessage .= "Already created : ";
                foreach ($alreadyCreated as $created) {
                    $errorMessage .= $created['id'] . ' ';
                }
                $errorMessage .= '<br/>';
            }

            // addd log
            if ($errorMessage != "") {
                // TODO : move log !
                mage::getModel('MarketPlace/Logs')->addLog(
                        Mage::helper('Amazon')->getMarketPlaceName(), MDN_MarketPlace_Model_Logs::kIsError, $errorMessage, MDN_MarketPlace_Model_Logs::kScopeCreation, array(
                    'fileName' => NULL
                        )
                );
            }
        }

        // build xml files
        if (count($products) > 0) {

            // set nbr products submitted
            $this->_addedProducts['ok'] = count($products);

            // get merchant id
            $merchant_id = $account->getParam('mws_merchantid');

            // build xml file
            $content = Mage::Helper('Amazon/MWS_Xml')->buildProductFeed($merchant_id, $products);

            // save file to export
            $fileToExport = $this->getProductFeedName();
            $history = 'productFeed_' . date('Y-m-d_H:i:s') . '.xml';

            mage::getModel('MarketPlace/Logs')->saveFile(
                    array(
                        'filenames' => array($history, $fileToExport),
                        'fileContent' => $content,
                        'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                        'type' => 'export'
                    )
            );

            // validation du flux
            //$xml = new DomDocument();
            //$xml->loadXML($content);
            //$res = $xml->schemaValidate(Mage::Helper('Amazon/XSD_Main')->getFilename('amzn-envelope.xsd'));

            if (count($relationship) > 0) {

                //send relationship feed
                $content = Mage::Helper('Amazon/MWS_Xml')->buildRelationshipFeed($merchant_id, $relationship);

                // save file to export
                $fileToExport = 'Relationship.xml';
                $history = 'Relationship_' . date('Y-m-d_H:i:s') . '.xml';

                mage::getModel('MarketPlace/Logs')->saveFile(
                        array(
                            'filenames' => array($history, $fileToExport),
                            'fileContent' => $content,
                            'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                            'type' => 'export'
                        )
                );

                // validation du flux
                $xml = new DomDocument();
                $xml->loadXML($content);
                //$res = $xml->schemaValidate(Mage::Helper('Amazon/XSD_Main')->getFilename('amzn-envelope.xsd'));

                $this->_sendRelationship = true;
            }

            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Get required fields
     *
     * @param string $category
     * @return array $parsed
     */
    public function getRequiredFields($category) {

        $parsed = array();

        $xsdTab = Mage::helper('Amazon/XSD_Main')->retrieveFilenameAndMainNode($category);

        $filename = Mage::Helper('Amazon/XSD_Main')->getFilename($xsdTab['filename'] . '.xsd');

        $parseAmazon = new MDN_Amazon_Helper_XSD_RequiredFields($filename);
        $parsed = $parseAmazon->getRequiredFields($xsdTab['category']);

        return $parsed;
    }

    /**
     * Build product data array
     *
     * @param string $k
     * @param string $v
     * @return array $retour
     * 
     */
    /* public function buildProductDataArray($k, $v) {

      // init
      $retour = array();

      // get sub nodes
      $tmp = explode("_", $k);

      // remove first item if this value egals "ProductData", redundant information
      if ($tmp[0] == "ProductData")
      $tmp = array_slice($tmp, 1, count($tmp) - 1);

      if (count($tmp) > 1) {

      // get first key
      $firstKey = $tmp[0];

      // build new array, remove first elt
      $newTmp = array_slice($tmp, 1, count($tmp) - 1);

      // build new key
      $newKey = implode("_", $newTmp);

      // recursive call
      $retour[$firstKey] = $this->buildProductDataArray($newKey, $v);
      } else {
      // add value
      $retour[$tmp[0]] = Mage::helper('MarketPlace/Product')->formatExportedTxt($v);
      }

      return $retour;
      } */

    /**
     * Send product feed
     *
     * @param int $mp_product_id
     * @param array $identifiants
     *
     */
    public function sendProductFile($mp_product_id = null) {

        $file = $this->getProductFeedName();
        $path = $this->getExportPath(Mage::helper('Amazon')->getMarketPlaceName());
        $filename = $path . $file;

        $response = Mage::Helper('Amazon/MWS_Feeds')->submitFeed('_POST_PRODUCT_DATA_', $filename);

        if ($response->getStatus() == 200) {

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);
            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            $xml = new DomDocument('1.0', 'utf8');
            $xml->load($filename);
            $tabIds = array();

            foreach ($xml->getElementsByTagName('SKU') as $skuElt) {


                $current_sku = $skuElt->nodeValue;

                $mp_product_id = Mage::getModel('Catalog/Product')->getIdBySku($current_sku);

                $tabIds[] = $mp_product_id;
            }

            // update product status
            if ($this->_reviseMode === false) {
                Mage::getModel('MarketPlace/Data')->updateStatus($tabIds, Mage::registry('mp_country')->getId(), self::kStatusPending);
                $mpType = self::kFeedTypeProductCreation;
            } else {
                $mpType = self::kFeedTypeReviseProducts;
            }

            $ids = implode(',', $tabIds);

            $feed_content = file_get_contents($filename);
            $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_marketplace_id(Mage::Helper('Amazon')->getMarketPlaceName())
                    ->setmp_feed_id($feedSubmissionId)
                    ->setmp_status(self::kFeedSubmitted)
                    ->setmp_ids($ids)
                    ->setmp_content($feed_content)
                    ->setmp_response('')
                    ->setmp_type($mpType)
                    ->setmp_date(date('Y-m-d H:i:s'))
                    ->setmp_country(Mage::registry('mp_country')->getId())
                    ->save();
        }
        else
            throw new Exception($response->getMessage(), 4);
    }

    /**
     * Send relationship feed
     * 
     * @throws Exception 
     */
    public function sendRelationshipFeed() {

        $file = 'Relationship.xml';
        $path = $this->getExportPath(Mage::helper('Amazon')->getMarketPlaceName());
        $filename = $path . $file;

        $response = Mage::Helper('Amazon/MWS_Feeds')->submitFeed('_POST_PRODUCT_RELATIONSHIP_DATA_', $filename);

        if ($response->getStatus() == 200) {

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);
            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(), MDN_MarketPlace_Model_Logs::kNoError, Mage::Helper('MarketPlace')->__('Relationship feed has been sent. (feed submission id : ' . $feedSubmissionId . ')'), MDN_MarketPlace_Model_Logs::kScopeCreation, array(
                'fileName' => "amazonRelationshipFeedReport-" . $feedSubmissionId . ".xml",
                'fileContent' => $response->getBody(),
                'type' => 'export'
                    )
            );

            $feed_content = file_get_contents($filename);
            $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_marketplace_id(Mage::Helper('Amazon')->getMarketPlaceName())
                    ->setmp_feed_id($feedSubmissionId)
                    ->setmp_status(self::kFeedSubmitted)
                    ->setmp_ids()
                    ->setmp_content($feed_content)
                    ->setmp_response('')
                    ->setmp_type(self::kFeedTypeRelationship)
                    ->setmp_date(date('Y-m-d H:i:s'))
                    ->setmp_country(Mage::registry('mp_country')->getId())
                    ->save();
        }
        else
            throw new Exception($response->getMessage(), 4);
    }

    /**
     * Import created products, call by the CRON
     *
     * @return string $retour
     * @see checkProductCreation()
     * @see importUnmatchedProducts()
     */
    public function importCreatedProducts() {

        $retour = '';

        $retour .= $this->importUnmatchedProducts();
        $this->checkProductCreation();

        return $retour;
    }

    /**
     * Check product creation
     *
     * @param string $marketplaceName
     */
    public function checkProductCreation() {

        $error = false;
        $errorMessage = '';
        // get request errors
        $errors = array();
        $success = array();
        //$unvailable = array();
        $actionRequired = array();

        $addedProducts = array();

        // load feed collection
        $collection = Mage::getModel('MarketPlace/Feed')
                ->getCollection()
                ->addFieldToFilter('mp_marketplace_id', Mage::helper('Amazon')->getMarketPlaceName())
                ->addFieldToFilter('mp_type', self::kFeedTypeProductCreation)
                ->addFieldToFilter('mp_country', Mage::registry('mp_country')->getId())
                ->addFieldToFilter(
                'mp_status', array(
            'in',
            array(
                self::kFeedSubmitted,
                self::kFeedInProgress
            )
                )
        );

        // check each lines
        foreach ($collection as $feedSubmission) {

            $id = $feedSubmission->getmp_feed_id();

            // request result to amazon
            $response = mage::helper('Amazon/Feed')->getFeedSubmissionResult($id);

            // save response
            mage::getModel('MarketPlace/Logs')->saveFile(
                    array(
                        'filenames' => array('addProductResponse.xml'),
                        'fileContent' => $response,
                        'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                        'type' => 'export'
                    )
            );

            // parse response
            $xml = new DomDocument('1.0', 'utf8');
            $xml->loadXML($response);
            $racine = $xml->documentElement;

            /*             * *****************************************************************
             *
             *                    GLOBAL ERRORS
             *
             * **************************************************************** */
            // check if response contains some error (GLOBAL ERRORS)
            if ($racine->getElementsByTagName('Error')->item(0)) {

                $code = $racine->getElementsByTagName('Error')->item(0)->getElementsByTagName('Code')->item(0)->nodeValue;

                if ($code == "FeedProcessingResultNotReady" || $code == "RequestThrottled") {

                    $feedSubmission->setmp_status(self::kFeedInProgress)
                            ->save();
                } else {
                    $log = "(id : $id)";

                    foreach ($racine->getElementsByTagName('Error') as $error) {

                        $code = $error->getElementsByTagName('Code')->item(0)->nodeValue;
                        $message = $error->getElementsByTagName('Message')->item(0)->nodeValue;

                        $log .= 'Error code : ' . $code . '. Description : ' . $message . "<br/>";
                    }

                    $feedSubmission->setmp_status(self::kFeedError)
                            ->setmp_response($response)
                            ->save();

                    $productsIds = $feedSubmission->getmp_ids();
                    $productsTab = explode(",", $productsIds);
                    Mage::getModel('MarketPlace/Data')->updateStatus($productsTab, Mage::registry('mp_country')->getId(), self::kStatusNotCreated);

                    throw new Exception($log);
                }
            } else {
                // get summary
                $processingSummary = $racine->getElementsByTagName('ProcessingSummary')->item(0);
                $messagesProcessed = $processingSummary->getElementsByTagName('MessagesProcessed')->item(0)->nodeValue;
                $messagesSuccessful = $processingSummary->getElementsByTagName('MessagesSuccessful')->item(0)->nodeValue;
                $messagesWithError = $processingSummary->getElementsByTagName('MessagesWithError')->item(0)->nodeValue;
                $messagesWithWarning = $processingSummary->getElementsByTagName('MessagesWithWarning')->item(0)->nodeValue;

                // load product feed
                $productFeed = new DomDocument('1.0', 'utf8');
                $productFeed->loadXML($feedSubmission->getContent());

                // check if submitted feed was correctly formated
                // if not, log the first error message
                if ($messagesWithError > 0) {

                    $traitedResults = array();

                    $results = $racine->getElementsByTagName('Result');

                    // tab which contain matched products but some of the information submitted does not match the product information that is already in the Amazon catalog
                    $t_match = array();

                    // tab which contain unauthorized products
                    $t_unauthorized = array();

                    /*                     * *************************************************************************************
                     *
                     *                                    PARSE RESULT
                     *
                     * ************************************************************************************* */
                    $cpt = 0;
                    foreach ($results as $result) {

                        // init
                        $sku = "";
                        $productID = "";
                        // retrieve message ID
                        $currentMessageID = $result->getElementsByTagName('MessageID')->item(0)->nodeValue;

                        // check if message ID already processed
                        if ($currentMessageID != 0 && !in_array($currentMessageID, $traitedResults)) {

                            // retrieve SKU
                            $messages = $productFeed->getElementsByTagName('Message');

                            foreach ($messages as $message) {
                                if ($message->getElementsByTagName('MessageID')->item(0)->nodeValue == $currentMessageID) {
                                    $sku = $message->getElementsByTagName('SKU')->item(0)->nodeValue;
                                    break;
                                }
                            }

                            // if SKU is defined in feed, then get product ID and add it in errors tab
                            if ($sku !== "") {
                                $productID = Mage::getModel('Catalog/product')->getIdBySku($sku);
                                $errors[] = $productID;
                            }

                            // save current message ID as processed
                            $traitedResults[] = $currentMessageID;
                        }

                        // find result code
                        $resultCode = $result->getElementsByTagName('ResultCode')->item(0)->nodeValue;

                        // if Error
                        if ($resultCode == "Error") {

                            // find error code and description
                            $errorCode = $result->getElementsByTagName('ResultMessageCode')->item(0)->nodeValue;
                            $errorDescription = $result->getElementsByTagName('ResultDescription')->item(0)->nodeValue;

                            switch ($errorCode) {
                                // Seller is not authorized to list products in this category
                                case '8026':

                                    // retrieve SKU
                                    $errorSku = $result->getElementsByTagName('AdditionalInfo')->item(0)->getElementsByTagName('SKU')->item(0)->nodeValue;
                                    $errorId = Mage::getModel('catalog/product')->getIdBySku($errorSku);
                                    $t_unauthorized[] = $errorId;

                                    break;
                                // if error 8041 (match� mais certaines informations divergent)
                                /*
                                case '8041':
                                case '8541': // TODO : a supprimer ? ce cas pose-t-il probleme ?

                                    // retrieve SKU and ASIN
                                    $matched_sku = $result->getElementsByTagName('AdditionalInfo')->item(0)->getElementsByTagName('SKU')->item(0)->nodeValue;
                                    preg_match('#ASIN (B\w+)#', $errorDescription, $match_asin);
                                    $errorSku = $result->getElementsByTagName('AdditionalInfo')->item(0)->getElementsByTagName('SKU')->item(0)->nodeValue;
                                    $errorId = Mage::getModel('catalog/product')->getIdBySku($errorSku);
                                    if (isset($match_asin[1])) {
                                        $t_match[] = array(
                                            'id' => $errorId,
                                            'asin' => $match_asin[1]
                                        );
                                    }

                                    break;
                                */
                                case '8542':
                                    // product corresponds to multiple ASINs
                                    $multipleAsins = array();
                                    $multipleMessage = Mage::Helper('MarketPlace')->__('This product corresponds to multiple ASINs, please select the good one : ');
                                    preg_match('#ASINs (\W[B\w+\W\s]{1,})\sbut#', $errorDescription, $multipleAsins);
                                    $multipleAsins[1] = str_replace(array('(', ')'), array('', ''), $multipleAsins[1]);
                                    $multipleMessage .= implode(',',$multipleAsins[1]);
                                    Mage::getModel('MarketPlace/Data')->updateStatus(array($productID), Mage::registry('mp_country')->getId(), MDN_MarketPlace_Helper_ProductCreation::kStatusActionRequired, null, $multipleMessage);
                                    $actionRequired[] = $productID;
                                    break;
                            }

                            // add error message in database
                            if ($productID != "" && !in_array($productID, $actionRequired)) {
                                Mage::getModel('MarketPlace/Data')->addMessage($productID, $errorDescription, Mage::registry('mp_country')->getId());
                            }

                            $error = true;
                            $errorMessage .= Mage::Helper('MarketPlace')->__('Error code : %s. Description : %s (id : %s)', $errorCode, $errorDescription, $id) . '<br/><br/>';
                        }
                    }

                    // save feed response
                    $feedSubmission->setmp_status(self::kFeedError)
                            ->setmp_response($response)
                            ->save();

                    // update created products
                    $pendingIds = $feedSubmission->getmp_ids();
                    $pendingIdsTab = explode(",", $pendingIds);

                    // if no error, it seems that all messageID = 0 (bad feed)
                    if (count($errors) > 0) {
                        // update marketplace status
                        foreach ($pendingIdsTab as $pendingId) {

                            if (!in_array($pendingId, $actionRequired)) {

                                if (in_array($pendingId, $errors)) {
                                    // set product as in error
                                    Mage::getModel('MarketPlace/Data')->updateStatus(array($pendingId), Mage::registry('mp_country')->getId(), self::kStatusInError);
                                } else {
                                    // set product as created
                                    Mage::getModel('MarketPlace/Data')->updateStatus(array($pendingId), Mage::registry('mp_country')->getId(), self::kStatusCreated);
                                    $addedProducts[] = $pendingId;
                                }
                            }
                        }
                    } else {
                        // set all products as not created
                        Mage::getModel('MarketPlace/Data')->updateStatus($pendingIdsTab, Mage::registry('mp_country')->getId(), self::kStatusInError);
                    }

                    // set unauthorized products as incomplete, error 8026
                    if (count($t_unauthorized) > 0) {
                        foreach ($t_unauthorized as $id) {
                            Mage::getModel('MarketPlace/Data')->updateStatus(array($id), Mage::registry('mp_country')->getId(), self::kStatusIncomplete);
                            $addedProducts[] = $id;
                        }
                    }

                    // try to match with ASIN, error n°8041
                    if (count($t_match) > 0) {
                        $this->tryToMatchWithASIN($t_match);
                    }
                } else {

                    // NO ERROR                    
                    $idsTab = array();
                    foreach ($productFeed->getElementsByTagName('SKU') as $skuNode) {

                        $idsTab[] = Mage::getModel('catalog/product')->getIdBySku($skuNode->nodeValue);
                    }

                    //$current_ids = $feedSubmission->getmp_ids();
                    //$idsTab = explode(',', $current_ids);

                    foreach ($idsTab as $addedProductId) {

                        // update marketplace status
                        Mage::getModel('MarketPlace/Data')->updateStatus($addedProductId, Mage::registry('mp_country')->getId(), self::kStatusIncomplete);

                        $addedProducts[] = $addedProductId;
                    }


                    // update feed status
                    $feedSubmission->setmp_status(self::kFeedDone)
                            ->setmp_response($response)
                            ->save();
                }
            }
        }

        if (count($addedProducts) > 0) {
            // send price and inventory feed for new products, if not => incomplete
            $this->UpdateStockAndPriceForNewProducts($addedProducts);
        }

        if ($error === true) {

            throw new Exception($errorMessage);
        }
    }

    /**
     * Product already exists on Amazon and merchant informations are differents so
     * match product with ASIN
     *
     * @param array $matched
     * @return int 0
     */
    public function tryToMatchWithASIN($matched) {

        $ids = array();
        $asins = array();

        $i = 0;
        foreach ($matched as $match) {
            $ids[$i] = $match['id'];
            $asins[$i] = $match['asin'];
            $i++;
        }

        // build request
        $request = new Zend_Controller_Request_Http();
        $request->setParam('product_ids', $ids);
        $request->setParam('product_asins', $asins);

        // add products
        $this->massProductCreation($request);

        // update stock and price for matched products
        $this->UpdateStockAndPriceForNewProducts($ids);

        return 0;
    }

    /**
     * Update database
     *
     * @param array $newProducts
     * 
     */
    public function UpdateStockAndPriceForNewProducts($newProducts) {

        $products = mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*')
                ->addFieldToFilter('entity_id', array('in', $newProducts))
                ->joinTable(
                'MarketPlace/Data', 'mp_product_id=entity_id', array(
            'mp_exclude' => 'mp_exclude',
            'mp_force_qty' => 'mp_force_qty',
            'mp_delay' => 'mp_delay'
                ), "mp_marketplace_id='" . Mage::registry('mp_country')->getId() . "'", 'inner'
        );

        if (count($products) > 0) {

            $config = Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon');
            $customPriceAttrName = Mage::registry('mp_country')->getParam('price_attribute');
            $customStockAttrName = $config->getstockAttribute();

            $customPriceMethod = ($customPriceAttrName) ? 'get'.$customPriceAttrName : null;
            $customStockMethod = ($customStockAttrName) ? 'get'.$customStockAttrName : null;

            if($customPriceMethod !== null || $customStockMethod !== null){

                foreach($products as $product){

                    if($customPriceMethod !== null){
                        $product->setmp_custom_price($product->$customPriceMethod());
                    }

                    if($customStockMethod !== null){
                        $product->setmp_custom_stock($product->$customStockMethod());
                    }

                }

            }

            $inventoryFeedName = 'newProductsInventoryFeed.xml';
            $priceFeedName = 'newProductsPriceFeed.xml';
            $imageFeedName = 'newProductsImageFeed.xml';

            mage::helper('Amazon/ProductUpdate')->setInventoryFeed($products, $inventoryFeedName);
            mage::helper('Amazon/ProductUpdate')->updateMarketPlaceStock($inventoryFeedName);
            mage::helper('Amazon/ProductUpdate')->setPriceFeed($products, $priceFeedName);
            mage::helper('Amazon/ProductUpdate')->updateMarketPlacePrices($priceFeedName);
            mage::helper('Amazon/ProductUpdate')->setImageFeed($products, $imageFeedName);
            mage::helper('Amazon/ProductUpdate')->updateImage($imageFeedName);
        }
    }

    /**
     * Import new products
     * 
     * @return string $retour
     */
    public function importUnmatchedProducts() {

        $retour = '';
        $tRetour = array();

        $feeds = Mage::getModel('MarketPlace/Feed')->getCollection()
                ->addFieldToFilter('mp_type', MDN_MarketPlace_Helper_Feed::kFeedTypeMatchingProducts)
                ->addFieldToFilter('mp_marketplace_id', Mage::helper('Amazon')->getMarketPlaceName())
                ->addFieldToFilter('mp_status', array(MDN_MarketPlace_Helper_Feed::kFeedSubmitted, MDN_MarketPlace_Helper_Feed::kFeedInProgress))
                ->addFieldToFilter('mp_country', Mage::registry('mp_country')->getId());

        if ($feeds->getFirstItem()) {

            foreach ($feeds as $feed) {

                // retrieve request id
                $requestId = $feed->getmp_feed_id();

                // check if not empty
                if ($requestId != "") {

                    // clean up
                    $requestId = trim($requestId);

                    // get report corresponding to request id
                    $report = mage::helper('Amazon/Report')->getReportById($requestId, '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_');

                    // not yet available, retry next time !!
                    if ($report != 'unvailable') {

                        // no longer available...
                        if ($report != 'none') {

                            $lines = explode("\n", $report);
                            $tRetour[] = $this->importProducts($lines);

                            $feed->setmp_response($report)
                                    ->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                                    ->save();
                        } else {

                            $feed->setmp_response('No longer available')
                                    ->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                                    ->save();

                            $tRetour[] = Mage::Helper('MarketPlace')->__('Empty inventory report');
                        }
                    } else {

                        $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedInProgress)
                                ->save();

                        $tRetour[] = Mage::Helper('MarketPlace')->__('Report not available yet');
                    }
                }
            }
        }

        // request product listing
        $this->requestListingProducts();

        $retour = implode('<br/>', $tRetour);

        return $retour;
    }

    /**
     * Import existing product into Magento
     * Used for matching new product (retrieve ASIN)
     *
     * @param array $lines
     * @return string $retour
     */
    public function importProducts($lines) {

        $retour = '';
        $nbr = 0;
        $errorMessage = "";
        $error = false;
        $unavailableProducts = array();

        for ($i = 0; $i < count($lines); $i++) {

            try {
                if ($i == 0 || $lines[$i] == "")
                    continue;

                $infos = explode("\t", $lines[$i]); // TODO : check if EAN already used for another product !

                $id = mage::getModel('catalog/product')->getIdBySku($infos[0]);

                if ($id === false) {
                    $unavailableProducts[] = $infos[0];
                    continue;
                }

                $add = mage::getModel('MarketPlace/Data')->getCollection()
                        ->addFieldToFilter('mp_marketplace_id', Mage::registry('mp_country')->getmpac_id())
                        ->addFieldToFilter('mp_product_id', $id)
                        ->getFirstItem();

                // check if product exists in database
                if ($add->getmp_id()) {

                    //check mp_reference
                    if ($add->getmp_reference() === NULL) {

                        $add->setmp_reference($infos[1])
                                ->setmp_marketplace_status(self::kStatusCreated)
                                ->setmp_exclude('0')
                                ->save();
                    } else {

                        $add->setmp_reference($infos[1])
                                ->setmp_marketplace_status(self::kStatusCreated)
                                ->save();
                    }
                } else {

                    // if not add it !
                    $add = mage::getModel('MarketPlace/Data');
                    $add->setmp_marketplace_id(Mage::registry('mp_country')->getmpac_id());
                    $add->setmp_exclude('0');
                    $add->setmp_product_id($id);
                    $add->setmp_reference($infos[1]);
                    $add->setmp_marketplace_status(self::kStatusCreated);
                    $add->save();
                }

                $nbr++;
            } catch (Exception $e) {
                $error = true;
                $errorMessage .= $e->getMessage() . '<br/>';
            }
        }

        $str = "";
        $i = 0;
        foreach ($unavailableProducts as $unavailable) {
            $i++;
            $str .= $unavailable;
            if ($i < count($unavailableProducts))
                $str .= ", ";
        }

        $retour .= Mage::Helper('MarketPlace')->__('%s products have been matched', $nbr);
        if (count($unavailableProducts) > 0) {
            $retour .= '<br/>';
            $retour .= Mage::Helper('MarketPlace')->__('Unvalaible product(s) : %s', $str);
        }

        if ($error) {
            throw new Exception($errorMessage, 14);
        }

        return $retour;
    }

    /**
     * Request stock file
     *
     * @return boolean
     */
    public function requestListingProducts() {

        // send the report request
        $reportType = '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_';
        $retour = Mage::Helper('Amazon/MWS_Reports')->requestReport($reportType, null, null);

        // save report request id by parsing response
        $retourXML = new DomDocument();
        $retourXML->loadXML($retour);
        $requestReportResult = $retourXML->getElementsByTagName('RequestReportResponse')->item(0)->getElementsByTagName('RequestReportResult')->item(0);
        $reportRequestInfo = $requestReportResult->getElementsByTagName('ReportRequestInfo')->item(0);
        $reportRequestId = $reportRequestInfo->getElementsByTagName('ReportRequestId')->item(0)->nodeValue;

        $feed = Mage::getModel('MarketPlace/Feed')
                ->setmp_feed_id($reportRequestId)
                ->setmp_type(MDN_MarketPlace_Helper_feed::kFeedTypeMatchingProducts)
                ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName())
                ->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedSubmitted)
                ->setmp_content($retour)
                ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                ->setmp_country(Mage::Registry('mp_country')->getId())
                ->save();

        return $retour;
    }

    /**
     * Check if file seem's to be ok
     *
     * @param string $lines
     * @return boolean
     */
    public function isProductFileOk($lines) {

        $tmp = explode("\t", $lines[0]);
        if (strtolower(trim($tmp[0])) == "sku" && strtolower(trim($tmp[1])) == "asin")
            return true;

        return false;
    }

    /**
     * Get last Lisitng products
     *
     * @return string
     */
    public function getLastListingProducts() {

        $feed = Mage::getModel('MarketPlace/Feed')->getCollection()
                ->addFieldToFilter('mp_type', MDN_MarketPlace_Helper_Feed::kFeedTypeMatchingProducts)
                ->addFieldToFilter('mp_marketplace_id', Mage::helper('Amazon')->getMarketPlaceName())
                ->addFieldToFilter('mp_status', MDN_MarketPlace_Helper_Feed::kFeedDone)
                ->addFieldToFilter('mp_country', Mage::registry('mp_country')->getId())
                ->setOrder('mp_feed_id', 'DESC')
                ->getFirstItem();

        return $feed->getResponse();
    }

    /**
     * Allow product creation
     * 
     * @return boolean
     */
    public function allowMatchingEan() {
        return true;
    }

    /**
     * Allow delete products
     * 
     * @return boolean 
     */
    public function allowDeleteProduct() {
        return true;
    }

    /**
     * Limit title up to 80 chars
     *
     * @param type $product
     * @return string
     */
    public function getProductTitle($product){

        return substr(parent::getProductTitle($product), 0, 79);

    }

}