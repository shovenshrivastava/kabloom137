<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 * @deprecated since version 2
 */

class MDN_Amazon_Helper_CheckConnexion extends Mage_Core_Helper_Abstract implements MDN_MarketPlace_Helper_Interface_CheckConnection {

    /**
     * Check connexion, try login/pass
     *
     * @return mixed
     * @deprecated since version 2
     */
    public function connexion() {

        throw new Exception('Method deprecated in '.__METHOD__);
        /*$connection = true;
        $message = '';
        $params = array(
            'Action' => 'GetReportRequestList',
        );

        $MWSOrders = new MWSClient($params);
        $response = $MWSOrders->sendRequest();

        if ($response->getStatus() != 200) {

            $connection = false;
            $xml = new DomDocument();
            $content = $response->getBody();

            $xml->loadXML($content);
            $message = '- Error while trying to connect on Amazon : ' . $xml->getElementsByTagName('Error')->item(0)->getElementsByTagName('Message')->item(0)->nodeValue . '<br/>';
        }
      
        return ($connection === true) ? $connection : $message;*/
    }

}
