<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */

set_time_limit(3600);

class MDN_Amazon_Helper_XSD_Parse extends Mage_Core_Helper_Abstract {
    
    /* @var DomDocument */
    private $xml;
    
    /* @var array */
    private static $_attributes = array(
        'name',
        'type',
        'minOccurs',
        'maxOccurs'
    );
    
    /* @var string */
    private static $_url = "https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_1_9/";
    /* @var array */
    private $parsed = array();
    /* @var array */
    private $parsedType = array();

    /**
     * Constructor
     *
     * Throw exception if file does'nt exists
     *
     * @param string $file
     * @see setXml
     *
     */
    public function __construct($file) {    
        
        $category = strrchr($file, "/");
        $category = preg_replace("#/#", "", $category);

        // check if file exists
        if(!file_exists($file))
            throw new Exception('Unavailable xsd file : '.$file);
        
        /*if (!in_array($category, self::$_xsd))
            throw new Exception('Unvalaible xsd file.');*/

        $this->setXml($file);
    }

    /**
     * Setter $this->_xml
     *
     * @param string $file
     */
    public function setXml($file) {

        $content = utf8_encode(file_get_contents($file));
        $this->xml = new DomDocument();
        $this->xml->loadXML($content);
    }

    /**
     * Search elt and build result
     *
     * @param string $name
     * @return array $tab
     * @see parseElement
     * @todo : code special function for Miscellaneous
     */
    public function toArray($name, $ref = false) {
        
        $tab = array();
        $element = null;

        // find element which has name = $name
        $element = $this->findNodeByName($name);

        // build result
        $tab = ($element == null) ? null : $this->parseElement($element, $ref);

        return $tab;
    }

    /**
     * Find node by name
     * 
     * @param string $name
     * @return XML_ELEMENT_NODE 
     */
    public function findNodeByName($name) {

        $elts = $this->xml->getElementsByTagName('element');

        $element = null;

        // find element which has name = $name
        foreach ($elts as $elt) {

            if ($elt->nodeType == XML_ELEMENT_NODE && $elt->hasAttribute('name') && $elt->getAttribute('name') == $name) {

                $element = $elt;
                break;
            }
        }

        return $element;
    }

    /**
     * Find node type
     * 
     * @param string $type
     * @return XML_ELEMENT_NODE 
     */
    public function findNodeType($type) {

        $node = null;

        $nodes = $this->xml->getElementsByTagName('simpleType');

        // find element which has name = $name
        foreach ($nodes as $elt) {

            if ($elt->nodeType == XML_ELEMENT_NODE && $elt->hasAttribute('name') && $elt->getAttribute('name') == $type) {

                $node = $elt;
                break;
            }
        }

        if ($node === null) {

            $nodes = $this->xml->getElementsByTagName('complexType');

            // find element which has name = $name
            foreach ($nodes as $elt) {

                if ($elt->nodeType == XML_ELEMENT_NODE && $elt->hasAttribute('name') && $elt->getAttribute('name') == $type) {

                    $node = $elt;
                    break;
                }
            }
        }


        return $node;
    }

    /**
     * Parse recursive
     *
     * @param DOM NODE LIST $elts
     * @return array $tab
     * @see getElements
     */
    public function parseElement($element, $withRefType = false) {

        $tab = array();

        $elts = $element->getElementsByTagName('element');
        /* if(!$elts->item(0)){
          $elts = $element;
          } */

        // this part is specific for SWVG.xsd. not use for other categories
        // TODO : improve this part !
        if ($element->getElementsByTagName('choice')->item(0)) {

            $ch = $element->getElementsByTagName('choice');

            foreach ($ch as $choiceNode) {

                $k = false;
                foreach ($choiceNode->getElementsByTagName('element') as $eltChoice) {

                    if ($eltChoice->hasAttribute('name')) {
                        $tab['Rating']['choices'][] = $eltChoice->getAttribute('name');
                        $this->parsed[$eltChoice->getAttribute('name')] = "true";
                        $k = true;
                    } else {
                        break;
                    }
                }

                if ($k) {
                    $tab['Rating']['type'] = "simple";
                    $tab['Rating']['html'] = "select";
                    $tab['Rating']['name'] = 'Rating';
                    if ($choiceNode->hasAttribute('minOccurs'))
                        $tab['Rating']['minOccurs'] = $choiceNode->getAttribute('minOccurs');
                }
            }
        }

        // walk dom list
        foreach ($elts as $elt) {

            $name = "";
            $ref = "";

            if ($elt->hasAttribute('name'))
                $name = $elt->getAttribute('name');

            if ($elt->hasAttribute('ref'))
                $ref = $elt->getAttribute('ref');

            if ($name != "" && $ref == "") {

                // parse sub categories
                if ($elt->getElementsByTagName('choice')->item(0)) {

                    if (!array_key_exists($name, $this->parsed)) {

                        $tab[$name]['choices'] = array();
                        $choiceNode = $elt->getElementsByTagName('choice')->item(0);
                        // get choices (node which content subcategories)
                        $choices = $choiceNode->getElementsByTagName('element');

                        foreach ($choices as $choice) {

                            // set subcategory name
                            if ($choice->hasAttribute('ref'))
                                $subcat = $choice->getAttribute('ref');
                            elseif ($choice->hasAttribute('name'))
                                $subcat = $choice->getAttribute('name');
                            else
                                $subcat = "";

                            $tab[$name]['choices'][] = $subcat;
                            // set subcat node as parsed
                            $this->parsed[$subcat] = "true";
                        }

                        $tab[$name]['html'] = "select";
                        $tab[$name]['type'] = "simple";
                        $tab[$name]['constraint'] = "notnull";
                        // set parent node as parsed
                        $this->parsed[$name] = "true";
                    }
                } else {

                    // check if $elt has xsd:element childs
                    if ($elt->getElementsByTagName('element')->item(0)) {

                        // check if current node has already been parsed
                        if (!array_key_exists($name, $this->parsed)) {
                            // set current node as parsed
                            $this->parsed[$name] = "true";
                            // recursive call
                            $tab[$name] = $this->parseElement($elt, $withRefType);
                            $tab[$name]['type'] = 'complex';
                            // check minOccurs value
                            if ($elt->hasAttribute('minOccurs'))
                                $tab[$name]['minOccurs'] = $elt->getAttribute('minOccurs');
                        }
                    } else {
                        // check if current node has already been parsed
                        if (!array_key_exists($name, $this->parsed)) {
                            // set $elt node as parsed
                            $this->parsed[$name] = "true";
                            // build array for $elt node
                            $tab[$name] = $this->getElements($elt);
                            $tab[$name]['type'] = 'simple';

                            // check node type
                            if ($elt->hasAttribute('type')) {

                                $type = $elt->getAttribute('type');
                                $typeNode = $this->findNodeType($type);

                                // if type is defined
                                if ($typeNode !== null) {
                                    $def = ($typeNode->getElementsByTagName('element')->item(0)) ? $this->parseElement($typeNode) : $this->getElements($typeNode);
                                    $tab[$name]['constraint'] = array(
                                        $type,
                                        $def
                                    );
                                } else {
                                    $tab[$name]['constraint'] = $elt->getAttribute('type');
                                }
                            }
                        }
                    }
                }
            }

            if ($ref != "" && $name == "") {

                $refNode = $this->findNodeByName($ref);

                if ($refNode !== null && $withRefType === true) {

                    $this->parsed[$ref] = "true";

                    $tab[$ref] = ($refNode->getElementsByTagName('element')->item(0)) ? $this->parseElement($refNode, $withRefType) : $this->getElements($refNode);

                    $tab[$ref]['type'] = ($refNode->getElementsByTagName('element')->item(0)) ? 'complex' : 'simple';

                    if ($elt->hasAttribute('minOccurs'))
                        $tab[$ref]['minOccurs'] = $elt->getAttribute('minOccurs');
                }
            }
        }

        return $tab;
    }

    /**
     * build array
     *
     * @param XML_ELEMENT_NODE $node
     * @return array $retour
     */
    public function getElements($node) {

        $retour = array();

        foreach (self::$_attributes as $attribute) {

            if ($node->hasAttribute($attribute)) {

                $retour[$attribute] = $node->getAttribute($attribute);
            }
        }

        // check restriction
        if ($node->getElementsByTagName('restriction')->item(0)) {

            $restriction = $node->getElementsByTagName('restriction')->item(0);
            $retour['restriction'] = $restriction->getAttribute('base');

            if (preg_match("/^xsd/", $retour['restriction'])) {
                $tmp = explode(":", $retour['restriction']);
                $restrictionType = $tmp[1];
            }
            else
                $restrictionType = $retour['restriction'];

            $xsdRestriction = Mage::Helper('Amazon/XSD_Restrictions');
            $xsdRestriction->setType($restrictionType);
            $xsdRestriction->setNode($node);
            $retour[$restrictionType] = $xsdRestriction->toArray();
            $retour['html'] = $xsdRestriction->getWidgetType();
        } else {

            if (!array_key_exists('type', $retour))
                $retour['type'] = "text";

            switch ($retour['type']) {

                case 'xsd:boolean':
                    $html = "select_yesno";
                    break;

                case 'LongStringNotNull':
                case 'MediumStringNotNull':
                    $html = "textarea";
                    break;

                default:
                    $html = "text";
                    break;
            }

            // add html widget type
            $retour['html'] = $html;
        }

        return $retour;
    }

    /**
     * Get subcategories
     * 
     * @return array $retour 
     */
    public function getSubcategories() {

        $retour = array();
        $findElt = false;
        $findEnum = false;
        $enumerations = null;
        $type = null;

        $elements = $this->xml->getElementsByTagName('element');

        // search ProductData node
        foreach ($elements as $elt) {

            if ($elt->nodeType == XML_ELEMENT_NODE && $elt->hasAttribute('name') && $elt->getAttribute('name') == 'ProductType') {

                if ($elt->hasAttribute('type'))
                    $type = $elt->getAttribute('type');

                if ($elt->getElementsByTagName('element')->item(0)) {
                    $elements = $elt->getElementsByTagName('element');
                    $findElt = true;
                    break;
                } elseif ($elt->getElementsByTagName('enumeration')->item(0)) {

                    $enumerations = $elt->getElementsByTagName('enumeration');
                    $findEnum = true;
                    break;
                }
            }
        }

        if ($findElt && $type === null) {
            // retrieve subcategories
            foreach ($elements as $elt) {

                if ($elt->hasAttribute('ref')) {

                    $subcategory = $elt->getAttribute('ref');
                    $retour[$subcategory] = $subcategory;
                    
                }elseif($elt->hasAttribute('name') && $elt->hasAttribute('type')){
                    
                    $subcategory = $elt->getAttribute('name');
                    $retour[$subcategory] = $subcategory;
                    
                }
            }
        } else {
            // cf miscellaneous xsd file
            $simpleTypes = $this->xml->getElementsByTagName('simpleType');

            foreach ($simpleTypes as $simpleElt) {

                if ($simpleElt->hasAttribute('name') && $simpleElt->getAttribute('name') == $type) {

                    foreach ($simpleElt->getElementsByTagName('enumeration') as $enumValue) {

                        $subcategory = $enumValue->getAttribute('value');
                        $retour[$subcategory] = $subcategory;
                    }
                }
            }
        }

        if ($findEnum) {

            foreach ($enumerations as $enum) {

                if ($enum->hasAttribute('value')) {

                    $subCategory = $enum->getAttribute('value');
                    $retour[$subCategory] = $subCategory;
                }
            }
        }

        return $retour;
    }

    /**
     * Init 
     */
    protected function init() {
        $this->parsed = array();
    }

    /**
     * get XSD
     * 
     * @return type 
     */
    public function getXSD() {
        return $this->_xsd;
    }

}