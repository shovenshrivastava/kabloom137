<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_XSD_Attributes extends Mage_Core_Helper_Abstract {
    
    /**
     * Create
     * 
     * @param string $category
     * @param string $subcategory
     * @param string $attrSet 
     */
    public function create($category, $subcategory, $attrSet){        
        
        // retrouver la category Magento associée et l'attribute set correspondant :)
        $attrIds = array();       
        
        $installer = new Mage_Eav_Model_Entity_Setup('core_write');        
        
        // creation du group
        $groupName = 'Amazon Required Fields';
        $installer->addAttributeGroup('catalog_product', $attrSet, $groupName);

        $group = Mage::getModel('Eav/Entity_Attribute_Group')->getCollection()
                    ->addFieldToFilter('attribute_group_name', $groupName)
                    ->addFieldToFilter('attribute_set_id', $attrSet)
                    ->getFirstItem();
        
        // recuperation des champs requis
        $filename = Mage::Helper('Amazon/XSD_Main')->retrieveFilename($category);  
        $filename = Mage::Helper('Amazon/XSD_Main')->getFilename($filename . '.xsd');
        $helper = new MDN_Amazon_Helper_XSD_RequiredFields($filename);
        $required = $helper->getRequiredFields($category);                
        
        $tab = ($subcategory) ? $required['subCat'][$subcategory] : $required['mainCat'];
        $subcategory = (!$subcategory && in_array($category, array('Clothing', 'ClothingAccessories'))) ? 'ClassificationData' : $subcategory;
        
        foreach($tab as $k => $array){
        
            $attrId = Mage::getModel('eav/config')->getAttribute('catalog_product', $array['name'])->getId();
            
            if(!$attrId){
                // creation des attributs
                $installer->addAttribute('catalog_product',$array['name'],array(
                        'type' 		=> 'varchar',
                        'visible' 	=> false,
                        'label'		=> $array['name'],
                        'required'  => false,
                        'default'   => '',
                        'user_defined' => true
                ));
                
                $attrId = Mage::getModel('eav/config')->getAttribute('catalog_product', $array['name'])->getId();
                
            }                        
            
            $attrIds[$attrId] = $array['name'];
            
        }                
        
        foreach($attrIds as $id => $name){                        
            
            // associtation des attributs au group        
            $installer->addAttributeToGroup('catalog_product', $attrSet, $group->getId(), $id);
        
            // insertion des associations dans la table market_place_required_fields
            if($subcategory)
                $path = $category.'_'.$subcategory.'_'.$name;
            else
                $path = $category.'_'.$name;
            
            $requiredField = Mage::getModel('MarketPlace/Requiredfields')
                                    ->getCollection()
                                    ->addFieldToFilter('mp_path',$path)
                                    ->addFieldToFilter('mp_marketplace_id', 'amazon')
                                    ->getFirstItem();

            if($requiredField->getmp_id()){
                $requiredField->setmp_attribute_name($name)
                        ->save();
            }else{
                $requiredField = Mage::getModel('MarketPlace/Requiredfields')
                        ->setmp_path($path)
                        ->setmp_marketplace_id('amazon')
                        ->setmp_attribute_name($name)
                        ->save();
            }
        }
        
    }
    
}
