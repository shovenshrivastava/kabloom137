<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_XSD_Main extends Mage_Core_Helper_Abstract {

    /**
     * Get base XSD directory
     *
     * @return string
     */
    public function getBaseXSD() {
        return Mage::getBaseDir('lib') . '/MDN/Marketplace/Amazon/XSD/Files/';
    }

    /**
     * Get filename
     *
     * @param string $xsdFile
     * @return string
     */
    public function getFilename($xsdFile) {

        return Mage::getBaseDir('lib') . '/MDN/Marketplace/Amazon/XSD/Files/' . $xsdFile;
    }

    /**
     * Retrieve main node
     *
     * @param string $category
     * @return string $retour
     */
    public function retrieveMainNode($category) {

        $retour = "";

        if ($category == "SWVG") {

            $retour = "SoftwareVideoGames";
        } elseif ($category == "ProductClothing") {

            $retour = "Clothing";
        }
        else
            $retour = $category;

        return $retour;
    }

    /**
     * Retrieve filename
     *
     * @param string $category
     * @return string $retour
     */
    public function retrieveFilename($category) {

        $retour = "";

        switch ($category) {
            case 'SoftwareVideoGames':
                $retour = 'SWVG';
                break;
            case 'Clothing':
                $retour = 'ProductClothing';
                break;
            default:
                $retour = $category;
        }

        return $retour;
    }

    /**
     * Retrieve filename and main node
     *
     * @param string $category
     * @return array $retour
     */
    public function retrieveFilenameAndMainNode($category) {

        $retour = array();

        $retour['filename'] = $category;

        $retour['category'] = $this->retrieveMainNode($category);

        return $retour;
    }

    /**
     * Get xsd filenames
     *
     * @return array $xsd
     */
    public function getFilenames() {

        $xsd = array(
            array('name' => 'AutoAccessory.xsd', 'requiredfields' => true),
            array('name' => 'Baby.xsd', 'requiredfields' => false),
            array('name' => 'Beauty.xsd', 'requiredfields' => false),
            array('name' => 'Books.xsd', 'requiredfields' => true),
            array('name' => 'CE.xsd', 'requiredfields' => true),
            array('name' => 'CameraPhoto.xsd', 'requiredfields' => false),
            array('name' => 'ClothingAccessories.xsd', 'requiredfields' => true),
            array('name' => 'Computers.xsd', 'requiredfields' => true),
            array('name' => 'EUCompliance.xsd', 'requiredfields' => false),
            array('name' => 'EntertainmentCollectibles.xsd', 'requiredfields' => true),
            array('name' => 'FBA.xsd', 'requiredfields' => false),
            array('name' => 'FoodAndBeverages.xsd', 'requiredfields' => false),
            array('name' => 'FoodServiceAndJanSan.xsd', 'requiredfields' => true),
            array('name' => 'GiftCards.xsd', 'requiredfields' => false),
            array('name' => 'Gourmet.xsd', 'requiredfields' => false),
            array('name' => 'Health.xsd', 'requiredfields' => false),
            array('name' => 'Home.xsd', 'requiredfields' => false),
            array('name' => 'HomeImprovement.xsd', 'requiredfields' => false),
            array('name' => 'Industrial.xsd', 'requiredfields' => false),
            array('name' => 'Jewelry.xsd', 'requiredfields' => false),
            array('name' => 'LabSupplies.xsd', 'requiredfields' => true),
            array('name' => 'LargeAppliances.xsd', 'requiredfields' => true),
            array('name' => 'Lighting.xsd', 'requiredfields' => false),
            array('name' => 'MaterialHandling.xsd', 'requiredfields' => false),
            array('name' => 'MechanicalFasteners.xsd', 'requiredfields' => true),
            array('name' => 'Miscellaneous.xsd', 'requiredfields' => false),
            array('name' => 'Motorcycles.xsd', 'requiredfields' => true),
            array('name' => 'Music.xsd', 'requiredfields' => true),
            array('name' => 'MusicalInstruments.xsd', 'requiredfields' => false),
            array('name' => 'Office.xsd', 'requiredfields' => false),
            array('name' => 'PetSupplies.xsd', 'requiredfields' => false),
            array('name' => 'PowerTransmission.xsd', 'requiredfields' => false),
            array('name' => 'ProductClothing.xsd', 'requiredfields' => true),
            array('name' => 'RawMaterials.xsd', 'requiredfields' => true),
            array('name' => 'SWVG.xsd', 'requiredfields' => true),
            array('name' => 'Shoes.xsd', 'requiredfields' => true),
            array('name' => 'Sports.xsd', 'requiredfields' => false),
            array('name' => 'SportsMemorabilia.xsd', 'requiredfields' => true),
            array('name' => 'TiresAndWheels.xsd', 'requiredfields' => true),
            array('name' => 'Tools.xsd', 'requiredfields' => false),
            array('name' => 'Toys.xsd', 'requiredfields' => true),
            array('name' => 'ToysBaby.xsd', 'requiredfields' => false),
            array('name' => 'Video.xsd', 'requiredfields' => true),
            array('name' => 'WineAndAlcohol.xsd', 'requiredfields' => true),
            array('name' => 'Wireless.xsd', 'requiredfields' => false)
        );

        return $xsd;
    }

    /**
     * Get all categories
     *
     * @param boolean $required
     * @return array $retour
     */
    public function getAllCategories($required = false) {

        $retour = array();

        $filenames = $this->getFilenames();

        foreach ($filenames as $filename) {
            if ($required) {
                if ($filename['requiredfields']) {
                    $tmp = explode(".", $filename['name']);
                    $retour[] = $this->retrieveMainNode($tmp[0]);
                }
            } else {
                $tmp = explode(".", $filename['name']);
                $retour[] = $this->retrieveMainNode($tmp[0]);
            }
        }

        return $retour;
    }

    /**
     * Get unit of measure for required fields
     *
     * @return array $retour
     */
    public function getUnitOfMeasureForRequiredFields() {

        $weightUnit = strtoupper(Mage::registry('mp_country')->getParam('weight_unit'));

        $retour = array(
            'PackageWeight' => $weightUnit,
            'ShippingWeight' => $weightUnit,
            'RAMSize' => 'GO',
            'HardDriveSize' => 'GO',
            'ProcessorSpeed' => 'GHz',
            'PitchCircleDiameter' => 'M',
            'FileSize' => 'MO',
            'MinimumManufacturerAgeRecommended' => 'years',
            'MaximumManufacturerAgeRecommended' => 'years'
        );

        return $retour;
    }

}
