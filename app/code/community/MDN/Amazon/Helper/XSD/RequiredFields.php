<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */

class MDN_Amazon_Helper_XSD_RequiredFields extends MDN_Amazon_Helper_XSD_Parse {

    /* @var array */
    private $required = array();
    /* @var string */
    private $requiredParentNode;

    /**
     * Constrcut
     * 
     * @param string $filename 
     */
    public function __construct($filename){

        parent::__construct($filename);

    }

    /**
     * Get required fields
     * 
     * @param string $category
     * @return array $retour 
     */
    public function getRequiredFields($category){

        $retour = array();
        $requiredFieldsMainCat = array();
        $requiredFieldsSubCat = array();
        $tmp = array();

        $this->initAll();
        $tabCat = $this->toArray($category, false);

        $requiredFieldsMainCat = $this->retrieveRequiredFields($tabCat);

        $subCategories = $this->getSubcategories();

        foreach($subCategories as $k => $v){

            $this->initAll();
            $tmp[$k] = $this->toArray($v, true);
            
        }

        foreach($tmp as $k => $v){

            $this->init();

            if($v === NULL) continue; // TODO : check why NULL ?????

           $requiredFieldsSubCat[$k] = $this->retrieveRequiredFields($v);
        }

        $retour = array(
                'mainCat' => $requiredFieldsMainCat,
                'subCat' => $requiredFieldsSubCat
         );
        
        return $retour;

    }

    /**
     * Retrieve required fields
     * 
     * @param array $tab
     * @param string $fromComplex
     * @return array 
     */
    public function retrieveRequiredFields($tab, $fromComplex = false){

        foreach($tab as $k => $widget){

            if($widget['type'] == "simple"){

                if($fromComplex === false)
                    $this->requiredParentNode = "";

                if(array_key_exists('minOccurs', $widget)){

                    if($widget['minOccurs'] > 0){
                        $widget['parentNode'] = ($this->requiredParentNode != "") ? $this->requiredParentNode.'_'.$k : $k;
                        $this->required[] = $widget;
                    }

                }
                else{
                    $widget['parentNode'] = ($this->requiredParentNode != "") ? $this->requiredParentNode.'_'.$k : $k;
                    $this->required[] = $widget;
                }
                

            }

            if($widget['type'] == "complex"){
                if((array_key_exists('minOccurs', $widget) && $widget['minOccurs'] > 0) || !array_key_exists('minOccurs', $widget)){
                    $this->requiredParentNode = ($this->requiredParentNode != "") ? $this->requiredParentNode.'_'.$k : $k;
                    $this->retrieveRequiredFields($widget, true);
                }
            }

        }

        return $this->required;

    }

    /**
     * Init    
     */
    protected function init(){
        $this->required = array();
        $this->requiredParentNode = "";
    }

    /**
     * Init all 
     */
    protected function initAll(){
        $this->init();
        parent::init();
    }

}
