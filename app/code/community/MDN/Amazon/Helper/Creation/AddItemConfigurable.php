<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2.0.1
 */
class MDN_Amazon_Helper_Creation_AddItemConfigurable extends MDN_Amazon_Helper_Creation_AddItem {   
    
    public function addItem($product, $category, $code, $params = array()) {
        
        // ajout du configurable
        // parcours des produits simples et ajout
        $retour = array();        
        $variationHelper = null;
        $variationAttributes = array();

        $manufacturerAttribute = Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_manufacturer_attribute();

        // retrieve informations                
        $productType = Mage::helper('Amazon/CategoriesAssociation')->getDefaultProductType($product);
        $recommendedBrowseNode = Mage::helper('Amazon/CategoriesAssociation')->getDefaultBrowseNode($product);

        // GENERAL
        $retour['parent']['general'] = array(
            'SKU' => $product->getsku()
        );

        // retrieve required fields
        $parsed = $this->getRequiredFields($category);

        // required fields for main category
        $requiredMainCat = $parsed['mainCat'];
        $mainCatFields = $this->_addMainCatRequiredFields($category, $requiredMainCat, $product);

        $details = array();
        $cptDetails = 0;                
        
        // FORMAT MAIN CAT FIELDS
        if(strtolower($category) == 'shoes'){
            foreach ($mainCatFields as $k => $v) {
                $cptDetails++;
                $details[$cptDetails] = $this->buildProductDataArray($k, $v);
            }
        }
        
        // ajout de la relation parent / child pour les produits configurables
        $variationHelper = Mage::Helper('Amazon/Configurables_' . ucfirst($category));
        // on recupere les attributs Amazon disponibles pour ce type de produit
        $variationAttributes = $variationHelper->getAttributes();                                
        $configurableAttributes = $variationHelper->getSuperAttributes($product);
        
        if (($productType !== NULL || $productType != "") && !array_key_exists(strtolower($productType), $variationAttributes)) {
            $details[++$cptDetails]['ProductType'] = $productType;
        }
        
        if (array_key_exists('main', $variationAttributes)) {

            if(strtolower($category) != 'home'){
                $details[++$cptDetails] = array(
                    'VariationData' => array(
                        'Parentage' => 'parent'
                    )
                );    
            }            

            // ajout du type de variation dans le produit parent            
            $details[++$cptDetails] = array(
                'VariationData' => array(
                    'VariationTheme' => $configurableAttributes['variationTheme']
                )
            );

            if(strtolower($category) == 'home'){
                $details[++$cptDetails]['Parentage'] = 'parent';
            }
        }

        // si il y a une sous-categorie
        if ($productType !== NULL && $productType != "") {

            // INSERTION DES TYPES DE VARIATION
            // si la catégorie possède des variations
            if (array_key_exists(strtolower($productType), $variationAttributes)) {                

                if(strtolower($category) != 'home'){

                    $details[++$cptDetails]['ProductType'][$productType] = array(
                        'VariationData' => array(
                            'Parentage' => 'parent'
                        )
                    );        

                }                           

                $details[++$cptDetails]['ProductType'][$productType] = array(
                    'VariationData' => array(
                        'VariationTheme' => $configurableAttributes['variationTheme']
                    )
                );

                if(strtolower($category) == 'home')
                    $details[++$cptDetails]['Parentage'] = 'parent';

            }
                        
            $subCatFields = array();            
            if(array_key_exists($productType, $parsed['subCat'])){
                // required fields for subcategory $productType
                $requiredSubCat = $parsed['subCat'][$productType];

                // retrieve values for subcategory required fields
                $subCatFields = $this->_addSubCatRequiredFields($category, $requiredSubCat, $productType, $product);    
            }

            // ADULT PRODUCT
            if ($params['isAdult'] === true) {
                $subCatFields = $this->_addAdult($category, $productType, $subCatFields);                    
            }

            // FORMAT SUB-CAT FIELDS
            foreach ($subCatFields as $k => $v) {
                $cptDetails++;
                $details[$cptDetails] = $this->buildProductDataArray($k, $v);
            }            
            
        }

        if(strtolower($category) == 'shoes'){
                
            $details[++$cptDetails]['ClassificationData'] = array();

        }
        
        // FORMAT MAIN CAT FIELDS
        if(strtolower($category) != 'shoes'){
            foreach ($mainCatFields as $k => $v) {
                $cptDetails++;
                $details[$cptDetails] = $this->buildProductDataArray($k, $v);
            }
        }

        // array which will be transformed into a node
        $retour['parent']['ProductData'] = array(
            'name' => Mage::helper('Amazon/XSD_Main')->retrieveMainNode($category),
            'details' => $details
        );

        // PRODUCT CONDITION
        $conditionAttribute = Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getconditionAttribute();
        $conditionDefaultValue = Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getconditionDefaultValue();
        if ($conditionDefaultValue)
        {
            $retour['parent']['Condition'] = $conditionDefaultValue;
        }
        else
        {
            if($conditionAttribute){

                if($product->getAttributeText($conditionAttribute)){
                    $retour['parent']['Condition'] = $product->getAttributeText($conditionAttribute);
                }else{
                    if($product->getData($conditionAttribute)){
                        $retour['parent']['Condition'] = $product->getData($conditionAttribute);
                    }
                }
            }
        }

        // DESCRIPTION
        $description = '';
        if (Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getdescriptionAttribute()) {
            $description = $product->getData(Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getdescriptionAttribute());
        } else {
            $description = $product->getdescription();
        }

        if (strlen($description) > 2000) {
            $description = strip_tags($description);
            if (strlen($description) > 2000)
                $description = substr($description, 0, 2000);
        }

        $retour['parent']['descriptionData'] = array(
            'Title' => Mage::Helper('Amazon/ProductCreation')->getProductTitle($product),
            'Brand' => $product->getAttributeText(Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_brand_attribute()),
            'Description' => $description
        );

        // POIDS
        $weight = $product->getweight();
        if ($weight !== "" && $weight != 0) {
            $retour['parent']['descriptionData']['PackageWeight'] = round($weight, 2);
            $retour['parent']['descriptionData']['ShippingWeight'] = round($weight, 2);
        }

        // MANUFACTURER
        $retour['parent']['descriptionData']['Manufacturer'] = $product->getAttributeText($manufacturerAttribute);
        // BROWSE NODE
        if ($recommendedBrowseNode) {
            $retour['parent']['descriptionData']['RecommendedBrowseNode'] = $recommendedBrowseNode;
        }    
                
        // AJOUT DES CHAMPS OPTIONNELS
        $retour['parent'] = Mage::Helper('Amazon/Fields_Optionnals')->getOptionnalsFields($retour['parent'], $product);
        
        // [DEBUT AJOUT DES ENFANTS]
        $childIds = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($product->getId());
        
        // ajout des enfants
        foreach ($childIds[0] as $childId) {

            $childProduct = Mage::getModel('catalog/product')->setStoreId(Mage::registry('mp_country')->getParam('store_id'))->load($childId);
            $childBarcode = Mage::helper('MarketPlace/Barcode')->getBarcodeForProduct($childProduct);
            $childBarcodeType = Mage::helper('Amazon/Checkbarcode')->getType($childBarcode);

            if ($childBarcodeType)
            {
                $childCode = array(
                    'type' => $childBarcodeType,
                    'value' => $childBarcode
                );
            }
            else
                $childCode = null;

            $simpleProductParams = array(
                'isAdult' => $params['isAdult'],
                'parent' => $product,
                'variationAttributes' => $variationAttributes
            );
            
            $retour['childs'][] = Mage::Helper('Amazon/Creation_AddItemSimple')->addItem($childProduct, $category, $childCode, $simpleProductParams);
            
        }
        // [FIN AJOUT DES ENFANTS]
        
        return $retour;        
        
    }
    
}
