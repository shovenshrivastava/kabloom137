<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2.0.1
 */
class MDN_Amazon_Helper_Creation_AddItemSimple extends MDN_Amazon_Helper_Creation_AddItem {

    public function addItem($product, $category, $code, $params = array()) {

        // init
        $retour = array();
        $variationHelper = null;
        $variationAttributes = array();

        if (array_key_exists('parent', $params)) {
            $variationHelper = Mage::Helper('Amazon/Configurables_' . ucfirst($category));
        }

        $variationAttributes = array_key_exists('variationAttributes', $params) ? $params['variationAttributes'] : $variationAttributes;

        // verif manufacturer
        $manufacturerAttribute = Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_manufacturer_attribute();

        // retrieve informations                
        $productType = Mage::helper('Amazon/CategoriesAssociation')->getDefaultProductType($product);
        $recommendedBrowseNode = Mage::helper('Amazon/CategoriesAssociation')->getDefaultBrowseNode($product);

        // GENERAL
        $retour['general'] = array(
            'SKU' => $product->getsku()
        );

        // retrieve required fields
        $parsed = $this->getRequiredFields($category);

        // required fields for main category
        $requiredMainCat = $parsed['mainCat'];
        $mainCatFields = $this->_addMainCatRequiredFields($category, $requiredMainCat, $product);

        $details = array();
        $cptDetails = 0;

        // FORMAT MAIN CAT FIELDS
        if (strtolower($category) == 'shoes') {
            foreach ($mainCatFields as $k => $v) {
                $cptDetails++;
                $details[$cptDetails] = $this->buildProductDataArray($k, $v);
            }
        }

        // winealcohol ex
        if ($productType !== NULL && $productType != "" && !array_key_exists(strtolower($productType), $variationAttributes) || in_array(strtolower($category), array('foodserviceandjansan'))) {

            switch (strtolower($category)) {
                case 'sports':
                case 'miscellaneous':
                case 'sportsmemorabilia':
                case 'foodserviceandjansan':
                case 'toysbaby':
                case 'rawmaterials':
                case 'powertransmission':
                    $details[++$cptDetails]['ProductType'] = $productType;
                    break;
                default:
                    $details[++$cptDetails]['ProductType'][$productType] = array();
                    break;
            }
        }

        // si c'est un produit enfant
        if (array_key_exists('parent', $params) && $params['parent'] instanceof Mage_Catalog_Model_Product) {

            // si la ctegories principale a des variations
            if (array_key_exists('main', $variationAttributes)) {

                $details[++$cptDetails] = array(
                    'VariationData' => array(
                        'Parentage' => 'child'
                    )
                );
                
                // parcours des variations possibles et ajout de celles existantes dans le produit Magento
                $tmp = array();
                foreach ($variationAttributes['main'] as $attr) {

                    // nom de la variation Amazon courante
                    $attrName = strtolower($attr);
                    // on recupere le nom de l'attribut Magento associé à cette variation
                    $configurableAttributes = $variationHelper->getSuperAttributes($params['parent']);                                                                                
                    // on verifie que cette variation existe pour le produit
                    /*if (array_key_exists($attrName, $configurableAttributes['associatedVariations']) && $product->getResource()->getAttribute($configurableAttributes['associatedVariations'][$attrName])->getFrontend()->getValue($product)) {
                        $details[++$cptDetails] = array(
                            'VariationData' => array(
                                $attr => $product->getResource()->getAttribute($configurableAttributes['associatedVariations'][$attrName])->getFrontend()->getValue($product),
                            )
                        );
                    }*/

                    if (array_key_exists($attrName, $configurableAttributes['associatedVariations']) && $product->getResource()->getAttribute($configurableAttributes['associatedVariations'][$attrName])->getFrontend()->getValue($product)) {
                        
                        if(strtolower($category) == 'toysbaby'){
                            $tmp[] =  array(
                                    $attr => $product->getResource()->getAttribute($configurableAttributes['associatedVariations'][$attrName])->setStoreId($product->getStoreId())->getFrontend()->getValue($product),
                                );

                        }else{
                        
                            $tmp[] = array(
                                'VariationData' => array(
                                    $attr => $product->getResource()->getAttribute($configurableAttributes['associatedVariations'][$attrName])->setStoreId($product->getStoreId())->getFrontend()->getValue($product),
                                )
                            );
                        }
                    }
                    
                }
                
                switch(strtolower($category)){
                        
                    case 'beauty':
                    case 'sports':
                    case 'toysbaby':
                    case 'home': 
                        $details[++$cptDetails] = array(
                            'VariationData' => array(
                                'VariationTheme' => $configurableAttributes['variationTheme']
                            )
                        );
                        foreach($tmp as $vd){
                            $details[++$cptDetails] = $vd;
                        }
                        break;
                    default:
                        
                        foreach($tmp as $vd){
                            $details[++$cptDetails] = $vd;
                        }
                        
                        $details[++$cptDetails] = array(
                            'VariationData' => array(
                                'VariationTheme' => $configurableAttributes['variationTheme']
                            )
                        );
                        break;

                }

                
            }
        }

        // GESTION DES SOUS-CATEGORIES
        if ($productType !== NULL && $productType != "") {

            $subCatFields = array();
            if(array_key_exists($productType, $parsed['subCat'])){
                // required fields for subcategory $productType
                $requiredSubCat = $parsed['subCat'][$productType];

                // retrieve values for subcategory required fields
                $subCatFields = $this->_addSubCatRequiredFields($category, $requiredSubCat, $productType, $product);
            }

            // INSERTION DES TYPES DE VARIATION (dans le cas d'un produit enfant UNIQUEMENT
            if (array_key_exists('parent', $params) && array_key_exists(strtolower($productType), $variationAttributes)) {

                if(strtolower($category) != 'home'){

                    $details[++$cptDetails]['ProductType'][$productType] = array(
                        'VariationData' => array(
                            'Parentage' => 'child'
                        )
                    );

                }    
                
                $configurableAttributes = $variationHelper->getSuperAttributes($params['parent']);
                $tmp = array();
                foreach ($variationAttributes[strtolower($productType)] as $attr) {

                    $attrName = strtolower($attr);
                    /*if (array_key_exists($attrName, $configurableAttributes['associatedVariations']) && $product->getResource()->getAttribute($configurableAttributes['associatedVariations'][$attrName])->getFrontend()->getValue($product)) {
                        $details[++$cptDetails]['ProductType'][$productType] = array(
                            'VariationData' => array(
                                $attr => $product->getResource()->getAttribute($configurableAttributes['associatedVariations'][$attrName])->getFrontend()->getValue($product),
                            )
                        );
                    }*/
                        
                    if (array_key_exists($attrName, $configurableAttributes['associatedVariations']) && $product->getResource()->getAttribute($configurableAttributes['associatedVariations'][$attrName])->getFrontend()->getValue($product)) {
                        
                        if(strtolower($category) == 'toysbaby'){
                            $tmp[] =  array(
                                    $attr => $product->getResource()->getAttribute($configurableAttributes['associatedVariations'][$attrName])->setStoreId($product->getStoreId())->getFrontend()->getValue($product),
                                );

                        }else{
                            $tmp[] = array(
                                'VariationData' => array(
                                    $attr => $product->getResource()->getAttribute($configurableAttributes['associatedVariations'][$attrName])->setStoreId($product->getStoreId())->getFrontend()->getValue($product),
                                )
                            );
                        }
                    }
                                            
                }

                switch(strtolower($category)){
                        
                    case 'beauty':
                    case 'sports':
                    case 'toysbaby':
                    case 'home' :
                        $details[++$cptDetails]['ProductType'][$productType] = array(
                            'VariationData' => array(
                                'VariationTheme' => $configurableAttributes['variationTheme']
                            )
                        );
                        foreach($tmp as $vd){
                            $details[++$cptDetails] = $vd;
                        }
                        break;
                    default:
                        
                        foreach($tmp as $vd){
                            $details[++$cptDetails] = $vd;
                        }
                        
                        $details[++$cptDetails]['ProductType'][$productType] = array(
                            'VariationData' => array(
                                'VariationTheme' => $configurableAttributes['variationTheme']
                            )
                        );
                        break;

                }

                if(strtolower($category) == 'home')
                    $details[++$cptDetails]['Parentage'] = 'child';
                
                
            }

            // ADULT PRODUCT
            if ($params['isAdult'] === true) {
                $subCatFields = $this->_addAdult($category, $productType, $subCatFields);
            }

            // FORMAT SUB-CAT FIELDS
            foreach ($subCatFields as $k => $v) {
                $cptDetails++;
                $details[$cptDetails]['ProductType'] = $this->buildProductDataArray($k, $v);
            }
        } else {
            // TODO : improve this part !
            // set index 0 see MWSFeed class line 193
            //$details[++$cptDetails]['ProductType'] = null;
        }

        if (strtolower($category) == 'shoes') {

            $details[++$cptDetails]['ClassificationData'] = array();
        }



        // FORMAT MAIN CAT FIELDS
        if (strtolower($category) != 'shoes') {
            foreach ($mainCatFields as $k => $v) {
                $cptDetails++;

                switch (strtolower($category)) {

                    case 'clothingaccessories':
                    case 'productclothing':
                    case 'toys':
                        $details[$cptDetails] = $this->buildProductDataArray($k, $v);
                        break;
                    default:
                        $details[$cptDetails]['ProductType'] = $this->buildProductDataArray($k, $v);
                        break;
                }
            }
        }

        // array which will be transformed into a node
        $retour['ProductData'] = array(
            'name' => Mage::helper('Amazon/XSD_Main')->retrieveMainNode($category),
            'details' => $details
        );

        // STANDARD PRODUCT ID          
        $retour['StandardProductID'] = array(
            'Type' => $code['type'],
            'Value' => $code['value']
        );
        
        // PRODUCT CONDITION
        $conditionAttribute = Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getconditionAttribute();
        $conditionDefaultValue = Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getconditionDefaultValue();
        if ($conditionDefaultValue)
        {
            $retour['Condition'] = $conditionDefaultValue;
        }
        else
        {
            if($conditionAttribute){

                if($product->getAttributeText($conditionAttribute)){
                    $retour['Condition'] = $product->getAttributeText($conditionAttribute);
                }else{
                    if($product->getData($conditionAttribute)){
                        $retour['Condition'] = $product->getData($conditionAttribute);
                    }
                }

            }
        }

        // DESCRIPTION
        $description = '';
        if (Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getdescriptionAttribute()) {
            $description = $product->getData(Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon')->getdescriptionAttribute());
        } else {
            $description = $product->getdescription();
        }

        if (strlen($description) > 2000) {
            $description = strip_tags($description);
            if (strlen($description) > 2000)
                $description = substr($description, 0, 2000);
        }

        $retour['descriptionData'] = array(
            'Title' => Mage::Helper('Amazon/ProductCreation')->getProductTitle($product),
            'Brand' => $product->getAttributeText(Mage::getModel('MarketPlace/Configuration')->getGeneralConfigObject()->getmp_brand_attribute()),
            'Description' => $description
        );

        // POIDS
        $weight = $product->getweight();
        if ($weight !== "" && $weight != 0) {
            $retour['descriptionData']['PackageWeight'] = round($weight, 2);
            $retour['descriptionData']['ShippingWeight'] = round($weight, 2);
        }

        // MANUFACTURER
        $retour['descriptionData']['Manufacturer'] = $product->getAttributeText($manufacturerAttribute);
        // BROWSE NODE
        if ($recommendedBrowseNode) {
            $retour['descriptionData']['RecommendedBrowseNode'] = $recommendedBrowseNode;
        }

        // AJOUT DES CHAMPS OPTIONNELS
        $retour = Mage::Helper('Amazon/Fields_Optionnals')->getOptionnalsFields($retour, $product);

        //echo '<pre>';var_dump($retour);die('</pre>');

        return $retour;
    }

}
