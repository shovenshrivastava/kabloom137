<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2.0.1
 */
abstract class MDN_Amazon_Helper_Creation_AddItem extends Mage_Core_Helper_Abstract {
    
    abstract function addItem($product, $category, $code, $params = array());
    
    /**
     * Build product data array
     *
     * @param string $k
     * @param string $v
     * @return array $retour
     * 
     */
    public function buildProductDataArray($k, $v) {

        // init
        $retour = array();

        // get sub nodes
        $tmp = explode("_", $k);

        // remove first item if this value egals "ProductData", redundant information
        if ($tmp[0] == "ProductData")
            $tmp = array_slice($tmp, 1, count($tmp) - 1);

        if (count($tmp) > 1) {

            // get first key
            $firstKey = $tmp[0];

            // build new array, remove first elt
            $newTmp = array_slice($tmp, 1, count($tmp) - 1);

            // build new key
            $newKey = implode("_", $newTmp);

            // recursive call
            $retour[$firstKey] = $this->buildProductDataArray($newKey, $v);
        } else {
            // add value
            $retour[$tmp[0]] = Mage::helper('MarketPlace/Product')->formatExportedTxt($v);
        }

        return $retour;
    }
    
    /**
     * Get required fields
     *
     * @param string $category
     * @return array $parsed
     */
    public function getRequiredFields($category) {

        $parsed = array();

        $xsdTab = Mage::helper('Amazon/XSD_Main')->retrieveFilenameAndMainNode($category);

        $filename = Mage::Helper('Amazon/XSD_Main')->getFilename($xsdTab['filename'] . '.xsd');

        $parseAmazon = new MDN_Amazon_Helper_XSD_RequiredFields($filename);
        $parsed = $parseAmazon->getRequiredFields($xsdTab['category']);

        return $parsed;
    }
    
    /**
     * Add main category requiredfields
     * 
     * @param string $category
     * @param array $requiredMainCat
     * @return array $mainCatField
     */
    protected function _addMainCatRequiredFields($category, $requiredMainCat, $product){
        
        $mainCatFields = array();
        $value = '';
        foreach ($requiredMainCat as $k => $v) {

            if ($v['parentNode'] != 'ProductType') {

                $name = Mage::Helper('Amazon/XSD_Main')->retrieveMainNode($category) . '_' . $v['parentNode'];
                $defaultValue = Mage::getModel('MarketPlace/Requiredfields')->loadDefaultForPath($name, 'amazon');

                // default value is not defined
                if ($defaultValue == "") {

                    // try to get associated attribute value
                    $attributeName = Mage::getModel('MarketPlace/Requiredfields')->loadValueForPath($name, 'amazon');

                    // missing ossociation
                    if ($attributeName == NULL || $attributeName == "") {                        
                        continue;
                    }

                    if($attributeName){
                        // get associated attribute value
                        if($product->getentity_id()){

                            $value = $product->getAttributeText($attributeName);
                            if(empty($value))
                                $value = $product->getData($attributeName);
                        }
                    }
                    
                } else {
                    
                    // default value used
                    $value = $defaultValue;
                    
                }

                //convert value if needed
                switch($v['parentNode'])
                {
                    case 'AgeRecommendation':
                        list($min, $max) = explode('-', $value);
                        $mainCatFields[$v['parentNode'].'_MinimumManufacturerAgeRecommended'] = $min;
                        $mainCatFields[$v['parentNode'].'_MaximumManufacturerAgeRecommended'] = $max;
                        break;
                    default:
                        $mainCatFields[$v['parentNode']] = $value;
                        break;
                }

            }
        }

        return $mainCatFields;
        
    }
    
    /**
     * Add subcategory required fields
     * 
     * @param string $category
     * @param array $requiredSubCat
     * @param string $productType
     * @return array $subCatFields
     */
    protected function _addSubCatRequiredFields($category, $requiredSubCat, $productType, $product){
        
        $subCatFields = array();
        $value = '';
        foreach ($requiredSubCat as $k => $v) {

            $name = Mage::Helper('Amazon/XSD_Main')->retrieveMainNode($category) . '_' . $productType . '_' . $v['parentNode'];
            $defaultValue = Mage::getModel('MarketPlace/Requiredfields')->loadDefaultForPath($name, 'amazon');

            if ($defaultValue == "") {

                $attributeName = Mage::getModel('MarketPlace/Requiredfields')->loadValueForPath($name, 'amazon');

                if ($attributeName == NULL || $attributeName == "") {
                    continue;
                }

                if($attributeName){
                    if($product->getentity_id()){

                        $value = $product->getAttributeText($attributeName);
                        if(empty($value))
                            $value = $product->getData($attributeName);
                    }
                }
                
            } else {
                $value = $defaultValue;
            }

            $subCatFields[$productType . '_' . $v['parentNode']] = $value;
        }
        
        return $subCatFields;
        
    }
    
    /**
     * Add adult product flag
     * 
     * @param string $category
     * @param string $productType
     * @param array $subCatFields
     * @return array $subCatFields
     */
    protected function _addAdult($category, $productType, $subCatFields){
        
        // ADULT PRODUCT
        $adult = Mage::Helper('Amazon/Category')->hasAdultAttribute($category, $productType);
        $path = '';
        $akeys = array();
        foreach ($adult as $ak => $av) {
            $akeys[] = $ak;
        }
        if(count($akeys) > 0){
            $path = implode('_', $akeys);
            $subCatFields[$productType . '_' . $path] = 1;
        }
        
        return $subCatFields;
        
    }
    
}
