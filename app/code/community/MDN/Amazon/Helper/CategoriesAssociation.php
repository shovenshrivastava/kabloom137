<?php

/*
 * Magento
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */

class MDN_Amazon_Helper_CategoriesAssociation extends Mage_Core_Helper_Abstract {

    /**
     * Return default browse node according to product's categories association
     * 
     * @param Mage_Catalog_Model_Product
     * @return mixed null|string
     */
    public function getDefaultBrowseNode($product) {

        $retour = null;
        $config = Mage::getModel('MarketPlace/Configuration')->getConfiguration('amazon');

        if ($config->getbrowsenodesAttribute()) {

            $browseNodeAttribute = $config->getbrowsenodesAttribute();

            if ($product->getAttributeText($browseNodeAttribute)) {
                $retour = $product->getAttributeText($browseNodeAttribute);
            } else {
                $retour = $product->getData($browseNodeAttribute);
            }
        } else {

            $data = $this->getDefaultCategoryData($product);
            $country = Mage::registry('mp_country')->getmpac_country_code();
            if (isset($data['browse_node_' . $country])) {
                $retour = Mage::Helper('Amazon/GUA_Main')->getBrowseNodeByCountry($data['browse_node_' . $country]);
            }
            
        }

        return $retour;
    }

    /**
     * Return default amazon category depending of product's categories
     * 
     * @param Mage_Catalog_Model_Product
     * @return mixed null|string
     */
    public function getDefaultCategory($product) {
        $data = $this->getDefaultCategoryData($product);
        if ($data != '') {
            $data = $data['cat'];
            $t = explode('-', $data);
            if (count($t) == 2)
                return $t[0];
        }
        else
            return null;
    }

    /**
     * Get default product type
     *
     * @param Mage_Catalog_Model_Product $product
     * @return mixed null | string
     */
    public function getDefaultProductType($product) {

        $data = $this->getDefaultCategoryData($product);
        if ($data != '') {
            $data = $data['cat'];
            $t = explode('-', $data);
            if (count($t) == 2 && $t[1] != "")
                return $t[1];
        }
        else
            return null;
    }

    /**
     * Has sub category
     *  
     * @param Mage_Catalog_Model_Product $product
     * @return boolean $retour
     */
    public function hasSubCat($product) {
        $retour = false;
        $data = $this->getDefaultCategoryData($product);
        if ($data != '') {
            $data = $data['cat'];
            $t = explode('-', $data);
            if (count($t) == 2 && $t[1] != "")
                $retour = true;
        }

        return $retour;
    }

    /**
     * Return datas association to product / categories / marketplace
     * 
     * @param Mage_Catalog_Model_Product
     * @return string
     */
    private function getDefaultCategoryData($product) {
        return mage::helper('MarketPlace/Categories')->getCategoryDataForProduct($product, mage::helper('Amazon')->getMarketPlaceName());
    }

}


