<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_Delete extends Mage_Core_Helper_Abstract {
    
    /**
     * Delete products on Amazon
     * 
     * @param array $ids
     * @return boolean
     * @throws Exception 
     */
    public function process($ids){
        
        $products = array();
        
        // retrieve skus
        foreach($ids as $id){
            
            $product = Mage::getModel('catalog/product')->load($id);
            
            $products[] = $product->getsku();
            
        }
        
        // build feed
        $merchantId = Mage::registry('mp_country')->getAssociatedAccount()->getParam('mws_merchantid');
        $feed = Mage::Helper('Amazon/MWS_Xml')->buildDeleteProducts($merchantId, $products);
        
        // save file
        $filename = 'AmazonDelete.xml';        
        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($filename),
                    'fileContent' => $feed,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );
        
        $path = Mage::app()->getConfig()->getTempVarDir().'/export/marketplace/'.Mage::helper('Amazon')->getMarketPlaceName().'/';
        $filename = $path . $filename;
        // send file
        $response = Mage::Helper('Amazon/MWS_Feeds')->submitFeed('_POST_PRODUCT_DATA_', $filename);

        $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_content($feed)
                    ->setmp_response($response->getBody())
                    ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                    ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeDeleteProduct)
                    ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName())
                    ->setmp_country(Mage::registry('mp_country')->getId());

        if ($response->getStatus() == 200) {

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);
            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedSubmitted)
                    ->setmp_feed_id($feedSubmissionId)
                    ->save();
        }
        else{

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                    ->setmp_feed_id(' - ')
                    ->save();

            throw new Exception($response->getMessage(), 4);
        }
        
        return false;                
        
    }
    
}
