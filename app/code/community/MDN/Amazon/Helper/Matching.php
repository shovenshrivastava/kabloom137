<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 * @todo : check limits, try with many products
 */
class MDN_Amazon_Helper_Matching extends MDN_MarketPlace_Helper_Matching {

    /* @var array */
    protected $_matched = array();

    /**
     * Process product matching
     * 
     * @param array $products 
     */
    public function Match($products) {

        // utilisation de l'api MWS pour recuperer les codes asin 
        $successIds = array();
        $submittedIds = array();
        $barcodeType = Mage::getModel('MarketPlace/Accounts')->load(Mage::registry('mp_country')->getmpac_account_id())->getParam('barcode_type');
        $params = array(
            'IdType' => $barcodeType
        );
        $requests = array();
        $cptFeeds = 0;
        $cptProducts = 1;
        $cpt = 0;
        $maxProductsInFeed = 5;
        $maxRequests = 15;
               
        foreach($products as $barcode => $product){
            
            if($cptFeeds < $maxRequests){
                
                if($cpt < (count($products) - 1) && $cptProducts < $maxProductsInFeed){
                    
                    $params['IdList.Id.'.$cptProducts] = $product[MDN_MarketPlace_Helper_Matching::kEAN];
                    $submittedIds[$cptFeeds][] = $product[self::kID];
                    $cptProducts++;
                    
                }else{

                    $params['IdList.Id.'.$cptProducts] = $product[MDN_MarketPlace_Helper_Matching::kEAN];
                    $submittedIds[$cptFeeds][] = $product[self::kID];
                    $cptProducts++;

                    // add products in feed                    
                    $requests[$cptFeeds] = $params;
                    $params = array(
                        'IdType' => $barcodeType
                    );
                    $cptProducts = 1;
                    $cptFeeds++;
                    
                }
                
                $cpt++;
                
            }else{
                // if more request trottled so stop
                break;
            }
            
        }

        foreach($submittedIds as $id)
            Mage::getModel('MarketPlace/Data')->addMessage($id, '', Mage::registry('mp_country')->getId());
        
        foreach($requests as $k => $params){
            
            $response = Mage::Helper('Amazon/MWS_Products')->getMatchingProductForId($params);      

            $xml = new DomDocument();
            $xml->loadXML($response->getBody());

            // response ok
            if ($response->getStatus() == 200) {

                // parcours des reponses
                foreach ($xml->getElementsByTagName('GetMatchingProductForIdResult') as $result) {

                    $id = $result->getAttribute('Id');
                    $status = $result->getAttribute('status');

                    // traitement ok
                    if ($status == "Success") {

                        if($result->getElementsByTagName('Product')->item(0)){
                            if($result->getElementsByTagName('Product')->item(0) && !$result->getElementsByTagName('Product')->item(1)){
                                $asin = $result->getElementsByTagName('ASIN')->item(0)->nodeValue;
                                $products[$id]['asin'] = $asin;
                                $this->_matched[] = $products[$id];
                                $successIds[] = $products[$id][self::kID];
                                // mise à jour du produit dans la table market_place_data
                                Mage::getModel('MarketPlace/Data')->updateStatus(array($products[$id][self::kID]), Mage::registry('mp_country')->getId(), MDN_MarketPlace_Helper_ProductCreation::kStatusPending, $asin);
                            }else{
                                // product corresponds to multiple ASINs
                                $message = '';
                                $asins = array();
                                $message .= Mage::Helper('MarketPlace')->__('This product corresponds to multiple ASINs, please select the good one : ');
                                foreach($result->getElementsByTagName('Product') as $productNode){
                                    
                                    $asins[] = $productNode->getElementsByTagName('ASIN')->item(0)->nodeValue;                                    
                                    
                                }
                                $message .= implode(',',$asins);
                                Mage::getModel('MarketPlace/Data')->updateStatus(array($products[$id][self::kID]), Mage::registry('mp_country')->getId(), MDN_MarketPlace_Helper_ProductCreation::kStatusActionRequired, null, $message);
                            }
                                
                        }else{
                            // no result...
                            $message = Mage::Helper('MarketPlace')->__('No result');
                            Mage::getModel('MarketPlace/Data')->updateStatus(array($products[$id][self::kID]), Mage::registry('mp_country')->getId(), MDN_MarketPlace_Helper_ProductCreation::kStatusInError, null, $message);
                        }
                            
                    } else {
                        // erreur lors du traitement

                        $message = '';
                        // retrieve message
                        if ($result->getElementsByTagName('Message')->item(0)) {
                            $message = $result->getElementsByTagName('Message')->item(0)->nodeValue;
                        }

                        // update status
                        Mage::getModel('MarketPlace/Data')->updateStatus(array($products[$id][self::kID]), Mage::registry('mp_country')->getId(), MDN_MarketPlace_Helper_ProductCreation::kStatusInError, null, $message);
                    }
                }
                
            } else {

                // reponse ko

                $message = '';

                if ($xml->getElementsByTagName('Error')->item(0)) {

                    $errorNode = $xml->getElementsByTagName('Error')->item(0);

                    // retrieve error type
                    if ($errorNode->getElementsByTagName('Type')->item(0)) {
                        $message .= $errorNode->getElementsByTagName('Type')->item(0)->nodeValue . ' : ';
                    }

                    // retrieve error code
                    if ($errorNode->getElementsByTagName('Code')->item(0)) {
                        $message .= $errorNode->getElementsByTagName('Code')->item(0)->nodeValue . ', ';
                    }

                    // retrieve error message
                    if ($errorNode->getElementsByTagName('Message')->item(0)) {
                        $message .= $errorNode->getElementsByTagName('Message')->item(0)->nodeValue;
                    }
                }

                // update status
                Mage::getModel('MarketPlace/Data')->updateStatus($submittedIds[$k], Mage::registry('mp_country')->getId(), MDN_MarketPlace_Helper_ProductCreation::kStatusInError, null, $message);
            }
        }
        
        // mise à jour des stocks / prix pour les produits matchés
        if (count($this->_matched) > 0) {

            $this->_addProducts();

            $request = new Zend_Controller_Request_Http();
            $request->setPost('product_ids', $successIds);
            if(!$request->getParam('isAjax'))
                Mage::Helper('Amazon/ProductUpdate')->update($request);
        }
    }
    
    /**
     * Add one product by asin code
     * used when one reference corresponds to multiple asins
     * 
     * @param int $productId
     * @param string $asin
     * @return int 0
     */
    public function matchByMpReference($productId, $asin){
        
        $i = 0;
        
        // init
        $this->_matched = array();	
            
        $item = Mage::getModel('MarketPlace/Data')->getCollection()
                    ->addFieldToFilter('mp_marketplace_id', Mage::registry('mp_country')->getmpac_id())
                    ->addFieldToFilter('mp_product_id', $productId)
                    ->getFirstItem();

        $product = Mage::getModel('catalog/product')->setStoreId(Mage::registry('mp_country')->getParam('store_id'))->load($productId);
        $this->_matched[$i] = array(
            MDN_MarketPlace_Helper_Matching::kSKU => $product->getsku(),
            MDN_MarketPlace_Helper_Matching::kNAME => $product->getname(),
            MDN_MarketPlace_Helper_Matching::kEAN => Mage::Helper('MarketPlace/Barcode')->getBarcodeForProduct($product),
            MDN_MarketPlace_Helper_Matching::kPRICE => Mage::Helper('MarketPlace/Product')->getPriceToExport($product),
            MDN_MarketPlace_Helper_Matching::kSTOCK => Mage::Helper('MarketPlace/Product')->getStockToExport($product),
            MDN_MarketPlace_Helper_Matching::kPackageWeight => $product->getweight(),
            MDN_MarketPlace_Helper_Matching::kShippingWeight => $product->getweight()
        );

        $this->_matched[$i]['asin'] = $asin;
        
        if(count($this->_matched) > 0)
            $this->_addProducts();
        
        $item->setmp_message('')
                ->setmp_marketplace_status(MDN_MarketPlace_Helper_ProductCreation::kStatusPending)
                ->save();
        
        return 0;
        
    }

    /**
     * massMatchByMpReference
     *
     * @param array $items (models array)
     * @return int
     */
    public function massMatchByMpReference($items){

        $this->_matched = array();
        $i = 0;

        foreach($items as $item){

            $product = Mage::getModel('catalog/product')->setStoreId(Mage::registry('mp_country')->getParam('store_id'))->load($item->getmp_product_id());

            $this->_matched[$i] = array(
                MDN_MarketPlace_Helper_Matching::kSKU => $product->getsku(),
                MDN_MarketPlace_Helper_Matching::kNAME => $product->getname(),
                MDN_MarketPlace_Helper_Matching::kEAN => Mage::Helper('MarketPlace/Barcode')->getBarcodeForProduct($product),
                MDN_MarketPlace_Helper_Matching::kPRICE => Mage::Helper('MarketPlace/Product')->getPriceToExport($product),
                MDN_MarketPlace_Helper_Matching::kSTOCK => Mage::Helper('MarketPlace/Product')->getStockToExport($product),
                MDN_MarketPlace_Helper_Matching::kPackageWeight => $product->getweight(),
                MDN_MarketPlace_Helper_Matching::kShippingWeight => $product->getweight()
            );

            $this->_matched[$i]['asin'] = $item->getmp_reference();

            $i++;
        }

        $this->_addProducts();

        foreach($items as $item)
            $item->setmp_marketplace_status('pending')
                ->save();

        return 0;

    }

    /**
     * Get product feed name
     *
     * @return string
     */
    protected function getProductFeedName() {
        return 'matchingProducts.xml';
    }

    /**
     * Add products
     *
     * @return int 0
     */
    protected function _addProducts() {

        // build xml file
        $content = $this->_buildProductFeed();

        // save file to export
        $fileToExport = $this->getProductFeedName();
        $history = 'matchingFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $fileToExport),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

        $this->_sendFeed();

        return 0;
    }

    /**
     * Build product feed
     *
     * @return string
     */
    protected function _buildProductFeed() {

        $account = Mage::getModel('MarketPlace/Accounts')->load(Mage::registry('mp_country')->getmpac_account_id());
        
        // add declaration
        $xml = new DOMDocument('1.0', 'utf-8');

        // create AmazonEnvelope
        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        // add attributes
        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        // add Header node
        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        // add document version to header node
        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        // add merchant identifier to header node
        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $xml->createTextNode($account->getParam('mws_merchantid'));
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        // add Message type node to AmazonEnvelope
        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Product');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

        // add purge and replace node to AmazonEnvelope
        $purgeAndReplace = $xml->createElement('PurgeAndReplace', '');
        $purgeAndReplace->appendChild($xml->createTextNode('false'));
        $amazonEnvelope->appendChild($purgeAndReplace);

        // add new products to feed
        $id = 0;
        foreach ($this->_matched as $k => $v) {

            $id++;

            // add message node
            $message = $xml->createElement('Message', '');
            $messageId = $xml->createElement('MessageID', '');
            $messageId->appendChild($xml->createTextNode($id));
            $message->appendChild($messageId);

            // add operation type node
            $operationType = $xml->createElement('OperationType', '');
            $operationType->appendChild($xml->createTextNode('Update'));
            $message->appendChild($operationType);

            // add product node
            $product = $xml->createElement('Product', '');

            $sku = $xml->createElement('SKU', '');
            $sku->appendChild($xml->createTextNode($v['sku']));
            $product->appendChild($sku);

            $standardProductId = $xml->createElement('StandardProductID', '');

            $type = $xml->createElement('Type', '');
            $type->appendChild($xml->createTextNode('ASIN'));
            $standardProductId->appendChild($type);

            $value = $xml->createElement('Value', '');
            $value->appendChild($xml->createTextNode($v['asin']));
            $standardProductId->appendChild($value);

            $product->appendChild($standardProductId);

            /*$descriptionData = $xml->createElement('DescriptionData', '');

            // title
            $title = $xml->createElement('Title');
            $title->appendChild($xml->createTextNode($v['name']));
            $descriptionData->appendChild($title);

            // description
            $description = $xml->createElement('Description', '');
            $description->appendChild($xml->createTextNode($v['name']));
            $descriptionData->appendChild($description);

            $weightUnit = strtoupper(Mage::registry('mp_country')->getParam('weight_unit'));

            if (isset($v[MDN_MarketPlace_Helper_Matching::kPackageWeight]) && $v[MDN_MarketPlace_Helper_Matching::kPackageWeight] != 0) {
                //package weight
                $packageWeight = $xml->createElement('PackageWeight', '');
                $unit = $xml->createAttribute('unitOfMeasure');
                $unit->appendChild($xml->createTextNode($weightUnit));
                $packageWeight->appendChild($unit);
                $packageWeight->appendChild($xml->createTextNode($v[MDN_MarketPlace_Helper_Matching::kPackageWeight]));
                $descriptionData->appendChild($packageWeight);
            }

            if (isset($v[MDN_MarketPlace_Helper_Matching::kShippingWeight]) && $v[MDN_MarketPlace_Helper_Matching::kShippingWeight] != 0) {
                // shipping weight
                $shippingWeight = $xml->createElement('ShippingWeight', '');
                $unit = $xml->createAttribute('unitOfMeasure');
                $unit->appendChild($xml->createTextNode($weightUnit));
                $shippingWeight->appendChild($unit);
                $shippingWeight->appendChild($xml->createTextNode($v[MDN_MarketPlace_Helper_Matching::kShippingWeight]));
                $descriptionData->appendChild($shippingWeight);
            }

            $product->appendChild($descriptionData);*/

            $message->appendChild($product);
            $amazonEnvelope->appendChild($message);
        }

        return $xml->saveXML();
    }

    /**
     * Send product feed
     *
     * @return int 0
     */
    protected function _sendFeed() {

        // add feed
        $filename = Mage::app()->getConfig()->getTempVarDir() . '/export/marketplace/' . Mage::Helper('Amazon')->getMarketPlaceName() . '/' . $this->getProductFeedName();
        $content = file_get_contents($filename);

        $feed = Mage::getModel('MarketPlace/Feed')
                ->setmp_content($content)
                ->setmp_marketplace_id(Mage::Helper(ucfirst('Amazon'))->getMarketPlaceName())
                ->setmp_date(date('Y-m-d H:i:s'), Mage::getModel('core/date')->timestamp())
                ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeProductCreation)
                ->setmp_country(Mage::registry('mp_country')->getId())
                ->save();

        $response = Mage::Helper('Amazon/MWS_Feeds')->submitFeed('_POST_PRODUCT_DATA_', $filename);

        if ($response->getStatus() == 200) {

            $tabIds = array();

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);

            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            // update product status
            foreach ($this->_matched as $matched) {

                $productId = Mage::getModel('Catalog/Product')->getIdBySku($matched['sku']);

                if (!$productId)
                    $productId = Mage::Helper('MarketPlace/UnexistingProduct')->process($matched['ean']);

                $tabIds[] = $productId;
            }

            $feed->setmp_response($response->getBody())
                    ->setmp_feed_id($feedSubmissionId)
                    ->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedSubmitted)
                    ->save();
        }else {

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                    ->save();
        }

        return 0;
    }

}
