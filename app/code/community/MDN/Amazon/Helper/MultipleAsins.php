<?php

/**
 * Class MDN_Amazon_Helper_MultipleAsins
 *
 * @author Nicolas Mugnier <nicolas@boostmyshop.com>
 */
class MDN_Amazon_Helper_MultipleAsins extends Mage_Core_Helper_Abstract
{

    /**
     * Select asins
     *
     * @param array $selectedAsin
     */
    public function select($selectedAsin)
    {

        if (is_array($selectedAsin) && count($selectedAsin) > 0) {

            $toMatch = array();

            foreach ($selectedAsin as $id => $asin) {

                $item = Mage::getModel('MarketPlace/Data')->load($id);
                $item->setmp_reference($asin);
                $item->setmp_message('');
                $item->setmp_marketplace_status('not_created');
                $item->save();

                $toMatch[] = $item;

            }


            Mage::Helper('Amazon/Matching')->massMatchByMpReference($toMatch);

        }

    }

}