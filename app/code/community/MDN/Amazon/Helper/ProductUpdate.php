<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */

class MDN_Amazon_Helper_ProductUpdate extends MDN_MarketPlace_Helper_ProductUpdate {
    
    /**
     * Get export path name
     *
     * @return string
     *
     */
    public function getExportPath(){
        return Mage::app()->getConfig()->getTempVarDir().'/export/marketplace/'.Mage::helper('Amazon')->getMarketPlaceName().'/';
    }

    /**
     * Retrieve products to update on amazon marketplace
     * - simple products
     * - visibility : search, catalog, nowhere, catalog/search
     * - mp_reference not null in market_place_data table
     *
     * @return collection
     */
    public function getProductsToExport() {

        return mage::helper('MarketPlace/Product')->getProductsToExport(Mage::registry('mp_country')->getId());

    }

    /**
     * Update stock and price
     * 
     * @param request $request
     * @return int $nbr
     */
    public function update($request = null){
        
        // load products
        $products = $this->getProducts($request, true);
        $nbr = count($products);
        $i = 0;        
        
        try{                                

            // check last export
            Mage::Helper('Amazon/CheckResponse')->checkExport();

            if($nbr > 0){

                // before update
                $this->beforeUpdate();

                // curency
                $configCurrency = Mage::Helper('MarketPlace/Currency')->getCurrencyForCurrentCountry();

                // 1 - construction de l'entete des 2 fichiers xml
                // 2 - parcours des produits et construction des flux au fur et à mesure
                // 3 - sauvegarde
                // 4 - envoie des 2 flux
                $xmlInventory = $this->getXmlInventory();
                $xmlPrice = $this->getXmlPrice();

                foreach($products as $product){

                    $stockTab = array();
                    $priceTab = array();

                    $sku = trim($product->getsku());

                    // STOCK
                    $stockTab[$i]['sku'] = $sku;
                    $delay = mage::helper('MarketPlace/Product')->getDelayToExport($product);
                    $stock = mage::helper('MarketPlace/Product')->getStockToExport($product, Mage::Helper('Amazon')->getMarketPlaceName());
                    $stockTab[$i]['stock'] = $stock;
                    $stockTab[$i]['delay'] = $delay;
                    // add item
                    $this->_addItemXmlInventory($xmlInventory, $stockTab[$i], $i+1);                

                    // stock last stock & last delay sent
                    $this->_data[$sku]['stock'] = $stock;
                    $this->_data[$sku]['delay'] = $delay;

                    // PRICE
                    $priceTab[$i]['sku'] = $sku;
                    $productPrice = str_replace(',','.',mage::helper('MarketPlace/Product')->getPriceToExport($product));                                       
                    $priceTab[$i]['price'] = $productPrice;
                    // add item
                    $this->_addItemXmlPrice($xmlPrice, $priceTab[$i], $i+1, $configCurrency);

                    // stock last price sent
                    $this->_data[$sku]['price'] = $productPrice;

                    $i++;

                }

                // sauvegarde flux de stock
                $contentInventory = $xmlInventory->saveXML();
                mage::getModel('MarketPlace/Logs')->saveFile(
                        array(
                            'filenames' => array($this->getInventoryFeedName()),
                            'fileContent' => $contentInventory,
                            'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                            'type' => 'export'
                        )
                );
                // update
                $this->updateMarketPlaceStock($this->getInventoryFeedName());

                // sauvegarde flux de prix
                $contentPrice = $xmlPrice->saveXML();
                mage::getModel('MarketPlace/Logs')->saveFile(
                        array(
                            'filenames' => array($this->getPriceFeedName()),
                            'fileContent' => $contentPrice,
                            'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                            'type' => 'export'
                        )
                );
                // update 
                $this->updateMarketPlacePrices($this->getPriceFeedName());

                // after update
                $this->afterUpdate();
                        
            }

        }catch(Exception $e){
            throw new Exception($e->getMessage().' : '.$e->getCode());
        }            
            
     
        return $nbr;

    }
    
    /**
     * get inventory XML
     * 
     * @return DomDocument $xml
     */
    public function getXmlInventory(){
        
        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        
        $merchantId = Mage::getModel('MarketPlace/Countries')->getAccountByCountryId(Mage::registry('mp_country')->getId())->getParam('mws_merchantid');
        
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Inventory');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);
        
        return $xml;
        
    }
    
    /**
     * Add an item into inventory feed
     * 
     * @param DomDocument $xml
     * @param array $tab
     * @param int $id 
     */
    protected function _addItemXmlInventory(&$xml, $tab, $id){

        $message = $xml->createElement('Message', '');

        $messageId = $xml->createElement('MessageID');
        $txtMessageId = $xml->createTextNode($id);
        $messageId->appendChild($txtMessageId);
        $message->appendChild($messageId);

        $operationType = $xml->createElement('OperationType', '');
        $operationType->appendChild($xml->createTextNode('Update'));
        $message->appendchild($operationType);

        $inventory = $xml->createElement('Inventory', '');

        // add sku
        $sku = $xml->createElement('SKU');
        $sku->appendChild($xml->createTextNode($tab['sku']));
        $inventory->appendChild($sku);

        // add stock
        $quantity = $xml->createElement('Quantity', '');
        $quantity->appendChild($xml->createTextNode((int) $tab['stock']));
        $inventory->appendChild($quantity);

        // add delay
        $fulfillmentLatency = $xml->createElement('FulfillmentLatency', '');
        $fulfillmentLatency->appendChild($xml->createTextNode($tab['delay']));
        $inventory->appendChild($fulfillmentLatency);

        $message->appendChild($inventory);
        
        $xml->getElementsByTagName('AmazonEnvelope')->item(0)->appendChild($message);
        
    }
    
    /**
     * get price XML
     * 
     * @return DomDocument $xml
     */
    public function getXmlPrice(){
        
        $xml = new DomDocument('1.0', 'utf-8');

        $amazonEnvelope = $xml->createElement('AmazonEnvelope', '');

        $xmlxsi = $xml->createAttribute('xmlns:xsi');
        $xmlxsiValue = $xml->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $xml->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $xml->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $xml->appendChild($amazonEnvelope);

        $header = $xml->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion = $xml->createElement('DocumentVersion', '');
        $txtDocumentVersion = $xml->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier = $xml->createElement('MerchantIdentifier');
        
        $merchantId = Mage::getModel('MarketPlace/Countries')->getAccountByCountryId(Mage::registry('mp_country')->getId())->getParam('mws_merchantid');
        
        $txtMerchantIdentifier = $xml->createTextNode($merchantId);
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType = $xml->createElement('MessageType');
        $txtMessageType = $xml->createTextNode('Price');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);        
        
        return $xml;
        
    }
    
    /**
     * Add item inot price xml
     * 
     * @param DomDocument $xml
     * @param array $tab
     * @param int $id
     * @param string $configCurrency 
     */
    protected function _addItemXmlPrice(&$xml, $tab, $id, $configCurrency){

        $message = $xml->createElement('Message', '');

        $messageId = $xml->createElement('MessageID');
        $txtMessageId = $xml->createTextNode($id);
        $messageId->appendChild($txtMessageId);
        $message->appendChild($messageId);

        $price = $xml->createElement('Price', '');

        $sku = $xml->createElement('SKU');
        $sku->appendChild($xml->createTextNode($tab['sku']));
        $price->appendChild($sku);

        $standardPrice = $xml->createElement('StandardPrice', '');
        $currency = $xml->createAttribute('currency');
        $currencyValue = $xml->createTextNode($configCurrency);
        $currency->appendChild($currencyValue);
        $standardPrice->appendChild($currency);

        $standardPrice->appendChild($xml->createTextNode($tab['price']));
        $price->appendChild($standardPrice);

        $message->appendChild($price);     

        $xml->getElementsByTagName('AmazonEnvelope')->item(0)->appendChild($message);                
        
    }

    /**
     * Export stocks (CRON)
     * 
     * @deprecated since version 2
     *
     */
    public function exportStocks() {
        
        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*try {

            $products = $this->getProductsToExport();
            $filename = $this->getInventoryFeedName();
            $this->setInventoryFeed($products, $filename);
            $this->updateMarketPlaceStock($filename);
            
        } catch (Exception $e) {

            $errorCode = 0;

            if (preg_match('/^Notice/', $e->getMessage()))
                $errorCode = 98;
            elseif (preg_match('/^Warning/', $e->getMessage()))
                $errorCode = 99;
            else
                $errorCode = $e->getCode();

            throw new Exception($e->getMessage(), $errorCode);

        }*/
    }

    /**
     * Retrieve inventory feed name
     *
     * @return string
     */
    public function getInventoryFeedName() {
        return 'inventoryFeed.xml';
    }

    /**
     * Build & save inventory feed
     * 
     * @param collection $products
     * @param string $filename     
     *
     */
    public function setInventoryFeed($products, $filename) {

        $tab = array();
        $id = Mage::registry('mp_country')->getAssociatedAccount()->getParam('mws_merchantid');
        $i = 0;
        foreach ($products as $product) {

            $delay = mage::helper('MarketPlace/Product')->getDelayToExport($product);
            $delay = ($delay <= 0) ? 1 : $delay; // delay must be gretter than 0 !
            
            $tab[$i]['sku'] = $product->getsku();
            $tab[$i]['stock'] = mage::helper('MarketPlace/Product')->getStockToExport($product, Mage::Helper('Amazon')->getMarketPlaceName());
            $tab[$i]['delay'] = $delay;                    

            $i++;
        }       

        $content = Mage::Helper('Amazon/MWS_Xml')->buildInventoryFeed($id, $tab);

        $history = 'inventoryFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $filename),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

    }

    /**
     * Update marketplace stock
     *
     * @param string $filename
     *
     */
    public function updateMarketPlaceStock($filename) {

        $response = '';

        $file = $filename;
        $path = $this->getExportPath();
        $filename = $path . $file;
        $content = file_get_contents($filename);

        $params = array(
            'Action' => 'SubmitFeed',
            'FeedType' => '_POST_INVENTORY_AVAILABILITY_DATA_'
        );

        $response = Mage::Helper('Amazon/MWS_Feeds')->sendFeed($params, $filename);

        $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_content($content)
                    ->setmp_response($response->getBody())
                    ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                    ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeUpdateStockPrice)
                    ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName())
                    ->setmp_country(Mage::registry('mp_country')->getId());

        if ($response->getStatus() == 200) {

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);
            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedSubmitted)
                    ->setmp_feed_id($feedSubmissionId)
                    ->save();
        }
        else{

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                    ->setmp_feed_id(' - ')
                    ->save();

            throw new Exception($response->getMessage(), 4);
        }

        return $response;
    }

    /**
     * Export prices (CRON)
     * 
     * @deprecated since version 2
     *
     */
    public function exportPrices() {
        
        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*try {
            $products = $this->getProductsToExport();
            $filename = $this->getPriceFeedName();
            $this->setPriceFeed($products, $filename);
            $this->updateMarketPlacePrices($filename);
        } catch (Exception $e) {

            $errorCode = 0;

            if (preg_match('/^Notice/', $e->getMessage()))
                $errorCode = 98;
            elseif (preg_match('/^Warning/', $e->getMessage()))
                $errorCode = 99;
            else
                $errorCode = $e->getCode();

            throw new Exception($e->getMessage(), $errorCode);

        }*/
    }

    /**
     * Retrieve price feed name
     *
     * @return string
     */
    public function getPriceFeedName() {
        return 'priceFeed.xml';
    }

    /**
     * Build & save price feed
     *
     * @param collection $products
     * @param string $filename
     *
     */
    public function setPriceFeed($products, $filename) {

        $tab = array();
        $id = Mage::registry('mp_country')->getAssociatedAccount()->getParam('mws_merchantid');
        $i = 0;
        foreach ($products as $product) {

            $tab[$i]['sku'] = $product->getsku();
            $productPrice = mage::helper('MarketPlace/Product')->getPriceToExport($product);

            $productPrice = str_replace(",", ".", $productPrice);                    
            
            $tab[$i]['price'] = $productPrice;

            $i++;
        }

        $content = Mage::Helper('Amazon/MWS_Xml')->buildPriceFeed($id, $tab);

        $history = 'priceFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $filename),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );
    }

    /**
     * Update marketplace prices
     *
     * @param string $filename
     *
     */
    public function updateMarketPlacePrices($filename) {

        $file = $filename;
        $path = $this->getExportPath();
        $filename = $path . $file;
        $content = file_get_contents($filename);

        $params = array(
            'Action' => 'SubmitFeed',
            'FeedType' => '_POST_PRODUCT_PRICING_DATA_'
        );
        
        $response = Mage::Helper('Amazon/MWS_Feeds')->sendFeed($params, $filename);

        $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_content($content)
                    ->setmp_response($response->getBody())
                    ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                    ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeUpdateStockPrice)
                    ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName())
                    ->setmp_country(Mage::registry('mp_country')->getId());

        if ($response->getStatus() == 200) {

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);
            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedSubmitted)
                    ->setmp_feed_id($feedSubmissionId)
                    ->save();

            return $response;
        }
        else{

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                    ->setmp_feed_id(' - ')
                    ->save();

            throw new Exception($response->getMessage(), 4);
        }

    }

    /**
     * Export image
     * 
     * @deprecated since version 2
     */
    public function exportImage(){
        
        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*try {
            $products = $this->getProductsToExport();
            $filename = $this->getImageFeedName();
            $this->setImageFeed($products, $filename);
            $this->updateImage($filename);
        } catch (Exception $e) {

            $errorCode = 0;

            if (preg_match('/^Notice/', $e->getMessage()))
                $errorCode = 98;
            elseif (preg_match('/^Warning/', $e->getMessage()))
                $errorCode = 99;
            else
                $errorCode = $e->getCode();

            throw new Exception($e->getMessage(), $errorCode);
            
        }*/
    }

    /**
     * Get image feed name
     *
     * @return string
     */
    public function getImageFeedName(){
        return 'imageFeed.xml';
    }

    /**
     * Set image feed
     *
     * @param array $products
     * @param string $filename
     */
    public function setImageFeed($products, $filename){

        $tab = array();
        $id = Mage::registry('mp_country')->getAssociatedAccount()->getParam('mws_merchantid');

        $content = Mage::Helper('Amazon/MWS_Xml')->buildImageFeed($id, $products);

        $history = 'imageFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $filename),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

    }

    /**
     * Update image
     *
     * @param string $filename
     * @return object $response
     */
    public function updateImage($filename){

        $file = $filename;
        $path = $this->getExportPath();
        $filename = $path . $file;
        $content = file_get_contents($filename);

        $params = array(
            'Action' => 'SubmitFeed',
            'FeedType' => '_POST_PRODUCT_IMAGE_DATA_'
        );

        $response = Mage::Helper('Amazon/MWS_Feeds')->sendFeed($params, $filename);

        $feed = Mage::getModel('MarketPlace/Feed')
                    ->setmp_content($content)
                    ->setmp_response($response->getBody())
                    ->setmp_date(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp()))
                    ->setmp_type(MDN_MarketPlace_Helper_Feed::kFeedTypeMedia)
                    ->setmp_marketplace_id(Mage::helper('Amazon')->getMarketPlaceName())
                    ->setmp_country(Mage::registry('mp_country')->getId());

        if ($response->getStatus() == 200) {

            $xml = new DomDocument();
            $report = $response->getBody();
            $xml->loadXML($report);
            $feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedDone)
                    ->setmp_feed_id($feedSubmissionId)
                    ->save();
            
            return $response;
        }
        else{

            $feed->setmp_status(MDN_MarketPlace_Helper_Feed::kFeedError)
                    ->setmp_feed_id(' - ')
                    ->save();

            throw new Exception($response->getMessage(), 4);
        }

    }

    /**
     * Update stock and price for ids $ids
     *
     * @param array $ids
     * @return int 0
     * @deprecated since version 2
     */
    public function updateStockPriceFromGrid($ids){

        throw new Expetion(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*$t_stock = array();
        $t_price = array();
        $error = false;
        $message = '';
        $errorMsg = '';
        $merchant_id = Mage::getModel('MarketPlace/Countries')->getAccountByCountryId(Mage::registry('mp_country')->getId())->getParam('mws_merchantid');       

        $store_id = Mage::registry('mp_country')->getParam('store_id');
        
        $i = 0;
        foreach($ids as $id){

            $product = Mage::getModel('Catalog/Product')->setStoreId($store_id)->load($id);

            // stock
            $t_stock[$i]['sku'] = $product->getsku();
            $t_stock[$i]['stock'] = mage::helper('MarketPlace/Product')->getStockToExport($product, Mage::Helper('Amazon')->getMarketPlaceName());
            $t_stock[$i]['delay'] = mage::helper('MarketPlace/Product')->getDelayToExport($product);

            // price
            $t_price[$i]['sku'] = $product->getsku();
            $productPrice = mage::helper('MarketPlace/Product')->getPriceToExport($product);
            $productPrice = str_replace(",", ".", $productPrice);
            // add marketplace coef
            $coef = Mage::registry('mp_country')->getParam('price_coef');
            if($coef && $coef > 0){
                $productPrice = round($coef * $productPrice, 2);
            }            
            $t_price[$i]['price'] = $productPrice;

            $i++;
        }

        // save stock file
        $stock_content = Mage::Helper('Amazon/MWS_Xml')->buildInventoryFeed($merchant_id, $t_stock);

        $history = 'inventoryFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $this->getInventoryFeedName()),
                    'fileContent' => $stock_content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

        // send stock file
        try{

            $this->updateMarketPlaceStock($this->getInventoryFeedName());
            
        }catch(Exception $e){

            $error = true;
            $errorMsg .= 'Update Stock form grid : '.$e->getMessage();

        }

        // save price file
        $price_content = Mage::Helper('Amazon/MWS_Xml')->buildPriceFeed($merchant_id, $t_price);

        $history = 'priceFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $this->getPriceFeedName()),
                    'fileContent' => $price_content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

        // send price file
        try{

            $this->updateMarketPlacePrices($this->getPriceFeedName());
            
        }catch(Exception $e){

            $error = true;
            $errorMsg .= 'Update Price form grid : '.$e->getMessage();

        }

        if($error){
            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::helper('Amazon')->getMarketPlaceName(),
                    MDN_MarketPlace_Model_Logs::kIsError,
                    Mage::Helper('MarketPlace')->__($errorMsg),
                    MDN_MarketPlace_Model_Logs::kScopeUpdate,
                    array('fileName' => NULL)
            );
            $message = $errorMsg;

            throw new Exception($errorMsg);
        }else{

            $t_ids = array();

            //update incomplete status
            $collection = Mage::getModel('MarketPlace/Data')->getCollection()
                                ->addFieldToFilter('mp_marketplace_id', 'amazon')
                                ->addFieldToFIlter('mp_marketplace_status', MDN_MarketPlace_Helper_ProductCreation::kStatusIncomplete)
                                ->addFieldToFilter('mp_product_id', array('in', $ids));

            foreach($collection as $item){

                $item->setmp_marketplace_status(MDN_MarketPlace_Helper_ProductCreation::kStatusCreated)
                        ->save();

                $t_ids[] = $item->getmp_product_id();

            }

            // send image feed for incomplete products
            if(count($t_ids) > 0){
                $this->updateImageFromGrid($t_ids);
            }

        }

        return 0;*/

    }

    /**
     * Update image for ids $ids
     *
     * @param array $ids
     * @return int 0
     */
    public function updateImageFromGrid($ids){

        $products = array();
        $merchant_id = Mage::getModel('MarketPlace/Countries')->getAccountByCountryId(Mage::registry('mp_country')->getId())->getParam('mws_merchantid');

        foreach($ids as $id){

            $products[] = Mage::getModel('Catalog/Product')->load($id);

        }

        $content = Mage::Helper('Amazon/MWS_Xml')->buildImageFeed($merchant_id, $products);

        $history = 'imageFeed_' . date('Y-m-d_H:i:s') . '.xml';

        mage::getModel('MarketPlace/Logs')->saveFile(
                array(
                    'filenames' => array($history, $this->getImageFeedName()),
                    'fileContent' => $content,
                    'marketplace' => Mage::helper('Amazon')->getMarketPlaceName(),
                    'type' => 'export'
                )
        );

        $this->updateImage($this->getImageFeedName());

        return 0;

    }

    /**
     * Get marketplace name
     *
     * @return string
     */
    public function getMp(){

        if($this->_mp === null){

            $this->_mp = Mage::registry('mp_country')->getId();

        }

        return $this->_mp;

    }
 

}