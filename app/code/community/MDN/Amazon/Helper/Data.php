<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Helper_Data extends MDN_MarketPlace_Helper_Abstract {

    /**
     * Get product URL
     * 
     * @param string $asin
     * @param MDN_MarketPlace_Model_Countries $country
     * @return string $html
     */
    public function getProductUrl($asin, $country){
        $html = '';
        
        $html .= '<a href="http://'.$country->getParam('domain').'/gp/product/'.$asin.'" target="_blank">'.$asin.'</a>';
        
        return $html;
    }
    
    /**
     * Create tracking file
     *
     * @param unknown_type $orderIds
     * @return string
     * @todo : useless ?
     */
    public function createFlatTrackingFile($orderIds) {

        $lineReturn = "\r\n";
        $retour = 'order-id	order-item-id	quantity	ship-date	carrier-code	carrier-name	tracking-number	ship-method' . $lineReturn;

        if (!$orderIds)
            return $retour;

        foreach ($orderIds as $orderId) {
            //init vars
            $lineModel = 'order_ref	order_item_id	qty	ship_date	carrier_code	carrier_name	tracking	ship_method';
            $order = mage::getModel('sales/order')->load($orderId);

            //retrieve shipment
            $shipment = null;
            foreach ($order->getShipmentsCollection() as $item) {
                $shipment = $item;
                break;
            }

            if ($shipment == null) {
                continue;
            }

            //add items
            foreach ($order->getAllItems() as $item) {
                $line = $lineModel;
                $line = str_replace('order_ref', $order->getmarketplace_order_id(), $line);
                $line = str_replace('order_item_id', $item->getmarketplace_item_id(), $line);
                $line = str_replace('qty', (int) $item->getqty_ordered(), $line);

                $shipDate = date('Y-m-d', strtotime($shipment->getcreated_at()));
                $line = str_replace('ship_date', $shipDate, $line);

                $line = str_replace('carrier_code', 'Other', $line);
                $line = str_replace('carrier_name', 'La Poste', $line);

                $tracking = '';
                foreach ($order->getTracksCollection() as $track) {
                    if (is_object($track->getNumberDetail()))
                        $tracking = $track->getNumberDetail()->gettracking();
                }

                $line = str_replace('tracking', $tracking, $line);
                $line = str_replace('ship_method', 'Colissimo', $line);

                $retour .= $line . $lineReturn;
            }
        }

        return $retour;
    }

    /**
     * Return product export file content
     *
     * @return string
     */
    public function getProductFile() {

        $content = '';
        $content .= "sku\tprice\tquantity\titem-condition\r\n";

        $fileName = 'export' . $this->getMarketPlaceName() . '-' . date('Y-m-d_H:i:s') . '.csv';

        $collection = mage::helper('Amazon/ProductUpdate')->getProductsToExport();

        if ($collection->count() == 0)
            throw new Exception('No product to export.', 0);

        foreach ($collection as $product) {
            
            //collect datas
            $sku = $product->getSku();

            $price = mage::helper('MarketPlace/Product')->getPriceToExport($product);
            $stock = mage::helper('MarketPlace/Product')->getStockToExport($product);

            $content .= $sku . "\t" . $price . "\t" . $stock . "\t11\r\n";
        }

        mage::getModel('MarketPlace/Logs')->saveFile(array(
            'filenames' => array(
                $fileName
            ),
            'fileContent' => $content,
            'type' => 'export',
            'marketplace' => $this->getMarketPlaceName()
        ));

        return $content;
    }

    /**
     * Parse XSD
     *
     * @param string $value
     * @param string $parentNode
     * @return array
     */
    public function parseXSD($value, $parentNode) {

        $filename = Mage::Helper('Amazon/XSD_Parse')->getFilename($value.'.xsd');
        $parseAmazon = new MDN_Amazon_Helper_XSD_Parse($filename);
        $parsed = $parseAmazon->toArray($parentNode);
        return $parsed;
    }

    /**
     * Define if the market place need custom association with categories
     *
     * @return boolean
     */
    public function needCategoryAssociation() {
        return true;
    }

    /**
     * Form controls to associate category
     *
     * @param string $name
     * @param string $value
     *
     * @return string $html
     */
    public function getCategoryForm($name, $value) {

        $defaultCat = (is_array($value) && array_key_exists('cat', $value)) ? $value['cat'] : '';

        $html = '';
        $currentGrp = '';
        
        //add category & sub category combo
        $browseNode = mage::helper('Amazon/GUA_Main')->getAllCategoriesSubCategories();
        if(count($browseNode) > 0){
            $html = '<select name="' . $name . '[cat]" id="' . $name . '[cat]">';        
            foreach ($browseNode as $itemKey => $itemName) {
                
                $selected = '';
                if ($itemKey == $defaultCat)
                    $selected = ' selected ';
                $html .= '<option value="' . $itemKey . '" ' . $selected . '>' . $itemName . '</option>';
                
            }
            $html .= '</select>';
        }else{
            $html .= '<span>'.$this->__('No category available').'</span>&nbsp;<a href="'.Mage::Helper('adminhtml')->getUrl('MarketPlace/Configuration', array()).'">Add</a><br/>';
        }
        
        $collection = Mage::getModel('MarketPlace/Countries')->getCollection()->distinct(true);
        $collection->getSelect()->group('mpac_country_code');        
        
        $html .= '<div class="grid" style="margin-top:20px;">';
        $html .= '<table class="data" cellspacing="0" width="100%">';
        $html .= '<thead><tr class="headings">';
        
        $html .= '<th>'.$this->__('Country').'</th><th>'.$this->__('Browsenodes').'</th><th>'.$this->__('Search').'</th></tr></thead>';
        $html .= '<tbody>';
        
        $cpt = 0;
        foreach($collection as $item){
            
            $class = ($cpt%2==0)?'class="even"':'';
            $html .= '<tr '.$class.'>';
            $html .= '<td>'.$item->getmpac_country_code().'</td>';
            
            $browseNodes = Mage::Helper('Amazon/GUA_Main')->getBrowseNodes($item->getmpac_country_code());
            
            $html .= '<td>';
            $html .= '<select name="' . $name . '[browse_node_'.$item->getmpac_country_code().']" id="' . $name . '[browse_node_'.$item->getmpac_country_code().']">';

            foreach($browseNodes as $code => $label){

                $tmp = explode('/', $label);
                if($currentGrp != trim($tmp[0])){
                    $html .= '<optgroup label="'.trim($tmp[0]).'">';
                    $currentGrp = trim($tmp[0]);
                }
                $selected = (is_array($value) && array_key_exists('browse_node_'.$item->getmpac_country_code(), $value) && $code == $value['browse_node_'.$item->getmpac_country_code()]) ? 'selected' : '';
                $html .= '<option value="' . $code . '" ' . $selected . '>' . $label . '</option>';
                if($currentGrp != trim($tmp[0])){
                    $html .= '</optgroup>';
                }

            }

            $html .= '</select></td>';
            $html .= '<td>';
            $html .= ' <input type="button" value="Prev" onclick="findBrowseNode(\'' . $name . '[browse_node_'.$item->getmpac_country_code().']\', false, -1);"> <input type="text" size="10" id="search_' . $name . '[browse_node_'.$item->getmpac_country_code().']" onkeyup="findBrowseNode(\'' . $name . '[browse_node_'.$item->getmpac_country_code().']\', true, 1);"> <input type="button" value="Next" onclick="findBrowseNode(\'' . $name . '[browse_node_'.$item->getmpac_country_code().']\', false, 1);">';
            $html .= '</td>';
            $html .= '</tr>';
            $cpt++;
        }
        $html .= '</tbody></table></div>';
        $html .= '<br/><a href="http://www.boostmyshop.com/docs/controller.php?action=index&autoSelectPath=%2Fdocs%2FAmazon%2F7+-+FAQ%2F" target="_blanck">'.$this->__('See available browsenodes by country').'</a>';
        
        return $html;
    }

    /**
     * Get marketplace name
     *
     * @return string
     */
    public function  getMarketPlaceName() {
        return 'amazon';
    }

    /**
     * Get marketplace orders
     *
     * @return array
     */
    public function getMarketPlaceOrders(){
        return Mage::helper('Amazon/Orders')->getMarketPlaceOrders();
    }

    /**
     * Import marketplace orders
     *
     * @param array $orders
     * @return <type>
     */
    public function importMarketPlaceOrders($orders){
        return Mage::helper('Amazon/Orders')->importMarketPlaceOrders($orders);
    }

    /**
     * Update stock and prices
     */
    public function updateStocksAndPrices(){
        return Mage::helper('Amazon/ProductUpdate')->update();
    }

    /**
     * Send tracking
     */
    public function sendTracking(){
        Mage::Helper('Amazon/Tracking')->sendTracking();
    }

    /**
     * Check product creation
     */
    public function checkProductCreation(){
        return Mage::helper('Amazon/ProductCreation')->importCreatedProducts();
    }

    /**
     * Is marketplace allow manual update
     *
     * @return boolean
     */
    public function allowManualUpdate(){
        return true;
    }
    
    /**
     * Is marketplace allow order importation ?
     *
     * @return boolean
     */
    public function allowImportOrders(){
        return (Mage::registry('mp_country')->getParam('enable_order_importation') == 1);
    }

    /**
     * Is marketplace allow stocks and prices update ?
     *
     * @return boolean
     */
    public function allowUpdateStocksAndPrices(){
        return (Mage::registry('mp_country')->getParam('enable_product_update') == 1);
    }

    /**
     * Is marketplace allow trackings ?
     *
     * @return boolean
     */
    public function allowTracking(){
        return (Mage::registry('mp_country')->getParam('enable_tracking_export') == 1);
    }

    /**
     * Is marketplace allow product creation ?
     *
     * @return boolean
     */
    public function allowProductCreation(){                 
        return (Mage::registry('mp_country')->getParam('enable_product_creation') == 1);
    }
    
    /**
     * Amazon allow request feed submission result from grid
     * 
     * @return boolean 
     */
    public function allowRequestFeedSubmissionResultFromGrid(){
        return true;
    }
    
    /**
     * Set current country
     * 
     * @param MDN_MarketPlace_Model_Countries $country 
     * @deprecated
     */
    public function setCurrentAccount($country){
        
        throw new Exception('Method deprecated in %s', __METHOD__);
        /*$account = Mage::getModel('MarketPlace/Accounts')->load($country->getmpac_account_id());
        
        Mage::register('mp_country', $country);
        Mage::register('mp_account', $account);*/
        
    }
    
    /**
     * Is marketplace allow revise products
     * 
     * @return boolean 
     */
    public function allowReviseProducts(){
        return true;
    }

}