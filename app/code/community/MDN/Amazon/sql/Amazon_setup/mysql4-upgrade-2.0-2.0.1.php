<?php

$installer = $this;

$installer->startSetup();

$installer->run("
        CREATE TABLE IF NOT EXISTS `{$this->getTable('amazon_variation_types')}` (
            avt_id INTEGER AUTO_INCREMENT,
            avt_variation_type VARCHAR(255),
            avt_attribute_code VARCHAR(50) UNIQUE,
            CONSTRAINT avt_primary_key PRIMARY KEY(avt_id),
            CONSTRAINT UC_amazon_variation_types UNIQUE(avt_variation_type, avt_attribute_code)
        );
");

$installer->endSetup();
