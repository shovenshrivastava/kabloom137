<?php

$installer = $this;

$installer->startSetup();

$installer->run("
        CREATE TABLE IF NOT EXISTS `{$this->getTable('amazon_optionnals_fields')}` (
            aof_id INTEGER AUTO_INCREMENT,
            aof_name VARCHAR(255),
            aof_attribute_code VARCHAR(50) UNIQUE,
            CONSTRAINT avt_primary_key PRIMARY KEY(aof_id),
            CONSTRAINT UC_amazon_optionnals_fields UNIQUE(aof_name, aof_attribute_code)
        );
");

$installer->endSetup();