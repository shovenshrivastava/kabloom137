<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2.0
 */
class MDN_Amazon_VariationTypesController extends Mage_Adminhtml_Controller_Action {
    
    /**
     * Save action 
     */
    public function saveAction(){
        
        try{
            
            $data = $this->getRequest()->getPost();
            
            foreach($data['data'] as $attributeCode => $variationType){           

                $variationType = (!$variationType) ? new Zend_Db_Expr('null') : $variationType;
                
                $item = Mage::getModel('Amazon/VariationTypes')->load($attributeCode, 'avt_attribute_code');

                if($item->getId()){

                    $item->setavt_variation_type($variationType)
                            ->save();

                }else{

                    $item->setavt_attribute_code($attributeCode)
                            ->setavt_variation_type($variationType)
                            ->save();

                }                
    
            }
            
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Variation types saved'));
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage().' : '.$e->getTraceAsString());
        }
        
        $this->_redirect('MarketPlace/Attributes/Index', array('mp' => 'amazon', 'tab' => 'amazon_variations'));
        
    }
    
}
