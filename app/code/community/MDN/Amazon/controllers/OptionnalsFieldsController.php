<?php

class MDN_Amazon_OptionnalsFieldsController extends Mage_Adminhtml_Controller_Action {
    
    /**
     * Save form
     */
    public function saveAction(){
        try{
            
            $post = $this->getRequest()->getPost();
            
            foreach($post['data'] as $id => $data){
                
                $obj = Mage::getModel('Amazon/OptionnalsFields')->load($id);
                
                if($obj){
                    
                    $obj->setaof_name($data['amazon_attribute'])
                            ->setaof_attribute_code($data['magento_attribute'])
                            ->save();
                    
                }else{
                    
                    // add a new entry
                    Mage::getModel('Amazon/OptionnalsFields')
                            ->setaof_name($data['amazon_attribute'])
                            ->setaof_attribute_code($data['magento_attribute'])
                            ->save();
                    
                }
                
            }
            
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Optionnals fields saved'));
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('MarketPlace/Attributes/Index', array('mp' => 'amazon', 'tab' => 'amazon_optionnals'));
    }
    
    /**
     * Delete optionnal field
     */
    public function deleteAction(){
        try{
            
            $id = $this->getRequest()->getParam('id');
            
            $obj = Mage::getModel('Amazon/OptionnalsFields')->load($id);
            
            if($obj){
                
                $obj->delete();
                
            }
            
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Optionnal field deleted'));
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('MarketPlace/Attributes/Index', array('mp' => 'amazon', 'tab' => 'amazon_optionnals'));
    }
    
    public function displayNewOptionnalFieldRowAction(){
        try{
            
            $id = date('Hms');
            
            $html = '';
            $html .= '<td><input type="text" name="data[new_'.$id.'][amazon_attribute]" id="data_new_'.$id.'_amazon_attribute" value=""/></td>';
            $html .= '<td>';
            $html .= '<select name="data[new_'.$id.'][magento_attribute]" id="data_new_'.$id.'_amazon_attribute">';
            $html .= '<option></option>';
        
            $entityTypeId = Mage::getModel('eav/entity_type')->loadByCode('catalog_product')->getId();
            $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setEntityTypeFilter($entityTypeId);          

            foreach ($attributes as $attribute) {
                $html .= '<option value="'.$attribute->getAttributeCode().'">'.$attribute->getName().'</option>';           
            }

            $html .= '</select></td>';
            
            $this->getResponse()->setBody($html);
        }catch(Exception $e){
            $this->getResponse()->setBody();
        }
        
    }
    
} 
