<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_AttributesController extends Mage_Adminhtml_Controller_Action {
    
    /**
     * Create attribute action 
     */
    public function createAction(){
        
        try{
        
            $category = $this->getRequest()->getParam('category');
            $subcategory = $this->getRequest()->getParam('subcat');
            $set = $this->getRequest()->getParam('set');
            $tab = $this->getRequest()->getParam('tab');
            
            Mage::Helper('Amazon/XSD_Attributes')->create($category, $subcategory, $set);
            
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Attributes successfully created'));
            
            
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage().' : '.$e->getTraceAsString());
            
        }
        
        $this->_redirect('Amazon/Requiredfields/Edit', array('category' => $category, 'subcat' => $subcategory, 'tab' => $tab));
        
    }
    
}
