<?php



class MDN_Amazon_GridController extends Mage_Adminhtml_Controller_Action {

    public function exportCsvAction()
    {
        $grid = $this->getRequest()->getParam('grid');

        $country = Mage::getModel('MarketPlace/Countries')->load($this->getRequest()->getParam('country_id'));
        Mage::register('mp_country', $country);

        $fileName = 'amazon_grid_'.$grid.'.csv';
        $content = $this->getLayout()->createBlock('Amazon/Grids_'.$grid)
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

}