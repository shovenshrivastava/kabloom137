<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Debug_AmazonReportController extends Mage_Adminhtml_Controller_Action {

    /**
     * Count report
     */
    public function getReportCountAction() {

        try {
            
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            
            $response = mage::helper('Amazon/Report')->getReportCount();
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        } catch (Exception $e) {
            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Get report
     */
    public function getReportAction() {

        try {
            
            $countryId = $this->getRequest()->getParam('countryId');
            $reportId = $this->getRequest()->getParam('reportId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            
            $response = mage::helper('Amazon/Report')->getReport($reportId);
            $this->_prepareDownloadResponse('orders.txt', $response, 'text/plain');
        } catch (Exception $e) {
            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Get report list action
     */
    public function getReportListAction() {

        try {
            
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            
            $response = mage::helper('Amazon/Report')->getReportList();
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        } catch (Exception $e) {
            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Get report request list action
     */
    public function getReportRequestListAction() {

        try {
            
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            
            $data = $this->getRequest()->getPost();
            $helper = Mage::Helper('Amazon/Report');
            $type = '_GET_FLAT_FILE_ORDERS_DATA_';           
            
            if($data['token'] == ""){
                $response = $helper->getReportRequestList($type);                
            }else{
                $response = $helper->getReportListByNextToken($data['token'], $type);                
            }
            
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        } catch (Exception $e) {
            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Get report list by next token action
     */
    public function getReportListByNextTokenAction() {
        try {
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $response = Mage::helper('Amazon/Report')->getReportListByNextToken($this->getRequest()->getParam('token'), $this->getRequest()->getParam('type'));
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        } catch (Exception $e) {
            $message = Mage::Helper('MarketPlace/Errors')->formatErrorMessage($e);
            Mage::getSingleton('adminhtml/session')->addError($message);
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }
    
    /**
     * Get report by id 
     */
    public function getReportByIdAction(){
        try{
            
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            
            $reportRequestId = $this->getRequest()->getParam('reportRequestId');
            $helper = Mage::Helper('Amazon/Report');
            
            $result = $helper->getReportById($reportRequestId, '_GET_FLAT_FILE_ORDERS_DATA_');
            
            if($result == 'none')
                throw new Exception('Unable to find report request id : '.$reportRequestId);
            
            else
                $this->_prepareDownloadResponse('reponse.xml', $result, 'text/xml');
            
        }catch(Exception $e){
            
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
            
        }
                        
    }

}

