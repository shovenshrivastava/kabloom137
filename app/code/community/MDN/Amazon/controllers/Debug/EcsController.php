<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 * @deprecated since version 2
 */
class MDN_Amazon_Debug_EcsController extends Mage_Adminhtml_Controller_Action {

    /**
     * ECS search 
     */
    public function requestAction() {

        throw new Exception('Method deprecated in '.__METHOD__);
        
        /*try{
            $item_id = $this->getRequest()->getParam('ecs_search');
            $id_type = $this->getRequest()->getParam('ecs_type');
            $retour = mage::helper('Amazon/ECS_Main')->request($item_id, $id_type);
            $this->_prepareDownloadResponse('ecsResponse.xml', $retour->getBody(), 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug'));
        }*/

    }

}
