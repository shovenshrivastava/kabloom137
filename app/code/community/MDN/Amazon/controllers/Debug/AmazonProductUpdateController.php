<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Debug_AmazonProductUpdateController extends Mage_Adminhtml_Controller_Action{

    /**
     * Cron update stocks and prices action
     */
    public function cronExportStockAndPriceAction(){
        try{
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $nbr = Mage::helper('MarketPlace/Main')->updateStocksAndPrices(Mage::helper('Amazon')->getMarketPlaceName());
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('%s product(s) updated', $nbr));
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Construction du fichier inventoryFeed.xml (mise à jour du stock)
     * Affichage du fichier
     */
    public function getInventoryFeedAction() {

        try{

            $start = microtime(true);

            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $helper = mage::helper('Amazon/ProductUpdate');
            $products = $helper->getProducts();

            $filename = $helper->getInventoryFeedName();
            $helper->setInventoryFeed($products, $filename);

            $end = microtime(true);

            $executionTime = $end - $start;

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::Helper('Amazon')->getMarketPlaceName(),
                    MDN_MarketPlace_Model_Logs::kNoError,
                    Mage::Helper('MarketPlace')->__('Generate inventory feed'),
                    MDN_MarketPlace_Model_Logs::kScopeUpdate,
                    array('fileName' => NULL),
                    $executionTime
            );

            $filePath = Mage::app()->getConfig()->getTempVarDir();
            $filePath .= '/export/marketplace/amazon';

            $content = file_get_contents($filePath . '/inventoryFeed.xml');
            
            $this->_prepareDownloadResponse('testInventoryFeed.xml', $content, 'text/xml');
            
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Construction du fichier priceFeed.xml (mise à jour des prix)
     * Affichage du fichier
     */
    public function getPriceFeedAction() {

        try{

            $start = microtime(true);

            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $helper = mage::helper('Amazon/ProductUpdate');

            $products = $helper->getProducts();
            $filename = $helper->getPriceFeedName();
            $helper->setPriceFeed($products, $filename);

            $end = microtime(true);

            $executionTime = $end - $start;

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::Helper('Amazon')->getMarketPlaceName(),
                    MDN_MarketPlace_Model_Logs::kNoError,
                    Mage::Helper('MarketPlace')->__('Generate price feed'),
                    MDN_MarketPlace_Model_Logs::kScopeUpdate,
                    array('fileName' => NULL),
                    $executionTime
            );

            $filePath = Mage::app()->getConfig()->getTempVarDir();
            $filePath .= '/export/marketplace/amazon';

            $content = file_get_contents($filePath . '/priceFeed.xml');

            $this->_prepareDownloadResponse('testPriceFeed.xml', $content, 'text/xml');
            
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Send inventory feed action
     */
    public function sendInventoryFeedAction() {
        try{

            $start = microtime(true);
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $helper = mage::helper('Amazon/ProductUpdate');
            $filename = $helper->getInventoryFeedName();
            $response = $helper->updateMarketPlaceStock($filename);

            $end = microtime(true);

            $executionTime = $end - $start;

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::Helper('Amazon')->getMarketPlaceName(),
                    MDN_MarketPlace_Model_Logs::kNoError,
                    Mage::Helper('MarketPlace')->__('Send sock feed'),
                    MDN_MarketPlace_Model_Logs::kScopeUpdate,
                    array('fileName' => NULL),
                    $executionTime
            );

            $this->_prepareDownloadResponse('response.xml', $response->getBody(), 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Send price feed action
     */
    public function sendPriceFeedAction() {
        try{

            $start = microtime(true);
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $helper = mage::helper('Amazon/ProductUpdate');
            $filename = $helper->getPriceFeedName();
            $response = $helper->updateMarketPlacePrices($filename);

            $end = microtime(true);

            $executionTime = $end - $start;

            mage::getModel('MarketPlace/Logs')->addLog(
                    Mage::Helper('Amazon')->getMarketPlaceName(),
                    MDN_MarketPlace_Model_Logs::kNoError,
                    Mage::Helper('MarketPlace')->__('Send price feed'),
                    MDN_MarketPlace_Model_Logs::kScopeUpdate,
                    array('fileName' => NULL),
                    $executionTime
            );

            $this->_prepareDownloadResponse('response.xml', $response->getBody(), 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Get image feed action
     */
    public function getImageFeedAction(){
        try{
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $helper = mage::helper('Amazon/ProductUpdate');

            $products = $helper->getProducts();
            $filename = $helper->getImageFeedName();
            $helper->setImageFeed($products, $filename);

            $filePath = Mage::app()->getConfig()->getTempVarDir();
            $filePath .= '/export/marketplace/amazon';

            $content = file_get_contents($filePath . '/imageFeed.xml');

            $this->_prepareDownloadResponse('imageFeed.xml', $content, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Send image feed action
     */
    public function sendImageFeedAction(){
        try{
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $helper = mage::helper('Amazon/ProductUpdate');
            $filename = $helper->getImageFeedName();
            $response = $helper->updateImage($filename);
            $this->_prepareDownloadResponse('response.xml', $response->getBody(), 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

}
