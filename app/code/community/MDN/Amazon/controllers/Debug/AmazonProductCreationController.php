<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Debug_AmazonProductCreationController extends Mage_Adminhtml_Controller_Action{

    /**
     * Cron check product creation action
     */
    public function cronCheckProductCreationAction() {

        try{
            
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId); 
            
            Mage::register('mp_country', $country);
            mage::helper('MarketPlace/Main')->checkProductCreation(Mage::helper('Amazon')->getMarketPlaceName());
            
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Product creation checked'));
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
            
        }catch(Exception $e){
            
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
            
        }
    }

    /**
     * Request listing products action
     */
    public function requestListingProductsAction() {
        try{
            
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId); 
            
            Mage::register('mp_country', $country);
            $retour = mage::helper('Amazon/ProductCreation')->requestListingProducts();
            $this->_prepareDownloadResponse('response.xml', $retour, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Show listing products
     */
    public function showListingProductsAction(){
        try{

            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId); 
            
            Mage::register('mp_country', $country);
            $content = Mage::Helper('Amazon/ProductCreation')->getLastListingProducts();

            $this->_prepareDownloadResponse('listingProducts.txt', $content, 'text/plain');

        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

}
