<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2013 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */

set_time_limit(3600);

class MDN_Amazon_Debug_CompareController extends Mage_Adminhtml_Controller_Action {
    
    /**
     * Process comparaison
     */
    public function processAction(){
        try{
            $report = '';
            $type = 'text/csv';
            $name = 'amazonDiff.csv';
            $amazonTab = array();
            $magentoTab = array();
            $line = '';
            
            $tErrorStock = array();
            $tErrorPrice = array();
            $tMissingInMagento = array();
            $tMissingInAmazon = array();
            $tErrorAsin = array();
            
            $nbrInMagento = 0;
            $nbrInAmazon = 0;
            
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            
            // magento data
            $dataCollection = Mage::getModel('MarketPlace/Data')->getCollection()
                                ->addFieldToFilter('mp_marketplace_id', Mage::registry('mp_country')->getId())
                                ->addFieldToFilter('mp_marketplace_status', 'created');
            
            foreach($dataCollection as $item){
                
                if($item->getmp_reference()){
                    
                    $sku = Mage::getModel('catalog/product')->load($item->getmp_product_id())->getsku();
                    
                    $magentoTab[$sku] = array(
                        'stock' => $item->getmp_last_stock_sent(),
                        'price' => $item->getmp_last_price_sent(),
                        'delay' => $item->getmp_last_delay_sent(),
                        'date' => $item->getmp_last_update(),
                        'asin' => $item->getmp_reference()
                    );
                }
                
            }
            
            // upload file
            $uploader = new Varien_File_Uploader('report');
            $uploader->setAllowedExtensions(array('txt', 'csv'));
            $path = Mage::app()->getConfig()->getTempVarDir() . '/import/marketplace/amazon/';

            // create directories if necessary
            if (!file_exists($path))
                mkdir($path, 0777, true);

            $uploader->save($path);

            //if there is a file uploaded
            if ($uploadFile = $uploader->getUploadedFileName()) {
                $file = Mage::helper('MarketPlace')->renameUploadedFile($uploadFile, $path, mage::helper('Amazon')->getMarketPlaceName());
                $filename = $path.$file;
                $lines = file($filename); 
                
                for($i = 1; $i < count($lines); $i++){
                    
                    $tmp = explode("\t", $lines[$i]);
                    
                    if(count($tmp) >= 4){
                        $amazonTab[trim($tmp[0])] = array(
                            'stock' => trim($tmp[3]),
                            'price' => trim($tmp[2]),
                            'asin' => trim($tmp[1])
                        );
                    }
                    
                }
                
            }
            
            $nbrInMagento = count($magentoTab);
            $nbrInAmazon = count($amazonTab);
            
            // compare
            foreach($amazonTab as $sku => $values){                                
                
                // check if asin exists
                if(!array_key_exists($sku, $magentoTab)){
                    $line = '"'.$sku.'","'.$values['asin'].'","'.$values['stock'].'","'.$values['price'].'"'."\n";
                    // product missing in magento
                    $tMissingInMagento[] = $line;
                }else{                                        
                    
                    $line = '"'.$sku.'","'.$values['asin'].'","'.$values['stock'].'","'.$values['price'].'"';
                    $line .=',"'.$magentoTab[$sku]['asin'].'","'.$magentoTab[$sku]['stock'].'","'.$magentoTab[$sku]['price'].'"'."\n";
                    
                    // check asin
                    if($values['asin'] != $magentoTab[$sku]['asin'])
                        $tErrorAsin[] = $line;
                    
                    // check stock
                    if($values['stock'] != $magentoTab[$sku]['stock'])
                        $tErrorStock[] = $line;
                    
                    // check price
                    if($values['price'] != $magentoTab[$sku]['price'])
                        $tErrorPrice[] = $line;
                 
                    // remove entry in magento tab in order to find missing on amazon
                    unset($magentoTab[$sku]);
                    
                }
                
            }
            
            // build report
            $report .= $this->__('Products in Magento : %s', $nbrInMagento)."\n";
            $report .= $this->__('Products in Amazon : %s', $nbrInAmazon)."\n";
            $report .= $this->__('Missing in Magento : %s', count($tMissingInMagento))."\n";
            $report .= $this->__('Missing in Amazon : %s', count($magentoTab))."\n";
            $report .= $this->__('Stock error : %s', count($tErrorStock))."\n";
            $report .= $this->__('Price error : %s', count($tErrorPrice))."\n";
            $report .= $this->__('Asin error : %s', count($tErrorAsin))."\n";
            $report .= "\n";
            
            $errorHeader = '"Sku","ASIN (Amazon)","Stock (Amazon)","Price (Amazon)","ASIN (Magento)","Stock (Magento)","Price (Magento)"'."\n";
            $missingHeader = '"Sku","ASIN","Stock","Price"'."\n";
            
            // stock errors
            $report .= $this->__('Stock errors')."\n";
            $report .= $errorHeader;
            foreach($tErrorStock as $line){
                $report .= $line;
            }
            $report .= "\n";
            // price error
            $report .= $this->__('Price errors')."\n";
            $report .= $errorHeader;
            foreach($tErrorPrice as $line){
                $report .= $line;
            }
            $report .= "\n";
            // asin error
            $report .= $this->__('ASIN errors')."\n";
            $report .= $errorHeader;
            foreach($tErrorAsin as $line){
                $report .= $line;
            }
            $report .= "\n";
            // missing in Magento
            $report .= $this->__('Missing in Magento')."\n";
            $report .= $missingHeader;
            foreach($tMissingInMagento as $line){
                $report .= $line;
            }
            $report .= "\n";
            // missing in Amazon
            $report .= $this->__('Missing in Amazon')."\n";
            $report .= $missingHeader;
            foreach($magentoTab as $sku => $values){
                $report .= '"'.$sku.'","'.$values['asin'].'","'.$values['stock'].'","'.$values['price'].'"'."\n";
            }
            
            // download report
            if($report != ''){
                $this->_prepareDownloadResponse($name, $report, $type);
            }else{
                Mage::getSingleton('adminhtml/session')->addError($this->__('Empty report'));
                $this->_redirectReferer();
            }
            
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getmessage().' : '.$e->getTraceAsString());
            $this->_redirectReferer();
        }
    }
    
}
