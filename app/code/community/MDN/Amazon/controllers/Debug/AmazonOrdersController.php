<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Debug_AmazonOrdersController extends Mage_Adminhtml_Controller_Action{

    /**
     * Cron import orders action
     */
    public function cronImportOrdersAction(){
        try{
            $countryId = $this->getRequest()->getParam('countryId');
            Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($countryId));
            $debug = mage::helper('MarketPlace/Main')->importOrders(Mage::helper('Amazon')->getMarketPlaceName());
            mage::getSingleton('adminhtml/session')->addSuccess($debug);
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }
    
    /**
     * Récupération du fichier de commandes Amazon (fichier plat)
     * Affichage du contenu
     */
    public function getMarketPlaceOrdersAction() {

        try{

            $countryId = $this->getRequest()->getParam('countryId');
            Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($countryId));
            
            $orders = mage::helper('Amazon/Orders')->getMarketPlaceOrders();
            
            $newOrders = array();
            

            foreach($orders as $ordersTab){
                foreach($ordersTab as $order){
                    if(!mage::helper('MarketPlace')->orderAlreadyImported($order['mpOrderId'])){
                        $newOrders[] = $order;
                    }
                }
            }
            
            ob_start();
            var_dump($newOrders);
            $debug = ob_get_contents();
            ob_end_clean();
            $debug = '<pre>'.$debug.'</pre>';

            mage::getSingleton('adminhtml/session')->addSuccess('Orders tab : '.$debug);
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
            
        }catch(Exception $e){

            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
            
        }
    }

    /*public function getOrderAcknowledgmentFeedAction() {

        try {
            $xml = mage::helper('MarketPlace/Amazon')->getOrderAcknowledgmentFeed();
            $this->_prepareDownloadResponse('orderAckonwledgmentFeed.xml', $xml, 'text/xml');
        } catch (Exception $e) {
            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirectReferer();
        }
    }*/

}
