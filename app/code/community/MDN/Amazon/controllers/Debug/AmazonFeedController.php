<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Debug_AmazonFeedController extends Mage_Adminhtml_Controller_Action{

    /**
     * Get feed submission list action
     */
    public function getFeedSubmissionListAction() {

        try{
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $response = mage::helper('Amazon/Feed')->getFeedSubmissionList();
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
        
    }

    /**
     * Get feed submission result action
     */
    public function getFeedSubmissionResultAction() {

        try{
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $id = $this->getRequest()->getPost('feedSubmissionId');
            if ($id != "") {
                $response = mage::helper('Amazon/Feed')->getFeedSubmissionResult($id);
                $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
            }else{
                mage::getSingleton('adminhtml/session')->addError('Empty request id');
                $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
            }
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }

    }

    /**
     * Get feed submission count action
     */
    public function getFeedSubmissionCountAction() {

        try{
            $countryId = $this->getRequest()->getParam('countryId');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);
            $response = mage::helper('Amazon/Feed')->getFeedSubmissionCount();
            $this->_prepareDownloadResponse('response.xml', $response, 'text/xml');
        }catch(Exception $e){
            mage::getSingleton('adminhtml/session')->addError('An error occured : '.$e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
        
    }

}
