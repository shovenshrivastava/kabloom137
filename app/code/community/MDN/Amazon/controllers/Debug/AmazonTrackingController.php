<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_Debug_AmazonTrackingController extends Mage_Adminhtml_Controller_Action{

    /**
     * Cron send tracking action
     */
    public function cronSendTrackingAction() {

        try {
            $countryId = $this->getRequest()->getParam('countryId');
            Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($countryId));
            Mage::helper('MarketPlace/Main')->sendTracking(Mage::helper('Amazon')->getMarketplaceName());
            mage::getSingleton('adminhtml/session')->addSuccess('Tracking successfully send.');
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        } catch (Exception $e) {

            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Get shipping confirmation feed action
     */
    public function getShippingConfirmationFeedAction() {

        try {

            $countryId = $this->getRequest()->getParam('countryId');
            Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($countryId));
            $boolean = mage::helper('Amazon/Tracking')->buildTrackingFile();

            if($boolean === true){

                $message = 'Shipping confirmation feed has been saved. Ready for send to Amazon.';

            }
            else{

                $message = 'No Actionable orders data requested yet !';

            }

            mage::getSingleton('adminhtml/session')->addSuccess($message);
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));

        } catch (Exception $e) {

            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * Send tracking file action
     */
    public function sendTrackingAction() {

        try {

            $countryId = $this->getRequest()->getParam('countryId');
            Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($countryId));
            $result = mage::helper('Amazon/Tracking')->sendTracking();
            if($result === true){
                mage::getSingleton('adminhtml/session')->addSuccess('Tracking successfully send.');   
            }
            else{
                mage::getSingleton('adminhtml/session')->addSuccess('File to export doesn\'t exists yet !');
            }
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        } catch (Exception $e) {

            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

    /**
     * request actionnable orders data action
     */
    public function requestActionableOrderDataAction() {

        try {

            $countryId = $this->getRequest()->getParam('countryId');
            Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($countryId));
            $requestId = mage::helper('Amazon/Tracking')->getOrdersToUpdate();
            mage::getSingleton('adminhtml/session')->addSuccess('Request sent to Amazon (request ID : '.$requestId.')');
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        } catch (Exception $e) {

            mage::getSingleton('adminhtml/session')->addError('An error occured : ' . $e->getMessage());
            $this->_redirect('Amazon/Main/index', array('tab' => 'debug', 'country_id' => $countryId));
        }
    }

}
