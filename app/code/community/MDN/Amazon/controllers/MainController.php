<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_MainController extends Mage_Adminhtml_Controller_Action {

    /**
     * Main screen
     */
    public function indexAction() {                        
        
        try{
            $currentCountry = Mage::getModel('MarketPlace/Countries')->getCurrentCountry($this->getRequest()->getParam('country_id'), 'amazon');

            if(!$currentCountry instanceof MDN_MarketPlace_Model_Countries || !$currentCountry->getId()){
                Mage::getSingleton('adminhtml/session')->addError($this->__('No active account. Before using previous screen, you must activate at least one account.'));
                $this->_redirect('MarketPlace/Configuration');
            }else{
                Mage::register('mp_country', $currentCountry);

                $this->loadLayout();

                $this->_setActiveMenu('sales/marketplace/amazon');

                $this->renderLayout();
            }
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage().' : '.$e->getTraceAsString());
            $this->_redirectReferer();
        }
        
    }

    /**
     * Save products datas
     */
    public function saveAction() {

        try {

            Mage::helper('MarketPlace')->save($this->getRequest(), $this->getRequest()->getParam('country_id'));

            //confirm & redirect
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Data saved'));
        } catch (Exception $e) {
            
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            
        }

        $this->_redirect('Amazon/Main/index', array('country_id' => $this->getRequest()->getParam('country_id')));
    }

    /**
     * selectAsinAction
     *
     */
    public function selectAsinAction(){

        try{

            $countryId = $this->getRequest()->getParam('country_id');
            $country = Mage::getModel('MarketPlace/Countries')->load($countryId);
            Mage::register('mp_country', $country);

            $selectedAsin = $this->getRequest()->getParam('selected_asin');

            if(is_array($selectedAsin) && count($selectedAsin) > 0){
                Mage::helper('Amazon/MultipleAsins')->select($selectedAsin);
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Asins selected'));
            }else{
                Mage::getSingleton('adminhtml/session')->addError($this->__('No asin selected'));
            }


        }catch(Exception $e){

            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());

        }

        $this->_redirect('Amazon/Main/index', array('country_id' => $this->getRequest()->getParam('country_id')));

    }

    /**
     * Format string
     *
     * @param string $value
     * @return string $value
     */
    public function formatUcFirst($value) {

        $value = strtolower($value);
        $value = ucfirst($value);

        return $value;
    }

    /**
     * Check connection action 
     * 
     * @deprecated 
     */
    public function checkConnexionAction() {
        
        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*try {

            $connexion = mage::helper('Amazon/CheckConnexion')->connexion();
            if ($connexion === true) {

                Mage::getSingleton('adminhtml/session')->addSuccess('Connexion OK.');
            } else {

                Mage::getSingleton('adminhtml/session')->addError('Connexion FAILED : <br/>' . $connexion);
            }

            $this->_redirectReferer();
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirectReferer();
        }*/
    }

    /**
     * Reset last update action 
     * 
     * @deprecated 
     */
    public function resetLastUpdateAction() {

        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*try {

            Mage::Helper('Amazon/ResetLastUpdate')->run();

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Amazon last updated successfully reset'));
            $this->_redirectReferer();
        } catch (Exception $e) {

            Mage::getSingleton('adminhtml/session')->addError($e->getMessage . ' : ' . $e->getTraceAsString());
            $this->_redirectReferer();
        }*/
    }
    
    /**
     * Test advertising API connection 
     * 
     * @deprecated 
     */
    public function AdvertisingApiConnectionTestAction(){
        
        throw new Exception(Mage::Helper('MarketPlace')->__('Deprecated method in %s',__METHOD__));
        
        /*try{
            
            // send a request for some ASIN code
            $response = Mage::Helper('Amazon/ECS_Main')->request('B007R4GQXM', 'ASIN');            
            Mage::getSingleton('adminhtml/session')->addSuccess('Connection OK.');
            
        }catch(Exception $e){            
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());            
        }
        
        $this->_redirectReferer();*/
    }
    
    /**
     * products created grid ajax 
     */
    public function productsCreatedGridAjaxAction(){
        
        $country = $this->getRequest()->getParam('country_id');

        Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($country));

        $block = $this->getLayout()->createBlock('Amazon/Grids_ProductsCreated');
        $this->getResponse()->setBody(
            $block->toHtml()
        );
        
    }
    
    /**
     * products created waiting for update grid ajax 
     */
    public function productsCreatedWaitingForUpdateGridAjaxAction(){
        
        $country = $this->getRequest()->getParam('country_id');

        Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($country));

        $block = $this->getLayout()->createBlock('Amazon/Grids_ProductsCreatedWaitingForUpdate');
        $this->getResponse()->setBody(
            $block->toHtml()
        );
        
    }
    
    /**
     * products created up to date grid ajax 
     */
    public function productsCreatedUpToDateGridAjaxAction(){
        
        $country = $this->getRequest()->getParam('country_id');

        Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($country));

        $block = $this->getLayout()->createBlock('Amazon/Grids_ProductsCreatedUpToDate');
        $this->getResponse()->setBody(
            $block->toHtml()
        );
        
    }
    
    /**
     * products t oadd grid ajax 
     */
    public function productsToAddGridAjaxAction(){
        
        $country = $this->getRequest()->getParam('country_id');

        Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($country));

        $block = $this->getLayout()->createBlock('Amazon/Grids_ProductsToAdd');
        $this->getResponse()->setBody(
            $block->toHtml()
        );
        
    }
    
    /**
     * products pending grid ajax
     */
    public function productsPendingGridAjaxAction(){
        
        $country = $this->getRequest()->getParam('country_id');

        Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($country));

        $block = $this->getLayout()->createBlock('Amazon/Grids_ProductsPending');
        $this->getResponse()->setBody(
            $block->toHtml()
        );
        
    }
    
    /**
     * products in error grid ajax 
     */
    public function productsInErrorGridAjaxAction(){
        
        $country = $this->getRequest()->getParam('country_id');

        Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($country));

        $block = $this->getLayout()->createBlock('Amazon/Grids_ProductsInError');
        $this->getResponse()->setBody(
            $block->toHtml()
        );
        
    }

    /**
     * multiple asins grid ajax
     */
    public function multipleAsinsGridAjaxAction(){

        $country = $this->getRequest()->getParam('country_id');

        Mage::register('mp_country', Mage::getModel('MarketPlace/Countries')->load($country));

        $block = $this->getLayout()->createBlock('Amazon/Grids_MultipleAsins');
        $this->getResponse()->setBody(
            $block->toHtml()
        );

    }

}