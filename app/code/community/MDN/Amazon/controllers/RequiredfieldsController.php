<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2012 Boost My Shop (http://www.boostmyshop.com)
 * @author : Nicolas MUGNIER
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @package MDN_Amazon
 * @version 2
 */
class MDN_Amazon_RequiredfieldsController extends Mage_Adminhtml_Controller_Action {

    /**
     * Edit action 
     */
    public function EditAction(){
        
        $this->loadLayout();
        $this->_setActiveMenu('sales/marketplace/configuration/attributes');
        $this->renderLayout();
        
    }

    /**
     * Save action 
     */
    public function saveAction(){

        try{

            $marketplace = strtolower(Mage::helper('Amazon')->getMarketPlaceName());

            $tmp = $this->getRequest()->getPost();           

            if(array_key_exists('form_key', $tmp))
                    unset($tmp['form_key']);
            
            foreach($tmp as $path => $value){

                $requiredField = Mage::getModel('MarketPlace/Requiredfields')
                                ->getCollection()
                                ->addFieldToFilter('mp_path',$path)
                                ->addFieldToFilter('mp_marketplace_id', $marketplace)
                                ->getFirstItem();

                if($requiredField->getmp_id()){
                    $requiredField->setmp_attribute_name($value['attribute'])
                            ->setmp_default($value['value'])
                            ->save();
                }else{
                    $requiredField = Mage::getModel('MarketPlace/Requiredfields')
                            ->setmp_path($path)
                            ->setmp_marketplace_id($marketplace)
                            ->setmp_attribute_name($value['attribute'])
                            ->setmp_default($value['value'])
                            ->save();
                }

            }

            Mage::getSingleton('adminhtml/session')->addSuccess('Data saved');
            $this->_redirect('*/*/Edit', array('category' => $this->getRequest()->getParam('category'), 'tab' => $this->getRequest()->getParam('tab')));

        }catch(Exception $e){

            Mage::getSingleton('adminhtml/session')->addError($e->getMessage().' : '.$e->getTraceAsString());
            $this->_redirectReferer();
            
        }

    }

}
