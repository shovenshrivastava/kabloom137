<?php
/*
 * Author Rudyuk Vitalij Anatolievich
 * Email rvansp@gmail.com
 * Blog www.cervic.info
 */
?>
<?php

class Infomodus_Upslabel_Block_Adminhtml_Lists_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('upslabelGrid');
        $this->setDefaultSort('created_time');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('upslabel/upslabel')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('upslabel_id', array(
            'header' => Mage::helper('upslabel')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'upslabel_id',
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('upslabel')->__('Title'),
            'align' => 'left',
            'width' => '250px',
            'index' => 'title',
        ));

        $this->addColumn('order_id', array(
            'header' => Mage::helper('upslabel')->__('Order ID'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'order_id',
            'frame_callback' => array($this, 'callback_order_link'),
        ));

        $this->addColumn('shipment_id', array(
            'header' => Mage::helper('upslabel')->__('Shipment or Credit memos ID'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'shipment_id',
            'frame_callback' => array($this, 'callback_ship_or_credit_link'),
        ));

        $this->addColumn('labelname', array(
            'header' => Mage::helper('upslabel')->__('Print'),
            'align' => 'left',
            'width' => '120px',
            'index' => 'labelname',
            'frame_callback' => array($this, 'callback_print'),
        ));

        $this->addColumn('type', array(
            'header' => Mage::helper('upslabel')->__('Type'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'type',
            'type' => 'options',
            'options' => Mage::getModel('upslabel/config_listsType')->getTypes(),
        ));

        $this->addColumn('statustext', array(
            'header' => Mage::helper('upslabel')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'statustext',
            'type' => 'options',
            'options' => Mage::getModel('upslabel/config_statuslabels')->getListsStatus(),
            'frame_callback' => array($this, 'callback_statustext'),
            'filter_condition_callback' => array($this, '_listsUpsStatusFilter'),
        ));

        $this->addColumn('created_time', array(
            'header' => Mage::helper('upslabel')->__('Created date'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'created_time',
        ));


        $this->addColumn('action',
            array(
                'header' => Mage::helper('upslabel')->__('Action'),
                'width' => '100',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('upslabel')->__('Delete'),
                        'url' => array('base' => '*/*/delete'),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));

        $this->addExportType('*/*/exportCsv', Mage::helper('upslabel')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('upslabel')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('upslabel_id');
        $this->getMassactionBlock()->setFormFieldName('upslabel');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('upslabel')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('upslabel')->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('upslabel_pdflabels', array(
            'label' => Mage::helper('sales')->__('Print Pdf Labels'),
            'url' => Mage::app()->getStore()->getUrl('upslabel/adminhtml_pdflabels', array('type' => 'lists')),
        ));
        return $this;
    }

    public function callback_print($value, $row, $column, $isExport)
    {
        if($row->getStatus() == 1){return;}
        $HVR = false;
        if (file_exists(Mage::getBaseDir('media'). DS . 'upslabel'. DS .'label' . DS . "HVR" . $row->getTrackingnumber() . ".html")) {
            $HVR = ' / <a href="' . Mage::getBaseUrl('media') . 'upslabel/label/HVR' . $row->getTrackingnumber() . '.html" target="_blank">HVR</a>';
        }
        $Pdf = '<a href="' . $this->getUrl('upslabel/adminhtml_pdflabels/onepdf/order_id/' . $row->getOrderId() . '/shipment_id/' . $row->getShipmentId() . '/type/' . $row->getType()) . '" target="_blank">PDF</a>';
        $Image = '<a href="' . $this->getUrl('upslabel/adminhtml_upslabel/print/imname/' . 'label' . $row->getTrackingnumber() . '.gif') . '" target="_blank">Image</a>';
        $Html = '<a href="' . Mage::getBaseUrl('media') . 'upslabel/label/' . $row->getTrackingnumber() . '.html" target="_blank">Html</a>';
        return $Pdf.' / '.$Html.' / '.$Image.$HVR;
    }

    public function callback_statustext($value, $row, $column, $isExport)
    {
        return $row->getStatustext();
    }

    public function callback_order_link($value, $row, $column, $isExport)
    {
        return '<a href="'.$this->getUrl('adminhtml/sales_order/view/order_id/' . $row->getOrderId()).'">&nbsp; '.$row->getOrderId().' &nbsp;</a>';
    }

    public function callback_ship_or_credit_link($value, $row, $column, $isExport)
    {
        $path = 'adminhtml/sales_order_shipment/view/shipment_id/';
        if($row->getType() == 'refund'){
            $path = 'adminhtml/sales_order_creditmemo/view/creditmemo_id/';
        }
        return '<a href="'.$this->getUrl($path . $row->getShipmentId()).'">&nbsp; '.$row->getShipmentId().' &nbsp;</a>';
    }

    public function _listsUpsStatusFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $status = 0;
        switch($value){
            case "success": $status = 0;  break;
            case "error": $status = 1; break;
        }
        $collection->addFieldToFilter('status', $status);
        return $this;
    }
}