<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml sales orders grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
/* DEPRECATED */
class Infomodus_Upslabel_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_order_grid');
        $this->setUseAjax(true);
        $this->setDefaultSort('delivery_date');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Retrieve collection class
     *
     * @return string
     */
    protected function _getCollectionClass()
    {
        return 'sales/order_grid_collection';
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        /*$collection->getSelect()->join(array("t123" => Mage::getConfig()->getTablePrefix() . 'upslabel'), 't123.order_id = main_table.entity_id AND t123.type="shipment"', array('t123.statustext'));*/
        $this->setCollection($collection);
        //echo $collection->getSelect(); exit;
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();
        $this->addColumn('statuslabel', array(
            'header' => Mage::helper('upslabel')->__('Shipping label status'),
            'index' => 'statuslabel',
            'type'  => 'options',
            'width' => '120px',
            'sortable' => false,
            'frame_callback' => array($this, 'callback_upsstatus'),
            'filter_condition_callback' => array($this, '_orderUpsStatusFilter'),
            'options' => Mage::getModel('upslabel/config_statuslabels')->getStatus(),
        ));
    }

    public function callback_upsstatus($value, $row, $column, $isExport)
    {
        $collections = Mage::getModel('upslabel/upslabel');
        $item = $collections->getCollection()->addFieldToFilter('order_id', $row->getId())->addFieldToFilter('type', 'shipment')->getFirstItem();
        return $item->getStatustext();
    }

    protected function _orderUpsStatusFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $status = 0;
        $is_need_filter = true;
        switch($value){
            case "success": $statustext = '="Successfully"'; $status = 0;  break;
            case "error": $statustext = '!="Successfully"'; $status = 1; break;
            case "notcreated": $is_need_filter = false; break;
        }
        if($is_need_filter == true){
            $collection->getSelect()->distinct(true)->join(array("t123" => Mage::getConfig()->getTablePrefix() . 'upslabel'), 't123.order_id = main_table.entity_id AND t123.type="shipment" AND t123.status="'.$status.'" AND t123.statustext'.$statustext, NULL);
            /*$query = $collection->getSelect();
            echo $query; exit;*/
        }
        else {
            $collection->getSelect()->distinct(true)->joinLeft(array("t123" => Mage::getConfig()->getTablePrefix() . 'upslabel'), 'main_table.entity_id = t123.order_id AND t123.type="shipment"', NULL)->where("t123.order_id IS NULL");
           /* $query = $collection->getSelect();
           echo $query; exit;*/
        }
        return $this;
    }
}
