<?php
/**
 * Created by PhpStorm.
 * User: Vitalij
 * Date: 29.05.14
 * Time: 11:37
 */
require_once 'UpslabelController.php';
require_once 'PdflabelsController.php';
require_once Mage::getBaseDir() . '/amazon/entities/Order.php';
require_once Mage::getBaseDir() . '/amazon/entities/Package.php';
require_once Mage::getBaseDir() . '/amazon/entities/Product.php';
require_once Mage::getBaseDir() . '/amazon/entities/Address.php';
//require_once Mage::getBaseDir() . '/amazon/edi/856.php';
require_once Mage::getBaseDir() . '/amazon/constants.php';
require_once Mage::getBaseDir() . '/amazon/essentials/write.php';


class Infomodus_Upslabel_Adminhtml_AutocreatelabelController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    { 		

       $order_id = Mage::registry('amazon_order');
       try {
			if(!empty($order_id)){
			$order_ids[0]= $order_id;
            $ptype = 'order';
            $type = 'shipment';
           $order_ids = $order_ids;	
	   }else{
			//$k = 0;
            $ptype = $this->getRequest()->getParam('type');
            $type = 'shipment';
            $order_ids = $this->getRequest()->getParam($ptype . '_ids');
			//echo $order_ids; exit;
		}			
		
            foreach ($order_ids AS $orderId) {
				//$k++;
                $order = Mage::getModel('sales/order')->load($orderId);	
				$orderIncrementId = $order->getIncrementId();				
                if ($order->canShip()) {	
                    $itemQty = $order->getItemsCollection()->count();
                    $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($itemQty);
                    $shipment = new Mage_Sales_Model_Order_Shipment_Api();
                    $shipmentId = $shipment->create($order->getIncrementId(), array(), '', true, true);
					$shipmentId = Mage::getModel('sales/order_shipment')->load($shipmentId, 'increment_id')->getId();
                } else {	
                    $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
					$shipment = $order->getShipmentsCollection()->getFirstItem();
					$shipmentId = $shipment->getId();
                }
				
                if ($shipmentId && $shipmentId > 0) {
                    $collections = Mage::getModel('upslabel/upslabel');
                    $colls = $collections->getCollection()->addFieldToFilter('order_id', $orderId)->addFieldToFilter('shipment_id', $shipmentId)->addFieldToFilter('type', $type)->addFieldToFilter('status', 0);
                    if (count($colls) == 0) {
                        $controller = new Infomodus_Upslabel_Adminhtml_UpslabelController();
                        $controller->intermediatehandy($orderId, $type, $shipmentId);

                        /** for setting values based on warehouse assigned starts here  - by anwar **/
                        $warehouse_id = $order->getAssignationWarehouse();
                        //$warehouse_id = isset($warehouse_id) ? $warehouse_id : 1;
                        if(count(explode(',',$warehouse_id))>1){
                          $warehouse_id = 1;  
                        }
                        $warehouseData = Mage::getModel('pointofsale/pointofsale')->load($warehouse_id);
                        
                        /* Partner Shipping Accounts Logic
                         * added by Nareshsseeta
                         * */
                         
                        $tradPrtnr = $order->getTradingPartner();
                          if(!empty($tradPrtnr)){
							 
							  $tradingpartner =$order->getTradingPartner();
							 }else{
								  $tradingpartner ="kabloom";
								 }
                        $collection = Mage::getResourceModel('partner/partner_collection');				
						$partners = Mage::getSingleton("partner/partner")->getCollection()->addFieldToFilter('partner_name', array('eq'=>$tradingpartner));
						foreach($partners as $partner){
						$partnerId =$partner->getPartnerId();} 
						if($partnerId&&$warehouse_id){
						
							$collection = Mage::getSingleton("partner/partnershipping")->getCollection()->addFieldToFilter('partner_id',$partnerId)->addFieldToFilter('partner_warehouse',$warehouse_id)->addFieldToFilter('partner_carrier',1);			
							$partnershipping = $collection->load();
							foreach($partnershipping as $partnershipping){
								$AccessLicenseNumber = $partnershipping->getPartnerLicense();
								$shipperNumber = $partnershipping->getPartnerAccount();
								$Password = $partnershipping->getPartnerAccountPassword();
								$UserId = $partnershipping->getPartnerUser();
							
							}
							//print_r($AccessLicenseNumber);
							//print_r($UserId);
							//print_r($Password);
							//die;
						}else{
							 /* Partner Shipping Accounts Logic */
						
                        $AccessLicenseNumber = Mage::getStoreConfig('upslabel/credentials/accesslicensenumber');
                        $UserId = Mage::getStoreConfig('upslabel/credentials/userid');
                        $Password = Mage::getStoreConfig('upslabel/credentials/password');
                        $shipperNumber = Mage::getStoreConfig('upslabel/credentials/shippernumber');}
						/** for setting values based on warehouse assigned ends here   - by anwar **/
						
						
						
						$warehouse = Mage::getModel('pointofsale/pointofsale')->getCollection()											
											->addFieldToFilter('place_id', $order->getAssignationWarehouse())
											->getFirstItem()
											->getData();   //add by sourav
											
					    if($order->getTradingPartner()=='amazon' && isset($warehouse['upsaccount'])){		//add by sourav
							 $warehouse['upsaccount'];
						}else{
                        	
						}

                        $lbl = Mage::getModel('upslabel/ups');
						
                        $lbl->setCredentials($AccessLicenseNumber, $UserId, $Password, $shipperNumber);
						
						/* added by nareshseeta 
						 * timeintransit and ups service codes mapping 
						 * assigning service code variable for sales->orders->grid 
						 * create ups shipping labels auto generation 
						 */						
						
						$coreResource = Mage::getSingleton('core/resource');
						$connection = $coreResource->getConnection('core_read');
						if($order->getTradingPartner()=='amazon')
						{
						$upsservice_code = $order->getPickupMethod();
						$select = $connection->select()->from('ups_code_map')->where('amazon_ups_code = ?', $upsservice_code)->orWhere('ups_transit_desc = ?', $upsservice_code);
						}else{
						$upsservice_code = substr($order->getPickupCode(),0,3 );
						$pickupMethod = $order->getPickupMethod();
						$select = $connection->select()->from('ups_code_map')->where('ups_transit_code = ?', $upsservice_code)->orWhere('ups_transit_desc = ?', $pickupMethod);
								}
								//echo 'se'.$select;die;
						$ups_codes = $connection->fetchAll($select);				
						
						foreach ($ups_codes as $ups_code)
						{						  
							$methord_id = $ups_code['ups_service_code'];
							$upsPickType = $ups_code['ups_pickup_type'];
							//$upsDeliveryType = 0;//$ups_code['ups_delivery_type'];
						 }
						
						//if($tradingpartner=='Marketplace'){
							
							//$upsDeliveryType = 0;
							
							//}else{
								
								//if($upsDeliveryType==0){
									
									//$upsDeliveryType = 0;
									
									//}
								//}
							
							
						$controller->defConfRarams['serviceCode'] = ($methord_id>0)?$methord_id:'03';					
						// ups service code mapping	
						
								
						if($tradingpartner=='Marketplace'){
							$controller->defConfRarams['atten_name'] = "Amazon.com";
							}else{
						$controller->defConfRarams['atten_name'] = $tradingpartner.".com";
					}
													
						$controller->defConfRarams['warehouse'] = array('address_line_1'=>$warehouse['address_line_1'],
																		'address_line_2'=>$warehouse['address_line_2'],
																		'city'=>$warehouse['city'],
																		'state'=>$warehouse['state'],
																		'postal_code'=>$warehouse['postal_code']
																	);		//add by sourav
						/* saturday logic for UPS API - added by naresh*/    
						$orderDeliveryDate = strtotime($order->getDeliveringDate());
						$orderDeliveryDay = date('l', $orderDeliveryDate);
						if($orderDeliveryDay =="Saturday"){
						$controller->defConfRarams['saturdaydelivery'] = intval(1);
							}
							/* Res/Com type  for UPS API - added by naresh*/  
						  
						//$orderPickupType = $order->getPickupType();
						
						if($upsPickType =="RES"){
							$controller->defConfRarams['residentialaddress'] = intval(1);
							}
							if($tradingpartner=="Jet"){
								$upsDeliveryType=0;
								
								}
                        if($upsDeliveryType ==1 || $tradingpartner!="Jet"){
							$controller->defConfRarams['upsDeliveryType'] = intval(2);
							}
                        
                        $lbl = $controller->setParams($lbl, $controller->defConfRarams, array($controller->defParams));
						
						$is_fedex = false;
						$pickup_method = $order->getPickupMethod(); // pickup method in the grid
						if(strripos($pickup_method, 'fedex')!== false){ // for fedex labels
							$lbl = Mage::getModel('upslabel/fedex');
							$is_fedex = true;
						}
						
                        if($is_fedex){ // for fedex labels only
							$upsl = $lbl->getShip($orderId);
						}else{	// for non fedex labels
							$upsl = $lbl->getShip();
						}
						//if($k == 2) {die;}
						//$shipmentId = $this->imOrder->getShipmentsCollection()->getFirstItem(); 			//add by sourav
						$shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);			//add by sourav
							
						
						
                        if ($controller->defConfRarams['default_return'] == 1) {
                            $lbl->serviceCode = array_key_exists('default_return_servicecode', $controller->defConfRarams) ? $controller->defConfRarams['default_return_servicecode'] : '';
                            $upsl2 = $lbl->getShipFrom();
                        }
                        $upslabel = Mage::getModel('upslabel/upslabel');
                        $colls2 = $upslabel->getCollection()->addFieldToFilter('order_id', $orderId)->addFieldToFilter('shipment_id', $shipmentId)->addFieldToFilter('type', $type)->addFieldToFilter('status', 1);
                        if (count($colls2) > 0) {
                            foreach ($colls2 AS $c) {
                                $c->delete();
                            }
                        }
                        if (!array_key_exists('error', $upsl) || !$upsl['error']) {							
							if($is_fedex){ // for fedex labels only
								//print_r($upsl); die;
								$trackingNumber = $upsl->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;
								//echo '<pre>track:'.$trackingNumber;print_r($upsl);die;
								$upslabel = Mage::getModel('upslabel/upslabel');
								$upslabel->setTitle('Order ' . $orderId);
								$upslabel->setOrderId($orderId);
								$upslabel->setShipmentId($shipmentId);
								$upslabel->setType($type);
								
								$upslabel->setTrackingnumber($trackingNumber);
								//$upslabel->setShipmentidentificationnumber($upsl['shipidnumber']);
								//$upslabel->setShipmentdigest($upsl['digest']);
								$upslabel->setLabelname('fedex_'.$orderIncrementId.'.png');
								$upslabel->setStatustext($upsl->HighestSeverity);
								$upslabel->setStatus(0);
								$upslabel->setCreatedTime(Date("Y-m-d H:i:s"));
								$upslabel->setUpdateTime(Date("Y-m-d H:i:s"));
								$upslabel->save();
							}else{ // for non fedex labels
									foreach ($upsl['arrResponsXML'] AS $upsl_one) {
										$upslabel = Mage::getModel('upslabel/upslabel');
										$upslabel->setTitle('Order ' . $orderId . ' TN' . $upsl_one['trackingnumber']);
										$upslabel->setOrderId($orderId);
										$upslabel->setShipmentId($shipmentId);
										$upslabel->setType($type);
										/*$upslabel->setBase64Image();*/
										$upslabel->setTrackingnumber($upsl_one['trackingnumber']);
										$upslabel->setShipmentidentificationnumber($upsl['shipidnumber']);
										$upslabel->setShipmentdigest($upsl['digest']);
										$upslabel->setLabelname('label' . $upsl_one['trackingnumber'] . '.gif');
										$upslabel->setStatustext(Mage::helper('adminhtml')->__('Successfully'));
										$upslabel->setStatus(0);
										$upslabel->setCreatedTime(Date("Y-m-d H:i:s"));
										$upslabel->setUpdateTime(Date("Y-m-d H:i:s"));
										$upslabel->save();

										$upslabel = Mage::getModel('upslabel/labelprice');
										$upslabel->setOrderId($orderId);
										$upslabel->setShipmentId($shipmentId);
										$upslabel->setPrice($upsl['price']['price'] . " " . $upsl['price']['currency']);
										$upslabel->save();
									}
							}
                            if ($controller->defConfRarams['default_return'] == 1) {
                                if (!array_key_exists('error', $upsl2) || !$upsl2['error']) {
                                    foreach ($upsl2['arrResponsXML'] AS $upsl_one) {
                                        $upslabel = Mage::getModel('upslabel/upslabel');
                                        $upslabel->setTitle('Order ' . $orderId . ' TN' . $upsl_one['trackingnumber']);
                                        $upslabel->setOrderId($orderId);
                                        $upslabel->setShipmentId($shipmentId);
                                        $upslabel->setType($type);
                                        /*$upslabel->setBase64Image();*/
                                        $upslabel->setTrackingnumber($upsl_one['trackingnumber']);
                                        $upslabel->setShipmentidentificationnumber($upsl['shipidnumber']);
                                        $upslabel->setShipmentdigest($upsl['digest']);
                                        $upslabel->setLabelname('label' . $upsl_one['trackingnumber'] . '.gif');
                                        $upslabel->setStatustext(Mage::helper('adminhtml')->__('Successfully'));
                                        $upslabel->setStatus(0);
                                        $upslabel->setCreatedTime(Date("Y-m-d H:i:s"));
                                        $upslabel->setUpdateTime(Date("Y-m-d H:i:s"));
                                        $upslabel->save();

                                        $upslabel = Mage::getModel('upslabel/labelprice');
                                        $upslabel->setOrderId($orderId);
                                        $upslabel->setShipmentId($shipmentId);
                                        $upslabel->setPrice($upsl2['price']['price'] . " " . $upsl2['price']['currency']);
                                        $upslabel->save();
                                    }
                                } else {
                                    $upslabel = Mage::getModel('upslabel/upslabel');
                                    $upslabel->setTitle('Order ' . $orderId);
                                    $upslabel->setOrderId($orderId);
                                    $upslabel->setShipmentId($shipmentId);
                                    $upslabel->setType($type);
                                    $upslabel->setStatustext($upsl2['errordesc']);
                                    $upslabel->setStatus(1);
                                    $upslabel->setCreatedTime(Date("Y-m-d H:i:s"));
                                    $upslabel->setUpdateTime(Date("Y-m-d H:i:s"));
                                    $upslabel->save();
                                }
                            }
                            if ($controller->defConfRarams['addtrack'] == 1 && $type == 'shipment') {
								if($is_fedex){ // for fedex labels only
									$trTitle = 'FedEx';
									$shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
									$track = Mage::getModel('sales/order_shipment_track')
											->setNumber($trackingNumber)
											->setCarrierCode('fedex')
											->setTitle($trTitle);
									$shipment->addTrack($track);
								}else{	// for non fedex labels
									$trTitle = 'United Parcel Service';
									$shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
									foreach ($upsl['arrResponsXML'] AS $upsl_one1) {
										$track = Mage::getModel('sales/order_shipment_track')
											->setNumber(trim($upsl_one1['trackingnumber']))
											->setCarrierCode('ups')
											->setTitle($trTitle);
										$shipment->addTrack($track);
									}
								}
								
                                $shipment->save();
                               if(!$order->getTradingPartner()=='amazon'){ 
							
                               $shipment->sendEmail(true)->setEmailSent(true)->save();
							}
                            }

                        } else {
                            $upslabel = Mage::getModel('upslabel/upslabel');
                            $upslabel->setTitle('Order ' . $orderId);
                            $upslabel->setOrderId($orderId);
                            $upslabel->setShipmentId($shipmentId);
                            $upslabel->setType($type);
                            $upslabel->setStatustext($upsl['errordesc']);
                            $upslabel->setStatus(1);
                            $upslabel->setCreatedTime(Date("Y-m-d H:i:s"));
                            $upslabel->setUpdateTime(Date("Y-m-d H:i:s"));
                            $upslabel->save();
                        }
                    }
                }
            }



            /* nirmesh start */
            // print_r($_POST);die;
            $i = 0;
            foreach ($_POST['order_ids'] as $id) {
                $i++;
                $order = Mage::getModel('sales/order')->load($id);
                $amazon_order[$i] = new Order();

                $amazon_order[$i]->orderId = $order->getId();
              $amazon_order[$i]->partnerOrderId = $order->getPartnerOrderId();
  
              $amazon_order[$i]->wareHouse = $order->getWarehouse();

                $amazon_order[$i]->orderDate = $order->getCreatedAt();
$amazon_order[$i]->shipmentMethod = $order->getPartnerShipmentMethod();

                $product = new Product();
                $ordered_items = $order->getAllItems();
                $products = array();
                $j = 0;
                foreach ($ordered_items as $item) {
                    $product = new Product();
                    $product->id = $item->getProductId();
                    $product->sku = $item->getSku();
                    $product->quantity = $item->getQtyOrdered();
                    $product->price = $item->getPrice();
                    $product->title = $item->getName();
                    $products[$j] = $product;
                    $j++;
                }
                $package = new Package();
                foreach ($order->getTracksCollection() as $_track) {
                    $package->trackingNumber = $_track->getNumber();
                    $package->shipmentCode = $_track->getCarrierCode();
                }
               // echo "------";
                //print_r($package);
//                $shipmentCollection = Mage::getResourceModel('sales/order_shipment_collection')
//                        ->setOrderFilter($order)
//                        ->load();
//                foreach ($shipmentCollection as $shipment) {
//                    // This will give me the shipment IncrementId, but not the actual tracking information.
//                    foreach ($shipment->getAllTracks() as $tracknum) {
//                        $tracknums[] = $tracknum->getNumber();
//                    }
//                }

               // $package->trackingNumber = $tracknums;

//                $ship_col = Mage::getResourceModel('sales/order_shipment_track_collection')->addAttributeToFilter('track_number', $tracknums)->load();
//                foreach ($ship_col as $sc) {
//                    $trackMethod = $sc->getTitle();
//                }

               // $package->shipmentCode = $trackMethod;
                $package->weight = '3';

                $amazon_order[$i]->package = $package;
                $amazon_order[$i]->products = $products;

                $shipping_address = new Address();
                $shipping_address->firstname = $order->getShippingAddress()->getFirstname();
                $shipping_address->lastname = $order->getShippingAddress()->getLastname();
                $shipping_address->street = $order->getShippingAddress()->getStreet();
                $shipping_address->city = $order->getShippingAddress()->getCity();
                $shipping_address->region = $order->getShippingAddress()->getRegion();
                $shipping_address->telephone = $order->getShippingAddress()->getTelephone();
                $shipping_address->postcode = $order->getShippingAddress()->getPostcode();
                $shipping_address->countryId = $order->getShippingAddress()->getCountryId();

                $billing_address = new Address();
                $billing_address->firstname = $order->getBillingAddress()->getFirstname();
                $billing_address->lastname = $order->getBillingAddress()->getLastname();
                $billing_address->street = $order->getBillingAddress()->getStreet();
                $billing_address->city = $order->getBillingAddress()->getCity();
                $billing_address->region = $order->getBillingAddress()->getRegion();
                $billing_address->telephone = $order->getBillingAddress()->getTelephone();
                $billing_address->postcode = $order->getBillingAddress()->getPostcode();
                $billing_address->countryId = $order->getBillingAddress()->getCountryId();



                $amazon_order[$i]->shipmentAddress = $shipping_address;
                $amazon_order[$i]->billingAddress = $billing_address;
				
				//$icn = rand(100000000, 999999999);
             //$edi = generateResponse($amazon_order[$i], $icn);
             //   write_edi($edi, $icn);
                //$amazon_order[$i]->package = $order->getId();
            }
          //  print_r($amazon_order);
	//die();
          

//           write_edi($edi, 'amaze.txt');


			//die('Autocreatelabel out');
            if(!empty($order_id)){
				 Mage::unregister('amazon_order');
				//no PDF for EDI order creation 
			}else{
			$pickship = $this->getRequest()->getParam('pickship');
			$pickpack = $this->getRequest()->getParam('pickpack');
			$giftmessage = $this->getRequest()->getParam('giftmessage');
            $resp = Infomodus_Upslabel_Adminhtml_PdflabelsController::create($order_ids, $type, $ptype, $pickship, $pickpack, $giftmessage);
            if (!$resp) {
                 $this->_redirectReferer();
                 return $order_id;
                 
            }
		}
        } catch (Exception $e) {
            echo $e->getMessage();
            return $order_id;
            Mage::unregister('amazon_order');
            
        }
        if(!empty($order_id)){
			//return $order_id;
			Mage::unregister('amazon_order');
		}else{
        return true;
	}
		}
} 
