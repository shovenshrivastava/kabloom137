<?php
class Infomodus_Upslabel_Adminhtml_RegeneratepickupController extends Mage_Core_Controller_Front_Action{
	public function indexAction($order_ids, $type, $ptype){
		$ptype = $this->getRequest()->getParam('type');
		$order_ids = $this->getRequest()->getParam($ptype . '_ids');
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		foreach ($order_ids AS $orderId) {
			$order=Mage::getModel('sales/order')->load($orderId);
			
			 if (($order->getState() == Mage_Sales_Model_Order::STATE_NEW) || ($order->getState() == Mage_Sales_Model_Order::STATE_PROCESSING)){
					$assignedWarehouse = $order->getAssignationStock();
					$warehouseData = Mage::helper('core')->jsonDecode($assignedWarehouse);					
					$ordered_items = $order->getAllItems();
					foreach ($ordered_items as $item) {
						if ($item->getProductType() == 'simple'){
							$warehouse_id = array_search(1,$warehouseData[$item->getProductId()]);// use this data for -2 days from delivery date
							$warehouse = Mage::getModel('pointofsale/pointofsale')->getCollection()
													->addFieldToSelect('postal_code')
													->addFieldToFilter('place_id', $warehouse_id);
												$warehouse_data = $warehouse->getData();
												$transit_final_zip = $warehouse_data[0]['postal_code'];// use this data for -2 days from delivery date
												
												$warehouse_id_2 = $warehouse_id; // use this data for -1 day from delivery date
												$transit_final_zip_2 = $transit_final_zip;// use this data for -1 day from delivery date
						}
					}
					
					$delivery_date = Mage::app()->getLayout()->getBlockSingleton('operations_adminhtml/operations_order_renderer_delivery')->custom_renderer($order);
					
					$tmp_array = explode("-", $delivery_date);
					$tmp_delivery_date = array();
					$tmp_delivery_date['year'] = $tmp_array[0];
					$tmp_delivery_date['month'] = $tmp_array[1];
					$tmp_delivery_date['day'] = $tmp_array[2];
					$delivery_date_day = $tmp_delivery_date['day'];
					
					// predict the tmp_pickup_date, if the delivery date is away from 2 days - u should subtract 2 days, if the delivery date is away from 1 day - u should subtract 1 day,if the delivery date is today  then you can make delivery date itself as tmp_pickup_date bcoz in bcoz the timeintransit api will consider pickup date as today itself automatically
					$currentdate = strtotime(date("Y-m-d"));
					$date = strtotime($delivery_date);
					$datediff = $date - $currentdate;
					$differance = floor($datediff / (60 * 60 * 24));
					$pickup_error = false;
					
					$final_warehouse_hours_data = Mage::getModel('pointofsale/pointofsale')->load($warehouse_id)->getData('hours');
					$warehouse_timings = json_decode($final_warehouse_hours_data,true);
					
					if ($differance == 0) {
						//echo 'today';            
						$tmp_pickup_date = str_replace("-", "", $delivery_date);
						$pickup_error = true;
						$error_msg = 'Regeneration cannot be done on delivery date';
						continue; // break the remaining flow
					} elseif ($differance == 1) {
						//echo 'tommorrow';
						
						$pickup_day = date("l");
						$warehouse_closing_time = explode(':',$warehouse_timings[$pickup_day]['to']);
						$pickup_time_in_minutes = (date("H") * 60) + date("i");
						$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
						
						if($pickup_time_in_minutes < $warehouse_closing_time_in_minutes){// when warehouse is opened
							$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
						}else{// when warehouse is closed
							$pickup_error = true;
							$error_msg = 'Assigned Warehouse is closed a day before the delivery date at the time of Regeneration';
						}
					} elseif ($differance > 1) {
						//echo 'Future Date';
						
						if($differance == 2){ // when delivery date is exactly after 2 days				
							// if condition executes when tmp pickupdate date is today
							
							// if pickupdate is today check if the warehouse is open now or not - starts here
							$pickup_day = date("l");
							$warehouse_closing_time = explode(':',$warehouse_timings[$pickup_day]['to']);
							$pickup_time_in_minutes = (date("H") * 60) + date("i");
							$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
							// if pickupdate is today check if the warehouse is open now or not - ends here
							
							if($pickup_time_in_minutes < $warehouse_closing_time_in_minutes){// when warehouse is opened
								$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 2 days')));
							}else{
								$tmp_pickup_day = date('l', strtotime($delivery_date . ' - 1 days'));
								$warehouse_closing_time = explode(':',$warehouse_timings[$tmp_pickup_day]['to']);
								if(empty($warehouse_closing_time[0])){// when warehouse is closed
									$pickup_error = true;
									$error_msg = 'A day before Delivery Date, assigned warehouse is completely closed';
								}else{
									$warehouse_tmrw_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
									if($warehouse_tmrw_closing_time_in_minutes == 0){// when warehouse is closed
										$pickup_error = true;
										$error_msg = 'A day before delivery date, assigned warehouse is completely closed for shipping';
									}else{ // when warehouse is opened
										$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
									}
								}
							}
						}else{	// when delivery date is >= 3 days
							// 
							
							$tmp_pickup_day = date('l', strtotime($delivery_date . ' - 2 days'));
							$warehouse_closing_time = explode(':',$warehouse_timings[$tmp_pickup_day]['to']);
							if(empty($warehouse_closing_time[0])){	//monday				// when warehouse is closed
								$tmp_pickup_day = date('l', strtotime($delivery_date . ' - 1 days'));
								$warehouse_closing_time = explode(':',$warehouse_timings[$tmp_pickup_day]['to']);
								if(empty($warehouse_closing_time[0])){ 	//tue			// when warehouse is closed
									$pickup_error = true;
									$error_msg = '1 day and 2 days before delivery date, assigned warehouse is completely closed for shipping';
								}else{
									$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
									if($warehouse_closing_time_in_minutes == 0){	// when warehouse is closed
										$pickup_error = true;
										$error_msg = '1 day and 2 days before delivery date, assigned warehouse is closed for shipping';
									}else{// when warehouse is opened
										$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
									}
								}
							}else{
								$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
								if($warehouse_closing_time_in_minutes == 0){
									$tmp_pickup_day = date('l', strtotime($delivery_date . ' - 1 days'));
									$warehouse_closing_time = explode(':',$warehouse_timings[$tmp_pickup_day]['to']);
									if(empty($warehouse_closing_time[0])){ 	//tue			// when warehouse is closed
										$pickup_error = true;
										$error_msg = '1 day and 2 days before delivery date, assigned warehouse is closed for shipping or completely closed';
									}else{
										$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
										if($warehouse_closing_time_in_minutes == 0){	// when warehouse is closed
											$pickup_error = true;
											$error_msg = '1 day and 2 days before delivery date, assigned warehouse is closed for shipping';
										}else{// when warehouse is opened
											$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
										}
									}
								}else{// when warehouse is opened
									$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 2 days')));
								}
							}						
						}
					} else {
						//echo 'past date'; 
						$tmp_pickup_date = str_replace("-", "", $delivery_date);
						$pickup_error = true;
						$error_msg = 'Delivery date is past date';
						continue; // break the remaining flow
					}
					
					// if error exists while considering pickupdate break the flow after updating error msg - starts here
					if($pickup_error){
						//$error_msg = "Pickup method is not available for tentative date";
						$query_3 = "UPDATE sales_flat_order SET pickup_method = '" . $error_msg . "' WHERE entity_id = " . $orderId;
						$writeConnection->query($query_3);
						continue; // break the remaining flow
					}
					// if error exists while considering pickupdate break the flow after updating error msg - ends here
					
					$tmp_pickup_datetime['year'] = substr($tmp_pickup_date, 0, 4);
					$tmp_pickup_datetime['month'] = substr($tmp_pickup_date, 4, 2);
					$tmp_pickup_datetime['day'] = substr($tmp_pickup_date, 6, 2);
					$tmp_pickup_datetime['hrs'] = '23'; // assumption as customer is not entering delivery time details
					$tmp_pickup_datetime['mins'] = '59'; // assumption as customer is not entering delivery time details
					$tmp_pickup_datetime['secs'] = '59'; // assumption as customer is not entering delivery time details

					$address = $order->getShippingAddress();
					
					// assigning postCode_from. if warehouse is available on - 2 days then use $transit_final_zip and if warehouse is available on -1 day then use $transit_final_zip_2
					$postCode_from = (empty($transit_final_zip)) ? $transit_final_zip_2 : $transit_final_zip; 
					//$postCode_from = $transit_final_zip; // Shipper location details
					if(empty($postCode_from)) 
					 $postCode_from = '02446'; // This is main warehouse 305 Harvard Street, Brookline, MA
					
					$warehouse_loc = Mage::getModel('pointofsale/pointofsale')->getCollection()
								->addFieldToSelect('country_code')
								->addFieldToSelect('city')
								->addFieldToFilter('postal_code', $postCode_from);
						$ware_loc_data = $warehouse_loc->getData();
						$warehouse_country_code = $ware_loc_data[0]['country_code'];
						$warehouse_city = $ware_loc_data[0]['city'];
				   
					 $city_from = $warehouse_city; //  warehouse city
					 if(empty($city_from)) 
					 $city_from = 'Brookline'; // This is main warehouse 305 Harvard Street, Brookline, MA
					 
					 $country_from =  $warehouse_country_code; // country code from warehouse
					 if(empty($country_from)) 
					 $country_from = 'US'; // This is main warehouse 305 Harvard Street, Brookline, MA
					
					$postCode_to = $address->getPostcode(); // delivery location details
					$country_to = $address->getCountry(); // delivery location details
					
					$shipping_amount = $order->getShippingAmount();
					
					$timeTransit_response = $this->timeInTransit($tmp_pickup_date, $postCode_to, $country_to, $postCode_from, $country_from, $delivery_date_day, $shipping_amount, $transit_final_zip_2, $city_from);
					
					$is_shortlisted_service_method = false;
					
					$ups_end_method = end($timeTransit_response);
					if ($ups_end_method->Service->Code == "GND") {// first check with ground method with pickup date,delvery time diff as <=48 hrs
						if ($ups_end_method->EstimatedArrival->BusinessDaysInTransit <= 2) {
							//check if transit days is <= 48 hrs
							$gnd_arrival_datetime = array();
							$gnd_pickup_datetime_scheduled = array();

							$gnd_pickup_date = $ups_end_method->EstimatedArrival->Pickup->Date;
							$gnd_pickup_time = $ups_end_method->EstimatedArrival->Pickup->Time;
							$gnd_pickup_datetime_scheduled['year'] = substr($gnd_pickup_date, 0, 4);
							$gnd_pickup_datetime_scheduled['month'] = substr($gnd_pickup_date, 4, 2);
							$gnd_pickup_datetime_scheduled['day'] = substr($gnd_pickup_date, 6, 2);
							$gnd_pickup_datetime_scheduled['hrs'] = substr($gnd_pickup_time, 0, 2);
							$gnd_pickup_datetime_scheduled['mins'] = substr($gnd_pickup_time, 2, 2);
							$gnd_pickup_datetime_scheduled['secs'] = substr($gnd_pickup_time, 4, 2);

							$gnd_arrival_date = $ups_end_method->EstimatedArrival->Arrival->Date;
							$gnd_arrival_time = $ups_end_method->EstimatedArrival->Arrival->Time;
							$gnd_arrival_datetime['year'] = substr($gnd_arrival_date, 0, 4);
							$gnd_arrival_datetime['month'] = substr($gnd_arrival_date, 4, 2);
							$gnd_arrival_datetime['day'] = substr($gnd_arrival_date, 6, 2);
							$gnd_arrival_datetime['hrs'] = substr($gnd_arrival_time, 0, 2);
							$gnd_arrival_datetime['mins'] = substr($gnd_arrival_time, 2, 2);
							$gnd_arrival_datetime['secs'] = substr($gnd_arrival_time, 4, 2);

							$gnd_time_diff = strtotime($gnd_arrival_datetime['year'] . '-' . $gnd_arrival_datetime['month'] . '-' . $gnd_arrival_datetime['day'] . ' ' . $gnd_arrival_datetime['hrs'] . ':' . $gnd_arrival_datetime['mins'] . ':' . $gnd_arrival_datetime['secs']) - strtotime($tmp_pickup_datetime['year'] . '-' . $tmp_pickup_datetime['month'] . '-' . $tmp_pickup_datetime['day'] . ' ' . $tmp_pickup_datetime['hrs'] . ':' . $tmp_pickup_datetime['mins'] . ':' . $tmp_pickup_datetime['secs']); // time diff bw tmp pickup and ups grnd delivery
							//print_r($gnd_arrival_datetime);die;
							
							if ($gnd_time_diff < 172800) {
								if ($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer delivery date
									$shortlisted_ups_service_code = $ups_end_method->Service->Code;
									$shortlisted_ups_service_desc = $ups_end_method->Service->Description;
									$shortlisted_ups_service_pickup_datetime = $gnd_pickup_datetime_scheduled;

									array_pop($timeTransit_response); // remove the last element from array which is ground
									$is_shortlisted_service_method = true;
								} // if($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']){	END					
							} // if($gnd_time_diff < 172800){ END
						} // if($ups_end_method->EstimatedArrival->BusinessDaysInTransit <= 2){ END 
					} // if($ups_end_method->Service->Code == "GND"){ END
					
					if (!$is_shortlisted_service_method) { // execute foreach if service is not shortlisted
						$tmp_time_diff = 0;
						foreach ($timeTransit_response as $each_timeTransit_response) {
							if ($each_timeTransit_response->EstimatedArrival->BusinessDaysInTransit <= 2) {
								//check if transit days is <= 48 hrs
								$air_arrival_datetime = array();
								$air_pickup_datetime_scheduled = array();

								$air_pickup_date = $each_timeTransit_response->EstimatedArrival->Pickup->Date;
								$air_pickup_time = $each_timeTransit_response->EstimatedArrival->Pickup->Time;
								$air_pickup_datetime_scheduled['year'] = substr($air_pickup_date, 0, 4);
								$air_pickup_datetime_scheduled['month'] = substr($air_pickup_date, 4, 2);
								$air_pickup_datetime_scheduled['day'] = substr($air_pickup_date, 6, 2);
								$air_pickup_datetime_scheduled['hrs'] = substr($air_pickup_time, 0, 2);
								$air_pickup_datetime_scheduled['mins'] = substr($air_pickup_time, 2, 2);
								$air_pickup_datetime_scheduled['secs'] = substr($air_pickup_time, 4, 2);

								$air_arrival_date = $each_timeTransit_response->EstimatedArrival->Arrival->Date;
								$air_arrival_time = $each_timeTransit_response->EstimatedArrival->Arrival->Time;
								$air_arrival_datetime['year'] = substr($air_arrival_date, 0, 4);
								$air_arrival_datetime['month'] = substr($air_arrival_date, 4, 2);
								$air_arrival_datetime['day'] = substr($air_arrival_date, 6, 2);
								$air_arrival_datetime['hrs'] = substr($air_arrival_time, 0, 2);
								$air_arrival_datetime['mins'] = substr($air_arrival_time, 2, 2);
								$air_arrival_datetime['secs'] = substr($air_arrival_time, 4, 2);
								
								$air_time_diff = strtotime($air_arrival_datetime['year'] . '-' . $air_arrival_datetime['month'] . '-' . $air_arrival_datetime['day'] . ' ' . $air_arrival_datetime['hrs'] . ':' . $air_arrival_datetime['mins'] . ':' . $air_arrival_datetime['secs']) - strtotime($tmp_pickup_datetime['year'] . '-' . $tmp_pickup_datetime['month'] . '-' . $tmp_pickup_datetime['day'] . ' ' . $tmp_pickup_datetime['hrs'] . ':' . $tmp_pickup_datetime['mins'] . ':' . $tmp_pickup_datetime['secs']); // time diff bw tmp pickup and ups air delivery
								//print_r($air_arrival_datetime);die;
								//echo 'air_time_diff :'.$air_time_diff.'tmp_delivery_date day :'.$tmp_delivery_date['day'].'tmp_delivery_date day :'.$tmp_delivery_date['day']
								if ($air_time_diff < 172800) {
									if ($tmp_delivery_date['day'] == $air_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer delivery date
										//print_r($air_arrival_datetime);die;
										if ($air_time_diff > $tmp_time_diff) {
											$tmp_time_diff = $air_time_diff;

											$shortlisted_ups_service_code = $each_timeTransit_response->Service->Code;
											$shortlisted_ups_service_desc = $each_timeTransit_response->Service->Description;
											$shortlisted_ups_service_pickup_datetime = $air_pickup_datetime_scheduled;

											$is_shortlisted_service_method = true;
										} // if($air_time_diff > $tmp_time_diff){ END
									} // if($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']){	END
								} // if($air_time_diff < 172800){ END
							} // if($each_timeTransit_response->EstimatedArrival->BusinessDaysInTransit <= 2){ END
						} // foreach($timeTransit_response as $each_timeTransit_response){ END
					} // if(!$is_shortlisted_service_method){ END
					
					
					
					
					$pickupDate = $shortlisted_ups_service_pickup_datetime['year'] . '-' . $shortlisted_ups_service_pickup_datetime['month'] . '-' . $shortlisted_ups_service_pickup_datetime['day'] . ' ' . $shortlisted_ups_service_pickup_datetime['hrs'] . ':' . $shortlisted_ups_service_pickup_datetime['mins'] . ':' . $shortlisted_ups_service_pickup_datetime['secs']; // '2015-02-23 11:11:11' format

					 $query = "UPDATE sales_flat_order SET pickup_date = '" . $pickupDate . "',pickup_code = '" . $shortlisted_ups_service_code . "',pickup_method = '" . $shortlisted_ups_service_desc . "' WHERE entity_id = " . $orderId;
					 
					$writeConnection->query($query);
					
					$query_2 = "UPDATE sales_flat_order_grid SET pickup_date = '" . $pickupDate . "',pickup_method = '" . $shortlisted_ups_service_desc . "',delivering_date = '".$delivery_date."' WHERE entity_id = " . $orderId;
					 
					$writeConnection->query($query_2);
					//print_r($tmp_delivery_date);
			}
		}
		$this->_redirectReferer();
		//print_r($order_ids);	
	}
	
	public function timeInTransit($tmp_pickup_date, $postCode_to, $country_to, $postCode_from, $country_from, $delivery_date_day, $shipping_amount, $transit_final_zip_2, $city_from) {
        //Configuration
        $access = Mage::getStoreConfig('upslabel/credentials/accesslicensenumber'); //"8CCFD3D445A6F805";
        $userid = Mage::getStoreConfig('upslabel/credentials/userid'); //"newKaBloom";
        $passwd = Mage::getStoreConfig('upslabel/credentials/password'); //"Rosegarden123";
        $wsdl = Mage::getModuleDir('etc', 'Mage_Usa') . DS . 'wsdl' . DS . 'Ups' . DS . 'TNTWS.wsdl';
        $operation = "ProcessTimeInTransit";
        $endpointurl = 'https://wwwcie.ups.com/webservices/TimeInTransit';
        $outputFileName = "XOLTResult.xml";

        try {

            $mode = array
                (
                'soap_version' => 'SOAP_1_1', // use soap 1.1 client
                'trace' => 1
            );

            // initialize soap client
            $client = new SoapClient($wsdl, $mode);

            //set endpoint url
            $client->__setLocation($endpointurl);

            //create soap header
            $usernameToken['Username'] = $userid;
            $usernameToken['Password'] = $passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;

            $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', $upss);
            $client->__setSoapHeaders($header);

            //create soap request
            $requestoption['RequestOption'] = 'TNT';
            $request['Request'] = $requestoption;

            $addressFrom['City'] = $city_from;
            $addressFrom['CountryCode'] = $country_from; //'US';
            $addressFrom['PostalCode'] = $postCode_from; //'02446';
            $addressFrom['StateProvinceCode'] = '';
            $shipFrom['Address'] = $addressFrom;
            $request['ShipFrom'] = $shipFrom;

            $addressTo['City'] = '';
            $addressTo['CountryCode'] = $country_to; //'US';
            $addressTo['PostalCode'] = $postCode_to; //'55447';
            $addressTo['StateProvinceCode'] = '';
            $shipTo['Address'] = $addressTo;
            $request['ShipTo'] = $shipTo;

            $pickup['Date'] = $tmp_pickup_date; //'20150205'; //Ymd format
            $request['Pickup'] = $pickup;

            $unitOfMeasurement['Code'] = 'KGS';
            $unitOfMeasurement['Description'] = 'Kilograms';
            $shipmentWeight['UnitOfMeasurement'] = $unitOfMeasurement;
            $shipmentWeight['Weight'] = '2';
            $request['ShipmentWeight'] = $shipmentWeight;

            $request['TotalPackagesInShipment'] = '1';

            $invoiceLineTotal['CurrencyCode'] = 'CAD';
            $invoiceLineTotal['MonetaryValue'] = '10';
            $request['InvoiceLineTotal'] = $invoiceLineTotal;

            $request['MaximumListSize'] = '1';
            //echo '<pre>';
            //echo "Request.......\n";
            //print_r($request);
            //echo "\n\n";
            //get response
            $resp = $client->__soapCall($operation, array($request));
            //get status
            //echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";
            //save soap request and response to file

            $fw = fopen($outputFileName, 'w');
            //var_dump(get_object_vars($resp));
            $tmp_array = $resp->TransitResponse->ServiceSummary; //die;

            if (isset($shipping_amount)) {
                $tocheck_shortest_routes = $tmp_array;
                // can't explain, test b/w 02446 - 02482 u will understand to foreach loop
                $not_found = false;
                $not_found_count = 0;
                foreach ($tocheck_shortest_routes as $each_timeTransit_response) {
                    $service_arrival_date = $each_timeTransit_response->EstimatedArrival->Arrival->Date;
                    $air_arrival_datetime['day'] = substr($service_arrival_date, 6, 2);
                    if ($delivery_date_day == $air_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer 		delivery date
                        $not_found = false;
                    } else {
                        $not_found = true;
                        $not_found_count += 1;
                    }
                }

                if ($not_found && (count($tocheck_shortest_routes) == $not_found_count)) {
                    // This executes when - 2 days pick methods not not deliveing on delivery date, so here update tmp pickup date
                    $tmp_pickup_datetime_year = substr($tmp_pickup_date, 0, 4);
                    $tmp_pickup_datetime_month = substr($tmp_pickup_date, 4, 2);
                    $tmp_pickup_datetime_day = substr($tmp_pickup_date, 6, 2);
                    $tmp_pickup_datetime = $tmp_pickup_datetime_year . '-' . $tmp_pickup_datetime_month . '-' . $tmp_pickup_datetime_day;
                    
                    $tmp_pickup_date_new = str_replace("-", "", date('Y-m-d', strtotime($tmp_pickup_datetime . ' + 1 days')));
                    
                    $addressFrom['City'] = '';
                    $addressFrom['CountryCode'] = $country_from; //'US';
                    $addressFrom['PostalCode'] = $transit_final_zip_2; 
                    $addressFrom['StateProvinceCode'] = '';
                    $shipFrom['Address'] = $addressFrom;
                    $request['ShipFrom'] = $shipFrom;
                    
                    $pickup['Date'] = $tmp_pickup_date_new; //'20150205'; //Ymd format
                    $request['Pickup'] = $pickup;

                    $resp = $client->__soapCall($operation, array($request));
                    $tmp_array = $resp->TransitResponse->ServiceSummary; //die;
                }
            }

            return $tmp_array;
            //echo count($tmp_array);print_r($tmp_array);die;
            // fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
            // fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
            // fclose($fw);
        } catch (Exception $e) {
            print_r($e);
        }
    }
}
?>