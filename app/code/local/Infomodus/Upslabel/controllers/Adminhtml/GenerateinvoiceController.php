<?php

require_once Mage::getBaseDir() . '/amazon/entities/Order.php';
require_once Mage::getBaseDir() . '/amazon/entities/Package.php';
require_once Mage::getBaseDir() . '/amazon/entities/Product.php';
require_once Mage::getBaseDir() . '/amazon/entities/Address.php';
require_once Mage::getBaseDir() . '/amazon/entities/Invoice.php';
require_once Mage::getBaseDir() . '/amazon/edi/810.php';
require_once Mage::getBaseDir() . '/amazon/constants.php';
require_once Mage::getBaseDir() . '/amazon/essentials/write.php';

class Infomodus_Upslabel_Adminhtml_GenerateinvoiceController extends Mage_Core_Controller_Front_Action {

    public function indexAction($order_ids, $type, $ptype, $club = false) {

        $ptype = $this->getRequest()->getParam('type');
        $order_ids = $this->getRequest()->getParam($ptype . '_ids');
		if($this->getRequest()->getParam('track_id')){
			$track_id = $this->getRequest()->getParam('track_id');
			if(strlen($track_id) >= 30){
				$track_id = substr($track_id, -12);
			}
			
			$trackingNumbers = Mage::getResourceModel('sales/order_shipment_track_collection')
								->addAttributeToSelect('*')
								->addAttributeToFilter('track_number',$track_id);
								
								
			$trackingNumbers->getSelect()->order('order_id desc')->limit(1);
			$trackData = $trackingNumbers->getData();
			$order_idss = $trackData[0]['order_id'];
			$orderData = Mage::getModel("sales/order")->load($order_idss);
			$sendorder = $orderData->getId();
			$order_ids[] = $orderData->getId();	 
		}
		
	$i = 0;

        foreach ($order_ids as $id) {

            $i++;
            $order = Mage::getModel('sales/order')->load($id);

	$x = Mage::getSingleton("partner/partner")->getCollection()->addFieldToSelect('priority')->addFieldToFilter('partner_name', array('eq'=>$order->getTradingPartner()));
	$res = $x->getData();

//if($res[0]['priority']==0 && $order->getStatus() =='track_sent')

//{

//$invoices = Mage::getResourceModel('sales/order_invoice_collection')
                //->setOrderFilter($id)
                //->load();
                ////print_r($invoices);die;
//$invoice_id = $invoices->getData()[0]["increment_id"];
//echo $invoice_id; die;
           $_invoice = $order->getInvoiceCollection()->getLastItem();
            //print_r($order->getData()); die;
            if($_invoice->getId()==""){
				          //Generating Invoice starts here 
                //$order=Mage::getModel('sales/order')->load($order_id);  
                $pickup_method = $order->getPickupMethod();               
                //if ((($order->getStatus() == Mage_Sales_Model_Order::STATE_NEW) || ($order->getStatus() == Mage_Sales_Model_Order::STATE_PROCESSING)) && ($pickup_method != "Local Delivery")) {
                        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                        $invoice->register();

                        $invoice->getOrder()->setCustomerNoteNotify(false);          
                        $invoice->getOrder()->setIsInProcess(true);
                        $order->addStatusHistoryComment('Automatically INVOICED by Kabloom Development Team.', false);

                        $transactionSave = Mage::getModel('core/resource_transaction')
                                ->addObject($invoice)
                                ->addObject($invoice->getOrder());

                        $transactionSave->save();
               // }
          //Generating Invoice ends here
				
				}
		$_invoice = $order->getInvoiceCollection()->getLastItem();
            $amazon_order[$i] = new Order();

            $amazon_order[$i]->orderId = $order->getId();
            $amazon_order[$i]->partnerOrderId = $order->getPartnerOrderId();
            $amazon_order[$i]->customerOrderNumber = $order->getCustomerOrderNumber();


            $amazon_order[$i]->warehouse = $order->getWarehouse();

            $amazon_order[$i]->orderDate = $order->getCreatedAt();

            $product = new Product();
            $ordered_items = $order->getAllItems();
            $products = array();
            $j = 0;
            foreach ($ordered_items as $item) {
                $product = new Product();
                $product->id = $item->getProductId();
                $product->sku = $item->getSku();
                $product->quantity = $item->getQtyOrdered();
                $product->price = $item->getPrice();
                $product->title = $item->getName();
		$product->shipped = $item->getQtyShipped();
                $products[$j] = $product;
                $j++;
            }
            $invoice = new Invoice();
            $invoice->invoiceNumber = $_invoice->getId();

            $amazon_order[$i]->invoice = $invoice;


            $amazon_order[$i]->package = $package;
            $amazon_order[$i]->products = $products;

            $shipping_address = new Address();
            $shipping_address->firstname = $order->getShippingAddress()->getFirstname();
            $shipping_address->lastname = $order->getShippingAddress()->getLastname();
            $shipping_address->street = $order->getShippingAddress()->getStreet();
            $shipping_address->city = $order->getShippingAddress()->getCity();
            $shipping_address->region = $order->getShippingAddress()->getRegion();
$shipping_address->regionCode = $order->getShippingAddress()->getRegionCode();

            $shipping_address->telephone = $order->getShippingAddress()->getTelephone();
            $shipping_address->postcode = $order->getShippingAddress()->getPostcode();
            $shipping_address->countryId = $order->getShippingAddress()->getCountryId();

            $billing_address = new Address();
            $billing_address->firstname = $order->getBillingAddress()->getFirstname();
            $billing_address->lastname = $order->getBillingAddress()->getLastname();
            $billing_address->street = $order->getBillingAddress()->getStreet();
            $billing_address->city = $order->getBillingAddress()->getCity();
            $billing_address->regionCode = $order->getBillingAddress()->getRegionCode();
            $billing_address->telephone = $order->getBillingAddress()->getTelephone();
            $billing_address->postcode = $order->getBillingAddress()->getPostcode();
            $billing_address->countryId = $order->getBillingAddress()->getCountryId();



            $amazon_order[$i]->shipmentAddress = $shipping_address;
            $amazon_order[$i]->billingAddress = $billing_address;

            $icn = rand(100000000, 999999999);
            $edi = generateResponse_810($amazon_order[$i], $icn);
            write_edi($edi, $icn);
 $order->setStatus('invoice_sent')->save();
//}
        }
//print_r($amazon_order);die;

Mage::getSingleton('adminhtml/session')->addSuccess('Invoice Generated Successfully'); 
			if($club){
				
				
				$refererUrl = $this->_getRefererUrl().'orderref/'.$sendorder;
				$this->getResponse()->setRedirect($refererUrl);
				return $this;
				//$this->_redirect('*/*/', array('_query', array('orderref',$sendorder)));				
				//$this->_redirect('admin_picklist/adminhtml_picklistsendtrack/orderref/'.$sendorder);
			} else{
				$this->_redirect('adminhtml/sales_order');
			}
//$this->_redirect('adminhtml/sales_order');
    }

}

?>
