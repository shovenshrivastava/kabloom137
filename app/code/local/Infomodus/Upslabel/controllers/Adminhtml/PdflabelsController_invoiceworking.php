<?php

/*
 * Author Rudyuk Vitalij Anatolievich
 * Email rvansp@gmail.com
 * Blog www.cervic.info
 */
require('ex.php');
class Infomodus_Upslabel_Adminhtml_PdflabelsController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
		
        $ptype = $this->getRequest()->getParam('type');
		
        if ($ptype != 'lists') {
            $type = 'shipment';
            $order_ids = $this->getRequest()->getParam($ptype . '_ids');
            if ($ptype == 'creditmemo') {
                $ptype = 'shipment';
                $type = 'refund';
            }
		
            $resp = $this->create($order_ids, $type, $ptype);
        } else {
            $order_ids = $this->getRequest()->getParam('upslabel');
            $resp = $this->createFromLists($order_ids);
        }

        if (!$resp) {
            $this->_redirectReferer();
        }
    }

    public function onepdfAction()
    {
			
        $order_id = $this->getRequest()->getParam('order_id');
        $shipment_id = $this->getRequest()->getParam('shipment_id');
        $type = $this->getRequest()->getParam('type');
        $img_path = Mage::getBaseDir('media') . '/upslabel/label/';
        $url_image_path = Mage::getBaseUrl('media') . 'upslabel/label/';
        $pdf = new Zend_Pdf();
        $i = 0;
        $collections = Mage::getModel('upslabel/upslabel');
        $colls = $collections->getCollection()->addFieldToFilter('order_id', $order_id)->addFieldToFilter('shipment_id', $shipment_id)->addFieldToFilter('type', $type)->addFieldToFilter('status', 0);
        foreach ($colls AS $k => $v) {
            $coll = $v['upslabel_id'];
            break;
        }
        $width = strlen(Mage::getStoreConfig('upslabel/printing/dimensionx')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensionx') : 1400 / 2.6;
        $heigh = strlen(Mage::getStoreConfig('upslabel/printing/dimensiony')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensiony') : 800 / 2.6;
        if (strlen(Mage::getStoreConfig('upslabel/printing/holstx')) > 0 && strlen(Mage::getStoreConfig('upslabel/printing/holsty')) > 0) {
            $holstSize = Mage::getStoreConfig('upslabel/printing/holstx') . ':' . Mage::getStoreConfig('upslabel/printing/holsty') . ':';
        } else {
            $holstSize = Zend_Pdf_Page::SIZE_A4;
        }
        $collection_one = Mage::getModel('upslabel/upslabel')->load($coll);
        if ($collection_one->getOrderId() == $order_id) {
            foreach ($colls AS $collection) {
                if (file_exists($img_path . $collection->getLabelname()) && filesize($img_path . $collection->getLabelname()) > 1024) {
                    $page = $pdf->newPage($holstSize);
                    $pdf->pages[] = $page;
                    $f_cont = file_get_contents($img_path . $collection->getLabelname());
                    $img = imagecreatefromstring($f_cont);
                    if (Mage::getStoreConfig('upslabel/printing/verticalprint') == 1) {
                        $FullImage_width = imagesx($img);
                        $FullImage_height = imagesy($img);
                        $full_id = imagecreatetruecolor($FullImage_width, $FullImage_height);
                        $col = imagecolorallocate($img, 125, 174, 240);
                        $IMGfuul = imagerotate($img, -90, $col);
                    } else {
                        $IMGfuul = $img;
                    }
                    $rnd = rand(10000, 999999);
                    imagejpeg($IMGfuul, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                    $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
					$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					$page->setFont($font, 12);
					
					$order=Mage::getModel('sales/order')->load($order_id); 
					
					$orderItems = $order->getItemsCollection()
							->addAttributeToSelect('*')
							->load();
					$page->drawImage($image, 20, 350, $width+20, $heigh+350); //draw shipping label ups
					//shipping adddress
					$line = 340;
					$address = $order->getShippingAddress();
					$custName = $address->getName();
					$custAddr = $address->getStreetFull();
					$region = $address->getRegion();
					$country = $address->getCountry();
					$zipcode = $address->getPostcode();
					$baddress = $order->getBillingAddress();
					$bcustName = $baddress->getName();
					$bcustAddr = $baddress->getStreetFull();
					$bregion = $baddress->getRegion();
					$bcountry = $baddress->getCountry();
					$bzipcode = $baddress->getPostcode();
					$page->drawText("Ship to: ", 20, $line, 'UTF-8');
					$page->drawText("Bill to: ", 120, $line, 'UTF-8');
					$line -=14;
					$page->drawText($custName, 20, $line, 'UTF-8');
					$page->drawText($bcustName, 120, $line, 'UTF-8');
					$line -=14;
					$page->drawText($custAddr, 20, $line, 'UTF-8');
					$page->drawText($bcustAddr, 120, $line, 'UTF-8');
					$line -=14;
					$page->drawText($region, 20, $line, 'UTF-8');
					$page->drawText($bregion, 120, $line, 'UTF-8');
					$line -=14;
					$page->drawText($country, 20, $line, 'UTF-8');
					$page->drawText($bcountry, 120, $line, 'UTF-8');
					$line -=14;
					$page->drawText($zipcode, 20, $line, 'UTF-8');
					$page->drawText($bzipcode, 120, $line, 'UTF-8');
					$line -=30;
                                                                                                      
					$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
					$page->setFont($font2, 12);
					$page->drawText("Order #:".$order->getIncrementId(), 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText("Product Information:", 20, $line, 'UTF-8');
					$line -=14;
					$page->setFont($font, 10);
					foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
					$page->drawText("Name: ".$sItem->getName(), 20, $line, 'UTF-8');
					$page->drawText("Ordered quantity: ".$sItem->getData('qty_ordered'), 20, $line-14, 'UTF-8');
					$line -=28;
					}
					}

//$message = Mage::getModel('giftmessage/message');

/* Add Gift Message*/ 
/*$gift_message_id = $order->getOrder()->getGiftMessageId();
if(!is_null($gift_message_id)) {
$message->load((int)$gift_message_id);
$gift_sender = $message->getData('sender');
$gift_recipient = $message->getData('recipient');
$gift_message = $message->getData('message');
$page->drawText("Gift Message: ".$gift_message, 20, $line-14, 'UTF-8');
}*/
               unlink($img_path . 'lbl' . $rnd . '.jpeg');
                    $i++;
                }
            }
        }
        if ($i > 0) {
            $pdfData = $pdf->render();
            header("Content-Disposition: inline; filename=result.pdf");
            header("Content-type: application/x-pdf");
            echo $pdfData;
        }
    }

   static public function create($order_ids, $type, $ptype, $pickship = false, $pickpack = false, $giftprint = false)
    {
        
        $img_path = Mage::getBaseDir('media') . '/upslabel/label/';
        $pdf = new Zend_Pdf();
        $i = 0;
		$j= 0;
		$feds = 0;
		$upscount = 0;
		
        //$pdf->pages = array_reverse($pdf->pages);
        if (!is_array($order_ids)) {
            $order_ids = explode(',', $order_ids);
            
        }
        $width = strlen(Mage::getStoreConfig('upslabel/printing/dimensionx')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensionx') : 1400 / 2.6;
        $heigh = strlen(Mage::getStoreConfig('upslabel/printing/dimensiony')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensiony') : 1600 / 2.6;
        if (strlen(Mage::getStoreConfig('upslabel/printing/holstx')) > 0 && strlen(Mage::getStoreConfig('upslabel/printing/holsty')) > 0) {
            $holstSize = Mage::getStoreConfig('upslabel/printing/holstx') . ':' . Mage::getStoreConfig('upslabel/printing/holsty') . ':';
        } else {
            $holstSize = Zend_Pdf_Page::SIZE_LETTER;
        }
		
        foreach ($order_ids as $order_id) {
			
			
		   $collections = Mage::getModel('upslabel/upslabel');
		   $colls = $collections->getCollection()->addFieldToFilter($ptype . '_id', $order_id)->addFieldToFilter('type', $type)->addFieldToFilter('status', 0);	  
		
            $order = Mage::getModel('sales/order')->load($order_id); 
            $orderItems = $order->getItemsCollection()->addAttributeToSelect('*')->load();
            $orderDate = date('F d, Y', strtotime($order->getCreatedAt()));	
            $giftMessage = Mage::getModel("giftmessage/message")->load($order->getGiftMessageId());
		    $giftMessageNote = $giftMessage->getMessage();		
		    $giftMessageSender = $giftMessage->getSender();  
		    $amazonorder = ($order->getTradingPartner()==("amazon")||$order->getTradingPartner()==("amazon-marketplace"));
            $targetorder = $order->getTradingPartner()==("target");
            $jetorder = $order->getTradingPartner()==("Jet");
            $pickup_mehtod = $order->getPickupMethod();
            $ups = (strripos($pickup_mehtod,'ups'));
            $fde = strripos($pickup_mehtod,'fde');
            $fedex = strripos($pickup_mehtod,'fedex');
		    $address = $order->getShippingAddress();
			//print_r($address->getName()); die;
			$custName = $address->getName();
			$custAddr = $address->getStreetFull();
			$region = $address->getRegion();
			$country = $address->getCountry();
			$zipcode = $address->getPostcode();
			$baddress = $order->getBillingAddress();
			$bcustName = $baddress->getName();
			$bcustAddr = $baddress->getStreetFull();
			$bregion = $baddress->getRegion();
			$bcountry = $baddress->getCountry();
			$bzipcode = $baddress->getPostcode();
	  
	  
			// amazon order PDF create starts 
            if($amazonorder){
				
			
			foreach ($colls AS $k => $v) 
				{
			$coll = $v['upslabel_id'];
			 
			$collection = Mage::getModel('upslabel/upslabel')->load($coll);
			
			if (($collection->getOrderId() == $order_id && $ptype == "order") || ($collection->getShipmentId() == $order_id && $ptype != "order")) {
			$page = $pdf->newPage($holstSize);
			$pdf->pages[] = $page;
			//picklist
			if(($pickpack && !$pickship && !$giftprint) || (!$pickpack && !$pickship && !$giftprint)) {
			$image = Zend_Pdf_Image::imageWithPath($img_path.'/amazonlabelfull.jpg' );
				
			  $pdf1=new PDF_AutoPrint();
			  $pdf1->AddPage();
			  $pdf1->SetFont('Arial','B',20);
			  $pdf1->Rotate(90);
			  $pdf1->Image($img_path.'/amazonlabelfull.jpg',-360,10,390,250);
			  $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			  $page->setFont($font, 11);
			  $order=Mage::getModel('sales/order')->load($order_id); 

			  
			  $page->drawImage($image,-180, $heigh-100,660,1015); //draw shipping label ups
			  
			  $line = 860;
			  $new_width = 0;
			  $new_line = $heigh;
			  $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			  $page->setFont($font5, 11);
			  $page->drawText("Billing Address : ", 20, $line, 'UTF-8');
			  $pdf1->SetFont('Arial','B',14);
			  $pdf1->Text(-250, 60, 'Billing Address : ');
			  $line -=14;
			  $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			  $page->setFont($font5, 8);
			  $page->drawText($bcustName, 20, $line, 'UTF-8');
			  $pdf1->SetFont('Arial','B',12);
			  $pdf1->Text(-250, 65, $bcustName);
			  $line -=10;
			  $page->drawText($bcustAddr, 20, $line, 'UTF-8');
			  
			  $pdf1->Text(-250, 70, $bcustAddr);
			  $line -=10;
			  $page->drawText($bregion.', '.$bcountry.' - '.$bzipcode, 20, $line, 'UTF-8');
			  
			  $pdf1->Text(-250, 75, $bregion.', '.$bcountry.' - '.$bzipcode);
			  $line -=30;
			  $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			  $page->setFont($font5, 11);
			  $page->drawText("Shipping Address : ", 20, $line, 'UTF-8');
			  $pdf1->SetFont('Arial','B',14);
			  $pdf1->Text(-250, 85, "Shipping Address : ");			  
			  $line -=14;
			  $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			  $page->setFont($font5, 8);
			  $page->drawText($custName, 20, $line, 'UTF-8');	
			  $pdf1->SetFont('Arial','B',12);
			  $pdf1->Text(-250, 90, $custName);				  
			  $line -=10;
			  $page->drawText($custAddr, 20, $line, 'UTF-8');
			  
			  $pdf1->Text(-250, 95, $custAddr);			  
			  $line -=10;
			  $page->drawText($region.', '.$country.' - '.$zipcode, 20, $line, 'UTF-8');
			  
			  $pdf1->Text(-250, 100, $region.', '.$country.' - '.$zipcode);	
			  $line -=20; 
			  $new_width = 20;		 
			  $new_line = 725;
			  $font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			  $page->setLineWidth(0.5)->drawLine(0,$new_line-4,380,$new_line-4);
			  $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
     		  $page->setFont($font5, 10);			  
			  $page->drawText("Your Order of  ".$orderDate."  (". $order->getIncrementId().")", $new_width,$new_line, 'UTF-8');
			  $pdf1->SetFont('Arial','B',14);
			 
			  $pdf1->Text(-250, 110, "Your Order of  ".$orderDate."  (". $order->getIncrementId().")");
			  $new_line -=14;		   
			  $page->setLineWidth(0.5)->drawLine(0,$new_line-6,380,$new_line-6);
			  $page->setFont($font, 8);
			  $page->drawText("Qty", $new_width,$new_line, 'UTF-8');
			  $page->drawText("Item", $new_width+25,$new_line, 'UTF-8');
			  $page->drawText("Item Price", $new_width+260,$new_line, 'UTF-8');
			  $page->drawText("Total", $new_width+320,$new_line, 'UTF-8');
			  $pdf1->SetFont('Arial','B',12);
			  $pdf1->Text(-250, 120, "Qty");
			  $pdf1->Text(-240, 120, "Item");
			  $pdf1->Text(-150, 120, "Item Price");
			  $pdf1->Text(-120, 120, "Total");
			  $new_line -=14;
			  foreach($orderItems as $sItem) {
			  if($sItem->getProductType() == "simple"){
			  $_Pdetails = Mage::getSingleton('catalog/product')->loadByAttribute('sku',$sItem->getSku());
			  $pdt_name = $_Pdetails->getName();//$sItem->getName();               
			  $page->drawText('IN THIS SHIPMENT', $new_width+25, $new_line, 'UTF-8');
			  $pdf1->Text(-240, 130, 'IN THIS SHIPMENT');
			  for($lp=0;$lp<strlen($pdt_name);$lp=($lp+35)){
			 
				  $pdf1->Text(-240, 140+$pdn, substr($pdt_name,$lp,35));
				  if(($lp+35)<strlen($pdt_name)) {
					  $pdn = 5;
					  
				  }
			  } 
			  for($lp=0;$lp<strlen($pdt_name);$lp=($lp+58)){
			  $page->drawText(substr($pdt_name,$lp,58), $new_width+25, $new_line-14, 'UTF-8');
			  
			  if(($lp+58)<strlen($pdt_name)) {
				 
				  $new_line -=10;
			  }
			  } 
		     $page->drawText(intval($sItem->getData('qty_ordered')), $new_width, $new_line, 'UTF-8');
			 $pdf1->Text(-250, 140, intval($sItem->getData('qty_ordered')));
		     if(empty($giftMessageNote)){
			  $page->drawText("$".floatval($sItem->getPrice()), $new_width+260, $new_line, 'UTF-8');
			  $page->drawText("$".floatval($order->getGrandTotal()), $new_width+320, $new_line, 'UTF-8'); 
			  $pdf1->Text(-150, 140, "$".floatval($sItem->getPrice()));	
              $pdf1->Text(-120, 140, "$".floatval($order->getGrandTotal()));			  
						}
					}
				}
		   
				$page->drawText('Amazon.com, LLC', $new_width+25, $new_line-35, 'UTF-8');
				$pdf1->Text(-240, 160, 'Amazon.com, LLC');	
				$font9 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font9, 10);
				$pdf1->SetFont('Arial','B',14);
               				
				$page->drawText($sItem->getSku(), $new_width+25, $new_line-45, 'UTF-8');
				$pdf1->Text(-240, 170, $sItem->getSku());
				$new_line -=14;	
				$font15 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFont($font15, 10);
				$new_line =675; 
				$new_width = $new_width;
				$start = 0;
				//word wrap logic for gift message starts here
				$words_key = str_word_count($giftMessageNote,2);
				$tmp_key = 58;
				$check_limit = 15;
				$start = 0;
				$lastLine = false;
				$line_chars = 65;
				$pdf1->SetFont('Arial','B',12);
				
				if(strlen($giftMessageNote) <= $line_chars){ // If the gift message has < one line
					$page->drawText(substr($giftMessageNote,$start,$line_chars), $new_width+25, $new_line-40, 'UTF-8');
                    $pdf1->Text(-240, 180+$inew, substr($giftMessageNote,$start,$line_chars));	
					$inew=5;					
					$new_line -=10;
				}else{										// If the gift message has > one line
					for($loop=0;$loop<strlen($giftMessageNote);$loop=($loop+1)){
						$tmp_value = $words_key[$tmp_key];
						if(!$lastLine){
							if(empty($tmp_value)){
								for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
									$tmp_value = $words_key[$i];
									if(!empty($tmp_value)){
										$end_word_key = $i; // previous line last word
										break;
									}
								}
								for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
									$tmp_value = $words_key[$j];
									if(!empty($tmp_value)){
										$first_word_key = $j; // next line first word
										break;
									}
								}
							}else{
								$end_word_key = $tmp_key; // previous line last word
								for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
									$tmp_value = $words_key[$j];
									if(!empty($tmp_value)){
										$first_word_key = $j; // next line first word
										break;
									}
								}
							}
						}
						$current_line_chars = $first_word_key - $start;	
						$page->drawText(substr($giftMessageNote,$start,$current_line_chars), $new_width+25, $new_line-40, 'UTF-8');											
						$pdf1->Text(-240, 180+$inew, substr($giftMessageNote,$start,$current_line_chars));
						$inew=5;
						$start = $first_word_key ;
						if(($end_word_key+40) > strlen($giftMessageNote)){
							$lastLine = true;
							$tmp_key = end(array_keys($words_key))-1;
							$first_word_key = strlen($giftMessageNote);
						}else{
							$tmp_key = $end_word_key+60;
						}
						$new_line -=10;
					}
				}									
				//word wrap logic gift message ends here
						  $new_line =625; 
						  $new_width = $new_width;
						  
						$page->setLineWidth(0.9)->drawLine(0,$new_line-35,380,$new_line-35);
						$page->drawText("This Shipment Completes Your Order.", 20, $new_line-42, 'UTF-8');
						$pdf1->Text(-250, 200,"This Shipment Completes Your Order.");
				}
				
				
				
				//amazon gift message in box
				if(!$pickpack && !$pickship && $giftprint) {
					// reciepe & gift message boxes
					$page->setFillColor(Zend_Pdf_Color_Html::color('#FFFFFF'));
					//$page->drawRectangle(20, 20, 300, 180);							
					$page->drawRectangle(320, 20, 580, 180);
					$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
					$page->setFont($font2, 11);
					$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
					$new_line = 140;
					$new_width = 340;
					//$giftMessage = Mage::getModel("giftmessage/message")->load($order->getGiftMessageId());		
					$giftMessageNote = $giftMessage->getMessage();
					$giftMessageSender = $giftMessage->getSender();
					$new_line -=14;
					//$page->drawText("From: ".$giftMessageSender, $new_width, $new_line, 'UTF-8');
					//$new_line -=14;
					$pdf1=new PDF_AutoPrint();
					$pdf1->AddPage();
					$pdf1->SetFont('Arial','',30);
					$pdf1->Rotate(90);
					$pdf1->Text(-260, 20, "Gift Message: ");
					$page->drawText("Gift Message: ", $new_width, $new_line, 'UTF-8');
					
					$new_line -=14;
					$start = 0;
					$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					$page->setFont($font2, 11);
					//word wrap logic for gift message starts here
					$words_key = str_word_count($giftMessageNote,2);
					$tmp_key = 35;
					$check_limit = 15;
					$start = 0;
					$lastLine = false;
					$line_chars = 45;
					$pdf1->SetFillColor(0, 0, 255);
					$pdf1->Rect(-270, 30, 275, 160, 'D');
					if(strlen($giftMessageNote) <= $line_chars){ // If the gift message has < one line
						$page->drawText(substr($giftMessageNote,$start,$line_chars), $new_width, $new_line, 'UTF-8');	
						for($lp=0;$lp<strlen($giftMessageNote);$lp=($lp+35)){
							  $pdf1->Text(-260, 180+$pdn, substr($giftMessageNote,$lp,35));
							  if(($lp+35)<strlen($giftMessageNote)) {
								   $pdn = $pdn+15;
								  
							  }
						  }
						$new_line -=14;
					}else{										// If the gift message has > one line
						for($loop=0;$loop<strlen($giftMessageNote);$loop=($loop+1)){
							$tmp_value = $words_key[$tmp_key];
							if(!$lastLine){
								if(empty($tmp_value)){
									for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
										$tmp_value = $words_key[$i];
										if(!empty($tmp_value)){
											$end_word_key = $i; // previous line last word
											break;
										}
									}
									for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
										$tmp_value = $words_key[$j];
										if(!empty($tmp_value)){
											$first_word_key = $j; // next line first word
											break;
										}
									}
								}else{
									$end_word_key = $tmp_key; // previous line last word
									for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
										$tmp_value = $words_key[$j];
										if(!empty($tmp_value)){
											$first_word_key = $j; // next line first word
											break;
										}
									}
								}
							}
							$current_line_chars = $first_word_key - $start;	
							//echo substr($giftMessageNote,$start,$current_line_chars);
							$page->drawText(substr($giftMessageNote,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');											
							$start = $first_word_key ;
							if(($end_word_key+40) > strlen($giftMessageNote)){
								$lastLine = true;
								$tmp_key = end(array_keys($words_key))-1;
								$first_word_key = strlen($giftMessageNote);
							}else{
								$tmp_key = $end_word_key+40;
							}
							$new_line -=14;
						}
					}										
					//word wrap logic gift message ends here
					
					//$pdf1->Cell(-250,110,$giftMessageNote,0,0,'C');
					for($lp=0;$lp<strlen($giftMessageNote);$lp=($lp+52)){
							  $pdf1->Text(-260, 52+$pdn, substr($giftMessageNote,$lp,52));
							  if(($lp+50)<strlen($giftMessageNote)) {
								  $pdn = $pdn+15;
								  
							  }
						  }
					//$pdf1->MultiCell( 150, 20, $giftMessageNote, 1);
				//$pdf1->Text(0, 80, $giftMessageNote);
				//picklist
				}
			
			
			
			
				if(!$pickpack && !$pickship && !$giftprint){
					    $page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
						$page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));							
						$page->drawRectangle(20, 20, 300, 140);							
						$page->drawRectangle(320, 20, 580, 140);
						$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
						$page->setFont($font5, 11);
						$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
						$new_line = 120;
						$new_width = 340;
                        $page->drawText("Order ID #:".$order->getIncrementId(), $new_width, $new_line, 'UTF-8');
						$new_line -=14;
						$page->drawText("Product Information:", $new_width, $new_line, 'UTF-8');
						$new_line -=14;
						$font6 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
						$page->setFont($font6, 10);
						
							foreach($orderItems as $sItem) {							
								
							 if($sItem->getProductType() == "simple"){
								 $_Pdetails = Mage::getSingleton('catalog/product')->loadByAttribute('sku',$sItem->getSku());													  
                                        $pdt_name ="Name: ". $_Pdetails->getName();//$sItem->getName();   
							           
                                        for($lp=0;$lp<strlen($pdt_name);$lp=($lp+45)){
                                            $page->drawText(substr($pdt_name,$lp,45), $new_width, $new_line, 'UTF-8');
                                            if(($lp+45)<strlen($pdt_name)) $new_line -=14;                                                                                       
                                        }     
					$page->drawText("Ordered quantity: ".round($sItem->getData('qty_ordered')), $new_width, $new_line-14, 'UTF-8');
						}
					}
			//picklist
				
					foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
											$pdt_data = Mage::getModel("catalog/product")->load($sItem->getProductId());
                                             $pdt_recipe = $pdt_data->getRecipe();
						}
					}                                      
                   $recipe_line = "Recipe: ".$pdt_recipe;
                   $new_width = $new_width-300;
                   $new_line = $new_line+30;
                   for($lp=0;$lp<strlen($recipe_line);$lp=($lp+45)){
					   $page->drawText(substr($recipe_line,$lp,45), $new_width, $new_line, 'UTF-8');
					   if(($lp+45)<strlen($recipe_line)) $new_line -=14;                                    
				}
				//picklist
                }
				if((!$pickpack && $pickship && !$giftprint) || (!$pickpack && !$pickship && !$giftprint)) {
                $page->rotate(20, 140, M_PI/2);
				$pdf1=new PDF_AutoPrint();
				$pdf1->AddPage();
				$pdf1->SetFont('Arial','',12);
				
                if($ups!== false){
					 $f_cont = file_get_contents($img_path . $collection->getLabelname());
                            $img = imagecreatefromstring($f_cont);
                            if (Mage::getStoreConfig('upslabel/printing/verticalprint') == 1) {
                                $FullImage_width = imagesx($img);
                                $FullImage_height = imagesy($img);
                                $full_id = imagecreatetruecolor($FullImage_width, $FullImage_height);
                                $col = imagecolorallocate($img, 125, 174, 240);
                                $IMGfuul = imagerotate($img, -90, $col);
                            } else {
                                $IMGfuul = $img;
                            }
                            $rnd = rand(10000, 999999);
                            imagejpeg($IMGfuul, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                            $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
                    $font99 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					$page->setFont($font99, 11);       
					$page->drawImage($image,  80, -540, 400, 140);
					$pdf1->Image($img_path. 'lbl' . $rnd . '.jpeg',0,0,280,250);
					$pdf1->Text(10, 120, 'Contains Cut Flowers, Product of Colombia and Thailand');
					$page->drawText('Contains Cut Flowers, Product of Colombia and Thailand', 100, -380);
		
				   }
				 if($fedex!== false||$fde!== false){
					$fedex_img_path = Mage::getBaseDir('media') . '/fedexlabel/label/';
                    $image = Zend_Pdf_Image::imageWithPath($fedex_img_path . $collection->getLabelname());
                    //print_r($image); die;
					$font99 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					$page->setFont($font99, 11);
                    $page->drawImage($image, 100, -400, 400, 140);
					$pdf1->Image($fedex_img_path . $collection->getLabelname(),0,0,200,260);
					$pdf1->Text(10, 270, 'Contains Cut Flowers, Product of Colombia and Thailand');
                    $page->drawText('Contains Cut Flowers, Product of Colombia and Thailand', 120, -420);	
				   }
				   //picklist
				}
				unlink($img_path . 'lbl' . $rnd . '.jpeg');
					$i++;
                        }
                    }
                     
                    unset($IMGfuul);
				//end of custom code
						}
					
					
            
		
            if($targetorder){
				
				foreach ($colls AS $k => $v) 
					{
				$coll = $v['upslabel_id'];
                $collection = Mage::getModel('upslabel/upslabel')->load($coll);
				
				if (($collection->getOrderId() == $order_id && $ptype == "order") || ($collection->getShipmentId() == $order_id && $ptype != "order")) {
				$order = Mage::getModel('sales/order')->load($order_id); 
            $orderItems = $order->getItemsCollection()->addAttributeToSelect('*')->load();
			
		//foreach ($orderItems as $orderItem){ 
			//$prodqtys = round($orderItem->getData('qty_ordered'));
			
			//for($x = 0; $x < $prodqtys; $x++) {	
					
                $page = $pdf->newPage($holstSize);
                $pdf->pages[] = $page;
				//picklist
				if(($pickpack && !$pickship && !$giftprint) || (!$pickpack && !$pickship && !$giftprint)) {
                $image = Zend_Pdf_Image::imageWithPath($img_path.'/target_logo.jpg' );
                $feds++;
                ///$rnd = rand(10000, 999999);
				$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
                $page->setFont($font, 11);
                
                //top part heading text
                $page->setFillColor(Zend_Pdf_Color_Html::color('#EEEEEE'));
				$page->drawRectangle( 0,970,620, 1020, $fillType = Zend_Pdf_Page::SHAPE_DRAW_FILL);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
				$orderDate = date('F d, Y', strtotime($order->getCreatedAt()));
				$page->setFont($font, 10);
				$page->drawText("Thankyou for Your purchase", 240, 995, 'UTF-8');
				$page->drawText("if you ordered additional items they will arrive seperatley ", 180, 982, 'UTF-8');
				
				// target Logo and barcode 
				$page->drawImage($image,10,900,240,960);				
				$newwidth = 440;
				$newheight = 960;
				$page->drawText("Order ID #: ".$order->getIncrementId(), $newwidth, $newheight, 'UTF-8'); 
				$newheight -=14;
				$page->drawText("Your Order From : ".$orderDate, $newwidth, $newheight, 'UTF-8'); 
				$newheight -=36;
			    $fontPath =   Zend_Pdf_Font::fontWithPath(Mage::getBaseDir('media') . '/fonts/fre3of9x.ttf');
     	        $page->setFont($fontPath, 36);
				$barcodeImage = "*".$order->getPartnerOrderId()."*";
				$page->drawText($barcodeImage, $newwidth, $newheight, 'UTF-8');
				$newheight -=14; 
			    $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));	
				$page->setFont($font5, 10);
				$page->drawText("Scan for returns ", $newwidth, $newheight, 'UTF-8');
				$newheight -=14;
				
				// From, Bill to and shipping address
				
				$page->setFillColor(Zend_Pdf_Color_Html::color('#555555'));
				$page->drawRectangle( 02,860,610, 880, $fillType = Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#EEEEEE'));
				$page->drawRectangle( 02,780,610, 860, $fillType = Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#FFFFFF'));	 
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font5, 11);
				$line = 865;
				$newwidth = 20;
				$page->drawText("FROM :", $newwidth, $line, 'UTF-8');					
				$line -=24;
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));	
				$page->setFont($font5, 10);
				$page->drawText("Target.com", $newwidth, $line, 'UTF-8');					
				$line -=14;
				$page->drawText("225 Transfer Drive", $newwidth, $line, 'UTF-8');					
				$line -=14;
				$page->drawText("Indianpolis, IN 46214", $newwidth, $line, 'UTF-8');
				$line = 865;
				$newwidth = 180;
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font5, 11);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#FFFFFF'));
				$page->drawText("SEND TO", $newwidth, $line, 'UTF-8');					
				$line -=24;
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFont($font5, 10);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
				$page->drawText($custName, $newwidth, $line, 'UTF-8');					
				$line -=14;
				$page->drawText($custAddr, $newwidth, $line, 'UTF-8');					
				$line -=14;
				$page->drawText($region.', '.$country.' - '.$zipcode, $newwidth, $line, 'UTF-8');
				$line = 865;
				$newwidth = 420;
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font5, 11);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#FFFFFF'));
				$page->drawText("BILL TO", $newwidth, $line, 'UTF-8');
				$line -=24;
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFont($font5, 10);
				$page->drawText($bcustName, $newwidth, $line, 'UTF-8');
				$line -=14;
				$page->drawText($bcustAddr, $newwidth, $line, 'UTF-8');
				$line -=14;
				$page->drawText($bregion.', '.$bcountry.' - '.$bzipcode, $newwidth, $line, 'UTF-8');
				
				
				//item qty desc starts here
				  $new_width = 20;		
				  $new_line = 770;
				  $font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				  $page->setFont($font, 10);
				  $page->drawText("QTY", $new_width,$new_line, 'UTF-8');
				  $page->drawText("ITEM# ", $new_width+40,$new_line, 'UTF-8');
				  $page->drawText("DPCI#", $new_width+100,$new_line, 'UTF-8');
				  $page->drawText("DESCRIPTION", $new_width+180,$new_line, 'UTF-8');
				  $page->drawText("MFG ID", $new_width+320,$new_line, 'UTF-8');
				  $page->drawText("UPC", $new_width+460,$new_line, 'UTF-8');
				  $page->drawText("RETURN", $new_width+520,$new_line, 'UTF-8');
				  $new_line -=6;
				  $page->setFont($font, 9);
				  foreach($orderItems as $sItem) {
				  if($sItem->getProductType() == "simple"){
				  $_Pdetails = Mage::getSingleton('catalog/product')->loadByAttribute('sku',$sItem->getSku());	
				  //print_r($_Pdetails->getName()); die;	
				  $font9 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				  $page->setFont($font9, 9);	
				  							  
				  $pdt_item_id = $sItem->getJetOrderItemId();   
				 
				  $page->drawText($pdt_item_id, $new_width+40, $new_line-14, 'UTF-8');
				  $pdt_name = $_Pdetails->getName();		
				  for($lp=0;$lp<strlen($pdt_name);$lp=($lp+30)){
				  $page->drawText(substr($pdt_name,$lp,30), $new_width+180, $new_line-14, 'UTF-8');
				  if(($lp+30)<strlen($pdt_name)) $new_line -=10;
							}
				  $page->drawText(intval($sItem->getData('qty_ordered')), $new_width, $new_line-14, 'UTF-8');
				  $page->drawText($_Pdetails->getData('dpci'), $new_width+100, $new_line-14, 'UTF-8');
				  $page->drawText($sItem->getSku(), $new_width+320, $new_line-14, 'UTF-8');	
				  $page->drawText($_Pdetails->getData('barcode'), $new_width+460, $new_line-14, 'UTF-8');	
						}
					}
					
					 
					 
				
				//our promise & refund paragraphs start here 
				$new_line -=14;	
				$new_line =710; 
				$new_width = 20;
					  
				$page->setFillColor(Zend_Pdf_Color_Html::color('#EEEEEE'));
				$page->drawRectangle( 02,$new_line-120,610, $new_line, $fillType = Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
				$font9 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font9, 9);
				$new_line -=10;
				$page->drawText("our promise to you ", $new_width, $new_line-10, 'UTF-8');
				$new_line -=10;
				$font9 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFont($font9, 9);
				$page->drawText("We promise to attempt a return on every item purchased in our stores or on Target.com by scanning your receipt or packing slip, offering ", $new_width, $new_line-10, 'UTF-8');
				$new_line -=10;
				$page->drawText("receipt look-up or a non-receipted return or exchange with a valid form of identification. Most items can be returned in your Target® ", $new_width, $new_line-10, 'UTF-8');
				$new_line -=10;
				$page->drawText("store as noted in the Return Method column. Just bring in this packing slip and the item. If you prefer or need to mail in your return,", $new_width, $new_line-10, 'UTF-8');
				$new_line -=10;
				$page->drawText(" go to Target.com/returns to follow the easy return process and print the prepaid Target.com return label.", $new_width, $new_line-10, 'UTF-8');
				$new_line -=16;
				
				$font9 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font9, 9);
				$page->drawText("refund/exchange policy ", $new_width, $new_line-10, 'UTF-8');
				$new_line -=10;
				$font9 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFont($font9, 9);
				$page->drawText("Most unopened items in new condition returned within 90 days will receive a refund or exchange. Some items have a modified return policy", $new_width, $new_line-10, 'UTF-8');
				$new_line -=10;
				$page->drawText(" thatis less than 90 days. Those items will show a \"return by\" date under the item on your receipt or packing slip and in the \"Item details, shipping\"", $new_width, $new_line-10, 'UTF-8');
				$new_line -=10;
				$page->drawText("tab if purchased on Target.com. Items that are opened or damaged or do not have a packing slip or receipt may be denied a refund or exchange.", $new_width, $new_line-10, 'UTF-8');
				$new_line -=10;
				$page->drawText("  All bundled items must be returned with all components for a refund. Go to Target.com/returns for full refund/exchange policy.", $new_width, $new_line-10, 'UTF-8');
				$new_line -=10;
				$page->drawText("", $new_width, $new_line-10, 'UTF-8');
				$new_line -=10;
				
				$page->setFillColor(Zend_Pdf_Color_Html::color('#EEEEEE'));
				$page->drawRectangle(0,$new_line-5,610, $new_line-20, $fillType = Zend_Pdf_Page::SHAPE_DRAW_FILL);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
				$page->setFont($font, 9);
				$page->drawText("© 2013 Target. Target and the Bullseye Design are registered trademarks of Target Brands, Inc. All rights reserved.", 40, $new_line-15, 'UTF-8');	
				
				//picklist
				}
				if((!$pickpack && !$pickship && $giftprint) || (!$pickpack && !$pickship && !$giftprint)) {
				// reciepe & gift message boxes
				$page->setFillColor(Zend_Pdf_Color_Html::color('#FFFFFF'));
				$page->drawRectangle(20, 20, 300, 180);							
				$page->drawRectangle(320, 20, 580, 180);
				$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font2, 11);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
				$new_line = 140;
				$new_width = 340;
				//$giftMessage = Mage::getModel("giftmessage/message")->load($order->getGiftMessageId());		
				$giftMessageNote = $giftMessage->getMessage();
				$giftMessageSender = $giftMessage->getSender();
				$new_line -=14;
				//$page->drawText("From: ".$giftMessageSender, $new_width, $new_line, 'UTF-8');
				//$new_line -=14;
				$page->drawText("Gift Message: ", $new_width, $new_line, 'UTF-8');
				$new_line -=14;
				$start = 0;
				//word wrap logic for gift message starts here
				$words_key = str_word_count($giftMessageNote,2);
				$tmp_key = 35;
				$check_limit = 15;
				$start = 0;
				$lastLine = false;
				$line_chars = 45;
				if(strlen($giftMessageNote) <= $line_chars){ // If the gift message has < one line
					$page->drawText(substr($giftMessageNote,$start,$line_chars), $new_width, $new_line, 'UTF-8');	
					$new_line -=14;
				}else{										// If the gift message has > one line
					for($loop=0;$loop<strlen($giftMessageNote);$loop=($loop+1)){
						$tmp_value = $words_key[$tmp_key];
						if(!$lastLine){
							if(empty($tmp_value)){
								for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
									$tmp_value = $words_key[$i];
									if(!empty($tmp_value)){
										$end_word_key = $i; // previous line last word
										break;
									}
								}
								for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
									$tmp_value = $words_key[$j];
									if(!empty($tmp_value)){
										$first_word_key = $j; // next line first word
										break;
									}
								}
							}else{
								$end_word_key = $tmp_key; // previous line last word
								for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
									$tmp_value = $words_key[$j];
									if(!empty($tmp_value)){
										$first_word_key = $j; // next line first word
										break;
									}
								}
							}
						}
						$current_line_chars = $first_word_key - $start;	
						//echo substr($giftMessageNote,$start,$current_line_chars);
						$page->drawText(substr($giftMessageNote,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');											
						$start = $first_word_key ;
						if(($end_word_key+40) > strlen($giftMessageNote)){
							$lastLine = true;
							$tmp_key = end(array_keys($words_key))-1;
							$first_word_key = strlen($giftMessageNote);
						}else{
							$tmp_key = $end_word_key+40;
						}
						$new_line -=14;
					}
				}										
				//word wrap logic gift message ends here
			
			//picklist
			}	
			if(!$pickpack && !$pickship && !$giftprint){
				
			foreach($orderItems as $sItem) {
			if($sItem->getProductType() == "simple"){
					$pdt_data = Mage::getModel("catalog/product")->load($sItem->getProductId());
                    $pdt_recipe = $pdt_data->getRecipe();
						}
					} 
					
	        $recipe_line = "Recipe: ".$pdt_recipe;                  
			$new_width = 30;
			$new_line = 130;               
			
		   //$page->drawText($recipe_line, $new_width-300, $new_line+15, 'UTF-8');
			for($lp=0;$lp<strlen($recipe_line);$lp=($lp+45)){
			   $page->drawText(substr($recipe_line,$lp,45), $new_width, $new_line, 'UTF-8');
			   if(($lp+45)<strlen($recipe_line)) $new_line -=14;                                    
					}
				
				//picklist
				}
				if((!$pickpack && $pickship && !$giftprint) || (!$pickpack && !$pickship && !$giftprint)) {	
			
                $page->rotate(20, 140, M_PI/2);
                if($ups!== false){
					 $f_cont = file_get_contents($img_path . $collection->getLabelname());
                            $img = imagecreatefromstring($f_cont);
                            if (Mage::getStoreConfig('upslabel/printing/verticalprint') == 1) {
                                $FullImage_width = imagesx($img);
                                $FullImage_height = imagesy($img);
                                $full_id = imagecreatetruecolor($FullImage_width, $FullImage_height);
                                $col = imagecolorallocate($img, 125, 174, 240);
                                $IMGfuul = imagerotate($img, -90, $col);
                            } else {
                                $IMGfuul = $img;
                            }
                            $rnd = rand(10000, 999999);
                            imagejpeg($IMGfuul, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                            $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
							$font99 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
							$page->setFont($font99, 11);
                            $page->drawImage($image,  80, -540, 400, 140);
                            $page->drawText('Contains Cut Flowers, Product of Colombia and Thailand', 100, -380);
				
				   }
				 if($fedex!== false||$fde!== false){
					$fedex_img_path = Mage::getBaseDir('media') . '/fedexlabel/label/';
                    $image = Zend_Pdf_Image::imageWithPath($fedex_img_path . $collection->getLabelname());
					$font99 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					$page->setFont($font99, 11);
                    $page->drawImage($image, 60, -360, 300, 120);
                    $page->drawText('Contains Cut Flowers, Product of Colombia and Thailand', 40, -380);	
				   }
				   
				// Picklist  
				}
				unlink($img_path . 'lbl' . $rnd . '.jpeg');
					$i++;
                        }
					//}
				//}
                    }
                    unset($IMGfuul);
                
                    
                }
                    
        if(!($targetorder || $amazonorder)){
						
				
				foreach ($colls AS $k => $v) 
			{	$coll = $v['upslabel_id'];
                $collection = Mage::getModel('upslabel/upslabel')->load($coll);
				
				if (($collection->getOrderId() == $order_id && $ptype == "order") || ($collection->getShipmentId() == $order_id && $ptype != "order")) {
     			$websiteId = Mage::getModel('core/store')->load($order->getStoreId())->getWebsiteId(); 
			//print_r($orderItems->getData());
			
		foreach ($orderItems as $sItem){ 
		if($sItem->getProductType()=="simple"){ 
		
			$prodqtys = round($sItem->getData('qty_ordered'));		
			$prodqtys = count($prodqtys);
			//echo "this si x".$x;
			//echo "<br />";
			//echo "this si prod qty".$prodqtys;
			//echo "<br />";
			for($x = 0; $x < $prodqtys; $x++) {	
				$x++;
                $page = $pdf->newPage($holstSize);
                $pdf->pages[] = $page;
				
				//picklist
				if(($pickpack && !$pickship && !$giftprint) || (!$pickpack && !$pickship && !$giftprint)) {
                if($websiteId==3){
                $image = Zend_Pdf_Image::imageWithPath($img_path.'/bloom-logo.png' );
                }else{
					if($jetorder){
						$jetimage = Zend_Pdf_Image::imageWithPath($img_path.'/jet.png' );
						}						
					$image = Zend_Pdf_Image::imageWithPath($img_path.'/kabloom-logo.png' );
					}
                $feds++;
                ///$rnd = rand(10000, 999999);
				$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
                $page->setFont($font, 11);
                
                //top part heading text
                $page->setFillColor(Zend_Pdf_Color_Html::color('#E3E3E3'));
				$page->drawRectangle( 0,900,620, 1020, $fillType = Zend_Pdf_Page::SHAPE_DRAW_FILL);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
				$orderDate = date('F d, Y', strtotime($order->getCreatedAt()));
				$page->setFont($font, 11);
				//$page->drawText("Thankyou for Your purchase", 240, 995, 'UTF-8');
				//$page->drawText("if you ordered additional items they will arrive seperatley ", 180, 982, 'UTF-8');
				
				// target Logo and barcode 
				if($jetorder){
				$page->drawImage($jetimage,10,930,160,1000);
				$page->drawImage($image,180,930,300,1000);
				}else{
					$page->drawImage($image,10,930,160,1000);
					}				
				$newwidth = 360;
				$newheight = 980;
				if($websiteId==3){
					 $page->drawText("Customer Service", $newwidth, $newheight, 'UTF-8'); 
				$newheight -=14;
				$page->drawText("Website  : http://bloomkonnectexpress.com/", $newwidth, $newheight, 'UTF-8'); 
				$newheight -=14;
				$page->drawText("Email  : customerservice@kabloomcorp.com", $newwidth, $newheight, 'UTF-8');
				$newheight -=14;				
				$page->drawText("Phone  :  (877)893-9984", $newwidth, $newheight, 'UTF-8');
                
                }else{	
				$page->drawText("Customer Service", $newwidth, $newheight, 'UTF-8'); 
				$newheight -=14;
				$page->drawText("Website  : https://www.kabloom.com", $newwidth, $newheight, 'UTF-8'); 
				$newheight -=14;
				$page->drawText("Email  : customerservice@kabloomcorp.com", $newwidth, $newheight, 'UTF-8');			
				$newheight -=14;				
				$page->drawText("Phone  : (800)522-5666", $newwidth, $newheight, 'UTF-8'); 
				}
				$newheight -=14;
				
				// From, Bill to and shipping address
				
				$page->setFillColor(Zend_Pdf_Color_Html::color('#555555'));
				$page->drawRectangle( 02,860,610, 880, $fillType = Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#EEEEEE'));
				$page->drawRectangle( 02,780,610, 860, $fillType = Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#FFFFFF'));	 
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font5, 11);
				$line = 865;
				$newwidth = 30;
				$page->drawText("Billing Address :", $newwidth, $line, 'UTF-8');					
				$line -=24;
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));	
				//$page->setFont($font5, 11);
				
			  $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			  $page->setFont($font5, 11);
			  $page->drawText($bcustName, 30, $line, 'UTF-8');
			  $line -=14;
			  $page->drawText($bcustAddr, 30, $line, 'UTF-8');
			  $line -=14;
			  $page->drawText($bregion.', '.$bcountry.' - '.$bzipcode, 30, $line, 'UTF-8');
			  $line -=30;
				$line = 865;
				$newwidth = 340;
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font5, 11);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#FFFFFF'));
				$page->drawText("Shipping Address : ", $newwidth, $line, 'UTF-8');
				$line -=24;
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFont($font5, 11);
				$page->drawText($custName, $newwidth, $line, 'UTF-8');
				$line -=14;
				$page->drawText($custAddr, $newwidth, $line, 'UTF-8');
				$line -=14;
				$page->drawText($region.', '.$country.' - '.$zipcode, $newwidth, $line, 'UTF-8');
				
				
				//item qty desc starts here
				  $new_width = 20;		
				  $new_line = 740;
				  $font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				  $page->setFont($font, 10);
				$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			  $page->setLineWidth(0.5)->drawLine(0,$new_line-4,610,$new_line-4);
			  $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
     		  $page->setFont($font5, 10);			  
			  $page->drawText("Your Order of  ".$orderDate."  (". $order->getIncrementId().")", $new_width,$new_line, 'UTF-8');
			  $new_line -=14;		   
			  $page->setLineWidth(0.5)->drawLine(0,$new_line-6,610,$new_line-6);
			  $page->setFont($font, 8);
			  $page->drawText("Qty", $new_width,$new_line, 'UTF-8');
			  $page->drawText("Item", $new_width+25,$new_line, 'UTF-8');
			  $page->drawText("Item Price", $new_width+360,$new_line, 'UTF-8');
			  $page->drawText("Total", $new_width+460,$new_line, 'UTF-8');
			  $new_line -=14;
			  //foreach($orderItems as $sItem) {
			  if($sItem->getProductType() == "simple"){
			  $_Pdetails = Mage::getSingleton('catalog/product')->loadByAttribute('sku',$sItem->getSku());
			  $pdt_name = $_Pdetails->getName();//$sItem->getName();               
			  $page->drawText('IN THIS SHIPMENT', $new_width+25, $new_line, 'UTF-8');    
			  for($lp=0;$lp<strlen($pdt_name);$lp=($lp+58)){
			  $page->drawText(substr($pdt_name,$lp,58), $new_width+25, $new_line-14, 'UTF-8');
			  if(($lp+58)<strlen($pdt_name)) $new_line -=10;
			  } 
		     $page->drawText(intval($sItem->getData('qty_ordered')), $new_width, $new_line, 'UTF-8');
		     if(empty($giftMessageNote)){
				 $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFont($font5, 11);
			  $page->drawText("$".floatval($sItem->getPrice()), $new_width+360, $new_line-10, 'UTF-8');
			  $page->drawText("$".floatval($order->getGrandTotal()), $new_width+460, $new_line-10, 'UTF-8'); 									  
						}
					}
				//}
		   
				//$page->drawText('Amazon.com, LLC', $new_width+25, $new_line-35, 'UTF-8');
				$font9 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font9, 10);	
				$page->drawText($sItem->getSku(), $new_width+25, $new_line-45, 'UTF-8');
				$new_line -=14;	
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			    $page->setFont($font5, 11);
				$new_line =635; 
				$new_width = $new_width;
				$start = 0;
				//word wrap logic for gift message starts here
				//$words_key = str_word_count($giftMessageNote,2);
				//$tmp_key = 75;
				//$check_limit = 15;
				//$start = 0;
				//$lastLine = false;
				//$line_chars = 45;
				//if(strlen($giftMessageNote) <= $line_chars){ // If the gift message has < one line
					//$page->drawText(substr($giftMessageNote,$start,$line_chars), $new_width, $new_line, 'UTF-8');	
					//$new_line -=14;
				//}else{										// If the gift message has > one line
					//for($loop=0;$loop<strlen($giftMessageNote);$loop=($loop+1)){
						//$tmp_value = $words_key[$tmp_key];
						//if(!$lastLine){
							//if(empty($tmp_value)){
								//for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
									//$tmp_value = $words_key[$i];
									//if(!empty($tmp_value)){
										//$end_word_key = $i; // previous line last word
										//break;
									//}
								//}
								//for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
									//$tmp_value = $words_key[$j];
									//if(!empty($tmp_value)){
										//$first_word_key = $j; // next line first word
										//break;
									//}
								//}
							//}else{
								//$end_word_key = $tmp_key; // previous line last word
								//for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
									//$tmp_value = $words_key[$j];
									//if(!empty($tmp_value)){
										//$first_word_key = $j; // next line first word
										//break;
									//}
								//}
							//}
						//}
						//$current_line_chars = $first_word_key - $start;	
						////echo substr($giftMessageNote,$start,$current_line_chars);
						//$page->drawText(substr($giftMessageNote,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');											
						//$start = $first_word_key ;
						//if(($end_word_key+40) > strlen($giftMessageNote)){
							//$lastLine = true;
							//$tmp_key = end(array_keys($words_key))-1;
							//$first_word_key = strlen($giftMessageNote);
						//}else{
							//$tmp_key = $end_word_key+40;
						//}
						//$new_line -=14;
					//}
				//}										
				//word wrap logic gift message ends here
						  $new_line =625; 
						  $new_width = $new_width;
						  
						  
						$page->setLineWidth(0.9)->drawLine(0,$new_line-35,610	,$new_line-35);
						$new_line -=14;
						if($jetorder){
							
				$font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFont($font5, 8);
						$page->drawText("Return Policy: KaBloom strives for an utmost delightful customer experience. if you are not completely satisfied with your bouquet, please email or call us. We will be", 20, $new_line-38, 'UTF-8');
						$page->drawText("happy to reship a new bouquet to you, at our cost. Given the perishable nature of fresh flowers, we do not follow a return policy. Thank you for understanding.", 20, $new_line-46, 'UTF-8');	
						 }else{
													$page->drawText("This Shipment Completes Your Order.", 20, $new_line-42, 'UTF-8');
												}
				
				//our promise & refund paragraphs start here 
				$new_line -=14;	
				$new_line =710; 
				$new_width = 20;
				
				//picklist
				}
				if((!$pickpack && !$pickship && $giftprint) || (!$pickpack && !$pickship && !$giftprint)) {
				// reciepe & gift message boxes
				$page->setFillColor(Zend_Pdf_Color_Html::color('#FFFFFF'));
				$page->drawRectangle(20, 20, 300, 180);							
				$page->drawRectangle(320, 20, 580, 180);
				$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
				$page->setFont($font2, 11);
				$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
				$new_line = 140;
				$new_width = 340;
				//$giftMessage = Mage::getModel("giftmessage/message")->load($order->getGiftMessageId());		
				$giftMessageNote = $giftMessage->getMessage();
				$giftMessageSender = $giftMessage->getSender();
				$new_line -=14;
				//$page->drawText("From: ".$giftMessageSender, $new_width, $new_line, 'UTF-8');
				//$new_line -=14;
				$page->drawText("Gift Message: ", $new_width, $new_line, 'UTF-8');
				$new_line -=14;
				$start = 0;
				$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				$page->setFont($font2, 11);
				//word wrap logic for gift message starts here
				$words_key = str_word_count($giftMessageNote,2);
				$tmp_key = 35;
				$check_limit = 15;
				$start = 0;
				$lastLine = false;
				$line_chars = 45;
				if(strlen($giftMessageNote) <= $line_chars){ // If the gift message has < one line
					$page->drawText(substr($giftMessageNote,$start,$line_chars), $new_width, $new_line, 'UTF-8');	
					$new_line -=14;
				}else{										// If the gift message has > one line
					for($loop=0;$loop<strlen($giftMessageNote);$loop=($loop+1)){
						$tmp_value = $words_key[$tmp_key];
						if(!$lastLine){
							if(empty($tmp_value)){
								for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
									$tmp_value = $words_key[$i];
									if(!empty($tmp_value)){
										$end_word_key = $i; // previous line last word
										break;
									}
								}
								for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
									$tmp_value = $words_key[$j];
									if(!empty($tmp_value)){
										$first_word_key = $j; // next line first word
										break;
									}
								}
							}else{
								$end_word_key = $tmp_key; // previous line last word
								for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
									$tmp_value = $words_key[$j];
									if(!empty($tmp_value)){
										$first_word_key = $j; // next line first word
										break;
									}
								}
							}
						}
						$current_line_chars = $first_word_key - $start;	
						//echo substr($giftMessageNote,$start,$current_line_chars);
						$page->drawText(substr($giftMessageNote,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');											
						$start = $first_word_key ;
						if(($end_word_key+40) > strlen($giftMessageNote)){
							$lastLine = true;
							$tmp_key = end(array_keys($words_key))-1;
							$first_word_key = strlen($giftMessageNote);
						}else{
							$tmp_key = $end_word_key+40;
						}
						$new_line -=14;
					}
				}										
				//word wrap logic gift message ends here
			
			//picklist
			}	
			if(!$pickpack && !$pickship && !$giftprint){
				
			//foreach($orderItems as $sItem) {
			if($sItem->getProductType() == "simple"){
					$pdt_data = Mage::getModel("catalog/product")->load($sItem->getProductId());
                    $pdt_recipe = $pdt_data->getRecipe();
						}
					//} 
					
	        $recipe_line = "Recipe: ".$pdt_recipe;                  
			$new_width = 30;
			$new_line = 130;               
			
		   //$page->drawText($recipe_line, $new_width-300, $new_line+15, 'UTF-8');
			for($lp=0;$lp<strlen($recipe_line);$lp=($lp+45)){
			   $page->drawText(substr($recipe_line,$lp,45), $new_width, $new_line, 'UTF-8');
			   if(($lp+45)<strlen($recipe_line)) $new_line -=14;                                    
					}
			
			//picklist
			}
			if((!$pickpack && $pickship && !$giftprint) || (!$pickpack && !$pickship && !$giftprint)) {
				
                $page->rotate(20, 140, M_PI/2);
                if($ups!== false){
					 $f_cont = file_get_contents($img_path . $collection->getLabelname());
                            $img = imagecreatefromstring($f_cont);
                            if (Mage::getStoreConfig('upslabel/printing/verticalprint') == 1) {
                                $FullImage_width = imagesx($img);
                                $FullImage_height = imagesy($img);
                                $full_id = imagecreatetruecolor($FullImage_width, $FullImage_height);
                                $col = imagecolorallocate($img, 125, 174, 240);
                                $IMGfuul = imagerotate($img, -90, $col);
                            } else {
                                $IMGfuul = $img;
                            }
                            $rnd = rand(10000, 999999);
                            imagejpeg($IMGfuul, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                            $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
							$font99 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
							$page->setFont($font99, 11);
                            $page->drawImage($image,  80, -540, 400, 140);
                            $page->drawText('Contains Cut Flowers, Product of Colombia and Thailand', 100, -380);
				
				   }
				 if($fedex!== false||$fde!== false){
					$fedex_img_path = Mage::getBaseDir('media') . '/fedexlabel/label/';
                    $image = Zend_Pdf_Image::imageWithPath($fedex_img_path . $collection->getLabelname());
					$font99 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					$page->setFont($font99, 11);
                    $page->drawImage($image, 60, -360, 300, 120);
                    $page->drawText('Contains Cut Flowers, Product of Colombia and Thailand', 40, -380);	
				   }
				   
			//picklist	   
			}
				unlink($img_path . 'lbl' . $rnd . '.jpeg');
					$i++;
                        }
					}
				}
			}
                    }
                    
                    unset($IMGfuul);
               // die;
                    
                }
                                        
          //Generating Invoice starts here 
                $order=Mage::getModel('sales/order')->load($order_id);  
                $pickup_method = $order->getPickupMethod();               
               // if ((($order->getStatus() == Mage_Sales_Model_Order::STATE_NEW) || ($order->getStatus() == Mage_Sales_Model_Order::STATE_PROCESSING)) && ($pickup_method != "Local Delivery")) {
                        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                        $invoice->register();

                        $invoice->getOrder()->setCustomerNoteNotify(false);          
                        $invoice->getOrder()->setIsInProcess(true);
                        $order->addStatusHistoryComment('Automatically INVOICED by Kabloom Development Team.', false);

                        $transactionSave = Mage::getModel('core/resource_transaction')
                                ->addObject($invoice)
                                ->addObject($invoice->getOrder());

                        $transactionSave->save();
               // }
          //Generating Invoice ends here
		 	
     
	}
        
		if($pickpack || $pickship || $giftprint) {
			 $pdfData = $pdf->render();
			 //$pdf->save("test.pdf");
			 //$redirectUrl = 'http://52.24.36.137/ex.php?pdf=test';
			 //Mage::app()->getResponse()->setRedirect($redirectUrl);
			 //return true;
			 
			 
			 
			 $pdf1->AutoPrint(true);
			 $pdf1->Output();
			 die;
		} else{
			//$pdf->save();
			if ($i > 0) {	
				//echo 'fedex pdf count :'.$feds;
				//echo ' inside if $i value:'.$i.' $j value:'.$j;die;
				$pdfData = $pdf->render();
				header("Content-Disposition: inline; filename=result.pdf");
				header("Content-type: application/x-pdf");
				die($pdfData);
				//echo $pdfData;
				//return true;
			} else {
				//echo 'inside else $i value:'.$i.' $j value:'.$j;die;
				if ($j > 0) {		
					$pdfData = $pdf->render();
					header("Content-Disposition: inline; filename=result.pdf");
					header("Content-type: application/x-pdf");
					die($pdfData);
				}
				return false;
			
		
						}
		}

        
				}

    static public function createFromLists($order_ids)
    {
		
        $img_path = Mage::getBaseDir('media') . '/upslabel/label/';
        $pdf = new Zend_Pdf();
        $i = 0;
        //$pdf->pages = array_reverse($pdf->pages);
        if (!is_array($order_ids)) {
            $order_ids = explode(',', $order_ids);
        }
        $width = strlen(Mage::getStoreConfig('upslabel/printing/dimensionx')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensionx') : 1400 / 2.6;
        $heigh = strlen(Mage::getStoreConfig('upslabel/printing/dimensiony')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensiony') : 800 / 2.6;
        if (strlen(Mage::getStoreConfig('upslabel/printing/holstx')) > 0 && strlen(Mage::getStoreConfig('upslabel/printing/holsty')) > 0) {
            $holstSize = Mage::getStoreConfig('upslabel/printing/holstx') . ':' . Mage::getStoreConfig('upslabel/printing/holsty') . ':';
        } else {
            $holstSize = Zend_Pdf_Page::SIZE_A4;
        }
        foreach ($order_ids as $order_id) {
            $collection = Mage::getModel('upslabel/upslabel')->load($order_id);
            if ($collection && $collection->getStatus() == 0) {
                if (file_exists($img_path . $collection->getLabelname()) && filesize($img_path . $collection->getLabelname()) > 1024) {
                    $page = $pdf->newPage($holstSize);
                    $pdf->pages[] = $page;
                    $f_cont = file_get_contents($img_path . $collection->getLabelname());
                    $img = imagecreatefromstring($f_cont);
                    if (Mage::getStoreConfig('upslabel/printing/verticalprint') == 1) {
                        $FullImage_width = imagesx($img);
                        $FullImage_height = imagesy($img);
                        $full_id = imagecreatetruecolor($FullImage_width, $FullImage_height);
                        $col = imagecolorallocate($img, 125, 174, 240);
                        $IMGfuul = imagerotate($img, -90, $col);
                    } else {
                        $IMGfuul = $img;
                    }
                    $rnd = rand(10000, 999999);
                    imagejpeg($IMGfuul, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                    $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
                    $page->drawImage($image, 0, 0, $width, $heigh);
                    unlink($img_path . 'lbl' . $rnd . '.jpeg');
                    $i++;
                }
                unset($IMGfuul);
            }
        }
        //$pdf->save();
        if ($i > 0) {
            $pdfData = $pdf->render();
            header("Content-Disposition: inline; filename=result.pdf");
            header("Content-type: application/x-pdf");
            echo $pdfData;
            return true;
        } else {
            return false;
        }
    }
   public function addShipperAction($shipper_address){
		$shipper = array(
			'Contact' => array(
				'PersonName' => $shipper_address['personname'],
				'CompanyName' => $shipper_address['companyname'],
				'PhoneNumber' => $shipper_address['phonenumber']
			),
			'Address' => array(
				'StreetLines' => array($shipper_address['streetlines']),
				'City' => $shipper_address['city'],
				'StateOrProvinceCode' => $shipper_address['stateorprovincecode'],
				'PostalCode' => $shipper_address['postalcode'],
				'CountryCode' => $shipper_address['countrycode']
			)
		);
		return $shipper;
	}
	public function addRecipientAction($recipient_address){
		$recipient = array(
			'Contact' => array(
				'PersonName' => $recipient_address['personname'],
				//'CompanyName' => 'Residential',
				'PhoneNumber' => $recipient_address['phonenumber']
			),
			'Address' => array(
				'StreetLines' => array($recipient_address['streetlines'][0],$recipient_address['streetlines'][1]),
				'City' => $recipient_address['city'],
				'StateOrProvinceCode' => $recipient_address['stateorprovincecode'],
				'PostalCode' => $recipient_address['postalcode'],
				'CountryCode' => $recipient_address['countrycode'],
				'Residential' => true
			)
		);
		return $recipient;	                                    
	}
	public function addShippingChargesPaymentAction($billaccount){
		$shippingChargesPayment = array(
			'PaymentType' => 'THIRD_PARTY',
			'Payor' => array(
				'ResponsibleParty' => array(
					'AccountNumber' => $billaccount,
					'Contact' => null,
					'Address' => array(
						'CountryCode' => 'US'
					)
				)
			)
		);
		return $shippingChargesPayment;
	}
	public function addLabelSpecificationAction(){
		$labelSpecification = array(
			'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
			'ImageType' => 'PNG',  // valid values DPL, EPL2, PDF, ZPLII and PNG
			'LabelStockType' => 'PAPER_4X6' // default value is PAPER_7X4.75
		);
		return $labelSpecification;
	}
	public function addSpecialServicesAction(){
		$specialServices = array(
			'SpecialServiceTypes' => array('COD'),
			'CodDetail' => array(
				'CodCollectionAmount' => array(
					'Currency' => 'USD', 
					'Amount' => 150
				),
				'CollectionType' => 'ANY' // ANY, GUARANTEED_FUNDS
			)
		);
		return $specialServices; 
	}
	public function addPackageLineItem1Action($servicetype){
		$packageLineItem = array(
			'SequenceNumber'=>1,
			'GroupPackageCount'=>1,
			'Weight' => array(
				'Value' => 3.0,
				'Units' => 'LB'
			),
			'Dimensions' => array(
				'Length' => 28,
				'Width' => 8,
				'Height' => 8,
				'Units' => 'IN'
			),
			//'SpecialServicesRequested' => $this->addSpecialServicesAction()
			/*'CustomerReferences' => array(
				'0' => array(
					'CustomerReferenceType' => 'CUSTOMER_REFERENCE', // valid values CUSTOMER_REFERENCE, INVOICE_NUMBER, P_O_NUMBER and SHIPMENT_INTEGRITY
					'Value' => 'GR4567892'
				), 
				'1' => array(
					'CustomerReferenceType' => 'INVOICE_NUMBER', 
					'Value' => 'INV4567892'
				),
				'2' => array(
					'CustomerReferenceType' => 'P_O_NUMBER', 
					'Value' => 'PO4567892'
				)
			),*/
			
		);
		if($servicetype == 'FEDEX_GROUND'){
			$packageLineItem['SpecialServicesRequested'] = $this->addSpecialServicesAction();
		}
		return $packageLineItem;
	}
}

