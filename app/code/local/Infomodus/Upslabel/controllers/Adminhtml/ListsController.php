<?php
/*
 * Author Rudyuk Vitalij Anatolievich
 * Email rvansp@gmail.com
 * Blog www.cervic.info
 */
?>
<?php

class Infomodus_Upslabel_Adminhtml_ListsController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('upslabel/lists')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));

        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $AccessLicenseNumber = Mage::getStoreConfig('upslabel/credentials/accesslicensenumber');
                $UserId = Mage::getStoreConfig('upslabel/credentials/userid');
                $Password = Mage::getStoreConfig('upslabel/credentials/password');
                $shipperNumber = Mage::getStoreConfig('upslabel/credentials/shippernumber');

                $id = $this->getRequest()->getParam('id');


                $lbl = Mage::getModel('upslabel/ups');

                $lbl->setCredentials($AccessLicenseNumber, $UserId, $Password, $shipperNumber);
                $lbl->packagingReferenceNumberCode = Mage::getStoreConfig('upslabel/packaging/packagingreferencenumbercode');

                $collection = Mage::getModel('upslabel/upslabel')->load($id);
                $lbl->deleteLabel($collection->getShipmentidentificationnumber());
                @unlink(Mage::getBaseDir('media') . '/upslabel/label/' . $collection->getLabelname());
                @unlink(Mage::getBaseDir('media') . '/upslabel/label/' . $collection->getTrackingnumber() . '.html');
                @unlink(Mage::getBaseDir('media') . '/upslabel/label/' . "HVR" . $collection->getTrackingnumber() . ".html");

                $collection->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $upslabelIds = $this->getRequest()->getParam('upslabel');
        if (!is_array($upslabelIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                $AccessLicenseNumber = Mage::getStoreConfig('upslabel/credentials/accesslicensenumber');
                $UserId = Mage::getStoreConfig('upslabel/credentials/userid');
                $Password = Mage::getStoreConfig('upslabel/credentials/password');
                $shipperNumber = Mage::getStoreConfig('upslabel/credentials/shippernumber');

                $lbl = Mage::getModel('upslabel/ups');

                $lbl->setCredentials($AccessLicenseNumber, $UserId, $Password, $shipperNumber);
                foreach ($upslabelIds as $upslabelId) {
                    $upslabel = Mage::getModel('upslabel/upslabel')->load($upslabelId);

                    $lbl->deleteLabel($upslabel->getShipmentidentificationnumber());
                    @unlink(Mage::getBaseDir('media') . '/upslabel/label/' . $upslabel->getLabelname());
                    @unlink(Mage::getBaseDir('media') . '/upslabel/label/' . $upslabel->getTrackingnumber() . '.html');
                    @unlink(Mage::getBaseDir('media') . '/upslabel/label/' . "HVR" . $upslabel->getTrackingnumber() . ".html");

                    $upslabel->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($upslabelIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName = 'ups_labels.csv';
        $content = $this->getLayout()->createBlock('upslabel/adminhtml_lists_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'ups_labels.xml';
        $content = $this->getLayout()->createBlock('upslabel/adminhtml_lists_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK', '');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}