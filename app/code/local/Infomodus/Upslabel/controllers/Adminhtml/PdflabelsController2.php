<?php

/*
 * Author Rudyuk Vitalij Anatolievich
 * Email rvansp@gmail.com
 * Blog www.cervic.info
 */

class Infomodus_Upslabel_Adminhtml_PdflabelsController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
		
        $ptype = $this->getRequest()->getParam('type');
		
        if ($ptype != 'lists') {
            $type = 'shipment';
            $order_ids = $this->getRequest()->getParam($ptype . '_ids');
            if ($ptype == 'creditmemo') {
                $ptype = 'shipment';
                $type = 'refund';
            }
		
            $resp = $this->create($order_ids, $type, $ptype);
        } else {
            $order_ids = $this->getRequest()->getParam('upslabel');
            $resp = $this->createFromLists($order_ids);
        }

        if (!$resp) {
            $this->_redirectReferer();
        }
    }

    public function onepdfAction()
    {
			
        $order_id = $this->getRequest()->getParam('order_id');
        $shipment_id = $this->getRequest()->getParam('shipment_id');
        $type = $this->getRequest()->getParam('type');
        $img_path = Mage::getBaseDir('media') . '/upslabel/label/';
        $url_image_path = Mage::getBaseUrl('media') . 'upslabel/label/';
        $pdf = new Zend_Pdf();
        $i = 0;
        $collections = Mage::getModel('upslabel/upslabel');
        $colls = $collections->getCollection()->addFieldToFilter('order_id', $order_id)->addFieldToFilter('shipment_id', $shipment_id)->addFieldToFilter('type', $type)->addFieldToFilter('status', 0);
        foreach ($colls AS $k => $v) {
            $coll = $v['upslabel_id'];
            break;
        }
        $width = strlen(Mage::getStoreConfig('upslabel/printing/dimensionx')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensionx') : 1400 / 2.6;
        $heigh = strlen(Mage::getStoreConfig('upslabel/printing/dimensiony')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensiony') : 800 / 2.6;
        if (strlen(Mage::getStoreConfig('upslabel/printing/holstx')) > 0 && strlen(Mage::getStoreConfig('upslabel/printing/holsty')) > 0) {
            $holstSize = Mage::getStoreConfig('upslabel/printing/holstx') . ':' . Mage::getStoreConfig('upslabel/printing/holsty') . ':';
        } else {
            $holstSize = Zend_Pdf_Page::SIZE_A4;
        }
        $collection_one = Mage::getModel('upslabel/upslabel')->load($coll);
        if ($collection_one->getOrderId() == $order_id) {
            foreach ($colls AS $collection) {
                if (file_exists($img_path . $collection->getLabelname()) && filesize($img_path . $collection->getLabelname()) > 1024) {
                    $page = $pdf->newPage($holstSize);
                    $pdf->pages[] = $page;
                    $f_cont = file_get_contents($img_path . $collection->getLabelname());
                    $img = imagecreatefromstring($f_cont);
                    if (Mage::getStoreConfig('upslabel/printing/verticalprint') == 1) {
                        $FullImage_width = imagesx($img);
                        $FullImage_height = imagesy($img);
                        $full_id = imagecreatetruecolor($FullImage_width, $FullImage_height);
                        $col = imagecolorallocate($img, 125, 174, 240);
                        $IMGfuul = imagerotate($img, -90, $col);
                    } else {
                        $IMGfuul = $img;
                    }
                    $rnd = rand(10000, 999999);
                    imagejpeg($IMGfuul, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                    $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
					$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					$page->setFont($font, 12);
					
					$order=Mage::getModel('sales/order')->load($order_id); 
					
					$orderItems = $order->getItemsCollection()
							->addAttributeToSelect('*')
							->load();
					$page->drawImage($image, 20, 350, $width+20, $heigh+350); //draw shipping label ups
					//shipping adddress
					$line = 340;
					$address = $order->getShippingAddress();
					$custName = $address->getName();
					$custAddr = $address->getStreetFull();
					$region = $address->getRegion();
					$country = $address->getCountry();
					$zipcode = $address->getPostcode();
					$baddress = $order->getBillingAddress();
					$bcustName = $baddress->getName();
					$bcustAddr = $baddress->getStreetFull();
					$bregion = $baddress->getRegion();
					$bcountry = $baddress->getCountry();
					$bzipcode = $baddress->getPostcode();
					$page->drawText("Ship to: ", 20, $line, 'UTF-8');
					$page->drawText("Bill to: ", 120, $line, 'UTF-8');
					$line -=14;
					$page->drawText($custName, 20, $line, 'UTF-8');
					$page->drawText($bcustName, 120, $line, 'UTF-8');
					$line -=14;
					$page->drawText($custAddr, 20, $line, 'UTF-8');
					$page->drawText($bcustAddr, 120, $line, 'UTF-8');
					$line -=14;
					$page->drawText($region, 20, $line, 'UTF-8');
					$page->drawText($bregion, 120, $line, 'UTF-8');
					$line -=14;
					$page->drawText($country, 20, $line, 'UTF-8');
					$page->drawText($bcountry, 120, $line, 'UTF-8');
					$line -=14;
					$page->drawText($zipcode, 20, $line, 'UTF-8');
					$page->drawText($bzipcode, 120, $line, 'UTF-8');
					$line -=30;
                                                                                                      
					$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
					$page->setFont($font2, 12);
					$page->drawText("Order #:".$order->getIncrementId(), 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText("Product Information:", 20, $line, 'UTF-8');
					$line -=14;
					$page->setFont($font, 10);
					foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
					$page->drawText("Name: ".$sItem->getName(), 20, $line, 'UTF-8');
					$page->drawText("Ordered quantity: ".$sItem->getData('qty_ordered'), 20, $line-14, 'UTF-8');
					$line -=28;
					}
					}

//$message = Mage::getModel('giftmessage/message');

/* Add Gift Message*/ 
/*$gift_message_id = $order->getOrder()->getGiftMessageId();
if(!is_null($gift_message_id)) {
$message->load((int)$gift_message_id);
$gift_sender = $message->getData('sender');
$gift_recipient = $message->getData('recipient');
$gift_message = $message->getData('message');
$page->drawText("Gift Message: ".$gift_message, 20, $line-14, 'UTF-8');
}*/
               unlink($img_path . 'lbl' . $rnd . '.jpeg');
                    $i++;
                }
            }
        }
        if ($i > 0) {
            $pdfData = $pdf->render();
            header("Content-Disposition: inline; filename=result.pdf");
            header("Content-type: application/x-pdf");
            echo $pdfData;
        }
    }

   static public function create($order_ids, $type, $ptype)
    {
        
        $img_path = Mage::getBaseDir('media') . '/upslabel/label/';
        $pdf = new Zend_Pdf();
        $i = 0;
		$j= 0;
        //$pdf->pages = array_reverse($pdf->pages);
        if (!is_array($order_ids)) {
            $order_ids = explode(',', $order_ids);
        }
        $width = strlen(Mage::getStoreConfig('upslabel/printing/dimensionx')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensionx') : 1400 / 2.6;
        $heigh = strlen(Mage::getStoreConfig('upslabel/printing/dimensiony')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensiony') : 800 / 2.6;
        if (strlen(Mage::getStoreConfig('upslabel/printing/holstx')) > 0 && strlen(Mage::getStoreConfig('upslabel/printing/holsty')) > 0) {
            $holstSize = Mage::getStoreConfig('upslabel/printing/holstx') . ':' . Mage::getStoreConfig('upslabel/printing/holsty') . ':';
        } else {
            $holstSize = Zend_Pdf_Page::SIZE_A4;
        }
		
        foreach ($order_ids as $order_id) {
            $collections = Mage::getModel('upslabel/upslabel');
            $colls = $collections->getCollection()->addFieldToFilter($ptype . '_id', $order_id)->addFieldToFilter('type', $type)->addFieldToFilter('status', 0);
   
          /* amazon order pdf call */
          $amazonorders = Mage::getModel('sales/order')->load($order_id);
          $amazonorder = $amazonorders->getTradingPartner()=='amazon';

                          $is_s_day = 0;								
                          $orderItem = Mage::getResourceModel('sales/order_item_collection')->addFieldToFilter('order_id', $order_id)->getFirstItem()->getProductOptions();
                           foreach ($orderItem['options'] as $fdate){
                                   if ($fdate['label'] === 'Delivery Date') {
                                                  if (array_key_exists('value', $fdate)) {
                                                          $dDate = date('w',strtotime($orderItemOption['value']));
                                                          if($dDate==6)
                                                                  $is_s_day = 1;
                                                  }
                                          }
                           }			//add by sourav
             
            //if ($colls) {
				//if(count($colls)>0){	
                $order=Mage::getModel('sales/order')->load($order_id);
                $pickup_method = $order->getPickupMethod(); // pickup method in the grid
                
                if(strripos($pickup_method, 'fedex')!== false){ // for fedex labels
                    // Fedex Label api call starts here
                    $shipping_address = $order->getShippingAddress(); 
			// Copyright 2009, FedEx Corporation. All rights reserved.
			// Version 12.0.0
			//echo Mage::getModuleDir('etc', 'Mage_Usa').DS.'wsdl'.DS.'FedEx'.DS.'library';die;
			require_once(Mage::getModuleDir('etc', 'Mage_Usa').DS.'wsdl'.DS.'FedEx'.DS.'library'.DS.'fedex-common.php');

			//The WSDL is not included with the sample code.
			//Please include and reference in $path_to_wsdl variable.
			$path_to_wsdl = Mage::getModuleDir('etc', 'Mage_Usa').DS.'wsdl'.DS.'FedEx'.DS."ShipService_v15.wsdl";
			//echo 'key :'.getProperty('key');$this->cdtAction();die;
                        //header('Content-Type: image/png');
			define('SHIP_LABEL', 'shipgroundlabel.png');  // PDF label file. Change to file-extension .png for creating a PNG label (e.g. shiplabel.png)
			//define('SHIP_CODLABEL', 'CODgroundreturnlabel.pdf');  // PDF label file. Change to file-extension ..png for creating a PNG label (e.g. CODgroundreturnlabel.png)

			ini_set("soap.wsdl_cache_enabled", "0");

			$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
                        
                        
                        $warehouse_id = $order->getAssignationWarehouse();
                        $warehouse_id = isset($warehouse_id) ? $warehouse_id : 1;
                        if(count(explode(',',$warehouse_id))>1){
                          $warehouse_id = 1;  
                        }
                        $warehouseData = Mage::getModel('pointofsale/pointofsale')->load($warehouse_id);
                        $shipaccount = $warehouseData->getFedexaccount();
                        $billaccount = $warehouseData->getFedexaccount();
                        $meter = $warehouseData->getFedexmeter();
                        $key = $warehouseData->getFedexkey();
                        $password = $warehouseData->getFedexpassword();

			$request['WebAuthenticationDetail'] = array(
				'UserCredential' =>array(
					'Key' => $key,//getProperty('key'), 
					'Password' => $password,//getProperty('password')
				)
			);//echo 'request :';print_r($request);die;
			$request['ClientDetail'] = array(
				'AccountNumber' => $shipaccount,//getProperty('shipaccount'), 
				'MeterNumber' => $meter,//getProperty('meter')
			);
			$request['TransactionDetail'] = array('CustomerTransactionId' => '*** Ground Domestic Shipping Request using PHP ***');
			$request['Version'] = array(
				'ServiceId' => 'ship', 
				'Major' => '15', 
				'Intermediate' => '0', 
				'Minor' => '0'
			);
			$recipient_address = array();
			$shipper_address = array();
			
			$coreResource = Mage::getSingleton('core/resource');
			$connection = $coreResource->getConnection('core_read');
                        //echo '<pre>';print_r($shipping_address->getPostcode());die;
			$select = $connection->select()->from('citylist')->where('zipcode = ?', $shipping_address->getPostcode());
			$row = $connection->fetchAll($select);
			$region_id = $row[0]['region_id'];
			//echo '$region_id :'.$region_id;die;
			
			$recipient_phoneno = '1234567890';
			if(is_numeric($shipping_address->getTelephone()) && strlen($shipping_address->getTelephone()) > 7 ){
				 $recipient_phoneno = $shipping_address->getTelephone();
			}
			 
			$recipient_address['personname'] = $shipping_address->getFirstname().' '.$shipping_address->getLastname();
			$recipient_address['phonenumber'] = $recipient_phoneno;
			$recipient_address['streetlines'] = $shipping_address->getStreet();
			$recipient_address['city'] = $shipping_address->getCity();
			$recipient_address['stateorprovincecode'] = $region_id;
			$recipient_address['postalcode'] = $shipping_address->getPostcode();
			$recipient_address['countrycode'] = $shipping_address->getCountryId();
			
			$shipper_address['personname'] = 'Kabloom Admin';
			$shipper_address['companyname'] = 'Kabloom Flowers';
			$shipper_address['phonenumber'] = '8005225666';
			$shipper_address['streetlines'] = '305 Harvard Street';
			$shipper_address['city'] = 'Massachusetts';
			$shipper_address['stateorprovincecode'] = 'MA';
			$shipper_address['postalcode'] = '02446';
			$shipper_address['countrycode'] = 'US';
			
			// Determine whether the ship type is ground or express(air) starts
			
			// through order information you should fetch the fedex service, which is made static as of now
			$fedex_air_methods =  array('FedEx SameDay','FedEx SameDay City','FedEx First Overnight','FedEx Priority Overnight','FedEx Standard Overnight','FedEx 2Day A.M.','FedEx 2Day','FedEx Express Saver');
			$fedex_ground_methods = array('FedEx Ground','FedEx Home Delivery','FedEx SmartPost');
			$order_shipment_service = 'FedEx Home Delivery';
			//$is_fedex_air = in_array($order_shipment_service, $fedex_air_methods);
			$is_fedex_ground = strripos($pickup_method, 'gr');//in_array($order_shipment_service, $fedex_ground_methods);
			$servicetype = 'FEDEX_GROUND'; // by default 'FEDEX_GROUND'
//			if($is_fedex_air){
//				$servicetype = 'PRIORITY_OVERNIGHT';
//			}
			// if($is_fedex_ground !== false){ //if($is_fedex_ground){
				// $servicetype = 'FEDEX_GROUND';              // for 'FedEx Ground'
			// }elseif($pickup_method == 'FEDEX_NEXT_STD'){
				// $servicetype = 'STANDARD_OVERNIGHT';        // for 'FedEx Standard Overnight
			// }elseif($pickup_method == 'FEDEX_SECOND'){
				// $servicetype = 'FEDEX_2_DAY';               // for FedEx Two Day
			// }elseif($pickup_method == 'FEDEX_NEXT_PRI'){
				// $servicetype = 'PRIORITY_OVERNIGHT';        // for FedEx Priority Overnight'
			// }else{
				// $servicetype = 'PRIORITY_OVERNIGHT';        // for others    
			// }
			
			$coreResource = Mage::getSingleton('core/resource');
			$connection = $coreResource->getConnection('core_read');
			$select = $connection->select()->from('fedex_code_map')->where('fed_method_code = ?', $pickup_method);
			$service_codes = $connection->fetchAll($select);
			if(is_array($service_codes) && count($service_codes)>0){
				foreach($service_codes as $service_code){
					$servicetype = $service_code['fed_service_type'];
				}
			}
			
			// Determine whether the ship type is ground or express(air) ends
			
                        $pdflabelcontrollerObj = new Infomodus_Upslabel_Adminhtml_PdflabelsController(); 
                        
			$request['RequestedShipment'] = array(
				'ShipTimestamp' => date('c'),
				'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
				'ServiceType' => $servicetype, // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
				'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
				'Shipper' => $pdflabelcontrollerObj->addShipperAction($shipper_address),
				'Recipient' => $pdflabelcontrollerObj->addRecipientAction($recipient_address),
				'ShippingChargesPayment' => $pdflabelcontrollerObj->addShippingChargesPaymentAction($billaccount),
				'LabelSpecification' => $pdflabelcontrollerObj->addLabelSpecificationAction(), 
				/* Thermal Label */
				/*
				'LabelSpecification' => array(
					'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
					'ImageType' => 'EPL2', // valid values DPL, EPL2, PDF, ZPLII and PNG
					'LabelStockType' => 'STOCK_4X6.75_LEADING_DOC_TAB',
					'LabelPrintingOrientation' => 'TOP_EDGE_OF_TEXT_FIRST'
				),
				*/
				'PackageCount' => 1,
				'PackageDetail' => 'INDIVIDUAL_PACKAGES',                                        
				'RequestedPackageLineItems' => array(
					'0' => $pdflabelcontrollerObj->addPackageLineItem1Action($servicetype)
				)
			);
			   
			   //echo 'before try..';die;
                        try {

				if(setEndpoint('changeEndpoint')){
					$newLocation = $client->__setLocation(setEndpoint('endpoint'));
				}	
				$response = $client->processShipment($request); // FedEx web service invocation
				//echo '<pre>';print_r($response);die;
				$resource = Mage::getSingleton('core/resource');
				$writeConnection = $resource->getConnection('core_write');
				$query = "UPDATE upslabel SET statustext = '" . $response->HighestSeverity . "' WHERE order_id = " . $order_id;
				
				$res = $writeConnection->exec($query);
				if($res == 0){
                                    //$this->_redirectReferer();                                    
                                }
				//echo 'query :'.$query;echo 'count :'.$res;die;
				if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR'){
					//print_r($response);die;
					//printSuccess($client, $response);

					//$fp = fopen(SHIP_CODLABEL, 'wb');   
					//fwrite($fp, $response->CompletedShipmentDetail->CompletedPackageDetails->CodReturnDetail->Label->Parts->Image); //Create COD Return PNG or PDF file
					//fclose($fp);
					//echo '<a href="./'.SHIP_CODLABEL.'">'.SHIP_CODLABEL.'</a> was generated.'.Newline;
					
					// Create PNG or PDF label
					// Set LabelSpecification.ImageType to 'PNG' for generating a PNG label
				
					$fp = fopen(SHIP_LABEL, 'wb');   
					fwrite($fp, ($response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image));
					fclose($fp);
                                        
					/// creating a4 size paper starts
                                   
        
                    $page = $pdf->newPage($holstSize);
                    $pdf->pages[] = $page;
                    
                     $rnd = rand(10000, 999999);
                    //imagejpeg($IMGfuul, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                    $image = Zend_Pdf_Image::imageWithPath(SHIP_LABEL);
					$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					$page->setFont($font, 12);
					
					//$order=Mage::getModel('sales/order')->load($order_id); 
					
					$orderItems = $order->getItemsCollection()
							->addAttributeToSelect('*')
							->load();
					$page->drawImage($image, 20, 350, $width+20, $heigh+350); //draw shipping label fedex
                                         $page->setFont($font, 10);
                                         $page->drawText("Contains Cut Flowers, Product of Colombia and Thailand", 22, 345, 'UTF-8'); // static content on label
                                         $page->setFont($font, 12);
					//shipping adddress
					$line = 300;
                                        $new_width = 30;
					$new_line = 300;
                                        
                                        foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
											$pdt_data = Mage::getModel("catalog/product")->load($sItem->getProductId());
                                             $pdt_recipe = $pdt_data->getRecipe();
						}
					}
                                        
                                        $recipe_line = "Recipe: ".$pdt_recipe;
//                                        for($lp=0;$lp<strlen($recipe_line);$lp=($lp+45)){
//                                            $page->drawText(substr($recipe_line,$lp,45), $new_width, $new_line, 'UTF-8');
//                                            if(($lp+45)<strlen($recipe_line)) $new_line -=14;                                                                                       
//                                        }
                                        //word wrap logic for recipe starts here
                                        $words_key = str_word_count($recipe_line,2);
                                        $tmp_key = 35;
                                        $check_limit = 15;
                                        $start = 0;
                                        for($loop=0;$loop<strlen($recipe_line);$loop=($loop+1)){
                                                if(strlen($recipe_line)<($tmp_key-1)){
                                                        $first_word_key	= strlen($recipe_line);
                                                }
                                                $tmp_value = $words_key[$tmp_key];
                                                if(empty($tmp_value)){
                                                        for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
                                                                $tmp_value = $words_key[$i];
                                                                if(!empty($tmp_value)){
                                                                        $end_word_key = $i; // previous line last word
                                                                        break;
                                                                }
                                                        }
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }else{
                                                        $end_word_key = $tmp_key; // previous line last word
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }
												
                                                $current_line_chars = $first_word_key - $start;	
                                                $page->drawText(substr($recipe_line,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');
                                                $new_line -=14;
                                                $tmp_key = $end_word_key+40;
                                                $start = $first_word_key ;

                                        }
                                        //word wrap logic for recipe  ends here
                                        
					$address = $order->getShippingAddress();
					$custName = $address->getName();
					$custAddr = $address->getStreetFull();
					$region = $address->getRegion();
					$country = $address->getCountry();
					$zipcode = $address->getPostcode();
					$baddress = $order->getBillingAddress();
					$bcustName = $baddress->getName();
					$bcustAddr = $baddress->getStreetFull();
					$bregion = $baddress->getRegion();
					$bcountry = $baddress->getCountry();
					$bzipcode = $baddress->getPostcode();
					/*$page->drawText("Ship to: ", 20, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($custName, 20, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($custAddr, 20, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($region, 20, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($country, 20, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($zipcode, 20, $line, 'UTF-8');					
					$line -=20; 
					$page->drawText("Bill to: ", 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText($bcustName, 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText($bcustAddr, 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText($bregion, 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText($bcountry, 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText($bzipcode, 20, $line, 'UTF-8');
					$line -=14;*/
                                        
                                        $new_width = $width+60;		 
					$new_line = $heigh+330;
					$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);					
					$page->setFont($font2, 12);
					$page->drawText("Order #:".$order->getIncrementId(), $new_width, $new_line, 'UTF-8');
					$new_line -=14;
					$page->drawText("Product Information:", $new_width, $new_line, 'UTF-8');
					$new_line -=14;
					$page->setFont($font, 10);
					foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
					//$page->drawText("Name: ".$sItem->getName(), $new_width, $new_line, 'UTF-8');
                                        $pdt_name = "Name: ".$sItem->getName();
                                        for($lp=0;$lp<strlen($pdt_name);$lp=($lp+45)){
                                            $page->drawText(substr($pdt_name,$lp,45), $new_width, $new_line, 'UTF-8');
                                            if(($lp+45)<strlen($pdt_name)) $new_line -=14;                                                                                       
                                        }
					$page->drawText("Ordered quantity: ".$sItem->getData('qty_ordered'), $new_width, $new_line-14, 'UTF-8');
						}
					}
					
                                        $giftMessage = Mage::getModel("giftmessage/message")->load($order->getGiftMessageId());		
					$giftMessageNote = $giftMessage->getMessage();
                                        $giftMessageSender = $giftMessage->getSender();

					$new_line =284;
					$new_width = $width+88;	
                                        $page->drawText("From: ".$giftMessageSender, $new_width, $new_line, 'UTF-8');
					$new_line -=14;
                                        $page->drawText("Gift Message: ", $new_width, $new_line, 'UTF-8');
                                        $new_line -=14;
                                        $start = 0;
                                        
                                        
                                         //word wrap logic starts here
                                        $words_key = str_word_count($giftMessageNote,2);
                                        $tmp_key = 35;
                                        $check_limit = 15;
                                        $start = 0;
                                        for($loop=0;$loop<strlen($giftMessageNote);$loop=($loop+1)){
                                                //echo 'loop :'.$loop;
                                                $tmp_value = $words_key[$tmp_key];
                                                if(empty($tmp_value)){
                                                //echo 'inside if';die;
                                                        for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
                                                                $tmp_value = $words_key[$i];
                                                                if(!empty($tmp_value)){
                                                                        $end_word_key = $i; // previous line last word
                                                                        break;
                                                                }
                                                        }
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                        //echo '$end_word_key :'.$end_word_key.'$first_word_key :'.$first_word_key;
                                                        //print_r($words_key);
                                                        //die;
                                                }else{
                                                //echo 'inside else';die;
                                                        $end_word_key = $tmp_key; // previous line last word
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }
                                                $current_line_chars = $first_word_key - $start;	
                                                //echo '$tmp_key :'.$tmp_key.'$loop :'.$loop.'$current_line_chars :'.$current_line_chars.'$first_word_key :'.$first_word_key.'$end_word_key :'.$end_word_key;
                                                //echo '*****'.substr($giftMessageNote,$start,$current_line_chars). '*****';//die;
                                                $page->drawText(substr($giftMessageNote,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');                                                
                                                $new_line -=14;
                                                $tmp_key = $end_word_key+40;
                                                $start = $first_word_key ;

                                        }
                                        //word wrap logic  ends here
                                        
//                                        for($lop=0;$lop<strlen($giftMessageNote);$lop=($lop+40)){
//                                                $page->drawText(wordwrap(substr($giftMessageNote,$lop,40),40), $new_width, $new_line, 'UTF-8');
//                                                $start+=40;
//                                                $new_line -=14;
//                                        }                     
                                        $i++;
                                        //unlink($img_path . 'lbl' . $rnd . '.jpeg');
                                          
				}else{
                                        // ERROR CONDITION WHEN FEDEX API IS CALLED FOR THE SPECIFIC ORDER ID
				}
				//writeToLog($client);    // Write to log file
			} catch (SoapFault $exception) {
                            
                                // EXCEPTION HANDLER WHEN ERROR COMES IN TRY BLOCK
                                
				//$this->_redirectReferer();
				//printFault($exception, $client);
			}
			// Fedex Label api call ends here
                }elseif(strripos($pickup_method, 'ups')!== false){ // for ups labels, whose label status should be success
				
                    if ($colls) {
                        if(count($colls)>0){
                            foreach ($colls AS $k => $v) {
                    $coll = $v['upslabel_id'];
					
                    $collection = Mage::getModel('upslabel/upslabel')->load($coll);
                    if (($collection->getOrderId() == $order_id && $ptype == "order") || ($collection->getShipmentId() == $order_id && $ptype != "order")) {
						
                        if (file_exists($img_path . $collection->getLabelname()) && filesize($img_path . $collection->getLabelname()) > 1024) {
                            $page = $pdf->newPage($holstSize);
                            $pdf->pages[] = $page;
                            $f_cont = file_get_contents($img_path . $collection->getLabelname());
                            $img = imagecreatefromstring($f_cont);
                            if (Mage::getStoreConfig('upslabel/printing/verticalprint') == 1) {
                                $FullImage_width = imagesx($img);
                                $FullImage_height = imagesy($img);
                                $full_id = imagecreatetruecolor($FullImage_width, $FullImage_height);
                                $col = imagecolorallocate($img, 125, 174, 240);
                                $IMGfuul = imagerotate($img, -90, $col);
                            } else {
                                $IMGfuul = $img;
                            }
                            $rnd = rand(10000, 999999);
                            imagejpeg($IMGfuul, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                            $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
                      //custom code for pdf
					$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					$page->setFont($font, 12);
					//$order=Mage::getModel('sales/order')->load($order_id); 
					
					$giftMessage = Mage::getModel("giftmessage/message")->load($order->getGiftMessageId());		// add by sourav
					$giftMessageNote = $giftMessage->getMessage();		// add by sourav
					$giftMessageSender = $giftMessage->getSender();
					$orderItems = $order->getItemsCollection()
							->addAttributeToSelect('*')
							->load();							
					
					
						
					$page->drawImage($image, 20, 350, $width+20, $heigh+350); //draw shipping label ups
                                        $page->setFont($font, 10);
                                         $page->drawText("Contains Cut Flowers, Product of Colombia and Thailand", 22, 445, 'UTF-8'); // static content on label
                                         $page->setFont($font, 12);
					//shipping adddress
					$line = 340;
					$new_width = 30;
					$new_line = 300;
                                        
                                        foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
											$pdt_data = Mage::getModel("catalog/product")->load($sItem->getProductId());
                                             $pdt_recipe = $pdt_data->getRecipe();
						}
					}
                                        
                                        $recipe_line = "Recipe: ".$pdt_recipe;
                                        //word wrap logic for recipe starts here
                                        $words_key = str_word_count($recipe_line,2);
                                        $tmp_key = 35;
                                        $check_limit = 15;
                                        $start = 0;
                                        for($loop=0;$loop<strlen($recipe_line);$loop=($loop+1)){
                                                if(strlen($recipe_line)<($tmp_key-1)){
                                                        $first_word_key	= strlen($recipe_line);
                                                }
                                                $tmp_value = $words_key[$tmp_key];
                                                if(empty($tmp_value)){
                                                        for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
                                                                $tmp_value = $words_key[$i];
                                                                if(!empty($tmp_value)){
                                                                        $end_word_key = $i; // previous line last word
                                                                        break;
                                                                }
                                                        }
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }else{
                                                        $end_word_key = $tmp_key; // previous line last word
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }
												
                                                $current_line_chars = $first_word_key - $start;	
                                                $page->drawText(substr($recipe_line,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');
                                                $new_line -=14;
                                                $tmp_key = $end_word_key+40;
                                                $start = $first_word_key ;

                                        }
                     //word wrap logic for recipe  ends here
                                        
					$address = $order->getShippingAddress();
					$custName = $address->getName();
					$custAddr = $address->getStreetFull();
					$region = $address->getRegion();
					$country = $address->getCountry();
					$zipcode = $address->getPostcode();
					$baddress = $order->getBillingAddress();
					$bcustName = $baddress->getName();
					$bcustAddr = $baddress->getStreetFull();
					$bregion = $baddress->getRegion();
					$bcountry = $baddress->getCountry();
					$bzipcode = $baddress->getPostcode();					
								
					$new_width = $width+60;		 //Add by sourav
					$new_line = $heigh+330;
					$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
					
					if($is_s_day==1){		 //Add by sourav
						$page->setFont($font2, 14);					
						$page->drawText("S",60, $new_line+34, 'UTF-8');
					}
					
					$page->drawText("Order ID #:".$order->getIncrementId(), $new_width, $new_line, 'UTF-8');
					$new_line -=14;
					$page->drawText("Product Information:", $new_width, $new_line, 'UTF-8');
					$new_line -=14;
					$page->setFont($font, 10);
                                        
					foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
                                             //$page->drawText("Name: ".$sItem->getName(), $new_width, $new_line, 'UTF-8');
                                        $pdt_name = "Name: ".$sItem->getName();
                                        for($lp=0;$lp<strlen($pdt_name);$lp=($lp+45)){
                                            $page->drawText(substr($pdt_name,$lp,45), $new_width, $new_line, 'UTF-8');
                                            if(($lp+45)<strlen($pdt_name)) $new_line -=14;                                                                                       
                                        }
                                                 //$page->drawText("Name: ".$sItem->getName(), $new_width, $new_line, 'UTF-8');
                                                 $page->drawText("Ordered quantity: ".$sItem->getData('qty_ordered'), $new_width, $new_line-14, 'UTF-8');
                                            }
					}
					
                                                $new_line =284;	
                                                $new_width = $width+88;	                                                
                                                $page->drawText("From: ".$giftMessageSender, $new_width, $new_line, 'UTF-8');
						$new_line -=14;
						$page->drawText("Gift Message: ", $new_width, $new_line, 'UTF-8');
						$new_line -=14;
						$start = 0;
                                                
                                                //word wrap logic starts here
                                        $words_key = str_word_count($giftMessageNote,2);
                                        $tmp_key = 35;
                                        $check_limit = 15;
                                        //$xy = 1;
                                        $start = 0;
                                        for($loop=0;$loop<strlen($giftMessageNote);$loop=($loop+1)){
                                                //echo 'loop :'.$loop;
                                                $tmp_value = $words_key[$tmp_key];
                                                if(empty($tmp_value)){
                                                //echo 'inside if';die;
                                                        for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
                                                                $tmp_value = $words_key[$i];
                                                                if(!empty($tmp_value)){
                                                                        $end_word_key = $i; // previous line last word
                                                                        break;
                                                                }
                                                        }
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                        //echo '$end_word_key :'.$end_word_key.'$first_word_key :'.$first_word_key;
                                                        //print_r($words_key);
                                                        //die;
                                                }else{
                                                //echo 'inside else';die;
                                                        $end_word_key = $tmp_key; // previous line last word
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }
                                                $current_line_chars = $first_word_key - $start;	
                                                //echo '$loop :'.$loop.'$first_word_key :'.$first_word_key;die;
                                                //echo '$tmp_key :'.$tmp_key.'$loop :'.$loop.'$current_line_chars :'.$current_line_chars.'$first_word_key :'.$first_word_key.'$end_word_key :'.$end_word_key;
                                                //echo '*****'.substr($giftMessageNote,$start,$current_line_chars). '*****';//die;
                                                //if($xy == 2) {
                                                //echo '$tmp_key :'.$tmp_key.'$loop :'.$loop.'$current_line_chars :'.$current_line_chars.'$first_word_key :'.$first_word_key.'$end_word_key :'.$end_word_key;
                                                //die;
                                                //}
                                                $page->drawText(substr($giftMessageNote,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');
                                                //$start+=40;
                                                $new_line -=14;
                                                $tmp_key = $end_word_key+40;
                                                //$xy++;
                                                $start = $first_word_key ;

                                        }
                                        //word wrap logic  ends here
                                                
//                                                for($lop=0;$lop<strlen($giftMessageNote);$lop=($lop+40)){
//                                                        $page->drawText(wordwrap(substr($giftMessageNote,$lop,40),40), $new_width, $new_line, 'UTF-8');
//                                                        $new_line -=14;
//                                                }
					//end of custom code


                            unlink($img_path . 'lbl' . $rnd . '.jpeg');
                            $i++;
                        }
                    }
                    unset($IMGfuul);
                } 
                        }
                    }
                }else{ // other than fedex, ups and local delivery methods, whose label status should be success 
                    if($order->getPickupMethod()!=='Local Delivery'){
                    if ($colls) {
                        if(count($colls)>0){
                            foreach ($colls AS $k => $v) {
                    $coll = $v['upslabel_id'];
					
                    $collection = Mage::getModel('upslabel/upslabel')->load($coll);
                    if (($collection->getOrderId() == $order_id && $ptype == "order") || ($collection->getShipmentId() == $order_id && $ptype != "order")) {
						
                        if (file_exists($img_path . $collection->getLabelname()) && filesize($img_path . $collection->getLabelname()) > 1024) {
                            $page = $pdf->newPage($holstSize);
                            $pdf->pages[] = $page;
                            $f_cont = file_get_contents($img_path . $collection->getLabelname());
                            $img = imagecreatefromstring($f_cont);
                            if (Mage::getStoreConfig('upslabel/printing/verticalprint') == 1) {
                                $FullImage_width = imagesx($img);
                                $FullImage_height = imagesy($img);
                                $full_id = imagecreatetruecolor($FullImage_width, $FullImage_height);
                                $col = imagecolorallocate($img, 125, 174, 240);
                                $IMGfuul = imagerotate($img, -90, $col);
                            } else {
                                $IMGfuul = $img;
                            }
                            $rnd = rand(10000, 999999);
                            imagejpeg($IMGfuul, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                            $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
                            $page->drawImage($image, 0, 0, $width, $heigh);


//custom code for pdf

					$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					$page->setFont($font, 12);
					//$order=Mage::getModel('sales/order')->load($order_id); 
					
					$giftMessage = Mage::getModel("giftmessage/message")->load($order->getGiftMessageId());		// add by sourav
					$giftMessageNote = $giftMessage->getMessage();		// add by sourav
					$giftMessageSender = $giftMessage->getSender();
					$orderItems = $order->getItemsCollection()
							->addAttributeToSelect('*')
							->load();							
					
					
						
					$page->drawImage($image, 20, 350, $width+20, $heigh+350); //draw shipping label ups
					
					$line = 340;
					$new_width = 30;
					$new_line = 300;
                                        
                     foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
											$pdt_data = Mage::getModel("catalog/product")->load($sItem->getProductId());
                                             $pdt_recipe = $pdt_data->getRecipe();
						}
						}
                                        
                                        $recipe_line = "Recipe: ".$pdt_recipe;
                                        //word wrap logic for recipe starts here
                                        $words_key = str_word_count($recipe_line,2);
                                        $tmp_key = 35;
                                        $check_limit = 15;
                                        $start = 0;
                                        for($loop=0;$loop<strlen($recipe_line);$loop=($loop+1)){
                                                if(strlen($recipe_line)<($tmp_key-1)){
                                                        $first_word_key	= strlen($recipe_line);
                                                }
                                                $tmp_value = $words_key[$tmp_key];
                                                if(empty($tmp_value)){
                                                        for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
                                                                $tmp_value = $words_key[$i];
                                                                if(!empty($tmp_value)){
                                                                        $end_word_key = $i; // previous line last word
                                                                        break;
                                                                }
                                                        }
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }else{
                                                        $end_word_key = $tmp_key; // previous line last word
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }
												
                                                $current_line_chars = $first_word_key - $start;	
                                                $page->drawText(substr($recipe_line,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');
                                                $new_line -=14;
                                                $tmp_key = $end_word_key+40;
                                                $start = $first_word_key ;

                                        }
                     //word wrap logic for recipe  ends here
                                        
					$address = $order->getShippingAddress();
					$custName = $address->getName();
					$custAddr = $address->getStreetFull();
					$region = $address->getRegion();
					$country = $address->getCountry();
					$zipcode = $address->getPostcode();
					$baddress = $order->getBillingAddress();
					$bcustName = $baddress->getName();
					$bcustAddr = $baddress->getStreetFull();
					$bregion = $baddress->getRegion();
					$bcountry = $baddress->getCountry();
					$bzipcode = $baddress->getPostcode();
					
									
					$new_width = $width+60;		 //Add by sourav
					$new_line = $heigh+330;
					$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
					
					if($is_s_day==1){		 //Add by sourav
						$page->setFont($font2, 14);					
						$page->drawText("S",60, $new_line+34, 'UTF-8');
					}
					
					$page->drawText("Order ID #:".$order->getIncrementId(), $new_width, $new_line, 'UTF-8');
					$new_line -=14;
					$page->drawText("Product Information:", $new_width, $new_line, 'UTF-8');
					$new_line -=14;
					$page->setFont($font, 10);
					
					foreach($orderItem as $sItem) {
					 if($sItem->getProductType() == "simple"){
                                        $pdt_name = "Name: ".$sItem->getName();
                                        for($lp=0;$lp<strlen($pdt_name);$lp=($lp+45)){
                                            $page->drawText(substr($pdt_name,$lp,45), $new_width, $new_line, 'UTF-8');
                                            if(($lp+45)<strlen($pdt_name)) $new_line -=14;                                                                                       
                                        }     
					$page->drawText("Ordered quantity: ".$sItem->getData('qty_ordered'), $new_width, $new_line-14, 'UTF-8');
						}
					}
					
						$new_line =284;
						$new_width = $width+88;
						$page->drawText("From: ".$giftMessageSender, $new_width, $new_line, 'UTF-8');
						$new_line -=14;
						$page->drawText("Gift Message: ", $new_width, $new_line, 'UTF-8');
						$new_line -=14;
						$start = 0;
                                                //word wrap logic starts here
                                        $words_key = str_word_count($giftMessageNote,2);
                                        $tmp_key = 35;
                                        $check_limit = 15;
                                        //$xy = 1;
                                        $start = 0;
                                        for($loop=0;$loop<strlen($giftMessageNote);$loop=($loop+1)){
                                                //echo 'loop :'.$loop;
                                                $tmp_value = $words_key[$tmp_key];
                                                if(empty($tmp_value)){
                                                //echo 'inside if';die;
                                                        for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
                                                                $tmp_value = $words_key[$i];
                                                                if(!empty($tmp_value)){
                                                                        $end_word_key = $i; // previous line last word
                                                                        break;
                                                                }
                                                        }
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                        //echo '$end_word_key :'.$end_word_key.'$first_word_key :'.$first_word_key;
                                                        //print_r($words_key);
                                                        //die;
                                                }else{
                                                //echo 'inside else';die;
                                                        $end_word_key = $tmp_key; // previous line last word
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }
                                                $current_line_chars = $first_word_key - $start;	
                                                //echo '$loop :'.$loop.'$first_word_key :'.$first_word_key;die;
                                                //echo '$tmp_key :'.$tmp_key.'$loop :'.$loop.'$current_line_chars :'.$current_line_chars.'$first_word_key :'.$first_word_key.'$end_word_key :'.$end_word_key;
                                                //echo '*****'.substr($giftMessageNote,$start,$current_line_chars). '*****';//die;
                                                //if($xy == 2) {
                                                //echo '$tmp_key :'.$tmp_key.'$loop :'.$loop.'$current_line_chars :'.$current_line_chars.'$first_word_key :'.$first_word_key.'$end_word_key :'.$end_word_key;
                                                //die;
                                                //}
                                                $page->drawText(substr($giftMessageNote,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');
                                                //$start+=40;
                                                $new_line -=14;
                                                $tmp_key = $end_word_key+40;
                                                //$xy++;
                                                $start = $first_word_key ;

                                        }
                                        //word wrap logic  ends here
                                        
//						for($lop=0;$lop<strlen($giftMessageNote);$lop=($lop+40)){
//							$page->drawText(substr($giftMessageNote,$lop,40), $new_width, $new_line, 'UTF-8');
//							$start+=40;
//							$new_line -=14;
//						}
					//end of custom code


                            unlink($img_path . 'lbl' . $rnd . '.jpeg');
                            $i++;
                        }
                    }
                    unset($IMGfuul);
                }
                        }
                    }
                }
                } 
				//}

/* 
 * added by nareshseeta 
 * generating amazon PDF label with UPS label
 * 
 * */
 
 if($amazonorder){
              if(count($amazonorder)>0){				
                  foreach ($colls AS $k => $v) {
                      $coll = $v['upslabel_id'];
                      $collection = Mage::getModel('upslabel/upslabel')->load($coll);
						if (($collection->getOrderId() == $order_id && $ptype == "order") || ($collection->getShipmentId() == $order_id && $ptype != "order")) {
							if (file_exists($img_path . $collection->getLabelname()) && filesize($img_path . $collection->getLabelname()) > 1024)  {
                              $page = $pdf->newPage($holstSize);
                              $pdf->pages[] = $page;    
                              $image = Zend_Pdf_Image::imageWithPath($img_path.'/amazonlabel-new.jpg' ); //amazonlabel.jpg                     
							//custom code for pdf

                                                  $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
                                                  $page->setFont($font, 11);
                                                  $order=Mage::getModel('sales/order')->load($order_id); 

                                                  $giftMessage = Mage::getModel("giftmessage/message")->load($order->getGiftMessageId());		// add by sourav
                                                  $giftMessageNote = $giftMessage->getMessage();		// add by sourav
                                                  $giftMessageSender = $giftMessage->getSender();  
                                                  $orderItems = $order->getItemsCollection()
                                                                  ->addAttributeToSelect('*')
                                                                  ->load();	
                                                  $page->drawImage($image,-60, $heigh-120,620,865); //draw shipping label ups
                                                  
                                                 // $page->drawImage($image,-40, bot $heigh-40,620, Top 850);
                                                  //shipping adddress                      120,-30     600      500 left height
                                                  $line = 810;
                                                  $new_width = 0;
                                                  $new_line = $heigh;
                                                  $address = $order->getShippingAddress();
                                                  $custName = $address->getName();
                                                  $custAddr = $address->getStreetFull();
                                                  $region = $address->getRegion();
                                                  $country = $address->getCountry();
                                                  $zipcode = $address->getPostcode();
                                                  $baddress = $order->getBillingAddress();
                                                  $bcustName = $baddress->getName();
                                                  $bcustAddr = $baddress->getStreetFull();
                                                  $bregion = $baddress->getRegion();
                                                  $bcountry = $baddress->getCountry();
                                                  $bzipcode = $baddress->getPostcode();

                                                   $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
                                                   $page->setFont($font5, 11);
                                                  $page->drawText("Billing Address : ", 20, $line, 'UTF-8');
                                                  $line -=14;
                                                   $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
                                                   $page->setFont($font5, 8);
                                                  $page->drawText($bcustName, 20, $line, 'UTF-8');
                                                  $line -=10;
                                                  $page->drawText($bcustAddr, 20, $line, 'UTF-8');
                                                  $line -=10;
                                                  $page->drawText($bregion.', '.$bcountry.' - '.$bzipcode, 20, $line, 'UTF-8');
                                                  $line -=30;
                                                   $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
                                                   $page->setFont($font5, 11);
                                                  $page->drawText("Shipping Address : ", 20, $line, 'UTF-8');					
                                                  $line -=14;
                                                   $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
                                                   $page->setFont($font5, 8);
                                                  $page->drawText($custName, 20, $line, 'UTF-8');					
                                                  $line -=10;
                                                  $page->drawText($custAddr, 20, $line, 'UTF-8');					
                                                  $line -=10;
                                                  $page->drawText($region.', '.$country.' - '.$zipcode, 20, $line, 'UTF-8');					
                                                 		
                                                  $line -=20; 

                                                  $new_width = 20;		 //Add by sourav
                                                  $new_line = 695;
                                                  $font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);

                                                  if($is_s_day==1){		 //Add by sourav
                                                          $page->setFont($font2, 14);					
                                                          $page->drawText("S",60, $new_line+34, 'UTF-8');
                                                  }
                                                  $page->setLineWidth(0.5)->drawLine(0,$new_line-4,400,$new_line-4);
                                                  $font5 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
                                                   $page->setFont($font5, 11);
                                                  $page->drawText("Your Order ID #:".$order->getIncrementId(), $new_width,$new_line, 'UTF-8');
                                                  $new_line -=14;
                                               
                                                  $page->setLineWidth(0.5)->drawLine(0,$new_line-6,400,$new_line-6);
                                                  $page->setFont($font, 8);
                                                   $page->drawText("Qty", $new_width,$new_line, 'UTF-8');
                                                    $page->drawText("Name", $new_width+25,$new_line, 'UTF-8');
                                                  $new_line -=14;
                                                  //$orderItem = Mage::getResourceModel('sales/order_item_collection')->addFieldToFilter('order_id', $order_id)->getFirstItem()->getProductOptions();
                                                  //print_r($orderItems->getData());die;
                                                  foreach($orderItems as $sItem) {
                                                  if($sItem->getProductType() == "simple"){
                                                  //$page->drawText($sItem->getName(), $new_width+60, $new_line, 'UTF-8');
                                                  
                                                  $pdt_name = $sItem->getName();
                                                
                                                  
                                        for($lp=0;$lp<strlen($pdt_name);$lp=($lp+45)){
                                            $page->drawText(substr($pdt_name,$lp,45), $new_width+25, $new_line, 'UTF-8');
                                            if(($lp+45)<strlen($pdt_name)) $new_line -=14;                                                                                       
                                        }
                                                  $page->drawText(intval($sItem->getData('qty_ordered')), $new_width, $new_line, 'UTF-8');
                                                          }
                                                  }

                                                  $new_line =690; 
                                                  $new_width = $new_width+25;
                                               $page->drawText("From: ".$giftMessageSender, $new_width, $new_line-40, 'UTF-8');
												$new_line -=10;
                                                          $page->drawText("Gift Message: ", $new_width, $new_line-40, 'UTF-8');
                                                         
                                                          $start = 0;
                                                         for($lop=0;$lop<strlen($giftMessageNote);$lop=($lop+80)){
                                                                  $page->drawText(substr($giftMessageNote,$start,80), $new_width+45, $new_line-40, 'UTF-8');
                                                                  $start+=0;
                                                                  $new_line -=10;
                                                          }
                                                          
                                                 $page->setLineWidth(0.9)->drawLine(0,$new_line-35,400,$new_line-35);
                                                 $page->drawText("This Completes Your Gift Order.", 20, $new_line-42, 'UTF-8');
                            $f_cont = file_get_contents($img_path . $collection->getLabelname());
                            $img = imagecreatefromstring($f_cont);
                            $rnd = rand(10000, 999999);
                            imagejpeg($img, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                            $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
                            $page->drawImage($image, 20, 280, $width+390, $heigh+110);
                            //$page->rotate(20, 140, M_PI/2);
                            //$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
							//$page->drawText('Contains Cut Flowers, Product of Colombia and Thailand', 400, 200);
							$page->setFillColor(new Zend_Pdf_Color_GrayScale(1.0));
                            $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.2));							
							//$page->rotate(-20, -140,  M_PI/-2);
							
							$page->drawRectangle(20, 20, 300, 260);							
							$page->drawRectangle(320, 20, 580, 260);
							$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
							$page->setFont($font5, 11);
							$page->setFillColor(Zend_Pdf_Color_Html::color('#000000'));
							$new_line = 240;
							$new_width = 340;

							$page->drawText("Order ID #:".$order->getIncrementId(), $new_width, $new_line, 'UTF-8');
							$new_line -=14;
							$page->drawText("Product Information:", $new_width, $new_line, 'UTF-8');
							$new_line -=14;
							$font6 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
							$page->setFont($font6, 10);
							foreach($orderItems as $sItem) {
							 if($sItem->getProductType() == "simple"){
							//$page->drawText("Name: ".$sItem->getName(), $new_width, $new_line, 'UTF-8');
                                        $pdt_name = "Name: ".$sItem->getName();
                                        for($lp=0;$lp<strlen($pdt_name);$lp=($lp+45)){
                                            $page->drawText(substr($pdt_name,$lp,45), $new_width, $new_line, 'UTF-8');
                                            if(($lp+45)<strlen($pdt_name)) $new_line -=14;                                                                                       
                                        }     
					$page->drawText("Ordered quantity: ".$sItem->getData('qty_ordered'), $new_width, $new_line-14, 'UTF-8');
						}
					}
					foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
											$pdt_data = Mage::getModel("catalog/product")->load($sItem->getProductId());
                                             $pdt_recipe = $pdt_data->getRecipe();
						}
					}                                        
                   $recipe_line = "Recipe: ".$pdt_recipe;
                   $page->drawText($recipe_line, $new_width-300, $new_line+15, 'UTF-8');
                   
                   $page->rotate(20, 140, M_PI/2);
					$page->drawText('Contains Cut Flowers, Product of Colombia and Thailand', 170, -380);
					//end of custom code


                              unlink($img_path . 'lbl' . $rnd . '.jpeg');
                              $i++;
                          }
                      }
                      unset($IMGfuul);
                  }
             }
       }else{	
				
					 $order=Mage::getModel('sales/order')->load($order_id); 
					 
					if($order->getPickupMethod()=='Local Delivery'){
                                         //START Handle Shipment
                                        /* if (($order->getState() == Mage_Sales_Model_Order::STATE_NEW) || ($order->getState() == Mage_Sales_Model_Order::STATE_PROCESSING)) {   
                                        $shipment = $order->prepareShipment();
                                        $shipment->register();

                                        $order->setIsInProcess(true);
                                        $order->addStatusHistoryComment('Automatically Shipment by Kabloom Development Team.', false);

                                        $transactionSave = Mage::getModel('core/resource_transaction')
                                            ->addObject($shipment)
                                            ->addObject($shipment->getOrder())
                                            ->save();
                                         }*/
                                        //END Handle Shipment
                                            
					  $j++;
					 $giftMessage = Mage::getModel("giftmessage/message")->load($order->getGiftMessageId());		// add by sourav
					 $giftMessageNote = $giftMessage->getMessage();		// add by sourav
					$giftMessageSender = $giftMessage->getSender();
					 $orderItems = $order->getItemsCollection()
							->addAttributeToSelect('*')
							->load(); 
							
					 $page = $pdf->newPage($holstSize);
                     $pdf->pages[] = $page;	
					 $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					 $page->setFont($font, 12);
					 
					 $page->drawText("Shipping by Local Delivery", 40, 710, 'UTF-8');
                                         $line = 665;
                                         $address = $order->getShippingAddress();
					$custName = $address->getName();
					$custAddr = $address->getStreetFull();
					$region = $address->getRegion();
					$country = $address->getCountry();
					$zipcode = $address->getPostcode();
                    $telephone = $address->getTelephone();
					$baddress = $order->getBillingAddress();
					$bcustName = $baddress->getName();
					$bcustAddr = $baddress->getStreetFull();
					$bregion = $baddress->getRegion();
					$bcountry = $baddress->getCountry();
					$bzipcode = $baddress->getPostcode();
                    $btelephone = $baddress->getTelephone();
					
					$page->drawText("Ship to: ", 40, $line, 'UTF-8');					
					$line -=28;
					$page->drawText($custName, 40, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($custAddr, 40, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($region, 40, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($country, 40, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($zipcode, 40, $line, 'UTF-8');					
					$line -=42; 
                                        $page->drawText("Recipient No:".$telephone, 40, $line, 'UTF-8');					
					$line -=42; 
                                        $page->drawText("Sender No:".$btelephone, 40, $line, 'UTF-8');					
					 
					 
					 $line = 340;
					$new_width = 30;
					$new_line = 300;
                                        
                     foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
											$pdt_data = Mage::getModel("catalog/product")->load($sItem->getProductId());
                                             $pdt_recipe = $pdt_data->getRecipe();
						}
					}
                                        
                                        $recipe_line = "Recipe: ".$pdt_recipe;
                                        //word wrap logic for recipe starts here
                                        $words_key = str_word_count($recipe_line,2);
                                        $tmp_key = 35;
                                        $check_limit = 15;
                                        $start = 0;
                                        for($loop=0;$loop<strlen($recipe_line);$loop=($loop+1)){
                                                if(strlen($recipe_line)<($tmp_key-1)){
                                                        $first_word_key	= strlen($recipe_line);
                                                }
                                                $tmp_value = $words_key[$tmp_key];
                                                if(empty($tmp_value)){
                                                        for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
                                                                $tmp_value = $words_key[$i];
                                                                if(!empty($tmp_value)){
                                                                        $end_word_key = $i; // previous line last word
                                                                        break;
                                                                }
                                                        }
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }else{
                                                        $end_word_key = $tmp_key; // previous line last word
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }
												
                                                $current_line_chars = $first_word_key - $start;	
                                                $page->drawText(substr($recipe_line,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');
                                                $new_line -=14;
                                                $tmp_key = $end_word_key+40;
                                                $start = $first_word_key ;

                                        }
					//word wrap logic for recipe  ends here
					$new_width = $width+60;		 //Add by sourav
					$new_line = $heigh+330;
					$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);					
					$page->setFont($font2, 12);
					$page->drawText("Order ID #:".$order->getIncrementId(), $new_width, $new_line, 'UTF-8');
					$new_line -=14;
					$page->drawText("Product Information:", $new_width, $new_line, 'UTF-8');
					$new_line -=14;
					$page->setFont($font, 10);
					foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
					//$page->drawText("Name: ".$sItem->getName(), $new_width, $new_line, 'UTF-8');
                                         $pdt_name = "Name: ".$sItem->getName();
                                        for($lp=0;$lp<strlen($pdt_name);$lp=($lp+45)){
                                            $page->drawText(substr($pdt_name,$lp,45), $new_width, $new_line, 'UTF-8');
                                            if(($lp+45)<strlen($pdt_name)) $new_line -=14;                                                                                       
                                        }
					$page->drawText("Ordered quantity: ".$sItem->getData('qty_ordered'), $new_width, $new_line-14, 'UTF-8');
						}
					}
					
					$new_line =340;
					$new_width = $width+88;	
                                        $page->drawText("From: ".$giftMessageSender, $new_width, $new_line, 'UTF-8');
                                        $new_line -=14;
                                        $page->drawText("Gift Message: ", $new_width, $new_line, 'UTF-8');
                                        $new_line -=14;
                                        $start = 0;

                                        //word wrap logic starts here
                                        $words_key = str_word_count($giftMessageNote,2);
                                        $tmp_key = 35;
                                        $check_limit = 15;
                                        //$xy = 1;
                                        $start = 0;
                                        for($loop=0;$loop<strlen($giftMessageNote);$loop=($loop+1)){
                                                //echo 'loop :'.$loop;
												if(strlen($giftMessageNote)<($tmp_key-1)){
													$first_word_key	= strlen($giftMessageNote);
												}
                                                $tmp_value = $words_key[$tmp_key];
                                                if(empty($tmp_value)){
                                                //echo 'inside if';die;
                                                        for($i=$tmp_key-1;$i>$tmp_key-$check_limit;$i--){
                                                                $tmp_value = $words_key[$i];
                                                                if(!empty($tmp_value)){
                                                                        $end_word_key = $i; // previous line last word
                                                                        break;
                                                                }
                                                        }
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                        //echo '$end_word_key :'.$end_word_key.'$first_word_key :'.$first_word_key;
                                                        //print_r($words_key);
                                                        //die;
                                                }else{
                                                //echo 'inside else';die;
                                                        $end_word_key = $tmp_key; // previous line last word
                                                        for($j=$tmp_key+1;$j<$tmp_key+$check_limit;$j++){
                                                                $tmp_value = $words_key[$j];
                                                                if(!empty($tmp_value)){
                                                                        $first_word_key = $j; // next line first word
                                                                        break;
                                                                }
                                                        }
                                                }
												
                                                $current_line_chars = $first_word_key - $start;	
                                                $page->drawText(substr($giftMessageNote,$start,$current_line_chars), $new_width, $new_line, 'UTF-8');
                                                //$start+=40;
                                                $new_line -=14;
                                                $tmp_key = $end_word_key+40;
                                                //$xy++;
                                                $start = $first_word_key ;

                                        }
                                        //word wrap logic  ends here                                                
					
					 unlink($img_path . 'lbl' . $rnd . '.jpeg');
					 unset($IMGfuul);
					 
					 
					}elseif(strpos($order->getPickupMethod(),'Federal') !== false){
							
					  $j++;
					 $giftMessage = Mage::getModel("giftmessage/message")->load($order->getGiftMessageId());		// add by sourav
					 $giftMessageNote = $giftMessage->getMessage();		// add by sourav
					 $giftMessageSender = $giftMessage->getSender();
					 $orderItems = $order->getItemsCollection()
							->addAttributeToSelect('*')
							->load(); 
							
					 $page = $pdf->newPage($holstSize);
                     $pdf->pages[] = $page;	
					 $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
					 $page->setFont($font, 12);
					 
					 $page->drawText("Shipping by ".$order->getPickupMethod(), 40, 710, 'UTF-8');
					 $page->drawText("Pickup date:- ".$order->getPickupDate(), 40, 680, 'UTF-8');
					 
					 $line = 340;
					$new_width = (int)$width+30;
					$new_line = $heigh+350;
					$address = $order->getShippingAddress();
					$custName = $address->getName();
					$custAddr = $address->getStreetFull();
					$region = $address->getRegion();
					$country = $address->getCountry();
					$zipcode = $address->getPostcode();
					$baddress = $order->getBillingAddress();
					$bcustName = $baddress->getName();
					$bcustAddr = $baddress->getStreetFull();
					$bregion = $baddress->getRegion();
					$bcountry = $baddress->getCountry();
					$bzipcode = $baddress->getPostcode();
					
					$page->drawText("Ship to: ", 20, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($custName, 20, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($custAddr, 20, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($region, 20, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($country, 20, $line, 'UTF-8');					
					$line -=14;
					$page->drawText($zipcode, 20, $line, 'UTF-8');					
					$line -=20; 
					$page->drawText("Bill to: ", 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText($bcustName, 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText($bcustAddr, 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText($bregion, 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText($bcountry, 20, $line, 'UTF-8');
					$line -=14;
					$page->drawText($bzipcode, 20, $line, 'UTF-8');
					$line -=14;	
					
					$new_width = $width+60;		 //Add by sourav
					$new_line = $heigh+330;
					$font2 = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);					
					$page->setFont($font2, 12);
					$page->drawText("Order ID #:".$order->getIncrementId(), $new_width, $new_line, 'UTF-8');
					$new_line -=14;
					$page->drawText("Product Information:", $new_width, $new_line, 'UTF-8');
					$new_line -=14;
					$page->setFont($font, 10);
					foreach($orderItems as $sItem) {
					 if($sItem->getProductType() == "simple"){
					$page->drawText("Name: ".$sItem->getName(), $new_width, $new_line, 'UTF-8');
					$page->drawText("Ordered quantity: ".$sItem->getData('qty_ordered'), $new_width, $new_line-14, 'UTF-8');
						}
					}
					
					$new_line =340;
						$page->drawText("From: ".$giftMessageSender, $new_width, $new_line, 'UTF-8');
						$new_line -=14;
						$page->drawText("Gift Message: ", $new_width, $new_line, 'UTF-8');
						$new_line -=14;
						$start = 0;
						for($lop=0;$lop<strlen($giftMessageNote);$lop=($lop+50)){
							$page->drawText(substr($giftMessageNote,$start,50), $new_width, $new_line, 'UTF-8');
							$start+=50;
							$new_line -=14;
						}
						
					 unlink($img_path . 'lbl' . $rnd . '.jpeg');
					 unset($IMGfuul);
					 
					 
					
					}
					
					
					
				}
            //}
                               
                //Generating Invoice starts here 
                $order=Mage::getModel('sales/order')->load($order_id);  
                $pickup_method = $order->getPickupMethod();               
                if ((($order->getState() == Mage_Sales_Model_Order::STATE_NEW) || ($order->getState() == Mage_Sales_Model_Order::STATE_PROCESSING)) && ($pickup_method != "Local Delivery")) {
                        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                        $invoice->register();

                        $invoice->getOrder()->setCustomerNoteNotify(false);          
                        $invoice->getOrder()->setIsInProcess(true);
                        $order->addStatusHistoryComment('Automatically INVOICED by Kabloom Development Team.', false);

                        $transactionSave = Mage::getModel('core/resource_transaction')
                                ->addObject($invoice)
                                ->addObject($invoice->getOrder());

                        $transactionSave->save();
                }
                //Generating Invoice ends here
		 	
        }
        
        //$pdf->save();
        if ($i > 0) {	
            //echo 'inside if $i value:'.$i.' $j value:'.$j;die;
            $pdfData = $pdf->render();
            header("Content-Disposition: inline; filename=result.pdf");
            header("Content-type: application/x-pdf");
            die($pdfData);
            //echo $pdfData;
            //return true;
        } else {
            //echo 'inside else $i value:'.$i.' $j value:'.$j;die;
			if ($j > 0) {		
				$pdfData = $pdf->render();
				header("Content-Disposition: inline; filename=result.pdf");
				header("Content-type: application/x-pdf");
				die($pdfData);
			}
            return false;
        }
    }

    static public function createFromLists($order_ids)
    {
		
        $img_path = Mage::getBaseDir('media') . '/upslabel/label/';
        $pdf = new Zend_Pdf();
        $i = 0;
        //$pdf->pages = array_reverse($pdf->pages);
        if (!is_array($order_ids)) {
            $order_ids = explode(',', $order_ids);
        }
        $width = strlen(Mage::getStoreConfig('upslabel/printing/dimensionx')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensionx') : 1400 / 2.6;
        $heigh = strlen(Mage::getStoreConfig('upslabel/printing/dimensiony')) > 0 ? Mage::getStoreConfig('upslabel/printing/dimensiony') : 800 / 2.6;
        if (strlen(Mage::getStoreConfig('upslabel/printing/holstx')) > 0 && strlen(Mage::getStoreConfig('upslabel/printing/holsty')) > 0) {
            $holstSize = Mage::getStoreConfig('upslabel/printing/holstx') . ':' . Mage::getStoreConfig('upslabel/printing/holsty') . ':';
        } else {
            $holstSize = Zend_Pdf_Page::SIZE_A4;
        }
        foreach ($order_ids as $order_id) {
            $collection = Mage::getModel('upslabel/upslabel')->load($order_id);
            if ($collection && $collection->getStatus() == 0) {
                if (file_exists($img_path . $collection->getLabelname()) && filesize($img_path . $collection->getLabelname()) > 1024) {
                    $page = $pdf->newPage($holstSize);
                    $pdf->pages[] = $page;
                    $f_cont = file_get_contents($img_path . $collection->getLabelname());
                    $img = imagecreatefromstring($f_cont);
                    if (Mage::getStoreConfig('upslabel/printing/verticalprint') == 1) {
                        $FullImage_width = imagesx($img);
                        $FullImage_height = imagesy($img);
                        $full_id = imagecreatetruecolor($FullImage_width, $FullImage_height);
                        $col = imagecolorallocate($img, 125, 174, 240);
                        $IMGfuul = imagerotate($img, -90, $col);
                    } else {
                        $IMGfuul = $img;
                    }
                    $rnd = rand(10000, 999999);
                    imagejpeg($IMGfuul, $img_path . 'lbl' . $rnd . '.jpeg', 100);
                    $image = Zend_Pdf_Image::imageWithPath($img_path . 'lbl' . $rnd . '.jpeg');
                    $page->drawImage($image, 0, 0, $width, $heigh);
                    unlink($img_path . 'lbl' . $rnd . '.jpeg');
                    $i++;
                }
                unset($IMGfuul);
            }
        }
        //$pdf->save();
        if ($i > 0) {
            $pdfData = $pdf->render();
            header("Content-Disposition: inline; filename=result.pdf");
            header("Content-type: application/x-pdf");
            echo $pdfData;
            return true;
        } else {
            return false;
        }
    }
   public function addShipperAction($shipper_address){
		$shipper = array(
			'Contact' => array(
				'PersonName' => $shipper_address['personname'],
				'CompanyName' => $shipper_address['companyname'],
				'PhoneNumber' => $shipper_address['phonenumber']
			),
			'Address' => array(
				'StreetLines' => array($shipper_address['streetlines']),
				'City' => $shipper_address['city'],
				'StateOrProvinceCode' => $shipper_address['stateorprovincecode'],
				'PostalCode' => $shipper_address['postalcode'],
				'CountryCode' => $shipper_address['countrycode']
			)
		);
		return $shipper;
	}
	public function addRecipientAction($recipient_address){
		$recipient = array(
			'Contact' => array(
				'PersonName' => $recipient_address['personname'],
				//'CompanyName' => 'Residential',
				'PhoneNumber' => $recipient_address['phonenumber']
			),
			'Address' => array(
				'StreetLines' => array($recipient_address['streetlines'][0],$recipient_address['streetlines'][1]),
				'City' => $recipient_address['city'],
				'StateOrProvinceCode' => $recipient_address['stateorprovincecode'],
				'PostalCode' => $recipient_address['postalcode'],
				'CountryCode' => $recipient_address['countrycode'],
				'Residential' => true
			)
		);
		return $recipient;	                                    
	}
	public function addShippingChargesPaymentAction($billaccount){
		$shippingChargesPayment = array(
			'PaymentType' => 'SENDER',
			'Payor' => array(
				'ResponsibleParty' => array(
					'AccountNumber' => $billaccount,
					'Contact' => null,
					'Address' => array(
						'CountryCode' => 'US'
					)
				)
			)
		);
		return $shippingChargesPayment;
	}
	public function addLabelSpecificationAction(){
		$labelSpecification = array(
			'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
			'ImageType' => 'PNG',  // valid values DPL, EPL2, PDF, ZPLII and PNG
			'LabelStockType' => 'PAPER_4X6' // default value is PAPER_7X4.75
		);
		return $labelSpecification;
	}
	public function addSpecialServicesAction(){
		$specialServices = array(
			'SpecialServiceTypes' => array('COD'),
			'CodDetail' => array(
				'CodCollectionAmount' => array(
					'Currency' => 'USD', 
					'Amount' => 150
				),
				'CollectionType' => 'ANY' // ANY, GUARANTEED_FUNDS
			)
		);
		return $specialServices; 
	}
	public function addPackageLineItem1Action($servicetype){
		$packageLineItem = array(
			'SequenceNumber'=>1,
			'GroupPackageCount'=>1,
			'Weight' => array(
				'Value' => 3.0,
				'Units' => 'LB'
			),
			'Dimensions' => array(
				'Length' => 28,
				'Width' => 8,
				'Height' => 8,
				'Units' => 'IN'
			),
			//'SpecialServicesRequested' => $this->addSpecialServicesAction()
			/*'CustomerReferences' => array(
				'0' => array(
					'CustomerReferenceType' => 'CUSTOMER_REFERENCE', // valid values CUSTOMER_REFERENCE, INVOICE_NUMBER, P_O_NUMBER and SHIPMENT_INTEGRITY
					'Value' => 'GR4567892'
				), 
				'1' => array(
					'CustomerReferenceType' => 'INVOICE_NUMBER', 
					'Value' => 'INV4567892'
				),
				'2' => array(
					'CustomerReferenceType' => 'P_O_NUMBER', 
					'Value' => 'PO4567892'
				)
			),*/
			
		);
		if($servicetype == 'FEDEX_GROUND'){
			$packageLineItem['SpecialServicesRequested'] = $this->addSpecialServicesAction();
		}
		return $packageLineItem;
	}
}

