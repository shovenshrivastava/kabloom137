<?php
require_once Mage::getBaseDir() . '/amazon/entities/Order.php';
require_once Mage::getBaseDir() . '/amazon/entities/Product.php';
require_once Mage::getBaseDir() . '/amazon/edi/855.php';
require_once Mage::getBaseDir() . '/amazon/constants.php';
require_once Mage::getBaseDir() . '/amazon/essentials/write.php';


class Infomodus_Upslabel_Adminhtml_AcceptorderController extends Mage_Core_Controller_Front_Action{
	public function indexAction($order_ids, $type, $ptype){
        $ptype = $this->getRequest()->getParam('type');
		$order_ids = $this->getRequest()->getParam($ptype . '_ids');
        $i = 0;
        foreach ($order_ids as $id) {
                $i++;
                $order = Mage::getModel('sales/order')->load($id);            
                $amazon_order[$i] = new Order();
                $amazon_order[$i]->orderId = $order->getId();
                $amazon_order[$i]->partnerOrderId = $order->getPartnerOrderId();
                $amazon_order[$i]->wareHouse = $order->getWarehouse();
	            $amazon_order[$i]->shipmentMethod = $order->getPartnerShipmentMethod();
                $amazon_order[$i]->orderDate = $order->getCreatedAt();
                $product = new Product();
                $ordered_items = $order->getAllItems();
                $products = array();
                $j = 0;
                foreach ($ordered_items as $item) {
                    $product = new Product();
                    $product->id = $item->getProductId();
                    $product->sku = $item->getSku();
                    $product->quantity = $item->getQtyOrdered();
                    $product->price = $item->getPrice();
                    $product->title = $item->getName();
                    $products[$j] = $product;
                    $j++;
                }
                $amazon_order[$i]->products = $products;				
		        $icn = rand(100000000, 999999999);
                //print_r($amazon_order[$i]);
                $edi = generateResponse_855($amazon_order[$i], $icn);
                //echo "edi is:".$edi;
                write_edi($edi, $icn);
			}
            
            Mage::getSingleton('adminhtml/session')->addSuccess('Order Accepted Successfully'); 
$this->_redirect('adminhtml/sales_order');
    }
}
?>