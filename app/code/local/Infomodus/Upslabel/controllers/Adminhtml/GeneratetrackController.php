<?php
require_once 'UpslabelController.php';
require_once 'GenerateinvoiceController.php';
require_once Mage::getBaseDir() . '/amazon/entities/Order.php';
require_once Mage::getBaseDir() . '/amazon/entities/Package.php';
require_once Mage::getBaseDir() . '/amazon/entities/Product.php';
require_once Mage::getBaseDir() . '/amazon/entities/Address.php';
require_once Mage::getBaseDir() . '/amazon/edi/856.php';
require_once Mage::getBaseDir() . '/amazon/constants.php';
require_once Mage::getBaseDir() . '/amazon/essentials/write.php';
require_once Mage::getBaseDir() . '/jet/token.php';


class Infomodus_Upslabel_Adminhtml_GeneratetrackController extends Mage_Core_Controller_Front_Action{
	public function indexAction($order_ids, $type, $ptype){
                     
        $ptype = $this->getRequest()->getParam('type');
		$order_ids = $this->getRequest()->getParam($ptype . '_ids');
		//print_r($order_ids); exit;
		if($this->getRequest()->getParam('track_id')){
			$track_id = $this->getRequest()->getParam('track_id');
			if(strlen($track_id) >= 30){
				$track_id = substr($track_id, -12);
			}
			
			$trackingNumbers = Mage::getResourceModel('sales/order_shipment_track_collection')
								->addAttributeToSelect('*')
								->addAttributeToFilter('track_number',$track_id);
								
								
			$trackingNumbers->getSelect()->order('order_id desc')->limit(1);
			$trackData = $trackingNumbers->getData();
			$order_idss = $trackData[0]['order_id'];
			$orderData = Mage::getModel("sales/order")->load($order_idss);
			
			$order_ids[] = $orderData->getId();	 
		}
		
            $i = 0;
            foreach ($order_ids as $id) {
                $i++;
                $order = Mage::getModel('sales/order')->load($id);
      if($order->getTradingPartner()=="Jet"){

					echo "this is jet"; 
					$shipJetOrder['orderId'] = $order->getIncrementId();
		$shippedOrderArray = array();	
		$order = Mage::getModel('sales/order')->loadByIncrementId($shipJetOrder['orderId']);
		$ordered_items = $order->getAllItems(); 			
		$shippedOrderArray['alt_order_id'] = $order->getIncrementId();
		$shipment = $order->getShipmentsCollection()->getFirstItem();
		$shipmentIncrementId = $shipment->getIncrementId();		
		$shippedOrderArray['shipments'][0]['alt_shipment_id'] = $shipmentIncrementId;
		//print_r($order->getTracksCollection()->getData()); die;
		
		foreach ($order->getTracksCollection() as $_track) {
			$shippedOrderArray['shipments'][0]['shipment_tracking_number'] = $_track->getNumber();
			$shippedOrderArray['shipments'][0]['carrier'] = strtoupper($_track->getCarrierCode());
		}
		
        $PickupDate =  date( 'Y-m-d\TH:i:s'. substr ( ( string ) microtime (), 1, 8 ) , strtotime($order->getPickupDate()) ); //2015-10-17T07:06:21.5663224Z  2015-09-30T05:47:58.272272272
        $DeliveringDate =  date( 'Y-m-d\TH:i:s'. substr ( ( string ) microtime (), 1, 8 ) , strtotime($order->getdeliveringDate()) ); 
        $shippedOrderArray['shipments'][0]['response_shipment_date'] = "$PickupDate";
		$shippedOrderArray['shipments'][0]['response_shipment_method'] = $order->getPartnerShipmentMethod();
		$shippedOrderArray['shipments'][0]['expected_delivery_date'] = "$DeliveringDate";
		$shippedOrderArray['shipments'][0]['ship_from_zip_code'] = "08003" ;
		$shippedOrderArray['shipments'][0]['carrier_pick_up_date'] = "$PickupDate";
		$ordered_items = $order->getAllItems();
		$j = 0;
        foreach ($ordered_items as $item) {
					//print_r($item->getData());
                    //$shippedOrderArray['shipments']['shipment_items']['shipment_item_id'] = "fulfillable" ;
					$shippedOrderArray['shipments'][0]['shipment_items'][$j]['alt_shipment_item_id'] = $item->getItemId();
                    $shippedOrderArray['shipments'][0]['shipment_items'][$j]['merchant_sku'] = $item->getSku();
                    $shippedOrderArray['shipments'][0]['shipment_items'][$j]['response_shipment_sku_quantity'] = round($item->getQtyOrdered());
                    $shippedOrderArray['shipments'][0]['shipment_items'][$j]['response_shipment_cancel_qty'] = round($item->getQtyOrdered());
                    $j++;
                }
		
        //$shippedOrderArray['shipments']['shipment_items']['RMA_number'] = "" ;
        //$shippedOrderArray['shipments']['shipment_items']['days_to_return'] = 30 ;
        //$shippedOrderArray['shipments']['shipment_items']['return_location']['address1'] = "2 Pin Oak Lane, Suite 100" ;
        //$shippedOrderArray['shipments']['shipment_items']['return_location']['address2'] = "fulfillable" ;
        //$shippedOrderArray['shipments']['shipment_items']['return_location']['city'] = "CHERRY HILL" ;
        //$shippedOrderArray['shipments']['shipment_items']['return_location']['state'] = "NJ" ;
        //$shippedOrderArray['shipments']['shipment_items']['return_location']['zip_code'] = "08003" ;
		//print_r($shippedOrderArray); die;
		$end_point = "orders/".$order->getPartnerOrderId()."/shipped";
		jetAPIPUT($end_point, $shippedOrderArray);
		$order->setStatus('invoice_sent')->save();
		Mage::getSingleton('adminhtml/session')->addSuccess('Jet Order Complete '); 
		 
					}else{
                $amazon_order[$i] = new Order();

                $amazon_order[$i]->orderId = $order->getId();
              $amazon_order[$i]->partnerOrderId = $order->getPartnerOrderId();
  
              $amazon_order[$i]->wareHouse = $order->getWarehouse();
	$amazon_order[$i]->shipmentMethod = $order->getPartnerShipmentMethod();
                $amazon_order[$i]->orderDate = $order->getCreatedAt();

                $product = new Product();
                $ordered_items = $order->getAllItems();
                $products = array();
                $j = 0;
                foreach ($ordered_items as $item) {
                    $product = new Product();
                    $product->id = $item->getProductId();
                    $product->sku = $item->getSku();
                    $product->quantity = $item->getQtyOrdered();
                    $product->price = $item->getPrice();
                    $product->title = $item->getName();
                    $products[$j] = $product;
                    $j++;
                }
                $package = new Package();
                foreach ($order->getTracksCollection() as $_track) {
                    $package->trackingNumber = $_track->getNumber();
                    $package->shipmentCode = $_track->getCarrierCode();
                }
             
                $package->weight = '3';

                $amazon_order[$i]->package = $package;
                $amazon_order[$i]->products = $products;

                $shipping_address = new Address();
                $shipping_address->firstname = $order->getShippingAddress()->getFirstname();
                $shipping_address->lastname = $order->getShippingAddress()->getLastname();
                $shipping_address->street = $order->getShippingAddress()->getStreet();
                $shipping_address->city = $order->getShippingAddress()->getCity();
                $shipping_address->region = $order->getShippingAddress()->getRegion();
                $shipping_address->telephone = $order->getShippingAddress()->getTelephone();
                $shipping_address->postcode = $order->getShippingAddress()->getPostcode();
                $shipping_address->countryId = $order->getShippingAddress()->getCountryId();

                $billing_address = new Address();
                $billing_address->firstname = $order->getBillingAddress()->getFirstname();
                $billing_address->lastname = $order->getBillingAddress()->getLastname();
                $billing_address->street = $order->getBillingAddress()->getStreet();
                $billing_address->city = $order->getBillingAddress()->getCity();
                $billing_address->region = $order->getBillingAddress()->getRegion();
                $billing_address->telephone = $order->getBillingAddress()->getTelephone();
                $billing_address->postcode = $order->getBillingAddress()->getPostcode();
                $billing_address->countryId = $order->getBillingAddress()->getCountryId();



                $amazon_order[$i]->shipmentAddress = $shipping_address;
                $amazon_order[$i]->billingAddress = $billing_address;
				
				$icn = rand(100000000, 999999999);
                $edi = generateResponse_856($amazon_order[$i], $icn);
                echo "edi is:".$edi;
                write_edi($edi, $icn);
                //$amazon_order[$i]->package = $order->getId();
		$order->setStatus('track_sent')->save();
				}
            }
//print_r($amazon_order);die;
            
            Mage::getSingleton('adminhtml/session')->addSuccess('Track Sent Successfully'); 
			if($this->getRequest()->getParam('club')){
				$club = $this->getRequest()->getParam('club');
				$resp = Infomodus_Upslabel_Adminhtml_GenerateinvoiceController::indexAction($order_ids, $type, $ptype, $club);
				
				return true;
			} else{
				$this->_redirect('adminhtml/sales_order');
			}
    }
}
?>
