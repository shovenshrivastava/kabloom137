<?php
class Infomodus_Upslabel_Model_Fedex
{
	function getShip($order_id)
    {
		// Fedex Label api call starts here
			$order = Mage::getModel('sales/order')->load($order_id);
			$orderIncrementId = $order->getIncrementId();
			$pickup_method = $order->getPickupMethod(); // pickup method in the grid
			
            $shipping_address = $order->getShippingAddress(); 
			
			// Copyright 2009, FedEx Corporation. All rights reserved.
			// Version 12.0.0
			//echo Mage::getModuleDir('etc', 'Mage_Usa').DS.'wsdl'.DS.'FedEx'.DS.'library';die;
			require_once(Mage::getModuleDir('etc', 'Mage_Usa').DS.'wsdl'.DS.'FedEx'.DS.'library'.DS.'fedex-common.php');

			//The WSDL is not included with the sample code.
			//Please include and reference in $path_to_wsdl variable.
			$path_to_wsdl = Mage::getModuleDir('etc', 'Mage_Usa').DS.'wsdl'.DS.'FedEx'.DS."ShipService_v15.wsdl";
			//echo  Mage::getModuleDir('etc', 'Mage_Usa').DS.'wsdl'.DS.'FedEx';die;
            //header('Content-Type: image/png');
			$path = Mage::getBaseDir('media') . DS . 'fedexlabel' . DS . "label" . DS;
			define('SHIP_LABEL', 'fedex_'.$orderIncrementId.'.png');  // PDF label file. Change to file-extension .png for creating a PNG label (e.g. shiplabel.png)
			//define('SHIP_CODLABEL', 'CODgroundreturnlabel.pdf');  // PDF label file. Change to file-extension ..png for creating a PNG label (e.g. CODgroundreturnlabel.png)

			ini_set("soap.wsdl_cache_enabled", "0");

			$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
                        
                        
                        $warehouse_id = $order->getAssignationWarehouse();
                        $warehouse_id = isset($warehouse_id) ? $warehouse_id : 1;
                        if(count(explode(',',$warehouse_id))>1){
                          $warehouse_id = 1;  
                        }
                        $warehouseData = Mage::getModel('pointofsale/pointofsale')->load($warehouse_id);
                        
                         /* Partner Shipping Accounts Logic
                         * added by Nareshsseeta
                         * */
                         
                        
                         if(!empty($order->getTradingPartner())){
							 
							  $tradingpartner =$order->getTradingPartner();
							 }else{
								  $tradingpartner ="kabloom";
								 }
                        $collection = Mage::getResourceModel('partner/partner_collection');				
						$partners = Mage::getSingleton("partner/partner")->getCollection()->addFieldToFilter('partner_name', array('eq'=>$tradingpartner));
						foreach($partners as $partner){
						$partnerId =$partner->getPartnerId();} 
						//echo 'warehouse:'.$warehouse_id;die;
						if($partnerId&&$warehouse_id){
						
							$collection = Mage::getSingleton("partner/partnershipping")->getCollection()
							->addFieldToFilter('partner_id',$partnerId)->addFieldToFilter('partner_warehouse',$warehouse_id)
							->addFieldToFilter('partner_carrier',0);			
							$partnershipping = $collection->load();
							//echo '<pre>';print_r($partnershipping->getData());
							//die;
							foreach($partnershipping as $partnershipping){
								
						$shipaccount = $partnershipping->getPartnerAccount();
                        $billaccount = $partnershipping->getPartnerAccount();
                        $meter = $partnershipping->getPartnerMeter();
                        $key = $partnershipping->getPartnerLicense();
                        $password = $partnershipping->getPartnerAccountPassword();
							
							}
						}else{
							 /* ENDPartner Shipping Accounts Logic */
						
                        $shipaccount = $warehouseData->getFedexaccount();
                        $billaccount = $warehouseData->getFedexaccount();
                        $meter = $warehouseData->getFedexmeter();
                        $key = $warehouseData->getFedexkey();
                        $password = $warehouseData->getFedexpassword();
						}
			$request['WebAuthenticationDetail'] = array(
				'UserCredential' =>array(
					'Key' => $key,//getProperty('key'), 
					'Password' => $password,//getProperty('password')
				)
			);//echo 'request :';print_r($request);die;
			$request['ClientDetail'] = array(
				'AccountNumber' => $shipaccount,//getProperty('shipaccount'), 
				'MeterNumber' => $meter,//getProperty('meter')
			);
			$request['TransactionDetail'] = array('CustomerTransactionId' => '*** Ground Domestic Shipping Request using PHP ***');
			$request['Version'] = array(
				'ServiceId' => 'ship', 
				'Major' => '15', 
				'Intermediate' => '0', 
				'Minor' => '0'
			);
			$recipient_address = array();
			$shipper_address = array();
			
			$coreResource = Mage::getSingleton('core/resource');
			$connection = $coreResource->getConnection('core_read');
                        //echo '<pre>';print_r($shipping_address->getPostcode());die;
			$select = $connection->select()->from('citylist')->where('zipcode = ?', $shipping_address->getPostcode());
			$row = $connection->fetchAll($select);
			$region_id = $row[0]['region_id'];
			//echo '$region_id :'.$region_id;die;
			
			$recipient_phoneno = '1234567890';
			if(is_numeric($shipping_address->getTelephone()) && strlen($shipping_address->getTelephone()) > 7 ){
				 $recipient_phoneno = $shipping_address->getTelephone();
			}
			
			$zipcode = substr($shipping_address->getPostcode(), 0, 5);
			$warezipcode = substr($warehouseData->getPostalCode(), 0, 5);
			$recipient_address['personname'] = $shipping_address->getFirstname().' '.$shipping_address->getLastname();
			$recipient_address['phonenumber'] = $recipient_phoneno;
			$recipient_address['streetlines'] = $shipping_address->getStreet();
			$recipient_address['city'] = $shipping_address->getCity();
			$recipient_address['stateorprovincecode'] = $region_id;
			$recipient_address['postalcode'] = $zipcode;
			$recipient_address['countrycode'] = $shipping_address->getCountryId();
			
			$shipper_address['personname'] = 'AMAZON.COM';
			$shipper_address['companyname'] = 'Amazon Fulfillment Services';
			$shipper_address['phonenumber'] = $warehouseData->getMainPhone();//'8005225666';//
			$shipper_address['streetlines'] = $warehouseData->getData('address_line_1') . ' ' .$warehouseData->getData('address_line_2');//'305 Harvard Street';//
			$shipper_address['city'] = $warehouseData->getCity();//'Massachusetts';//
			$shipper_address['stateorprovincecode'] = $warehouseData->getState();//'MA';//
			$shipper_address['postalcode'] = $warezipcode;//'02446';//
			$shipper_address['countrycode'] = $warehouseData->getCountryCode();//'US';//
			
			if(empty($shipper_address['phonenumber'])){
				$shipper_address['phonenumber'] = '8005225666';// default phone number
			}
			
			// Determine whether the ship type is ground or express(air) starts
			
			// through order information you should fetch the fedex service, which is made static as of now
			$fedex_air_methods =  array('FedEx SameDay','FedEx SameDay City','FedEx First Overnight','FedEx Priority Overnight','FedEx Standard Overnight','FedEx 2Day A.M.','FedEx 2Day','FedEx Express Saver');
			$fedex_ground_methods = array('FedEx Ground','FedEx Home Delivery','FedEx SmartPost');
			$order_shipment_service = 'FedEx Home Delivery';
			//$is_fedex_air = in_array($order_shipment_service, $fedex_air_methods);
			$is_fedex_ground = strripos($pickup_method, 'gr');//in_array($order_shipment_service, $fedex_ground_methods);
			$servicetype = 'FEDEX_GROUND'; // by default 'FEDEX_GROUND'
//			if($is_fedex_air){
//				$servicetype = 'PRIORITY_OVERNIGHT';
//			}
			// if($is_fedex_ground !== false){ //if($is_fedex_ground){
				// $servicetype = 'FEDEX_GROUND';              // for 'FedEx Ground'
			// }elseif($pickup_method == 'FEDEX_NEXT_STD'){
				// $servicetype = 'STANDARD_OVERNIGHT';        // for 'FedEx Standard Overnight
			// }elseif($pickup_method == 'FEDEX_SECOND'){
				// $servicetype = 'FEDEX_2_DAY';               // for FedEx Two Day
			// }elseif($pickup_method == 'FEDEX_NEXT_PRI'){
				// $servicetype = 'PRIORITY_OVERNIGHT';        // for FedEx Priority Overnight'
			// }else{
				// $servicetype = 'PRIORITY_OVERNIGHT';        // for others    
			// }
			
			$coreResource = Mage::getSingleton('core/resource');
			$connection = $coreResource->getConnection('core_read');
			$select = $connection->select()->from('fedex_code_map')->where('fed_method_code = ?', $pickup_method)->orWhere('fed_method_desc = ?', $pickup_method);
			//echo $select;die;
			$service_codes = $connection->fetchAll($select);
			//echo '<pre>';print_r($service_codes);die;
			if(is_array($service_codes) && count($service_codes)>0){
				foreach($service_codes as $service_code){
					$servicetype = $service_code['fed_service_type'];
				}
			}
			//echo $servicetype;die;
			// Determine whether the ship type is ground or express(air) ends
			
                        $pdflabelcontrollerObj = new Infomodus_Upslabel_Adminhtml_PdflabelsController();
                       //echo  $pickdatenew = strtotime(date('Y-m-d')); 
                        //echo $pickdatedesired = date( 'Y-m-d\TH:i:s', strtotime(date(''). ' + 2 days'));
                       
                        
			$request['RequestedShipment'] = array(
			
			
				'ShipTimestamp' => date('c'), //$pickdatedesired,//
				'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
				'ServiceType' => $servicetype, // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
				'PackagingType' => 'YOUR_PACKAGING', // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
				'Shipper' => $pdflabelcontrollerObj->addShipperAction($shipper_address),
				'Recipient' => $pdflabelcontrollerObj->addRecipientAction($recipient_address),
				'ShippingChargesPayment' => $pdflabelcontrollerObj->addShippingChargesPaymentAction($billaccount),
				'LabelSpecification' => $pdflabelcontrollerObj->addLabelSpecificationAction(), 
				//['SpecialServicesRequested']['SpecialServiceTypes'] = 'SATURDAY_DELIVERY';
				/* Thermal Label */
				/*
				'LabelSpecification' => array(
					'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
					'ImageType' => 'EPL2', // valid values DPL, EPL2, PDF, ZPLII and PNG
					'LabelStockType' => 'STOCK_4X6.75_LEADING_DOC_TAB',
					'LabelPrintingOrientation' => 'TOP_EDGE_OF_TEXT_FIRST'
				),
				*/
				'PackageCount' => 1,
				'PackageDetail' => 'INDIVIDUAL_PACKAGES',                                        
				'RequestedPackageLineItems' => array(
					'0' => $pdflabelcontrollerObj->addPackageLineItem1Action($servicetype)
				)
			);
			   
			   //echo 'before try..';die;
            try {
				//echo '<pre>';print_r($request); //die;
				if(setEndpoint('changeEndpoint')){
					$newLocation = $client->__setLocation(setEndpoint('endpoint'));
				}	
				$response = $client->processShipment($request); // FedEx web service invocation
				//echo $orderIncrementId.' '.$response->HighestSeverity;//die;
				//echo '<pre>';print_r($response);die;
				if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR'){
					$trackingNumber = $response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;
					//echo 'if path :'.$path .SHIP_LABEL;
					$fp = fopen($path .'fedex_'.$orderIncrementId.'.png', 'w');   
					fwrite($fp, ($response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image));
					fclose($fp);
				}else{
                                        // ERROR CONDITION WHEN FEDEX API IS CALLED FOR THE SPECIFIC ORDER ID
				}
				//writeToLog($client);    // Write to log file
			} catch (SoapFault $exception) {
                            
                                // EXCEPTION HANDLER WHEN ERROR COMES IN TRY BLOCK
                                
				//$this->_redirectReferer();
				//printFault($exception, $client);
			}
			// Fedex Label api call ends here
			
			return $response;
	}
}
?>
