<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Owner
 * Date: 28.12.11
 * Time: 9:38
 * To change this template use File | Settings | File Templates.
 */
class Infomodus_Upslabel_Model_Observer
{
    public function initUpslabel($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if (get_class($block) == 'Mage_Adminhtml_Block_Widget_Grid_Massaction' || get_class($block) == 'Enterprise_SalesArchive_Block_Adminhtml_Sales_Order_Grid_Massaction') {
            $type = '';
            if ($block->getRequest()->getControllerName() == 'sales_order') {
                $type = 'order';
            } else if ($block->getRequest()->getControllerName() == 'sales_shipment') {
                $type = 'shipment';
            } else if ($block->getRequest()->getControllerName() == 'sales_creditmemo') {
                $type = 'creditmemo';
            }
            if (strlen($type) > 0) {
				
                if ($type == 'order') {
                    $block->addItem('upslabel_autocreatelabel', array(
                        'label' => Mage::helper('sales')->__('1. Create Labels for Order'),// default value - Create UPS Labels for Orders 
                        'url' => Mage::app()->getStore()->getUrl('upslabel/adminhtml_autocreatelabel', array('type' => $type)),
                    ));
					// $block->addItem('fedexlabel_autocreatelabel', array(
                        // 'label' => Mage::helper('sales')->__('Fedex Label for Orders'),
                        // 'url' => Mage::app()->getStore()->getUrl('sitemap/index/createfedexlabel', array('type' => $type)),
                    // ));
                }
		  $block->addItem('upslabel_generatetrack', array(
                    'label' => Mage::helper('sales')->__('2. Send tracking number'), // default value - Print UPS Shipping Labels
                    'url' => Mage::app()->getStore()->getUrl('upslabel/adminhtml_generatetrack', array('type' => $type)),
                ));

		$block->addItem('upslabel_generateinvoice', array(
                    'label' => Mage::helper('sales')->__('3. Send invoice'), // default value - Print UPS Shipping Labels
                    'url' => Mage::app()->getStore()->getUrl('upslabel/adminhtml_generateinvoice', array('type' => $type)),
                ));
                
                $block->addItem('upslabel_pdflabels', array(
                    'label' => Mage::helper('sales')->__('Reprint Label'), // default value - Print UPS Shipping Labels
                    'url' => Mage::app()->getStore()->getUrl('upslabel/adminhtml_pdflabels', array('type' => $type)),
                ));

		 $block->addItem('upslabel_acceptorder', array(
                    'label' => Mage::helper('sales')->__('Accept Order'), // default value - Print UPS Shipping Labels
                    'url' => Mage::app()->getStore()->getUrl('upslabel/adminhtml_acceptorder', array('type' => $type)),
                ));
		$block->addItem('upslabel_regeneratepickup', array(
                    'label' => Mage::helper('sales')->__('Regenerate Pickup Details'), // default value - Print UPS Shipping Labels
                    'url' => Mage::app()->getStore()->getUrl('upslabel/adminhtml_regeneratepickup', array('type' => $type)),
                ));
                
            }
        }
    }

    public function addbutton($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_Shipment_View) {
            $block->removeButton('order_label');
            $shipment_id = $block->getShipment()->getId();
            if ($shipment_id) {
                $order_idd = $block->getShipment()->getOrderId();
                if ($order_idd) {
                    $collections = Mage::getModel('upslabel/upslabel');
                    $colls = $collections->getCollection()->addFieldToFilter('shipment_id', $shipment_id)->addFieldToFilter('type', 'shipment')->addFieldToFilter('status', 0)->getFirstItem();
                    if ($colls->getShipmentId() == $shipment_id) {
                        $block->addButton('order_label', array(
                            'label' => Mage::helper('sales')->__('UPS Label'),
                            'onclick' => 'setLocation(\'' . $block->getUrl('upslabel/adminhtml_upslabel/showlabel/order_id/' . $order_idd . '/shipment_id/' . $shipment_id . '/type/shipment') . '\')',
                            'class' => 'go'
                        ));
                    } else {
                        $block->addButton('order_label', array(
                            'label' => Mage::helper('sales')->__('UPS Label'),
                            'onclick' => 'setLocation(\'' . $block->getUrl('upslabel/adminhtml_upslabel/intermediate/order_id/' . $order_idd . '/shipment_id/' . $shipment_id . '/type/shipment') . '\')',
                            'class' => 'go'
                        ));
                    }

                    if (Mage::getModel('dhllabel/dhllabel')) {
                        $collections = Mage::getModel('dhllabel/dhllabel');
                        $collection = $collections->getCollection()->addFieldToFilter('shipment_id', $shipment_id)->addFieldToFilter('type', 'shipment')->addFieldToFilter('status', 0)->getFirstItem();
                        if ($collection->getShipmentId() == $shipment_id) {
                            $block->addButton('order_label_dhl', array(
                                'label' => Mage::helper('sales')->__('Dhl Label'),
                                'onclick' => 'setLocation(\'' . $block->getUrl('dhllabel/adminhtml_dhllabel/showlabel/order_id/' . $order_idd . '/shipment_id/' . $shipment_id . '/type/shipment') . '\')',
                                'class' => 'go'
                            ));
                        } else {
                            $block->addButton('order_label_dhl', array(
                                'label' => Mage::helper('sales')->__('Dhl Label'),
                                'onclick' => 'setLocation(\'' . $block->getUrl('dhllabel/adminhtml_dhllabel/intermediate/order_id/' . $order_idd . '/shipment_id/' . $shipment_id . '/type/shipment') . '\')',
                                'class' => 'go'
                            ));
                        }
                    }
                }
            }
        }

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_Creditmemo_View) {
            $block->removeButton('cancel');
            $shipment_id = $block->getCreditmemo()->getId();
            $order_idd = $block->getCreditmemo()->getOrderId();
            if ($shipment_id) {
                $collections = Mage::getModel('upslabel/upslabel');
                $colls = $collections->getCollection()->addFieldToFilter('shipment_id', $shipment_id)->addFieldToFilter('type', 'refund')->addFieldToFilter('status', 0)->getFirstItem();
                if ($colls->getShipmentId() != $shipment_id) {
                    $block->addButton('cancel', array(
                            'label' => Mage::helper('sales')->__('Ups label'),
                            'class' => 'save',
                            'onclick' => 'setLocation(\'' . $block->getUrl('upslabel/adminhtml_upslabel/intermediate/order_id/' . $order_idd . '/shipment_id/' . $shipment_id . '/type/refund') . '\')'
                        )
                    );
                } else {
                    $block->addButton('cancel', array(
                            'label' => Mage::helper('sales')->__('Ups label'),
                            'class' => 'save',
                            'onclick' => 'setLocation(\'' . $block->getUrl('upslabel/adminhtml_upslabel/showlabel/order_id/' . $order_idd . '/shipment_id/' . $shipment_id . '/type/refund') . '\')'
                        )
                    );
                }

                if (Mage::getModel('dhllabel/dhllabel')) {
                    $collections = Mage::getModel('dhllabel/dhllabel');
                    $collection = $collections->getCollection()->addFieldToFilter('shipment_id', $shipment_id)->addFieldToFilter('type', 'refund')->addFieldToFilter('status', 0)->getFirstItem();
                    if ($collection->getShipmentId() != $shipment_id) {
                        $block->addButton('cancel_dhl', array(
                                'label' => Mage::helper('sales')->__('Dhl label'),
                                'class' => 'save',
                                'onclick' => 'setLocation(\'' . $block->getUrl('dhllabel/adminhtml_dhllabel/intermediate/order_id/' . $order_idd . '/shipment_id/' . $shipment_id . '/type/refund') . '\')'
                            )
                        );
                    } else {
                        $block->addButton('cancel_dhl', array(
                                'label' => Mage::helper('sales')->__('Dhl label'),
                                'class' => 'save',
                                'onclick' => 'setLocation(\'' . $block->getUrl('dhllabel/adminhtml_dhllabel/showlabel/order_id/' . $order_idd . '/shipment_id/' . $shipment_id . '/type/refund') . '\')'
                            )
                        );
                    }
                }
            }
        }

        if($block instanceof Mage_Adminhtml_Block_Sales_Order_Grid && Mage::getStoreConfig('upslabel/additional_settings/order_grid_column_enable') == 1) {
            $block->addColumnAfter('statuslabel', array(
                'header' => Mage::helper('upslabel')->__('Label status'), // default value - UPS label status
                'index' => 'statuslabel',
                'type'  => 'options',
                'width' => '120px',
                'sortable' => false,
                'frame_callback' => array($this, 'callback_upsstatus'),
                'filter_condition_callback' => array($this, '_orderUpsStatusFilter'),
                'options' => Mage::getModel('upslabel/config_statuslabels')->getStatus(),
            ), 'status');
        }

        if($block instanceof Mage_Adminhtml_Block_Sales_Shipment_Grid && Mage::getStoreConfig('upslabel/additional_settings/shipment_grid_column_enable') == 1) {
            $block->addColumnAfter('upsprice', array(
                'header' => Mage::helper('adminhtml')->__('Price (Ups)'),
                'index' => 'upsprice',
                'type'  => 'options',
                'width' => '100px',
                'sortable' => false,
                'frame_callback' => array($this, 'callback_upsprice'),
                'filter_condition_callback' => array($this, '_ShipUpsStatusFilter'),
                'options' => Mage::getModel('upslabel/config_statuslabels')->getStatus(),
            ), "total_qty");
        }

        if($block instanceof Mage_Adminhtml_Block_Sales_Creditmemo_Grid && Mage::getStoreConfig('upslabel/additional_settings/credit_grid_column_enable') == 1) {
            $block->addColumnAfter('upsprice', array(
                'header' => Mage::helper('adminhtml')->__('Price (Ups)'),
                'index' => 'upsprice',
                'type'  => 'options',
                'width' => '100px',
                'sortable' => false,
                'frame_callback' => array($this, 'callback_upspricecredit'),
                'filter_condition_callback' => array($this, '_CreditUpsStatusFilter'),
                'options' => Mage::getModel('upslabel/config_statuslabels')->getStatus(),
            ), "state");
        }
    }

    public function callback_upsstatus($value, $row, $column, $isExport)
    {
        $collections = Mage::getModel('upslabel/upslabel');
        $item = $collections->getCollection()->addFieldToFilter('order_id', $row->getId())->addFieldToFilter('type', 'shipment')->getFirstItem();
        return $item->getStatustext();
    }

    public function _ShipUpsStatusFilter($collection, $column)
    {
        $this->_orderUpsStatusFilter($collection, $column, $type="shipment", $id="shipment_id");
    }

    public function _CreditUpsStatusFilter($collection, $column)
    {
        $this->_orderUpsStatusFilter($collection, $column, $type="refund", $id="shipment_id");
    }

    public function _orderUpsStatusFilter($collection, $column, $type="shipment", $id="order_id")
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $status = 0;
        $is_need_filter = true;
        switch($value){
            case "success": $statustext = '="Successfully"'; $status = 0;  break;
            case "error": $statustext = '!="Successfully"'; $status = 1; break;
            case "notcreated": $is_need_filter = false; break;
        }
        if($is_need_filter == true){
            $collection->getSelect()->distinct(true)->join(array("t123" => Mage::getConfig()->getTablePrefix() . 'upslabel'), 't123.'.$id.' = main_table.entity_id AND t123.type="'.$type.'" AND t123.status="'.$status.'" AND t123.statustext'.$statustext, NULL);
            //$query = $collection->getSelect();
            //print_r(get_class_methods(get_class($collection->getSelect())));
            $collection->getSelect()->setPart('where', str_replace(array("(status ", " status "), array('(main_table.status ', ' main_table.status '), $collection->getSelect()->getPart('where')));
            //echo $query; exit;
        }
        else {
            $collection->getSelect()->distinct(true)->joinLeft(array("t123" => Mage::getConfig()->getTablePrefix() . 'upslabel'), 'main_table.entity_id = t123.'.$id.' AND t123.type="'.$type.'"', NULL)->where("t123.".$id." IS NULL");
            $collection->getSelect()->setPart('where', str_replace(array("(status ", " status "), array('(main_table.status ', ' main_table.status '), $collection->getSelect()->getPart('where')));
             //$query = $collection->getSelect();
            //echo $query; exit;
        }
        return $this;
    }

    public function callback_upsprice($value, $row, $column, $isExport, $type="shipment")
    {
        $c = '';
        $items = array();
        $collections = Mage::getModel('upslabel/labelprice');
        $items = $collections->getCollection()->addFieldToFilter('shipment_id', $row->getId())->addFieldToFilter('type', $type);
        if (count($items) > 0) {
            foreach ($items AS $item) {
                $c .= $item->getPrice() . "<br>";
            }

            return '<div style=" padding-left: 5px;">' . $c . '</div>';
        } else {
            return "";
        }
    }
    public function callback_upspricecredit($value, $row, $column, $isExport)
    {
        return $this->callback_upsprice($value, $row, $column, $isExport, "refund");
    }
}
