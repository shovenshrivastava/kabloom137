<?php
class Creare_DynamicSitemap_IndexController extends Mage_Core_Controller_Front_Action
{
	
	protected function _prepareLayout() {
		
		if ($headBlock = $this->getLayout()->getBlock('head')) {
			$headBlock->setTitle($title);
		}
		return parent::_prepareLayout();			
	}
	
    public function IndexAction() {
      
	  $this->loadLayout();   
      $this->renderLayout(); 
	  
    }
	
	public function zipcode_calender_logicAction() {
		
		$recipient_zipcode = $_POST['value'];
		//$allwarehouse_zipcodes = array('02446','02445');
		
		date_default_timezone_set('America/New_York');
		 $storeCode = Mage::app()->getStore()->getCode();
		$collection = Mage::getModel('pointofsale/pointofsale')->getCollection();
		//echo 'delivery_date :'.$delivery_date;die;
       // print_r($collection->getData());die;
	   $zip_found = false;
        foreach ($collection->getData() as $data) { // check for local delivery warehouse
            //$x = array_search($recipient_zipcode, explode(',', $data['inventory_assignation_rules']));
            $x = in_array($recipient_zipcode, explode(',', $data['inventory_assignation_rules']));
			//if($data['enablelocaldeli']){	// check if the local delivery is allowed for this warehouse
				if ($x !== false) {
					$zip_found = true;
					$warehouse_timings = json_decode($data['hours'],true);
					$today_day = date('l');
					$warehouse_closing_time = explode(':',$warehouse_timings[$today_day]['from']); // 'from' in warehouse timings refers to Local delivery closing time
					$warehouse_shipping_closing_time = explode(':',$warehouse_timings[$today_day]['to']);// 'to' in warehouse timings refers to shipping  closing time
				}
            //}
			
        }
		
		if(!$zip_found){
			foreach ($collection->getData() as $data) { // check for shipping enabled warehouse
				$warehouse_timings_tmp = json_decode($data['hours'],true);
				$today_day_tmp = date('l');
				if (array_key_exists($today_day_tmp, $warehouse_timings_tmp)) {
					$warehouse_shipping_closing_time_tmp = explode(':',$warehouse_timings_tmp[$today_day_tmp]['to']);// 'to' in warehouse timings refers to shipping  closing time
					$warehouse_closing_time_in_minutes_tmp = ($warehouse_shipping_closing_time_tmp[0] * 60) + $warehouse_shipping_closing_time_tmp[1];
					$present_time_in_minutes = (date('H') * 60) + date('i');
					if($present_time_in_minutes < $warehouse_closing_time_in_minutes_tmp){
						$zip_found = true;
						$warehouse_timings = json_decode($data['hours'],true);
						$today_day = date('l');
						$warehouse_closing_time = explode(':',$warehouse_timings[$today_day]['from']); // 'from' in warehouse timings refers to Local delivery closing time
						$warehouse_shipping_closing_time = explode(':',$warehouse_timings[$today_day]['to']);// 'to' in warehouse timings refers to shipping  closing time
					}
				}
			}
		}
		
		if(!$zip_found){
			if($storeCode != 'bkstore'){
				//$warehouse_hours = $collection->addFieldToFilter('name','Brookline')->getFirstItem()->getHours();
				//$warehouse_timings = json_decode($warehouse_hours,true);
				$today_day = date('l');
				//$warehouse_closing_time = explode(':',$warehouse_timings[$today_day]['to']);// 'to' in warehouse timings refers to shipping  closing time
				$warehouse_closing_time = array(0,0); // this is req, even if no warehouse matches. so setting this value to 0
			}else{
				$warehouse_hours = $collection->addFieldToFilter('name','Bogota')->getFirstItem()->getHours();
				$warehouse_timings = json_decode($warehouse_hours,true);
				$today_day = date('l');
				$warehouse_closing_time = explode(':',$warehouse_timings[$today_day]['to']);// 'to' in warehouse timings refers to shipping  closing time
				}
			//print_r($da);die;
		}
		
		$present_time_in_minutes = (date('H') * 60) + date('i');
		$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
	
		
		//$current_hour = date('H', time());
		
		//$calender_config = explode(':',Mage::getStoreConfig('catalog/calenderconfig/list_mode'));
		//$calender_hours_condition = $calender_config[0]; // the hours condition which is set from the backend, System/configuration/catalog/calender config/calender time
		
		// if(empty($warehouse_closing_time_in_minutes) || $warehouse_closing_time_in_minutes ==0){
			// $warehouse_closing_time_in_minutes = 900;
		// }
		
		// condition-1 : enable today's date, which is there by default
		// condition-2 : only today's date block
		// condition-3 : today block and tomorrow onwards enable
		// condition-4 : only today,tomorrow block 
		// condition-5 : only today,tomorrow and day after tomorrow block
		
		if (is_numeric($recipient_zipcode)) {
			// code to get saturday enable of disable flag
			$saturdayEn = Mage::getModel('citylist/citylist')->getCollection()->addFieldToFilter('zipcode', $recipient_zipcode);
			foreach ($saturdayEn->getData() as $saturdayEnable) {
			$saturdayUPSEnable = $saturdayEnable['ups_saturday'];
			$saturdayFedexEnable = $saturdayEnable['fedex_saturday'];
			if($saturdayFedexEnable==2 || $saturdayUPSEnable==2){
			echo $satEnable="Yes";
			}else{
			echo $satEnable="No";
			}
			}
			echo $satEnable="&";
			//end of code to get saturday enable of disable flag
			//if(in_array($recipient_zipcode,$allwarehouse_zipcodes)){
			if($storeCode != 'bkstore'){
				if($zip_found){ // local delivery
					if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ // warehouse open for LD
						if($today_day =='Sunday'){
							echo "condition-2";
						}else{
							echo "condition-1";
						}
					}else{ // warehouse close for LD
						$warehouse_closing_time_in_minutes = ($warehouse_shipping_closing_time[0] * 60) + $warehouse_shipping_closing_time[1];
						if($today_day =='Saturday'){
							if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
								echo "condition-4";
							}else{
								echo "condition-5";
							}
						}elseif($today_day =='Sunday'){
							if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
								echo "condition-4";
							}else{
								echo "condition-4";
							}
						}else{
							if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
								echo "condition-3";
							}else{
								echo "condition-4";
							}
						}
					}
				}else{ // Non local delivery
					if($today_day =='Saturday'){
						if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
							echo "condition-4";
						}else{
							echo "condition-5";
						}
					}elseif($today_day =='Sunday'){
						if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
							echo "condition-4";
						}else{
							echo "condition-4";
						}
					}else{
						if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
							echo "condition-3";
						}else{
							echo "condition-4";
						}
					}
				}
				
			}else{ // for bloomkonnect
				if($today_day =='Friday'){
					if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
						echo "condition-6";
					}else{
						echo "condition-6";
					}
				}elseif($today_day =='Saturday'){
					if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
						echo "condition-5";
					}else{
						echo "condition-6";
					}
				}elseif($today_day =='Sunday'){
					if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
						echo "condition-5";
					}else{
						echo "condition-5";
					}
				}else{
					if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
						echo "condition-4";//print_r($warehouse_closing_time);
					}else{
						echo "condition-5";
					}
				}
            }

		}else{
			echo "condition-0";
		}
    }
}