<?php
    class Ht_Shipdate_Block_Shipdate extends Mage_Core_Block_Template
    {
        public function _prepareLayout()
        {
            return parent::_prepareLayout();
        }

        public function getShipdate()     
        { 
            if (!$this->hasData('shipdate')) {
                $this->setData('shipdate', Mage::registry('shipdate'));
            }
            return $this->getData('shipdate');

        }
        public function getDateFormat()
        {       
            return Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT) . ' ' . Mage::app()->getLocale()->getTimeStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        }
}