<?php
class Ht_Shipdate_Block_Adminhtml_Shipdate extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_shipdate';
    $this->_blockGroup = 'shipdate';
    $this->_headerText = Mage::helper('shipdate')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('shipdate')->__('Add Item');
    parent::__construct();
  }
}