<?php

class Ht_Shipdate_Block_Adminhtml_Shipdate_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('shipdate_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('shipdate')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('shipdate')->__('Item Information'),
          'title'     => Mage::helper('shipdate')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('shipdate/adminhtml_shipdate_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}