<?php

class Ht_Shipdate_Block_Adminhtml_Shipdate_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'shipdate';
        $this->_controller = 'adminhtml_shipdate';
        
        $this->_updateButton('save', 'label', Mage::helper('shipdate')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('shipdate')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('shipdate_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'shipdate_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'shipdate_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('shipdate_data') && Mage::registry('shipdate_data')->getId() ) {
            return Mage::helper('shipdatedate')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('shipdate_data')->getTitle()));
        } else {
            return Mage::helper('shipdatedate')->__('Add Item');
        }
    }
}