<?php
    /**
    * CustomSearch Observer
    *
    * @category    Exxex
    * @package     Essex_Customermod
    * @author      Sharpdotinc.com
    */
    class Ht_Shipdate_Model_Observer
    {				

        public function checkout_controller_onepage_save_shipping_method($observer)
        {
            if (Mage::getStoreConfig('shipdate/shipdate_general/on_which_page')==1){
                $request = $observer->getEvent()->getRequest();
                $quote =  $observer->getEvent()->getQuote();

                $desiredShipDate = Mage::helper('shipdate')->getFormatedShipDateToSave($request->getPost('ship_date', ''));
                $quote->setShipDate($desiredShipDate);
                $quote->save();
            }

            return $this;
        }
        

}