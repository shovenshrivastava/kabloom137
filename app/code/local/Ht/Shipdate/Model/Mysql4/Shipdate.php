<?php

class Ht_Shipdate_Model_Mysql4_Shipdate extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the deliverydate_id refers to the key field in your database table.
        $this->_init('shipdate/shipdate', 'shipdate_id');
    }
}