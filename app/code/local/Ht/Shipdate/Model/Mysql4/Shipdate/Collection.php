<?php

class Ht_Shipdate_Model_Mysql4_Shipdate_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('shipdate/shipdate');
    }
}