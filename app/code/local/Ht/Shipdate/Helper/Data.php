<?php

    class Ht_Shipdate_Helper_Data extends Mage_Core_Helper_Abstract
    {
        public function getFormatedShipDate($date = null)
        {
            //if null or 0-0-0 00:00:00 return no date string
            if(empty($date) ||$date == null || $date == '0000-00-00 00:00:00'){
                return Mage::helper('shipdate')->__("No Ship Date Specified.");
            }

            //Format Date
            $formatedDate = Mage::helper('core')->formatDate($date, 'medium');
            //TODO: check that date is valid before passing it back

            return $formatedDate; 
        }

        public function getFormatedShipDateToSave($date = null)
        {
            if(empty($date) ||$date == null || $date == '0000-00-00 00:00:00'){
                return null;
            }

            $timestamp = null;
            try{
                //TODO: add Better Date Validation
                $timestamp = strtotime($date);
                $dateArray = explode("-", $date);
                if(count($dateArray) != 3){
                    //invalid date
                    return null;
                }
                //die($timestamp."<<");
                //$formatedDate = date('Y-m-d H:i:s', strtotime($timestamp));
                //$formatedDate = date('Y-m-d H:i:s', mktime(0, 0, 0, $dateArray[0], $dateArray[1], $dateArray[2]));
                $formatedDate = date('Y-m-d H:i:s',strtotime($date));
            } catch(Exception $e){
                //TODO: email error 
                //return null if not converted ok
                return null;
            }                

            return $formatedDate;         
        }
        public function saveShipDate($observer){

            $order = $observer->getEvent()->getOrder();
            if (Mage::getStoreConfig('shipdate/shipdate_general/on_which_page')==2){
                $desiredArrivalDate = Mage::helper('shipdate')->getFormatedDeliveryDateToSave(Mage::app()->getRequest()->getParam('ship_date'));
                $order->setShippingArrivalDate($desiredArrivalDate);
            }else{
                $cart = Mage::getModel('checkout/cart')->getQuote()->getData();
                $desiredShipDate = Mage::helper('shipdate')->getFormatedShipDateToSave($cart['ship_date']);
                $order->setShipDate($desiredShipDate);
            }
        }
        public function saveShipDateAdmin($observer){
          
            $order = $observer->getEvent()->getOrder();
            $cart = Mage::app()->getRequest()->getParams();
            $desiredShipDate = Mage::helper('shipdate')->getFormatedShipDateToSave($cart['shipp_date_display']);
            $order->setShipDate($desiredArrivalDate);

        }

}