<?php
class Ht_Shipmentfilter_Model_Shipping extends Mage_Shipping_Model_Shipping
{  
    public function collectCarrierRates($carrierCode, $request)
    {   //Mage::log($carrierCode, null, 'mylogfile.log');
        if (!$this->_checkCarrierAvailability($carrierCode, $request)) {
            return $this;
        }
        return parent::collectCarrierRates($carrierCode, $request);
    }
 
    protected function _checkCarrierAvailability($carrierCode, $request = null)
    {
	
		date_default_timezone_set('America/New_York'); 
	   //date_default_timezone_set('Asia/Kolkata'); 
	   
	   
       //hide ups rates from front end side
       if($carrierCode == 'ups' || $carrierCode == 'fedex'){ 
			// Checking for admin session
			Mage::getSingleton('core/session', array('name'=>'adminhtml') ); 
			$adminsession = Mage::getSingleton('admin/session', array('name'=>'adminhtml'));
			if($adminsession->isLoggedIn()) {
				 return true; 
			}else{
			   return false;
			}
		}
        // get delivery date and zipcode from product page
		$read= Mage::getSingleton('core/resource')->getConnection('core_read');
		$quote = Mage::getSingleton('checkout/session')->getQuote();
		$cartCount = Mage::helper('checkout/cart')->getCart()->getItemsCount();
		$_cart = Mage::getSingleton('checkout/cart');
		$cartItems = $quote->getAllVisibleItems();
		$i=1;
		foreach($_cart->getItems() as $item){//start for each product
			if($i==1){
				$ptype = $item->getData('product_type');
				$pId = $item->getData('product_id'); 
				$itemId = $item->getData('item_id'); 
				$first_item[] = $item;
				if($ptype == "configurable"){
					$resultC=$read->query("SELECT * FROM sales_flat_quote_item WHERE parent_item_id = '".$itemId."'");
					$rowP = $resultC->fetch();
					$zip = $rowP['cities'];
					$dDate = $rowP['shipping'];
				}else{
					$zip = $item->getData('cities'); 
					$dDate = $item->getData('shipping'); 
				}
			}
			$i++;
		} 
		$timestampdDate = strtotime($dDate);
		$daydDate = date('l', $timestampdDate); 
       // cut off time logic
	   
	   // shipping rates for rulala category products starts here
	   $session = Mage::getSingleton('checkout/session');

        foreach ($session->getQuote()->getAllVisibleItems() as $item) {
			$_product = Mage::getModel('catalog/product')->load($item->getProductId());
			$cartProductCatId = $_product->getCategoryIds();
			//print_r($cartProductCatId);
        }
     
//print_r($cartProductCatId);

        if (in_array('235', $cartProductCatId) || in_array('234', $cartProductCatId)) {
            $rg = '1';
        }

        if ($rg == 1) {
             // echo "say rulala";
			 
			// new logic following four conditions starts here
	   /*
			condition 1 - if the delivery date, which is not saturday, is today or tomorrow  				- ship price = 19.99
			condition 2 - if the delivery date, which is not saturday, is Day after tomorrow/Future Date    - ship price = 12.99
			condition 3 - if the delivery date, which is saturday, is today or tomorrow    					- ship price = 24.99
			condition 4 - if the delivery date, which is saturday, is Day after tomorrow/Future Date    	- ship price = 19.99
	   */
	   
	   
	   
	   // for finding current date, if the time falls below closing time its today else it is tommorow starts here
	   $collection = Mage::getModel('pointofsale/pointofsale')->getCollection();
		//echo 'delivery_date :'.$delivery_date;die;
       // print_r($collection->getData());die;
	   $zip_found = false;
        foreach ($collection->getData() as $data) {
            //$x = array_search($recipient_zipcode, explode(',', $data['inventory_assignation_rules']));
            $x = in_array($recipient_zipcode, explode(',', $data['inventory_assignation_rules']));
			if($data['enablelocaldeli']){	// check if the local delivery is allowed for this warehouse
				if ($x !== false) {
					$zip_found = true;
					$warehouse_timings = json_decode($data['hours'],true);
					$today_day = date('l');
					$warehouse_closing_time = explode(':',$warehouse_timings[$today_day]['to']);
				}
            }			
        }
		
		if(!$zip_found){
			$warehouse_hours = $collection->addFieldToFilter('name','Brookline')->getFirstItem()->getHours();
			$warehouse_timings = json_decode($warehouse_hours,true);
			$today_day = date('l');
			$warehouse_closing_time = explode(':',$warehouse_timings[$today_day]['to']);
		}
		
		$present_time_in_minutes = (date('H') * 60) + date('i');
		$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
		//echo '$present_time_in_minutes :'.$present_time_in_minutes.'$warehouse_closing_time_in_minutes :'.$warehouse_closing_time_in_minutes;die;
		if(empty($warehouse_closing_time_in_minutes) || $warehouse_closing_time_in_minutes ==0){
			$warehouse_closing_time_in_minutes = 900;
		}
		if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
			if((date('l') == 'Saturday' && date('l', $timestampdDate) == 'Monday') || (date('l') == 'Sunday' && date('l', $timestampdDate) == 'Tuesday')){
				$currentdate = strtotime(date('Y-m-d H:i:s',mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))));
			}else{
				$currentdate = strtotime(date("Y-m-d"));
			}
		}else{
			//$currentdate = strtotime(date('Y-m-d H:i:s',mktime(date("H"), date("i"), date("s"), date("m")  , date("d")+1, date("Y"))));
			if((date('l') == 'Friday' && date('l', $timestampdDate) == 'Monday') || (date('l') == 'Saturday' && date('l', $timestampdDate) == 'Tuesday')){
				$currentdate = strtotime(date('Y-m-d H:i:s',mktime(0, 0, 0, date("m")  , date("d")+2, date("Y"))));
			}else{
				$currentdate = strtotime(date('Y-m-d H:i:s',mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))));
			}
		}
	   // for finding current date, if the time falls below closing time its today else it is tommorow ends here
	   
	   //$currentdate = strtotime(date("Y-m-d"));
	   //$currentdate = strtotime('2015-03-07');
	   
	   $deliverydate = strtotime(date('Y-m-d', $timestampdDate));
	   $datediff = $deliverydate - $currentdate;
	   $differance = floor($datediff / (60 * 60 * 24));
	  // echo 'diff:'.$differance;die;
	   if($daydDate!='Saturday'){
			if (($differance == 0 || $differance == 1) && $carrierCode== 'flatrate') { // for condition 1
				//echo 'today or tomorrow';
				return true;
			}elseif ($differance > 1 && $carrierCode == 'flatrate5') {				// for condition 2
				//echo 'Day after tomorrow/Future Date';
				return true;
			} else {
				//echo 'past date'; 
				return false;
			}
	   }elseif($daydDate=='Saturday'){
			if (($differance == 0 || $differance == 1) && $carrierCode== 'flatrate3') {// for condition 3
				//echo 'today or tomorrow';
				return true;
			}elseif ($differance > 1 && $carrierCode == 'flatrate4') {					// for condition 4
				//echo 'Day after tomorrow/Future Date';
				return true;
			} else {
				//echo 'past date'; 
				return false;
			}
	   }else{
			return false;
	   }
	   // new logic following four conditions ends here 
			 
            // if (($carrierCode == 'flatrate4' || $carrierCode == 'flatrate5') && $daydDate != 'Saturday') {
                // return true;
            // }
             // if (($carrierCode == 'flatrate3' || $carrierCode == 'flatrate4') && $daydDate == 'Saturday') {
                // return true;
            // }
            // shipping rates for rulala category products ends here 
        }else{ 
	   // new logic following four conditions starts here
	   /*
			condition 1 - if the delivery date, which is not saturday, is today or tomorrow  				- ship price = 19.99
			condition 2 - if the delivery date, which is not saturday, is Day after tomorrow/Future Date    - ship price = 0
			condition 3 - if the delivery date, which is saturday, is today or tomorrow    					- ship price = 24.99
			condition 4 - if the delivery date, which is saturday, is Day after tomorrow/Future Date    	- ship price = 9.99
	   */
	   
	   
	   
	   // for finding current date, if the time falls below closing time its today else it is tommorow starts here
	   $collection = Mage::getModel('pointofsale/pointofsale')->getCollection();
		//echo 'delivery_date :'.$delivery_date;die;
       // print_r($collection->getData());die;
	   $zip_found = false;
        foreach ($collection->getData() as $data) {
            //$x = array_search($recipient_zipcode, explode(',', $data['inventory_assignation_rules']));
            $x = in_array($recipient_zipcode, explode(',', $data['inventory_assignation_rules']));
			if($data['enablelocaldeli']){	// check if the local delivery is allowed for this warehouse
				if ($x !== false) {
					$zip_found = true;
					$warehouse_timings = json_decode($data['hours'],true);
					$today_day = date('l');
					$warehouse_closing_time = explode(':',$warehouse_timings[$today_day]['to']);
				}
            }			
        }
		
		if(!$zip_found){
			$warehouse_hours = $collection->addFieldToFilter('name','Brookline')->getFirstItem()->getHours();
			$warehouse_timings = json_decode($warehouse_hours,true);
			$today_day = date('l');
			$warehouse_closing_time = explode(':',$warehouse_timings[$today_day]['to']);
		}
		
		$present_time_in_minutes = (date('H') * 60) + date('i');
		$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
		//echo '$present_time_in_minutes :'.$present_time_in_minutes.'$warehouse_closing_time_in_minutes :'.$warehouse_closing_time_in_minutes;die;
		if(empty($warehouse_closing_time_in_minutes) || $warehouse_closing_time_in_minutes ==0){
			$warehouse_closing_time_in_minutes = 900;
		}
		if($present_time_in_minutes < $warehouse_closing_time_in_minutes){ 
			if((date('l') == 'Saturday' && date('l', $timestampdDate) == 'Monday') || (date('l') == 'Sunday' && date('l', $timestampdDate) == 'Tuesday')){
				$currentdate = strtotime(date('Y-m-d H:i:s',mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))));
			}else{
				$currentdate = strtotime(date("Y-m-d"));
			}
		}else{
			//$currentdate = strtotime(date('Y-m-d H:i:s',mktime(date("H"), date("i"), date("s"), date("m")  , date("d")+1, date("Y"))));
			if((date('l') == 'Friday' && date('l', $timestampdDate) == 'Monday') || (date('l') == 'Saturday' && date('l', $timestampdDate) == 'Tuesday')){
				$currentdate = strtotime(date('Y-m-d H:i:s',mktime(0, 0, 0, date("m")  , date("d")+2, date("Y"))));
			}else{
				$currentdate = strtotime(date('Y-m-d H:i:s',mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))));
			}
		}
	   // for finding current date, if the time falls below closing time its today else it is tommorow ends here
	   
	   //$currentdate = strtotime(date("Y-m-d"));
	   //$currentdate = strtotime('2015-03-07');
	   
	   $deliverydate = strtotime(date('Y-m-d', $timestampdDate));
	   $datediff = $deliverydate - $currentdate;
	   $differance = floor($datediff / (60 * 60 * 24));
	  // echo 'diff:'.$differance;die;
	   if($daydDate!='Saturday'){
			if (($differance == 0 || $differance == 1) && $carrierCode== 'flatrate') { // for condition 1
				//echo 'today or tomorrow';
				return true;
			}elseif ($differance > 1 && $carrierCode == 'freeshipping') {				// for condition 2
				//echo 'Day after tomorrow/Future Date';
				return true;
			} else {
				//echo 'past date'; 
				return false;
			}
	   }elseif($daydDate=='Saturday'){
			if (($differance == 0 || $differance == 1) && $carrierCode== 'flatrate3') {// for condition 3
				//echo 'today or tomorrow';
				return true;
			}elseif ($differance > 1 && $carrierCode == 'flatrate2') {					// for condition 4
				//echo 'Day after tomorrow/Future Date';
				return true;
			} else {
				//echo 'past date'; 
				return false;
			}
	   }else{
			return false;
	   }
	   // new logic following four conditions ends here
	   }	   
			// if(($carrierCode== 'flatrate' || $carrierCode == 'freeshipping') && $daydDate!='Saturday'){
	// return true;
			// }
               //echo "current time";
               // $transitdays=1;
				//$hour = Mage::getModel('core/date')->date('G');
                //$dayoftoday = Mage::getModel('core/date')->date('D'); // D for Sat and l for Saturday
                //$holidays_options = Mage::getStoreConfig('shipdate/shipdate_general/holidays_options'); 
                //$holidays = split(';', $holidays_options);
               // $shipdate_dayoff = Mage::getStoreConfig('shipdate/shipdate_general/shipdate_dayoff');
                //echo "next date";
                //$ship_date = date('m/d/Y', strtotime($date .' +'.$transitdays .'day'));
				//$next_date = date('m/d/Y', strtotime($date .' +1 day'));
                //echo "cut offtime";
				//$cutofftime=Mage::getStoreConfig('shipdate/shipdate_general/cutofftime'); 
                
					//if(($dDate == $next_date && $hour > $cutofftime-1)){
					  //echo "flase for freeshipping cut off";
					//$statusofrate=0;
					//}     
	       // if(($carrierCode == 'flatrate2' || $carrierCode == 'flatrate3') && $daydDate=='Saturday'){
				// return true;
			// }else{
					// return false;
                // }
}
}