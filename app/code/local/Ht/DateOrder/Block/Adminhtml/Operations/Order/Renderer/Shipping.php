<?php
 
/**
 * Adminhtml operations orders grid renderer
 *
 * @category   Inchoo
 * @package    Inchoo_DateOrder
 */
class Ht_DateOrder_Block_Adminhtml_Operations_Order_Renderer_Shipping
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        //load first item of the order
        $orderItem = Mage::getResourceModel('sales/order_item_collection')
                        ->addFieldToFilter('order_id', $row->getId())
                        ->getFirstItem()
                        ;
 
        $orderItemOptions = $orderItem->getProductOptions();
 
        //if product doesn't have options stop with rendering
        if (!array_key_exists('options', $orderItemOptions)) {
            return;
        }
 
        $orderItemOptions = $orderItemOptions['options'];
 
        //if product options isn't array stop with rendering
        if (!is_array($orderItemOptions)) {
            return;
        }
 
        foreach ( $orderItemOptions as $orderItemOption) {
             $transitdays=1;
            if ($orderItemOption['label'] === 'Delivery Date') {
                if (array_key_exists('value', $orderItemOption)) {
                $dDate = $orderItemOption['value'];

//echo "transit before:". $transitdays;
				$holidays_options = Mage::getStoreConfig('shipdate/shipdate_general/holidays_options'); 
                $holidays = split(';', $holidays_options);
                $k=0;
                for($t=$transitdays; $t>=0; $t--){
					foreach($holidays as $holiday){
						if($holiday==date('m/d/Y', strtotime($dDate .' -'.$t .'day'))){
						$k++;
						}              
					}
                }
              $transitdays= $transitdays+$k;
               // logic for off days
				$ship_date_days=array();
				for($da=0; $da<=$transitdays; $da++){ 
               $ship_date_days[] = date('l', strtotime($dDate .' -'.$da.'day'));
				}
				//print_r($ship_date_days);
                $shipdate_dayoff_option = Mage::getStoreConfig('shipdate/shipdate_general/shipdate_dayoff');
				$shipdate_dayoff = split(',', $shipdate_dayoff_option);
                $totaldayoff=sizeof($shipdate_dayoff); 
				$d=0;
                foreach($shipdate_dayoff as $dayoff){
					switch ($dayoff){ 
					case 0 : $dayoff="Sunday"; break;
					case 1 : $dayoff="Monday"; break;
					case 2 : $dayoff="Tuesday"; break;
					case 3 : $dayoff="Wednesday"; break;
					case 4 : $dayoff="Thursday"; break;
					case 5 : $dayoff="Friday"; break;
					case 6 : $dayoff="Saturday";
					}   
					foreach($ship_date_days as $ship_date_day){
				   	if($ship_date_day==$dayoff && $dayoff=='Sunday' ){
 						$d=$d+1;
					}
					}
				}
				//echo "da".$d;
				 $transitdays=$transitdays+$d; 
 				// end of logic for off days

$ship_date_view = date('M d\, Y', strtotime($dDate .' -'.$transitdays.'day'));


                    //return date('M d\, Y', $dDate);
                return $ship_date_view;
				}
            }
 
        }
 
        //if product options doesn't have Delivery Date custom option return void
        return;
    }
}