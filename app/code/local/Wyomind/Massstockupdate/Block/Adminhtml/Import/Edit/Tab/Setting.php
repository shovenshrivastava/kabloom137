<?php

class Wyomind_Massstockupdate_Block_Adminhtml_Import_Edit_Tab_Setting extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $model = Mage::getModel('massstockupdate/import');
        $model->load($this->getRequest()->getParam('profile_id'));
        $this->setForm($form);

        $fieldset = $form->addFieldset('massstockupdate_form', array('legend' => $this->__('Profile Setting')));

        (isset($_GET['debug'])) ? $type = 'text' : $type = 'hidden';

        if ($this->getRequest()->getParam('profile_id')) {
            $fieldset->addField('profile_id', $type, array(
                'name' => 'profile_id',
                'value' => $model->getProfileId()
            ));
        }

        $fieldset->addField('profile_name', 'text', array(
            'name' => 'profile_name',
            'value' => $model->getProfileName(),
            'label' => Mage::helper('massstockupdate')->__('Profile name'),
            "required" => true,
        ));

        $fieldset->addField('file_path', 'text', array(
            'name' => 'file_path',
            'value' => $model->getFilePath(),
            'label' => Mage::helper('massstockupdate')->__('Path to csv file'),
            "required" => true,
        ));
        $fieldset->addField('file_separator', 'select', array(
            'name' => 'file_separator',
            'value' => $model->getFileSeparator(),
            'label' => Mage::helper('massstockupdate')->__('Field separator'),
            "required" => true,
            'options' => array(
                ';' => ';',
                ',' => ',',
                '|' => '|',
                "\t" => '\tab',
            ),
        ));
        $fieldset->addField('file_enclosure', 'select', array(
            'name' => 'file_enclosure',
            'value' => $model->getFileEnclosure(),
            'label' => Mage::helper('massstockupdate')->__('Field enclosure'),
            "required" => true,
            'options' => array(
                "none" => 'none',
                '"' => '"',
                '\'' => '\'',
            ),
        ));
        $fieldset = $form->addFieldset('massstockupdate_form2', array('legend' => $this->__('Profile Options')));
        $options[] = 'set in a specific column';
        $options[] = 'sum of all specified columns';
        if (Mage::helper("core")->isModuleEnabled("Wyomind_Advancedinventory")) {
            $options[] = 'sum of all local stocks';
        }
        $fieldset->addField('sku_offset', 'text', array(
            'name' => 'sku_offset',
            'value' => ($model->getSkuOffset()) ? $model->getSkuOffset() : 1,
            "required" => true,
            "class" => "validate-number",
            'label' => Mage::helper('massstockupdate')->__('Skus offset'),
            'note' => 'Define the sku\'s columnn',
            'options' => $options,
        ));
        $fieldset->addField('auto_set_total', 'select', array(
            'name' => 'auto_set_total',
            'value' => $model->getAutoSetTotal(),
            'label' => Mage::helper('massstockupdate')->__('Total stock calculation'),
            'note' => 'Calculate the total stock for each product depending of the above method',
            'options' => $options,
        ));

        $fieldset->addField('auto_set_instock', 'select', array(
            'name' => 'auto_set_instock',
            'value' => $model->getAutoSetInstock(),
            'label' => Mage::helper('massstockupdate')->__('Stock status update'),
            'note' => 'Update stock status for each product depending of the above method',
            'options' => array(
                1 => 'automatically',
                0 => 'defined in a specific column',
            ),
        ));
        /* $fieldset->addField('auto_set_managestock', 'select', array(
          'name' => 'auto_set_managestock',
          'value' => $model->getAutoSetManagestock(),
          'label' => Mage::helper('massstockupdate')->__('Automatically set "Manage stock"?'),
          'note' => 'Set all updated product on "Manage stock = yes"',
          'options' => array(
          1 => 'yes',
          0 => 'no',
          ),
          )); */


        $fieldset = $form->addFieldset('massstockupdate_form3', array('legend' => $this->__('Custom rules')));

        $fieldset->addField('use_custom_rules', 'select', array(
            'name' => 'use_custom_rules',
            'value' => $model->getUseCustomRules(),
            'label' => Mage::helper('massstockupdate')->__('Use custom rules'),
            'note' => '',
            'options' => array(
                1 => 'yes',
                0 => 'no',
            ),
        ));

        $fieldset->addField('custom_rules', 'textarea', array(
            'name' => 'custom_rules',
            'value' => $model->getCustomRules(),
            'label' => Mage::helper('massstockupdate')->__('Rules'),
            'note' => '',
            'options' => array(
                1 => 'yes',
                0 => 'no',
            ),
        ));


        $fieldset->addField('mapping', $type, array(
            'name' => 'mapping',
            'value' => $model->getMapping()
        ));
        $fieldset->addField('cron_setting', $type, array(
            'name' => 'cron_setting',
            'value' => $model->getCronSetting()
        ));
        $fieldset->addField('run', $type, array(
            'name' => 'run',
            'value' => ''
        ));

        if (version_compare(Mage::getVersion(), '1.3.0', '>')) {

            $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
            ->addFieldMap('use_custom_rules', 'use_custom_rules')
            ->addFieldMap('custom_rules', 'custom_rules')
            ->addFieldDependence('custom_rules', 'use_custom_rules', 1));
        }






        if (Mage::getSingleton('adminhtml/session')->getMassstockupdateData()) {
            //$form->setValues(Mage::getSingleton('adminhtml/session')->getMassstockupdateData());
            Mage::getSingleton('adminhtml/session')->getMassstockupdateData(null);
        } elseif (Mage::registry('massstockupdate_data')) {
            //$form->setValues(Mage::registry('massstockupdate_data')->getData());
        }

        return parent::_prepareForm();
    }

}
