<?php

class TM_FireCheckout_IndexController extends Mage_Checkout_Controller_Action
{
    protected $_sectionUpdateFunctions = array(
        'payment-method'  => '_getPaymentMethodsHtml',
        'shipping-method' => '_getShippingMethodsHtml',
        'review'          => '_getReviewHtml',
    );

    private $_loadedLayout = null;

    /**
     * @return Mage_Checkout_OnepageController
     */
    public function preDispatch()
    {
        parent::preDispatch();
        $this->_preDispatchValidateCustomer();
        return $this;
    }

    protected function _ajaxRedirectResponse()
    {
        $this->getResponse()
            ->setHeader('HTTP/1.1', '403 Session Expired')
            ->setHeader('Login-Required', 'true')
            ->sendResponse();
        return $this;
    }

    /**
     * Validate ajax request and redirect on failure
     *
     * @return bool
     */
    protected function _expireAjax()
    {
        if (!$this->getCheckout()->getQuote()->hasItems()
            || $this->getCheckout()->getQuote()->getHasError()
            || $this->getCheckout()->getQuote()->getIsMultiShipping()) {
            $this->_ajaxRedirectResponse();
            return true;
        }
        $action = $this->getRequest()->getActionName();
        if (Mage::getSingleton('checkout/session')->getCartWasUpdated(true)
            && !in_array($action, array('index', 'progress'))) {
            $this->_ajaxRedirectResponse();
            return true;
        }

        return false;
    }

    protected function _getLoadedUpdateCheckoutLayout()
    {
        if (null === $this->_loadedLayout) {
            $layout = $this->getLayout();
            $update = $layout->getUpdate();
            $update->load('firecheckout_index_updatecheckout');
            $this->_initLayoutMessages('checkout/session');
            $layout->generateXml();
            $layout->generateBlocks();
            $this->_loadedLayout = $layout;
        }
        return $this->_loadedLayout;
    }

    /**
     * Get shipping method html
     *
     * @return string
     */
    protected function _getShippingMethodHtml()
    {
        return $this->_getLoadedUpdateCheckoutLayout()->getBlock('checkout.shipping.method')->toHtml();
    }

    /**
     * Get payment method html
     *
     * @return string
     */
    protected function _getPaymentMethodHtml()
    {
        return $this->_getLoadedUpdateCheckoutLayout()->getBlock('checkout.payment.method')->toHtml();
    }

    /**
     * Get coupon code html
     *
     * @return string
     */
    protected function _getCouponDiscountHtml()
    {
        return $this->_getLoadedUpdateCheckoutLayout()->getBlock('checkout.coupon')->toHtml();
    }

    /**
     * Get order review html
     *
     * @return string
     */
    protected function _getReviewHtml()
    {
        return $this->_getLoadedUpdateCheckoutLayout()->getBlock('checkout.review')->toHtml();
    }

    /**
     * Get fire checkout model
     *
     * @return TM_FireCheckout_Model_Type_Standard
     */
    public function getCheckout()
    {
        return Mage::getSingleton('firecheckout/type_standard');
    }

    public function forgotpasswordAction()
    {
        $session = Mage::getSingleton('customer/session');

        if ($this->_expireAjax() || $session->isLoggedIn()) {
            return;
        }

        $email = $this->getRequest()->getPost('email');
        $result = array(
            'success' => false
        );
        if ($email) {
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $session->setForgottenEmail($email);
                $result['error'] = Mage::helper('checkout')->__('Invalid email address.');
            } else {
                $customer = Mage::getModel('customer/customer')
                    ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                    ->loadByEmail($email);

                if ($customer->getId()) {
                    try {
                        $newPassword = $customer->generatePassword();
                        $customer->changePassword($newPassword, false);
                        $customer->sendPasswordReminderEmail();
                        $result['success'] = true;
                        $result['message'] = Mage::helper('customer')->__('A new password has been sent.');
                    } catch (Exception $e){
                        $result['error'] = $e->getMessage();
                    }
                } else {
                    $result['error'] = Mage::helper('customer')->__('This email address was not found in our records.');
                    $session->setForgottenEmail($email);
                }
            }
        } else {
            $result['error'] = Mage::helper('customer')->__('Please enter your email.');
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function loginAction()
    {
        $session = Mage::getSingleton('customer/session');

        if ($this->_expireAjax() || $session->isLoggedIn()) {
            return;
        }

        $result = array(
            'success' => false
        );

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $session->login($login['username'], $login['password']);
                    $result['redirect'] = Mage::getUrl('*/*/index', array('_secure'=>true));
                    $result['success'] = true;
                } catch (Mage_Core_Exception $e) {
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            $message = Mage::helper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', Mage::helper('customer')->getEmailConfirmationUrl($login['username']));
                            break;
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            break;
                        default:
                            $message = $e->getMessage();
                    }
                    $result['error'] = $message;
                    $session->setUsername($login['username']);
                } catch (Exception $e) {
                    // Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } else {
                $result['error'] = Mage::helper('customer')->__('Login and password are required.');
            }
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Checkout page
     */
    public function indexAction()
    {
        if (!Mage::helper('firecheckout')->canFireCheckout()) {
            Mage::getSingleton('checkout/session')->addError($this->__('The fire checkout is disabled.'));
            $this->_redirect('checkout/cart');
            return;
        }

        if (!Mage::getStoreConfig('firecheckout/mobile/enabled')
            && $this->_isMobile()) {

            $this->_redirect('checkout/onepage');
            return;
        }

        $quote = $this->getCheckout()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('checkout/cart');
            return;
        }
        if (!$quote->validateMinimumAmount()) {
            $error = Mage::getStoreConfig('sales/minimum_order/error_message');
            Mage::getSingleton('checkout/session')->addError($error);
            $this->_redirect('checkout/cart');
            return;
        }
        Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
        Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure'=>true)));

        $this->getCheckout()->applyDefaults()->initCheckout();
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        $this->getLayout()->getBlock('head')->setTitle(Mage::getStoreConfig('firecheckout/general/title'));
        $this->renderLayout();
    }

    /**
     * Order success action
     */
    public function successAction()
    {
        $session = $this->getCheckout()->getCheckout();
        if (!$session->getLastSuccessQuoteId()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $lastQuoteId = $session->getLastQuoteId();
        $lastOrderId = $session->getLastOrderId();
        $lastRecurringProfiles = $session->getLastRecurringProfileIds();
        if (!$lastQuoteId || (!$lastOrderId && empty($lastRecurringProfiles))) {
            $this->_redirect('checkout/cart');
            return;
        }

        $session->clear();
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        Mage::dispatchEvent('checkout_onepage_controller_success_action', array('order_ids' => array($lastOrderId)));
        $this->renderLayout();
    }

    public function failureAction()
    {
        $lastQuoteId = $this->getCheckout()->getCheckout()->getLastQuoteId();
        $lastOrderId = $this->getCheckout()->getCheckout()->getLastOrderId();

        if (!$lastQuoteId || !$lastOrderId) {
            $this->_redirect('checkout/cart');
            return;
        }

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Address JSON
     */
    public function getAddressAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        $addressId = $this->getRequest()->getParam('address', false);
        if ($addressId) {
            $address = $this->getCheckout()->getAddress($addressId);

            if (Mage::getSingleton('customer/session')->getCustomer()->getId() == $address->getCustomerId()) {
                $this->getResponse()->setHeader('Content-type', 'application/x-json');
                $this->getResponse()->setBody($address->toJson());
            } else {
                $this->getResponse()->setHeader('HTTP/1.1','403 Forbidden');
            }
        }
    }

    /**
     * Checks is the region_id or postcode or country was changed
     */
    protected function _isAddressChanged($type = 'Billing', $data, $addressId = false)
    {
        $data = array_merge(array(
            'region_id'     => 0,
            'postcode'      => 0,
            'country_id'    => 0
        ), $data);

        $address = $this->getCheckout()->getQuote()->{"get{$type}Address"}();

        if ($addressId) {
            return $address->getCustomerAddressId() != $addressId;
        } else {
            return $data['region_id']    != $address->getRegionId()
                || $data['postcode']     != $address->getPostcode()
                || $data['country_id']   != $address->getCountryId();
        }
    }

    /**
     * Update checkout sections
     */
    public function updateCheckoutAction()
    {
        if ($this->_expireAjax() || !$this->getRequest()->isPost()) {
            return;
        }

        $result = array();
        $boxesToUpdate = array(
            'coupon-discount'   => false,
            'payment-method'    => false,
            'shipping-method'   => false,
            'review'            => true
        );

        /**
         * @var Mage_Sales_Model_Quote
         */
        $quote = $this->getCheckout()->getQuote();

        // discount coupon
        $couponData = $this->getRequest()->getPost('coupon', array());
        $processCoupon = $this->getRequest()->getPost('process_coupon', false);
        if ($couponData && $processCoupon) {
            if (!empty($couponData['remove'])) {
                $couponData['code'] = '';
            }
            $oldCouponCode = $quote->getCouponCode();
            if ($oldCouponCode != $couponData['code']) {
                try {
                    $quote->setCouponCode(
                        strlen($couponData['code']) ? $couponData['code'] : ''
                    );
                    if (!$this->isValidCouponCode($couponData['code'])) {
                        $quote->setCouponCode('');
                    }
                    $this->getRequest()->setPost('payment-method', true);
                    if ($couponData['code']) {
                        if ($couponData['code'] == $quote->getCouponCode()) {
                            Mage::getSingleton('checkout/session')->addSuccess(
                                $this->__('Coupon code "%s" was applied.', Mage::helper('core')->htmlEscape($couponData['code']))
                            );
                        } else {
                            Mage::getSingleton('checkout/session')->addError(
                                $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponData['code']))
                            );
                        }
                    } else {
                        Mage::getSingleton('checkout/session')->addSuccess($this->__('Coupon code was canceled.'));
                    }
                } catch (Mage_Core_Exception $e) {
                    Mage::getSingleton('checkout/session')->addError($e->getMessage());
                } catch (Exception $e) {
                    Mage::getSingleton('checkout/session')->addError($this->__('Cannot apply the coupon code.'));
                }
                $boxesToUpdate['coupon-discount'] = true;
            }
        }

        $billingData = $this->getRequest()->getPost('billing', array());
        $billingData = $this->_filterPostData($billingData);
        $billingAddressId = $this->getRequest()->getPost('billing_address_id', false);
        $shippingUpdated = false;
        $paymentData = $this->getRequest()->getPost('payment', array());

        // billing section
        if ($this->getRequest()->getPost('payment-method', false)
            || $this->_isAddressChanged('Billing', $billingData, $billingAddressId)) {

            if (isset($billingData['email'])) {
                $billingData['email'] = trim($billingData['email']);
            }
            $billingResult = $this->getCheckout()->saveBilling($billingData, $billingAddressId, false);

            if (!isset($billingResult['error'])) {
                $paymentData = $this->getRequest()->getPost('payment', array());
                $this->getCheckout()->applyPaymentMethod(isset($paymentData['method']) ? $paymentData['method'] : null);

                $boxesToUpdate['payment-method'] = true;

                if (!$quote->isVirtual()
                    && isset($billingData['use_for_shipping']) && $billingData['use_for_shipping'] == 1) {

                    $shippingUpdated = true;
                    $boxesToUpdate['shipping-method'] = true;
                    $result['duplicateBillingInfo'] = 'true';
                }
            } else {
                $result['error_messages'] = $billingResult['message'];
            }
        } elseif ($this->getRequest()->getPost('payment-method-changed', false)) {
            $this->getCheckout()->applyPaymentMethod(isset($paymentData['method']) ? $paymentData['method'] : null);
        }

        $shippingData = $this->getRequest()->getPost('shipping', array());
        $shippingAddressId = $this->getRequest()->getPost('shipping_address_id', false);
        // shipping section
        if (!$quote->isVirtual() && !$shippingUpdated) {
            if ($this->getRequest()->getPost('shipping-method', false)
                   || $this->_isAddressChanged('Shipping', $shippingData, $shippingAddressId)) {

                $shippingResult = $this->getCheckout()->saveShipping($shippingData, $shippingAddressId, false);

                if (!isset($shippingResult['error'])) {
                    $boxesToUpdate['shipping-method'] = true;
                }
            }
        }

        $this->getCheckout()->applyShippingMethod($this->getRequest()->getPost('shipping_method', false));
        $quote->collectTotals()->save();

        foreach ($boxesToUpdate as $boxId => $needUpdate) {
            if (!$needUpdate) {
                continue;
            }
            $method = str_replace(' ', '', ucwords(str_replace('-', ' ', $boxId)));
            $result['update_section'][$boxId] = $this->{'_get' . $method . 'Html'}();
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Create order action
     */
    public function saveOrderAction()
    {
		
		
    /*  if ($this->_expireAjax()) {
            return;
        }

        $result = array();
		
        try {
            $this->getCheckout()->applyShippingMethod($this->getRequest()->getPost('shipping_method', false));

            $billing = $this->_filterPostData($this->getRequest()->getPost('billing', array()));
            $result = $this->getCheckout()->saveBilling(
                $billing,
                $this->getRequest()->getPost('billing_address_id', false)
            );
            if ($result) {
                $result['success'] = false;
                $result['error'] = true;
                $result['error_messages'] = $result['message'];
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                return;
            }

            if ((!isset($billing['use_for_shipping']) || !$billing['use_for_shipping'])
                && !$this->getCheckout()->getQuote()->isVirtual()) {

                $result = $this->getCheckout()->saveShipping(
                    $this->_filterPostData($this->getRequest()->getPost('shipping', array())),
                    $this->getRequest()->getPost('shipping_address_id', false)
                );
                if ($result) {
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = $result['message'];
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }

            if ($requiredAgreements = Mage::helper('firecheckout')->getRequiredAgreementIds()) {
                $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                if ($diff = array_diff($requiredAgreements, $postedAgreements)) {
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = Mage::helper('checkout')->__('Please agree to all the terms and conditions before placing the order.');
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }

            $result = $this->_savePayment();
            if ($result && !isset($result['redirect'])) {
                $result['error_messages'] = $result['error'];
            }

            if (!isset($result['error'])) {
                Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method', array('request'=>$this->getRequest(), 'quote'=>$this->getCheckout()->getQuote()));
                $this->_subscribeToNewsletter();
            }

            Mage::getSingleton('customer/session')->setOrderCustomerComment(
                $this->getRequest()->getPost('order-comment')
            );

            if (!isset($result['redirect']) && !isset($result['error'])) {
                if ($data = $this->getRequest()->getPost('payment', false)) {
                    $this->getCheckout()->getQuote()->getPayment()->importData($data);
                }

                //$this->getCheckout()->saveOrder();
                $redirectUrl = $this->getCheckout()->getCheckout()->getRedirectUrl(); test
                $this->getCheckout()->getQuote()->save();
                $result['success'] = true;
                $result['order_created'] = true;
                $result['error']   = false;
            } elseif (isset($result['redirect'])) {
                $paymentData = $this->getRequest()->getPost('payment', false);
                if ('paypal_express' == $paymentData['method']
                    && Mage::getStoreConfig('firecheckout/general/paypalexpress_register')) {
                    $this->getCheckout()->registerCustomerIfRequested();
                }
            }
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getCheckout()->getQuote(), $e->getMessage());
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();

            if ($gotoSection = $this->getCheckout()->getCheckout()->getGotoSection()) {
                $result['goto_section'] = $gotoSection;
                $this->getCheckout()->getCheckout()->setGotoSection(null);
            }

            if ($updateSection = $this->getCheckout()->getCheckout()->getUpdateSection()) {
                if (isset($this->_sectionUpdateFunctions[$updateSection])) {

                    $layout = $this->_getLoadedUpdateCheckoutLayout();

                    $updateSectionFunction = $this->_sectionUpdateFunctions[$updateSection];
                    $result['update_section'] = array(
                        'name' => $updateSection,
                        'html' => $this->$updateSectionFunction()
                    );
                }
                $this->getCheckout()->getCheckout()->setUpdateSection(null);
            }

            $this->getCheckout()->getQuote()->save();
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getCheckout()->getQuote(), $e->getMessage());
            $result['success']  = false;
            $result['error']    = true;
            $result['error_messages'] = Mage::helper('checkout')->__('There was an error processing your order. Please contact us or try again later.');
            $this->getCheckout()->getQuote()->save();
        } 
		*/
		/* My code */
		$email = $_POST['billing']['email'];

		$read1 = Mage::getSingleton('core/resource')->getConnection('core_read'); 

		$qry1 = "Select * from customer_entity where email='$email'"; //query            
		$res1 = $read1->fetchRow($qry1);
		
		
		if((($_POST['billing']['customer_password']!="") && 
		($_POST['billing']['customer_password']==$_POST['billing']['confirm_password'])) || (!empty($res1))){
			$this->createUserOrderAction();
		}else{
			$this->createOrderAction();
		}
		
				/*Mage::getSingleton('checkout/session')->clear();
				$message = $this->__('Your ordered successfully..');
				Mage::getSingleton('core/session')->addSuccess($message);
				$result = array();
				$redirectUrl = $this->getCheckout()->getCheckout()->getRedirectUrl();
				
                //$this->getCheckout()->getQuote()->save();
				
                $result['success'] = true;
                $result['order_created'] = true;
                $result['error'] = false;*/
				
		//}
		
		
				
		
		//header("location:http://whiteuniverse.com/kabloomdev/index.php/checkout/cart");	
		//echo $redirectUrl = $this->getCheckout()->getCheckout()->getRedirectUrl();die;
		/* End My code */
		
        /**
         * when there is redirect to third party, we don't want to save order yet.
         * we will save the order in return action.
         */
		
        if (isset($redirectUrl)){
			
            $result['redirect'] = $redirectUrl;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
	
	
	/**
	 * Create user and order
`	*/	 
	public function createUserOrderAction(){
	
				$countArray = count($_POST['shipping']['firstname']);
				
			
				$items = Mage::getSingleton('checkout/cart')->getQuote()->getAllItems();
				$subTotal = 0;
				
				
				$i=0;
				if($i<$countArray){
				foreach($items as $item) { 
				//if(!empty($_POST['shipping']['firstname'][$i])){
				if($item->getPrice()!=0.0000){
					
				$websiteId = Mage::app()->getWebsite()->getId();
				$store = Mage::app()->getStore();
				$customer = Mage::getModel("customer/customer");
				$customeremail = Mage::getModel("customer/customer");
				$customer->setWebsiteId($websiteId);
				$customer->setStore($store);
				 
				$addr="";
				foreach($_POST['billing']['street'] as $addvalue){
					$addr.= $addvalue." ";
				}
								
				$customer->setFirstname($_POST['billing']['firstname']);
				$customer->setLastname($_POST['billing']['lastname']);
				$customer->setEmail($_POST['billing']['email']);
				$email = $_POST['billing']['email'];
				//$customer->setPasswordHash(md5("abhi@123"));
				$customer->setPasswordHash(md5($_POST['billing']['customer_password']));
				$customer->setCity($_POST['billing']['city']);
				$customer->setCountryId($_POST['billing']['country_id']);
				$customer->setTelephone($_POST['billing']['telephone']);
				$customer->setPostcode($_POST['billing']['postcode']);
				$customer->setStreet($addr);
				
				
				$read = Mage::getSingleton('core/resource')->getConnection('core_read'); 

				$qry = "Select * from customer_entity where email='$email'"; //query            
				$res = $read->fetchRow($qry); 
				
				if(empty($res)) {
					
						$customer->save();
					
					/**
					* Email for created user account
					*/
					$to = $email;
					$subject = "Kabloom";

					$message = "
					<html>
					<head>
					<title>Kabloom</title>
					</head>
					<body>
					<p>Your account has been created and you login detail is below!</p>
					<table>
					<tr>
					<th>User name :</th>
					<td>".$email."</td>
					</tr>
					<tr>
					<th>Password :</th>
					<td>".$_POST['billing']['customer_password']."</td>
					</tr>
					</table>
					</body>
					</html>
					";

					// Always set content-type when sending HTML email
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

					// More headers
					$headers .= 'From: <info@kabloom.in>' . "\r\n";
					$headers .= 'Cc: ' . "\r\n";
					mail($to,$subject,$message,$headers);
					
					
				}
				$customer->loadByEmail($_POST['billing']['email']);
				
				$customer1 = Mage::getModel("customer/customer")->load($customer->getId());
				
				$address = Mage::getModel("customer/address");
				$shipAddress = $_POST['shipping']['street'][$i]." ".$_POST['shipping']['street1'][$i];
				$_custom_address = array (
					'firstname' => $_POST['shipping']['firstname'][$i],
					'lastname' => $_POST['shipping']['lastname'][$i],
					'street' => array (
						'0' => $_POST['shipping']['street'][$i],
						'1' => $_POST['shipping']['street'][$i],
					),
					'city' => $_POST['shipping']['city'][$i],
					'region_id' => 493,
					'region' => '',
					'postcode' => $_POST['shipping']['postcode'][$i],
					'country_id' => $_POST['shipping']['country_id'][$i], 
					'telephone' => $_POST['shipping']['telephone'][$i],
				);
				
				
				$address->setData($_custom_address)
				->setCustomerId($customer->getId())
				->setIsDefaultBilling('1')
				->setIsDefaultShipping('1')
				->setSaveInAddressBook('1');
				 
					$address->save();
				
				
				

				$id = $customer->getId();
				$customer = Mage::getModel('customer/customer')->load($id);
				//echo "<pre>";print_r($customer->getData());die;
				$transaction = Mage::getModel('core/resource_transaction');
				$storeId = $customer->getStoreId();
				$reservedOrderId = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId);
				$order = Mage::getModel('sales/order')
				->setIncrementId($reservedOrderId)
				->setStoreId($storeId)
				->setQuoteId(0)
				->setGlobal_currency_code('INR')
				->setBase_currency_code('INR')
				->setStore_currency_code('INR')
				->setOrder_currency_code('INR');
				
				$order->setCustomer_email($customer->getEmail())
				->setCustomerFirstname($customer->getFirstname())
				->setCustomerLastname($customer->getLastname())
				->setCustomerGroupId($customer->getGroupId())
				->setCustomer_is_guest(0)
				->setCustomer($customer);
				
				$billing = $customer->getDefaultBillingAddress();
				
				$billingAddress = Mage::getModel('sales/order_address')
				->setStoreId($storeId)
				->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
				->setCustomerId($customer->getId())
				->setCustomerAddressId($billing->getDefaultBilling())
				->setCustomer_address_id($billing->getEntityId())
				->setPrefix($billing->getPrefix())
				->setFirstname($customer->getFirstname())
				->setMiddlename($customer->getMiddlename())
				->setLastname($customer->getLastname())
				->setSuffix($billing->getSuffix())
				->setCompany($customer->getCompany())
				->setStreet($customer->getStreet())
				->setCity($customer->getCity())
				->setCountry_id($customer->getCountryId())
				->setRegion($billing->getRegion())
				->setRegion_id($billing->getRegionId())
				->setPostcode($customer->getPostcode())
				->setTelephone($customer->getTelephone())
				->setFax($customer->getFax());
				$order->setBillingAddress($billingAddress);
				
				$shipping = $customer->getDefaultShippingAddress();
				
				$shippingAddress = Mage::getModel('sales/order_address')
				->setStoreId($storeId)
				->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
				->setCustomerId($customer->getId())
				->setCustomerAddressId($customer1->getDefaultShipping())
				->setCustomer_address_id($shipping->getEntityId())
				->setPrefix($shipping->getPrefix())
				->setFirstname($shipping->getFirstname())
				->setMiddlename($shipping->getMiddlename())
				->setLastname($shipping->getLastname())
				->setSuffix($shipping->getSuffix())
				->setCompany($shipping->getCompany())
				->setStreet($shipping->getStreet())
				->setCity($shipping->getCity())
				->setCountry_id($shipping->getCountryId())
				->setRegion($shipping->getRegion())
				->setRegion_id($shipping->getRegionId())
				->setPostcode($shipping->getPostcode())
				->setTelephone($shipping->getTelephone())
				->setFax($shipping->getFax());
				$order->setShippingAddress($shippingAddress)
				->setShipping_method('flatrate_flatrate');
				
				$orderPayment = Mage::getModel('sales/order_payment')
				->setStoreId($storeId)
				->setCustomerPaymentId(0)
				->setMethod('purchaseorder')
				->setPo_number(' – ');
				$order->setPayment($orderPayment);
				
				
				
					$rowTotal = $item->getPrice() * $item->getQty;
					
					if($item->getPrice()!=0.0000){ 
						$id = $item->getProductId();
						$qty = $item->getQty();
						$price = $item->getPrice(); 
						$name = $item->getName(); 
						
					$products = array(
					$id => array(
					'qty' => $qty
					)
					);
					//echo "<pre>";print_r($products);
				foreach ($products as $productId=>$product) { 
				$_product = Mage::getModel('catalog/product')->load($productId);

				$rowTotal = $price * $product['qty'];
				$orderItem = Mage::getModel('sales/order_item')
				->setStoreId($storeId)
				->setQuoteItemId(0)
				->setQuoteParentItemId(NULL)
				->setProductId($productId)
				->setProductType($_product->getTypeId())
				->setQtyBackordered(NULL)
				->setTotalQtyOrdered($product['rqty'])
				->setQtyOrdered($product['qty'])
				->setName($name)
				->setSku($_product->getSku())
				->setPrice($price)
				->setBasePrice($price)
				->setOriginalPrice($price)
				->setRowTotal($rowTotal)
				->setBaseRowTotal($rowTotal);
				//echo "<pre>";print_r($orderItem);
				$subTotal = $rowTotal;
				
				$order->addItem($orderItem);
				
				
				
				$order->setSubtotal($subTotal)
				->setBaseSubtotal($subTotal)
				->setGrandTotal($subTotal)
				->setBaseGrandTotal($subTotal);
				$transaction->addObject($order);
				$transaction->addCommitCallback(array($order, 'place'));
				$transaction->addCommitCallback(array($order, 'save'));
				$transaction->save();
				
				
				//$order->getSendConfirmation(null);
				$order->sendNewOrderEmail();
				
			}
			
				
				
						
				}
				unset($orderItem);
				unset($order);
				
					
				$i++;
				}
			
				
				}
				}
	}
	
	
	/**
	 * Create order for guest
	 */
	 public function createOrderAction(){
			$countArray = count($_POST['shipping']['firstname']);
			$items = Mage::getSingleton('checkout/cart')->getQuote()->getAllItems();
			$subTotal = 0;
			$i=0;
			if($i<$countArray){
				foreach($items as $item) { 
					if($item->getPrice()!=0.0000){
						
						
						//$id=3; // get Customer Id
						//$customer = Mage::getModel('customer/customer')->load($id);
						
						$transaction = Mage::getModel('core/resource_transaction');
						$websiteId = Mage::app()->getWebsite()->getId();
						$store = Mage::app()->getStore();
						$customer = Mage::getModel("customer/customer");
						$customeremail = Mage::getModel("customer/customer");
						$customer->setWebsiteId($websiteId);
						$customer->setStore($store);
						$reservedOrderId = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId);
						
						$addr="";
						foreach($_POST['billing']['street'] as $addvalue){
							$addr.= $addvalue." ";
						}
						$customer->setFirstname($_POST['billing']['firstname']);
						$customer->setLastname($_POST['billing']['lastname']);
						$customer->setEmail($_POST['billing']['email']);
						$email = $_POST['billing']['email'];
						//$customer->setPasswordHash(md5("abhi@123"));
						$customer->setPasswordHash(md5($_POST['billing']['customer_password']));
						$customer->setCity($_POST['billing']['city']);
						//$customer->setCountry($_POST['billing']['country_id']);
						$customer->setTelephone($_POST['billing']['telephone']);
						$customer->setPostcode($_POST['billing']['postcode']);
						$customer->setStreet($addr);
						
						
						
						$shipAddress = $_POST['shipping']['street'][$i]." ".$_POST['shipping']['street1'][$i];
						
						$customershipping = Mage::getModel("customer/customer");
						$customershipping->setFirstname($_POST['shipping']['firstname'][$i]);
						$customershipping->setLastname($_POST['shipping']['lastname'][$i]);
						$customershipping->setStreet($shipAddress);
						$customershipping->setCity($_POST['shipping']['city'][$i]);
						$customershipping->setRegion('');
						$customershipping->setPostcode('');
						$customershipping->setTelephone($_POST['shipping']['telephone'][$i]);
						
					
						
						
						
						$order = Mage::getModel('sales/order')
						->setIncrementId($reservedOrderId)
						->setStoreId($storeId)
						->setQuoteId(0)
						->setGlobal_currency_code('INR')
						->setBase_currency_code('INR')
						->setStore_currency_code('INR')
						->setOrder_currency_code('INR');
						//Set your store currency USD or any other

						// set Customer data
						$order->setCustomer_email($customer->getEmail())
						->setCustomerFirstname($customer->getFirstname())
						->setCustomerLastname($customer->getLastname())
						->setCustomerGroupId(0)
						->setCustomer_is_guest(0)
						->setCustomer($customer);

						// set Billing Address
						//$billing = $customer->getDefaultBillingAddress();
						$billingAddress = Mage::getModel('sales/order_address')
						->setStoreId($storeId)
						->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
						->setCustomerId(null)
						//->setCustomerAddressId($customer->getDefaultBilling())
						//->setCustomer_address_id($billing->getEntityId())
						//->setPrefix($billing->getPrefix())
						->setFirstname($customer->getFirstname())
						->setMiddlename('')
						->setLastname($customer->getLastname())
						//->setSuffix($billing->getSuffix())
						//->setCompany($billing->getCompany())
						->setStreet($customer->getStreet())
						->setCity($customer->getCity())
						->setCountry_id($customer->getCountryId())
						->setRegion($customer->getRegion())
						->setRegion_id($customer->getRegionId())
						->setPostcode($customer->getPostcode())
						->setTelephone($customer->getTelephone())
						->setFax($customer->getFax());
						$order->setBillingAddress($billingAddress);

						//$shipping = $customer->getDefaultShippingAddress();
						$shippingAddress = Mage::getModel('sales/order_address')
						->setStoreId($storeId)
						->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
						//->setCustomerId($customer->getId())
						//->setCustomerAddressId($customer->getDefaultShipping())
						//->setCustomer_address_id($shipping->getEntityId())
						//->setPrefix($shipping->getPrefix())
						->setFirstname($customershipping->getFirstname())
						->setMiddlename('')
						->setLastname($customershipping->getLastname())
						//->setSuffix($shipping->getSuffix())
						//->setCompany($shipping->getCompany())
						->setStreet($customershipping->getStreet())
						->setCity($customershipping->getCity())
						->setCountry_id($customershipping->getCountryId())
						->setRegion($customershipping->getRegion())
						->setRegion_id($customershipping->getRegionId())
						->setPostcode($customershipping->getPostcode())
						->setTelephone($customershipping->getTelephone())
						->setFax($customershipping->getFax());

						$order->setShippingAddress($shippingAddress)
						->setShipping_method('flatrate_flatrate');
						/*->setShippingDescription($this->getCarrierName('flatrate'));*/
						/*some error i am getting here need to solve further*/

						//you can set your payment method name here as per your need
						$orderPayment = Mage::getModel('sales/order_payment')
						->setStoreId($storeId)
						->setCustomerPaymentId(0)
						->setMethod('purchaseorder')
						->setPo_number(' – ');
						$order->setPayment($orderPayment);

						// let say, we have 2 products
						//check that your products exists
						//need to add code for configurable products if any
						$subTotal = 0;
						$id = $item->getProductId();
						$qty = $item->getQty();
						$price = $item->getPrice(); 
						$name = $item->getName(); 
						
						$products = array(
						$id => array(
						'qty' => $qty
						)
						);

						foreach ($products as $productId=>$product) {
						$_product = Mage::getModel('catalog/product')->load($productId);
						
						$rowTotal = $price * $product['qty'];
						$orderItem = Mage::getModel('sales/order_item')
						->setStoreId($storeId)
						->setQuoteItemId(0)
						->setQuoteParentItemId(NULL)
						->setProductId($productId)
						->setProductType($_product->getTypeId())
						->setQtyBackordered(NULL)
						->setTotalQtyOrdered($product['rqty'])
						->setQtyOrdered($product['qty'])
						->setName($_product->getName())
						->setSku($_product->getSku())
						->setPrice($price)
						->setBasePrice($price)
						->setOriginalPrice($price)
						->setRowTotal($rowTotal)
						->setBaseRowTotal($rowTotal);

						$subTotal = $rowTotal;
						$order->addItem($orderItem);
						}

						$order->setSubtotal($subTotal)
						->setBaseSubtotal($subTotal)
						->setGrandTotal($subTotal)
						->setBaseGrandTotal($subTotal);

						$transaction->addObject($order);
						$transaction->addCommitCallback(array($order, 'place'));
						$transaction->addCommitCallback(array($order, 'save'));
						$transaction->save();
						$order->sendNewOrderEmail();
						unset($orderItem);
						unset($order);
				
					$i++;
					
					}
				}
			}
	 
	 }
	 
	 
    /**
     * Save payment
     */
    protected function _savePayment()
    {
        try {
            $result = array();
            $data = $this->getRequest()->getPost('payment', array());
            $result = $this->getCheckout()->savePayment($data);

            $redirectUrl = $this->getCheckout()->getQuote()->getPayment()->getCheckoutRedirectUrl();
            if ($redirectUrl) {
                $result['redirect'] = $redirectUrl;
            }
        } catch (Mage_Payment_Exception $e) {
            if ($e->getFields()) {
                $result['fields'] = $e->getFields();
            }
            $result['error'] = $e->getMessage();
        } catch (Mage_Core_Exception $e) {
            $result['error'] = $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
            $result['error'] = Mage::helper('checkout')->__('Unable to set Payment Method.');
        }
        return $result;
    }

    /**
     * Subsribe payer to newsletterr.
     * All notices and error messages are not shown,
     * to not confuse payer during checkout (Only checkout messages can be showed).
     *
     * @return void
     */
    protected function _subscribeToNewsletter()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('newsletter')) {
            //$session            = Mage::getSingleton('core/session');
            $customerSession    = Mage::getSingleton('customer/session');
            $billingData        = $this->getRequest()->getPost('billing');
            $email              = $customerSession->isLoggedIn() ?
                $customerSession->getCustomer()->getEmail() : $billingData['email'];

            try {
                if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 &&
                    !$customerSession->isLoggedIn()) {
                    Mage::throwException(Mage::helper('newsletter')->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::getUrl('customer/account/create/')));
                }

                $ownerId = Mage::getModel('customer/customer')
                        ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                        ->loadByEmail($email)
                        ->getId();
                if ($ownerId !== null && $ownerId != $customerSession->getId()) {
                    Mage::throwException(Mage::helper('newsletter')->__('Sorry, but your can not subscribe email adress assigned to another user.'));
                }

                $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
                /*if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                    $session->addSuccess(Mage::helper('newsletter')->__('Confirmation request has been sent.'));
                } else {
                    $session->addSuccess(Mage::helper('newsletter')->__('Thank you for your subscription.'));
                }*/
            } catch (Mage_Core_Exception $e) {
                //$session->addException($e, Mage::helper('newsletter')->__('There was a problem with the subscription: %s', $e->getMessage()));
            } catch (Exception $e) {
                //$session->addException($e, Mage::helper('newsletter')->__('There was a problem with the subscription.'));
            }
        }
    }

    /**
     * Filtering posted data. Converting localized data if needed
     *
     * @param array
     * @return array
     */
    protected function _filterPostData($data)
    {
        $data = $this->_filterDates($data, array('dob'));
        return $data;
    }

    // https://github.com/mrlynn/MobileBrowserDetectionExample
    private function _isMobile()
    {
       $isMobile = false;
       if(preg_match('/(android|up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
           $isMobile = true;
       }
       if((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml')>0)
        or ((isset($_SERVER['HTTP_X_WAP_PROFILE'])
        or isset($_SERVER['HTTP_PROFILE'])))) {

           $isMobile = true;
       }

       $mobileUserAgent = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
       $mobileAgents = array(
           'w3c ','acs-','alav','alca','amoi','andr','audi','avan','benq',
           'bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco',
           'eric','hipt','inno','ipaq','java','jigs','kddi','keji','leno',
           'lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef',
           'mobi','mot-','moto','mwbp','nec-','newt','noki','oper','palm',
           'pana','pant','phil','play','port','prox','qwap','sage','sams',
           'sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem',
           'smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh',
           'tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
           'wapr','webc','winw','winw','xda','xda-'
       );

       if(in_array($mobileUserAgent, $mobileAgents)) {
           $isMobile = true;
       }

       if (isset($_SERVER['ALL_HTTP'])) {
           if (strpos(strtolower($_SERVER['ALL_HTTP']), 'OperaMini') > 0) {
               $isMobile = true;
           }
       }
       if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') > 0) {
           $isMobile = false;
       }
       return $isMobile;
    }

    public function isValidCouponCode($code)
    {
        $websiteId = Mage::app()->getStore()->getWebsiteId();
        $customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        $rules = Mage::getResourceModel('salesrule/rule_collection')
            ->setValidationFilter($websiteId, $customerGroupId, $code)
            ->load();

        foreach ($rules as $rule) {
            foreach ($rule->getCoupons() as $coupon) {
                if (strtolower($coupon->getCode()) == strtolower($code)) {
                    return true;
                }
            }
        }
        return false;
    }
}
