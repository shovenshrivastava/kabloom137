<?php

class TM_FireCheckout_Model_Observer
{
    public function setCustomerComment($data)
    {
        $comment = trim(Mage::getSingleton('customer/session')->getOrderCustomerComment());
        if (!empty($comment)) {
            $data['order']->addStatusHistoryComment($comment)
                ->setIsVisibleOnFront(true)
                ->setIsCustomerNotified(false);
        }
    }

    public function unsetCustomerComment()
    {
        Mage::getSingleton('customer/session')->setOrderCustomerComment(null);
    }

    public function addToCartComplete(Varien_Event_Observer $observer)
    {
        $generalConfig = Mage::getStoreConfig('firecheckout/general');
        if ($generalConfig['enabled'] && $generalConfig['redirect_to_checkout']) {
            $observer->getResponse()
                ->setRedirect(
                    Mage::helper('firecheckout/url')->getCheckoutUrl()
                );
            Mage::getSingleton('checkout/session')->setNoCartRedirect(true);
        }
    }
}
