<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    MagePsycho
 * @package     MagePsycho_Citydropdown
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @generator   http://www.mgt-commerce.com/kickstarter/ Mgt Kickstarter
 */

class MagePsycho_Citydropdown_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getUaeCities()
    {
        $helper = Mage::helper('directory');
        $cities = array(
            $helper->__('Abu Dhabi'),
            $helper->__('Ajman'),
            $helper->__('Al Ain'),
            $helper->__('Dubai'),
            $helper->__('Fujairah'),
            $helper->__('Ras al Khaimah'),
            $helper->__('Sharjah'),
            $helper->__('Umm al Quwain'),
        );
        return $cities;
    }
 
    public function getUaeCitiesAsDropdown($selectedCity = '')
    {
        $cities = $this->getUaeCities();
        $options = '';
        foreach($cities as $city){
            $isSelected = $selectedCity == $city ? ' selected="selected"' : null;
            $options .= '<option value="' . $city . '"' . $isSelected . '>' . $city . '</option>';
        }
        return $options;
    }

}