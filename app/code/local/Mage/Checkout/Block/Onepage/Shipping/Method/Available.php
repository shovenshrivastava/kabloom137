<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * One page checkout status
 *
 * @category   Mage
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Checkout_Block_Onepage_Shipping_Method_Available extends Mage_Checkout_Block_Onepage_Abstract
{
    protected $_rates;
    protected $_address;

    public function getShippingRates()
    {
		$hour = Mage::getModel('core/date')->date('G');
		$next_date = date('m/d/Y', strtotime($date .' +1 day'));
		// code to update address from product page inputs
		$read= Mage::getSingleton('core/resource')->getConnection('core_read');
		$quote = Mage::getSingleton('checkout/session')->getQuote();
		$cartCount = Mage::helper('checkout/cart')->getCart()->getItemsCount();
		$_cart = Mage::getSingleton('checkout/cart');
		$cartItems = $quote->getAllVisibleItems();
		$i=1;
		foreach($_cart->getItems() as $item){//start for each product
			if($i==1){
				$ptype = $item->getData('product_type');
				$pId = $item->getData('product_id'); 
				$itemId = $item->getData('item_id'); 
				$first_item[] = $item;
				if($ptype == "configurable"){
					$resultC=$read->query("SELECT * FROM sales_flat_quote_item WHERE parent_item_id = '".$itemId."'");
					$rowP = $resultC->fetch();
					$zip = $rowP['cities'];
					$dDate = $rowP['shipping'];
				}else{
					$zip = $item->getData('cities'); 
					$dDate = $item->getData('shipping'); 
				}
			}
			$i++;
		} 
		if($zip){
			$results=$read->query("SELECT * FROM citylist WHERE zipcode = '".$zip."'");
			// $zipcode = $results->fetchAll($query);
			$zipcode=array();
			$opzip='';
			$j=0;
			while ($row = $results->fetch()){
				if($j==0){
				 $country_id=$row[country_id];
				 $regionId=$row[region_id];
				 $citydefaultname = $row[default_name];
				}
				$j++;
		
			$country = Mage::getModel('directory/country')->loadByCode($country_id);
			$countryName = $country->getName(); // looks like "United States"
			$regionModel = Mage::getModel('directory/region')->loadByCode($regionId, $country_id);
			$region_id = $regionModel->getId();
			$region = Mage::getModel('directory/region')->load($region_id);
			$regionName = $region->getName(); // looks like Massachusetts
			}// end of while
		} //end of if for zip
		 $zipcode = $zip;
		 $country = $country_id;
		// Update the cart's quote.
		$cart = Mage::getSingleton('checkout/cart');
		$address = $cart->getQuote()->getShippingAddress();
		$address->setCountryId($country)
				->setCity($citydefaultname)
				->setPostcode($zipcode)
				->setRegionId($region_id)
				->setRegion($regionName)
				->setCollectShippingrates(true);
		$cart->save();
//echo Mage::getStoreConfig('shipdate/shipdate_general/holidays_options'); 
	//end of  code to update address from product page inputs
//------------------------------------------------------------------------------------------//
// Find if our shipping has been included.
/*$rates = $address->collectShippingRates()
                 ->getGroupedAllShippingRates();
foreach ($rates as $carrier) {
    foreach ($carrier as $rate) {
        print_r($rate->getData());
    }
}*/
        if (empty($this->_rates)) {
            $this->getAddress()->collectShippingRates()->save();

            $groups = $this->getAddress()->getGroupedAllShippingRates();
            /*
            if (!empty($groups)) {
                $ratesFilter = new Varien_Filter_Object_Grid();
                $ratesFilter->addFilter(Mage::app()->getStore()->getPriceFilter(), 'price');

                foreach ($groups as $code => $groupItems) {
                    $groups[$code] = $ratesFilter->filter($groupItems);
                }
            }
            */




            return $this->_rates = $groups;
        }

        return $this->_rates;
    }

    public function getAddress()
    {
        if (empty($this->_address)) {
            $this->_address = $this->getQuote()->getShippingAddress();
        }
        return $this->_address;
    }

    public function getCarrierName($carrierCode)
    {
        if ($name = Mage::getStoreConfig('carriers/'.$carrierCode.'/title')) {
            return $name;
        }
        return $carrierCode;
    }

    public function getAddressShippingMethod()
    {
        return $this->getAddress()->getShippingMethod();
    }

    public function getShippingPrice($price, $flag)
    {
        return $this->getQuote()->getStore()->convertPrice(Mage::helper('tax')->getShippingPrice($price, $flag, $this->getAddress()), true);
    }
}
