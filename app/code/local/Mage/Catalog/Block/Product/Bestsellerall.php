<?php 
class Mage_Catalog_Block_Product_Bestsellerall extends Mage_Catalog_Block_Product_Abstract{
    public function __construct(){
        parent::__construct();

 $storeId = (int) Mage::app()->getStore()->getId();
        // Date
        $date = new Zend_Date();
        $toDate = $date->setDay(1)->getDate()->get('Y-MM-dd');
        $fromDate = $date->subMonth(1)->getDate()->get('Y-MM-dd');
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addStoreFilter()
            ->addPriceData()
            ->addTaxPercents()
            ->addUrlRewrite()
            ->setPageSize(6);
        $collection->getSelect()
            ->joinLeft(
                array('aggregation' => $collection->getResource()->getTable('sales/bestsellers_aggregated_monthly')),
                "e.entity_id = aggregation.product_id AND aggregation.store_id={$storeId} AND aggregation.period BETWEEN '{$fromDate}' AND '{$toDate}'",
                array('SUM(aggregation.qty_ordered) AS sold_quantity')
            )
            ->group('e.entity_id')
            ->order(array('sold_quantity DESC', 'e.created_at'));
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        //Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
$collection->setPageSize(30)->setCurPage(1);   
$this->setProductCollection($collection);        
/*return $collection;

        $storeId = Mage::app()->getStore()->getId();
        $visibility = array(
                       Mage_Catalog_Model_Product_visibility::VISIBILITY_BOTH
                       );

        $products = Mage::getResourceModel('reports/product_collection')
            ->addOrderedQty()
            ->addAttributeToSelect('*')
            ->addAttributeToSelect(array('name', 'price', 'small_image'))
			->addAttributeToFilter('visibility', $visibility)
            ->setOrder('ordered_qty', 'desc'); // most best sellers on top
        // Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        $products->setPageSize(8)->setCurPage(1);
        $this->setProductCollection($products);*/
    }
}