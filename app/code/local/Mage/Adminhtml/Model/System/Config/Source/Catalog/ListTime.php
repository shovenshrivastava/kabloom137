<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Mage_Adminhtml_Model_System_Config_Source_Catalog_ListTime
{
    public function toOptionArray()
    {
        return array(
            //array('value'=>'', 'label'=>''),
			 array('value'=>'01:00', 'label'=>Mage::helper('adminhtml')->__('01:00')),
			  array('value'=>'02:00', 'label'=>Mage::helper('adminhtml')->__('02:00')),
			   array('value'=>'03:00', 'label'=>Mage::helper('adminhtml')->__('03:00')),
			    array('value'=>'04:00', 'label'=>Mage::helper('adminhtml')->__('04:00')),
				 array('value'=>'05:00', 'label'=>Mage::helper('adminhtml')->__('05:00')),
				  array('value'=>'06:00', 'label'=>Mage::helper('adminhtml')->__('06:00')),
				   array('value'=>'07:00', 'label'=>Mage::helper('adminhtml')->__('07:00')),
				    array('value'=>'08:00', 'label'=>Mage::helper('adminhtml')->__('08:00')),
					 array('value'=>'09:00', 'label'=>Mage::helper('adminhtml')->__('09:00')),
					  array('value'=>'10:00', 'label'=>Mage::helper('adminhtml')->__('10:00')),
					   array('value'=>'11:00', 'label'=>Mage::helper('adminhtml')->__('11:00')),				  
            array('value'=>'12:00', 'label'=>Mage::helper('adminhtml')->__('12:00')),
            array('value'=>'13:00', 'label'=>Mage::helper('adminhtml')->__('13:00')),
			array('value'=>'14:00', 'label'=>Mage::helper('adminhtml')->__('14:00')),
			array('value'=>'15:00', 'label'=>Mage::helper('adminhtml')->__('15:00')),
			array('value'=>'16:00', 'label'=>Mage::helper('adminhtml')->__('16:00')),
			array('value'=>'17:00', 'label'=>Mage::helper('adminhtml')->__('17:00')),
			array('value'=>'18:00', 'label'=>Mage::helper('adminhtml')->__('18:00')),
			array('value'=>'19:00', 'label'=>Mage::helper('adminhtml')->__('19:00')),
			array('value'=>'20:00', 'label'=>Mage::helper('adminhtml')->__('20:00')),
			array('value'=>'21:00', 'label'=>Mage::helper('adminhtml')->__('21:00')),
			array('value'=>'22:00', 'label'=>Mage::helper('adminhtml')->__('22:00')),
			array('value'=>'23:00', 'label'=>Mage::helper('adminhtml')->__('23:00')),
			array('value'=>'24:00', 'label'=>Mage::helper('adminhtml')->__('24:00')),
        );
    }
}
