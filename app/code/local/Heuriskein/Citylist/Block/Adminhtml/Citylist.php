<?php
class Heuriskein_Citylist_Block_Adminhtml_Citylist extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_citylist';
    $this->_blockGroup = 'citylist';
    $this->_headerText = Mage::helper('citylist')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('citylist')->__('Add Item');
    parent::__construct();
  }
}