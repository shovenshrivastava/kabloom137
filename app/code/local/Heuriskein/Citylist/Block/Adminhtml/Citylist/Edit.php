<?php

class Heuriskein_Citylist_Block_Adminhtml_Citylist_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'citylist';
        $this->_controller = 'adminhtml_citylist';
        
        $this->_updateButton('save', 'label', Mage::helper('citylist')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('citylist')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('citylist_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'citylist_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'citylist_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('citylist_data') && Mage::registry('citylist_data')->getId() ) {
            return Mage::helper('citylist')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('citylist_data')->getTitle()));
        } else {
            return Mage::helper('citylist')->__('Add Item');
        }
    }
}