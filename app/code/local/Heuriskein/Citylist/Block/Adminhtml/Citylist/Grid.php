<?php

class Heuriskein_Citylist_Block_Adminhtml_Citylist_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('citylistGrid');
      $this->setDefaultSort('citylist_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('citylist/citylist')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('citylist_id', array(
          'header'    => Mage::helper('citylist')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'citylist_id',
      ));

          $this->addColumn('default_name', array(
          'header'    => Mage::helper('citylist')->__('City Name'),
          'align'     =>'left',
          'index'     => 'default_name',
      ));
	   $this->addColumn('country_id', array(
          'header'    => Mage::helper('citylist')->__('Country Id'),
          'align'     =>'left',
		   'width'     => '50px',
          'index'     => 'country_id',
      ));

	   $this->addColumn('citytype', array(
          'header'    => Mage::helper('citylist')->__('City Type'),
          'align'     =>'right',
          'width'     => '100px',
          'index'     => 'citytype',
      ));
	  $this->addColumn('zipcode', array(
          'header'    => Mage::helper('citylist')->__('Zipcode'),
          'align'     =>'right',
          'width'     => '100px',
          'index'     => 'zipcode',
      ));
	    $this->addColumn('area_code', array(
          'header'    => Mage::helper('citylist')->__('Area Code'),
          'align'     =>'right',
          'width'     => '100px',
          'index'     => 'area_code',
      ));
	  
	     $this->addColumn('cityaliasname', array(
          'header'    => Mage::helper('citylist')->__('City Alias Name'),
          'align'     =>'right',
          'width'     => '100px',
          'index'     => 'cityaliasname',
      ));

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('citylist')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      $this->addColumn('status', array(
          'header'    => Mage::helper('citylist')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('citylist')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('citylist')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('citylist')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('citylist')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('citylist_id');
        $this->getMassactionBlock()->setFormFieldName('citylist');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('citylist')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('citylist')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('citylist/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('citylist')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('citylist')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}