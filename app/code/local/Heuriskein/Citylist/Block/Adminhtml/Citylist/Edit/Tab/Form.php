<?php

class Heuriskein_Citylist_Block_Adminhtml_Citylist_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('citylist_form', array('legend'=>Mage::helper('citylist')->__('Item information')));
     
         $fieldset->addField('default_name', 'text', array(
          'label'     => Mage::helper('citylist')->__('City Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'default_name',
      ));
   

  $fieldset->addField('country_id', 'select', array(
        'name'  => 'country_id',
        'label'     => 'Country Id',
		  'class'     => 'required-entry',
          'required'  => true,
        'values'    => Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(), 
    ));
	
    $fieldset->addField('region_id', 'text', array(
	'name' => 'region_id',
   'label' => Mage::helper('citylist')->__('State'),
    'class'     => 'required-entry',
    'required' => true,
    ));
	
	   $fieldset->addField('zipcode', 'text', array(
          'label'     => Mage::helper('citylist')->__('Zipcode'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'zipcode',
      ));
	   $fieldset->addField('citytype', 'text', array(
          'label'     => Mage::helper('citylist')->__('City type'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'citytype',
      ));
	  $fieldset->addField('area_code', 'text', array(
          'label'     => Mage::helper('citylist')->__('Area Code'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'area_code',
        ));
		
		  $fieldset->addField('cityaliasname', 'text', array(
         'label'     => Mage::helper('citylist')->__('City Alias Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'cityaliasname',
        ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('citylist')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('citylist')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('citylist')->__('Disabled'),
              ),
          ),
      ));
     
    
     
      if ( Mage::getSingleton('adminhtml/session')->getCitylistData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getCitylistData());
          Mage::getSingleton('adminhtml/session')->setCitylistData(null);
      } elseif ( Mage::registry('citylist_data') ) {
          $form->setValues(Mage::registry('citylist_data')->getData());
      }
      return parent::_prepareForm();
  }
}