<?php

class Heuriskein_Citylist_Block_Adminhtml_Citylist_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('citylist_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('citylist')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('citylist')->__('Item Information'),
          'title'     => Mage::helper('citylist')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('citylist/adminhtml_citylist_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}