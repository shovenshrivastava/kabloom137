<?php
class Heuriskein_Citylist_Block_Citylist extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getCitylist()     
     { 
        if (!$this->hasData('citylist')) {
            $this->setData('citylist', Mage::registry('citylist'));
        }
        return $this->getData('citylist');
        
    }
}