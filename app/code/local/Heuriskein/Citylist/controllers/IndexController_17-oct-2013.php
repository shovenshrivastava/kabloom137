<?php
class Heuriskein_Citylist_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/cities?id=15 
    	 *  or
    	 * http://site.com/cities/id/15 	
    	 */
    	/* 
		$cities_id = $this->getRequest()->getParam('id');

  		if($cities_id != null && $cities_id != '')	{
			$cities = Mage::getModel('cities/cities')->load($cities_id)->getData();
		} else {
			$cities = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($cities == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$citiesTable = $resource->getTableName('cities');
			
			$select = $read->select()
			   ->from($citiesTable,array('cities_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$cities = $read->fetchRow($select);
		}
		Mage::register('cities', $cities);
		*/

			
		$this->loadLayout();     
		$this->renderLayout();
    }
	
public function delayAction()
    {
		//echo 'yes now you are in delay action ';die;
		$this->loadLayout();     
		$this->renderLayout();
    }
public function shippingaddrAction(){
		$shipcount = $_REQUEST['shipcount']; ;
		echo $html = '<div class="firecheckout-section" id="ship_'.$shipcount.'">
					  <div class="" id="shipping-address'.$shipcount.'">
						<ul class="form-list">
						   <li><a href="javascript:void(0);" onClick="removeShipping('.$shipcount.'); return false;">Delete Shipping </a></li>	
						  <li id="shipping-new-address-form">
							<fieldset>
							  <input type="hidden" id="shipping:address_id'.$shipcount.'" value="320" name="shipping[address_id]">
							  <ul>
								<li class="fields">
								  <div class="customer-name">
									<div class="field name-firstname">
									  <label class="required" for="shipping:firstname"><em>*</em>First Name</label>
									  <div class="input-box">
										<input type="text"  class="input-text required-entry" maxlength="255" title="First Name" value="" name="shipping[firstname][]" id="shipping:firstname'.$shipcount.'">
									  </div>
									</div>
									<div class="field name-lastname">
									  <label class="required" for="shipping:lastname"><em>*</em>Last Name</label>
									  <div class="input-box">
										<input type="text"  class="input-text required-entry" maxlength="255" title="Last Name" value="" name="shipping[lastname][]" id="shipping:lastname'.$shipcount.'">
									  </div>
									</div>
								  </div>
								</li>
								<li class="fields">
								  <div class="fields">
									<label for="shipping:company">Company</label>
									<div class="input-box">
									  <input type="text"  class="input-text" title="Company" value="" name="shipping[company][]" id="shipping:company'.$shipcount.'">
									</div>
								  </div>
								</li>
								<li class="wide">
								  <label class="required" for="shipping:street1"><em>*</em>Address</label>
								  <div class="input-box">
									<input type="text" class="input-text required-entry" value="" id="shipping:street1'.$shipcount.'" name="shipping[street][]" title="Street Address">
								  </div>
								</li>
								<li class="wide">
								  <div class="input-box">
									<input type="text" class="input-text" value="" id="shipping:street2'.$shipcount.'" name="shipping[street][]" title="Street Address 2">
								  </div>
								</li>
								<li class="fields">
								  <div class="field">
									<label class="required" for="shipping:postcode"><em>*</em>Zip/Postal Code</label>
									<div class="input-box">
									  <select class="input-text validate-zip-international required-entry" id="shipping:postcode'.$shipcount.'" name="shipping[postcode][]" title="Zip/Postal Code">
										<option value="110001">110001</option>
										<option value="110002">110002</option>
									  </select>
									</div>
								  </div>
								  <div class="field">
									<label class="required" for="shipping:city"><em>*</em>City</label>
									<div class="input-box">
									  <input type="text" id="shipping:city'.$shipcount.'" class="input-text required-entry" value="New Delhi" name="shipping[city][]" title="City">
									</div>
								  </div>
								</li>
								<li class="fields">
								  <div class="field">
									<label class="required" for="shipping:country_id"><em>*</em>Country</label>
									<div class="input-box">
									  <input type="text" class="input-text required-entry" title="Country" value="India" name="shipping[country_id][]" id="shipping:country_id'.$shipcount.'">
									</div>
								  </div>
								  <div class="field">
									<label class="required" for="shipping:region"><em>*</em>State/Province</label>
									<div class="input-box">
									  <input type="text" class="input-text required-entry" title="State/Province" value="Uttarakhand" name="shipping[region_id][]" id="shipping:region_id'.$shipcount.'">
									</div>
								  </div>
								</li>
								<li class="fields">
								  <div class="field">
									<label class="required" for="shipping:telephone"><em>*</em>Telephone</label>
									<div class="input-box">
									  <input type="text" id="shipping:telephone'.$shipcount.'" class="input-text required-entry" title="Telephone" value="" name="shipping[telephone][]">
									</div>
								  </div>
								  <div class="field">
									<label for="shipping:fax">Fax</label>
									<div class="input-box">
									  <input type="text" id="shipping:fax'.$shipcount.'" class="input-text" title="Fax" value="" name="shipping[fax][]">
									</div>
								  </div>
								</li>
								<li class="no-display">
								  <input type="hidden" value="1" name="shipping[save_in_address_book][]">
								</li>
							  </ul>
							</fieldset>
						  </li>
						</ul>
					  </div>
					</div>';
					die;
		$this->loadLayout();     
		$this->renderLayout();
	}
public function customeshipAction(){
		$this->loadLayout();     
		$this->renderLayout();
	}
}