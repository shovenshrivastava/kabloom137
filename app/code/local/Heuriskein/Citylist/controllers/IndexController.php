<?php
class Heuriskein_Citylist_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/cities?id=15 
    	 *  or
    	 * http://site.com/cities/id/15 	
    	 */
    	/* 
		$cities_id = $this->getRequest()->getParam('id');

  		if($cities_id != null && $cities_id != '')	{
			$cities = Mage::getModel('cities/cities')->load($cities_id)->getData();
		} else {
			$cities = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($cities == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$citiesTable = $resource->getTableName('cities');
			
			$select = $read->select()
			   ->from($citiesTable,array('cities_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$cities = $read->fetchRow($select);
		}
		Mage::register('cities', $cities);
		*/

			
		$this->loadLayout();     
		$this->renderLayout();
    }
	
public function delayAction()
    {
		//echo 'yes now you are in delay action ';die;
		$this->loadLayout();     
		$this->renderLayout();
    }
public function shippingaddrAction(){ 
		$shipcount = $_REQUEST['shipcount'];
		$itemId = $_REQUEST['itemId'];
			$quote = Mage::getSingleton('checkout/session')->getQuote();
			
			$cartItems = $quote->getAllVisibleItems();
			
			
			/*$cart = Mage::getModel('checkout/cart')->getQuote();
			
			foreach ($cart->getAllItems() as $item) {
				//echo $productName = $item->getProduct()->getName();
				//$productPrice = $item->getProduct()->getPrice();
				
				if ($itemId == $item->getItemId()){
					$itemsCity = $item->getCities();
					$itemDate = $item->getShipping();
					$related='style="display:block"';
					$first_item = $item;
					break;
					
				}else{ 
					$itemsCity = $item->getCities();
					$itemDate = $item->getShipping();
					$first_item = $item;
					$related='style="display:none"';
					
				}
				
				
			}*/
			foreach ($cartItems as $key => $item) { 
				if ($itemId == $item->getItemId()){ 
						  $itemsCity = $item->getCities();
						  $itemDate = $item->getShipping(); 
						  $first_item = $item;
						  /*if ($item->getProduct()->isConfigurable()){ 
							$related='style="display:block"';
						  }else{
						  $related='style="display:none"';
						  }*/
						  break;
					}
					
			}
			
			
			$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
			//$sql        = "Select DISTINCT zipcode from citylist where default_name='".$itemsCity."'";
			$sql        = "Select DISTINCT zipcode from citylist";
			$rows       = $connection->fetchAll($sql); 

			$optionzip = "";
			foreach($rows as $zipvalue){
				$optionzip.='<option value="'.$zipvalue['zipcode'].'">'.$zipvalue['zipcode'].'</option>';
			}
			
		$ordernumber = ++$shipcount;	
		if(!empty($first_item)){
		echo $html = '<div class="firecheckout-section" id="ship_'.$shipcount.'" '.$related.'>
					  <div class="" id="shipping-address'.$shipcount.'"><div style="height:70px;" class="block-title"><span>Deliver My '.$ordernumber.' order to :<img width="70" height="70" src="'.Mage::helper('catalog/image')->init($first_item->getProduct(), 'thumbnail').'" style="display:inline-block; float:right; padding-right:5px;"><br>'.$first_item->getName().'</span></div>
						<ul class="form-list">
						   <!-- <li><a href="javascript:void(0);" onClick="removeShipping('.$shipcount.'); return false;">Delete Shipping </a></li>	 -->
						  <li id="shipping-new-address-form">
							<fieldset>
							  <input type="hidden" id="shipping:address_id'.$shipcount.'" value="320" name="shipping[address_id]">
							  <ul>
								<li class="fields">
								  <div class="customer-name">
									<div class="field name-firstname">
									  <label class="required" for="shipping:firstname"><em>*</em>First Name</label>
									  <div class="input-box">
										<input type="text"  class="input-text required-entry" maxlength="255" title="First Name" value="" name="shipping[firstname][]" id="shipping:firstname'.$shipcount.'">
									  </div>
									</div>
									<div class="field name-lastname">
									  <label class="required" for="shipping:lastname"><em>*</em>Last Name</label>
									  <div class="input-box">
										<input type="text"  class="input-text required-entry" maxlength="255" title="Last Name" value="" name="shipping[lastname][]" id="shipping:lastname'.$shipcount.'">
									  </div>
									</div>
								  </div>
								</li>
								<li class="fields">
								  <div class="fields">
									<label for="shipping:company">Company</label>
									<div class="input-box">
									  <input type="text"  class="input-text" title="Company" value="" name="shipping[company][]" id="shipping:company'.$shipcount.'">
									</div>
								  </div>
								</li>
								<li class="fields">
								  <div class="fields">
									<label for="shipping:company">Delivery Date</label>
									<div class="input-box">
									  <input type="text" readonly="readonly"  class="input-text" title="Company" value="'.$itemDate.'" name="shipping[shippingdate][]" id="shipping:shippingdate'.$shipcount.'">
									</div>
								  </div>
								</li>
								<li class="wide">
								  <label class="required" for="shipping:street1"><em>*</em>Address</label>
								  <div class="input-box">
									<input type="text" class="input-text required-entry" value="" id="shipping:street1'.$shipcount.'" name="shipping[street][]" title="Street Address">
								  </div>
								</li>
								<li class="wide">
								  <div class="input-box">
									<input type="text" class="input-text" value="" id="shipping:street2'.$shipcount.'" name="shipping[street1][]" title="Street Address 2">
								  </div>
								</li>
								<li class="fields">
								  <div class="field">
									<label class="required" for="shipping:postcode"><em>*</em>Zip/Postal Code</label>
									<div class="input-box">
									 
									  <select class="validate-select" title="Zip/Postal Code" name="shipping[postcode][]" id="shipping:postcode'.$shipcount.'">'.$optionzip.'</select>
									</div>
								  </div>
								  <div class="field">
									<label class="required" for="shipping:city"><em>*</em>City</label>
									<div class="input-box">
									  <input type="text" readonly="readonly" id="shipping:city'.$shipcount.'" class="input-text required-entry" value="'.$itemsCity.'" name="shipping[city][]" title="City">
									</div>
								  </div>
								</li>
								
								<li class="fields">
  <div class="field">
    <label class="required" for="shipping:country_id"><em>*</em>Country</label>
    <div class="input-box">
      <select  title="Country" class="validate-select" name="shipping[country_id][]" id="shipping:country_id'.$shipcount.'"><option selected="selected" value="IN">India</option><option value="AE">United Arab Emirates</option></select>
    </div>
  </div>
	  
</li>
								
								<li class="fields">
								  <div class="field">
									<label class="required" for="shipping:telephone"><em>*</em>Telephone</label>
									<div class="input-box">
									  <input type="text" id="shipping:telephone'.$shipcount.'" class="input-text required-entry" title="Telephone" value="" name="shipping[telephone][]">
									</div>
								  </div>
								  <div class="field">
									<label for="shipping:fax">Fax</label>
									<div class="input-box">
									  <input type="text" id="shipping:fax'.$shipcount.'" class="input-text" title="Fax" value="" name="shipping[fax][]">
									</div>
								  </div>
								</li>
								<li class="no-display">
								  <input type="hidden" value="1" name="shipping[save_in_address_book][]">
								</li>
							  </ul>
							</fieldset>
						  </li>
						</ul>
					  </div>
					</div>';
					}
					die;
		$this->loadLayout();     
		$this->renderLayout();
	}
public function customeshipAction(){
		$this->loadLayout();     
		$this->renderLayout();
	}
}