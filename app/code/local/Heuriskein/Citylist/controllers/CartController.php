<?php require_once 'Mage/Checkout/controllers/CartController.php';  
class Heuriskein_Citylist_CartController extends Mage_Checkout_CartController  
{
	public function addAction()
	{
		
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();


	       try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();


// code to stop add to cart for same day
$options = $product->getOptions();
$optionvals=$params['options'];
            if ($options) {
                foreach ($options as $option) {
                    if ($option->getTitle() == 'Delivery Date') {
 						$deliveryDateID = $option->getOptionId();
						$dDate = $optionvals[$deliveryDateID];
						$todayDate = Mage::getModel('core/date')->date('m/d/Y');
						  if($dDate==$todayDate){
							//header('Location: ' . $_SERVER['HTTP_REFERER'].'?q=sameday');
						   //exit;
							}  		
                     }
				}
			}

// end of code to stop add to cart for same day


			//print_r($product);
	//  ===  for managing custom option  ==============  //
           
       		//  ==============  custom option manage end here  ============  //		
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

			//  ==========   Custom code added here  ================  //
            if ($options) {
                foreach ($options as $option) {
                    if ($option->getTitle() == 'Delivery Date') {
 						$deliveryDateID = $option->getOptionId();
						$deliveryDate = $optionvals[$deliveryDateID];
						$dDate = $deliveryDate;
						$todayDate = Mage::getModel('core/date')->date('m/d/Y');
						  if($dDate==$todayDate){
							//header('Location: ' . $_SERVER['HTTP_REFERER'].'?q=sameday');
						   //exit;
							}  		
                     }
					if ($option->getTitle() == 'Delivery City') {
						$zipcodeID=$option->getOptionId();
						$zipcode=$optionvals[$zipcodeID];	
                    }
					if ($option->getTitle() == 'Location Type') {
                        $deliveryLocationID = $option->getOptionId();
						$deliveryLocation = $optionvals[$deliveryLocationID];
                    }
                }
            }
			$cartItems = $cart->getItems()->getData();
			$cartItemsIndex = (int) (count($cartItems) - 1);
			$quoteItemId = $cartItems[$cartItemsIndex]['item_id'];
			$quote = Mage::getSingleton('checkout/session')->getQuote();
			$_cart = Mage::getSingleton( 'checkout/cart' );
			$cartItems = $quote->getAllVisibleItems();
			foreach ($_cart->getItems() as $item) {
			  	if($quoteItemId==$item->getId()){
					$item->setData('cities', $zipcode);
					$item->setData('shipping', $deliveryDate);
					$item->setData('LocationType', $deliveryLocation);
					$item->save();
				}
				
				if($item->getProduct()->isConfigurable()){
				$item->setData('cities', $zipcode);
					$item->setData('shipping', $deliveryCity);
					$item->setData('LocationType', $deliveryLocation);
								$item->save();
				}
			}
			//  ==============  Custom code End here ================= //

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
	}
 /**
     * Update product configuration for a cart item
     */
    public function updateItemOptionsAction()
    {
        $cart   = $this->_getCart();
        $id = (int) $this->getRequest()->getParam('id');
        $params = $this->getRequest()->getParams();

        if (!isset($params['options'])) {
            $params['options'] = array();
        }
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $quoteItem = $cart->getQuote()->getItemById($id);
            if (!$quoteItem) {
                Mage::throwException($this->__('Quote item is not found.'));
            }

            $item = $cart->updateItem($id, new Varien_Object($params));
            if (is_string($item)) {
                Mage::throwException($item);
            }
            if ($item->getHasError()) {
                Mage::throwException($item->getMessage());
            }

            $related = $this->getRequest()->getParam('related_product');
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);
		//  ==========   Custom code added here  ================  //
$product = $this->_initProduct();
 $options = $product->getOptions();
//print_r($options); 
$optionvals=$params['options'];
//print_r($optionval); 
            if ($options) {
                foreach ($options as $option) {
                    if ($option->getTitle() == 'Delivery Date') {
 $deliveryDateID = $option->getOptionId();
						$deliveryDate = $optionvals[$deliveryDateID];
						

                     }
					if ($option->getTitle() == 'Delivery City') {
	$zipcodeID=$option->getOptionId();
						$zipcode=$optionvals[$zipcodeID];	
                       
                    }
					if ($option->getTitle() == 'Location Type') {
                        $deliveryLocationID = $option->getOptionId();
						$deliveryLocation = $optionvals[$deliveryLocationID];
                    }
                }
            }
			$cartItems = $cart->getItems()->getData();
			$cartItemsIndex = (int) (count($cartItems) - 1);
			$quoteItemId = $cartItems[$cartItemsIndex]['item_id'];
			$quote = Mage::getSingleton('checkout/session')->getQuote();
			$_cart = Mage::getSingleton( 'checkout/cart' );
			$cartItems = $quote->getAllVisibleItems();
			foreach ($_cart->getItems() as $item) {
			  	if($quoteItemId==$item->getId()){
					$item->setData('cities', $zipcode);
					$item->setData('shipping', $deliveryDate);
					$item->setData('LocationType', $deliveryLocation);
					$item->save();
				}
				
				if($item->getProduct()->isConfigurable()){
				$item->setData('cities', $zipcode);
					$item->setData('shipping', $deliveryCity);
					$item->setData('LocationType', $deliveryLocation);
								$item->save();
				}
			}
			//  ==============  Custom code End here ================= //

            Mage::dispatchEvent('checkout_cart_update_item_complete',
                array('item' => $item, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );
            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was updated in your shopping cart.', Mage::helper('core')->htmlEscape($item->getProduct()->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice($e->getMessage());
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError($message);
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot update the item.'));
            Mage::logException($e);
            $this->_goBack();
        }
        $this->_redirect('*/*');
    }
	
/*	public function saveProductOption($product)
    {     
$options = array(
        array(
            'is_delete' => 0,
            'is_require' => false,
            'previous_group' => 'text',
            'title' => 'Delivery Date',
            'type' => 'field',
            'price_type' => 'fixed',
            'price' => '0.0000'
        ),
        array(
           'is_delete' => 0,
            'is_require' => false,
            'previous_group' => 'text',
            'title' => 'Delivery City',
            'type' => 'field',
            'price_type' => 'fixed',
            'price' => '0.0000'
        ),
		array(
  			 'is_delete' => 0,
            'is_require' => false,
            'previous_group' => 'text',
            'title' => 'Location Type',
            'type' => 'field',
            'price_type' => 'fixed',
            'price' => '0.0000'
		)
);
      $product->setHasOptions(1)->save();
		foreach($options as $option_data){
			$option = Mage::getModel('catalog/product_option')
				->setProductId($product->getId())
				->addData($option_data);
			$value = Mage::getModel('catalog/product_option_value');
			$value->setOption($option);
			$option->addValue($value);
			$option->save();
			$product->addOption($option);
		}
  	//$product->save();
$product_id=$product->getId();
$optionTitle="Delivery Date";
citylist_get_custom_option_id($product_id, $optionTitle);
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load($store));
        return $optionIDDate;
    }*/
		
	public function saveProductOptionDate($product)
    {
        $store = Mage::app()->getStore()->getId();
        $optionData = array(
            'is_delete' => 0,
            'is_require' => false,
            'previous_group' => 'text',
            'title' => 'Delivery Date',
            'type' => 'field',
            'price_type' => 'fixed',
            'price' => '0.0000'
        );

      $product->setHasOptions(1);
      $product->setCanSaveCustomOptions(1);
      $product->setOptions(array($optionData));
      $product->setProductOptions(array($optionData));

      $opt = Mage::getSingleton('catalog/product_option');
      $opt->setProduct($product);
      $opt->addOption($optionData);
      $opt->saveOptions();
      $product->setOption($opt);
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
             //$product->getResource()->save($product);
		$product_id=$product->getId();
		$optionTitle='Delivery Date';
		$optionIDDate = $this->citylist_get_custom_option_id($product_id, $optionTitle);
	Mage::app()->setCurrentStore(Mage::getModel('core/store')->load($store));
        return $optionIDDate;
    }
	
	public function saveProductOptionCity($product)
    {
        $store = Mage::app()->getStore()->getId();
       
        $optionData = array(
            'is_delete' => 0,
            'is_require' => false,
            'previous_group' => 'text',
            'title' => 'Delivery City',
            'type' => 'field',
            'price_type' => 'fixed',
            'price' => '0.0000'
        );
  	  $product->setHasOptions(1);
      $product->setCanSaveCustomOptions(1);
      $product->setOptions(array($optionData));
      $product->setProductOptions(array($optionData));

      $opt = Mage::getSingleton('catalog/product_option');
      $opt->setProduct($product);
      $opt->addOption($optionData);
      $opt->saveOptions();
      $product->setOption($opt);    

      Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));

       //$product->getResource()->save($product);
   
      $product_id=$product->getId();
$optionTitle='Delivery City';
$optionIDCity = $this->citylist_get_custom_option_id($product_id, $optionTitle);
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load($store));
        return $optionIDCity;
    }

	public function saveProductOptionLocation($product)
    {
        $store = Mage::app()->getStore()->getId();
        $optionData = array(
            'is_delete' => 0,
            'is_require' => false,
            'previous_group' => 'text',
            'title' => 'Location Type',
            'type' => 'field',
            'price_type' => 'fixed',
            'price' => '0.0000'
        );

   	  $product->setHasOptions(1);
      $product->setCanSaveCustomOptions(1);
      $product->setOptions(array($optionData));
      $product->setProductOptions(array($optionData));

      $opt = Mage::getSingleton('catalog/product_option');
      $opt->setProduct($product);
      $opt->addOption($optionData);
      $opt->saveOptions();
      $product->setOption($opt);
     
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
    
       //$product->getResource()->save($product);
      
       $product_id=$product->getId();
$optionTitle='Location Type';
$optionIDLocation = $this->citylist_get_custom_option_id($product_id, $optionTitle);
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load($store));
        return $optionIDLocation;
    }
private function citylist_get_custom_option_id($product_id, $optionTitle)
{
    $sql = "
        select 
            o.option_id 
        from 
            catalog_product_option o
        inner join
            catalog_product_option_title t on t.option_id = o.option_id
        where
            t.title = '".$optionTitle."'
            and o.product_id = '".$product_id."'
        limit 1
            ";
    $data = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
    if (count($data) > 0)
    {
        return current($data[0]);
    }
    return false;
}

}