<?php
class Heuriskein_Citylist_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/cities?id=15 
    	 *  or
    	 * http://site.com/cities/id/15 	
    	 */
    	/* 
		$cities_id = $this->getRequest()->getParam('id');

  		if($cities_id != null && $cities_id != '')	{
			$cities = Mage::getModel('cities/cities')->load($cities_id)->getData();
		} else {
			$cities = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($cities == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$citiesTable = $resource->getTableName('cities');
			
			$select = $read->select()
			   ->from($citiesTable,array('cities_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$cities = $read->fetchRow($select);
		}
		Mage::register('cities', $cities);
		*/

			
		$this->loadLayout();     
		$this->renderLayout();
    }
	
public function delayAction()
    {
		//echo 'yes now you are in delay action ';die;
		$this->loadLayout();     
		$this->renderLayout();
    }
public function shippingaddrAction(){
		$shipcount = $_REQUEST['shipcount']; ;
		echo $html = '<div class="firecheckout-section" id="ship_'.$shipcount.'">
					  <div class="" id="shipping-address">
						<ul class="form-list">
						   <li><a href="javascript:void(0);" onClick="removeShipping('.$shipcount.'); return false;">Delete Shipping </a></li>	
						  <li id="shipping-new-address-form">
							<fieldset>
							  <input type="hidden" id="shipping:address_id" value="320" name="shipping[address_id]">
							  <ul>
								<li class="fields">
								  <div class="customer-name">
									<div class="field name-firstname">
									  <label class="required" for="shipping:firstname"><em>*</em>First Name</label>
									  <div class="input-box">
										<input type="text" onChange="shipping.setSameAsBilling(false)" class="input-text required-entry" maxlength="255" title="First Name" value="" name="shipping[firstname]" id="shipping:firstname">
									  </div>
									</div>
									<div class="field name-lastname">
									  <label class="required" for="shipping:lastname"><em>*</em>Last Name</label>
									  <div class="input-box">
										<input type="text" onChange="shipping.setSameAsBilling(false)" class="input-text required-entry" maxlength="255" title="Last Name" value="" name="shipping[lastname]" id="shipping:lastname">
									  </div>
									</div>
								  </div>
								</li>
								<li class="fields">
								  <div class="fields">
									<label for="shipping:company">Company</label>
									<div class="input-box">
									  <input type="text" onChange="shipping.setSameAsBilling(false);" class="input-text" title="Company" value="" name="shipping[company]" id="shipping:company">
									</div>
								  </div>
								</li>
								<li class="wide">
								  <label class="required" for="shipping:street1"><em>*</em>Address</label>
								  <div class="input-box">
									<input type="text" onChange="shipping.setSameAsBilling(false);" class="input-text required-entry" value="" id="shipping:street1" name="shipping[street][]" title="Street Address">
								  </div>
								</li>
								<li class="wide">
								  <div class="input-box">
									<input type="text" onChange="shipping.setSameAsBilling(false);" class="input-text" value="" id="shipping:street2" name="shipping[street][]" title="Street Address 2">
								  </div>
								</li>
								<li class="fields">
								  <div class="field">
									<label class="required" for="shipping:postcode"><em>*</em>Zip/Postal Code</label>
									<div class="input-box">
									  <select class="input-text validate-zip-international required-entry" id="shipping:postcode" name="shipping[postcode]" title="Zip/Postal Code">
										<option value="110001">110001</option>
										<option value="110002">110002</option>
										<option value="110003">110003</option>
										<option value="110004">110004</option>
										<option value="110005">110005</option>
										<option value="110006">110006</option>
										<option value="110007">110007</option>
										<option value="110008">110008</option>
										<option value="110009">110009</option>
										<option value="110010">110010</option>
										<option value="110011">110011</option>
										<option value="110012">110012</option>
										<option value="110013">110013</option>
										<option value="110014">110014</option>
										<option value="110015">110015</option>
										<option value="110016">110016</option>
										<option value="110017">110017</option>
										<option value="110018">110018</option>
										<option value="110019">110019</option>
										<option value="110020">110020</option>
										<option value="110021">110021</option>
										<option value="110022">110022</option>
										<option value="110023">110023</option>
										<option value="110024">110024</option>
										<option value="110025">110025</option>
										<option value="110026">110026</option>
										<option value="110027">110027</option>
										<option value="110028">110028</option>
										<option value="110029">110029</option>
										<option value="110030">110030</option>
										<option value="110033">110033</option>
										<option value="110034">110034</option>
										<option value="110035">110035</option>
										<option value="110036">110036</option>
										<option value="110037">110037</option>
										<option value="110038">110038</option>
										<option value="110039">110039</option>
										<option value="110040">110040</option>
										<option value="110041">110041</option>
										<option value="110042">110042</option>
										<option value="110043">110043</option>
										<option value="110044">110044</option>
										<option value="110045">110045</option>
										<option value="110046">110046</option>
										<option value="110047">110047</option>
										<option value="110048">110048</option>
										<option value="110049">110049</option>
										<option value="110052">110052</option>
										<option value="110054">110054</option>
										<option value="110055">110055</option>
										<option value="110056">110056</option>
										<option value="110057">110057</option>
										<option value="110058">110058</option>
										<option value="110059">110059</option>
										<option value="110060">110060</option>
										<option value="110061">110061</option>
										<option value="110062">110062</option>
										<option value="110063">110063</option>
										<option value="110064">110064</option>
										<option value="110065">110065</option>
										<option value="110066">110066</option>
										<option value="110067">110067</option>
										<option value="110068">110068</option>
										<option value="110069">110069</option>
										<option value="110070">110070</option>
										<option value="110071">110071</option>
										<option value="110072">110072</option>
										<option value="110073">110073</option>
										<option value="110074">110074</option>
										<option value="110075">110075</option>
										<option value="110076">110076</option>
										<option value="110077">110077</option>
										<option value="110078">110078</option>
										<option value="110081">110081</option>
										<option value="110082">110082</option>
										<option value="110083">110083</option>
										<option value="110084">110084</option>
										<option value="110085">110085</option>
										<option value="110086">110086</option>
										<option value="110087">110087</option>
										<option value="110088">110088</option>
										<option value="110089">110089</option>
										<option value="110091">110091</option>
										<option value="110092">110092</option>
										<option value="110096">110096</option>
										<option value="110101">110101</option>
										<option value="110103">110103</option>
										<option value="110104">110104</option>
										<option value="110105">110105</option>
										<option value="110106">110106</option>
										<option value="110107">110107</option>
										<option value="110108">110108</option>
										<option value="110109">110109</option>
										<option value="110110">110110</option>
										<option value="110112">110112</option>
										<option value="110113">110113</option>
										<option value="110114">110114</option>
										<option value="110115">110115</option>
										<option value="110116">110116</option>
										<option value="110117">110117</option>
										<option value="110118">110118</option>
										<option value="110119">110119</option>
										<option value="110120">110120</option>
										<option value="110122">110122</option>
										<option value="110124">110124</option>
										<option value="110125">110125</option>
										<option value="110301">110301</option>
										<option value="110302">110302</option>
										<option value="110501">110501</option>
										<option value="110502">110502</option>
										<option value="110503">110503</option>
										<option value="110504">110504</option>
										<option value="110510">110510</option>
										<option value="110511">110511</option>
										<option value="110512">110512</option>
										<option value="110601">110601</option>
										<option value="110602">110602</option>
										<option value="110603">110603</option>
										<option value="110604">110604</option>
										<option value="110605">110605</option>
										<option value="110606">110606</option>
										<option value="110607">110607</option>
										<option value="110608">110608</option>
										<option value="110609">110609</option>
										<option value="263139">263139</option>
									  </select>
									</div>
								  </div>
								  <div class="field">
									<label class="required" for="shipping:city"><em>*</em>City</label>
									<div class="input-box">
									  <input type="text" onChange="shipping.setSameAsBilling(false);" id="shipping:city" class="input-text required-entry" readonly="readonly" value="New Delhi" name="shipping[city]" title="City">
									</div>
								  </div>
								</li>
								<li class="fields">
								  <div class="field">
									<label class="required" for="shipping:country_id"><em>*</em>Country</label>
									<div class="input-box">
									  <input type="text" class="input-text required-entry" readonly="readonly" title="Country" value="India" name="shipping[country_id]" id="shipping:country_id">
									</div>
								  </div>
								  <div class="field">
									<label class="required" for="shipping:region"><em>*</em>State/Province</label>
									<div class="input-box">
									  <input type="text" class="input-text required-entry" readonly="readonly" title="State/Province" value="Uttarakhand" name="shipping[region_id]" id="shipping:region_id">
									</div>
								  </div>
								</li>
								<li class="fields">
								  <div class="field">
									<label class="required" for="shipping:telephone"><em>*</em>Telephone</label>
									<div class="input-box">
									  <input type="text" onChange="shipping.setSameAsBilling(false);" id="shipping:telephone" class="input-text required-entry" title="Telephone" value="" name="shipping[telephone]">
									</div>
								  </div>
								  <div class="field">
									<label for="shipping:fax">Fax</label>
									<div class="input-box">
									  <input type="text" onChange="shipping.setSameAsBilling(false);" id="shipping:fax" class="input-text" title="Fax" value="" name="shipping[fax]">
									</div>
								  </div>
								</li>
								<li class="no-display">
								  <input type="hidden" value="1" name="shipping[save_in_address_book]">
								</li>
							  </ul>
							</fieldset>
						  </li>
						</ul>
					  </div>
					</div>';
					die;
		$this->loadLayout();     
		$this->renderLayout();
	}
public function customeshipAction(){
		$this->loadLayout();     
		$this->renderLayout();
	}
}