<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class Heuriskein_Citylist_CartController extends Mage_Checkout_CartController
{
	public function addAction()
	{
		
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
		
		
		
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
			
	//  ===  for managing custom option  ==============  //
			if (!$product->getHasOptions()) {
            		$optionID = $this->saveProductOption($product);
					$optionIDCity = $this->saveProductOptionCity($product);
       		 } else {
            $options = $product->getOptions();
            if ($options) {
                foreach ($options as $option) {
                    if ($option->getTitle() == 'Delivery Date') {
                        $optionID = $option->getOptionId();
                    }
					if ($option->getTitle() == 'Delivery City') {
                        $optionIDCity = $option->getOptionId();
                    }
                }
            }
            if(empty($optionID)) {
                $optionID = $this->saveProductOption($product);
            }
			if(empty($optionIDCity)) {
                $optionIDCity = $this->saveProductOptionCity($product);
            }
        }
			$deliveryDate = $params['shipping'];
			$deliveryCity = $params['cities'];
		if (!empty($deliveryCity) || !empty($deliveryDate)){	
	  		 $optionData = array($optionID => $deliveryDate,$optionIDCity => $deliveryCity);
	  		 $params['options'] = $optionData;
		}
	//  ==============  custom option manage end here  ============  //		
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

			//  ==========   Custom code added here  ================  //
			$cartItems = $cart->getItems()->getData();
			$cartItemsIndex = (int) (count($cartItems) - 1);
			$quoteItemId = $cartItems[$cartItemsIndex]['item_id'];
			$quote = Mage::getSingleton('checkout/session')->getQuote();
			$_cart = Mage::getSingleton( 'checkout/cart' );
			$cartItems = $quote->getAllVisibleItems();
			foreach ($_cart->getItems() as $item) {
			  	if($quoteItemId==$item->getId()){
					$item->setData('cities', $params['cities']);
					$item->setData('shipping', $params['shipping']);
					$item->save();
				}
				
				if($item->getProduct()->isConfigurable()){
								$item->setData('cities', $params['cities']);
								$item->setData('shipping', $params['shipping']);
						
								$item->save();
				}
			}
			//  ==============  Custom code End here ================= //

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
	}
	
	
	public function saveProductOption($product)
    {

        $store = Mage::app()->getStore()->getId();
        $opt = Mage::getModel('catalog/product_option');
        $opt->setProduct($product);
        $option = array(
            'is_delete' => 0,
            'is_require' => false,
            'previous_group' => 'text',
            'title' => 'Delivery Date',
            'type' => 'field',
            'price_type' => 'fixed',
            'price' => '0.0000'
        );
        $opt->addOption($option);
        $opt->saveOptions();
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
        $product->setHasOptions(1);
        $product->save();

        $options = $product->getOptions();
        if ($options) {
            foreach ($options as $option) {
                if ($option->getTitle() == 'Delivery Date') {
                    $optionID = $option->getOptionId();
                }
            }
        }
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load($store));
        return $optionID;
    }
	
	public function saveProductOptionCity($product)
    {
        $store = Mage::app()->getStore()->getId();
        $opt = Mage::getModel('catalog/product_option');
        $opt->setProduct($product);
        $option = array(
            'is_delete' => 0,
            'is_require' => false,
            'previous_group' => 'text',
            'title' => 'Delivery City',
            'type' => 'field',
            'price_type' => 'fixed',
            'price' => '0.0000'
        );
        $opt->addOption($option);
        $opt->saveOptions();
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
        $product->setHasOptions(1);
        $product->save();

        $options = $product->getOptions();
        if ($options) {
            foreach ($options as $option) {
                if ($option->getTitle() == 'Delivery City') {
                    $optionID = $option->getOptionId();
                }
            }
        }
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load($store));
        return $optionIDCity;
    }
}
?> 