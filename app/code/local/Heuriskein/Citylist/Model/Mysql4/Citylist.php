<?php

class Heuriskein_Citylist_Model_Mysql4_Citylist extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the citylist_id refers to the key field in your database table.
        $this->_init('citylist/citylist', 'citylist_id');
    }
}