<?php
//Mage::log('My log entry', null, 'mylogfile.log') or exit("unable to log");
class Heuriskein_Citylist_Model_Observer
{
 public function setProductInfo($observer)
    {
        //check that we haven't made the option already
   $product = $observer->getProduct();
if($product->isObjectNew()){$newProduct=1;}else{$newProduct=0;}
if (!$product->getHasOptions()) {
 $optionIDDate = $this->saveProductOptionDate($product);
$optionIDLocation = $this->saveProductOptionLocation($product);
 $optionIDCity = $this->saveProductOptionCity($product);
  
  }else{   
		$options = $product->getOptions();
            if ($options) {
                foreach ($options as $option) {
                   if ($option->getTitle() == 'Delivery Date') {
                        $optionIDDate = $option->getOptionId();
                    }
					if ($option->getTitle() == 'Location Type') {
                        $optionIDLocation = $option->getOptionId();
                    }
					if ($option->getTitle() == 'Delivery City') {
                        $optionIDCity = $option->getOptionId();
                    }
					
                }// end of foreach
				// print($optionIDDate );print($optionIDCity);print($optionIDLocation);exit();
            }// end of options if
			if(is_null($optionIDDate)) {
                $optionIDDate = $this->saveProductOptionDate($product);
            }
			if(is_null($optionIDCity)) {
                $optionIDCity = $this->saveProductOptionCity($product);
            }
			if(is_null($optionIDLocation)) {
                $optionIDLocation = $this->saveProductOptionLocation($product);
            }
       }//end of else
}//end of function


  	public function saveProductOptionDate($product)
    {
        $store = Mage::app()->getStore()->getId();
      
if($product->isObjectNew()){
 $optionData = array('is_delete' => 0, 'is_require' => false, 'previous_group' => 'text', 'title' => 'Delivery Date', 'type' => 'field', 'price_type' => 'fixed', 'price' => '0.0000', 'sort_order' => 3);
      $product->setHasOptions(1);
      $product->setCanSaveCustomOptions(1);
      $product->setOptions(array($optionData));
      $product->setProductOptions(array($optionData));
		$opt = Mage::getSingleton('catalog/product_option');
		$opt->setProduct($product);
		$opt->addOption($optionData);
		$opt->saveOptions();
		$product->setOption($opt);
			
}/*else{
$option = array(
 'is_delete' => 0,
			 'is_require' => 0,
            'title' => 'Delivery Date',
		     'type' => 'field',
            'price_type' => 'fixed',
			 'price' => '0.0000'
);
// $product = Mage::getModel('catalog/product')->load($productId);
    
$product->setProductOptions($options);
$product->setCanSaveCustomOptions(true);
$product->setHasOptions(true);
//Do not forget to save the product
$product->save();
}*/
	Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
	return true;
    }
	
	public function saveProductOptionCity($product)
    {
        $store = Mage::app()->getStore()->getId();	
        $optionData = array('is_delete' => 0,'is_require' => false, 'previous_group' => 'text', 'title' => 'Delivery City', 'type' => 'field', 'price_type' => 'fixed', 'price' => '0.0000', 'sort_order' => 1);
if($product->isObjectNew()){
		$product->setHasOptions(1);
		$product->setCanSaveCustomOptions(1);
		$product->setOptions(array($optionData));
		$product->setProductOptions(array($optionData));
		$opt = Mage::getSingleton('catalog/product_option');
		$opt->setProduct($product);
		$opt->addOption($optionData);
		$opt->saveOptions();
		$product->setOption($opt);    
}/*else{
$options = array(
        array(
            'is_delete' => 0,
			 'is_require' => 0,
            'title' => 'Delivery City',
		     'type' => 'field',
            'price_type' => 'fixed',
			 'price' => '0.0000'
        )
);
$product->setProductOptions($options);
$product->setCanSaveCustomOptions(true);
//Do not forget to save the product
$product->save();
}*/
		Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
		return true;
    }

	public function saveProductOptionLocation($product)
    {
        $store = Mage::app()->getStore()->getId();
        $optionData = array('is_delete' => 0, 'is_require' => false, 'previous_group' => 'text', 'title' => 'Location Type', 'type' => 'field', 'price_type' => 'fixed', 'price' => '0.0000', 'sort_order' => 2);
if($product->isObjectNew()){
			$product->setHasOptions(1);
		$product->setCanSaveCustomOptions(1);
		$product->setOptions(array($optionData));
		$product->setProductOptions(array($optionData));
		$opt = Mage::getSingleton('catalog/product_option');
		$opt->setProduct($product);
		$opt->addOption($optionData);
		$opt->saveOptions();
		$product->setOption($opt);    
			
}/*else{
$options = array(
        array(
            'is_delete' => 0,
			 'is_require' => 0,
            'title' => 'Delivery Location',
		   'type' => 'field',
            'price_type' => 'fixed',
			 'price' => '0.0000'
        )
);
$product->setProductOptions($options);
$product->setCanSaveCustomOptions(true);
//Do not forget to save the product
$product->save();
}*/
		Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
		return true;
    }
private function citylist_get_custom_option_id($product_id, $optionTitle)
{
    $sql = "
        select 
            o.option_id 
        from 
            catalog_product_option o
        inner join
            catalog_product_option_title t on t.option_id = o.option_id
        where
            t.title = '".$optionTitle."'
            and o.product_id = '".$product_id."'
        limit 1
            ";
    $data = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
    if (count($data) > 0)
    {
        return current($data[0]);
    }
    return false;
}
}
?>