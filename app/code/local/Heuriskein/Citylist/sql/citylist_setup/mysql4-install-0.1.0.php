<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('citylist')};
CREATE TABLE {$this->getTable('citylist')} (
  `citylist_id` int(11) unsigned NOT NULL auto_increment,
  `region_id` varchar(255) NOT NULL,
  `country_id` varchar(2) NOT NULL default '',
	 `zipcode` int(11) unsigned NOT NULL default '0',
	   `citytype` varchar(255) NOT NULL default '',
	 `area_code` smallint(6) unsigned NOT NULL default '0',
  `cityaliasname` varchar(255) NOT NULL default '',
  `default_name` varchar(225) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`citylist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 