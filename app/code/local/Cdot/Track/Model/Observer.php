<?php
require_once Mage::getBaseDir() . '/amazon/entities/Order.php';
require_once Mage::getBaseDir() . '/amazon/entities/Package.php';
require_once Mage::getBaseDir() . '/amazon/entities/Product.php';
require_once Mage::getBaseDir() . '/amazon/entities/Address.php';
require_once Mage::getBaseDir() . '/amazon/entities/Invoice.php';
//require_once Mage::getBaseDir() . '/amazon/edi/810.php';
require_once Mage::getBaseDir() . '/amazon/constants.php';
require_once Mage::getBaseDir() . '/amazon/essentials/write.php';


class Cdot_Track_Model_Observer {
    /*
      public function collect_tracking_info(Varien_Event_Observer $observer) {

      $shipment = $observer->getEvent()->getShipment();

      $order = $shipment->getOrder();
      //echo $order->getId();die;
      $track_info = Mage::getResourceModel('sales/order_shipment_track_collection')->addAttributeToSelect('track_number')->addAttributeToFilter('order_id', $order->getId());
      //echo "!!".$track_info->getSelect();die;
      //print_r($track_info->getData());die;
      //do something with order - get the increment id:
      //echo   $order->getIncrementId();die;
      //get all of the order items:
      // $items = $order->getAllItems();
      }
*/
      public function collect_invoice_info(Varien_Event_Observer $observer) {

      //echo "invoice saved";die;
      $_event = $observer->getEvent();
      $_invoice = $_event->getInvoice();
//	echo $_invoice->getId();die;
      $order = $_invoice->getOrder();

         /* nirmesh start */

        $order = Mage::getModel('sales/order')->load($order->getId());
        $amazon_order = new Order();

        $amazon_order->orderId = $order->getId();
        $amazon_order->partnerOrderId = $order->getPartnerOrderId();

        $amazon_order->customerOrderNumber = $order->getCustomerOrderNumber();

        $amazon_order->orderDate = $order->getCreatedAt();
	$amazon_order->warehouse = $order->getPartnerWarehouse();
        $product = new Product();
        $ordered_items = $order->getAllItems();
        $products = array();
        $i = 0;
        //echo count($ordered_items);
        //echo "******************************";
        foreach ($ordered_items as $item) {

          
            if ($item->getProductType() == 'simple') {
             //   echo "inside";
                //$product->id = $item->getProductId();
                $product->sku = $item->getSku();
                $product->shipped = $item->getQtyShipped();
                $product->price = $item->getPrice();
                $product->title = $item->getName();
                $products[$i] = $product;
                $i++;
            }
        }
       // die;
       /* $package = new Package();
        foreach ($order->getTracksCollection() as $_track) {
            $package->trackingNumber = $_track->getNumber();
            $package->shipmentCode = $_track->getCarrierCode();
        }

        $package->weight = '3';
*/
        $amazon_order->package = $package;
	$invoice = new Invoice();
	$invoice->invoiceNumber = $_invoice->getId();

        $amazon_order->invoice = $invoice;

        $amazon_order->products = $products;

        $shipping_address = new Address();
        $shipping_address->firstname = $order->getShippingAddress()->getFirstname();
        $shipping_address->lastname = $order->getShippingAddress()->getLastname();
        $shipping_address->street = $order->getShippingAddress()->getStreet();
        $shipping_address->city = $order->getShippingAddress()->getCity();
        $shipping_address->region = $order->getShippingAddress()->getRegion();
        $shipping_address->telephone = $order->getShippingAddress()->getTelephone();
        $shipping_address->postcode = $order->getShippingAddress()->getPostcode();
        $shipping_address->countryId = $order->getShippingAddress()->getCountryId();

        $billing_address = new Address();
        $billing_address->firstname = $order->getBillingAddress()->getFirstname();
        $billing_address->lastname = $order->getBillingAddress()->getLastname();
        $billing_address->street = $order->getBillingAddress()->getStreet();
        $billing_address->city = $order->getBillingAddress()->getCity();
        $billing_address->region = $order->getBillingAddress()->getRegion();
        $billing_address->telephone = $order->getBillingAddress()->getTelephone();
        $billing_address->postcode = $order->getBillingAddress()->getPostcode();
        $billing_address->countryId = $order->getBillingAddress()->getCountryId();



        $amazon_order->shipmentAddress = $shipping_address;
        $amazon_order->billingAddress = $billing_address;
        //$amazon_order[$i]->package = $order->getId();
        //   }
          //print_r($amazon_order);
         // die;

        $icn = rand(100000000, 999999999);
        //$edi = generateResponse($amazon_order, $icn);
        //write_edi($edi, $icn);
      }
     

    public function save_shipping_date(Varien_Event_Observer $observer) {

   
  if (Mage::app()->getStore()->isAdmin()) {
            $front = 0;
        } else {
            $front = 1;
        }
		$order = $observer->getEvent()->getOrder();

 if ($front == 0 && $order->getState() == "new"){
 $trading_part =$order->getTradingPartner();
if(empty($trading_part)) {

       $oldOrderId = Mage::getSingleton('core/session')->getSomeSessionVar(); 
	   $orderData = Mage::getModel("sales/order")->load($oldOrderId);
	   //echo $orderData['partner_order_id']; die;
	   
       $order_id = $order->getId(); //die;
        //echo 'hi:'.print_r($order->getData());die;
		
		$resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $opt = Mage::app()->getRequest()->getParam('options');
		$delivery_date1 = $opt['764'];
		$delivery_date2 = strtotime($delivery_date1);
		$delivery_date = date('Y-m-d', $delivery_date2);
		
		$query = "UPDATE sales_flat_order SET state ='processing', status ='processing', delivering_date = '".$delivery_date."', assignation_warehouse = '".$orderData['assignation_warehouse']."', partner_order_id = '".$orderData['partner_order_id']."', trading_partner = 'Reship'  WHERE entity_id = " . $order_id;

        $writeConnection->query($query);
		
		$query9 = "UPDATE sales_flat_order_grid SET status ='processing', assignation_warehouse = '".$orderData['assignation_warehouse']."', trading_partner = 'Reship' WHERE entity_id = " . $order_id;

        $writeConnection->query($query9);
		
     // echo 'hi:'.print_r($order_id);die;
        //$delivery_date = Mage::app()->getLayout()->getBlockSingleton('operations_adminhtml/operations_order_renderer_delivery')->custom_renderer($order); // get the delivery date from order item(s:10 options value) in Y-m-d
		Mage::log('order_id :'.$order_id.'Delivery date(Y-m-d):'.$delivery_date, null, 'cdotsys.log');
        /* nirmesh */

        $delivery_date_nirmesh = $delivery_date." 19:30:00";
         
        $collection = Mage::getModel('pointofsale/pointofsale')->getCollection();
        
		//echo 'delivery_date :'.$delivery_date;die;
        //print_r($collection->getData());
		//echo 'count :'.count($collection->getData());die;
        foreach ($collection->getData() as $data) {          
            
	    //check for warehouse stock against the order item starts here
            $items = $order->getAllItems();
            $product_id = 0;
            foreach ($items as $itemId => $item)
            {
              if($item->getProduct()->getTypeID()=='simple'){			  
                 $product_id = $item->getProductId();break;
              }
            }
            $collection_count = Mage::getModel('advancedinventory/stock')->getCollection()
                    ->addFieldToFilter('product_id', $product_id)
					->addFieldToFilter('place_id', $data['place_id'])
					->addFieldToFilter('quantity_in_stock', array('gt'=>0))->count();
			 //check for warehouse stock against the order item ends here
			  if($collection_count>0){ // if the warehouse has the stock for order item
					//if($data['enablelocaldeli']){		// check if the local delivery is allowed for this warehouse	
						//$warehouse[]['postal_code'] = $data['postal_code'];
						//$warehouse[]['inventory_assignation_rules'] = $data['inventory_assignation_rules'];
						
						

						$warehouse[$data['postal_code']] = $data['inventory_assignation_rules'];
						$x = array_search($_POST['shipping']['postcode'], explode(',', $data['inventory_assignation_rules']));
						if ($x !== false) {
							$delivery_day = date('l', strtotime($delivery_date));
							$warehouse_timings = json_decode($data['hours'],true);
							$warehouse_closing_time = explode(':',$warehouse_timings[$delivery_day]['from']); // 'from' in warehouse timings refers to Local delivery closing time
							if(!empty($warehouse_closing_time[0])){
								$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
								if($warehouse_closing_time_in_minutes != 0){
									$final_zip = $data['postal_code'];
								}
							}
						}
						//echo "!!".$final_zip;die;
						if (empty($final_zip)) {
						//echo $_POST['shipping']['postcode']."==".$data['postal_code']."<br/>";
						    if($_POST['shipping']['postcode']==$data['postal_code']){
								$delivery_day = date('l', strtotime($delivery_date));
								$warehouse_timings = json_decode($data['hours'],true);
								$warehouse_closing_time = explode(':',$warehouse_timings[$delivery_day]['from']); // 'from' in warehouse timings refers to Local delivery closing time
								if(!empty($warehouse_closing_time[0])){
									$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
									if($warehouse_closing_time_in_minutes != 0){
										$final_zip = $data['postal_code'];
									}
								}			
							}
						}			
					//}
					//if($data['enableshipping']){ // check if the shipping is allowed for this warehouse
                                                // for two days - starts here
                                                $tmp_dlvy_day = date('l', strtotime($delivery_date .' - 2 days'));
                                                $warehouse_timings = json_decode($data['hours'],true);
                                                $warehouse_shipping_closing_time = explode(':',$warehouse_timings[$tmp_dlvy_day]['to']); // 'to' in warehouse timings refers to shipping delivery closing time
                                                if(!empty($warehouse_shipping_closing_time[0])){                                                    
                                                    $warehouse_shipping_closing_time_in_minutes = ($warehouse_shipping_closing_time[0] * 60) + $warehouse_shipping_closing_time[1];
                                                    if($warehouse_shipping_closing_time_in_minutes != 0){
                                                       $ware_zip[] = $data['postal_code'];
                                                    }
                                                }
                                                // for two days - ends here
                                                
                                                // for one day - starts here
                                                $tmp_dlvy_day_2 = date('l', strtotime($delivery_date .' - 1 days'));
                                                //$warehouse_timings = json_decode($data['hours'],true);
                                                $warehouse_shipping_closing_time_2 = explode(':',$warehouse_timings[$tmp_dlvy_day_2]['to']); // 'to' in warehouse timings refers to shipping delivery closing time
                                                if(!empty($warehouse_shipping_closing_time_2[0])){
                                                    $warehouse_shipping_closing_time_in_minutes_2 = ($warehouse_shipping_closing_time_2[0] * 60) + $warehouse_shipping_closing_time_2[1];
                                                    if($warehouse_shipping_closing_time_in_minutes_2 != 0){
                                                       $ware_zip_2[] = $data['postal_code'];
                                                    }
                                                }
                                                // for one day - ends here
					//}
				}	
        }
        
//		echo '<pre>$final_zip :'.$final_zip;die;
        if ($final_zip) {
            $warehouse_name = Mage::getModel('pointofsale/pointofsale')->getCollection()
                    ->addFieldToSelect('place_id')
                    ->addFieldToFilter('postal_code', $final_zip);
            //echo $warehouse_name->getSelect();die;
           // print_r($warehouse_name->getData());
            $ware_data = $warehouse_name->getData();
            $warehouse_id = $ware_data[0]['place_id'];

            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');

            //$query = "UPDATE sales_flat_order SET assignation_warehouse = '" . $warehouse_id . " WHERE entity_id = " . $order_id;
           $pickup_method = "'Local Delivery'";
           $pickup_code = "'LD'";
           $delivery_date_nirmesh = "'$delivery_date_nirmesh'";
           $data = Mage::getModel('pointofsale/pointofsale')->getCollection()->addFieldToSelect('place_id')->getData();
            foreach ($data as $k => $v) {
                if ($v['place_id'] == $warehouse_id)
                    $wh[$v['place_id']] = 1;
                else
                    $wh[$v['place_id']] = 0;
            }
            $ordered_items = $order->getAllItems();
            foreach ($ordered_items as $item) {

                if ($item->getProductType() == 'simple')
                    $prepare[$item->getProductId()] = $wh;
            }


            $assignation_data = Mage::helper('core')->jsonEncode($prepare);

			Mage::log('inside if condition order_id :'.$order_id.'delivery_date_nirmesh :'.$delivery_date_nirmesh.'pickup_method :'.$pickup_method, null, 'cdotsys.log');
            $query = "UPDATE sales_flat_order SET assignation_stock ='" . $assignation_data . "', assignation_warehouse = " . $orderData['assignation_warehouse'] . ',pickup_date = ' . $delivery_date_nirmesh. ',pickup_code = ' . $pickup_code . ',pickup_method = ' . $pickup_method . ",delivering_date = '".$delivery_date."' WHERE entity_id = " . $order_id;
			
            $writeConnection->query($query);
			
			$query_2 = "UPDATE sales_flat_order_grid SET pickup_date = " . $delivery_date_nirmesh . ', assignation_warehouse = "' . $orderData['assignation_warehouse'] . '" , pickup_method = ' . $pickup_method . ",delivering_date = '".$delivery_date."' WHERE entity_id = " . $order_id;
			
            $writeConnection->query($query_2);

            return;
        } else {
            // this is useful for anwar
            if (empty($final_zip)) {
                //if (1) {
                $distances = '';
                foreach ($ware_zip as $k => $v) {
                    $distances[$v] = $this->getDistance($_POST['shipping']['postcode'], $v);
                }
                $index = array_search(min($distances), $distances);
                $transit_final_zip = $index;
                $warehouse_name = Mage::getModel('pointofsale/pointofsale')->getCollection()
                    ->addFieldToSelect('place_id')
                    ->addFieldToFilter('postal_code', $transit_final_zip);
                //print_r($warehouse_name->getData());
                $ware_data = $warehouse_name->getData();
                $warehouse_id = $ware_data[0]['place_id'];
                
                $distances = '';
                foreach ($ware_zip_2 as $k => $v) {
                    $distances[$v] = $this->getDistance($_POST['shipping']['postcode'], $v);
                }
                $index_2 = array_search(min($distances), $distances);
                $transit_final_zip_2 = $index_2;
                $warehouse_name_2 = Mage::getModel('pointofsale/pointofsale')->getCollection()
                    ->addFieldToSelect('place_id')
                    ->addFieldToFilter('postal_code', $transit_final_zip_2);
                //print_r($warehouse_name->getData());
                $ware_data_2 = $warehouse_name_2->getData();
                $warehouse_id_2 = $ware_data_2[0]['place_id'];
                
               //echo '$transit_final_zip :'.$transit_final_zip.' $transit_final_zip_2 :'.$transit_final_zip_2;die;
                //print_r($ware_zip);die;
            }
        }
		
		//echo '$transit_final_zip :'.$transit_final_zip;die;
		
	/* nirmesh assigning items to warehouse*/
        $data = Mage::getModel('pointofsale/pointofsale')->getCollection()->addFieldToSelect('place_id')->getData();
        foreach ($data as $k => $v) {
            if ($v['place_id'] == $warehouse_id)
                $wh[$v['place_id']] = 1;
            else
                $wh[$v['place_id']] = 0;
        }
        $ordered_items = $order->getAllItems();
        foreach ($ordered_items as $item) {

            if ($item->getProductType() == 'simple')
                $prepare[$item->getProductId()] = $wh;
        }


        $assignation_data = Mage::helper('core')->jsonEncode($prepare);
        /* till here */

        //echo '<pre>$transit_final_zip :'.$transit_final_zip;die;

        /* end here */
        $tmp_array = explode("-", $delivery_date);
        //echo 'explode';print_r($tmp_array);die;
        $tmp_delivery_date = array();
        $tmp_delivery_date['year'] = $tmp_array[0];
        $tmp_delivery_date['month'] = $tmp_array[1];
        $tmp_delivery_date['day'] = $tmp_array[2];
        $delivery_date_day = $tmp_delivery_date['day'];
        //echo 'delivery_date :'.$delivery_date;//die;
        // predict the tmp_pickup_date, if the delivery date is away from 2 days - u should subtract 2 days, if the delivery date is away from 1 day - u should subtract 1 day,if the delivery date is today  then you can make delivery date itself as tmp_pickup_date bcoz in bcoz the timeintransit api will consider pickup date as today itself automatically
        $currentdate = strtotime(date("Y-m-d"));
        $date = strtotime($delivery_date);
        $datediff = $date - $currentdate;
        $differance = floor($datediff / (60 * 60 * 24));
		Mage::log('order_id :'.$order_id.'Delivery date :'.$delivery_date.'Current date :'.date("Y-m-d").'Difference b/w current date and Delivery date :'.$differance, null, 'cdotsys.log');
        if ($differance == 0) {
            //echo 'today';            
            $tmp_pickup_date = str_replace("-", "", $delivery_date);
        } elseif ($differance == 1) {
            //echo 'tommorrow';
            $tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
        } elseif ($differance > 1) {
            //echo 'Future Date';
			
			if($differance == 2){ // when delivery date is exactly after 2 days				
				// if condition executes when tmp pickupdate date is today
				
				// if pickupdate is today check if the warehouse is open now or not - starts here 
				$final_warehouse_hours_data = Mage::getModel('pointofsale/pointofsale')->load($warehouse_id)->getData('hours');
				$warehouse_timings = json_decode($final_warehouse_hours_data,true);
				$pickup_day = date("l");
				$warehouse_closing_time = explode(':',$warehouse_timings[$pickup_day]['to']);
				$pickup_time_in_minutes = (date("H") * 60) + date("i");
				$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
				// if pickupdate is today check if the warehouse is open now or not - ends here
				
				if($pickup_time_in_minutes < $warehouse_closing_time_in_minutes){
					$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 2 days')));
				}else{
					$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
				}
			}else{	// when delivery date is >= 3 days
				// else condition executes when tmp pickupdate is not day
                                
                                //
                                if(empty($transit_final_zip)){
                                   $tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days'))); 
                                }else{
                                    $tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 2 days')));
                                }
			}
            
            //$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 2 days')));
        } else {
            //echo 'past date'; 
			//Mage::log('order_id :'.$order_id.'Delivery date :'.$delivery_date.'Current date :'.date("Y-m-d").'Difference b/w current date and Delivery date :'.$differance, null, 'cdotsys.log');
            $tmp_pickup_date = str_replace("-", "", $delivery_date);
        }

        //echo 'tmp_pickup_date before call:'.$tmp_pickup_date;//die;

        $tmp_pickup_datetime['year'] = substr($tmp_pickup_date, 0, 4);
        $tmp_pickup_datetime['month'] = substr($tmp_pickup_date, 4, 2);
        $tmp_pickup_datetime['day'] = substr($tmp_pickup_date, 6, 2);
        $tmp_pickup_datetime['hrs'] = '23'; // assumption as customer is not entering delivery time details
        $tmp_pickup_datetime['mins'] = '59'; // assumption as customer is not entering delivery time details
        $tmp_pickup_datetime['secs'] = '59'; // assumption as customer is not entering delivery time details

        $address = $order->getShippingAddress();
        //print_r(get_class_methods($address)); die;
        //$final_zip = getnearestwarehouse();//nirmesh
        
        
        // assigning postCode_from. if warehouse is available on - 2 days then use $transit_final_zip and if warehouse is available on -1 day then use $transit_final_zip_2
        $postCode_from = (empty($transit_final_zip)) ? $transit_final_zip_2 : $transit_final_zip; 
        //$postCode_from = $transit_final_zip; // Shipper location details
        if(empty($postCode_from)) 
         $postCode_from = '02446'; // This is main warehouse 305 Harvard Street, Brookline, MA
        
        $warehouse_loc = Mage::getModel('pointofsale/pointofsale')->getCollection()
                    ->addFieldToSelect('country_code')
                    ->addFieldToSelect('city')
                    ->addFieldToFilter('postal_code', $postCode_from);
            $ware_loc_data = $warehouse_loc->getData();
            $warehouse_country_code = $ware_loc_data[0]['country_code'];
            $warehouse_city = $ware_loc_data[0]['city'];
       
         $city_from = $warehouse_city; //  warehouse city
         if(empty($city_from)) 
		 $city_from = 'Brookline'; // This is main warehouse 305 Harvard Street, Brookline, MA
         
         $country_from =  $warehouse_country_code; // country code from warehouse
		 if(empty($country_from)) 
		 $country_from = 'US'; // This is main warehouse 305 Harvard Street, Brookline, MA
        //$country_from = 'US'; // Shipper location details
        $postCode_to = $address->getPostcode(); // delivery location details
        //$postCode_to = '55447'; // temporary delivery location details delete this line, and uncomment above line
        $country_to = $address->getCountry(); // delivery location details
        //echo 'shipping desc:'.$order->getShippingDescription();
        $shipping_amount = $order->getShippingAmount();
        //echo 'time diff:'.$diff = strtotime('2015-01-22 18:11:08') - strtotime('2015-01-20 18:11:08');
        // 2 days = 48 hours = 172800 seconds
		//echo '$tmp_pickup_date :'.$tmp_pickup_date.'$postCode_to :'.$postCode_to.'$country_to :'.$country_to.'$postCode_from :'.$postCode_from.'$country_from :'.$country_from.'$delivery_date_day :'.$delivery_date_day.'$shipping_amount :'.$shipping_amount;die;
		$tit_Parameters = 'tmp_pickup_date :'.$tmp_pickup_date.'postCode_to :'.$postCode_to.'country_to :'.$country_to.'postCode_from :'.$postCode_from.'country_from :'.$country_from.'delivery_date_day :'.$delivery_date_day.'shipping_amount :'.$shipping_amount;
		//Mage::log('inside else condition order_id :'.$order_id.'TimeinTransit parameters:'.$tit_Parameters, null, 'cdotsys.log');
        $timeTransit_response = $this->timeInTransit($tmp_pickup_date, $postCode_to, $country_to, $postCode_from, $country_from, $delivery_date_day, $shipping_amount, $transit_final_zip_2, $city_from); //die;
        //echo count($timeTransit_response);print_r($timeTransit_response);die;
//die('nirmesh below');
        $is_shortlisted_service_method = false;
        
      //  if (isset($shipping_amount)) { // if condition for free shipping
            //echo 'inside free';die;
            $ups_end_method = end($timeTransit_response);
            if ($ups_end_method->Service->Code == "GND") {// first check with ground method with pickup date,delvery time diff as <=48 hrs
                if ($ups_end_method->EstimatedArrival->BusinessDaysInTransit <= 2) {
                    //check if transit days is <= 48 hrs
                    $gnd_arrival_datetime = array();
                    $gnd_pickup_datetime_scheduled = array();

                    $gnd_pickup_date = $ups_end_method->EstimatedArrival->Pickup->Date;
                    $gnd_pickup_time = $ups_end_method->EstimatedArrival->Pickup->Time;
                    $gnd_pickup_datetime_scheduled['year'] = substr($gnd_pickup_date, 0, 4);
                    $gnd_pickup_datetime_scheduled['month'] = substr($gnd_pickup_date, 4, 2);
                    $gnd_pickup_datetime_scheduled['day'] = substr($gnd_pickup_date, 6, 2);
                    $gnd_pickup_datetime_scheduled['hrs'] = substr($gnd_pickup_time, 0, 2);
                    $gnd_pickup_datetime_scheduled['mins'] = substr($gnd_pickup_time, 2, 2);
                    $gnd_pickup_datetime_scheduled['secs'] = substr($gnd_pickup_time, 4, 2);

                    $gnd_arrival_date = $ups_end_method->EstimatedArrival->Arrival->Date;
                    $gnd_arrival_time = $ups_end_method->EstimatedArrival->Arrival->Time;
                    $gnd_arrival_datetime['year'] = substr($gnd_arrival_date, 0, 4);
                    $gnd_arrival_datetime['month'] = substr($gnd_arrival_date, 4, 2);
                    $gnd_arrival_datetime['day'] = substr($gnd_arrival_date, 6, 2);
                    $gnd_arrival_datetime['hrs'] = substr($gnd_arrival_time, 0, 2);
                    $gnd_arrival_datetime['mins'] = substr($gnd_arrival_time, 2, 2);
                    $gnd_arrival_datetime['secs'] = substr($gnd_arrival_time, 4, 2);

                    $gnd_time_diff = strtotime($gnd_arrival_datetime['year'] . '-' . $gnd_arrival_datetime['month'] . '-' . $gnd_arrival_datetime['day'] . ' ' . $gnd_arrival_datetime['hrs'] . ':' . $gnd_arrival_datetime['mins'] . ':' . $gnd_arrival_datetime['secs']) - strtotime($tmp_pickup_datetime['year'] . '-' . $tmp_pickup_datetime['month'] . '-' . $tmp_pickup_datetime['day'] . ' ' . $tmp_pickup_datetime['hrs'] . ':' . $tmp_pickup_datetime['mins'] . ':' . $tmp_pickup_datetime['secs']); // time diff bw tmp pickup and ups grnd delivery
                    //print_r($gnd_arrival_datetime);die;
                    
                    if ($gnd_time_diff < 172800) {
                        if ($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer delivery date
                            $shortlisted_ups_service_code = $ups_end_method->Service->Code;
                            $shortlisted_ups_service_desc = $ups_end_method->Service->Description;
                            $shortlisted_ups_service_pickup_datetime = $gnd_pickup_datetime_scheduled;

                            array_pop($timeTransit_response); // remove the last element from array which is ground
                            $is_shortlisted_service_method = true;
                        } // if($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']){	END					
                    } // if($gnd_time_diff < 172800){ END
                } // if($ups_end_method->EstimatedArrival->BusinessDaysInTransit <= 2){ END 
            } // if($ups_end_method->Service->Code == "GND"){ END
            //echo 'shortlisted_ups_service_desc :'. $shortlisted_ups_service_desc;die;

            if (!$is_shortlisted_service_method) { // execute foreach if service is not shortlisted
                $tmp_time_diff = 0;
                foreach ($timeTransit_response as $each_timeTransit_response) {
                    if ($each_timeTransit_response->EstimatedArrival->BusinessDaysInTransit <= 2) {
                        //check if transit days is <= 48 hrs
                        $air_arrival_datetime = array();
                        $air_pickup_datetime_scheduled = array();

                        $air_pickup_date = $each_timeTransit_response->EstimatedArrival->Pickup->Date;
                        $air_pickup_time = $each_timeTransit_response->EstimatedArrival->Pickup->Time;
                        $air_pickup_datetime_scheduled['year'] = substr($air_pickup_date, 0, 4);
                        $air_pickup_datetime_scheduled['month'] = substr($air_pickup_date, 4, 2);
                        $air_pickup_datetime_scheduled['day'] = substr($air_pickup_date, 6, 2);
                        $air_pickup_datetime_scheduled['hrs'] = substr($air_pickup_time, 0, 2);
                        $air_pickup_datetime_scheduled['mins'] = substr($air_pickup_time, 2, 2);
                        $air_pickup_datetime_scheduled['secs'] = substr($air_pickup_time, 4, 2);

                        $air_arrival_date = $each_timeTransit_response->EstimatedArrival->Arrival->Date;
                        $air_arrival_time = $each_timeTransit_response->EstimatedArrival->Arrival->Time;
                        $air_arrival_datetime['year'] = substr($air_arrival_date, 0, 4);
                        $air_arrival_datetime['month'] = substr($air_arrival_date, 4, 2);
                        $air_arrival_datetime['day'] = substr($air_arrival_date, 6, 2);
                        $air_arrival_datetime['hrs'] = substr($air_arrival_time, 0, 2);
                        $air_arrival_datetime['mins'] = substr($air_arrival_time, 2, 2);
                        $air_arrival_datetime['secs'] = substr($air_arrival_time, 4, 2);
                        
                        $air_time_diff = strtotime($air_arrival_datetime['year'] . '-' . $air_arrival_datetime['month'] . '-' . $air_arrival_datetime['day'] . ' ' . $air_arrival_datetime['hrs'] . ':' . $air_arrival_datetime['mins'] . ':' . $air_arrival_datetime['secs']) - strtotime($tmp_pickup_datetime['year'] . '-' . $tmp_pickup_datetime['month'] . '-' . $tmp_pickup_datetime['day'] . ' ' . $tmp_pickup_datetime['hrs'] . ':' . $tmp_pickup_datetime['mins'] . ':' . $tmp_pickup_datetime['secs']); // time diff bw tmp pickup and ups air delivery
                        //print_r($air_arrival_datetime);die;
                        //echo 'air_time_diff :'.$air_time_diff.'tmp_delivery_date day :'.$tmp_delivery_date['day'].'tmp_delivery_date day :'.$tmp_delivery_date['day']
                        if ($air_time_diff < 172800) {
                            if ($tmp_delivery_date['day'] == $air_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer delivery date
                                //print_r($air_arrival_datetime);die;
                                if ($air_time_diff > $tmp_time_diff) {
                                    $tmp_time_diff = $air_time_diff;

                                    $shortlisted_ups_service_code = $each_timeTransit_response->Service->Code;
                                    $shortlisted_ups_service_desc = $each_timeTransit_response->Service->Description;
                                    $shortlisted_ups_service_pickup_datetime = $air_pickup_datetime_scheduled;

                                    $is_shortlisted_service_method = true;
                                } // if($air_time_diff > $tmp_time_diff){ END
                            } // if($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']){	END
                        } // if($air_time_diff < 172800){ END
                    } // if($each_timeTransit_response->EstimatedArrival->BusinessDaysInTransit <= 2){ END
                } // foreach($timeTransit_response as $each_timeTransit_response){ END
            } // if(!$is_shortlisted_service_method){ END
     //   } // if($shipping_amount == 0){ END
    /*    else { // else condition will work only when no shipping method is selected which is the worst case
            $shortlisted_ups_service_code = ''; //$each_timeTransit_response->Service->Code; 
            $shortlisted_ups_service_desc = ''; //$each_timeTransit_response->Service->Description;
            $shortlisted_ups_service_pickup_datetime = '0000-00-00 00:00:00'; //$air_pickup_datetime_scheduled;
        }

*/
	
        /* anwar assigning items to warehouse, if pickupdate and warehouse is updated*/
        $tmp_pickup_date_oneday = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
        $shrtlsted_upsdate = $shortlisted_ups_service_pickup_datetime['year'] . $shortlisted_ups_service_pickup_datetime['month'] . $shortlisted_ups_service_pickup_datetime['day']; 
        if($tmp_pickup_date_oneday ==  $shrtlsted_upsdate){
            $data = Mage::getModel('pointofsale/pointofsale')->getCollection()->addFieldToSelect('place_id')->getData();
            foreach ($data as $k => $v) {
                if ($v['place_id'] == $warehouse_id_2)
                    $wh[$v['place_id']] = 1;
                else
                    $wh[$v['place_id']] = 0;
            }
            $ordered_items = $order->getAllItems();
            foreach ($ordered_items as $item) {

                if ($item->getProductType() == 'simple')
                    $prepare[$item->getProductId()] = $wh;
            }
            $assignation_data = Mage::helper('core')->jsonEncode($prepare);
        }
         /* till here */   

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        //$table = 'sales_flat_order_grid';   
        $pickupDate = $shortlisted_ups_service_pickup_datetime['year'] . '-' . $shortlisted_ups_service_pickup_datetime['month'] . '-' . $shortlisted_ups_service_pickup_datetime['day'] . ' ' . $shortlisted_ups_service_pickup_datetime['hrs'] . ':' . $shortlisted_ups_service_pickup_datetime['mins'] . ':' . $shortlisted_ups_service_pickup_datetime['secs']; // '2015-02-23 11:11:11' format
		
		//echo '$shortlisted_ups_service_code :'.$shortlisted_ups_service_code.'$shortlisted_ups_service_desc :'.$shortlisted_ups_service_desc.'$pickupDate :'.$pickupDate;die;

        //$query = "UPDATE sales_flat_order SET pickup_date = '" . $pickupDate . "',pickup_method = '" . $shortlisted_ups_service_desc ."'assignation_warehouse = '" "'  WHERE entity_id = " . $order_id;
		//Mage::log('inside else condition order_id :'.$order_id.'pickupDate :'.$pickupDate.'shortlisted_ups_service_desc :'.$shortlisted_ups_service_desc, null, 'cdotsys.log');
         $query = "UPDATE sales_flat_order SET assignation_stock ='" . $assignation_data . "', assignation_warehouse = '" . $orderData['assignation_warehouse'] . "',pickup_date = '" . $pickupDate . "',pickup_code = '" . $shortlisted_ups_service_code . "',pickup_method = '" . $shortlisted_ups_service_desc . "',delivering_date = '".$delivery_date."' WHERE entity_id = " . $order_id;
		 
        $writeConnection->query($query);
		
		$query_2 = "UPDATE sales_flat_order_grid SET pickup_date = '" . $pickupDate . "', assignation_warehouse = '" . $orderData['assignation_warehouse'] . "', pickup_method = '" . $shortlisted_ups_service_desc . "',delivering_date = '".$delivery_date."' WHERE entity_id = " . $order_id;
		 
        $writeConnection->query($query_2);


}
	}
 if ($front == 1){

		//echo 'inside save_shipping_date';die;
        //date_default_timezone_set("Asia/Kolkata");  // change the timezone while hosting to live server accordingly
        $order = $observer->getEvent()->getOrder();


 $trading_part =$order->getTradingPartner();
if(empty($trading_part)) {

        //echo $_blockData;die;
       $order_id = $order->getId(); //die;
        //echo 'hi:'.print_r($order->getData());die;

      
        $delivery_date = Mage::app()->getLayout()->getBlockSingleton('operations_adminhtml/operations_order_renderer_delivery')->custom_renderer($order); // get the delivery date from order item(s:10 options value) in Y-m-d
		Mage::log('order_id :'.$order_id.'Delivery date(Y-m-d):'.$delivery_date, null, 'cdotsys.log');
        /* nirmesh */

        $delivery_date_nirmesh = $delivery_date." 19:30:00";
         
        $collection = Mage::getModel('pointofsale/pointofsale')->getCollection();
        
		//echo 'delivery_date :'.$delivery_date;die;
        //print_r($collection->getData());
		//echo 'count :'.count($collection->getData());die;
        foreach ($collection->getData() as $data) {          
            
	    //check for warehouse stock against the order item starts here
            $items = $order->getAllItems();
            $product_id = 0;
            foreach ($items as $itemId => $item)
            {
              if($item->getProduct()->getTypeID()=='simple'){			  
                 $product_id = $item->getProductId();break;
              }
            }
            $collection_count = Mage::getModel('advancedinventory/stock')->getCollection()
                    ->addFieldToFilter('product_id', $product_id)
					->addFieldToFilter('place_id', $data['place_id'])
					->addFieldToFilter('quantity_in_stock', array('gt'=>0))->count();
			 //check for warehouse stock against the order item ends here
			  if($collection_count>0){ // if the warehouse has the stock for order item
					//if($data['enablelocaldeli']){		// check if the local delivery is allowed for this warehouse	
						//$warehouse[]['postal_code'] = $data['postal_code'];
						//$warehouse[]['inventory_assignation_rules'] = $data['inventory_assignation_rules'];
						
						

						$warehouse[$data['postal_code']] = $data['inventory_assignation_rules'];
						$x = array_search($_POST['shipping']['postcode'], explode(',', $data['inventory_assignation_rules']));
						if ($x !== false) {
							$delivery_day = date('l', strtotime($delivery_date));
							$warehouse_timings = json_decode($data['hours'],true);
							$warehouse_closing_time = explode(':',$warehouse_timings[$delivery_day]['from']); // 'from' in warehouse timings refers to Local delivery closing time
							if(!empty($warehouse_closing_time[0])){
								$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
								if($warehouse_closing_time_in_minutes != 0){
									$final_zip = $data['postal_code'];
								}
							}
						}
						//echo "!!".$final_zip;die;
						if (empty($final_zip)) {
						//echo $_POST['shipping']['postcode']."==".$data['postal_code']."<br/>";
						    if($_POST['shipping']['postcode']==$data['postal_code']){
								$delivery_day = date('l', strtotime($delivery_date));
								$warehouse_timings = json_decode($data['hours'],true);
								$warehouse_closing_time = explode(':',$warehouse_timings[$delivery_day]['from']); // 'from' in warehouse timings refers to Local delivery closing time
								if(!empty($warehouse_closing_time[0])){
									$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
									if($warehouse_closing_time_in_minutes != 0){
										$final_zip = $data['postal_code'];
									}
								}			
							}
						}			
					//}
					//if($data['enableshipping']){ // check if the shipping is allowed for this warehouse
                                                // for two days - starts here
                                                $tmp_dlvy_day = date('l', strtotime($delivery_date .' - 2 days'));
                                                $warehouse_timings = json_decode($data['hours'],true);
                                                $warehouse_shipping_closing_time = explode(':',$warehouse_timings[$tmp_dlvy_day]['to']); // 'to' in warehouse timings refers to shipping delivery closing time
                                                if(!empty($warehouse_shipping_closing_time[0])){                                                    
                                                    $warehouse_shipping_closing_time_in_minutes = ($warehouse_shipping_closing_time[0] * 60) + $warehouse_shipping_closing_time[1];
                                                    if($warehouse_shipping_closing_time_in_minutes != 0){
                                                       $ware_zip[] = $data['postal_code'];
                                                    }
                                                }
                                                // for two days - ends here
                                                
                                                // for one day - starts here
                                                $tmp_dlvy_day_2 = date('l', strtotime($delivery_date .' - 1 days'));
                                                //$warehouse_timings = json_decode($data['hours'],true);
                                                $warehouse_shipping_closing_time_2 = explode(':',$warehouse_timings[$tmp_dlvy_day_2]['to']); // 'to' in warehouse timings refers to shipping delivery closing time
                                                if(!empty($warehouse_shipping_closing_time_2[0])){
                                                    $warehouse_shipping_closing_time_in_minutes_2 = ($warehouse_shipping_closing_time_2[0] * 60) + $warehouse_shipping_closing_time_2[1];
                                                    if($warehouse_shipping_closing_time_in_minutes_2 != 0){
                                                       $ware_zip_2[] = $data['postal_code'];
                                                    }
                                                }
                                                // for one day - ends here
					//}
				}	
        }
        
//		echo '<pre>$final_zip :'.$final_zip;die;
        if ($final_zip) {
            $warehouse_name = Mage::getModel('pointofsale/pointofsale')->getCollection()
                    ->addFieldToSelect('place_id')
                    ->addFieldToFilter('postal_code', $final_zip);
            //echo $warehouse_name->getSelect();die;
           // print_r($warehouse_name->getData());
            $ware_data = $warehouse_name->getData();
            $warehouse_id = $ware_data[0]['place_id'];

            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');

            //$query = "UPDATE sales_flat_order SET assignation_warehouse = '" . $warehouse_id . " WHERE entity_id = " . $order_id;
           $pickup_method = "'Local Delivery'";
           $pickup_code = "'LD'";
           $delivery_date_nirmesh = "'$delivery_date_nirmesh'";
           $data = Mage::getModel('pointofsale/pointofsale')->getCollection()->addFieldToSelect('place_id')->getData();
            foreach ($data as $k => $v) {
                if ($v['place_id'] == $warehouse_id)
                    $wh[$v['place_id']] = 1;
                else
                    $wh[$v['place_id']] = 0;
            }
            $ordered_items = $order->getAllItems();
            foreach ($ordered_items as $item) {

                if ($item->getProductType() == 'simple')
                    $prepare[$item->getProductId()] = $wh;
            }


            $assignation_data = Mage::helper('core')->jsonEncode($prepare);

			Mage::log('inside if condition order_id :'.$order_id.'delivery_date_nirmesh :'.$delivery_date_nirmesh.'pickup_method :'.$pickup_method, null, 'cdotsys.log');
            $query = "UPDATE sales_flat_order SET assignation_stock ='" . $assignation_data . "', assignation_warehouse = " . $warehouse_id . ',pickup_date = ' . $delivery_date_nirmesh. ',pickup_code = ' . $pickup_code . ',pickup_method = ' . $pickup_method . ",delivering_date = '".$delivery_date."' WHERE entity_id = " . $order_id;
			
            $writeConnection->query($query);
			
			$query_2 = "UPDATE sales_flat_order_grid SET  pickup_date = " . $delivery_date_nirmesh . ',pickup_method = ' . $pickup_method . ",delivering_date = '".$delivery_date."' WHERE entity_id = " . $order_id;
			
            $writeConnection->query($query_2);

            return;
        } else {
            // this is useful for anwar
            if (empty($final_zip)) {
                //if (1) {
                $distances = '';
                foreach ($ware_zip as $k => $v) {
                    $distances[$v] = $this->getDistance($_POST['shipping']['postcode'], $v);
                }
                $index = array_search(min($distances), $distances);
                $transit_final_zip = $index;
                $warehouse_name = Mage::getModel('pointofsale/pointofsale')->getCollection()
                    ->addFieldToSelect('place_id')
                    ->addFieldToFilter('postal_code', $transit_final_zip);
                //print_r($warehouse_name->getData());
                $ware_data = $warehouse_name->getData();
                $warehouse_id = $ware_data[0]['place_id'];
                
                $distances = '';
                foreach ($ware_zip_2 as $k => $v) {
                    $distances[$v] = $this->getDistance($_POST['shipping']['postcode'], $v);
                }
                $index_2 = array_search(min($distances), $distances);
                $transit_final_zip_2 = $index_2;
                $warehouse_name_2 = Mage::getModel('pointofsale/pointofsale')->getCollection()
                    ->addFieldToSelect('place_id')
                    ->addFieldToFilter('postal_code', $transit_final_zip_2);
                //print_r($warehouse_name->getData());
                $ware_data_2 = $warehouse_name_2->getData();
                $warehouse_id_2 = $ware_data_2[0]['place_id'];
                
               //echo '$transit_final_zip :'.$transit_final_zip.' $transit_final_zip_2 :'.$transit_final_zip_2;die;
                //print_r($ware_zip);die;
            }
        }
		
		//echo '$transit_final_zip :'.$transit_final_zip;die;
		
	/* nirmesh assigning items to warehouse*/
        $data = Mage::getModel('pointofsale/pointofsale')->getCollection()->addFieldToSelect('place_id')->getData();
        foreach ($data as $k => $v) {
            if ($v['place_id'] == $warehouse_id)
                $wh[$v['place_id']] = 1;
            else
                $wh[$v['place_id']] = 0;
        }
        $ordered_items = $order->getAllItems();
        foreach ($ordered_items as $item) {

            if ($item->getProductType() == 'simple')
                $prepare[$item->getProductId()] = $wh;
        }


        $assignation_data = Mage::helper('core')->jsonEncode($prepare);
        /* till here */

        //echo '<pre>$transit_final_zip :'.$transit_final_zip;die;

        /* end here */
        $tmp_array = explode("-", $delivery_date);
        //echo 'explode';print_r($tmp_array);die;
        $tmp_delivery_date = array();
        $tmp_delivery_date['year'] = $tmp_array[0];
        $tmp_delivery_date['month'] = $tmp_array[1];
        $tmp_delivery_date['day'] = $tmp_array[2];
        $delivery_date_day = $tmp_delivery_date['day'];
        //echo 'delivery_date :'.$delivery_date;//die;
        // predict the tmp_pickup_date, if the delivery date is away from 2 days - u should subtract 2 days, if the delivery date is away from 1 day - u should subtract 1 day,if the delivery date is today  then you can make delivery date itself as tmp_pickup_date bcoz in bcoz the timeintransit api will consider pickup date as today itself automatically
        $currentdate = strtotime(date("Y-m-d"));
        $date = strtotime($delivery_date);
        $datediff = $date - $currentdate;
        $differance = floor($datediff / (60 * 60 * 24));
		Mage::log('order_id :'.$order_id.'Delivery date :'.$delivery_date.'Current date :'.date("Y-m-d").'Difference b/w current date and Delivery date :'.$differance, null, 'cdotsys.log');
        if ($differance == 0) {
            //echo 'today';            
            $tmp_pickup_date = str_replace("-", "", $delivery_date);
        } elseif ($differance == 1) {
            //echo 'tommorrow';
            $tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
        } elseif ($differance > 1) {
            //echo 'Future Date';
			
			if($differance == 2){ // when delivery date is exactly after 2 days				
				// if condition executes when tmp pickupdate date is today
				
				// if pickupdate is today check if the warehouse is open now or not - starts here 
				$final_warehouse_hours_data = Mage::getModel('pointofsale/pointofsale')->load($warehouse_id)->getData('hours');
				$warehouse_timings = json_decode($final_warehouse_hours_data,true);
				$pickup_day = date("l");
				$warehouse_closing_time = explode(':',$warehouse_timings[$pickup_day]['to']);
				$pickup_time_in_minutes = (date("H") * 60) + date("i");
				$warehouse_closing_time_in_minutes = ($warehouse_closing_time[0] * 60) + $warehouse_closing_time[1];
				// if pickupdate is today check if the warehouse is open now or not - ends here
				
				if($pickup_time_in_minutes < $warehouse_closing_time_in_minutes){
					$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 2 days')));
				}else{
					$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
				}
			}else{	// when delivery date is >= 3 days
				// else condition executes when tmp pickupdate is not day
                                
                                //
                                if(empty($transit_final_zip)){
                                   $tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days'))); 
                                }else{
                                    $tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 2 days')));
                                }
			}
            
            //$tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 2 days')));
        } else {
            //echo 'past date'; 
			//Mage::log('order_id :'.$order_id.'Delivery date :'.$delivery_date.'Current date :'.date("Y-m-d").'Difference b/w current date and Delivery date :'.$differance, null, 'cdotsys.log');
            $tmp_pickup_date = str_replace("-", "", $delivery_date);
        }

        //echo 'tmp_pickup_date before call:'.$tmp_pickup_date;//die;

        $tmp_pickup_datetime['year'] = substr($tmp_pickup_date, 0, 4);
        $tmp_pickup_datetime['month'] = substr($tmp_pickup_date, 4, 2);
        $tmp_pickup_datetime['day'] = substr($tmp_pickup_date, 6, 2);
        $tmp_pickup_datetime['hrs'] = '23'; // assumption as customer is not entering delivery time details
        $tmp_pickup_datetime['mins'] = '59'; // assumption as customer is not entering delivery time details
        $tmp_pickup_datetime['secs'] = '59'; // assumption as customer is not entering delivery time details

        $address = $order->getShippingAddress();
        //print_r(get_class_methods($address)); die;
        //$final_zip = getnearestwarehouse();//nirmesh
        
        
        // assigning postCode_from. if warehouse is available on - 2 days then use $transit_final_zip and if warehouse is available on -1 day then use $transit_final_zip_2
        $postCode_from = (empty($transit_final_zip)) ? $transit_final_zip_2 : $transit_final_zip; 
        //$postCode_from = $transit_final_zip; // Shipper location details
        if(empty($postCode_from)) 
         $postCode_from = '02446'; // This is main warehouse 305 Harvard Street, Brookline, MA
        
        $warehouse_loc = Mage::getModel('pointofsale/pointofsale')->getCollection()
                    ->addFieldToSelect('country_code')
                    ->addFieldToSelect('city')
                    ->addFieldToFilter('postal_code', $postCode_from);
            $ware_loc_data = $warehouse_loc->getData();
            $warehouse_country_code = $ware_loc_data[0]['country_code'];
            $warehouse_city = $ware_loc_data[0]['city'];
       
         $city_from = $warehouse_city; //  warehouse city
         if(empty($city_from)) 
		 $city_from = 'Brookline'; // This is main warehouse 305 Harvard Street, Brookline, MA
         
         $country_from =  $warehouse_country_code; // country code from warehouse
		 if(empty($country_from)) 
		 $country_from = 'US'; // This is main warehouse 305 Harvard Street, Brookline, MA
        //$country_from = 'US'; // Shipper location details
        $postCode_to = $address->getPostcode(); // delivery location details
        //$postCode_to = '55447'; // temporary delivery location details delete this line, and uncomment above line
        $country_to = $address->getCountry(); // delivery location details
        //echo 'shipping desc:'.$order->getShippingDescription();
        $shipping_amount = $order->getShippingAmount();
        //echo 'time diff:'.$diff = strtotime('2015-01-22 18:11:08') - strtotime('2015-01-20 18:11:08');
        // 2 days = 48 hours = 172800 seconds
		//echo '$tmp_pickup_date :'.$tmp_pickup_date.'$postCode_to :'.$postCode_to.'$country_to :'.$country_to.'$postCode_from :'.$postCode_from.'$country_from :'.$country_from.'$delivery_date_day :'.$delivery_date_day.'$shipping_amount :'.$shipping_amount;die;
		$tit_Parameters = 'tmp_pickup_date :'.$tmp_pickup_date.'postCode_to :'.$postCode_to.'country_to :'.$country_to.'postCode_from :'.$postCode_from.'country_from :'.$country_from.'delivery_date_day :'.$delivery_date_day.'shipping_amount :'.$shipping_amount;
		//Mage::log('inside else condition order_id :'.$order_id.'TimeinTransit parameters:'.$tit_Parameters, null, 'cdotsys.log');
        $timeTransit_response = $this->timeInTransit($tmp_pickup_date, $postCode_to, $country_to, $postCode_from, $country_from, $delivery_date_day, $shipping_amount, $transit_final_zip_2, $city_from); //die;
        //echo count($timeTransit_response);print_r($timeTransit_response);die;
//die('nirmesh below');
        $is_shortlisted_service_method = false;
        
      //  if (isset($shipping_amount)) { // if condition for free shipping
            //echo 'inside free';die;
            $ups_end_method = end($timeTransit_response);
            if ($ups_end_method->Service->Code == "GND") {// first check with ground method with pickup date,delvery time diff as <=48 hrs
                if ($ups_end_method->EstimatedArrival->BusinessDaysInTransit <= 2) {
                    //check if transit days is <= 48 hrs
                    $gnd_arrival_datetime = array();
                    $gnd_pickup_datetime_scheduled = array();

                    $gnd_pickup_date = $ups_end_method->EstimatedArrival->Pickup->Date;
                    $gnd_pickup_time = $ups_end_method->EstimatedArrival->Pickup->Time;
                    $gnd_pickup_datetime_scheduled['year'] = substr($gnd_pickup_date, 0, 4);
                    $gnd_pickup_datetime_scheduled['month'] = substr($gnd_pickup_date, 4, 2);
                    $gnd_pickup_datetime_scheduled['day'] = substr($gnd_pickup_date, 6, 2);
                    $gnd_pickup_datetime_scheduled['hrs'] = substr($gnd_pickup_time, 0, 2);
                    $gnd_pickup_datetime_scheduled['mins'] = substr($gnd_pickup_time, 2, 2);
                    $gnd_pickup_datetime_scheduled['secs'] = substr($gnd_pickup_time, 4, 2);

                    $gnd_arrival_date = $ups_end_method->EstimatedArrival->Arrival->Date;
                    $gnd_arrival_time = $ups_end_method->EstimatedArrival->Arrival->Time;
                    $gnd_arrival_datetime['year'] = substr($gnd_arrival_date, 0, 4);
                    $gnd_arrival_datetime['month'] = substr($gnd_arrival_date, 4, 2);
                    $gnd_arrival_datetime['day'] = substr($gnd_arrival_date, 6, 2);
                    $gnd_arrival_datetime['hrs'] = substr($gnd_arrival_time, 0, 2);
                    $gnd_arrival_datetime['mins'] = substr($gnd_arrival_time, 2, 2);
                    $gnd_arrival_datetime['secs'] = substr($gnd_arrival_time, 4, 2);

                    $gnd_time_diff = strtotime($gnd_arrival_datetime['year'] . '-' . $gnd_arrival_datetime['month'] . '-' . $gnd_arrival_datetime['day'] . ' ' . $gnd_arrival_datetime['hrs'] . ':' . $gnd_arrival_datetime['mins'] . ':' . $gnd_arrival_datetime['secs']) - strtotime($tmp_pickup_datetime['year'] . '-' . $tmp_pickup_datetime['month'] . '-' . $tmp_pickup_datetime['day'] . ' ' . $tmp_pickup_datetime['hrs'] . ':' . $tmp_pickup_datetime['mins'] . ':' . $tmp_pickup_datetime['secs']); // time diff bw tmp pickup and ups grnd delivery
                    //print_r($gnd_arrival_datetime);die;
                    
                    if ($gnd_time_diff < 172800) {
                        if ($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer delivery date
                            $shortlisted_ups_service_code = $ups_end_method->Service->Code;
                            $shortlisted_ups_service_desc = $ups_end_method->Service->Description;
                            $shortlisted_ups_service_pickup_datetime = $gnd_pickup_datetime_scheduled;

                            array_pop($timeTransit_response); // remove the last element from array which is ground
                            $is_shortlisted_service_method = true;
                        } // if($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']){	END					
                    } // if($gnd_time_diff < 172800){ END
                } // if($ups_end_method->EstimatedArrival->BusinessDaysInTransit <= 2){ END 
            } // if($ups_end_method->Service->Code == "GND"){ END
            //echo 'shortlisted_ups_service_desc :'. $shortlisted_ups_service_desc;die;

            if (!$is_shortlisted_service_method) { // execute foreach if service is not shortlisted
                $tmp_time_diff = 0;
                foreach ($timeTransit_response as $each_timeTransit_response) {
                    if ($each_timeTransit_response->EstimatedArrival->BusinessDaysInTransit <= 2) {
                        //check if transit days is <= 48 hrs
                        $air_arrival_datetime = array();
                        $air_pickup_datetime_scheduled = array();

                        $air_pickup_date = $each_timeTransit_response->EstimatedArrival->Pickup->Date;
                        $air_pickup_time = $each_timeTransit_response->EstimatedArrival->Pickup->Time;
                        $air_pickup_datetime_scheduled['year'] = substr($air_pickup_date, 0, 4);
                        $air_pickup_datetime_scheduled['month'] = substr($air_pickup_date, 4, 2);
                        $air_pickup_datetime_scheduled['day'] = substr($air_pickup_date, 6, 2);
                        $air_pickup_datetime_scheduled['hrs'] = substr($air_pickup_time, 0, 2);
                        $air_pickup_datetime_scheduled['mins'] = substr($air_pickup_time, 2, 2);
                        $air_pickup_datetime_scheduled['secs'] = substr($air_pickup_time, 4, 2);

                        $air_arrival_date = $each_timeTransit_response->EstimatedArrival->Arrival->Date;
                        $air_arrival_time = $each_timeTransit_response->EstimatedArrival->Arrival->Time;
                        $air_arrival_datetime['year'] = substr($air_arrival_date, 0, 4);
                        $air_arrival_datetime['month'] = substr($air_arrival_date, 4, 2);
                        $air_arrival_datetime['day'] = substr($air_arrival_date, 6, 2);
                        $air_arrival_datetime['hrs'] = substr($air_arrival_time, 0, 2);
                        $air_arrival_datetime['mins'] = substr($air_arrival_time, 2, 2);
                        $air_arrival_datetime['secs'] = substr($air_arrival_time, 4, 2);
                        
                        $air_time_diff = strtotime($air_arrival_datetime['year'] . '-' . $air_arrival_datetime['month'] . '-' . $air_arrival_datetime['day'] . ' ' . $air_arrival_datetime['hrs'] . ':' . $air_arrival_datetime['mins'] . ':' . $air_arrival_datetime['secs']) - strtotime($tmp_pickup_datetime['year'] . '-' . $tmp_pickup_datetime['month'] . '-' . $tmp_pickup_datetime['day'] . ' ' . $tmp_pickup_datetime['hrs'] . ':' . $tmp_pickup_datetime['mins'] . ':' . $tmp_pickup_datetime['secs']); // time diff bw tmp pickup and ups air delivery
                        //print_r($air_arrival_datetime);die;
                        //echo 'air_time_diff :'.$air_time_diff.'tmp_delivery_date day :'.$tmp_delivery_date['day'].'tmp_delivery_date day :'.$tmp_delivery_date['day']
                        if ($air_time_diff < 172800) {
                            if ($tmp_delivery_date['day'] == $air_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer delivery date
                                //print_r($air_arrival_datetime);die;
                                if ($air_time_diff > $tmp_time_diff) {
                                    $tmp_time_diff = $air_time_diff;

                                    $shortlisted_ups_service_code = $each_timeTransit_response->Service->Code;
                                    $shortlisted_ups_service_desc = $each_timeTransit_response->Service->Description;
                                    $shortlisted_ups_service_pickup_datetime = $air_pickup_datetime_scheduled;

                                    $is_shortlisted_service_method = true;
                                } // if($air_time_diff > $tmp_time_diff){ END
                            } // if($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']){	END
                        } // if($air_time_diff < 172800){ END
                    } // if($each_timeTransit_response->EstimatedArrival->BusinessDaysInTransit <= 2){ END
                } // foreach($timeTransit_response as $each_timeTransit_response){ END
            } // if(!$is_shortlisted_service_method){ END
     //   } // if($shipping_amount == 0){ END
    /*    else { // else condition will work only when no shipping method is selected which is the worst case
            $shortlisted_ups_service_code = ''; //$each_timeTransit_response->Service->Code; 
            $shortlisted_ups_service_desc = ''; //$each_timeTransit_response->Service->Description;
            $shortlisted_ups_service_pickup_datetime = '0000-00-00 00:00:00'; //$air_pickup_datetime_scheduled;
        }

*/
	
        /* anwar assigning items to warehouse, if pickupdate and warehouse is updated*/
        $tmp_pickup_date_oneday = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
        $shrtlsted_upsdate = $shortlisted_ups_service_pickup_datetime['year'] . $shortlisted_ups_service_pickup_datetime['month'] . $shortlisted_ups_service_pickup_datetime['day']; 
        if($tmp_pickup_date_oneday ==  $shrtlsted_upsdate){
            $data = Mage::getModel('pointofsale/pointofsale')->getCollection()->addFieldToSelect('place_id')->getData();
            foreach ($data as $k => $v) {
                if ($v['place_id'] == $warehouse_id_2)
                    $wh[$v['place_id']] = 1;
                else
                    $wh[$v['place_id']] = 0;
            }
            $ordered_items = $order->getAllItems();
            foreach ($ordered_items as $item) {

                if ($item->getProductType() == 'simple')
                    $prepare[$item->getProductId()] = $wh;
            }
            $assignation_data = Mage::helper('core')->jsonEncode($prepare);
        }
         /* till here */   

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        //$table = 'sales_flat_order_grid';   
        $pickupDate = $shortlisted_ups_service_pickup_datetime['year'] . '-' . $shortlisted_ups_service_pickup_datetime['month'] . '-' . $shortlisted_ups_service_pickup_datetime['day'] . ' ' . $shortlisted_ups_service_pickup_datetime['hrs'] . ':' . $shortlisted_ups_service_pickup_datetime['mins'] . ':' . $shortlisted_ups_service_pickup_datetime['secs']; // '2015-02-23 11:11:11' format
		
		//echo '$shortlisted_ups_service_code :'.$shortlisted_ups_service_code.'$shortlisted_ups_service_desc :'.$shortlisted_ups_service_desc.'$pickupDate :'.$pickupDate;die;

        //$query = "UPDATE sales_flat_order SET pickup_date = '" . $pickupDate . "',pickup_method = '" . $shortlisted_ups_service_desc ."'assignation_warehouse = '" "'  WHERE entity_id = " . $order_id;
		//Mage::log('inside else condition order_id :'.$order_id.'pickupDate :'.$pickupDate.'shortlisted_ups_service_desc :'.$shortlisted_ups_service_desc, null, 'cdotsys.log');
         $query = "UPDATE sales_flat_order SET assignation_stock ='" . $assignation_data . "', assignation_warehouse = '" . $warehouse_id . "',pickup_date = '" . $pickupDate . "',pickup_code = '" . $shortlisted_ups_service_code . "',pickup_method = '" . $shortlisted_ups_service_desc . "',delivering_date = '".$delivery_date."' WHERE entity_id = " . $order_id;
		 
        $writeConnection->query($query);
		
		$query_2 = "UPDATE sales_flat_order_grid SET pickup_date = '" . $pickupDate . "',pickup_method = '" . $shortlisted_ups_service_desc . "',delivering_date = '".$delivery_date."' WHERE entity_id = " . $order_id;
		 
        $writeConnection->query($query_2);
}
}
    }

    public function timeInTransit($tmp_pickup_date, $postCode_to, $country_to, $postCode_from, $country_from, $delivery_date_day, $shipping_amount, $transit_final_zip_2, $city_from) {
        //Configuration
        $access = Mage::getStoreConfig('upslabel/credentials/accesslicensenumber'); //"8CCFD3D445A6F805";
        $userid = Mage::getStoreConfig('upslabel/credentials/userid'); //"newKaBloom";
        $passwd = Mage::getStoreConfig('upslabel/credentials/password'); //"Rosegarden123";
        $wsdl = Mage::getModuleDir('etc', 'Mage_Usa') . DS . 'wsdl' . DS . 'Ups' . DS . 'TNTWS.wsdl';
        $operation = "ProcessTimeInTransit";
        $endpointurl = 'https://wwwcie.ups.com/webservices/TimeInTransit';
        $outputFileName = "XOLTResult.xml";

        try {

            $mode = array
                (
                'soap_version' => 'SOAP_1_1', // use soap 1.1 client
                'trace' => 1
            );

            // initialize soap client
            $client = new SoapClient($wsdl, $mode);

            //set endpoint url
            $client->__setLocation($endpointurl);

            //create soap header
            $usernameToken['Username'] = $userid;
            $usernameToken['Password'] = $passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;

            $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', $upss);
            $client->__setSoapHeaders($header);

            //create soap request
            $requestoption['RequestOption'] = 'TNT';
            $request['Request'] = $requestoption;

            $addressFrom['City'] = $city_from;
            $addressFrom['CountryCode'] = $country_from; //'US';
            $addressFrom['PostalCode'] = $postCode_from; //'02446';
            $addressFrom['StateProvinceCode'] = '';
            $shipFrom['Address'] = $addressFrom;
            $request['ShipFrom'] = $shipFrom;

            $addressTo['City'] = '';
            $addressTo['CountryCode'] = $country_to; //'US';
            $addressTo['PostalCode'] = $postCode_to; //'55447';
            $addressTo['StateProvinceCode'] = '';
            $shipTo['Address'] = $addressTo;
            $request['ShipTo'] = $shipTo;

            $pickup['Date'] = $tmp_pickup_date; //'20150205'; //Ymd format
            $request['Pickup'] = $pickup;

            $unitOfMeasurement['Code'] = 'KGS';
            $unitOfMeasurement['Description'] = 'Kilograms';
            $shipmentWeight['UnitOfMeasurement'] = $unitOfMeasurement;
            $shipmentWeight['Weight'] = '2';
            $request['ShipmentWeight'] = $shipmentWeight;

            $request['TotalPackagesInShipment'] = '1';

            $invoiceLineTotal['CurrencyCode'] = 'CAD';
            $invoiceLineTotal['MonetaryValue'] = '10';
            $request['InvoiceLineTotal'] = $invoiceLineTotal;

            $request['MaximumListSize'] = '1';
            //echo '<pre>';
            //echo "Request.......\n";
            //print_r($request);
            //echo "\n\n";
            //get response
            $resp = $client->__soapCall($operation, array($request));
            //get status
            //echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";
            //save soap request and response to file

            $fw = fopen($outputFileName, 'w');
            //var_dump(get_object_vars($resp));
            $tmp_array = $resp->TransitResponse->ServiceSummary; //die;

            if (isset($shipping_amount)) {
                $tocheck_shortest_routes = $tmp_array;
                // can't explain, test b/w 02446 - 02482 u will understand to foreach loop
                $not_found = false;
                $not_found_count = 0;
                foreach ($tocheck_shortest_routes as $each_timeTransit_response) {
                    $service_arrival_date = $each_timeTransit_response->EstimatedArrival->Arrival->Date;
                    $air_arrival_datetime['day'] = substr($service_arrival_date, 6, 2);
                    if ($delivery_date_day == $air_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer 		delivery date
                        $not_found = false;
                    } else {
                        $not_found = true;
                        $not_found_count += 1;
                    }
                }

                if ($not_found && (count($tocheck_shortest_routes) == $not_found_count)) {
                    // This executes when - 2 days pick methods not not deliveing on delivery date, so here update tmp pickup date
                    $tmp_pickup_datetime_year = substr($tmp_pickup_date, 0, 4);
                    $tmp_pickup_datetime_month = substr($tmp_pickup_date, 4, 2);
                    $tmp_pickup_datetime_day = substr($tmp_pickup_date, 6, 2);
                    $tmp_pickup_datetime = $tmp_pickup_datetime_year . '-' . $tmp_pickup_datetime_month . '-' . $tmp_pickup_datetime_day;
                    
                    $tmp_pickup_date_new = str_replace("-", "", date('Y-m-d', strtotime($tmp_pickup_datetime . ' + 1 days')));
                    
                    $addressFrom['City'] = '';
                    $addressFrom['CountryCode'] = $country_from; //'US';
                    $addressFrom['PostalCode'] = $transit_final_zip_2; 
                    $addressFrom['StateProvinceCode'] = '';
                    $shipFrom['Address'] = $addressFrom;
                    $request['ShipFrom'] = $shipFrom;
                    
                    $pickup['Date'] = $tmp_pickup_date_new; //'20150205'; //Ymd format
                    $request['Pickup'] = $pickup;

                    $resp = $client->__soapCall($operation, array($request));
                    $tmp_array = $resp->TransitResponse->ServiceSummary; //die;
                }
            }

            return $tmp_array;
            //echo count($tmp_array);print_r($tmp_array);die;
            // fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
            // fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
            // fclose($fw);
        } catch (Exception $e) {
            print_r($e);
        }
    }

    public function getLnt($zip) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=
" . urlencode($zip) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $result1[] = $result['results'][0];
        $result2[] = $result1[0]['geometry'];
        $result3[] = $result2[0]['location'];
        return $result3[0];
    }

    public function getDistance($zip1, $zip2) {
        $first_lat = $this->getLnt($zip1);
        $next_lat = $this->getLnt($zip2);
        $lat1 = $first_lat['lat'];
        $lon1 = $first_lat['lng'];
        $lat2 = $next_lat['lat'];
        $lon2 = $next_lat['lng'];
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +
                cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
                cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        return ($miles * 0.8684);
    }

}

?>
