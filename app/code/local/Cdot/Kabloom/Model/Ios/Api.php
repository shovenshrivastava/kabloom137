<?php
ini_set("soap.wsdl_cache_enabled", "0");
class Cdot_Kabloom_Model_Ios_Api extends Mage_Api_Model_Resource_Abstract
{        
	public function getmycategoy($id)
        {
		$catagory_model = Mage::getModel('catalog/category')->load($id); 

		$collection = Mage::getResourceModel('catalog/product_collection');
		// $collection->addMinimalPrice()->addFinalPrice();

		$collection->addCategoryFilter($catagory_model); //category filter

		//$collection->addAttributeToFilter('status',1); //only enabled product

		$collection->addAttributeToSelect('*'); //add product attribute to be fetched
		
		//$collection->addStoreFilter();          

			if(!empty($collection))

			{
					foreach ($collection as $_product)
					{
						 $my_arr[]= $_product->getData();
							
					}

			} 
	
			return $collection;
        }
		
		public function getmybanner()
        {
			$banner_url= Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA). 'ves_layerslider/upload/banner/slider_auto3_1.jpg';
			$banner_url1= Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA). 'ves_layerslider/upload/banner/slider_auto2_1.jpg'; 
			$banner_url2= Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA). 'ves_layerslider/upload/banner/slider_auto3_3.jpg'; 
			
			$coll[]=array("image" => $banner_url, "image1" => $banner_url1, "image2" => $banner_url2);
	
			return $coll;
        }
		
		public function getcatlist($id)
        {
			$children = Mage::getModel('catalog/category')->getCategories($id);
			foreach ($children as $category) 
			{
				$collection[]= $category->getData();
			
			}
	
			return $collection;
        }
		
	public function getproductsearch($searchtxt)
	{		
		
		$product_collection = Mage::getResourceModel('catalog/product_collection')
						  ->addAttributeToSelect('*')
						  ->addAttributeToFilter(	array(
												array('attribute'=> 'name','like' => '%'.$searchtxt.'%'),
												array('attribute'=> 'sku','like' => '%'.$searchtxt.'%'),
											  ))
						  ->load();
		return $product_collection;
     
	}
}	
	