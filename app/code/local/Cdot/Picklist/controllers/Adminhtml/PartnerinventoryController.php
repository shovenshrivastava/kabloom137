<?php
class Cdot_Picklist_Adminhtml_PartnerinventoryController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Inventory of partners"));
	   $this->renderLayout();
    }
	
	public function filterAction()
    {
		$asel = $this->getRequest()->getParam('asel');
		$partner = $this->getRequest()->getParam('partner');
		$warehouse = $this->getRequest()->getParam('warehouse');
		
		Mage::getSingleton('core/session')->setAsel($asel);
		Mage::getSingleton('core/session')->setRepPartner($partner);
		Mage::getSingleton('core/session')->setRepWarehouse($warehouse);
		
		$refererUrl = $this->_getRefererUrl();
		$this->getResponse()->setRedirect($refererUrl);
		return $this;
    }
	
}