<?php
class Cdot_Picklist_Adminhtml_PickupcountController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Pickup Method Count"));
	   $this->renderLayout();
    }
	
	public function filterAction()
    {
		$asel = $this->getRequest()->getParam('asel');
		$selDate = $this->getRequest()->getParam('date');
		$selDateto = $this->getRequest()->getParam('dateto');
		$partner = $this->getRequest()->getParam('partner');
		$pickupmethod = $this->getRequest()->getParam('pickupmethod');
		Mage::getSingleton('core/session')->setAsel($asel);
		Mage::getSingleton('core/session')->setRepSelDate($selDate);
		Mage::getSingleton('core/session')->setRepSelDateTo($selDateto);
		Mage::getSingleton('core/session')->setRepPartner($partner);
		Mage::getSingleton('core/session')->setRepPickup($pickupmethod);
		
		$refererUrl = $this->_getRefererUrl();
		$this->getResponse()->setRedirect($refererUrl);
		return $this;
    }
	
}