<?php
class Cdot_Picklist_Adminhtml_ProductpartnersController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Products of Partners"));
	   $this->renderLayout();
    }
	
	public function filterAction()
    {
		$asel = $this->getRequest()->getParam('asel');
		$partner = $this->getRequest()->getParam('partner');
		
		Mage::getSingleton('core/session')->setAsel($asel);
		Mage::getSingleton('core/session')->setRepPartner($partner);
		
		
		$refererUrl = $this->_getRefererUrl();
		
		$this->getResponse()->setRedirect($refererUrl);
		return $this;
    }
	
}