<?php
class Cdot_Picklist_Adminhtml_PickliststatuschangeController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Picklist Status Change"));
	   $this->renderLayout();
    }
	
	public function orderAction()
    {
       $order_id = $this->getRequest()->getParam('order_id');
	   Mage::getSingleton('core/session')->setOrderIdss($order_id);
	   
	   $refererUrl = $this->_getRefererUrl();
	   $this->getResponse()->setRedirect($refererUrl);
	   return $this;
    }
	
}