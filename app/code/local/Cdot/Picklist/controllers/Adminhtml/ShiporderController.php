<?php
class Cdot_Picklist_Adminhtml_ShiporderController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Shipped Orders"));
	   $this->renderLayout();
    }
	
	public function filterAction()
    {
		$asel = $this->getRequest()->getParam('asel');
		$selDate = $this->getRequest()->getParam('date');
		$selDateto = $this->getRequest()->getParam('dateto');
		
		Mage::getSingleton('core/session')->setAsel($asel);
		Mage::getSingleton('core/session')->setRepSelDate($selDate);
		Mage::getSingleton('core/session')->setRepSelDateTo($selDateto);
		
		$refererUrl = $this->_getRefererUrl();
		
		
		$this->getResponse()->setRedirect($refererUrl);
		return $this;
    }
	
}