<?php
class Cdot_Picklist_Adminhtml_OrderdetailsController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Order Details"));
	   $this->renderLayout();
    }
	
	public function filterAction()
    {
		$asel = $this->getRequest()->getParam('asel');
		$selDate = $this->getRequest()->getParam('date');
		$selDateto = $this->getRequest()->getParam('dateto');
		$partner = $this->getRequest()->getParam('partner');
		$warehouse = $this->getRequest()->getParam('warehouse');
		$pickupmethod = $this->getRequest()->getParam('pickupmethod');
		Mage::getSingleton('core/session')->setAsel($asel);
		Mage::getSingleton('core/session')->setRepSelDate($selDate);
		Mage::getSingleton('core/session')->setRepSelDateTo($selDateto);
		Mage::getSingleton('core/session')->setRepPartner($partner);
		Mage::getSingleton('core/session')->setRepWarehouse($warehouse);
		Mage::getSingleton('core/session')->setRepPickup($pickupmethod);
		$userArray = Mage::getSingleton('admin/session')->getData();
		 
		$user = Mage::getSingleton('admin/session'); 
		$userId = $user->getUser()->getUserId();
		$userEmail = $user->getUser()->getEmail();
		$userFirstname = $user->getUser()->getFirstname();
		$userLastname = $user->getUser()->getLastname();
		$userUsername = $user->getUser()->getUsername();
		
		$refererUrl = $this->_getRefererUrl();
		/*$data = array('user_id'=>$userId,'name'=>$userUsername,'url'=>$refererUrl);
		$model = Mage::getModel('picklist/picklist')->setData($data);
		try {
			$insertId = $model->save()->getId();
			echo "Data successfully inserted. Insert ID: ".$insertId;
		} catch (Exception $e){
			echo $e->getMessage();   
		}*/
		
		$this->getResponse()->setRedirect($refererUrl);
		return $this;
    }
	
}