<?php
class Cdot_Picklist_Adminhtml_OrderzipController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Most Order Zip Code"));
	   $this->renderLayout();
    }
	
	public function filterAction()
    {
		$asel = $this->getRequest()->getParam('asel');
		$selDate = $this->getRequest()->getParam('date');
		$selDateto = $this->getRequest()->getParam('dateto');
		
		Mage::getSingleton('core/session')->setAsel($asel);
		Mage::getSingleton('core/session')->setRepSelDate($selDate);
		Mage::getSingleton('core/session')->setRepSelDateTo($selDateto);
		
		$userArray = Mage::getSingleton('admin/session')->getData();
		 
		
		
		$refererUrl = $this->_getRefererUrl();
		
		
		$this->getResponse()->setRedirect($refererUrl);
		return $this;
    }
	
}