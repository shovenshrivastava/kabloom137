<?php

class Cdot_Partner_Adminhtml_PartnerController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("partner/partner")->_addBreadcrumb(Mage::helper("adminhtml")->__("Partner  Manager"),Mage::helper("adminhtml")->__("Partner Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Partner"));
			    $this->_title($this->__("Manager Partner"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Partner"));
				$this->_title($this->__("Partner"));
			    $this->_title($this->__("Edit Partner"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("partner/partner")->load($id);
				if ($model->getId()) {
					Mage::register("partner_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("partner/partner");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Partner Manager"), Mage::helper("adminhtml")->__("Partner Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Partner Description"), Mage::helper("adminhtml")->__("Partner Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("partner/adminhtml_partner_edit"))->_addLeft($this->getLayout()->createBlock("partner/adminhtml_partner_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("partner")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Partner"));
		$this->_title($this->__("Partner"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("partner/partner")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("partner_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("partner/partner");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Partner Manager"), Mage::helper("adminhtml")->__("Partner Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Partner Description"), Mage::helper("adminhtml")->__("Partner Description"));


		$this->_addContent($this->getLayout()->createBlock("partner/adminhtml_partner_edit"))->_addLeft($this->getLayout()->createBlock("partner/adminhtml_partner_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {

						

						$model = Mage::getModel("partner/partner")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Partner was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setPartnerData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setPartnerData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("partner/partner");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('partner_ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("partner/partner");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'partner.csv';
			$grid       = $this->getLayout()->createBlock('partner/adminhtml_partner_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'partner.xml';
			$grid       = $this->getLayout()->createBlock('partner/adminhtml_partner_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
