<?php
class Cdot_Partner_Block_Adminhtml_Partner_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("partner_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("partner")->__("Partner Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("partner")->__("Partner Information"),
				"title" => Mage::helper("partner")->__("Partner Information"),
				"content" => $this->getLayout()->createBlock("partner/adminhtml_partner_edit_tab_form")->toHtml(),
				));
				
			
				return parent::_beforeToHtml();
		}

}
