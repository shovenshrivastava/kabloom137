<?php

class Cdot_Partner_Block_Adminhtml_Partner_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("partnerGrid");
				$this->setDefaultSort("partner_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("partner/partner")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("partner_id", array(
				"header" => Mage::helper("partner")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "partner_id",
				));
                
				$this->addColumn("partner_name", array(
				"header" => Mage::helper("partner")->__("Partner Name"),
				"index" => "partner_name",
				));
						$this->addColumn('status', array(
						'header' => Mage::helper('partner')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Cdot_Partner_Block_Adminhtml_Partner_Grid::getOptionArray2(),				
						));
										
		 $this->addRssList('partner/adminhtml_rss_rss/partner', Mage::helper('partner')->__('RSS'));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('partner_id');
			$this->getMassactionBlock()->setFormFieldName('partner_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_partner', array(
					 'label'=> Mage::helper('partner')->__('Remove Partner'),
					 'url'  => $this->getUrl('*/adminhtml_partner/massRemove'),
					 'confirm' => Mage::helper('partner')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray2()
		{
            $data_array=array(); 
			$data_array[0]='enabled';
			$data_array[1]='disabled';
            return($data_array);
		}
		static public function getValueArray2()
		{
            $data_array=array();
			foreach(Cdot_Partner_Block_Adminhtml_Partner_Grid::getOptionArray2() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getWarehousesArray2()
		{			
			$collection = Mage::getModel("pointofsale/pointofsale")->getCollection();			
			$warehouses = $collection->load();	
			$data_array=array();
				foreach($warehouses as $warehouse)
				{
				
				$i= $warehouse->getPlaceId();	
				$data_array[$i] = $warehouse->getName();
				}			
            return($data_array);
		}
		static public function getCarrierOptionArray2()
		{
            $data_array=array(); 
			$data_array['ups']='UPS';
			$data_array['fedex']='FEDEX';
            return($data_array);
		}
		static public function getCarrierArray2	()
		{			
			$data_array=array();
			foreach(Cdot_Partner_Block_Adminhtml_Partner_Grid::getCarrierOptionArray2() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);
		}
		

}
