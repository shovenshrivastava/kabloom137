<?php
	
class Cdot_Partner_Block_Adminhtml_Partner_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "partner_id";
				$this->_blockGroup = "partner";
				$this->_controller = "adminhtml_partner";
				$this->_updateButton("save", "label", Mage::helper("partner")->__("Save Partner"));
				$this->_updateButton("delete", "label", Mage::helper("partner")->__("Delete Partner"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("partner")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("partner_data") && Mage::registry("partner_data")->getId() ){

				    return Mage::helper("partner")->__("Edit Partner '%s'", $this->htmlEscape(Mage::registry("partner_data")->getId()));

				} 
				else{

				     return Mage::helper("partner")->__("Add Partner");

				}
		}
}
