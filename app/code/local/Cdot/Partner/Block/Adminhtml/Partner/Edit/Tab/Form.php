<?php
class Cdot_Partner_Block_Adminhtml_Partner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("partner_form", array("legend"=>Mage::helper("partner")->__("Partner information")));

				
						$fieldset->addField("partner_name", "text", array(
						"label" => Mage::helper("partner")->__("Partner Name"),
						"name" => "partner_name",
						));
									
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('partner')->__('Status'),
						'values'   => Cdot_Partner_Block_Adminhtml_Partner_Grid::getValueArray2(),
						'name' => 'status',
						));
						   $patner_option=Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray();
                                                 $fieldset->addField('priority', 'select', array(
						'label'     => Mage::helper('partner')->__('Generate track before invoice (EDI)'),
						//'values'   => Cdot_Partner_Block_Adminhtml_Partner_Grid::getValueArray3(),
						'values'   => $patner_option,
						'name' => 'priority',
						));

				if (Mage::getSingleton("adminhtml/session")->getPartnerData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getPartnerData());
					Mage::getSingleton("adminhtml/session")->setPartnerData(null);
				} 
				elseif(Mage::registry("partner_data")) {
				    $form->setValues(Mage::registry("partner_data")->getData());
				}
				return parent::_prepareForm();
		}
}
