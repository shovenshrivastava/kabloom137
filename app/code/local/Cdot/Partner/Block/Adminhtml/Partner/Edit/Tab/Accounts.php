<?php
class Cdot_Partner_Block_Adminhtml_Partner_Edit_Tab_Accounts extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("partner_form", array("legend"=>Mage::helper("partner")->__("Shipping Account information")));

				
															
						 $fieldset->addField('poinofsale', 'select', array(
						'label'     => Mage::helper('partner')->__('WareHouse'),
						'values'   => Cdot_Partner_Block_Adminhtml_Partner_Grid::getWarehousesArray2(),
						'name' => 'warehouse',
						));
						$fieldset->addField('carrier', 'select', array(
						'label'     => Mage::helper('partner')->__('Carrier'),
						'values'   => Cdot_Partner_Block_Adminhtml_Partner_Grid::getCarrierArray2(),
						'name' => 'carrier',
						));
						$fieldset->addField("account_number", "text", array(
						"label" => Mage::helper("partner")->__("Account Number"),
						"name" => "account_number",
						));
						$fieldset->addField("userid", "text", array(
						"label" => Mage::helper("partner")->__("User ID"),
						"name" => "user_id",
						));
						$fieldset->addField("access", "text", array(
						"label" => Mage::helper("partner")->__("Access Key"),
						"name" => "access_key",
						));
						
						

				if (Mage::getSingleton("adminhtml/session")->getPartnerData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getPartnerData());
					Mage::getSingleton("adminhtml/session")->setPartnerData(null);
				} 
				elseif(Mage::registry("partner_data")) {
				    $form->setValues(Mage::registry("partner_data")->getData());
				}
				return parent::_prepareForm();
		}
}
