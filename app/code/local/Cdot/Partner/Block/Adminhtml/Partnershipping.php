<?php


class Cdot_Partner_Block_Adminhtml_Partnershipping extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_partnershipping";
	$this->_blockGroup = "partner";
	$this->_headerText = Mage::helper("partner")->__("Shipping Accounts Manager");
	$this->_addButtonLabel = Mage::helper("partner")->__("Add New Shipping Account");
	parent::__construct();
	
	}

}
