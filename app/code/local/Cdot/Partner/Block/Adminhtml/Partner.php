<?php


class Cdot_Partner_Block_Adminhtml_Partner extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_partner";
	$this->_blockGroup = "partner";
	$this->_headerText = Mage::helper("partner")->__("Partner Manager");
	$this->_addButtonLabel = Mage::helper("partner")->__("Add New Partner");
	parent::__construct();
	
	}

}
