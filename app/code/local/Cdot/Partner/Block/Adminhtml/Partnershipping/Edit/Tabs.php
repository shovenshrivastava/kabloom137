<?php
class Cdot_Partner_Block_Adminhtml_Partnershipping_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("partnershipping_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("partner")->__("Shipping Account Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("partner")->__("Shipping Account Information"),
				"title" => Mage::helper("partner")->__("Shipping AccountInformation"),
				"content" => $this->getLayout()->createBlock("partner/adminhtml_partnershipping_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
