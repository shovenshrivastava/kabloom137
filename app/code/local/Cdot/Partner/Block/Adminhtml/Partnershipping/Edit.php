<?php
	
class Cdot_Partner_Block_Adminhtml_Partnershipping_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "partner_shipping_id";
				$this->_blockGroup = "partner";
				$this->_controller = "adminhtml_partnershipping";
				$this->_updateButton("save", "label", Mage::helper("partner")->__("Save Shipping Account"));
				$this->_updateButton("delete", "label", Mage::helper("partner")->__("Delete Shipping Account"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("partner")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("partnershipping_data") && Mage::registry("partnershipping_data")->getId() ){

				    return Mage::helper("partner")->__("Edit Shipping Account '%s'", $this->htmlEscape(Mage::registry("partnershipping_data")->getId()));

				} 
				else{

				     return Mage::helper("partner")->__("Add Shipping Account");

				}
		}
}
