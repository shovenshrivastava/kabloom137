<?php
class Cdot_Partner_Block_Adminhtml_Partnershipping_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("partner_form", array("legend"=>Mage::helper("partner")->__("Shipping Account information")));

				
						//$fieldset->addField("partner_shipping_id", "text", array(
						//"label" => Mage::helper("partner")->__("partner_shiiping_id"),
						//"name" => "partner_shipping_id",
						//));
						$fieldset->addField('partner_id', 'select', array(
						'label'     => Mage::helper('partner')->__('Partner Name'),
						'values'   => Cdot_Partner_Block_Adminhtml_Partnershipping_Grid::getValueArray6(),
						'name' => 'partner_id',
						));
									
						 $fieldset->addField('partner_warehouse', 'select', array(
						'label'     => Mage::helper('partner')->__('Partner Warehouse'),
						'values'   => Cdot_Partner_Block_Adminhtml_Partnershipping_Grid::getValueArray4(),
						'name' => 'partner_warehouse',
						));				
						 $fieldset->addField('partner_carrier', 'select', array(
						'label'     => Mage::helper('partner')->__('Shipping Carrier'),
						'values'   => Cdot_Partner_Block_Adminhtml_Partnershipping_Grid::getValueArray5(),
						'name' => 'partner_carrier',
						));
						$fieldset->addField("partner_user", "text", array(
						"label" => Mage::helper("partner")->__("Partner User ID"),
						"name" => "partner_user",
						));
					
						$fieldset->addField("partner_account", "text", array(
						"label" => Mage::helper("partner")->__("Shipping Account"),
						"name" => "partner_account",
						));
					
						$fieldset->addField("partner_account_password", "text", array(
						"label" => Mage::helper("partner")->__("Shipping Accont Password"),
						"name" => "partner_account_password",
						));
					
						$fieldset->addField("partner_license", "text", array(
						"label" => Mage::helper("partner")->__("Shipping Account License Key"),
						"name" => "partner_license",
						));
					
						$fieldset->addField("partner_meter", "text", array(
						"label" => Mage::helper("partner")->__("Shipping Account Meter Number"),
						"name" => "partner_meter",
						));
					

				if (Mage::getSingleton("adminhtml/session")->getPartnershippingData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getPartnershippingData());
					Mage::getSingleton("adminhtml/session")->setPartnershippingData(null);
				} 
				elseif(Mage::registry("partnershipping_data")) {
				    $form->setValues(Mage::registry("partnershipping_data")->getData());
				}
				return parent::_prepareForm();
		}
}
