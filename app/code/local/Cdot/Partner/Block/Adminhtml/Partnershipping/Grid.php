<?php

class Cdot_Partner_Block_Adminhtml_Partnershipping_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("partnershippingGrid");
				$this->setDefaultSort("partner_shipping_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("partner/partnershipping")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("partner_shipping_id", array(
				"header" => Mage::helper("partner")->__("Shipping ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "partner_shipping_id",
				));
                $this->addColumn('partner_name', array(
						'header' => Mage::helper('partner')->__('Partner Name'),
						'index' => 'partner_id',
						'type' => 'options',
						'options'=>Cdot_Partner_Block_Adminhtml_Partnershipping_Grid::getOptionArray7(),				
						));
						$this->addColumn('partner_warehouse', array(
						'header' => Mage::helper('partner')->__('Partner Warehouse'),
						'index' => 'partner_warehouse',
						'type' => 'options',
						'options'=>Cdot_Partner_Block_Adminhtml_Partnershipping_Grid::getOptionArray4(),				
						));
						
						$this->addColumn('partner_carrier', array(
						'header' => Mage::helper('partner')->__('Shipping Carrier'),
						'index' => 'partner_carrier',
						'type' => 'options',
						'options'=>Cdot_Partner_Block_Adminhtml_Partnershipping_Grid::getOptionArray5(),				
						));
					$this->addColumn("partner_user", array(
				"header" => Mage::helper("partner")->__("Partner User ID"),
				"index" => "partner_user",
				));	
				$this->addColumn("partner_account", array(
				"header" => Mage::helper("partner")->__("Shipping Account"),
				"index" => "partner_account",
				));
				$this->addColumn("partner_account_password", array(
				"header" => Mage::helper("partner")->__("Shipping Account Password"),
				"index" => "partner_account_password",
				));
				$this->addColumn("partner_license", array(
				"header" => Mage::helper("partner")->__("Shipping Account License Key"),
				"index" => "partner_license",
				));
				$this->addColumn("partner_meter", array(
				"header" => Mage::helper("partner")->__("Shipping Account Meter Number"),
				"index" => "partner_meter",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('partner_shipping_id');
			$this->getMassactionBlock()->setFormFieldName('partner_shipping_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_partnershipping', array(
					 'label'=> Mage::helper('partner')->__('Remove Partnershipping'),
					 'url'  => $this->getUrl('*/adminhtml_partnershipping/massRemove'),
					 'confirm' => Mage::helper('partner')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray4()
		{
			
			$collection = Mage::getModel("pointofsale/pointofsale")->getCollection();			
			$warehouses = $collection->load();	
			$data_array=array();
				foreach($warehouses as $warehouse)
				{
				
				$i= $warehouse->getPlaceId();	
				$data_array[$i] = $warehouse->getName();
				}			
            return($data_array);
		}
		
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Cdot_Partner_Block_Adminhtml_Partnershipping_Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray5()
		{
            $data_array=array(); 
			$data_array[0]='fedex';
			$data_array[1]='ups';
            return($data_array);
		}
		static public function getValueArray5()
		{
            $data_array=array();
			foreach(Cdot_Partner_Block_Adminhtml_Partnershipping_Grid::getOptionArray5() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		static public function getOptionArray6()
		{
			
			$collection = Mage::getModel("partner/partner")->getCollection();			
			$partners = $collection->load();	
			$data_array=array();
				foreach($partners as $partner)
				{
				
				$i= $partner->getPartnerId();		
					
				$data_array[$i] = $partner->getPartnerName();
				}			
				return($data_array);
		}
		
		static public function getValueArray6()
		{
            $data_array=array();
			foreach(Cdot_Partner_Block_Adminhtml_Partnershipping_Grid::getOptionArray6() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		static public function getOptionArray7()
		{
				$data_array=array();
				$collection = Mage::getModel("partner/partnershipping")->getCollection();			
				$partners = $collection->load();
				foreach($partners as $partner)
				{				
				$i= $partner->getPartnerId();	
				$collection = Mage::getResourceModel('partner/partner_collection');				
				$partner = Mage::getSingleton("partner/partner")->load($i);
				$data_array[$i] = $partner->getPartnerName();
				}						
				return($data_array);
		}
		
		static public function getValueArray7()
		{
            $data_array=array();
			foreach(Cdot_Partner_Block_Adminhtml_Partnershipping_Grid::getOptionArray7() as $k=>$v)
			{				
               $data_array[]=array('value'=>$k,'label'=>$v);	
			}           
            return($data_array);
		}
}
