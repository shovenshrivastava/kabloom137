<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table partner(partner_id int not null auto_increment, partner_name varchar(255),status int(10), primary key(partner_id));
    insert into partner values(1,'amazon,1);
    insert into partner values(2,'rulala',1);
		
create table if not exists partner_shipping(partner_shipping_id int not null auto_increment,partner_warehouse varchar(255),	partner_carrier	 varchar(255),partner_account varchar(255),	partner_user varchar(255),	partner_license varchar(255),	partner_meter varchar(255),	partner_id int,	foreign key (partner_id) references partner(partner_id) ON UPDATE CASCADE ON DELETE CASCADE,primary key(partner_shipping_id));
    
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 
