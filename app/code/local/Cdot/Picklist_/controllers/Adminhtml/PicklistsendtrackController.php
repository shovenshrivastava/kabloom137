<?php
class Cdot_Picklist_Adminhtml_PicklistsendtrackController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Boxing Tab"));
	   $this->renderLayout();
    }
	
	public function orderAction()
    {
       $order_id = $this->getRequest()->getParam('order_id');
	   Mage::getSingleton('core/session')->setOrderIdss($order_id);
	   $send = $this->getRequest()->getParam('send');
	   Mage::getSingleton('core/session')->setSendtrack($send);
	   $refererUrl = $this->_getRefererUrl();
	   $this->getResponse()->setRedirect($refererUrl);
	   return $this;
    }
	
}