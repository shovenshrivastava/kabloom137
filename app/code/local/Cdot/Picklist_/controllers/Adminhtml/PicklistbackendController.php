<?php
class Cdot_Picklist_Adminhtml_PicklistbackendController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Picklist"));
	   $this->renderLayout();
    }
	
	public function filterAction()
    {
		$prod_order = $this->getRequest()->getParam('prod_order');
		$prodSku = $this->getRequest()->getParam('prod_sku');
		$prodSku1 = $this->getRequest()->getParam('prod_sku1');
		//echo $prodSku1; exit;
		$selDate = $this->getRequest()->getParam('sel_date');
		$selDateto = $this->getRequest()->getParam('sel_dateto');
		$warehouse = $this->getRequest()->getParam('warehouse');
		Mage::getSingleton('core/session')->setProdorder($prod_order);
		Mage::getSingleton('core/session')->setProdSku($prodSku);
		Mage::getSingleton('core/session')->setProdSkuu($prodSku1);
		Mage::getSingleton('core/session')->setSelDate($selDate);
		Mage::getSingleton('core/session')->setSelDateTo($selDateto);
		Mage::getSingleton('core/session')->setWarehouse($warehouse);
		$refererUrl = $this->_getRefererUrl();
		$this->getResponse()->setRedirect($refererUrl);
		return $this;
    }
	
	public function pdfAction()
    {
		$pdf = new Zend_Pdf(); 
 
		// Add new page to the document 
		$page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4); 
		$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
		$page->setFont($font, 10);

		$page->drawText('Hello world!', 100, 510); 

		$pdf->pages[] = $page; 
		 
		// Get PDF document as a string 
		$pdfData = $pdf->render(); 
		 
		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/x-pdf"); 
		echo $pdfData;
    }
}
