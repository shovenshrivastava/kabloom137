<?php
class Cdot_Picklist_Model_Observer
{
    public function initPicklist($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if (get_class($block) == 'Mage_Adminhtml_Block_Widget_Grid_Massaction' || get_class($block) == 'Enterprise_SalesArchive_Block_Adminhtml_Sales_Order_Grid_Massaction') {
            $block->addItem('picklist', array(
                'label' => 'Bar Code',
                'url' => str_replace('index.php','',Mage::getBaseUrl('web', true)).'barprint.phtml',
            ));
        }
    }
}