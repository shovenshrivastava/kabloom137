<?php
class Cdotsys_Kabloomsapi_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Kabloom"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("doctors location", array(
                "label" => $this->__("Doctors Location"),
                "title" => $this->__("Doctors Location")
		   ));

      $this->renderLayout(); 
	  
    }
	public function customercreateAction()
        {
			$allData = Mage::app()->getRequest()->getParams();
			
			$email = $allData['email'];
			$firstname = $allData['firstname'];
			$lastname = $allData['lastname'];
			$password = $allData['password'];
			
			$client = new SoapClient('http://52.24.36.137/index.php/api/soap/?wsdl');

			// If somestuff requires api authentification,
			// then get a session token
			$session = $client->login('kabloomapi', 'kabloomapi');
			
			$result = $client->call($session,'customer.create',array(array(
			'email' => $email, 
			'firstname' => $firstname, 
			'lastname' => $lastname, 
			'password' => $password, 
			'website_id' => 1, 
			'store_id' => 1, 
			'group_id' => 1)));
	
			$final_data['code']=1;
			$final_data['message']='Your account created successfully';
			echo json_encode($final_data);

			//var_dump ($result); // If you don't need the session anymore
			$client->endSession($session);
			
        }        
		public function catalogcategoryinfoAction()
        {
			$allData = Mage::app()->getRequest()->getParams();
			$client = new SoapClient('http://homealive.asia/api/soap/?wsdl');

			// If somestuff requires api authentification,
			// then get a session token
			$session = $client->login('kabloom', 'kabloom');

			$result = $client->call($session, 'catalog_category.info', '5');
			echo "<pre>";print_r($result);
			
			// If you don't need the session anymore
			$client->endSession($session);
        }        
		public function catalogcategorytreeAction(){
			$client = new SoapClient('http://52.24.36.137/index.php/api/soap/?wsdl');

			// If somestuff requires api authentification,
			// then get a session token
			$session = $client->login('kabloomapi', 'kabloomapi');

			$result = $client->call($session, 'catalog_category.tree');
			$data = $result['children'][0]['children'];
			//echo "<pre>";print_r($data);
			$final_cat['code'] =1;
			$final_cat['message'] ='success';
			foreach($data as $value){
				
				$category_id = $value['category_id'];
				$name = $value['name'];
				$code=1;
				$message='success';
				$dataCollection=array('category_id'=>$category_id,'name'=>$name);
				
				//array_push($dataCollection,$category_id,$name);
				//echo json_encode($dataCollection);
				$final_cat[] = $dataCollection;
				
			}	
			
			//echo "<pre>";print_r($final_cat);
			echo json_encode($final_cat);
			// If you don't need the session anymore
			//$client->endSession($session);
		}
		public function categoryproductsAction()
        {
			/* $allData = Mage::app()->getRequest()->getParams();
			$category_id = $allData['id']; */
			
			$client = new SoapClient('http://52.24.36.137/index.php/api/soap/?wsdl');

			// If somestuff requires api authentification,
			// then get a session token
			$session = $client->login('kabloomapi', 'kabloomapi');

			$result = $client->call($session, 'catalog_category.assignedProducts', '186');
			//echo "<pre>";print_r($result);
			$mediaBaseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
			$imageUrl = $mediaBaseUrl.'catalog/product';
			foreach($result as $value){
				//echo $value['product_id'];
				$v = Mage::getModel('catalog/product')->load($value['product_id']);
				$productData = $v->getData();
				
				$final_data=array(
				'product_name'=>$productData['name'],
				'product_id'=>$productData['entity_id'],
				'product_price'=>$productData['price'],
				'product_special_price'=>$productData['special_price'],
				'product_description'=>$productData['description'],
				'product_thumnail_image'=>$imageThumbnail=$imageUrl.''.$productData['thumbnail'],
				'product_main_image'=>$mainImage =$imageUrl.''.$productData['image']
				);
				$final[] = $final_data;			
				
			}
			echo json_encode($final,JSON_PRETTY_PRINT);

			// If you don't need the session anymore
			$client->endSession($session);
        }        
		public function cartProductAdd()
        {
			$proxy = new SoapClient('http://homealive.asia/api/soap/?wsdl');
			$sessionId = $proxy->login('kabloom', 'kabloom');

			$quoteId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );
			$arrProducts = array(
				array(
					“product_id” => “1”,
					“qty” => 2
				)
			);
			$resultCartProductAdd = $proxy->call(
				$sessionId,
				“cart_product.add”,
				array(
					$quoteId,
					$arrProducts
				)
			);
        }        
		public function cartproductlistAction()
        {
			$client = new SoapClient('http://homealive.asia/api/soap/?wsdl');

			// If somestuff requires api authentification,
			// then get a session token
			$session = $client->login('kabloom', 'kabloom');

			$result = $client->call($session, 'cart_product.list', '15');
			var_dump ($result);
			
        }        
		public function cartProductRemove()
        {
			$proxy = new SoapClient('http://homealive.asia/api/soap/?wsdl');
			$sessionId = $proxy->login('kabloom', 'kabloom');

			$shoppingCartIncrementId = $proxy->call( $sessionId, 'cart.create', array( 'magento_store' ) );
			$arrProducts = array(
				array(
					“product_id” => “1”,
					“qty” => 2
				),
				array(
					“sku” => “testSKU”,
					“quantity” => 4
				)
			);
			$resultCartProductAdd = $proxy->call(
				$sessionId,
				“cart_product.add”,
				array(
					$shoppingCartId,
					$arrProducts
				)
			);
			$arrProducts = array(
				array(
					“product_id” => “1”
				),
			);
			$resultCartProductUpdate = $proxy->call(
				$sessionId,
				“cart_product.remove”,
				array(
					$shoppingCartId,
					$arrProducts
				)
			);
        }
}