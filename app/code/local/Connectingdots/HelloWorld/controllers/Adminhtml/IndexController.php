<?php

class Connectingdots_HelloWorld_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action{

   
   public function indexAction() 
   {
   	
	        $this->loadLayout(); 
			$this->get_all_bundle_products();
			
			
			Mage::app()->getResponse()->setRedirect(Mage::helper("adminhtml")->getUrl("adminhtml/catalog_product/index/"));
			$this->_getSession()->addSuccess($this->__('The Bundle Inventory Syncing is Successfull'));
			
			$this->renderLayout();

   }
   
   public function get_all_bundle_products()
	{
	
		$collectionBundle = array( 1 => "KITF1015",2 => "KITF1400",3 => "KITF1380", 4 => "KITF1410", 5 => "KITF1420");
		 
			
		// $collectionBundle = Mage::getResourceModel('catalog/product_collection')
					// ->addAttributeToFilter('type_id', array('eq' => 'bundle'));
		foreach ($collectionBundle as $option)
		{ 
			//$option_id = $option->getSku();
			$option_id = $option;
			$warehouse = Mage::getModel('pointofsale/pointofsale')->getCollection();

			foreach($warehouse->getData() as $x=>$y)
			{
				$place_id = $y['place_id'];
				$this->add_my_Inventory($option_id,1,$place_id);
			}
			
		}
	 return;
    }
	
	
	public function add_my_Inventory($sku,$qty,$warehouse_id)
	{	
	
		if($sku !="")
		{	
			// print "<BR>";print "<BR>";print "<BR>";
			// print $sku;print "<BR>";
			// print $qty;print "<BR>";
			// print $warehouse_id;print "<BR>";
					
			 $product_id = Mage::getModel("catalog/product")->getIdBySku($sku);
			 $product = Mage::getModel('catalog/product')->load($product_id);
			
			 $attributeValue= Mage::getResourceModel('catalog/product')->getAttributeRawValue($product_id, 'internal_sku');
			 
			 
			 $store_id = $product->getStoreIds();
			 $options = Mage::getModel('bundle/option')->getResourceCollection()
												  ->setProductIdFilter($product_id)
												  ->setPositionOrder(); 
			 $options->joinValues($store_id[0]);
			 $selections = $product->getTypeInstance(true)
							  ->getSelectionsCollection($product->getTypeInstance(true)
							  ->getOptionsIds($product), $product);

			foreach ($options->getItems() as $option)
			{
				$option_id = $option->getId();
				$getTitle = $option->getTitle();
				
				if( $option->getTitle()=='Flowers')
				{
					foreach($selections as $selection)
					{ 
						if($option_id == $selection->getOptionId())
						{	
							$pro_id=$selection->getId();
							
							$arr[$selection->getSku()] = $selection->getSelectionQty();
							$stk_level[$selection->getSku()] = $this->get_ware_house_stk($pro_id,$warehouse_id);
							
						}
					}
				}
				if( $option->getTitle()=='Select a vase')
				{
					foreach($selections as $selection)
					{ 
						if($option_id == $selection->getOptionId())
						{
							$pro_id=$selection->getId();
							$arr_vaise[$selection->getSku()] = $selection->getSelectionQty();
						
							$stk_level_vaise[$selection->getSku()] = $this->get_ware_house_stk($pro_id,$warehouse_id);
						}
					}
				}
				if( $option->getTitle()=='Add on')
				{
					foreach($selections as $selection)
					{ 
						if($option_id == $selection->getOptionId())
						{	
							$pro_id=$selection->getId();
							
							$arr_addon[$selection->getSku()] = $selection->getSelectionQty();
							$stk_level_addon[$selection->getSku()] =$this->get_ware_house_stk($pro_id,$warehouse_id);
						}
					}
				}
			}
				
				$keys = array_keys($arr);
				foreach ($keys as $key) 
				{
					$out[$key] = intval($stk_level[$key]/$arr[$key]);
				}
				
				$vaise_keys = array_keys($arr_vaise);
				foreach ($vaise_keys as $vaise_key) 
				{
					$out_vaise[$vaise_key] = intval($stk_level_vaise[$vaise_key]/$arr_vaise[$vaise_key]);
				}
				$addon_keys = array_keys($arr_addon);
				foreach ($addon_keys as $addon_key) 
				{
					$out_addon[$addon_key] = intval($stk_level_addon[$addon_key]/$arr_addon[$addon_key]);
				}
			
				// echo "<pre>";print_r($arr);print "<BR>";
				// print_r($stk_level);print "<BR>";
				// print_r($out);print "<BR>";
				
				// print_r($arr_vaise);print "<BR>";
				// print_r($stk_level_vaise);print "<BR>";
				// print_r($out_vaise);print "<BR>";

				
				// print_r($arr_addon);print "<BR>";
				// print_r($stk_level_addon);print "<BR>";
				// print_r($out_addon);print "<BR>";
				
				
				
				foreach ($stk_level_vaise as $key => $value) 
				{
					foreach ($stk_level_addon as $k => $v) 
					{						
						$combined_sku[] = $sku . "-" . $key . "-" . $k;
						
					}
					
				}
					//echo"<PRE>";print_r($combined_sku);echo"<BR>";
					
				
				$inv_value=min($out);
				
			foreach($combined_sku as $comb_sku)
			{
				$sku_id = Mage::getModel('catalog/product')->getIdBySku($comb_sku);
				$part = explode('-',$comb_sku);
				$main_sku=$part[0];
				$vaise_sku=$part[1];
				$addon_sku=$part[2];
				//echo $sku_id;echo"<BR>";
										
						if (array_key_exists($vaise_sku,$out_vaise))
						{	
							$vaise_min_value= $out_vaise[$vaise_sku];
						}
						if (array_key_exists($addon_sku,$out_addon))
						{	
							$addon_min_value= $out_addon[$addon_sku];
						}
						
						$min_stk_val= min($inv_value,$vaise_min_value,$addon_min_value);
						
							$collection = Mage::getModel('catalog/product')->getCollection();
							$collection->addAttributeToSelect('internal_sku');

							$collection->addFieldToFilter(array(array('attribute'=>'internal_sku','eq'=> $comb_sku),));

							foreach ($collection as $pid) 
							{	
								$internal_sku_id= $pid->getId();
								//print $internal_sku_id;print"<BR>";
															
								if($internal_sku_id != "")
								{
									$tot_int_sku_inv_qty= $this->update_all_inv($internal_sku_id,$min_stk_val,$warehouse_id);
  
									//echo "THIS IS TOTAL RETURNED INV QTY";echo"<BR>";echo $tot_int_sku_inv_qty;echo"<BR>";
									$int_sku_product = Mage::getModel('catalog/product');
									$int_sku_product ->load($internal_sku_id);
 
									$productInfoData = $int_sku_product->getData();
									$int_sku_stockData = $int_sku_product->getStockData();

									$int_sku_stockData['qty'] = $tot_int_sku_inv_qty;

									$int_sku_product->setStockData($int_sku_stockData);
									$int_sku_product->save();
									//echo "the Updated Qty Is TOT_INT_SKU_INV_QTY " . $tot_int_sku_inv_qty  ."DATA SAVED SUCCESS";echo"<BR>";
								}
								//echo "UPDATE THIS VALUES ". $min_stk_val;echo"<BR>";
							}
						
			
				
			}   
			
		}
		return;

	}

	public function get_ware_house_stk($pro_id,$warehouse_id)
	{	
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$writeConnection = $resource->getConnection('core_write');

		$query_qty_select_warehouse = "select `quantity_in_stock` from `advancedinventory_stock` where product_id =" . $pro_id . " and place_id =" . $warehouse_id;

		$existing_qty_warehouse = $readConnection->fetchOne($query_qty_select_warehouse);
		
		return $existing_qty_warehouse;
			
	}


	public function update_all_inv($product_id,$qty_ordered,$warehouse_id)
	{	
		// print $product_id;Print"<BR>";
		// print $qty_ordered;Print"<BR>";
		// print $warehouse_id;Print"<BR>";


		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$writeConnection = $resource->getConnection('core_write');

		$query_qty_select_warehouse = "select `quantity_in_stock` from `advancedinventory_stock` where product_id =" . $product_id . " and place_id =" . $warehouse_id;

		$existing_qty_warehouse = $readConnection->fetchOne($query_qty_select_warehouse);
		
		// print $existing_qty_warehouse;Print"<BR>";
		
		$updated_qty_warehouse = $qty_ordered;
		
		// print $updated_qty_warehouse;Print"<BR>";
		
		
		$query_qty_update = "UPDATE `advancedinventory_stock` SET `quantity_in_stock` = '" . $updated_qty_warehouse . "' WHERE  place_id = " . $warehouse_id . ' and ' . '`product_id` =' . $product_id;
		$writeConnection->query($query_qty_update); 
		
		
		$total_inv_qty = "SELECT SUM(quantity_in_stock) FROM advancedinventory_stock where product_id =" . $product_id ;
		
		$return_total_inv_qty = $readConnection->fetchOne($total_inv_qty);
		
		//print $return_total_inv_qty;Print"<BR>";
		return $return_total_inv_qty;
		
		
	}

	
	
	
}
?>