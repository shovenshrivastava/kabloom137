<?php
class Mageshopapps_Press_Block_Press extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
    public function getPress()     
    { 
        return Mage::getModel('press/press')->getCollection()
				->addFieldToFilter('status',1)
				->setOrder('position','DESC')
				->load();
    }
	public function getMostRecentPress()
	{
		$PressCollection = Mage::getModel('press/press')
						->getCollection()
						->addFieldToFilter('status',1)
						->setOrder('position',"DESC")
						->setPageSize(5)
						->load();
		return $PressCollection;
	}
}
