<?php

class Mageshopapps_Press_Block_Adminhtml_Press_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('press_form', array('legend'=>Mage::helper('press')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('press')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('small_image', 'image', array(
          'label'     => Mage::helper('press')->__('Small Image'),
		 // 'required'  => false,
		 'class'     => 'input-text required-entry required-file',
		  'required'  => true,
          'name'      => 'small_image',
		  'note'	  => 'Image Dimension: 232 * 218',
	   ));
      $fieldset->addField('full_image', 'image', array(
          'label'     => Mage::helper('press')->__('Full Image'),
		 'class'     => 'required-entry required-file',
		  'required'  => true,
          'name'      => 'full_image',
		
	  ));	  
	  
      $fieldset->addField('photo_url', 'text', array(
          'label'     => Mage::helper('press')->__('Url'),
          'required'  => false,
          'name'      => 'photo_url',
		  'note'	  => 'example: http://www.example.com/',
	  ));
	  
      $fieldset->addField('position', 'text', array(
          'label'     => Mage::helper('press')->__('Position'),
          'required'  => false,
          'name'      => 'position',
	  ));	  	  	  
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('press')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('press')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('press')->__('Disabled'),
              ),
          ),
      ));
	  
	  
	  $fieldset->addField('year', 'select', array(
          'label'     => Mage::helper('press')->__('Year'),
          'name'      => 'year',
          'values'    => array(
              array(
                  'value'     => 2009,
                  'label'     => Mage::helper('press')->__('2009'),
              ),
              array(
                  'value'     => 2010,
                  'label'     => Mage::helper('press')->__('2010'),
              ),
              array(
                  'value'     => 2011,
                  'label'     => Mage::helper('press')->__('2011'),
              ),

              array(
                  'value'     => 2012,
                  'label'     => Mage::helper('press')->__('2012'),
              ),

              array(
                  'value'     => 2013,
                  'label'     => Mage::helper('press')->__('2013'),
              ),
          ),
      ));
		if (!Mage::app()->isSingleStoreMode()) {
		   $fieldset->addField('store_view', 'multiselect', array(
				   'name'      => 'stores[]',
				   'label'     => Mage::helper('press')->__('Store View'),
				   'title'     => Mage::helper('press')->__('Store View'),
				//   'required'  => true,
				   'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
		   ));
  }
	else {
		   $fieldset->addField('store_view', 'hidden', array(
				   'name'      => 'stores[]',
				   'value'     => Mage::app()->getStore(true)->getId()
		   ));
  
  }
          if ( Mage::getSingleton('adminhtml/session')->getPressData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getPressData());
          Mage::getSingleton('adminhtml/session')->setPressData(null);
      } elseif ( Mage::registry('press_data') ) {
          $form->setValues(Mage::registry('press_data')->getData());
		  
		 
      }
	  
      return parent::_prepareForm();
  }
} ?>
