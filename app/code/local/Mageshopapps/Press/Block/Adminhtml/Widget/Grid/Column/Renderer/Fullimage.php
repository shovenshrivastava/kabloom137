<?php
class Mageshopapps_Press_Block_Adminhtml_Widget_Grid_Column_Renderer_fullimage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
    	if (empty($row['full_image'])) echo '';
    	echo '<img src="'. Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).$row['full_image']. '" width="200" height="200" />';
    }
}
