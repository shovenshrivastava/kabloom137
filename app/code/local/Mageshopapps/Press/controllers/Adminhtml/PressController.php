<?php

class Mageshopapps_Press_Adminhtml_PressController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('press/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('press/press')->load($id);
		
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
				//if($model->save() == true) { echo 'yes' ;} exit;	
			Mage::register('press_data', $model);
			
			$this->loadLayout();
			$this->_setActiveMenu('press/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('press/adminhtml_press_edit'))
				->_addLeft($this->getLayout()->createBlock('press/adminhtml_press_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('press')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
				
				 if(isset($data['stores'])) {
					  $stores = $data['stores'];
					  $storesCount = count($stores);
					  $storesIndex = 1;
					  $storesData = '';
                  foreach($stores as $store) {
						$storesData .= $store;
                   if($storesIndex < $storesCount) {
						$storesData .= ',';
                   }
					$storesIndex++;
                  }
					$data['store_view'] = $storesData;
                 } 

				
				if(isset($data['small_image']['delete'])	&&	$data['small_image']['delete']	==	1)	{
				$source_dir	=	Mage::getBaseDir('media')	.	DS	.	$data['small_image']['value'];											
				chmod($source_dir, 0777);
				unlink($source_dir);
				$data['small_image']	=	'';
			}
			elseif(isset($data['small_image'])	&&	is_array($data['small_image']))	{
				$data['small_image']	=	$data['small_image']['value'];
			
			}			
				
				if(isset($data['full_image']['delete'])	&&	$data['full_image']['delete']	==	1)	{
				$source_dir	=	Mage::getBaseDir('media')	.	DS	.	$data['full_image']['value'];											
				chmod($source_dir, 0777);
				unlink($source_dir);
				$data['full_image']	=	'';
			}
			elseif(isset($data['full_image'])	&&	is_array($data['full_image']))	{
				$data['full_image']	=	$data['full_image']['value'];
			
			}			
				
				
				if(isset($_FILES['small_image']['name']) && $_FILES['small_image']['name'] != '') {
					try {	
						$uploaderSmall = new Varien_File_Uploader('small_image');					
						$uploaderSmall->setAllowedExtensions(array('jpg','jpeg','gif','png'));
						$uploaderSmall->setAllowRenameFiles(false);
						$uploaderSmall->setFilesDispersion(false);						
						$pathSmall = Mage::getBaseDir('media') . DS . "pressImages" . DS . "small" . DS;
						
						$uploaderSmall->save($pathSmall, $_FILES['small_image']['name'] );						
						
						$data['small_image'] = 'pressImages/small/' . $_FILES['small_image']['name']; // $_FILES['small_image']['name'];						
						//echo $data['small_image'];
						//exit;
							
					} catch (Exception $e) {
						Mage::getSingleton('adminhtml/session')->setFormData($this->getRequest()->getPost());
						Mage::getSingleton('adminhtml/session')->addError(Mage::helper('press')->__('Unable to upload images.'));
						$this->_redirect('*/*/');
					}
				}
				
				
				
				if(isset($_FILES['full_image']['name']) && $_FILES['full_image']['name'] != '')
				{
					try {	
						if($data['photo_url'] == "")
						{																	
							$uploaderFull = new Varien_File_Uploader('full_image');					
							$uploaderFull->setAllowedExtensions(array('jpg','jpeg','gif','png','pdf'));
							$uploaderFull->setAllowRenameFiles(false);					
							$uploaderFull->setFilesDispersion(false);
							$pathFull = Mage::getBaseDir('media') . DS . "pressImages" . DS . "full" . DS;
							$uploaderFull->save($pathFull, $_FILES['full_image']['name']);
						
							$data['full_image'] = 'pressImages/full/' . $_FILES['full_image']['name'];
						}							
						
					} catch (Exception $e) {
						Mage::getSingleton('adminhtml/session')->setFormData($this->getRequest()->getPost());
						Mage::getSingleton('adminhtml/session')->addError(Mage::helper('press')->__('Unable to upload images.'));
						$this->_redirect('*/*/');
					}
				}
			$model = Mage::getModel('press/press');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
				
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
			
				$year = $this->getRequest()->getPost('year');
				//echo $year;
				$model->setYear($year); 
				$model->save();
				
					
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('press')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('press')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('press/press');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $pressIds = $this->getRequest()->getParam('press');
        if(!is_array($pressIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($pressIds as $pressId) {
                    $press = Mage::getModel('press/press')->load($pressId);
                    $press->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($pressIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $pressIds = $this->getRequest()->getParam('press');
        if(!is_array($pressIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($pressIds as $pressId) {
                    $press = Mage::getSingleton('press/press')
                        ->load($pressId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($pressIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'press.csv';
        $content    = $this->getLayout()->createBlock('press/adminhtml_press_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'press.xml';
        $content    = $this->getLayout()->createBlock('press/adminhtml_press_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
