<?php

class Mageshopapps_Press_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getPressUrl()
	{
		return $this->_getUrl("press");
	}
	
	public function getImageUrl(){
		return Mage::getBaseUrl('media').'pressImages';
	}
	
}
