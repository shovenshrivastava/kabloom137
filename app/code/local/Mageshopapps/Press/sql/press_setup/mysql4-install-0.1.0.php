<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('press')};
CREATE TABLE {$this->getTable('press')} (
  `press_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `small_image` varchar(255) NOT NULL default '',
  `full_image` varchar(255) NOT NULL default '',
  `photo_url` varchar(255) NOT NULL default '',
  `position` varchar(255) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  `store_view` int(11) NOT NULL,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`press_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 
