<?php
/**
 * Barcode Add-On
 *
 * @author      Matt Johnson <M@ttJohnson.com>
 */
abstract class UMA_BarcodePackingSlip_Model_Abstract extends Mage_Sales_Model_Order_Pdf_Abstract
{
    protected function convertToBarcodeString($toBarcodeString)
    {
        $str = $toBarcodeString;
        $barcode_data = str_replace(' ', chr(128), $str);

        $checksum = 104; # must include START B code 128 value (104) in checksum
        for($i=0;$i<strlen($str);$i++) {
                $code128 = '';
                if (ord($barcode_data{$i}) == 128) {
                        $code128 = 0;
                } elseif (ord($barcode_data{$i}) >= 32 && ord($barcode_data{$i}) <= 126) {
                        $code128 = ord($barcode_data{$i}) - 32;
                } elseif (ord($barcode_data{$i}) >= 126) {
                        $code128 = ord($barcode_data{$i}) - 50;
                }
        $checksum_position = $code128 * ($i + 1);
        $checksum += $checksum_position;
        }
        $check_digit_value = $checksum % 103;
        $check_digit_ascii = '';
        if ($check_digit_value <= 94) {
            $check_digit_ascii = $check_digit_value + 32;
        } elseif ($check_digit_value > 94) {
            $check_digit_ascii = $check_digit_value + 50;
        }
        $barcode_str = chr(154) . $barcode_data . chr($check_digit_ascii) . chr(156);
            
        return $barcode_str;

    }
}