<?php

class Ewall_Cartpopup_Model_System_Config_Source_Display
{
     public function toOptionArray()
    {
        return array(
            array('value'=>'yes', 'label'=>Mage::helper('cartpopup')->__('Yes')),
            array('value'=>'no', 'label'=>Mage::helper('cartpopup')->__('No'))
        );
    }}
