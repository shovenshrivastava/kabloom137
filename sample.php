<?php
require_once ('app/Mage.php');
Mage::app();
$date = $_REQUEST['date'];
$prodSku = $_REQUEST['sku'];
$warehouse = $_REQUEST['warehouse'];
// Create new PDF 
if($warehouse){
	$orders = Mage::getModel('sales/order')->getCollection()
        ->addFieldToFilter('created_at', array('like' => $date.' %') )
		->addFieldToFilter('assignation_warehouse', $warehouse)
		->addFieldToSelect('trading_partner');
} else{
	$orders = Mage::getModel('sales/order')->getCollection()
        ->addFieldToFilter('created_at', array('like' => $date.' %') )
		->addFieldToSelect('trading_partner');
}

$orders->getSelect()
		->columns('GROUP_CONCAT(entity_id) entity_ids')
		->columns('GROUP_CONCAT(increment_id) increment_ids')
        ->columns('COUNT(*) AS countOrder')
		->group('trading_partner');
		
$india_post = array();

$ordernum = array();	
$itemsku = array();

$itemname = array();
$qtyarr =  array();
$qty = 0;
$i = 0;
//$orders->printLogQuery(true);
if($_REQUEST['pickup']){
	foreach($orders as $order) :
		if($order['trading_partner'] == "")
		{
				$order['trading_partner'] = "Kabloom";
		}
		if($order['trading_partner'] == $_REQUEST['trading_partner']) 
		{
			$eSku = explode(',',$order['entity_ids']);
			foreach($eSku as $orinc) : 
				$orderData = Mage::getModel("sales/order")->load($orinc);
				if(!$prodSku){
					$india_post[] = $orderData->getIncrementId();
				}
				$ordered_items = $orderData->getAllItems();
				foreach($ordered_items as $item) : 
				
					if($item->getProductType() == 'simple'):
						if(!in_array($orderData->getIncrementId(), $ordernum) && $prodSku == $item->getSku()) {
							$ordernum[] = $orderData->getIncrementId();
						}
					endif;
				endforeach;					
			endforeach;
		}
	endforeach;
	if (!empty($ordernum)) {
		$india_post = $ordernum;
	}
	//print_r($india_post);
	
	foreach ($india_post as $value) :
	//echo $value;
		$orderdetails = Mage::getModel('sales/order')->loadByIncrementId($value);
		$ordered_items = $orderdetails->getAllItems();
		foreach($ordered_items as $item) :     
			if($item->getProductType() == 'simple') :
				if (in_array($item->getSku(), $itemsku))
				{
					$key = array_search($item->getSku(), $itemsku);
					$qty++;
					$qtyarr[$key] = $qty;
					
				}
				if (!in_array($item->getSku(), $itemsku))
				{
					$itemsku[$i] = $item->getSku();
					$itemname[$i] = $item->getName();
					$key = array_search($item->getSku(), $itemsku);
					$qty = 0;
					$qty++;
					$qtyarr[$key] = $qty;
					
				} 
			endif;
			$i++;
		endforeach;
	endforeach;
	//print_r($itemsku);
	//exit;
	$items = array_merge_recursive($itemsku, $qtyarr);
	$k = 0;
	$j = 780;
	
	$pdf = new Zend_Pdf(); 
	$page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4); 
	$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	$page->setFont($font, 10);
	$page->setFont($font, 10);
	$page->drawText("Product Sku",0 ,800 , 'UTF-8');
	
	$page->setFont($font, 10);		
	$page->drawText("Product Name", 250, 800 , "UTF-8");
	
	$page->setFont($font, 10);
	$page->drawText("Quantity", 550, 800 , "UTF-8");
	
	if (!empty($itemsku)) {
		foreach($itemsku as $skey => $sval) :
			$page->setFont($font, 10);
			$page->drawText($sval,$k ,$j , 'UTF-8');
			$page->setFont($font, 10);		
			$page->drawText($itemname[$skey], $k+ 250, $j , "UTF-8");
			$page->setFont($font, 10);
			$page->drawText($qtyarr[$skey], $k + 550, $j , "UTF-8");
			$j = $j - 20;
			$i++;	
		endforeach;
		$pdf->pages[] = $page; 
		$pdfData = $pdf->render(); 
		 
		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/x-pdf"); 
		echo $pdfData;
	} else{
		$url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer()  : Mage::getUrl();
		Mage::app()->getFrontController()->getResponse()->setRedirect($url);
		Mage::app()->getResponse()->sendResponse();
		return;
	}
} else {
	foreach($orders as $order) :
		if($order['trading_partner'] == "")
		{
				$order['trading_partner'] = "Kabloom";
		}
		if($order['trading_partner'] == $_REQUEST['trading_partner']) 
		{
			$eSku = explode(',',$order['entity_ids']);
			foreach($eSku as $orinc) : 
				$orderData = Mage::getModel("sales/order")->load($orinc);
				if(!$prodSku){
					$india_post[] = $orderData->getIncrementId();
				}
				$ordered_items = $orderData->getAllItems();
				foreach($ordered_items as $item) : 
				
					if($item->getProductType() == 'simple'):
						if(!in_array($orderData->getIncrementId(), $ordernum) && $prodSku == $item->getSku()) {
							$ordernum[] = $orderData->getIncrementId();
						}
					endif;
				endforeach;					
			endforeach;
		}
	endforeach;
	if (!empty($ordernum)) {
		$india_post = $ordernum;
	}
	//print_r($india_post);
	//exit;
	$pdf = new Zend_Pdf(); 
	$y = 0;
	
	${"page" . $y} = $pdf->newPage(Zend_Pdf_Page::SIZE_A4); 
	$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	${"page" . $y}->setFont($font, 10);

	$i = 1;
	
	$j = 800;
	$k = 0;
	
	if (!empty($india_post)) {
		foreach ($india_post as $key => $value) {
			if($i % 33 == 0){
				$y++;
				${"page" . $y} = $pdf->newPage(Zend_Pdf_Page::SIZE_A4); 
				$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
				${"page" . $y}->setFont($font, 10);
				$j = 800;
				$k = 0;
			}
			$fontPath =   Zend_Pdf_Font::fontWithPath(Mage::getBaseDir('media') . '/fonts/fre3of9x.ttf');
			${"page" . $y}->setFont($fontPath, 30);
			$barcodeImage = "*".$india_post[$key]."*";
			${"page" . $y}->drawText($barcodeImage,$k ,$j - 10 , 'UTF-8');
			${"page" . $y}->setFont($font, 10);
			${"page" . $y}->drawText($india_post[$key], $k, $j + 15, "UTF-8");
			${"page" . $y}->setFont($font, 10);
			${"page" . $y}->drawText($_REQUEST['trading_partner'], $k, $j + 30, "UTF-8");
			$k = $k + 150;
			if($i != 1 && ($i) % 4 == 0){
				$k = 0;
				$j = $j - 100;
			}
			if($i % 33 == 0 || $i == 31){
				$pdf->pages[] = ${"page" . $y};
				//echo "page" . $y;
			} 
			//echo "page" . $y;
			$i++;
		}//die;
		if($i <= 32){
			$pdf->pages[] = ${"page" . $y};
			//echo "page" . $y;
		}
		$pdfData = $pdf->render(); 
		 
		header("Content-Disposition: inline; filename=result.pdf"); 
		header("Content-type: application/x-pdf"); 
		echo $pdfData;
	} else{
		$url = Mage::helper('core/http')->getHttpReferer() ? Mage::helper('core/http')->getHttpReferer()  : Mage::getUrl();
		Mage::app()->getFrontController()->getResponse()->setRedirect($url);
		Mage::app()->getResponse()->sendResponse();
		return;
	}
}
?>