<?php
 
require_once 'app/Mage.php';
 
Mage::app();
 
$quote = Mage::getModel('sales/quote')
        ->setStoreId(Mage::app()->getStore('default')->getId());
 
$product = Mage::getModel('catalog/product')->load(33); /* 6 => Some product ID */
//echo "<pre>";print_r($product);die;
$buyInfo = array('qty' => 1);

$quote->addProduct($product, new Varien_Object($buyInfo));
 
$billingAddress = array(
    'firstname' => 'Branko',
    'lastname' => 'Ajzele',
    'company' => 'Inchoo',
    'email' =>  'branko@inchoo.net',
    'street' => array(
        'Sample Street Line_1',
        'Sample Street Line_2'
    ),
    'city' => 'City',
    'region_id' => '',
    'region' => 'State/Province',
    'postcode' => '12345',
    'country_id' => 'NL',
    'telephone' =>  '1234567890',
    'fax' => '123456987',
    'customer_password' => '',
    'confirm_password' =>  '',
    'save_in_address_book' => '0',
    'use_for_shipping' => '1',
);
$payments = Mage::getSingleton('payment/config')->getActiveMethods();
$payMethods = array();
foreach ($payments as $paymentCode=>$paymentModel) 
{
	//echo $paymentCode."<br>";
    $paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
    $payMethods[$paymentCode] = $paymentTitle;
}
echo "<pre>";
print_r($payMethods);
$methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
$shipMethods = array();
foreach ($methods as $shippigCode=>$shippingModel) 
{
	//echo $shippigCode."<br>";
    $shippingTitle = Mage::getStoreConfig('carriers/'.$shippigCode.'/title');
    $shipMethods[$shippigCode] = $shippingTitle;
}
echo "<pre>";print_r($shipMethods);
//die;
$quote->getBillingAddress()
        ->addData($billingAddress);

$quote->getShippingAddress()
        ->addData($billingAddress)
        ->setShippingMethod('tablerate_bestway')
        ->setPaymentMethod('checkmo')
        ->setCollectShippingRates(true)
        ->collectTotals();
 
$quote->setCheckoutMethod('guest')
            ->setCustomerId(null)
            ->setCustomerEmail($quote->getBillingAddress()->getEmail())
            ->setCustomerIsGuest(true)
            ->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID);
  
$quote->getPayment()->importData( array('method' => 'checkmo'));

$quote->save();
 
 
$service = Mage::getModel('sales/service_quote', $quote);

$service->submitAll();
echo "test";die;
 ?>