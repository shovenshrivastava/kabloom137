<?php
require_once('time.php');

function generateICN() { 
   return rand(100000000, 999999999);
}

function getItemResult($resultStatement) {
   switch($resultStatement)
   {
	   case "INSUFFICIENT_INVENTORY": { $Item_Result = "00" ; break;}
	   case "INVALID_SKU": { $Item_Result = "02N" ; break;}
	   default: { $Item_Result = "IA" ; break;}
   }
   return $Item_Result;
}

function generateResponse_856($order, $ICN) {
 //  print_r($order); die;
//   $service = "UPS_GR_RES";//to be removed this hard-coding.
   $service = $order->shipmentMethod;
//echo $service;die;
   $S_date = getSystemDate() ;
   $S_time = getSystemTime() ;
   //$ICN = generateICN();
   $T_count = 0;
   $Item_Result = "00" ;
   $Iden_number = 214 ;
   $BSN_06 = "AS" ;
   $BSN_07 = "NOR " ;
   $HL_count = 1 ;
   $Item_ship_total = 0 ;

   $edi = "";
   $edi = $edi . "ISA*00*          *00*          *ZZ*KABLOOM        *ZZ*AMAZONDS       *$S_date*$S_time*U*00401*$ICN*0*T*>\n";
   $edi = $edi . "GS*SH*KABLOOM*AMAZONDS*20$S_date*$S_time*42*X*004010\n";
   $edi = $edi . "ST*856*000000003\n";
 
	if ($BSN_06 == "72")
	{
	   $BSN_07 = "REJ" ;
    }

   $edi = $edi . "BSN*00*$Iden_number*20$S_date*$S_time*ZZZZ*$BSN_06*$BSN_07\n" ;
   $edi = $edi . "HL*1**O\n" ;
   $edi = $edi . "PRF*$order->partnerOrderId***20$S_date**$order->orderId\n" ;
   $edi = $edi . "N1*SF*$order->wareHouse*92*$order->wareHouse\n";
   
   $products = $order->products;
   $i = 0;
   $HL_count = 1 ;
   $package = $order->package;
   $Item_ship_total = 0;
   
   foreach($products as $product)
   { 
      $i++ ;
	  $HL_count++ ;	
	  $Item_Result = getItemResult($product->itemResult);
      $edi = $edi . "HL*$HL_count**I\n" ;
      $edi = $edi . "LIN*$i*SK*$product->sku\n" ;
	  //$edi = $edi . "SN1*$i*$product->shipped*EA**$product->quantity*EA**$Item_Result\n" ;
	  $edi = $edi . "SN1*$i*1*EA**1*EA**$Item_Result\n" ;
	  //$edi = $edi . "MAN*R*$package->packageId\n" ;	
	  $edi = $edi . "MAN*R*1\n" ;	
	  $HL_count++ ;	
	  $edi = $edi . "HL*$HL_count**P\n" ;
	  $edi = $edi . "TD1******Z*$package->weight*LB\n" ;
	  //$edi = $edi . "TD5**92*$package->shipmentCode\n" ;//UPS_RES
	  $edi = $edi . "TD5**92*$service\n" ;//UPS_RES
	  //$edi = $edi . "MAN*R*$package->packageId*$package->manifest*$package->carrierQual*$package->trackingNumber\n" ; 
	  $edi = $edi . "MAN*R*1*2*3*$package->trackingNumber\n" ;
	  $Item_ship_total = $Item_ship_total + $product->shipped ;
	  $T_count = $i * 8 + 9;
	}
	
    $edi = $edi . "DTM*ZZZ*20$S_date\n" ;
    $edi = $edi . "DTM*011*20$S_date\n" ;
    $edi = $edi . "CTT*$product->shipped*$HL_count\n";
    $edi = $edi . "SE*$T_count*000000003\n";
    $edi = $edi . "GE*1*42\n";
    $edi = $edi . "IEA*1*$ICN\n";
  
    //write_log('Info: generateTracking.php . $edi');
  // echo $edi; 
    return $edi;
}
?>
