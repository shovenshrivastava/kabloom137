<?php
require_once 'entities/Product.php';
require_once('time.php');

function getProductInventory($warehouse,$partner_name) {
     
	 require_once '../app/Mage.php';
     umask(0);
     Mage::app('default');
     
     switch($warehouse){
	 case "AFRH": $warehouse_id = 3; break;
	 case "AEXB": $warehouse_id = 5; break;
	 case "AEJM": $warehouse_id = 1; break;
	 case "AMCZ": $warehouse_id = 7; break;
	 case "AMDA": $warehouse_id = 8; break;
	 case "AMDB": $warehouse_id = 9; break;
	 case "AMDC": $warehouse_id = 10; break;
	 case "AQGJ": $warehouse_id = 2; break;
	 default: $warehouse_id = 3;
     
   }
    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');
    $productsCollection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('sku')
	->addAttributeToFilter(array(
        array('attribute'=>'type_id', 'eq'=>'simple'),
        array('attribute'=>'manufacturer', 'eq'=>'amazon'),
		));
			
                            
	     $products = array();
         $i = 0;
		foreach($productsCollection as $product) {
			$_product = Mage::getSingleton('catalog/product')->load($product->getEntityId());
			$_manufacturerId = $_product->getManufacturer();
			if($_manufacturerId==785){			
			if($product->getTypeId()=='simple'){	
				//echo $_manufacturerId;
												   
			 $query_qty_select_warehouse = "select `quantity_in_stock` from `advancedinventory_stock` where product_id =" . $product->getEntityId() . " and place_id =" . $warehouse_id;
		      $existing_qty_warehouse = $readConnection->fetchAll($query_qty_select_warehouse);
		
		      //if($existing_qty_warehouse[0]['quantity_in_stock']>0){
					$i++;
					$newproduct = new Product();        
					$newproduct->id = $product->getEntityId();
					$newproduct->price = $_product->getPrice();
					$newproduct->sku =  $product->getSku();//$product->getSku(); 
					if($warehouse_id==2){
						$newproduct->quantity = 0;//$existing_qty_warehouse[0]['quantity_in_stock'];
						}else{
					$newproduct->quantity = $existing_qty_warehouse[0]['quantity_in_stock'];
				}
					$products[$i] = $newproduct;	
					
					//print_r($products); die;
			
					//} 
				}
			}
		}
    
    return $products;
}

function generateResponse($warehouse,$partner_name, $ICN)
{
   $S_date = getSystemDate() ;
   $S_time = getSystemTime() ;
   $edi = "";
   $edi = $edi . "ISA*00*          *00*          *ZZ*KABLOOM        *ZZ*AMAZONDS       *$S_date*$S_time*U*00401*$ICN*0*P*>\n";
   $edi = $edi . "GS*IB*KABLOOM*AMAZONDS*20$S_date*$S_time*42*X*004010\n";
   $edi = $edi . "ST*846*000000003\n";
   $edi = $edi . "BIA*00*DD*45*20$S_date\n";
   $edi = $edi . "DTM*166*20$S_date**GM\n";
   $edi = $edi . "N1*ZZ*$warehouse*92*$warehouse\n";
   $productInventoryList = getProductInventory($warehouse,$partner_name);
   $i = 0;
   foreach ($productInventoryList as $productInventory) {
      $i++;
      $sku = $productInventory->sku;
      $edi = $edi . "LIN*$i*SK*$sku\n";
      $qty = $productInventory->quantity;
	  if ($qty == '')
	  {
		  $qty = 0;
	  }
      $edi = $edi . "SDQ*EA*92*$warehouse*$qty\n";
  }
  $numberOfSegments = 2 * $i + 6;
  $edi = $edi . "CTT*$i*0\n";
  $edi = $edi . "SE*$numberOfSegments*000000003\n";
  $edi = $edi . "GE*1*42\n";
  $edi = $edi . "IEA*1*$ICN\n";
  echo $edi;
 return $edi;
}
?>
