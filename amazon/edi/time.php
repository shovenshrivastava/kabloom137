<?php

function getTime() {
   date_default_timezone_set('Asia/Kolkata');
   return time();
}

function getSystemTime() {
   return date('Hi', getTime());
}

function getSystemDate(){
   return date('ymd', getTime());
}

?>