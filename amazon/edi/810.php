<?php
require_once('time.php');

/*
function generateICN() { 
   return rand(100000000, 999999999);
}
*/

function generateResponse_810($order, $ICN) {
//print_r($order);
   $S_date = getSystemDate() ;
   $S_time = getSystemTime() ;
   $T_count = 3 ;
   $Total_unit = 0;
   $Invoice_sum = 0 ;
   $Item_ship_total = 0 ;
   $invoice = $order->invoice;
   $Invoice_number = $invoice->invoiceNumber;
   $address = $order->billingAddress;
  // print_r($order); die;
   //$street = $address->street[0];
   //echo $street;
   if($address->region=='Washington') $address->region='WA';
   $edi = "";
   $edi = $edi . "ISA*00*          *00*          *ZZ*KABLOOM        *ZZ*AMAZONDS       *$S_date*$S_time*U*00401*$ICN*0*T*>\n";
   $edi = $edi . "GS*IN*KABLOOM*AMAZONDS*20$S_date*$S_time*42*X*004010\n";
   $edi = $edi . "ST*810*000000003\n" ;
   $edi = $edi . "BIG*20$S_date*$Invoice_number*20$S_date****CI\n" ;
   $edi = $edi . "CUR*BT*USD\n" ;
   $edi = $edi . "N1*RI*Boonstle LLC*92*KABLQ\n" ;
   $edi = $edi . "N3*305 HARVARD ST\n" ;
   $edi = $edi . "N4*BROOKLINE*MA*02446*$address->countryId\n" ; //stateCode has been converted to region, Akshay please verify  
   $edi = $edi . "N1*SF*$order->warehouse*92*$order->warehouse\n";
   $edi = $edi . "ITD*01*3*****30*****60 NET\n" ;
   
   $products = $order->products;
   $i = 0;
   foreach($products as $product)
   {  
       $i++ ;	    
	   $shipped = round($product->shipped);
	   $price = round($product->price, 2);
	   $edi = $edi . "IT1*$i*$shipped*EA*$price*NT*SK*$product->sku*PO*$order->partnerOrderId*VO*$order->orderId*ON*$order->customerOrderNumber\n" ;	
	   $Total_unit = $Total_unit + $product->shipped ;
	   $Invoice_sum = $Invoice_sum + ($product->shipped*$product->price) ;
	   $T_count = $i + 11;
   }
   $Invoice_sum = $Invoice_sum * 100;
   $edi = $edi . "TDS*$Invoice_sum\n" ;	
   $edi = $edi . "CTT*$i*$Total_unit\n";
   $edi = $edi . "SE*$T_count*000000003\n";
   $edi = $edi . "GE*1*42\n";
   $edi = $edi . "IEA*1*$ICN\n";
  
  //write_log('Info: generateTracking.php . $edi');
  //  echo $edi; die;
  return $edi; 
}
?>
