<?php
require_once('entities/Address.php');
require_once('entities/Product.php');
//require_once('../essentials/log.php');

function getOrderDate($xml) 
{
	echo 'Info: process.php,  Entering getShipmentService';
    $dom = new DOMDocument;
    $dom->loadXML($xml);
    $elements = $dom->getElementsByTagName('element');
	foreach($elements as $element)
	{  	  
	   $elementId = $element->getAttribute('Id');	   
	   if($elementId == "BEG05")
	   {	     
		     $value =  $element->nodeValue;
             $arr = array(substr($value, 0, 4),substr($value, 4, 2),substr($value, 6, 2));
             return implode("-",$arr);			 
	   }
     }
	 return "";
}

function getShipmentService($xml)
{
	echo 'Info: process.php,  Entering getShipmentService';
    $dom = new DOMDocument;
    $dom->loadXML($xml);
    $elements = $dom->getElementsByTagName('element');
	foreach($elements as $element)
	{  	  
	   $elementId = $element->getAttribute('Id');	   
	   if($elementId == "TD503")
	   {	     
		     return  $element->nodeValue;         
	   }
     }
	 return "";
}

//added by nareshseeta
function getShipmentServiceType($xml)
{
	echo 'Info: process.php,  Entering getShipmentService';
    $dom = new DOMDocument;
    $dom->loadXML($xml);
    $elements = $dom->getElementsByTagName('element');
	foreach($elements as $element)
	{  	  
	   $elementId = $element->getAttribute('Id');	   
	   if($elementId == "TD508")
	   {	     
		     return  $element->nodeValue;         
	   }
     }
	 return "";
}
function getGiftMessage($xml)
{
    //write_log('Info: process.php,  Entering getGiftMessage');
	echo 'Info: process.php,  Entering getGiftMessage';
    $dom = new DOMDocument;
    $dom->loadXML($xml);
    $loops = $dom->getElementsByTagName('loop');
    $i = 0;
	$found == false;
	foreach($loops as $loop)
	{  	   
	   $loopId = $loop->getAttribute('Id');	  	   
	   if($loopId=="PO1")
	   {  
	      $elements = $loop->getElementsByTagName('element');
		  foreach($elements as $element){
		     $elementId = $element->getAttribute('Id');
			 if($elementId == "MSG01"){
			   if($found == false) {
			       $found = true;
				}
				else {
				   return $element->nodeValue;
				}
			 }
		  }
	   }
     }
     return "";
}

function getAddress($xml, $type)
{
   //write_log('Info: process.php, Entering getAddress xml:'.$xml);
    //echo 'Info: process.php, Entering getAddress xml:'.$xml;
    $dom = new DOMDocument;
    $dom->loadXML($xml);
    $loops = $dom->getElementsByTagName('loop');
	$address = new Address();
	foreach ($loops as $loop) 
	{
      $elements = $loop->getElementsByTagName('element');
      foreach ($elements as $element) 
      {
        $value = $element->nodeValue;
		$id = $element->getAttribute('Id');
		if($id=='N101' && $value!=$type) break; 
        switch($id)
        {
         // case('N101'): {$address->firstname = $value; break;}
          case('N102'): {echo "......2........";$address->firstname = $value; break;}
          case('N301'): {$address->streetLine1 = $value; break;}
          case('N302'):	{$address->streetLine2 = $value; break;}
          case('N401'): {$address->city = $value; break;}
          case('N402'): {$address->region = $value; break;}
          case('N403'): {$address->postcode = $value; break;}
          case('N404'): {$address->countryId = $value; break;}
          case('PER06'): {$address->telephone = $value; break;}
       }
      }
   }
   //write_log('Info: process.php,  Exiting getAddress address:');
   print_r($address);
   return $address;
}


function getPartnerOrderId($xml)
{
    //write_log('Info: process.php,  Entering getPartnerOrderId');
    $dom = new DOMDocument;
    $dom->loadXML($xml);
    $elements = $dom->getElementsByTagName('element');
	foreach($elements as $element)
	{  	   
	   $elementId = $element->getAttribute('Id');	   
	   if($elementId == "BEG03")
	   {
		     $value = $element->nodeValue;
             return $value;   
	   }
     }
     return "";
}

function getCustomerOrderNumber($xml)
{
    $dom = new DOMDocument;
    $dom->loadXML($xml);
    $elements = $dom->getElementsByTagName('element');
	$found= false;
	foreach($elements as $element)
	{  	   	    
		$value = $element->nodeValue;
	    if($found==true) {
		   return $value;
		}
	    if($value=="OQ") {		  
           $found = true; 
		}   
    }
     return "";
}


function getWarehouse($xml)
{
    //write_log('Info: process.php,  Entering getWarehouse');
    $dom = new DOMDocument;
    $dom->loadXML($xml);
    $loops = $dom->getElementsByTagName('loop');
    $i = 0;
	foreach($loops as $loop)
	{  
	   $loopId = $loop->getAttribute('Id');
	   if($loopId == "N1")
	   {
	      $i++;
	      if($i == 2) 
	      {
	           $elements = $loop->getElementsByTagName('element');
               foreach ($elements as $element) 
               {    
                    $value = $element->nodeValue;
                    if($element->getAttribute('Id') == "N102")
                    {
                      return $value;
                   }            
               } 
	       }
	   }
     }
     return "";
}

function getProducts($xml)
{
    //write_log('Info: process.php,  Entering getProducts');
	require_once '../app/Mage.php';
    umask(0);
    Mage::app('default');
    $dom = new DOMDocument;
    $dom->loadXML($xml);
    $segments = $dom->getElementsByTagName('segment');
    $products = array();
    $count = 0;
    foreach($segments as $segment)
    {
      if($segment->getAttribute('Id') == 'PO1')
      {  
         $elements = $segment->getElementsByTagName('element');
         $product = new Product();
         foreach ($elements as $element) 
         {
            $value = $element->nodeValue;
            switch($element->getAttribute('Id'))
            {
               case('PO102'): { $product->quantity = $value; break;}
               case('PO107'): { 
			          $product->sku = $value;  
					  $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $product->sku);
	                  if($_product==null) $_productasin = Mage::getModel('catalog/product')->loadByAttribute('asin', $product->sku);
					  if($_product==null && $_productasin!=null) $product->sku =  $_productasin->getSku();
					  break;
				 }
			   case('PO104'): { $product->price = $value; break;}
            }
         }
      $products[$count++] = $product;
      }
    }
   //write_log('Info: process.php,  Exiting getProducts');
   return $products;
}
?>
