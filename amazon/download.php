<?php
include '../app/Mage.php';
umask(0);
Mage::app('default');
$fromDate = date('Y-m-d H:i:s', strtotime($_GET['from_date']));
$toDate = date('Y-m-d H:i:s', strtotime($_GET['to_date']));
$tradingPartner = $_GET['tradingPartner'];
echo $fromdate;

$orders = Mage::getModel('sales/order')->getCollection()
	->addAttributeToSelect('entity_id')
	->addAttributeToSelect('partner_order_id')
    ->addAttributeToFilter('main_table.trading_partner', $tradingPartner)
    ->addAttributeToFilter('main_table.created_at', array('from'=>$fromDate, 'to'=>$toDate));
$orders->getSelect()->join('sales_flat_shipment_track', 'main_table.entity_id = sales_flat_shipment_track.order_id', array('track_number'));
//echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//echo $orders->getSelect();die;
foreach($orders->getData() as $key=>$value)
{
	//if($value['trading_partner']=='amazon') {
	$fin[$key]['partner_order_id'] = $value['partner_order_id'];
	$fin[$key]['action'] = 'ship';
	$fin[$key]['track_number'] = $value['track_number'];
	$fin[$key]['package_weight'] = 3;
	$fin[$key]['manifest_id'] = '';
}

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=tracking_number.csv');
$output = fopen('php://output', 'w');
fputcsv($output, array('Order Id','action', 'Tracking ID','Package Weight','Manifest ID'));
$delimiter=",";
 foreach ($fin as $k=>$v) {
        fputcsv($output, $v, $delimiter);
}

?>
