<?php
require_once('entities/Address.php');
require_once('entities/Pickup.php');
require_once('entities/Order.php');
require_once('constants.php');
require_once('checkInventory.php');
ini_set('default_socket_timeout', default_socket_timeout);

function getCountryId($countryName) {
    $countryId = '';
    $countryCollection = Mage::getModel('directory/country')->getCollection();
    foreach ($countryCollection as $country) {
        if ($countryName == $country->getName()) {
            $countryId = $country->getCountryId();
            break;
        }
    }
    $countryCollection = null;
    return $countryId;
}

function getProductId($sku) {
    $id = Mage::getModel("catalog/product")->getIdBySku($sku);
    return $id;
}

function getProduct($sku) {
    $product = Mage::getModel("catalog/product")->getBySku( $sku );
    return $product;
}
  
function getStockQty($sku) {
    $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
    $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product);
    return $stock->getQty();
} 
 
function save_shipping_date($order, $pincode, $deliveryDate, $assignation_data, $shippingAmount) 
{    
     $trading_part =$order->getTradingPartner();
     $order_id = $order->getId(); 
	     $delivery_date_marketplace = '2015-02-12 15:17:38';//$deliveryDate;
	     $delivery_date = '2015-02-16';//substr($deliveryDate, 0 ,10);
	 $collection = Mage::getModel('pointofsale/pointofsale')->getCollection();
     foreach ($collection->getData() as $data) 
	 {
            $warehouse[$data['postal_code']] = $data['inventory_assignation_rules'];
            $x = array_search($pincode, explode(',', $data['inventory_assignation_rules']));
            if ($x !== false) {
		
                $final_zip = $data['postal_code'];
            }
            if (empty($final_zip)) {
               if($pincode==$data['postal_code']){
		
                    $final_zip = $data['postal_code'];
                }
	
            }
            $ware_zip[] = $data['postal_code'];
        }
        if ($final_zip) {
            $warehouse_name = Mage::getModel('pointofsale/pointofsale')->getCollection()
                    ->addFieldToSelect('place_id')
                    ->addFieldToFilter('postal_code', $final_zip);
            $ware_data = $warehouse_name->getData();
            $warehouse_id = $ware_data[0]['place_id'];
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
           $pickup_method = "'Local Delivery'";
           $delivery_date_nirmesh = "'$delivery_date_nirmesh'";
           $data = Mage::getModel('pointofsale/pointofsale')->getCollection()->addFieldToSelect('place_id')->getData();
            foreach ($data as $k => $v) {
                if ($v['place_id'] == $warehouse_id)
                    $wh[$v['place_id']] = 1;
                else
                    $wh[$v['place_id']] = 0;
            }
            $ordered_items = $order->getAllItems();
            foreach ($ordered_items as $item) {
                if ($item->getProductType() == 'simple')
                    $prepare[$item->getProductId()] = $wh;
            }
            $assignation_data = Mage::helper('core')->jsonEncode($prepare);
            $query = "UPDATE sales_flat_order SET assignation_stock ='" . $assignation_data . "', assignation_warehouse = " . $warehouse_id . ',pickup_date = "' . $deliveryDate . '",pickup_method = ' . $pickup_method . " WHERE entity_id = " . $order_id;
            $writeConnection->query($query);
		    $query1 = "UPDATE sales_flat_order_grid SET assignation_stock ='" . $assignation_data . "', assignation_warehouse = '" . $warehouse_id ."', trading_partner = '". $trading_part . "' WHERE entity_id = " . $order_id;
		   $writeConnection->query($query1);
            return;
        } else {
		
            // this is useful for anwar
            if (empty($final_zip)) {
                //if (1) {
                $distances = '';
                foreach ($ware_zip as $k => $v) {
                    $distances[$v] = getDistance($pincode, $v);
                }
                $index = array_search(min($distances), $distances);
                $transit_final_zip = $index;
				
                $warehouse_name = Mage::getModel('pointofsale/pointofsale')->getCollection()
                    ->addFieldToSelect('place_id')
                    ->addFieldToFilter('postal_code', $transit_final_zip);
            //print_r($warehouse_name->getData());
            $ware_data = $warehouse_name->getData();
            $warehouse_id = $ware_data[0]['place_id'];
            }
        }

	/* nirmesh assigning items to warehouse*/
        $data = Mage::getModel('pointofsale/pointofsale')->getCollection()->addFieldToSelect('place_id')->getData();
        foreach ($data as $k => $v) {
            if ($v['place_id'] == $warehouse_id)
                $wh[$v['place_id']] = 1;
            else
                $wh[$v['place_id']] = 0;
        }
        $ordered_items = $order->getAllItems();
        foreach ($ordered_items as $item) {
            if ($item->getProductType() == 'simple')
                $prepare[$item->getProductId()] = $wh;
        }
        $assignation_data = Mage::helper('core')->jsonEncode($prepare);
		//$delivery_date = f(date);
        $tmp_array = explode("-", $delivery_date);
        $tmp_delivery_date = array();
        $tmp_delivery_date['year'] = $tmp_array[0];
        $tmp_delivery_date['month'] = $tmp_array[1];
        $tmp_delivery_date['day'] = $tmp_array[2];
        $delivery_date_day = $tmp_delivery_date['day'];
        // predict the tmp_pickup_date, if the delivery date is away from 2 days - u should subtract 2 days, if the delivery date is away from 1 day - u should subtract 1 day,if the delivery date is today  then you can make delivery date itself as tmp_pickup_date bcoz in bcoz the timeintransit api will consider pickup date as today itself automatically
        $currentdate = strtotime(date("Y-m-d"));
        $date = strtotime($delivery_date);
        $datediff = $date - $currentdate;
        $differance = floor($datediff / (60 * 60 * 24));
        if ($differance == 0) {
            $tmp_pickup_date = str_replace("-", "", $delivery_date);
        } elseif ($differance == 1) {
            $tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 1 days')));
        } elseif ($differance > 1) {
            $tmp_pickup_date = str_replace("-", "", date('Y-m-d', strtotime($delivery_date . ' - 2 days')));
        } else {
            $tmp_pickup_date = str_replace("-", "", $delivery_date);
        }
		
        $tmp_pickup_datetime['year'] = substr($tmp_pickup_date, 0, 4);
        $tmp_pickup_datetime['month'] = substr($tmp_pickup_date, 4, 2);
        $tmp_pickup_datetime['day'] = substr($tmp_pickup_date, 6, 2);
        $tmp_pickup_datetime['hrs'] = '23'; // assumption as customer is not entering delivery time details
        $tmp_pickup_datetime['mins'] = '59'; // assumption as customer is not entering delivery time details
        $tmp_pickup_datetime['secs'] = '59'; // assumption as customer is not entering delivery time details
        $address = $order->getShippingAddress();
        //print_r(get_class_methods($address)); die;
        //$final_zip = getnearestwarehouse();//nirmesh

        $postCode_from = $transit_final_zip; // Shipper location details
        if(empty($postCode_from)) 
         $postCode_from = '08003'; // This is main warehouse 208 springdale road cherry hill,New Jersey
        
        $country_from = 'US'; // Shipper location details
        $postCode_to = $address->getPostcode(); // delivery location details
        //$postCode_to = '55447'; // temporary delivery location details delete this line, and uncomment above line
        $country_to = $address->getCountry(); // delivery location details
        $shipping_amount = $shippingAmount; //$order->getShippingAmount();
        // 2 days = 48 hours = 172800 seconds
		$timeTransit_response = timeInTransit($tmp_pickup_date, $postCode_to, $country_to, $postCode_from, $country_from, $delivery_date_day, $shipping_amount); //die;
        $is_shortlisted_service_method = false;
      //  if (isset($shipping_amount)) { // if condition for free shipping
            $ups_end_method = end($timeTransit_response);
            if ($ups_end_method->Service->Code == "GND") {// first check with ground method with pickup date,delvery time diff as <=48 hrs
                if ($ups_end_method->EstimatedArrival->BusinessDaysInTransit <= 2) {
                    //check if transit days is <= 48 hrs
                    $gnd_arrival_datetime = array();
                    $gnd_pickup_datetime_scheduled = array();

                    $gnd_pickup_date = $ups_end_method->EstimatedArrival->Pickup->Date;
                    $gnd_pickup_time = $ups_end_method->EstimatedArrival->Pickup->Time;
                    $gnd_pickup_datetime_scheduled['year'] = substr($gnd_pickup_date, 0, 4);
                    $gnd_pickup_datetime_scheduled['month'] = substr($gnd_pickup_date, 4, 2);
                    $gnd_pickup_datetime_scheduled['day'] = substr($gnd_pickup_date, 6, 2);
                    $gnd_pickup_datetime_scheduled['hrs'] = substr($gnd_pickup_time, 0, 2);
                    $gnd_pickup_datetime_scheduled['mins'] = substr($gnd_pickup_time, 2, 2);
                    $gnd_pickup_datetime_scheduled['secs'] = substr($gnd_pickup_time, 4, 2);

                    $gnd_arrival_date = $ups_end_method->EstimatedArrival->Arrival->Date;
                    $gnd_arrival_time = $ups_end_method->EstimatedArrival->Arrival->Time;
                    $gnd_arrival_datetime['year'] = substr($gnd_arrival_date, 0, 4);
                    $gnd_arrival_datetime['month'] = substr($gnd_arrival_date, 4, 2);
                    $gnd_arrival_datetime['day'] = substr($gnd_arrival_date, 6, 2);
                    $gnd_arrival_datetime['hrs'] = substr($gnd_arrival_time, 0, 2);
                    $gnd_arrival_datetime['mins'] = substr($gnd_arrival_time, 2, 2);
                    $gnd_arrival_datetime['secs'] = substr($gnd_arrival_time, 4, 2);
                    $gnd_time_diff = strtotime($gnd_arrival_datetime['year'] . '-' . $gnd_arrival_datetime['month'] . '-' . $gnd_arrival_datetime['day'] . ' ' . $gnd_arrival_datetime['hrs'] . ':' . $gnd_arrival_datetime['mins'] . ':' . $gnd_arrival_datetime['secs']) - strtotime($tmp_pickup_datetime['year'] . '-' . $tmp_pickup_datetime['month'] . '-' . $tmp_pickup_datetime['day'] . ' ' . $tmp_pickup_datetime['hrs'] . ':' . $tmp_pickup_datetime['mins'] . ':' . $tmp_pickup_datetime['secs']); // time diff bw tmp pickup and ups grnd delivery
                    //print_r($gnd_arrival_datetime);die;

                    if ($gnd_time_diff < 172800) {
                        if ($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer delivery date
                            $shortlisted_ups_service_code = $ups_end_method->Service->Code;
                            $shortlisted_ups_service_desc = $ups_end_method->Service->Description;
                            $shortlisted_ups_service_pickup_datetime = $gnd_pickup_datetime_scheduled;

                            array_pop($timeTransit_response); // remove the last element from array which is ground
                            $is_shortlisted_service_method = true;
                        } // if($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']){	END					
                    } // if($gnd_time_diff < 172800){ END
                } // if($ups_end_method->EstimatedArrival->BusinessDaysInTransit <= 2){ END 
            } // if($ups_end_method->Service->Code == "GND"){ END
      
            if (!$is_shortlisted_service_method) { // execute foreach if service is not shortlisted
                $tmp_time_diff = 0;
                foreach ($timeTransit_response as $each_timeTransit_response) {
                    if ($each_timeTransit_response->EstimatedArrival->BusinessDaysInTransit <= 2) {
                        //check if transit days is <= 48 hrs
                        $air_arrival_datetime = array();
                        $air_pickup_datetime_scheduled = array();

                        $air_pickup_date = $each_timeTransit_response->EstimatedArrival->Pickup->Date;
                        $air_pickup_time = $each_timeTransit_response->EstimatedArrival->Pickup->Time;
                        $air_pickup_datetime_scheduled['year'] = substr($air_pickup_date, 0, 4);
                        $air_pickup_datetime_scheduled['month'] = substr($air_pickup_date, 4, 2);
                        $air_pickup_datetime_scheduled['day'] = substr($air_pickup_date, 6, 2);
                        $air_pickup_datetime_scheduled['hrs'] = substr($air_pickup_time, 0, 2);
                        $air_pickup_datetime_scheduled['mins'] = substr($air_pickup_time, 2, 2);
                        $air_pickup_datetime_scheduled['secs'] = substr($air_pickup_time, 4, 2);

                        $air_arrival_date = $each_timeTransit_response->EstimatedArrival->Arrival->Date;
                        $air_arrival_time = $each_timeTransit_response->EstimatedArrival->Arrival->Time;
                        $air_arrival_datetime['year'] = substr($air_arrival_date, 0, 4);
                        $air_arrival_datetime['month'] = substr($air_arrival_date, 4, 2);
                        $air_arrival_datetime['day'] = substr($air_arrival_date, 6, 2);
                        $air_arrival_datetime['hrs'] = substr($air_arrival_time, 0, 2);
                        $air_arrival_datetime['mins'] = substr($air_arrival_time, 2, 2);
                        $air_arrival_datetime['secs'] = substr($air_arrival_time, 4, 2);

                        $air_time_diff = strtotime($air_arrival_datetime['year'] . '-' . $air_arrival_datetime['month'] . '-' . $air_arrival_datetime['day'] . ' ' . $air_arrival_datetime['hrs'] . ':' . $air_arrival_datetime['mins'] . ':' . $air_arrival_datetime['secs']) - strtotime($tmp_pickup_datetime['year'] . '-' . $tmp_pickup_datetime['month'] . '-' . $tmp_pickup_datetime['day'] . ' ' . $tmp_pickup_datetime['hrs'] . ':' . $tmp_pickup_datetime['mins'] . ':' . $tmp_pickup_datetime['secs']); // time diff bw tmp pickup and ups air delivery
   
                        if ($air_time_diff < 172800) {
                            if ($tmp_delivery_date['day'] == $air_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer delivery date
                                //print_r($air_arrival_datetime);die;
                                if ($air_time_diff > $tmp_time_diff) {
                                    $tmp_time_diff = $air_time_diff;

                                    $shortlisted_ups_service_code = $each_timeTransit_response->Service->Code;
                                    $shortlisted_ups_service_desc = $each_timeTransit_response->Service->Description;
                                    $shortlisted_ups_service_pickup_datetime = $air_pickup_datetime_scheduled;

                                    $is_shortlisted_service_method = true;
                                } // if($air_time_diff > $tmp_time_diff){ END
                            } // if($tmp_delivery_date['day'] == $gnd_arrival_datetime['day']){	END
                        } // if($air_time_diff < 172800){ END
                    } // if($each_timeTransit_response->EstimatedArrival->BusinessDaysInTransit <= 2){ END
                } // foreach($timeTransit_response as $each_timeTransit_response){ END
            } // if(!$is_shortlisted_service_method){ END
     //   } // if($shipping_amount == 0){ END
    /*    else { // else condition will work only when no shipping method is selected which is the worst case
            $shortlisted_ups_service_code = ''; //$each_timeTransit_response->Service->Code; 
            $shortlisted_ups_service_desc = ''; //$each_timeTransit_response->Service->Description;
            $shortlisted_ups_service_pickup_datetime = '0000-00-00 00:00:00'; //$air_pickup_datetime_scheduled;
        }

*/
		

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        //$table = 'sales_flat_order_grid';   
        $pickupDate = $shortlisted_ups_service_pickup_datetime['year'] . '-' . $shortlisted_ups_service_pickup_datetime['month'] . '-' . $shortlisted_ups_service_pickup_datetime['day'] . ' ' . $shortlisted_ups_service_pickup_datetime['hrs'] . ':' . $shortlisted_ups_service_pickup_datetime['mins'] . ':' . $shortlisted_ups_service_pickup_datetime['secs']; // '2015-02-23 11:11:11' format

         $query = "UPDATE sales_flat_order SET assignation_stock ='" . $assignation_data . "', assignation_warehouse = " . $warehouse_id . ",pickup_date = '" . $pickupDate . "',pickup_method = '" . $shortlisted_ups_service_desc . "' WHERE entity_id = " . $order_id;
        $writeConnection->query($query);
		 $query1 = "UPDATE sales_flat_order_grid SET assignation_stock ='" . $assignation_data . "', assignation_warehouse = '" . $warehouse_id ."', trading_partner = '". $trading_part . "' WHERE entity_id = " . $order_id;
        $writeConnection->query($query1);
    }

  
  
    function timeInTransit($tmp_pickup_date, $postCode_to, $country_to, $postCode_from, $country_from, $delivery_date_day, $shipping_amount) {
        //Configuration
        $access = Mage::getStoreConfig('upslabel/credentials/accesslicensenumber'); //"8CCFD3D445A6F805";
        $userid = Mage::getStoreConfig('upslabel/credentials/userid'); //"newKaBloom";
        $passwd = Mage::getStoreConfig('upslabel/credentials/password'); //"Rosegarden123";
        $wsdl = Mage::getModuleDir('etc', 'Mage_Usa') . DS . 'wsdl' . DS . 'Ups' . DS . 'TNTWS.wsdl';
        $operation = "ProcessTimeInTransit";
        $endpointurl = 'https://wwwcie.ups.com/webservices/TimeInTransit';
        $outputFileName = "XOLTResult.xml";
        try {

            $mode = array
                (
                'soap_version' => 'SOAP_1_1', // use soap 1.1 client
                'trace' => 1
            );

            // initialize soap client
            $client = new SoapClient($wsdl, $mode);

            //set endpoint url
            $client->__setLocation($endpointurl);
            //create soap header
            $usernameToken['Username'] = $userid;
            $usernameToken['Password'] = $passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;

            $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 'UPSSecurity', $upss);
            $client->__setSoapHeaders($header);

            //create soap request
            $requestoption['RequestOption'] = 'TNT';
            $request['Request'] = $requestoption;

            $addressFrom['City'] = '';
            $addressFrom['CountryCode'] = $country_from; //'US';
            $addressFrom['PostalCode'] = $postCode_from; //'02446';
            $addressFrom['StateProvinceCode'] = '';
            $shipFrom['Address'] = $addressFrom;
            $request['ShipFrom'] = $shipFrom;
            $addressTo['City'] = '';
            $addressTo['CountryCode'] = $country_to; //'US';
            $addressTo['PostalCode'] = $postCode_to; //'55447';
            $addressTo['StateProvinceCode'] = '';
            $shipTo['Address'] = $addressTo;
            $request['ShipTo'] = $shipTo;

            $pickup['Date'] = $tmp_pickup_date; //'20150205'; //Ymd format
            $request['Pickup'] = $pickup;
            $unitOfMeasurement['Code'] = 'KGS';
            $unitOfMeasurement['Description'] = 'Kilograms';
            $shipmentWeight['UnitOfMeasurement'] = $unitOfMeasurement;
            $shipmentWeight['Weight'] = '2';
            $request['ShipmentWeight'] = $shipmentWeight;

            $request['TotalPackagesInShipment'] = '1';
            $invoiceLineTotal['CurrencyCode'] = 'CAD';
            $invoiceLineTotal['MonetaryValue'] = '10';
            $request['InvoiceLineTotal'] = $invoiceLineTotal;
            $request['MaximumListSize'] = '1';

            $resp = $client->__soapCall($operation, array($request));

            $fw = fopen($outputFileName, 'w');
            //var_dump(get_object_vars($resp));
            $tmp_array = $resp->TransitResponse->ServiceSummary; //die;

            if (isset($shipping_amount)) {
			
                $tocheck_shortest_routes = $tmp_array;
                // can't explain, test b/w 02446 - 02482 u will understand to foreach loop
                $not_found = false;
                $not_found_count = 0;
                foreach ($tocheck_shortest_routes as $each_timeTransit_response) {
                    $service_arrival_date = $each_timeTransit_response->EstimatedArrival->Arrival->Date;
                    $air_arrival_datetime['day'] = substr($service_arrival_date, 6, 2);
                    if ($delivery_date_day == $air_arrival_datetime['day']) { // check whether ups delivery date matches w/ customer 		delivery date
                        $not_found = false;
                    } else {
                        $not_found = true;
                        $not_found_count += 1;
                    }
                }
                if ($not_found && (count($tocheck_shortest_routes) == $not_found_count)) {
				
                    $tmp_pickup_datetime_year = substr($tmp_pickup_date, 0, 4);
                    $tmp_pickup_datetime_month = substr($tmp_pickup_date, 4, 2);
                    $tmp_pickup_datetime_day = substr($tmp_pickup_date, 6, 2);
                    $tmp_pickup_datetime = $tmp_pickup_datetime_year . '-' . $tmp_pickup_datetime_month . '-' . $tmp_pickup_datetime_day;
                    $tmp_pickup_date_new = str_replace("-", "", date('Y-m-d', strtotime($tmp_pickup_datetime . ' + 1 days')));

                    $pickup['Date'] = $tmp_pickup_date_new; //'20150205'; //Ymd format
                    $request['Pickup'] = $pickup;

                    $resp = $client->__soapCall($operation, array($request));
                    $tmp_array = $resp->TransitResponse->ServiceSummary; //die;
                }
            }

            return $tmp_array;

        } catch (Exception $e) {
            //print_r($e);
        }
    }
function syncSimpleInventory($product_id,$qty_ordered,$warehouse_id)
{

    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');
    $writeConnection = $resource->getConnection('core_write');

    echo $query_qty_select_warehouse = "select `quantity_in_stock` from `advancedinventory_stock` where product_id =" . $product_id . " and place_id =" . $warehouse_id;
    echo $query_qty_select_catalog_inventory = "select `qty` from `cataloginventory_stock_item` where product_id =" . $product_id;

    echo  $existing_qty_warehouse = $readConnection->fetchOne($query_qty_select_warehouse);
    echo $existing_qty_catalog = $readConnection->fetchOne($query_qty_select_catalog_inventory);
	
    $updated_qty_warehouse = $existing_qty_warehouse-$qty_ordered;
    $updated_qty_catalog = $existing_qty_catalog-$qty_ordered;
    
    echo $query_qty_update = "UPDATE `advancedinventory_stock` SET `quantity_in_stock` = '" . $updated_qty_warehouse . "' WHERE  place_id = " . $warehouse_id . ' and ' . '`product_id` =' . $product_id;
    $writeConnection->query($query_qty_update); 
    
    echo $query_catalog_qty_update = "UPDATE `cataloginventory_stock_item` SET `qty` = '" . $updated_qty_catalog . "' WHERE `product_id` =" . $product_id;
     
    $writeConnection->query($query_qty_update); 
    $writeConnection->query($query_catalog_qty_update); 
    //echo "all updated success";die;
}
    function getLnt($zip) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=
" . urlencode($zip) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $result1[] = $result['results'][0];
        $result2[] = $result1[0]['geometry'];
        $result3[] = $result2[0]['location'];
        return $result3[0];
    }

    function getDistance($zip1, $zip2) {
	
        $first_lat = getLnt($zip1);
        $next_lat = getLnt($zip2);

        $lat1 = $first_lat['lat'];
        $lon1 = $first_lat['lng'];
        $lat2 = $next_lat['lat'];

        $lon2 = $next_lat['lng'];
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +
                cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
                cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        return ($miles * 0.8684);
    }  
  
  
function createOrder(&$partnerProducts, $billingAddress, $shipmentAddress, $wareHouse, $giftText, $partnerOrderId, $customerOrderNumber, $pickup, $businessType, $shippingAmount) {
    require_once '../app/Mage.php';
    umask(0);
    Mage::app('default');
    $customer_email = "shivankiitr@gmail.com";
    $customer = Mage::getModel("customer/customer")->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($customer_email);
    $transaction = Mage::getModel('core/resource_transaction');
    $storeId = $customer->getStoreId();
    $reservedOrderId = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId);

    $order = Mage::getModel('sales/order')
            ->setIncrementId($reservedOrderId)
            ->setStoreId($storeId);
    $order->setCustomer_email($customer->getEmail())
            ->setCustomerFirstname($customer->getFirstname())
            ->setCustomerLastname($customer->getLastname())
              ->setCustomerGroupId($customer->getGroupId())
            ->setCustomer_is_guest(0)
            ->setCustomer($customer);
    $billing = $customer->getDefaultBillingAddress();
	if($billingAddress == null)
	{
            $billingAddress = Mage::getModel('sales/order_address')
            ->setStoreId($storeId)
            ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
            ->setCustomerId($customer->getId())
            ->setCustomerAddressId($customer->getDefaultBilling())
            ->setFirstname($billing->getFirstname())
		    ->setMiddlename($billing->getMiddlename())
		    ->setLastname($billing->getLastname())
		    ->setStreet($billing->getStreet())
		    ->setCity($billing->getCity())
		    ->setCountryId($billing->getCountryId())
		    ->setRegion($billing->getRegion())
		    ->setRegionId($billing->getRegionId())
		    ->setPostcode($billing->getPostcode())
		    ->setTelephone($billing->getTelephone());
	}
	else
	{
	       $region = Mage::getModel('directory/region')->loadByCode($billingAddress->region, 'US');
           $region_id = $region->getId(); //12
           $region = Mage::getModel('directory/region')->load($region_id);
           $region_name = $region->getName();
		   $region_code = $billingAddress->region;
		   $billingStreet[0] = $billingAddress->streetLine1 != null ? $billingAddress->streetLine1 : "";
		   $billingStreet[1] = $billingAddress->streetLine2 != null ? $billingAddress->streetLine2 : "";
		   $name = $billingAddress->firstname;
		   //list($firstName, $lastName) = explode(' ', $name);
		   $name = explode(' ', $name);
			$firstName = $name[0];
			$lastName = (isset($name[1])) ? $name[1] : '';
			$company = (isset($name[2])) ? $name[2]."".$name[3] ."".$name[4] : '';
			
			
			$billingAddress = Mage::getModel('sales/order_address')
            ->setStoreId($storeId)
            ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
            ->setCustomerId($customer->getId())
            ->setCustomerAddressId($customer->getDefaultShipping())
            ->setFirstname($firstName != null ? $firstName : "")
            ->setMiddlename($billingAddress->middlename != null ? $billingAddress->middlename : "")
            ->setLastname($lastName != null ? $lastName : "")
            ->setCompany($company != null ? $company : "")
            ->setStreet($billingStreet)
            ->setCity($billingAddress->city != null ? $billingAddress->city : "")
            ->setCountryId($billingAddress->countryId)
            ->setRegionId($region_id)
			->setRegionCode($region_code)
			->setRegion($region_name)
            ->setPostcode($billingAddress->postcode != null ? $billingAddress->postcode : "")
            ->setTelephone($billingAddress->telephone != null ? $billingAddress->telephone : "Not available");
	}
    $order->setBillingAddress($billingAddress);
    $shipping = $customer->getDefaultShippingAddress();

  $region = Mage::getModel('directory/region')->loadByCode($shipmentAddress->region, 'US');
          $region_id = $region->getId(); //12
$region = Mage::getModel('directory/region')->load($region_id);
$region_name = $region->getName();
$region_code = $shipmentAddress->region;
$shippingStreet[0] = $shipmentAddress->streetLine1 != null ? $shipmentAddress->streetLine1 : "";
$shippingStreet[1] = $shipmentAddress->streetLine2 != null ? $shipmentAddress->streetLine2 : "";
$shippingTelephone = $shipmentAddress->telephone != null ? $shipmentAddress->telephone : $billingAddress->telephone ;
		  $shipname = $shipmentAddress->firstname;
		   
		   $Shippingname = explode(' ', $shipname);
			$shippingFirstName = $Shippingname[0];
			$shippingLastName = (isset($Shippingname[1])) ? $Shippingname[1] : '';
			$shippingCompany = (isset($Shippingname[2])) ? $Shippingname[2]." ".$Shippingname[3] ." ".$Shippingname[4] : '';
echo "............5...............".$region_code; 
    $shippingAddress = Mage::getModel('sales/order_address')
            ->setStoreId($storeId)
            ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
            ->setCustomerId($customer->getId())
            ->setCustomerAddressId($customer->getDefaultShipping())
            ->setFirstname($shippingFirstName != null ? $shippingFirstName : "")
            ->setMiddlename($shipmentAddress->middlename != null ? $shipmentAddress->middlename : "")
            ->setLastname($shippingLastName != null ? $shippingLastName : "")
            ->setCompany($shippingCompany != null ? $shippingCompany : "")
            ->setStreet($shippingStreet)
            ->setCity($shipmentAddress->city != null ? $shipmentAddress->city : "")
            ->setCountryId($shipmentAddress->countryId)
            ->setRegionId($region_id)
			->setRegionCode($region_code)
			->setRegion($region_name)
            ->setPostcode($shipmentAddress->postcode != null ? $shipmentAddress->postcode : "")
            ->setTelephone($shippingTelephone);
    $order->setShippingAddress($shippingAddress)
            ->setShippingMethod('flatrate_flatrate'); 
//print_r($order->getData());
//die;
    $orderPayment = Mage::getModel('sales/order_payment')
            ->setStoreId($storeId)
            ->setCustomerPaymentId(0)
            ->setMethod('purchaseorder')
            ->setPo_number('123');
    $order->setPayment($orderPayment);
 switch($wareHouse){
     case "AFRH": $wareHouseId = 3; break;
	 case "AEXB": $wareHouseId = 5; break;
	 case "AEJM": $wareHouseId = 1; break;
	 case "AMCZ": $wareHouseId = 7; break;
	 case "AMDA": $wareHouseId = 8; break;
	 case "AMDB": $wareHouseId = 9; break;
	 case "AMDC": $wareHouseId = 10; break;
	 case "AQGJ": $wareHouseId = 2; break;
	 default: $wareHouseId = 3;
   }
   echo $wareHouseId;
    $subTotal = 0;
	$info = Mage::getModel('sales/order')->getCollection()->addAttributeToFilter('partner_order_id', $partnerOrderId);
	if($info->count()>0)
	{
		echo "this is duplicate order";
	    foreach ($partnerProducts as $partnerProduct) {
		 $partnerProduct->itemResult = "DUPLICATE_ORDER";
		}
		return -1;
	}
    foreach ($partnerProducts as $partnerProduct) {
        $partnerProduct->price = str_replace('$','',$partnerProduct->price);
        $rowTotal = $partnerProduct->price * $partnerProduct->quantity;
		$qty = getStockQty($partnerProduct->sku);
		
		    if($qty == "") {
			   $partnerProduct->itemResult = "INVALID_SKU"; 
                $orderItem = Mage::getModel('sales/order_item')
                ->setStoreId($storeId)
                ->setQuoteItemId(0)
                ->setQuoteParentItemId(NULL)
                ->setProductType('simple')
                ->setQtyBackordered(NULL)
                ->setQtyOrdered($partnerProduct->quantity)
                ->setName($partnerProduct->title)
                ->setSku($partnerProduct->id)
                ->setPrice($partnerProduct->price)
                ->setBasePrice($partnerProduct->price)
                ->setOriginalPrice($partnerProduct->price)
                ->setRowTotal($rowTotal)
                ->setBaseRowTotal($rowTotal);
                 $subTotal += $rowTotal;
                 $order->addItem($orderItem);              			   
			}
		     else 
			 {
			    if($qty == 0) 
				{
			         $partnerProduct->itemResult = "INSUFFICIENT_INVENTORY";
			    }
		        else 
				{
		              $partnerProduct->itemResult = "SUFFICIENT_INVENTORY"; 
		        }
				echo ".........7..........".$partnerProduct->price;
		        $productItem = Mage::getModel('catalog/product')->loadByAttribute('sku',$partnerProduct->sku);
                $orderItem = Mage::getModel('sales/order_item')
		        ->setName($productItem->getName())
				->setDescription($productItem->getName())
                ->setStoreId($storeId)
                ->setQuoteItemId(0)
                ->setQuoteParentItemId(NULL)
                ->setProductId($productItem->getId())
                ->setProductType('simple')
                ->setQtyBackordered(NULL)
                ->setQtyOrdered($partnerProduct->quantity)
                ->setName($partnerProduct->title)
                ->setSku($partnerProduct->sku)
                ->setPrice($partnerProduct->price)
                ->setBasePrice($partnerProduct->price)
                ->setOriginalPrice($partnerProduct->price)
        ->setRowTotal($rowTotal)
        ->setBaseRowTotal($rowTotal);
          $subTotal += $rowTotal;
        $order->addItem($orderItem);
        syncSimpleInventory($productItem->getId(),$partnerProduct->quantity,$wareHouseId);
          }
   }
    $order->setSubtotal($subTotal)
            ->setBaseSubtotal($subTotal)
            ->setGrandTotal($subTotal)
            ->setBaseGrandTotal($subTotal);
  
    //$wareHouseId = (wareHouse == 'AEJM')?1:(($wareHouse == 'AFRH')?3:2); //This logic need to be verified
    $productSKU = $partnerProducts[0]->sku;
    $productId = getproductId($productSKU);
    $qty = $partnerProducts[0]->quantity;
    $json =  '{' . '"' . $productId . '":' . '{';
    for($i = 1; $i < 12; $i++) {
    $wareHouseQty = ($wareHouseId==$i)?$qty:0;
    $json = $json . '"'. $i . '":' . $wareHouseQty . ",";
    }
    $wareHouseQty = ($wareHouseId==$i)?$qty:0;
    $json = $json . '"'. $i . '":' . $wareHouseQty . '}}';

    $order->setAssignationStock($json);
    $order->setAssignationWarehouse($wareHouseId);
     $order->setProdSku($productSKU);

	$order->setPartnerWarehouse($wareHouse);
    $order->setTradingPartner($businessType);
    $order->setPartnerOrderId($partnerOrderId);
	$order->setWarehouse($wareHouse);
	//echo "......5........".$customerOrderNumber;
	$order->setCustomerOrderNumber($customerOrderNumber);
	$order->setPickupDate($pickup->datetime);
	$order->setPickupCode($pickup->code);
    $order->setPickupMethod($pickup->description);
	    $order->setPartnerShipmentMethod($pickup->code);
	    $order->setPickupType($pickup->type);
	//$order->setPickupMethod('FEDEX_NEXT_STD');
    $giftMessage = Mage::getModel('giftmessage/message');
    $giftMessage->setCustomerId($customer->getId());
    $giftMessage->setSender($businessType);
    $giftMessage->setRecipient($businessType);
    $giftMessage->setMessage($giftText);
    $giftObj = $giftMessage->save();
    $order->setGiftMessageId($giftObj->getId());
	$order->setStatus("processing");
	
    $order->save();
    $transaction->addObject($order);
    $transaction->addCommitCallback(array($order, 'save'));
    $transaction->save();
    $data = array('trading_partner'=>$businessType,'partner_order_id' => $partnerOrderId);
    /*$myOrder = Mage::getModel('sales/order_grid')->loadByIncrementId($reservedOrderId);
	$myOrder->addData($data); 
		try {
                $myOrder->save();

            } catch (Exception $e){
                echo $e->getMessage();
            }
	*/
    //save_shipping_date($order, $shipmentAddress->postcode, $deliveryDate, $json, $shippingAmount);
	//save_shipping_date($order, '033160', $deliveryDate, $json);
echo "........15.......";
//print_r($reservedOrderId); 
//die;
return $reservedOrderId;
/* till here */
}

?>
