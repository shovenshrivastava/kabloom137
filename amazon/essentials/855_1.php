<?php

require_once('time.php');

/*"00" Shipping 100 percent of ordered product 
"02N canceled due to missing/invalid SKU 
"03" canceled out of stock 
"04N canceled due to duplicate Amazon Ship ID 
"05' Canceled due to missing/invalid Bill To Location Code 
"06' Canceled due to missing/invalid Ship From Location Code 
"OT' Canceled due to missing/invalid Customer Ship to Name 
"08' Canceled due to missing/invalid Customer Ship to Address Line 1 
·og· Canceled due to missing/invalid Customer Ship to City 
"10" Canceled due to missing/invalid Customer Ship to State 
"11" Canceled due to missing/invalid Customer Ship to Postal Code 
"12" Canceled due to missing/invalid Customer Ship to Country Code 
"13" canceled due to missing/invalid Shipping Carrier/Shipping Method 
"20" canceled due to missing/invalid Unit Price 
"21" Canceled due to missing/invalid Ship to Address Une 2 */

function getItemResult($resultStatement) {
   switch($resultStatement)
   {
	   case "INSUFFICIENT_INVENTORY": { $Item_Result = "03" ; break;}
	   case "INVALID_SKU": { $Item_Result = "02N" ; break;}
	   default: { $Item_Result = "00" ; break;}
   }
   return $Item_Result;
}

function getTCount($numberOfItems) {
   return $numberOfItems*2 + 5;
}

function generateResponse($order, $ICN) {

   $ack_flag = 1 ;
   $nack_flag = 1 ;
   $BAK_02 = "AT";

   $S_date = getSystemDate() ;
   $S_time = getSystemTime() ;

   $T_count = getTCount(count($order->products)) ;
   $edi = "";
   $edi = $edi . "ISA*00*          *00*          *ZZ*KABLOOM        *ZZ*AMAZONDS       *$S_date*$S_time*U*00401*$ICN*0*T*>\n";
   $edi = $edi . "GS*PR*KABLOOM*AMAZONDS*20$S_date*$S_time*42*X*004010\n";
   $edi = $edi . "ST*855*000000003\n";
   $products = $order->products;
   foreach($products as $product)
   {   
        $Item_Result = getItemResult($product->itemResult);
	    if ($Item_Result != "00")
		{ 
		    $nack_flag =0 ; 
	    }
		
		if ($Item_Result == "00")
		{ 
		    $ack_flag =0 ; 
		}
    }
 
   if ($ack_flag ==0 && $nack_flag ==1 )
   {
       $edi = $edi . "BAK*00*AT*$order->partnerOrderId*$S_date***$order->orderId\n";
   }
   
   if ($nack_flag == 0 )
   {
       $edi = $edi . "BAK*00*RD*$order->partnerOrderId*$S_date***$order->orderId\n";
   }
      
   $edi = $edi . "Nl*SF*$order->wareHouse*92*$order->wareHouse\n";
   $i = 0;
   foreach($products as $product)
   {   
      $i++;
      $id = $product->id;
	  $edi = $edi . "P01*$i*$product->quantity*EA***SK*$id\n";
	  if ($Item_Result = "00")
	  {
	        $edi = $edi . "ACK*IA*$product->quantity*EA**************************$Item_Result\n" ;
	  }
	  else
	  {
	        $edi = $edi . "ACK*IR*$product->quantity*EA**************************$Item_Result\n" ;
	  }  
  }
  $edi = $edi . "CTT*$i\n";
  $edi = $edi . "SE*$T_count*000000003\n";
  $edi = $edi . "GE*1*42\n";
  $edi = $edi . "IEA*1*$ICN\n";
  echo $edi;
  //write_log('Info: generateResponse.php . $edi', 'test');
  //echo $edi;
  return $edi;
}
?>