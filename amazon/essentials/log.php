<?php
define("DEFAULT_LOG","../logs/php.log");
  
function write_log($message, $logfile='') {
  if($logfile == '') {
    if (defined('DEFAULT_LOG') == TRUE) {
        $logfile = DEFAULT_LOG;
    }
    else {
        error_log('No log file defined!',0);
        return array(status => false, message => 'No log file defined!');
    }
  }

  if( ($time = $_SERVER['REQUEST_TIME']) == '') {
    $time = time();
  }
  $date = date("Y-m-d H:i:s", $time);
  if($fd = @fopen($logfile, "a")) {
    $result = fputcsv($fd, array($date, $message));
    fclose($fd);
  }
  else {
   // return array(status => false, message => 'Unable to open log '.$logfile.'!');
  }
}

?>
