<?php
require_once("app/Mage.php");
umask(0);
Mage::app("default");

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
Mage::init();

// Set an Admin Session
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
Mage::getSingleton('core/session', array('name' => 'adminhtml'));
$userModel = Mage::getModel('admin/user');
$userModel->setUserId(1);
$session = Mage::getSingleton('admin/session');
$session->setUser($userModel);
$session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());

//$order = Mage::getModel('sales/order')->getCollection()->getLastItem();
$order = Mage::getModel('sales/order')->load('100001952', 'increment_id');

$array = $order->toArray();
$xml = new SimpleXMLElement('<order/>');

//necessary to flip keys/values for conversion
$array = array_flip($array);
array_walk_recursive($array, array($xml, 'addChild'));

echo $xml->asXML();

?>
