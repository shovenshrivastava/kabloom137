<?php

include 'app/Mage.php';
 umask(0);
    Mage::app('default');

$products = Mage::getModel('catalog/product')->getCollection()
            	->addAttributeToFilter('type_id', 'simple')
              ->addAttributeToSelect('entity_id')
              ->addAttributeToSelect('name')
              ->addAttributeToSelect('price')
           //   ->addAttributeToSelect('description')
              ->addAttributeToSelect('sku');


foreach($products->getData() as $key=>$value)
{

	$fin[$key]['sku'] = $value['sku'];
	$fin[$key]['name'] = $value['name'];
	$fin[$key]['price'] = $value['price'];
	//$fin[$key]['description'] = $value['description'];
	
	$fin[$key]['recipe'] = Mage::getResourceModel('catalog/product')->getAttributeRawValue($value['entity_id'], 'recipe');
	

}


header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=sku_recipe.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('sku','name','price','recipe'));

$delimiter=',';
$enclosure = '"';

 foreach ($fin as $k=>$v) {

        fputcsv($output, $v, $delimiter,$enclosure);
    }

?>
