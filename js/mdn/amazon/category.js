function findBrowseNode(comboName, fromScratch, direction)
{
    var combo = document.getElementById(comboName);
    var searchString = document.getElementById('search_' + comboName).value.toLowerCase();
    var start = 0;


    if (direction == 1)
    {
        if (!fromScratch)
            start = combo.selectedIndex + 1;
        for (var i=start; i<combo.length; i++){
            var currentCaption = combo.options[i].text.toLowerCase();
            if (currentCaption.indexOf(searchString) > -1)
            {
                combo.selectedIndex = i;
                return true;
            }
        }
    }
    else
    {
        if (!fromScratch)
            start = combo.selectedIndex - 1;
        for (var i=start; i>0; i--){
            var currentCaption = combo.options[i].text.toLowerCase();
            if (currentCaption.indexOf(searchString) > -1)
            {
                combo.selectedIndex = i;
                return true;
            }
        }
    }



    //if we reach here, means that there is no options
    alert('No result found');
}