<?php
require_once ('app/Mage.php');
Mage::app('default');
error_reporting(E_ALL);
ini_set('display_errors', 1);
umask(0);
require('pdf_js.php');

//echo $_REQUEST['pdf']; exit;

class PDF_AutoPrint extends PDF_JavaScript
{
function AutoPrint($dialog=false)
{
	//Open the print dialog or start printing immediately on the standard printer
	$param=($dialog ? 'true' : 'false');
	$script="print($param);";
	$this->IncludeJS($script);
}

function AutoPrintToPrinter($server, $printer, $dialog=false)
{
	//Print on a shared printer (requires at least Acrobat 6)
	$script = "var pp = getPrintParams();";
	if($dialog)
		$script .= "pp.interactive = pp.constants.interactionLevel.full;";
	else
		$script .= "pp.interactive = pp.constants.interactionLevel.automatic;";
	$script .= "pp.printerName = '\\\\\\\\".$server."\\\\".$printer."';";
	$script .= "print(pp);";
	$this->IncludeJS($script);
}
}

//$pdf=new PDF_AutoPrint();
//$pdf->AddPage();
//$pdf->SetFont('Arial','',20);
//$pdf->Text(90, 50, 'Print me!');
//Open the print dialog
//$pdf->AutoPrint(true);
//$pdf->Output();
?>
