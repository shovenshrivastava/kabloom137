<?php
include 'app/Mage.php';
 umask(0);
    Mage::app('default');
$fromDate = date('Y-m-d H:i:s', strtotime($_GET['from_date']));
$toDate = date('Y-m-d H:i:s', strtotime($_GET['to_date']));


$orders = Mage::getModel('sales/order')->getCollection()
	->addAttributeToSelect('increment_id')
		->addAttributeToSelect('status')
			->addAttributeToSelect('pickup_method')
			->addAttributeToSelect('delivering_date')
					->addAttributeToSelect('pickup_date')
					->addAttributeToSelect('coupon_code')
					->addAttributeToSelect('discount_amount')
					->addAttributeToSelect('total_qty_ordered')

	->addAttributeToSelect('grand_total')
		//->addAttributeToSelect('trading_partner')
   // ->addAttributeToFilter('main_table.trading_partner', 'amazon-marketplace')
    ->addAttributeToFilter('main_table.created_at', array('from'=>$fromDate, 'to'=>$toDate));
   // ->addAttributeToFilter('main_table.status', array('eq' => Mage_Sales_Model_Order::STATE_COMPLETE))

;

/*
$track_info = Mage::getResourceModel('sales/order_shipment_track_collection')->addAttributeToSelect('track_number')->addAttributeToFilter('order_id', $ids);
*/
//echo $track_info->getSelect();



$orders->getSelect()->join('sales_flat_order_item', 'order_id=entity_id', array('name'=>'name', 'sku' =>'sku','product_options'=>'product_options'), null,'left')->where('product_type = ?', 'simple');

/*
$orders->getSelect()->join('sales_flat_shipment_track', 'main_table.entity_id = sales_flat_shipment_track.order_id', array('track_number','carrier_code','title'));
*/
//$orders->getSelect()->join('sales_flat_order_item', '`sales_flat_order_item`.order_id=`main_table`.entity_id AND `sales_flat_order_item`.parent_item_id IS NULL  ', null);

//$orders->getSelect()->join('catalog_product_entity_varchar', '`catalog_product_entity_varchar`.attribute_id=499 AND `catalog_product_entity_varchar`.entity_id = `sales_flat_order_item`.`product_id`', array('recipe'  => new Zend_Db_Expr((`catalog_product_entity_varchar`.value ))));

//echo $orders->getSelect();die;

//print_r($orders->getData());die;

foreach($orders->getData() as $key=>$value)
{
	//if($value['trading_partner']=='amazon') {
	$fin[$key]['order_id'] = $value['increment_id'];
	$fin[$key]['status'] = $value['status'];
	//$fin[$key]['delivering_date'] = $value['delivering_date'];
	$res = unserialize($value['product_options']);
	

		$fin[$key]['delivery_date'] =  array_pop($res['info_buyRequest']['options']);

	
	$fin[$key]['pickup_method'] = $value['pickup_method'];
	$fin[$key]['pickup_date'] = $value['pickup_date'];
	$fin[$key]['name'] = $value['name'];
	$fin[$key]['sku'] = $value['sku'];
	$fin[$key]['recipe'] = $value['recipe'];
	$fin[$key]['total_qty_ordered'] = $value['total_qty_ordered'];
	$fin[$key]['grand_total'] = $value['grand_total'];
	$fin[$key]['coupon_code'] = $value['coupon_code'];
	$fin[$key]['discount_amount'] = $value['discount_amount'];
	
//}

}


//print_r($fin);
//die;


header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=recipe_report.csv');


// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('order_id','status', 'delivering_date','delivery_date','pickup_method','pickup_date','name','sku','recipe','total_qty_ordered','grand_total','coupon_code','discount_amount'));
$delimiter=',';
$enclosure = '"';

 foreach ($fin as $k=>$v) {

        fputcsv($output, $v, $delimiter,$enclosure);
    }

?>
