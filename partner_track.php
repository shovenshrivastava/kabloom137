<?php
include 'app/Mage.php';
 umask(0);
    Mage::app('default');

	$time = time();
	$to = date('Y-m-d H:i:s', $time);
	$lastTime = $time - 518400; // 60*60*24*2 - 4days
	$from = date('Y-m-d H:i:s', $lastTime);

$orders = Mage::getModel('sales/order')->getCollection()
	->addAttributeToSelect('increment_id')
	->addFieldToFilter('trading_partner','amazon')
	//->addAttributeToFilter('created_at', array('from' => $from, 'to' => $to));
		->addAttributeToSelect('partner_order_id');

$orders->getSelect()->join('sales_flat_shipment_track', 'main_table.entity_id = sales_flat_shipment_track.order_id', array('track_number','carrier_code','title'));


foreach($orders->getData() as $key=>$value)
{
	//if($value['trading_partner']=='amazon') {
	$fin[$key]['order_id'] = $value['increment_id'];
	$fin[$key]['partner_order_id'] = $value['partner_order_id'];
	$fin[$key]['track_number'] = $value['track_number'];

	
	//echo "###".$value['product_id'];
	
//}

}

//print_r($fin);
//die;


header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=recipe_report.csv');


// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('order_id','partner_order_id', 'track_number'));
$delimiter=',';
$enclosure = '"';

 foreach ($fin as $k=>$v) {

        fputcsv($output, $v, $delimiter,$enclosure);
    }

?>
