#!/bin/sh
# How many days retention do we want ?
DAYS=0

# geting present day 
now=$(date +"%m_%d_%Y")

# Where is the base directory
BASEDIR=/var/www/html/magento/mendelson1/log

#where is the backup directory
BKPDIR=/var/www/backup/mendelsonbkp

# Where is the log file
LOGFILE=/var/www/backup/log/mendelson.log

# add to tar 
tar -cvzf $now.tar.gz $BASEDIR

mv $now.tar.gz $BKPDIR

# REMOVE OLD FILES
echo `date` Purge Started >> $LOGFILE
find $BASEDIR -mtime +$DAYS | xargs rm
echo `date` Purge Completed >> $LOGFILE

