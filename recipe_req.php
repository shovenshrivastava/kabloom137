<?php
include 'app/Mage.php';
 umask(0);
    Mage::app('default');
    $resource = Mage::getSingleton('core/resource');
$fromDate = date('Y-m-d H:i:s', strtotime($_GET['from_date']));
$toDate = date('Y-m-d H:i:s', strtotime($_GET['to_date']));



$orders = Mage::getModel('sales/order')->getCollection()
//	->addAttributeToSelect('increment_id')
//		->addAttributeToSelect('status')
//			->addAttributeToSelect('pickup_method')
//			->addAttributeToSelect('delivering_date')
//					->addAttributeToSelect('pickup_date')
//					->addAttributeToSelect('coupon_code')
//					->addAttributeToSelect('discount_amount')
//					->addAttributeToSelect('total_qty_ordered')
//
//	->addAttributeToSelect('grand_total')
		//->addAttributeToSelect('trading_partner')
   // ->addAttributeToFilter('main_table.trading_partner', 'amazon-marketplace')
    ->addAttributeToFilter('main_table.created_at', array('from'=>$fromDate, 'to'=>$toDate));
   // ->addAttributeToFilter('main_table.status', array('eq' => Mage_Sales_Model_Order::STATE_COMPLETE))

;

                                    
/*
$track_info = Mage::getResourceModel('sales/order_shipment_track_collection')->addAttributeToSelect('track_number')->addAttributeToFilter('order_id', $ids);
*/
//echo $track_info->getSelect();



$orders->getSelect()->join('sales_flat_order_item', 'order_id=entity_id', array('name'=>'name',  'sku' =>'sku','price' =>'price','product_options'=>'product_options','product_id'=>'product_id'), null,'left')->where('sales_flat_order_item.product_type = ?', 'simple');


$orders->getSelect()->join(
    array('billing' => $resource->getTableName('sales/order_address')),
    'main_table.billing_address_id = billing.entity_id',
    array('billing_city' => 'city', 'billing_street' => 'street', 'billing_telephone' => 'telephone', 'billing_postcode' => 'postcode', 'billing_region' => 'region','billing_lastname' => 'lastname','billing_firstname' => 'firstname')
);

$orders->getSelect()->joinLeft(
    array('shipping' => $resource->getTableName('sales/order_address')),
    'main_table.shipping_address_id = shipping.entity_id',
    array('shipping_city' => 'city', 'shipping_street' => 'street', 'shipping_telephone' => 'telephone', 'shipping_postcode' => 'postcode', 'shipping_region' => 'region','shipping_lastname' => 'lastname','shipping_firstname' => 'firstname')
);

/*
$orders->getSelect()->join('sales_flat_shipment_track', 'main_table.entity_id = sales_flat_shipment_track.order_id', array('track_number','carrier_code','title'));
*/
//$orders->getSelect()->join('sales_flat_order_item', '`sales_flat_order_item`.order_id=`main_table`.entity_id AND `sales_flat_order_item`.parent_item_id IS NULL  ', null);

//$orders->getSelect()->join('catalog_product_entity_varchar', '`catalog_product_entity_varchar`.attribute_id=499 AND `catalog_product_entity_varchar`.entity_id = `sales_flat_order_item`.`product_id`', array('recipe'  => new Zend_Db_Expr((`catalog_product_entity_varchar`.value ))));

//echo $orders->getSelect();die;

//print_r($orders->getData());die;

foreach($orders->getData() as $key=>$value)
{
	//if($value['trading_partner']=='amazon') {
	$fin[$key]['order_id'] = $value['increment_id'];
	$fin[$key]['status'] = $value['status'];
	//$fin[$key]['delivering_date'] = $value['delivering_date'];
	$res = unserialize($value['product_options']);
	$fin[$key]['delivery_date'] =  array_pop($res['info_buyRequest']['options']);

	$fin[$key]['pickup_method'] = $value['pickup_method'];
	$fin[$key]['pickup_date'] = $value['pickup_date'];
	$fin[$key]['name'] = $value['name'];
	$fin[$key]['sku'] = $value['sku'];
	$fin[$key]['trading_partner'] = $value['trading_partner'];
	$fin[$key]['recipe'] = Mage::getResourceModel('catalog/product')->getAttributeRawValue($value['product_id'], 'recipe');
	$fin[$key]['total_qty_ordered'] = $value['total_qty_ordered'];
	$fin[$key]['grand_total'] = $value['grand_total'];
	$fin[$key]['coupon_code'] = $value['coupon_code'];
	$fin[$key]['discount_amount'] = $value['discount_amount'];
    $fin[$key]['shipping_amount'] = $value['shipping_amount'];
	$fin[$key]['shipping_method'] = $value['pickup_method'];
	$fin[$key]['price'] = $value['price'];
	$fin[$key]['billing_firstname'] = $value['billing_firstname'];
	$fin[$key]['billing_lastname'] = $value['billing_lastname'];
	$fin[$key]['billing_street'] = $value['billing_street'];
	$fin[$key]['billing_city'] = $value['billing_city'];
	$fin[$key]['billing_telephone'] = $value['billing_telephone'];
	$fin[$key]['billing_zipcode'] = $value['billing_zipcode'];
	$fin[$key]['shipping_firstname'] = $value['shipping_firstname'];
	$fin[$key]['shipping_lastname'] = $value['shipping_lastname'];
	$fin[$key]['shipping_city'] = $value['shipping_city'];
	$fin[$key]['shipping_street'] = $value['shipping_street'];
	$fin[$key]['shipping_telephone'] = $value['shipping_telephone'];
	$fin[$key]['shipping_postcode'] = $value['shipping_postcode'];
	$fin[$key]['shipping_region'] = $value['shipping_region'];
	$fin[$key]['purchase on'] = $value['created_at'];
	//$track = Mage::getResourceModel('sales/order_shipment_track_collection')->addAttributeToSelect('track_number')->addAttributeToFilter('order_id', $value['entity_id']);
	////$order = Mage::getModel('sales/order')->loadByIncrementId($value['entity_id']);
	////print_r($track->getData()); die;
	//foreach ($track->getData() as $_track) {
			 //$fin[$key]['track_number'] = $_track['track_number'];
			
		//}
		
		$fin[$key]['partner_order_id'] = $value['partner_order_id'];
		$fin[$key]['order_date'] = $value['created_at'];
	//print_r( $fin[$key]['track_number'] ); die;
        
	//echo "###".$value['product_id'];
	
//}

}
//echo '<pre>';print_r($fin);
//die;


header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=recipe_report.csv');


// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('order_id','status', 'delivery_date','pickup_method','pickup_date','name','sku','trading_partner','recipe','total_qty_ordered','grand_total','coupon_code','discount_amount','shipping_amount','shipping_method','price','billing_firstname','billing_lastname','billing_street','billing_city','billing_telephone','billing_zipcode','shipping_firstname','shipping_lastname','shipping_city','shipping_street','shipping_telephone','shipping_postcode','shipping_region','purchase on','partner_order_id','order_date'));
$delimiter=',';
$enclosure = '"';

 foreach ($fin as $k=>$v) {

        fputcsv($output, $v, $delimiter,$enclosure);
    }

?>
