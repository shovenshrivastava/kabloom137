<?php
include 'app/Mage.php';
 umask(0);
    Mage::app('default');
    $resource = Mage::getSingleton('core/resource');
$date = $_REQUEST['fromdate'];
$dateto = $_REQUEST['todate'];
$asel = $_REQUEST['type'];
$partner = $_REQUEST['partner'];
$pickupmethod = $_REQUEST['pickup'];
$warehouse = $_REQUEST['warehouse'];

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

if($date && $asel == "q1"){
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=most_order_product.csv');
	// output the column headings
	fputcsv($output, array('Sku','Name', 'Quantity'));
	$orders = Mage::getModel('sales/order_item')->getCollection()
        ->addFieldToFilter('created_at', array('from'=>$date, 'to'=>$dateto))
		->addFieldToSelect('sku')
		->addFieldToSelect('name')
		->addFieldToSelect('description');


$orders->getSelect()
        ->columns('COUNT(*) AS qty')
		->group('description')
		->order('qty DESC');
//$orders->printLogQuery(true); //exit;
}
if($date && $asel == "q2"){
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=most_order_zipcode.csv');
	// output the column headings
	
	fputcsv($output, array('Total count','Zipcode', 'City', 'State'));
$orders = Mage::getModel('sales/order')->getCollection()
        ->addFieldToFilter('main_table.created_at', array('from'=>$date, 'to'=>$dateto))
		->addAttributeToFilter('main_table.status', array('in' => array('invoice_sent')))
		->addFieldToSelect('entity_id');
		
$saleOrderTableAlias = "s_o";
//Table join
$orders->join(
    array($saleOrderTableAlias => 'sales/order_address'),
    'main_table.entity_id=s_o.entity_id',
	array('city')
);

$orders->getSelect()
        ->where("s_o.address_type >= 'shipping'");
		
$orders->getSelect()
        ->columns('COUNT(s_o.postcode) AS total_count')
		->columns('s_o.postcode AS zipcode')
		->columns('s_o.region AS State')
		->group('zipcode')
		->order('total_count DESC');
		//$orders->printLogQuery(true); exit;
}

if($date && $asel == "q3"){
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=shipped_orders.csv');
	// output the column headings
	
	fputcsv($output, array('Order Id','Partner Order Id', 'Track Numer'));
$orders = Mage::getModel('sales/order')->getCollection()
		->addFieldToSelect('partner_order_id');
		
$saleOrderTableAlias = "s_o";

$orders->getSelect()->
join(Mage::getConfig()->getTablePrefix().'sales_flat_shipment_track', 
'main_table.entity_id ='.Mage::getConfig()->getTablePrefix().'sales_flat_shipment_track.order_id',
array('order_id','track_number'));

$orders->getSelect()
        ->where(Mage::getConfig()->getTablePrefix()."sales_flat_shipment_track.created_at >= '".$date."' AND ".Mage::getConfig()->getTablePrefix()."sales_flat_shipment_track.created_at <= '".$dateto."'");

		//$orders->printLogQuery(true); exit;
}

if($asel == "q4"){
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=product_partners.csv');
	// output the column headings
	
	fputcsv($output, array('Trading Partner','Sku','Name', 'Recipe'));
$attributeName = 'recipe';
$eav = Mage::getResourceModel('eav/entity_attribute_collection')
                ->addFieldToFilter('attribute_code', $attributeName)
				->addFieldToSelect('attribute_id');
$attributeId = $eav->getFirstItem()->getAttributeId();
//$eav->printLogQuery(true);

		
$orders = Mage::getModel('sales/order')->getCollection()
		->addFieldToSelect('trading_partner');
		
$saleOrderTableAlias = "sf";
//Table join
$orders->join(
    array($saleOrderTableAlias => 'sales/order_item'),
    'main_table.entity_id = sf.item_id',
	array('sku','name')
);



$orders->getSelect()->
join(Mage::getConfig()->getTablePrefix().'catalog_product_entity_varchar', 
'sf.product_id ='.Mage::getConfig()->getTablePrefix().'catalog_product_entity_varchar.entity_id',
array('value'));

if($partner) {
	$orders->getSelect()
        ->where("main_table.trading_partner = '".$partner."' AND ".Mage::getConfig()->getTablePrefix()."catalog_product_entity_varchar.attribute_id = '".$attributeId."'")
		->distinct(TRUE);
} else {
	$orders->getSelect()
        ->where("main_table.trading_partner IN ('amazon', 'jet', 'Marketplace') AND ".Mage::getConfig()->getTablePrefix()."catalog_product_entity_varchar.attribute_id = '".$attributeId."'")
		->distinct(TRUE);
}

		
		//$orders->printLogQuery(true); exit;
}

if($date && $asel == "q6"){
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=pickup_count.csv');
	// output the column headings
	
	fputcsv($output, array('Trading Partner','Pickup Method','Count'));
	if($partner && $pickupmethod) {
		$orders = Mage::getModel('sales/order')->getCollection()
			->addFieldToFilter('created_at', array('from'=>$date, 'to'=>$dateto))
			->addFieldToFilter('trading_partner', $partner)
			->addFieldToFilter('pickup_method', array('like' => $pickupmethod.'%'))
			->addFieldToSelect('trading_partner')
			->addFieldToSelect('pickup_method');
	} elseif($partner) {
		$orders = Mage::getModel('sales/order')->getCollection()
			->addFieldToFilter('created_at', array('from'=>$date, 'to'=>$dateto))
			->addFieldToFilter('trading_partner', $partner)
			->addFieldToSelect('trading_partner')
			->addFieldToSelect('pickup_method');
	} elseif($pickupmethod) {
		$orders = Mage::getModel('sales/order')->getCollection()
			->addFieldToFilter('created_at', array('from'=>$date, 'to'=>$dateto))
			->addFieldToFilter('pickup_method', array('like' => $pickupmethod.'%'))
			->addFieldToSelect('trading_partner')
			->addFieldToSelect('pickup_method');
	} else{
		$orders = Mage::getModel('sales/order')->getCollection()
			->addFieldToFilter('created_at', array('from'=>$date, 'to'=>$dateto))
			->addFieldToSelect('trading_partner')
			->addFieldToSelect('pickup_method');
	}

$orders->getSelect()
        ->columns('COUNT(*) AS count')
		->group('pickup_method')
		->group('trading_partner')
		->order('count DESC')
		->distinct(TRUE);
//$orders->printLogQuery(true); exit;
}

if($asel == "q7"){
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=inventory_partners.csv');
	// output the column headings
	
	fputcsv($output, array('Trading Partner','Assignation Warehouse','Sku','Name','Quantity'));
if($partner && $warehouse) {
	$orders = Mage::getModel('sales/order')->getCollection()
        ->addFieldToFilter('trading_partner', $partner)
		->addFieldToFilter('assignation_warehouse', $warehouse)
		->addFieldToSelect('trading_partner');
} elseif($partner) {
	$orders = Mage::getModel('sales/order')->getCollection()
        ->addFieldToFilter('trading_partner', $partner)
		->addFieldToSelect('trading_partner');
} elseif($warehouse) {
	$orders = Mage::getModel('sales/order')->getCollection()
		->addFieldToFilter('assignation_warehouse', $warehouse)
		->addFieldToSelect('trading_partner');
} else {
	$orders = Mage::getModel('sales/order')->getCollection()
		->addFieldToSelect('trading_partner');
}

		
$saleOrderTableAlias = "s_o";
//Table join
$orders->join(
    array($saleOrderTableAlias => 'sales/order_item'),
    'main_table.entity_id=s_o.item_id',
	array('sku','name','description')
);

$saleOrderTableAlias1 = "p_o";
$orders->join(
    array($saleOrderTableAlias1 => 'pointofsale/pointofsale'),
    'main_table.assignation_warehouse=p_o.place_id',
	array('name as warehouse')
);

$orders->getSelect()
        ->columns('COUNT(*) AS qty')
		->group('s_o.sku')
		->group('main_table.trading_partner')
		->group('s_o.name')
		->order('qty DESC');
		//$orders->printLogQuery(true); exit;
}


foreach($orders->getData() as $key=>$value) :
	if($asel == "q7"){
		
		$fin[$key]['trading_partner'] = $value['trading_partner'];
		
		$fin[$key]['warehouse'] = $value['warehouse'];
		
		$fin[$key]['sku'] = $value['sku'];
		
		if($value['name'])
			$fin[$key]['name'] = $value['name'];
		else
			$fin[$key]['name'] = $value['description'];
		$fin[$key]['qty'] = $value['qty'];
		
	}
	elseif($date && $asel == "q6"){
		
		$fin[$key]['trading_partner'] = $value['trading_partner'];
		
		$fin[$key]['pickup_method'] = $value['pickup_method'];
		
		$fin[$key]['count'] = $value['count'];
		
	}
	elseif($asel == "q4"){
		
		$fin[$key]['trading_partner'] = $value['trading_partner'];
		
		$fin[$key]['sku'] = $value['sku'];
		
		$fin[$key]['name'] = $value['name'];
		
		$fin[$key]['value'] = $value['value'];
		
	}
	elseif($date && $asel == "q3"){
		
		$fin[$key]['order_id'] = $value['order_id'];
		
		$fin[$key]['partner_order_id'] = $value['partner_order_id'];
		
		$fin[$key]['track_number'] = $value['track_number'];
		
	}
	elseif($date && $asel == "q2") {
		$fin[$key]['total_count'] = $value['total_count'];
		
		$fin[$key]['zipcode'] = $value['zipcode'];
		
		$fin[$key]['city'] = $value['city'];
		
		$fin[$key]['State'] = $value['State'];
		
	} else {
		$fin[$key]['sku'] = $value['sku'];
		if($value['name']) {
			$fin[$key]['name'] = $value['name'];
		}		
		else {
			$fin[$key]['name'] = $value['description'];
		}	
		$fin[$key]['qty'] = $value['qty'];
		
	}
	
endforeach;



$delimiter=',';
$enclosure = '"';

 foreach ($fin as $k=>$v) {

        fputcsv($output, $v, $delimiter,$enclosure);
    }

?>
